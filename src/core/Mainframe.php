<?php

namespace Core;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 11.33.03
 */

use Config,
    Log,
    DB;

use services\Membership\Models\Affiliate;
use Underscore\Types\Arrays;
use Core\Token;

class Mainframe
{

    public $lang;
    public $rememberStaticMinutes;
    public $rememberStaticKey;
    private $cache;

    function __construct()
    {
        $this->lang = \Lang::getLocale();
        //$this->lang = \Cfg::get('DEFAULT_LANGUAGE');
        $this->rememberStaticMinutes = \Config::get('cache.rememberStaticMinutes');
        $this->rememberStaticKey = \Config::get('cache.rememberStaticKey');
        $this->cache = [];
    }

    private function remember($key, callable $callback)
    {
        if (isset($this->cache[$key]) AND $this->cache[$key] != false)
            return $this->cache[$key];
        $this->cache[$key] = call_user_func($callback);
        return $this->cache[$key];
    }

    function timezones()
    {
        return DB::table("timezones")
            ->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_timezones")
            ->orderBy('name')
            ->get();
    }

    function zones()
    {
        return DB::table("zones")
            ->orderBy('name')
            ->get();
    }

    function languages($active = true)
    {
        $query = DB::table("languages")->orderBy('position');
        if ($active) {
            $query->where('active', '=', 1);
        }
        $query->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_languages_" . $active);
        return $query->get();
    }

    function sections($exclude_id = 0)
    {
        $lang = \Core::getLang();
        $query = \Section::rows($lang);
        if ($exclude_id > 0) {
            $query->where("id", '<>', $exclude_id);
        }
        return $query->orderBy("name")->get();
    }

    function pages($section_id = false)
    {
        $lang = \Core::getLang();
        $query = \Page::rows($lang);
        if ($section_id !== false) {
            $query->where("section_id", $section_id);
        }
        return $query->orderBy("name")->get();
    }

    function campaignTypes()
    {
        $query = DB::table("campaign_types")->orderBy('name');
        return $query->get();
    }

    function campaigns()
    {
        return \Campaign::where("active", 1)->orderBy('name')->get();
    }

    function menuTypes()
    {
        $query = DB::table("menu_types")->orderBy('name');
        return $query->get();
    }

    function navTypes()
    {
        $query = DB::table("nav_types")->orderBy('name');
        return $query->get();
    }

    function languagesCodes($active = true)
    {
        $languages = \Mainframe::languages($active);
        $codes = \EchoArray::set($languages)->keys('id')->get();
        return $codes;
    }

    function languagesStrings($active = true, $withBlank = false)
    {
        $languages = \Mainframe::languages();
        $data = [];
        if ($withBlank) $data[''] = 'Scegli...';
        foreach ($languages as $l) {
            $data[$l->id] = $l->name;
        }
        return $data;
    }

    function countries()
    {
        $lang = $this->lang;
        return DB::table("countries")
            ->join('countries_lang', 'id', '=', 'country_id')
            ->where('lang_id', '=', $lang)
            ->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_countries_$lang")
            ->orderBy('name')->get();
    }

    function layouts($scope)
    {
        return DB::table("layouts")
            ->where('scope', '=', $scope)
            ->orderBy('position')->get();
    }

    function months()
    {
        $lang = $this->lang;
        return DB::table("months")
            ->join('months_lang', 'id', '=', 'month_id')
            ->where('lang_id', '=', $lang)
            ->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_months_$lang")
            ->orderBy('position')->get();
    }

    function countriesActive()
    {
        $lang = $this->lang;
        return DB::table("countries")
            ->join('countries_lang', 'id', '=', 'country_id')
            ->where('lang_id', '=', $lang)
            ->where('active', 1)
            ->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_countries_active_$lang")
            ->orderBy('name')->get();
    }

    function price_rules()
    {
        $lang = $this->lang;
        return \PriceRule::rows($lang)
            ->orderBy('name')->get();
    }


    function carriers()
    {
        $lang = $this->lang;
        return \Carrier::rows($lang)->where("active", 1)->orderBy('name')->get();
    }

    function messageTemplates()
    {
        $lang = $this->lang;
        return \MessageTemplate::rows($lang)->where("active", 1)->orderBy('name')->get();
    }

    function emails()
    {
        $lang = $this->lang;
        return \Email::rows($lang)->where("active", 1)->orderBy('name')->get();
    }

    function stock_reasons()
    {
        $lang = $this->lang;
        return \StockReason::rows($lang)->where("active", 1)->orderBy('id')->get();
    }

    function payment_states($acl = false)
    {
        $lang = $this->lang;

        $builder = \PaymentState::rows($lang)->where("deleted", 0)->where("hidden", 0)->orderBy('name');
        if ($acl) {
            $builder->where('unremovable', 0);
        }
        return $builder->get();

    }

    function order_states($acl = false)
    {
        $lang = $this->lang;

        $builder = \OrderState::rows($lang)->where("deleted", 0)->where("hidden", 0)->orderBy('name');
        if ($acl) {
            $builder->where('unremovable', 0);
        }
        return $builder->get();

    }

    function rma_states()
    {
        $lang = $this->lang;
        return \RmaState::rows($lang)->where("deleted", 0)->where("hidden", 0)->orderBy('position')->get();
    }


    function genders()
    {
        $lang = $this->lang;
        return \Gender::rows($lang)->where("active", 1)->get();
    }

    function peoples()
    {
        $lang = $this->lang;
        return \People::rows($lang)->where("active", 1)->get();
    }

    function payments()
    {
        $lang = $this->lang;
        return \Payment::rows($lang)->where("active", 1)->orderBy('name')->get();
    }

    function suppliers()
    {
        $lang = $this->lang;
        return \Supplier::rows($lang)->orderBy('name')->get();
    }

    function states($country_id)
    {
        return DB::table("states")
            ->where('country_id', '=', $country_id)
            ->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_states_$country_id")
            ->orderBy('name')->get();
    }

    function currencies()
    {
        return DB::table("currencies")
            ->where('active', 1)->get();
    }

    function modulePositions()
    {
        return DB::table("module_positions")->orderBy("name")->get();
    }

    function moduleTypes()
    {
        return DB::table("module_types")->where("active", 1)->orderBy("name")->get();
    }

    function magicTokensTemplates()
    {
        return \MagicTokenTemplate::where("active", 1)->orderBy("position")->get();
    }

    function days()
    {
        $lang = $this->lang;
        return DB::table("days")
            ->join('days_lang', 'id', '=', 'day_id')
            ->where('lang_id', '=', $lang)
            ->remember($this->rememberStaticMinutes, $this->rememberStaticKey . "_days")
            ->orderBy('position')->get();
    }

    function mailCryptProtocols()
    {
        return array('NONE' => 'Nessuno', 'TLS' => 'TLS', 'SSL' => 'SSL');
    }

    function currenciesFormats()
    {
        return array(1 => '[S]0,000.00 (in dollari)', 2 => '0 000,00[S] (in euro)', 3 => '[S]0.000,00', 4 => '0,000.00[S]', 5 => "0'000.00[S]");
    }

    function mailModes()
    {
        return array('PHP' => 'Utilizza la funzione mail() del PHP', 'SMTP' => 'Utilizza un server SMTP');
    }

    function backOrders($default = true)
    {
        $default = \Cfg::get("PRODUCTS_BACKORDERS");
        $label = ($default == 'allow') ? "Default (Permetti gli ordini)" : "Default (Rifiuta gli ordini)";
        $data = array('allow' => 'Permetti gli ordini', 'disallow' => 'Rifiuta gli ordini');
        if ($default) {
            $data['default'] = $label;
        }
        return $data;
    }

    function notifyStocks($default = true)
    {
        $default = \Cfg::get("PRODUCTS_NOTIFY_STOCK");
        $label = ($default == 'allow') ? "Default (Invia un avviso)" : "Default (Non inviare un avviso)";
        $data = array('allow' => 'Invia un avviso', 'disallow' => 'Non inviare un avviso');
        if ($default) {
            $data['default'] = $label;
        }
        return $data;
    }

    function qtyIncrements($default = true)
    {
        $default = \Cfg::get("PRODUCTS_QTY_INCREMENTS");
        $label = ($default == 'allow') ? "Default (Permetti incremento quantità)" : "Default (Non permettere incremento quantità)";
        $data = array('allow' => 'Permetti incremento quantità', 'disallow' => 'Non permettere incremento quantità');
        if ($default) {
            $data['default'] = $label;
        }
        return $data;
    }

    function geoBeahviorRestricted()
    {
        return array(
            'DENY_CATALOG' => 'Gli Utenti non possono vedere il Catalogo dei prodotti',
            'DENY_ORDER' => 'Gli Utenti possono vedere il Catalogo dei prodotti ma non possono effettuare un Ordine'
        );
    }

    function geoBeahviorUnrecognized()
    {
        return array(
            'ALLOW_ALL' => 'Tutte le funzioni sono disponibili',
            'DENY_CATALOG' => 'Gli Utenti non possono vedere il Catalogo dei prodotti',
            'DENY_ORDER' => 'Gli Utenti possono vedere il Catalogo dei prodotti ma non possono effettuare un Ordine'
        );
    }

    function robots()
    {
        return array(
            'INDEX,FOLLOW' => 'INDEX,FOLLOW',
            'NOINDEX,FOLLOW' => 'NOINDEX,FOLLOW',
            'INDEX,NOFOLLOW' => 'INDEX,NOFOLLOW',
            'NOINDEX,NOFOLLOW' => 'NOINDEX,NOFOLLOW',
        );
    }

    function frequencies()
    {
        return array(
            'always' => 'Sempre',
            'hourly' => 'Orario',
            'daily' => 'Giornaliero',
            'weekly' => 'Settimanalmente',
            'monthly' => 'Mensilmente',
            'yearly' => 'Annualmente',
            'never' => 'Mai',
        );
    }

    function combinationTypes()
    {
        return array(
            '0' => 'Nessuno',
            '+' => 'Aumento',
            '-' => 'Diminuzione'
        );
    }

    function frequenciesMin()
    {
        return array(
            'daily' => 'Giornaliero',
            'weekly' => 'Settimanalmente',
            'monthly' => 'Mensilmente'
        );
    }

    function mailSenders()
    {
        return array(
            'GENERAL' => 'Contatti generali',
            'ORDERS' => 'Gestione ordini',
            'CUSTOMERS' => 'Assistenza Clienti',
            'CUSTOM1' => 'Email personalizzata 1',
            'CUSTOM2' => 'Email personalizzata 2',
        );
    }

    function watermarkPosition()
    {
        return array(
            'stretch' => 'Stretch',
            'tile' => 'Tile',
            'top-left' => 'In cima/Sinistra',
            'top-right' => 'In cima/Destra',
            'bottom-left' => 'In basso/Sinistra',
            'bottom-right' => 'In basso/destra',
            'center' => 'Centro',
        );
    }

    function publishOptions()
    {
        return array(
            '1' => 'Pubblicato',
            '0' => 'Non pubblicato'
        );
    }

    function activeOptions()
    {
        return array(
            '1' => 'Abilita',
            '0' => 'Disabilita'
        );
    }

    function overrideOptions()
    {
        return array(
            '0' => 'Usa impostazioni globali',
            '1' => 'Usa impostazioni correnti',
            '-1' => 'Disabilita le impostazioni',
        );
    }

    function ogpOptions()
    {
        return array(
            'place' => 'place',
            'website' => 'website',
            'book' => 'book',
            'profile' => 'profile',
            'object' => 'object',
            'article' => 'article',
            'product' => 'product',
            'event' => 'event'
        );
    }

    function publicAcl()
    {
        return array(
            'P' => 'Pubblico',
            'R' => 'Solo utenti registrati'
        );
    }

    function yesNo()
    {
        return array(
            '1' => 'Sì',
            '0' => 'No'
        );
    }

    function enableDisabled()
    {
        return array(
            '1' => 'Abilita',
            '0' => 'Disabilita'
        );
    }

    function fieldTypes()
    {
        return array(
            'text' => 'Campo di testo',
            'textarea' => 'Area di testo',
            //'date' => 'Data',
            'boolean' => 'Sì/No',
            'select' => 'Elenco a discesa (selezione singola)',
            'multiselect' => 'Elenco a discesa (selezione multipla)',
            //'price' => 'Prezzo',
            //'media_image' => 'Immagine Media',
            //'file' => 'File'
        );
    }

    function validatorClasses()
    {
        return array(
            '' => 'Nessuno',
            'number' => 'Numeri decimali',
            'integer' => 'Numeri interi',
            'email' => 'Email',
            'url' => 'URL',
            'onlyNumberSp' => 'Solo numeri e spazi',
            'onlyLetterSp' => 'Solo lettere e spazi',
            'onlyLetterNumber' => 'Solo lettere e numeri, no spazi'
        );
    }

    function productTypes()
    {
        return array(
            'default' => 'Prodotto (default)',
            'pack' => 'Gruppo (prodotto composito)',
            'virtual' => 'Credito virtuale (Gift Card)',
            'service' => 'Servizio, prodotto scaricabile, ecc.',
        );
    }

    function productVisibility()
    {
        return array(
            'both' => 'Ovunque',
            'catalog' => 'Catalogo',
            'search' => 'Ricerca',
            'none' => 'Da nessuna parte',
        );
    }

    function productStatus()
    {
        return array(
            'N' => 'Nuovo',
            'U' => 'Usato',
            'R' => 'Ristrutturato'
        );
    }

    function seoXTypes()
    {
        return array(
            'title' => 'Titolo (tag title)',
            //'metatitle' => 'Titolo (meta title)',
            'h1' => 'Header H1',
            'keywords' => 'Meta Keywords',
            'description' => 'Meta Description',
            'facebook' => 'Facebook Description',
            'twitter' => 'Twitter Card Description',
            'google' => 'Google+ Description',
        );
    }

    function seoContexts()
    {
        return array(
            'product' => 'Scheda Prodotto',
            'seotext' => 'Scheda testo SEO',
        );
    }

    function carrierShippingMethod()
    {
        return array(
            1 => 'In base al peso totale',
            2 => 'In base al prezzo totale'
        );
    }

    function carrierRangeBehavior()
    {
        return array(
            0 => 'Usa il costo della fascia più grande',
            1 => 'Disattiva il mezzo di trasporto'
        );
    }

    function categoriesTree($parent_id, &$main = [], $parent_name = null)
    {
        $lang_id = $this->lang;
        $rows = DB::table('categories AS C')
            ->leftJoin('categories_lang AS L', 'C.id', '=', 'L.category_id')
            ->leftJoin('categories AS N', 'C.id', '=', 'N.parent_id')
            ->where('lang_id', $lang_id)
            ->where('C.parent_id', $parent_id)
            ->whereNull('C.deleted_at')
            ->groupBy('C.id')
            ->select('C.id', 'C.parent_id', 'L.name', DB::raw('COUNT(N.id) AS children'))
            ->get();

        $data = array();


        $firstLevelKey = 'Primo livello';
        if ($parent_id == 0)
            $data[$firstLevelKey] = array();
        foreach ($rows as $row) {

            if ($row->children > 0 AND $row->parent_id == 0) {
                $data[$row->name] = $this->categoriesTree($row->id, $main, $row->name);
            }
            if ($row->children > 0 AND $row->parent_id != 0) {
                $data[$row->id] = $row->name;
                $main[$parent_name . " >> " . $row->name] = $this->categoriesTree($row->id, $main, $row->name);
            }
            if ($row->children == 0 and $row->parent_id != 0) {
                $data[$row->id] = $row->name;
            }
            if ($row->parent_id == 0) {
                $data[$firstLevelKey][$row->id] = $row->name;
            }
        }
        return $parent_id == 0 ? array_merge($data, $main) : $data;
    }

    function collectionsTree()
    {

        $data = [];
        $lang_id = $this->lang;
        $query = "SELECT B.*,COUNT(C.id) as cnt FROM brands_lang B LEFT JOIN collections C ON C.brand_id=B.brand_id WHERE B.lang_id='$lang_id' GROUP BY B.brand_id HAVING cnt>0 ORDER BY name";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $collections = $this->collections($row->brand_id);
            $data_collections = \EchoArray::set($collections)->keyIndex('id', 'name')->get();
            $data[$row->name] = $data_collections;
        }
        return $data;
    }

    function pagesTree()
    {

        $data = [];
        $lang_id = $this->lang;
        $query = "SELECT B.*,COUNT(C.id) as cnt FROM sections_lang B LEFT JOIN pages C ON C.section_id=B.section_id WHERE B.lang_id='$lang_id' GROUP BY B.section_id HAVING cnt>0 ORDER BY name";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $collections = $this->pages($row->section_id);
            $data_collections = \EchoArray::set($collections)->keyIndex('id', 'name')->get();
            $data[$row->name] = $data_collections;
        }
        $collections = $this->pages(0);
        $data_collections = \EchoArray::set($collections)->keyIndex('id', 'name')->get();
        $data['Senza categoria'] = $data_collections;
        return $data;
    }

    function statesTree()
    {

        $data = [];
        $lang_id = $this->lang;
        $query = "SELECT C.*,COUNT(S.id) as cnt FROM countries_lang C LEFT JOIN states S ON S.country_id=C.country_id WHERE C.lang_id='$lang_id' GROUP BY C.country_id HAVING cnt>0 ORDER BY name";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $collections = $this->states($row->country_id);
            $data_collections = \EchoArray::set($collections)->keyIndex('id', 'name')->get();
            $data[$row->name] = $data_collections;
        }
        return $data;
    }

    function _collectionsTree($parent_id)
    {
        $lang_id = $this->lang;
        $rows = DB::table('collections AS C')
            ->leftJoin('collections_lang AS L', 'C.id', '=', 'L.brand_id')
            ->leftJoin('brands AS B', 'B.id', '=', 'C.brand_id')
            ->where('lang_id', $lang_id)
            ->where('C.parent_id', $parent_id)
            ->groupBy('C.id')
            ->select('C.id', 'C.parent_id', 'L.name', DB::raw('COUNT(N.id) AS children'))->get();

        $data = array();

        $firstLevel = array();
        $firstLevelKey = 'Primo livello';
        if ($parent_id == 0)
            $data[$firstLevelKey] = array();
        foreach ($rows as $row) {
            if ($row->children > 0) {
                $data[$row->name] = $this->categoriesTree($row->id);
            } else {
                $data[$row->id] = $row->name;
            }
            if ($row->parent_id == 0) {
                $data[$firstLevelKey][$row->id] = $row->name;
            }
        }
        return $data;
        return array_merge($firstLevel, $data);
    }

    function categories()
    {
        $lang = $this->lang;
        return \Category::join('categories_lang', 'id', '=', 'category_id')
            ->where('lang_id', '=', $lang)
            ->orderBy('name')->get();
    }

    function categoriesIn($ids)
    {
        if (!is_array($ids)) {
            $ids = explode(",", $ids);
        }
        $lang = $this->lang;
        return \Category::join('categories_lang', 'id', '=', 'category_id')
            ->where('lang_id', '=', $lang)
            ->whereIn('id', $ids)
            ->orderBy('name')->get();
    }

    function products($ids = null)
    {
        $lang = $this->lang;

        $query = \Product::join('products_lang', 'id', '=', 'product_id')
            ->where('lang_id', '=', $lang)
            ->select(array("id", "name", "sku"));

        if ($ids != null) {
            if (!is_array($ids)) {
                $ids = explode(",", $ids);
            }
            $query->whereIn("id", $ids);
        }

        $rows = $query->orderBy('name')->get();
        $count = count($rows);
        for ($i = 0; $i < $count; $i++) {
            $rows[$i]->name = $rows[$i]->name . " ({$rows[$i]->sku})";
        }
        return $rows;
    }

    function brands()
    {
        $lang = $this->lang;
        return \Brand::join('brands_lang', 'id', '=', 'brand_id')
            ->where('lang_id', '=', $lang)
            ->select(array('id', 'name'))
            ->orderBy('name')->get();
    }

    function trends()
    {
        $lang = $this->lang;
        return \Trend::rows($lang)
            ->select(array('id', 'name'))
            ->orderBy('name')->get();
    }

    function promo()
    {
        $lang = $this->lang;
        return \PriceRule::rows($lang)
            ->select(array('id', 'name'))
            ->orderBy('name')->get();
    }

    function taxes()
    {
        $lang = $this->lang;
        return \Tax::join('taxes_lang', 'id', '=', 'tax_id')
            ->where('lang_id', '=', $lang)
            ->select(array('id', 'name'))
            ->orderBy('name')->get();
    }

    function taxGroups()
    {
        $lang = $this->lang;
        return \TaxGroup::all();
    }

    function collections($brand_id)
    {
        $lang = $this->lang;
        $filter = '';
        if ($brand_id > 0)
            $filter = " AND C.brand_id=$brand_id";

        $query = "SELECT C.*,L.name,G.`name` AS category_name,B.`name` AS brand_name FROM collections C JOIN collections_lang L ON C.id=L.collection_id AND L.lang_id=? LEFT JOIN categories_lang G ON C.category_id=G.category_id AND G.lang_id=?"
            . " LEFT JOIN brands_lang B ON C.brand_id=B.brand_id AND B.lang_id=? "
            . " WHERE C.id >0 AND ISNULL(C.deleted_at) "
            . " $filter ORDER BY L.name";

        $rows = DB::select($query, [$lang, $lang, $lang]);
        $c = count($rows);
        for ($i = 0; $i < $c; $i++) {
            $row = $rows[$i];
            $row->name = ($row->category_name == '') ? $row->name : $row->name . " (Categoria: $row->category_name)";
            $row->name .= " [$row->brand_name]";
        }
        return $rows;

        $rows = \Collection::join('collections_lang', 'id', '=', 'collection_id')
            ->leftJoin('brands_lang', 'collections.brand_id', '=', 'brands_lang.brand_id')
            ->where('collections.lang_id', '=', $lang)
            ->select(array('collections.id', 'collections.name', 'collections.category_id', DB::raw('brands_lang.name as brand_name')))
            ->orderBy('collections.name')->get();
    }

    function tags()
    {
        return \Tag::all();
    }

    function attributes_multi($exclude_id = 0)
    {
        $lang = $this->lang;
        return DB::table("attributes")
            ->join('attributes_lang', 'id', '=', 'attribute_id')
            ->where('lang_id', '=', $lang)
            ->where('id', '<>', $exclude_id)
            ->whereIn('frontend_input', ['select', 'multiselect'])
            ->select(array('id', 'name'))
            ->orderBy('name')->get();
    }

    function attributes_sets($exclude_id = 0)
    {
        $rules = \AttributeSet::where("id", '<>', $exclude_id)->orderBy('uname')->get();
        $rules_ids = [];
        foreach($rules as $rule){
            $rules_ids[] = $rule->id;
        }
        $aggregates = DB::table('attributes_sets_content_cache')->whereIn('attribute_set_id', $rules_ids)->groupBy('attribute_set_id')->select('attribute_set_id', DB::raw('count(`id`) as aggregate'))->get();
        $counters = [];
        foreach($aggregates as $aggregate){
            $counters[$aggregate->attribute_set_id] = $aggregate->aggregate;
        }
        $data = array();
        foreach ($rules as $rule) {
            $total = isset($counters[$rule->attribute_set_id]) ? $counters[$rule->attribute_set_id] : 0;
            $data[$rule->id] = "$rule->uname (Contiene $total attributi)";
        }
        return $data;
    }

    function attributes_by_set($set, $orderBy = 'name')
    {
        $lang_id = \Lang::getLocale();

        $orderBy = ($orderBy == 'position') ? '`attributes_sets_content_cache`.`position`' : "`attributes`.`$orderBy`";

        $query = "select DISTINCT `attributes`.id, `name`, `frontend_input` "
            . "from `attributes` left join `attributes_lang` on `attributes_lang`.`attribute_id` = `id` and `lang_id` = :lang_id "
            . "LEFT JOIN `attributes_sets_content_cache` ON `attributes`.id=`attributes_sets_content_cache`.attribute_id "
            . "where `attributes`.`deleted_at` is null "
            . "AND attribute_set_id= :set "
            . "group by `attributes`.`id` "
            . "order by :orderBy asc";

        $rows = DB::select(DB::raw($query), ['lang_id' => $lang_id, 'set' => $set, 'orderBy' => $orderBy]);

        return $rows;

        \Utils::logSql($queryObj);

        return $queryObj->get();
    }

    function attributes_added_by_set($set, $old = array())
    {
        $lang_id = \Lang::getLocale();
        $queryObj = \Attribute::leftJoin('attributes_lang', function ($join) use ($lang_id) {
            $join->on('attributes_lang.attribute_id', '=', 'id')->on('lang_id', '=', DB::raw("'$lang_id'"));
        })
            ->whereIn('id', function ($query) use ($set) {
                $query->select(['attribute_id'])
                    ->from('attributes_sets_content')
                    ->where('rule', '+')
                    ->where('attribute_set_id', $set);
            });

        if (is_array($old)) {
            $queryObj->orWhereIn('id', $old);
        }

        $queryObj->select(DB::raw('DISTINCT id'), 'name', 'frontend_input')->orderBy('name')->groupBy('id');

        //\Utils::logSql($queryObj, "attributes_added_by_set query");

        return $queryObj->get();
    }

    function attributes_toadd_by_set($set, $parent)
    {
        $lang_id = \Lang::getLocale();

        $queryObj = \Attribute::leftJoin('attributes_lang', function ($join) use ($lang_id) {
            $join->on('attributes_lang.attribute_id', '=', 'id')->on('lang_id', '=', DB::raw("'$lang_id'"));
        })
            ->whereNotIn('id', function ($query) use ($set) {
                $query->select(['attribute_id'])
                    ->from('attributes_sets_content')
                    ->where('rule', '+')
                    ->where('attribute_set_id', $set);
            })
            ->whereNotIn('id', function ($query) use ($parent) {
                $query->select(['attribute_id'])
                    ->from('attributes_sets_content_cache')
                    ->where('attribute_set_id', $parent);
            })->select(DB::raw('DISTINCT id'), 'name', 'frontend_input')->orderBy('name')->groupBy('id');

        //\Utils::logSql($queryObj, "attributes_toadd_by_set query");

        return $queryObj->get();
    }

    function attributes_removed_by_set($set, $old = array())
    {
        $lang_id = \Lang::getLocale();

        $queryObj = \Attribute::leftJoin('attributes_lang', function ($join) use ($lang_id) {
            $join->on('attributes_lang.attribute_id', '=', 'id')->on('lang_id', '=', DB::raw("'$lang_id'"));
        })
            ->whereIn('id', function ($query) use ($set) {
                $query->select(['attribute_id'])
                    ->from('attributes_sets_content')
                    ->where('rule', '-')
                    ->where('attribute_set_id', $set);
            });

        if (is_array($old)) {
            $queryObj->orWhereIn('id', $old);
        }

        $queryObj->select(DB::raw('DISTINCT id'), 'name', 'frontend_input')->orderBy('name')->groupBy('id');

        //\Utils::logSql($queryObj, "attributes_removed_by_set query");

        return $queryObj->get();
    }

    function attributes_by_ids($ids = array(), $orderBy = 'name')
    {
        $lang_id = \Lang::getLocale();

        $queryObj = \Attribute::leftJoin('attributes_lang', function ($join) use ($lang_id) {
            $join->on('attributes_lang.attribute_id', '=', 'id')->on('lang_id', '=', DB::raw("'$lang_id'"));
        })
            ->whereIn('id', $ids);

        $queryObj->select(DB::raw('DISTINCT id'), 'name', 'frontend_input')->orderBy($orderBy)->groupBy('id');

        //\Utils::logSql($queryObj, "attributes_by_ids query");

        return $queryObj->get();
    }

    function combinations($product_id = 0)
    {
        $lang = $this->lang;
        \Utils::log($product_id, "combination product id");
        $data = ($product_id > 0) ? \ProductCombination::where("product_id", $product_id)->orderBy('name')->get() : \ProductCombination::orderBy('name')->get();
        return $data;
    }

    function customers($shop_id)
    {
        $lang = $this->lang;
        return \Customer::where("shop_id", $shop_id)->orderBy('name')->where('created_at', '>', '2015-01-01')->get();
    }

    function customersGroups($shop_id)
    {
        $lang = $this->lang;
        return \CustomerGroup::join('customers_groups_lang', 'id', '=', 'customer_group_id')
            ->where('lang_id', '=', $lang)
            ->select(array('id', 'name'))
            ->orderBy('name')->get();
    }

    function void()
    {
        return ['' => 'Seleziona...'];
    }

    function magicTokens()
    {
        $data = [];
        $rows = \DB::table("magic_tokens")->where("active", 1)->orderBy("position")->get();
        foreach ($rows as $row) {
            $data[] = $row;
        }

        $rows = DB::table("attributes")->where("is_shortcode", 1)->whereIn("frontend_input", ["select", "multiselect"])->orderBy("position")->get();
        foreach ($rows as $row) {
            $obj = new \stdClass();
            $obj->token = \Str::upper("attr:" . $row->code);
            $obj->description = "Valore attributo per " . $row->ldesc;
            $obj->regexp_match = "";
            $data[] = $obj;
        }

        $rows = DB::table("lexicons")->where("is_shortcode", 1)->orderBy("id")->get();
        foreach ($rows as $row) {
            $obj = new \stdClass();
            $obj->token = \Str::upper("lang:" . $row->code);
            $obj->description = "Valore lexicon per \"" . (($row->ldesc == "") ? $row->code : $row->ldesc) . "\"";
            $obj->regexp_match = "";
            $data[] = $obj;
        }
        return $data;
    }

    function selectCountries($withBlank = false, $label = 'Scegli')
    {
        $rows = $this->countries();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) {
            $data = ['' => $label] + $data;
        }
        return $data;
    }


    function selectLayouts($scope, $withBlank = false, $label = 'Nessuno')
    {
        $rows = $this->layouts($scope);
        $data = \EchoArray::set($rows)->keyIndex('filename', 'name')->get();
        if ($withBlank) {
            $data = ['' => $label] + $data;
        }
        return $data;
    }


    function selectDaysNumbers($withBlank = false, $label = 'Scegli')
    {
        $data = [];
        for ($i = 1; $i <= 31; $i++) {
            $data[$i] = $i;
        }
        if ($withBlank) {
            $data = ['' => $label] + $data;
        }
        return $data;
    }


    function selectYearsNumbers($withBlank = false, $label = 'Scegli')
    {
        $data = [];
        $limit = date("Y") - 12;
        $min = 1920;
        for (; $limit >= $min; $limit--) {
            $data[$limit] = $limit;
        }
        if ($withBlank) {
            $data = ['' => $label] + $data;
        }
        return $data;
    }

    function selectMonths($withBlank = false, $label = 'Scegli')
    {
        $rows = $this->months();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        $counter = 0;
        foreach ($data as $key => $item) {
            $counter++;
            $data[$key] = $item . ' - ' . $counter;
        }
        if ($withBlank) {
            $data = ['' => $label] + $data;
        }
        return $data;
    }

    function selectPriceRules()
    {
        $rows = $this->price_rules();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectModulePositions($withAll = false, $emtpyText = false)
    {
        $rows = $this->modulePositions();
        $data = \EchoArray::set($rows)->keyIndex('name', 'name')->get();
        if ($withAll) {
            $data = ['' => ($emtpyText) ? $emtpyText : 'Tutte le posizioni'] + $data;
        }
        return $data;
    }


    function selectModulePositionIds($withAll = false)
    {
        $rows = $this->modulePositions();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withAll) {
            $data = ['' => 'Tutte le posizioni'] + $data;
        }
        return $data;
    }


    function selectModuleTypes($withAll = false)
    {
        $rows = $this->moduleTypes();
        $data = \EchoArray::set($rows)->keyIndex('module', 'name')->get();
        if ($withAll) {
            $data = ['' => 'Tutte le tipologie'] + $data;
        }
        return $data;
    }

    function selectSuppliers()
    {
        $rows = $this->suppliers();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectStates($country_id)
    {
        $rows = $this->states($country_id);
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectTimezones()
    {
        $rows = $this->timezones();
        return \EchoArray::set($rows)->keyIndex('name', 'name')->get();
    }

    function selectZones()
    {
        $rows = $this->zones();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectLanguages($active = true)
    {
        $rows = $this->languages($active);
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectCampaignTypes()
    {
        $rows = $this->campaignTypes();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectMenuTypes()
    {
        $rows = $this->menuTypes();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectModuleTemplates()
    {
        $rows = DB::table("module_templates")->where("active", 1)->orderBy("position")->get();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function modulePositionsDesign($row_index)
    {
        $rows = \ModulePosition::where("active", 1)->where("is_design", 1)->where('row_index', $row_index)->orderBy("position")->get();
        return $rows;
    }

    function modulePositionsRows()
    {
        $rows = \ModulePosition::where("active", 1)->where("is_design", 1)->orderBy("row_index")->groupBy("row_index")->lists("row_index");
        return $rows;
    }

    function modules($mod_position)
    {
        $lang = \Core::getLang();
        $rows = \Module::rows($lang)->where("mod_position", $mod_position)->orderBy("position")->get();
        return $rows;
    }

    function modulesPositionsDesign()
    {
        $rows = \ModulePosition::where("is_design", 1)->orderBy('name')->get();
        return $rows;
    }

    function selectModulesPositionsDesign()
    {
        $rows = $this->modulesPositionsDesign();
        return \EchoArray::set($rows)->keyIndex('name', 'name')->get();
    }

    function modulesUnbinded()
    {
        $lang = \Core::getLang();
        $rows = \Module::rows($lang)->whereNotIn('mod_position', function ($query) {
            $query->select(['name'])
                ->from('module_positions')
                ->where('active', 1)
                ->where('is_design', 1);
        })->orderBy("name")->get();
        foreach ($rows as $row) {
            $row->name = "$row->name ($row->mod_type)";
        }
        return $rows;
    }

    function selectModulesUnbinded()
    {
        $rows = $this->modulesUnbinded();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectNavTypes()
    {
        $rows = $this->navTypes();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectDays()
    {
        $rows = $this->days();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectNavsSources()
    {
        $data = [];
        $lang = \Core::getLang();

        $submenu = [
            "base" => "Base",
            'general' => 'Proprietà generale prodotti',
            'pricerange' => 'Fascie di prezzo'
        ];
        $data["Navigazione"] = $submenu;

        $submenu = [
            'section' => 'Sezione',
            'page' => 'Pagina'
        ];
        $data["Contenuti"] = $submenu;


        $submenu = [
            'category' => 'Categoria',
            'brand' => 'Brand',
            'collection' => 'Collezione',
            'trend' => 'Gruppo personalizzato',
        ];

        $data["Catalogo"] = $submenu;

        $submenu = [];
        $rows = \Attribute::rows($lang)->where("is_nav", 1)->whereIn("frontend_input", ['select', 'multiselect'])->orderBy("name")->get();
        foreach ($rows as $row) {
            $submenu[$row->code] = $row->name;
        }
        $data['Attributi principali'] = $submenu;


        return $data;
    }

    function selectNavs($withModifiers = false)
    {
        $data = [];
        $lang = \Core::getLang();

        if ($withModifiers == false) {
            $submenu = [
                "base" => "Struttura base navigazione",
                "list" => "Entità di raccordo",
                'section' => 'Sezione',
                'page' => 'Pagina'
            ];
            $data["Contenuti"] = $submenu;
        }

        $submenu = [
            'category' => 'Categoria',
            'brand' => 'Brand',
            'collection' => 'Collezione',
            'product' => 'Prodotto singolo',
            'trend' => 'Gruppo personalizzato'
        ];
        if ($withModifiers == true) {
            unset($submenu['product']);
        }
        $data["Catalogo"] = $submenu;

        $submenu = [];
        $rows = \Attribute::rows($lang)->where("is_nav", 1)->whereIn("frontend_input", ['select', 'multiselect'])->orderBy("name")->get();
        foreach ($rows as $row) {
            $submenu[$row->code] = $row->name;
        }
        $data['Attributi principali'] = $submenu;

        $submenu = [
            'status' => 'Status prodotto',
            'qty' => 'Status quantità prodotto',
            'pricerange' => 'Fascie di prezzo'
        ];
        $data["Proprietà prodotti"] = $submenu;

        $submenu = [
            'promo' => 'Promozione',
        ];
        $data["Altro"] = $submenu;

        /*if ($withModifiers == false) {
            $submenu = [
                'url' => 'URL personalizzato',
                'separator' => 'Separatore',
            ];
            $data["Altro"] = $submenu;
        }*/

        return $data;
    }

    function selectNavsFilter()
    {
        $lang = \Core::getLang();
        $data = [];

        $submenu = [
            'minprice' => 'Prezzo minimo',
            'maxprice' => 'Prezzo massimo',
        ];
        $data["Altro"] = $submenu;

        $submenu = [];
        $rows = \Attribute::rows($lang)->where("is_nav", 0)->whereIn("frontend_input", ['select', 'multiselect'])->orderBy("name")->get();
        foreach ($rows as $row) {
            $submenu[$row->code] = $row->name;
        }
        $data['Attributi secondari'] = $submenu;


        return $data;
    }

    function selectNavsContentTypes()
    {
        $data = ['' => 'Nessuno', 'html' => 'HTML', 'image' => 'Immagine', 'modulePosition' => 'Posizione moduli'];
        return $data;
    }

    function selectNavsLinksModes()
    {
        $data = ['_self' => 'Stessa finestra', '_blank' => 'Nuova finestra', '_parent' => 'Finestra superiore'];
        return $data;
    }

    function selectNavsConditions()
    {
        $data = ['in' => 'Elementi nella lista', 'out' => 'Elementi fuori dalla lista', 'all' => 'Tutti', 'children' => 'Tutti i figli degli elementi nella lista'];
        return $data;
    }

    function selectNavsOrdering()
    {
        $data = ['given' => 'Ordinamento dato', 'alpha' => 'Alfabetico', 'position' => 'Posizione'];
        return $data;
    }

    function selectNavsTypes($mode = '')
    {
        $data = ['link' => 'Link diretto', 'url' => 'URL Personalizzato', 'separator' => 'Separatore (link vuoto)'];
        if ($mode == 'full') {
            //$data = $data + ['list' => 'Elenco di link', 'html' => 'HTML' , 'modulePosition' => 'Posizione moduli'];
            $data = $data + ['container' => 'Contenitore/Colonna', 'auto' => 'Lista automatica'];
        }
        return $data;
    }

    function selectNavsOther()
    {
        $data = [
            'Generale' => ['url' => 'URL Personalizzato', 'separator' => 'Separatore (Url vuoto)', 'container' => 'Contenitore/Colonna'],
            //'Posizione moduli' => $this->selectModulePositions(false),
            'Catalogo' => ['product' => 'Prodotto singolo'],
            'Speciale' => ['auto' => 'Lista automatica']
        ];
        return $data;
    }

    function selectNavsGeneralTree()
    {
        $lang = \Core::getLang();
        $data = array();
        $cache = array();
        $navtypes = \NavType::whereNotIn("id", array(1, 5))->get();
        foreach ($navtypes as $obj) {
            $data[$obj->name] = array();
            $cache[$obj->id] = $obj->name;
        }
        $rows = \Nav::rows($lang)->get();
        foreach ($rows as $row) {
            if (isset($cache[$row->navtype_id])) {
                $name = $cache[$row->navtype_id];
                $data[$name][$row->id] = $row->getPublicName();
            }
        }
        return $data;
    }

    function selectNavsTree()
    {
        $lang = \Core::getLang();
        $data = array();
        $cache = array();
        $navtypes = \NavType::get();
        foreach ($navtypes as $obj) {
            $data[$obj->name] = array();
            $cache[$obj->id] = $obj->name;
        }
        $rows = \Nav::rows($lang)->get();
        foreach ($rows as $row) {
            if (isset($cache[$row->navtype_id])) {
                $name = $cache[$row->navtype_id];
                $data[$name][$row->id] = $row->getPublicName();
            }
        }
        return $data;
    }

    function selectNavsGeneral()
    {
        $lang = \Core::getLang();
        $data = array();
        $cache = array();

        $rows = \Nav::rows($lang)->whereNotIn("navtype_id", array(1, 5))->get();
        foreach ($rows as $row) {
            $data[$row->id] = $row->getPublicName();
        }
        return $data;
    }

    function selectNavsTargetReordable($obj)
    {
        $type = $obj['target_type'];
        $ids = $obj['target_id'];
        $order = $obj['target_order'];

        $data = $this->selectNavsTarget($type, false);
        \Utils::log($data, "DATA");
        if ($order == 'given' AND count($data) > 0 AND count($ids) > 0) {
            $temp = [];
            foreach ($ids as $id) {
                $temp[$id] = $data[$id];
                unset($data[$id]);
            }
            \Utils::log($temp, "TEMP");
            $data = $temp + $data;
        }
        \Utils::log($data, "DATA AFTER");
        return $data;
    }

    function selectNavsTarget($type, $useTree = true)
    {
        $lang = \Core::getLang();
        switch ($type) {
            case 'general':
                $data = ($useTree) ? $this->selectNavsGeneralTree() : $this->selectNavsGeneral();
                break;

            case 'base':
                $rows = \Nav::rows($lang)->where("navtype_id", 1)->get();
                $data = array();
                foreach ($rows as $row) {
                    $data[$row->id] = $row->getPublicName();
                }
                break;

            case 'list':
                $rows = \Nav::rows($lang)->where("navtype_id", 2)->get();
                $data = array();
                foreach ($rows as $row) {
                    $data[$row->id] = $row->getPublicName();
                }
                break;

            case 'status':
                $rows = \Nav::rows($lang)->where("navtype_id", 3)->get();
                $data = array();
                foreach ($rows as $row) {
                    $data[$row->id] = $row->getPublicName();
                }
                break;

            case 'qty':
                $rows = \Nav::rows($lang)->where("navtype_id", 4)->get();
                $data = array();
                foreach ($rows as $row) {
                    $data[$row->id] = $row->getPublicName();
                }
                break;

            case 'pricerange':
                $rows = \Nav::rows($lang)->where("navtype_id", 5)->get();
                $data = array();
                foreach ($rows as $row) {
                    $data[$row->id] = $row->getPublicName();
                }
                break;

            case 'section':
                $data = self::selectSections(false);
                break;

            case 'page':
                $data = ($useTree) ? self::selectPagesTree() : self::selectPages();
                break;

            case 'currency':
                $data = self::selectCurrencies();
                break;

            case 'language':
                $data = self::selectLanguages();
                break;

            case 'category':
                $data = ($useTree) ? $this->selectCategoriesTree() : $this->selectCategories();
                break;

            case 'brand':
                $data = self::selectBrands(false);
                break;

            case 'collection':
                $data = ($useTree) ? $this->selectCollectionsTree() : $this->selectCollections(false);
                break;

            case 'trend':
                $data = self::selectTrends(false);
                break;

            case 'promo':
                $data = self::selectPromo(false);
                break;

            case 'product':
                $data = [];
                break;

            case 'modulePosition':
                $data = self::selectModulePositions(false);
                break;

            case 'other':
                $data = self::selectNavsOther();
                break;


            default: //assuming is a attribute
                $data = [];
                $attribute = \Attribute::where("code", $type)->first();
                $rows = \AttributeOption::rows($lang)->where("attribute_id", $attribute->id)->orderBy("name")->get();
                foreach ($rows as $row) {
                    $row = new \AttributeOptionPresenter($row);
                    $data[$row->id] = $row->completename;
                }
                break;
        }
        return $data;
    }


    function selectNavsSingleTarget($type, $id)
    {
        $lang = \Core::getLang();
        switch ($type) {

            case 'base':
                $obj = \Nav::getObj($id);
                $label = 'Base/Sito';
                $name = $obj->name;
                break;

            case 'list':
                $obj = \Nav::getObj($id);
                $label = 'Elem. di raccordo';
                $name = $obj->name;
                break;

            case 'status':
                $obj = \Nav::getObj($id);
                $label = 'Status prodotto';
                $name = $obj->name;
                break;

            case 'qty':
                $obj = \Nav::getObj($id);
                $label = 'Q.tà prodotto';
                $name = $obj->name;
                break;

            case 'pricerange':
                $obj = \Nav::getObj($id);
                $label = 'Fascia di prezzo';
                $name = $obj->name;
                break;

            case 'section':
                $obj = \Section::getObj($id);
                $label = 'Sezione';
                $name = $obj->name;
                break;

            case 'page':
                $obj = \Page::getObj($id);
                $label = 'Pagina';
                $name = $obj->name;
                break;

            case 'category':
                $obj = \Category::getObj($id);
                $label = 'Categoria';
                $name = $obj->name;
                break;

            case 'brand':
                $obj = \Brand::getObj($id);
                $label = 'Brand';
                $name = $obj->name;
                break;

            case 'collection':
                $obj = \Collection::getObj($id);
                $label = 'Collezione';
                $name = $obj->name;
                break;

            case 'trend':
                $obj = \Trend::getObj($id);
                $label = 'Gruppo pers.';
                $name = $obj->name;
                break;

            case 'product':
                $obj = \Product::getObj($id);
                $label = 'Prodotto';
                $name = $obj->name;
                break;

            default: //assuming is a attribute
                $data = [];
                $option = \AttributeOption::getObj($id);
                $attribute = \Attribute::getObj($option->attribute_id);
                $label = $attribute->name;
                $name = $option->name;
                break;
        }
        return compact('label', 'name');
    }


    function __selectNavs($withModifiers = false)
    {
        $data = [];
        $lang = \Core::getLang();

        if ($withModifiers == false) {
            $rows = \Nav::rows($lang)->where("navtype_id", 1)->get();
            $submenu = [];
            foreach ($rows as $row) {
                $submenu["base|" . $row->id] = $row->name;
            }
            $submenu['url'] = 'URL custom';
            $submenu['separator'] = 'Separatore';
            $data['Base'] = $submenu;

            $submenu = [];
            $rows = \Nav::rows($lang)->where("navtype_id", 2)->get();
            foreach ($rows as $row) {
                $submenu["list|" . $row->id] = $row->name;
            }
            $data['Entità di raccordo'] = $submenu;
        }

        $submenu = ['section' => 'Sezione', 'page' => 'Pagina'];
        $data['Contenuti'] = $submenu;

        $submenu = ['category' => 'Categoria', 'brand' => 'Brand', 'collection' => 'Collezione', 'product' => 'Prodotto singolo', 'trend' => 'Gruppo personalizzato'];
        $data['Prodotti'] = $submenu;

        $submenu = [];
        $rows = \Attributes::rows($lang)->where("is_nav", 1)->orderBy("name")->get();
        foreach ($rows as $row) {
            $submenu["attr|" . $row->id] = $row->name;
        }
        $data['Attributi principali'] = $submenu;

        $submenu = [];
        $rows = \Nav::rows($lang)->where("navtype_id", 3)->get();
        foreach ($rows as $row) {
            $submenu["status|" . $row->id] = $row->name;
        }
        $data['Status prodotto'] = $submenu;

        $submenu = [];
        $rows = \Nav::rows($lang)->where("navtype_id", 4)->get();
        foreach ($rows as $row) {
            $submenu["qty|" . $row->id] = $row->name;
        }
        $data['Status quantità prodotto'] = $submenu;

    }

    function selectMailCryptProtocols()
    {
        return $this->mailCryptProtocols();
    }

    function selectMailModes()
    {
        return $this->mailModes();
    }

    function selectProductTypes()
    {
        return $this->productTypes();
    }

    function selectProductVisibility()
    {
        return $this->productVisibility();
    }

    function selectProductStatus()
    {
        return $this->productStatus();
    }

    function selectGeoBeahviorRestricted()
    {
        return $this->geoBeahviorRestricted();
    }

    function selectGeoBeahviorUnrecognized()
    {
        return $this->geoBeahviorUnrecognized();
    }

    function selectRobots($global = false)
    {
        $arr = $this->robots();
        if ($global) {
            $arr = array_merge(array('global' => 'Usa impostazioni globali'), $arr);
        }
        return $arr;
    }

    function selectContactReason()
    {
        $lang = \Core::getLang();
        $rows = DB::table('message_reasons')->where('lang_id', $lang)->orderBy('position')->get();
        $data = ["" => ""];
        foreach ($rows as $row) {
            $data[$row->code] = $row->name;
        }

        return $data;
    }


    function selectOrdersByCustomer($customer_id = null)
    {
        $data = ['' => 'Nessun ordine'];
        if ($customer_id > 0) {
            $customer = \Customer::getObj($customer_id);
            return $customer_id->selectOrders('Nessun ordine');
        }
        return $data;
    }

    function selectFrequencies()
    {
        return $this->frequencies();
    }

    function selectCombinationTypes()
    {
        return $this->combinationTypes();
    }

    function selectFrequenciesMin()
    {
        return $this->frequenciesMin();
    }

    function selectMailSenders()
    {
        return $this->mailSenders();
    }

    function selectWatermarkPositions()
    {
        return $this->watermarkPosition();
    }

    function selectSeoXTypes()
    {
        return $this->seoXTypes();
    }

    function selectSeoContexts()
    {
        return $this->seoContexts();
    }

    function selectCurrencies()
    {
        $rows = $this->currencies();
        return \EchoArray::set($rows)->keyIndex('id', 'name')->get();
    }

    function selectCategories()
    {
        $rows = $this->categories();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        $data = array(0 => 'Categoria di primo livello') + $data;
        return $data;
    }

    function selectCategoriesTree($withBlank = false)
    {
        $data = $this->categoriesTree(0);
        if ($withBlank) $data = ["0" => "Scegli..."] + $data;
        return $data;
    }

    function selectCollectionsTree($withBlank = false)
    {
        $data = $this->collectionsTree();
        if ($withBlank) $data = ["0" => "Scegli..."] + $data;
        return $data;
    }

    function selectPagesTree($withBlank = false)
    {
        $data = $this->pagesTree();
        if ($withBlank) $data = ["0" => "Scegli..."] + $data;
        return $data;
    }

    function selectStatesTree($withBlank = false)
    {
        $data = $this->statesTree();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectCarriers($withBlank = false)
    {
        $rows = $this->carriers();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectMessageTemplates($withBlank = false)
    {
        $rows = $this->messageTemplates();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectGenders($withBlank = false)
    {
        $rows = $this->genders();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectCampaigns($withBlank = false)
    {
        $rows = $this->campaigns();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectStockReasons($withBlank = false)
    {
        $rows = $this->stock_reasons();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectPaymentStates($withBlank = false, $acl = false)
    {
        $rows = $this->payment_states($acl);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectOrderStates($withBlank = false, $acl = false)
    {
        $rows = $this->order_states($acl);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectRmaStates($withBlank = false)
    {
        $rows = $this->rma_states();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectEmailsOptions($withBlank = false)
    {
        $rows = $this->emails();
        $data = [];
        if ($withBlank) $data = ["" => "Scegli..."];
        foreach ($rows as $row) {
            $data[$row->id] = "$row->code - $row->name";
        }
        return $data;
    }

    function selectPeoples($withBlank = false)
    {
        $rows = $this->peoples();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectPayments($withBlank = false)
    {
        $rows = $this->payments();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectAttributesMulti($exclude_id = 0)
    {
        $rows = $this->attributes_multi($exclude_id);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        $data = array(0 => 'Seleziona un attributo') + $data;
        return $data;
    }

    function selectAttributeOptions($attribute_id)
    {
        $lang = \Core::getLang();
        $rows = \AttributeOption::where("attribute_id", $attribute_id)->rows($lang)->orderBy("name")->orderBy("uname")->get();
        $data = [];
        foreach ($rows as $row) {
            $row = new \AttributeOptionPresenter($row);
            $data[$row->id] = $row->completename;
        }

        //$data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    function selectBrands($withBlank = true)
    {
        $rows = $this->brands();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) {
            //$data = array('' => '') + $data;
            $data = array('0' => 'Scegli...') + $data;
        }
        return $data;
    }


    function selectTrends($withBlank = true)
    {
        $rows = $this->trends();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) {
            $data = array('0' => 'Scegli...') + $data;
        }
        return $data;
    }

    function selectPromo($withBlank = true)
    {
        $rows = $this->promo();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) {
            $data = array('0' => 'Scegli...') + $data;
        }
        return $data;
    }

    function selectCollections($withBlank = true, $brand_id = 0)
    {
        $rows = $this->collections($brand_id);

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) {
            //$data = array('' => '') + $data;
            $data = array('0' => 'Scegli...') + $data;
        }
        return $data;
    }

    function selectPublishOptions()
    {
        return $this->publishOptions();
    }

    function selectOverrideOptions()
    {
        return $this->overrideOptions();
    }

    function selectOgpOptions()
    {
        return $this->ogpOptions();
    }

    function selectPublicAcl()
    {
        return $this->publicAcl();
    }

    function selectFieldTypes()
    {
        return $this->fieldTypes();
    }

    function selectActiveOptions()
    {
        return $this->activeOptions();
    }

    function selectYesNo()
    {
        return $this->yesNo();
    }

    function selectEnabledDisabled()
    {
        return $this->enableDisabled();
    }

    function selectValidatorClasses()
    {
        return $this->validatorClasses();
    }

    function selectBackOrders($default = true)
    {
        return $this->backOrders($default);
    }

    function selectNotifyStocks($default = true)
    {
        return $this->notifyStocks($default);
    }

    function selectQtyIncrements($default = true)
    {
        return $this->qtyIncrements($default);
    }

    function selectAttributesSets($exclude_id, $withBlank = true)
    {
        $data = $this->attributes_sets($exclude_id);
        if ($withBlank)
            $data = array(0 => 'Non eredita da nessun Set esistente') + $data;
        return $data;
    }

    function selectTaxGroups()
    {
        $data = $this->taxGroups();
        $data = \EchoArray::set($data)->keyIndex('id', 'name')->get();
        $data = array(0 => 'Nessuna tassa') + $data;
        return $data;
    }

    function selectTaxes()
    {
        $data = $this->taxes();
        $data = \EchoArray::set($data)->keyIndex('id', 'name')->get();
        $data = array(0 => 'Nessuna tassa') + $data;
        return $data;
    }

    function selectTaxesRules()
    {
        $data = array(1 => 'Tasse incluse', 0 => 'Tasse escluse');
        return $data;
    }

    function selectModuleTags()
    {
        $data = array(
            'div' => 'DIV',
            'span' => 'SPAN',
            'p' => 'P',
            'aside' => 'ASIDE',
            'section' => 'SECTION',
        );
        return $data;
    }

    function selectModuleTitleTags()
    {
        $data = array(
            'div' => 'DIV',
            'span' => 'SPAN',
            'h1' => 'H1',
            'h2' => 'H2',
            'h3' => 'H3',
            'h4' => 'H4',
            'h5' => 'H5',
            'h6' => 'H6',
            'p' => 'P',
        );
        return $data;
    }

    function selectResponsiveValues($mode)
    {
        $data = ["" => "Nessuna regola"];
        for ($i = 1; $i <= 12; $i++) {
            $data["col-$mode-$i"] = "$i/12 larghezza colonna";
        }
        $data["$mode-hidden"] = "Non visualizzare la colonna";

        return $data;
    }

    function selectResponsiveValuesMin($mode)
    {
        $data = ["" => "Nessuno"];
        for ($i = 1; $i <= 12; $i++) {
            $data[$i] = "$i/12";
        }
        $data["hidden"] = "Nascosto";

        return $data;
    }

    function selectShippingRules()
    {
        $data = array(0 => 'Spedizione esclusa', 1 => 'Spedizione inclusa');
        return $data;
    }

    function selectCartRuleTypes()
    {
        $data = array('none' => 'Nessuna', 'discount' => 'Sconto percentuale/fisso', 'special_offer' => 'Offerta speciale');
        return $data;
    }

    function selectSpecialOfferTargets()
    {
        $data = array('same' => 'Gli stessi prodotti che soddisfano le condizioni logiche scelte sopra', 'new' => 'Altri prodotti in base a nuove condizioni logiche');
        return $data;
    }


    function selectReductionTypes()
    {
        $data = array('percent' => 'Importo percentuale', 'fixed' => 'Importo fisso');
        return $data;
    }

    function selectPriceReductionTypes()
    {
        $data = array('percent' => 'Riduzione per Importo percentuale', 'amount' => 'Riduzione per Importo fisso', 'fixed_amount' => 'Impostazione a Importo fisso', 'fixed_percent' => 'Impostazione a Importo percentuale');
        //$data = array(1 => 'Sconto per quantità', 2 => 'Offerta speciale');
        return $data;
    }

    function selectReductionTargets()
    {
        $data = array('cart' => 'Totale ordine (spedizione esclusa)', 'price' => 'Prezzo prodotti (scelta multipla)');
        return $data;
    }

    function selectPriceReductionTargets()
    {
        $data = array(1 => 'Prezzo di vendita finale (tasse incluse)', 2 => 'Prezzo di listino (tasse incluse)', 3 => 'Prezzo di acquisto (tasse escluse)', 4 => 'Prezzo di vendita (tasse escluse)');
        return $data;
    }

    function selectCampaignNetworks()
    {
        $data = array('' => 'Scegli...', 'zanox' => 'Zanox',);
        return $data;
    }

    function selectSections($useFirstLevel = true, $exclude_id = 0)
    {
        $rows = $this->sections($exclude_id);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($useFirstLevel) {
            $data = array(0 => "Sezione di primo livello") + $data;
        }
        return $data;
    }

    function selectPages()
    {
        $rows = $this->pages();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    function selectSectionsForPages($useFirstLevel = true)
    {
        $rows = $this->sections();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($useFirstLevel) {
            $data = array(0 => "Nessuna sezione") + $data;
        }
        return $data;
    }

    function selectProducts()
    {
        $data = $this->products();
        $data = \EchoArray::set($data)->keyIndex('id', 'name')->get();
        return $data;
    }

    function selectSmartProducts($ids)
    {
        if ($ids == null)
            return array();
        $data = $this->products($ids);
        $data = \EchoArray::set($data)->keyIndex('id', 'name')->get();
        return $data;
    }

    function selectTags()
    {
        $rows = $this->tags();
        $data = \EchoArray::set($rows)->keyIndex('id', 'tag')->get();
        return $data;
    }

    function optionsAttributesBySets($set_id)
    {
        $rows = $this->attributes_by_set($set_id);
        $html = '';
        foreach ($rows as $row) {
            $html .= '<option value="' . $row->id . '" selected>' . $row->name . ' (' . $row->frontend_input . ')</option>';
        }
        return $html;
    }

    function optionsAttributesAdded($set_id, $parent_id, $old = null)
    {
        //\Utils::log($old, "OLD VALUE");
        $rows = $this->attributes_added_by_set($set_id, $old);
        $toadd = $this->attributes_toadd_by_set($set_id, $parent_id);
        $html = '';
        $selected_ids = array();
        foreach ($rows as $row) {
            $html .= '<option value="' . $row->id . '" selected>' . $row->name . ' (' . $row->frontend_input . ')</option>';
            $selected_ids[] = $row->id;
        }
        foreach ($toadd as $row) {
            if (!in_array($row->id, $selected_ids)) {
                $html .= '<option value="' . $row->id . '">' . $row->name . ' (' . $row->frontend_input . ')</option>';
            }
        }
        return $html;
    }

    function optionsAttributesRemoved($set_id, $parent_id, $old = null)
    {
        $rows = $this->attributes_removed_by_set($set_id, $old);
        $toremove = $this->attributes_by_set($parent_id);
        $html = '';
        $selected_ids = array();
        foreach ($rows as $row) {
            $html .= '<option value="' . $row->id . '" selected>' . $row->name . ' (' . $row->frontend_input . ')</option>';
            $selected_ids[] = $row->id;
        }
        foreach ($toremove as $row) {
            if (!in_array($row->id, $selected_ids)) {
                $html .= '<option value="' . $row->id . '">' . $row->name . ' (' . $row->frontend_input . ')</option>';
            }
        }
        return $html;
    }

    function categoriesFlatten(&$ids, $categories_ids)
    {

        if (!is_array($categories_ids)) {
            $categories_ids = explode(",", $categories_ids);
        }

        $rows = DB::table("categories")->whereIn("id", $categories_ids)->select("parent_id", "id")->get();

        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $ids[] = $row->id;
                if ($row->parent_id > 0) {
                    $ids = $ids + $this->categoriesFlatten($ids, $row->parent_id);
                }
            }
        }
        return $ids;
    }

    function selectDefaultCategories($ids)
    {
        $rows = $this->categoriesIn($ids);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return array('' => 'Seleziona una categoria') + $data;
    }

    function selectCombinations()
    {
        $rows = $this->combinations();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return array('0' => 'Tutte le Varianti') + $data;
    }

    function selectCombinationsProducts($product_id)
    {
        $rows = $this->combinations($product_id);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return array('0' => 'Tutte le Varianti') + $data;
    }

    function selectCustomers($shop_id = 1, $withBlank = true, $blankText = 'Tutti i Clienti')
    {
        $data = [];
        $rows = $this->customers($shop_id);
        foreach ($rows as $row) {
            $data[$row->id] = $row->getName() . " - ($row->email)";
        }
        if ($withBlank) {
            $data = ['' => $blankText] + $data;
        }
        return $data;
    }


    function selectCustomersForRule($shop_id = 1, $withBlank = true, $blankText = 'Tutti i Clienti')
    {
        $data = [];
        $rows = $this->customers($shop_id);
        foreach ($rows as $row) {
            $data[$row->id] = $row->getName() . " - ($row->email)";
        }
        if ($withBlank) {
            $data = ['0' => $blankText] + $data;
        }
        return $data;
    }

    function selectCustomersGroups($shop_id = 1)
    {
        $rows = $this->customersGroups($shop_id);
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return array('0' => 'Tutti i Gruppi') + $data;
    }

    function selectCountriesConditions()
    {
        $rows = $this->countries();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return array('0' => 'Tutte le Nazioni') + $data;
    }

    function selectCurrenciesConditions()
    {
        $rows = $this->currencies();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return array('0' => 'Tutte le Valute') + $data;
    }

    function selectMagicTokensTemplates()
    {
        $rows = $this->magicTokensTemplates();
        $data = [];
        $languages = $this->languagesCodes();
        foreach ($rows as $row) {
            $obj = new \stdClass();
            $obj->name = $row->name;
            $obj->templates = [];
            foreach ($languages as $lang) {
                $tpl = new \stdClass();
                $tpl->lang = $lang;
                $tpl->content = $row->{$lang}->content;
                $token = new Token($tpl->content);
                $token->setLang($lang);
                $token->setSandboxMode(TRUE);
                $example = $token->render();
                $tpl->sample = $example;
                $obj->templates[] = $tpl;
            }
            $data[$row->id] = \Json::encode($obj);
        }
        return array(0 => 'Non usare un template') + $data;
    }

    function selectRulesOperators($include = null, $exclude = null)
    {
        $conditions = [
            '==' => 'è uguale a',
            '!=' => 'non è uguale a',
            '>=' => 'è uguale o maggiore di',
            '<=' => 'è uguale o minore di',
            '>' => 'è maggiore di',
            '<' => 'è minore di',
            '{}' => 'contiene',
            '!{}' => 'non contiene',
            '()' => 'è almeno uno tra',
            '!()' => 'non è almeno uno tra',
            '+=' => 'qualsiasi - deve esistere almeno una occorrenza',
            '-=' => 'nessuna - non deve esistere alcuna occorrenza',
        ];
        $temp = $conditions;
        if (is_array($include)) {
            $temp = [];
            foreach ($include as $symbol) {
                $temp[$symbol] = $conditions[$symbol];
            }
        }
        if (is_array($exclude)) {
            foreach ($exclude as $symbol) {
                unset($temp[$symbol]);
            }
        }
        return $temp;
    }

    function selectRange($min, $max)
    {
        $data = [];
        for ($i = $min; $i <= $max; $i++) {
            $data[$i] = $i;
        }
        return $data;
    }


    function selectImageTypes()
    {
        $rows = DB::table('images_type')->get();
        foreach ($rows as $row) {
            $row->label = $row->name . " ($row->width X $row->height)";
        }
        $data = \EchoArray::set($rows)->keyIndex('name', 'label')->get();
        return $data;
    }

    function selectPaginationTypes()
    {
        return ['static' => 'Paginazione statica', 'ajax' => 'Paginazione AJAX'];
    }

    function selectFrontendScopes()
    {
        return [
            'home' => 'Homepage',
            'product' => 'Pagina Prodotto',
            'catalog' => 'Pagina Catalogo',
            'page' => 'Pagina',
            'section' => 'Sezione',
            'search' => 'Risultati ricerca',
        ];
    }

    function selectOrderModes($isProduct = false)
    {
        $data = [
            'online' => 'ONLINE',
            'shop' => 'SHOP',
            'local' => 'LOCAL',
            'mixed' => 'MIXED',
            'master' => 'MASTER',
            'offline' => 'OFFLINE',
            '*' => 'SKIP',
        ];
        if ($isProduct)
            unset($data['mixed']);
        else
            unset($data['*']);

        return $data;
    }


    function selectShops()
    {
        return \MorellatoShop::codes();
    }

    function selectShopsById()
    {
        return \MorellatoShop::options();
    }

    function selectMasterOrders()
    {
        $data = ['' => 'Seleziona...'];
        $start_id = \Config::get('soap.order.startId');
        $rows = \Order::whereNull('parent_id')->where('id', '>', $start_id)->where('availability_mode', 'master')->orderBy('id', 'desc')->get();
        foreach ($rows as $row) {
            $customer = $row->getCustomer();
            $data[$row->id] = $row->reference . (($customer) ? ' - ' . $customer->name : null);
        }
        return $data;
    }


    function selectWarehouses()
    {
        return [
            '' => 'Seleziona...',
            '01' => '01',
            '03' => '03',
        ];
    }

    function selectStatesCodes($country_id)
    {
        $rows = $this->states($country_id);
        return \EchoArray::set($rows)->keyIndex('iso_code', 'id')->get();
    }

    function affiliates()
    {
        return Affiliate::where("active", 1)->orderBy('name')->get();
    }

    function selectAffiliates($withBlank = false)
    {
        $rows = $this->affiliates();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    function selectOrderFeeTypes($withBlank = false)
    {
        $data = [\Order::INVOICE_TYPE_RECEIPT => 'Ricevuta', \Order::INVOICE_TYPE_STARLING => 'Storno'];
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }
}

