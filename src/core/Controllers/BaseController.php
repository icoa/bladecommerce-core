<?php



class BaseController extends Controller {

    protected $fire_plugins = true;
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	function __construct(){
        $checkLang = true;
		if($checkLang){
			$currency_id = \Input::get('currency_id');
			$lang_id = \Input::get('lang_id');
			if(isset($currency_id) AND $currency_id > 0){
				\FrontTpl::setCurrency($currency_id);
			}
			if(isset($lang_id) AND $lang_id != ''){
				\FrontTpl::setLang($lang_id);
			}
		}
        $this->profileQuery();
        if (Request::ajax()) {
            $this->fire_plugins = false;
        }
		$this->discoverPlugins();
	}

	protected function profileQuery(){
        $profile_query = (int)\Input::get('blade_profile_query', 0);
        if(1 === $profile_query){
            \Config::set('app.writelogs', true);
            audit_watch();
        }
    }

	protected function discoverPlugins(){
	    if($this->fire_plugins === false)
	        return;

		$check = array_keys(config('plugins'));
		//Getting plugins from config files
		foreach($check as $class){
			if($class != 'alias' and config('plugins.'.$class, false)){
				$plugin = config("plugins.alias.$class");
                try{
                    $istance = new $plugin($this);
                    $istance->registerEvents();
                }catch (Exception $e){
                    audit_error($plugin. ' error', __METHOD__);
                    audit_error($e->getTraceAsString(), __METHOD__);
                }

			}
		}
	}

}