<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 09/12/2014
 * Time: 15:27
 */

class CronController extends BaseController {

    /*
     * SUGGESTED CRON TABLE
     *
     * /currency once-a-day at midnight
     * /sitemap once-a-day at midnight
     * /states-check twice-a-day 12:00 and 24:00
     *
     * */

    function getLevel($mode){
        ini_set('max_execution_time',600);
        echo '<pre>';
        switch($mode){
            case 'standard':
                $this->getCurrency();
                $this->getStatesCheck();
                $this->removeDeletedProducts();
                break;
            case 'middle':
                $this->getStatesCheck();
                break;
            case 'cache':
                $this->handleCache();
                $this->handleCacheAdvanced();
                break;
            case 'products':
                $this->handleCacheProducts();
                break;
            case 'batch2':
                $languages = \Core::getLanguages();
                foreach($languages as $lang){
                    $this->getSitemap($lang,'Products');
                }
                break;
            case 'batch':
                $languages = \Core::getLanguages();
                foreach($languages as $lang){
                    $this->getSitemap($lang,'Boilerplates');
                    $this->getSitemap($lang,'Categories');
                }
                break;

        }
    }

    function getGooglemerchant($lang,$locale){
        $builder = new services\GoogleMerchantBuilder($lang,$locale);
        $builder->batch();
    }

    function getCurrency(){
        echo "Executing -> ".__METHOD__;
        $defaultCurrency = 'EUR';
        $this->updateFromYahoo($defaultCurrency);
        \Currency::initCache();
    }

    function getSitemap($lang, $type = null){
        echo "Executing -> ".__METHOD__;
        $languages = \Core::getLanguages();
        if(in_array($lang,$languages)){
            $sitemap = new SitemapBuilder($lang);
            $sitemap->buildSitemap( $type );
        }
    }

    function handleCache(){
        \Category::initCache();
        \Brand::initCache();
        \Collection::initCache();
        \Trend::initCache();
        \Nav::initCache();
        \Attribute::initCache();
        \AttributeOption::initCache();
        \Section::initCache();
        \Page::initCache();
        \PriceRule::initCache();
        \Lexicon::initCache();
        \Helper::uncache();
    }

    function handleCacheAdvanced(){
        $rows = \Category::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Brand::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Collection::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Trend::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Nav::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Attribute::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \AttributeOption::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Section::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \Page::get();
        foreach($rows as $row){
            $row->uncache();
        }
        $rows = \PriceRule::get();
        foreach($rows as $row){
            $row->uncache();
        }
    }


    function handleCacheProducts(){
        $rows = \Product::where('is_out_of_production', 0)->get();
        foreach($rows as $row){
            //$row->uncache();
            \ProductHelper::buildProductCache($row->id, false);
        }
        \Product::initCache();
    }


    function getStatesCheck(){
        echo "Executing -> ".__METHOD__;
        \Utils::watch();
        //check pricerules/promotion
        $rows = PriceRule::get();
        $now = time();
        foreach($rows as $row){
            echo "Checking promo [$row->id]".PHP_EOL;
            $from = strtotime($row->date_from);
            $to = strtotime($row->date_to);
            //start but no finish
            if($from > 0 AND $now > $from AND $to == 0){
                if($row->active == 0){
                    echo "1 - This promo should be activated [$row->date_from]".PHP_EOL;
                    \ProductHelper::refreshPriceRule($row->id,1);
                }
            }
            if($to > 0 AND $now < $to AND $from == 0 ){
                if($row->active == 0){
                    echo "2 - This promo should be activated [$row->date_from]".PHP_EOL;
                    \ProductHelper::refreshPriceRule($row->id,1);
                }
            }
            if($to > 0 AND $now > $to AND $row->active == 1){
                echo "3 - This promo should be DE-activated [$row->date_to]".PHP_EOL;
                \ProductHelper::refreshPriceRule($row->id,0);
            }
            if($from > 0 AND $from > $now AND $row->active == 0){
                echo "4 - This promo should be DE-activated [$row->date_to]".PHP_EOL;
                \ProductHelper::refreshPriceRule($row->id,0);
            }
        }
        \ProductHelper::updateProductsStates();
    }



    private function updateFromYahoo( $defaultCurrency )
    {

        $data = array();
        // Get all currencies
        foreach(DB::table('currencies')->get() AS $currency)
        {
            $data[] = "{$defaultCurrency}{$currency->iso_code}=X";
        }
        // Ask Yahoo for exchange rate
        if( $data )
        {
            $url = 'http://download.finance.yahoo.com/d/quotes.csv?s=' . implode(',', $data) . '&f=sl1&e=.csv';
            echo $url;
            $content = $this->request($url);
            $lines = explode("\n", trim($content));
            // Update each rate
            foreach ($lines as $line)
            {
                $code = substr($line, 4, 3);
                $value = substr($line, 11, 6);
                if ($value)
                {
                    DB::table('currencies')
                        ->where('iso_code', $code)
                        ->update(array(
                            'conversion_rate' 		=> $value,
                            'updated_at'	=> new \DateTime('now'),
                        ));
                }
            }
            Cache::forget('exchange.currency');
        }

    }
    private function updateFromOpenExchangeRates($defaultCurrency, $api)
    {

        // Make request
        $content = json_decode( $this->request("http://openexchangerates.org/api/latest.json?base={$defaultCurrency}&app_id={$api}") );
        // Error getting content?
        if( isset($content->error) ) {
            $this->error($content->description);
            return;
        }
        // Parse timestamp for DB
        $timestamp = new \DateTime(strtotime($content->timestamp));
        // Update each rate
        foreach ($content->rates as $code=>$value)
        {
            $this->app['db']->table($this->table_name)
                ->where('code', $code)
                ->update(array(
                    'value' 		=> $value,
                    'updated_at'	=> $timestamp
                ));
        }
        Cache::forget('torann.currency');

    }
    private function request( $url )
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
        curl_setopt($ch, CURLOPT_MAXCONNECTS, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }
    private function removeDeletedProducts(){
        $query = "DELETE FROM cache_products WHERE id IN (SELECT id FROM products WHERE deleted_at IS NOT NULL)";
        DB::statement($query);
    }
}