<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-lug-2013 15.57.17
 */

use Carbon\Carbon;
use HtmlObject\Element;
use \Michelf\Markdown;
use Omnipay\Omnipay;
use services\BusinessDay;

class TestController extends BaseController
{

    function getIndex()
    {

    }


    function getEmailsend()
    {
        Config::set('mail.pretend', false);
        $email = Email::getByCode('CONTACT');
        $domain = config('services.mailgun.domain');
        $api = config('services.mailgun.secret');
        echo "Sending with:";
        echo "<p>Mailgun domain: $domain</p>";
        echo "<p>Mailgun secret: $api</p>";
        $email->sender_bcc = '';
        $data = ['details' => 'Test blade email'];
        $email->send($data, 'f.politi@m.icoa.it');
        echo 'DONE';
    }


    function getCurrency()
    {
        $value = 40.639;
        echo Format::convert($value, 2, 1);
        echo '<br>';
        $value = 50;
        echo Format::convert($value, 1, 2);
    }

    function getAffiliazionePosition()
    {
        $fileName = '500118A.jpg';
        $extension = 'jpg';
        $folder = '500118A';
        $position = null;
        $pathName = str_replace('.' . $extension, null, $fileName);
        if ($pathName == $folder) {
            $position = 1;
        } else {
            $tokens = explode('_', $pathName);
            $position = (int)end($tokens);
        }
        echo $position;
    }

    function getAffiliazione()
    {
        $rows = Product::inStocksForAffiliates()->select('id', 'sku')->get();
        echo '<pre>';
        echo print_r($rows->toArray(), 1);
    }


    function getError()
    {
        try {
            $c = 5 / 0;
        } catch (Exception $e) {
            audit_error($e->getMessage(), __METHOD__);
            audit_error($e->getTraceAsString(), __METHOD__);
            audit($e->getTraceAsString(), __METHOD__);
            audit_remote('Ciao Daniel, errore critico... MORIREMO TUTTI!!!' . date('d-m-Y H:i:s'), 'DEMO', $e);
        }
    }


    function getOrderCreate(){
        $cart = \CartManager::cart();
        if($cart){
            \OrderManager::create($cart->id, false);
            return "OrderManager::create($cart->id)";
        }
    }

}

