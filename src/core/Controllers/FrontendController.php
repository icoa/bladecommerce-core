<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 15.08.42
 */

class FrontendController extends BaseController
{

    protected $themeFolder = 'frontend';
    protected $theme;
    protected $view = '';
    protected $cookie = null;
    protected $segments = null;
    protected $scope_id = null;
    protected $asset_min = false;
    protected $asset_versioning = false;
    protected $redirect_url;
    /** @var Illuminate\Http\Response */
    protected $response;


    function __construct($repository = null)
    {
        $this->mobile();
        Session::set('theme', $this->themeFolder);

        $layout = 'default';
        if (Request::ajax()) {
            $layout = 'ajax';
        }

        $this->theme = Theme::theme($this->themeFolder)->layout($layout);
        \Config::set('theme::assetUrl', Theme::getConfig('assetUrl'));
        $this->asset_min = Theme::getConfig('assetUseMin');
        if (Input::get('assetUseMin') == 'false') {
            $this->asset_min = false;
        }
        $this->asset_versioning = Theme::getConfig('assetVersioning');
        \FrontTpl::setTheme($this->theme);
        \FrontTpl::setController($this);
        $this->_assets();
        $this->handleCampaign();
        $this->handleB2B();
        $this->handleCustomerGroup();
        parent::__construct();
    }


    function setSegments($segments)
    {
        $this->segments = $segments;
    }

    function setScopeId($scope_id)
    {
        $this->scope_id = $scope_id;
    }

    function getScopeId()
    {
        return $this->scope_id;
    }

    function route()
    {
        $verb = \Request::getMethod();
        $params = [];
        if (count($this->segments) == 0) {
            $method = 'index';
        } else {
            $method = $this->segments[0];
            $params = array_slice($this->segments, 1);
        }
        $method_name = strtolower($verb) . ucfirst($method);


        if (!method_exists($this, $method_name)) {
            $method_name = 'getIndex';
        }
        return call_user_func_array(array($this, $method_name), $params);
    }

    protected function setupLayout()
    {
        parent::setupLayout();
    }

    protected function setLayout($layout)
    {
        $this->theme->layout($layout);
    }

    function getTheme()
    {
        return $this->theme;
    }

    function hasView($view)
    {
        return \View::exists("theme.{$this->themeFolder}::views.$view");
    }

    function handleCampaign()
    {
        $cmpCode = (int)Input::get('cmpCode', Input::get('cmpcode', ''));
        if ($cmpCode > 0) {
            $campaign = \Campaign::getObj($cmpCode);
            if ($campaign !== null and $campaign->id > 0 and $campaign->active == 1) {
                Session::put('blade_campaign_id', $campaign->id);
                $cookie = \Cookie::make('blade_campaign_id', $campaign->id, 60 * 24 * 7);
                $this->cookie = $cookie;
            }
        }
    }

    function handleCustomerGroup()
    {
        if (!Input::has('grpCode') && !Input::has('grpcode')) {
            return;
        }
        $found = false;
        $grpCode = (int)Input::get('grpCode', Input::get('grpcode', ''));
        if ($grpCode > 0) {
            $group = CustomerGroup::getObj($grpCode);
            if ($group !== null and $group->id > 0) {
                Session::put('blade_customer_group_id', $group->id);
                $found = true;
            }
        }

        if (false === $found) {
            $grpCode = trim((string)Input::get('grpCode', Input::get('grpcode', '')));
            if ($grpCode !== '') {
                $group = CustomerGroup::findByCode(Str::upper($grpCode));
                if ($group !== null and $group->id > 0) {
                    Session::put('blade_customer_group_id', $group->id);
                    $found = true;
                }
            }
        }

        if (false === $found) {
            Session::forget('blade_customer_group_id');
        }
    }

    function handleB2B()
    {
        $b2b = (int)Input::get('b2b', 0);
        if ($b2b === 1) {
            Session::put('blade_b2b', 1);
        }
        $empty_cart = (int)Input::get('empty_cart', 0);
        if ($empty_cart === 1) {
            Session::forget('blade_auth');
            \CartManager::forgetCheckout(true);
            $this->redirect_url = \Link::to('nav', 13);
        }
    }

    function render($data, $statusCode = 200)
    {
        //\Utils::log(__METHOD__);
        \FrontTpl::bindData();
        $url = \FrontTpl::checkCountryRedirect();
        if ($url) {
            //\Utils::log("Redirect by country rules", __METHOD__);
            return \Redirect::to($url, 302);
        }
        if ($this->redirect_url) {
            return \Redirect::to($this->redirect_url, 302);
        }
        $response = $this->theme->scope($this->view, $data);
        if ($this->cookie != null) {
            $response->withCookie($this->cookie);
        }
        $this->response = $response->render($statusCode);
        $this->loadPlugins();
        $this->attachHeaders();
        return $this->response;
    }


    protected function toHead($asset, $depends = [], $attributes = [])
    {
        $key = Str::camel($asset);
        if (Str::startsWith($asset, ['http', '//'])) {
            $this->theme->asset()->add($key, $asset, $depends, $attributes);
        } else {
            $this->theme->asset()->usePath()->add($key, $asset, $depends, $attributes);
        }
    }

    protected function less($asset)
    {
        $path = \Theme::path();
        $assetTarget = str_replace(['css', '.less'], ['cache', '.css'], $asset);
        $file = $path . "/assets/" . $asset;
        if (file_exists($file)) {
            if (Config::get('app.debug')) {
                try {
                    touch($file);
                } catch (\Exception $e) {

                }
            }
            $less = new lessc;
            $target = $path . "/assets/" . $assetTarget;
            try {
                $less->checkedCompile($file, $target);
                return $this->toHead($assetTarget);
            } catch (\Exception $e) {
                \Utils::error($e->getMessage());
                \Utils::error($e->getTraceAsString());
            }
        }
    }

    public function toFooter($asset, $container = 'footer', $depends = [], $attributes = [])
    {
        $this->loadAsset($container, $asset, $depends, $attributes);
    }

    protected function toQueue($asset, $queueName = "echoBO")
    {
        $key = Str::camel($asset);
        $this->theme->asset()->queue($queueName)->usePath()->add($key, $asset);
    }

    private function loadAsset($container, $asset, $depends = [], $attributes = [])
    {
        $key = Str::camel($asset);
        if (Str::startsWith($asset, ['http', '//'])) {
            $this->theme->asset()->container($container)->add($key, $asset, $depends, $attributes);
        } else {
            $this->theme->asset()->container($container)->usePath()->add($key, $asset, $depends, $attributes);
        }
    }


    protected function loadPlugins()
    {
        $content = $this->response->getContent();
        $this->response->setContent(\Site::loadPlugins($content));
    }

    protected function _assets()
    {

        $version = $this->asset_versioning;

        if ($this->asset_min === true) {

            /*$browser = \FrontTpl::getBrowser();

            if ($browser) {
                if ($browser['browser_name'] == 'msie' AND (int)$browser['browser_number'] <= 9) {
                    //$this->toHead("css/bootstrap.min.css");
                    $this->toHead("css/ie1_blade_$version.css");
                    $this->toHead("css/ie2_blade_$version.css");
                    $this->toHead("css/ie3_blade_$version.css");
                    //$this->loadAsset("defer", "js/blade_$version.min.js");
                    $this->toFooter("js/blade_$version.min.js");
                    return;
                }
            }*/

            if (Config::get('mobile', false) === false) {
                $this->toHead("css/blade_$version.min.css");
            }
            $main_js_params = ['defer' => 'true'];
            $sri = \Utils::getMetaSri($this->themeFolder, 'js');
            if (is_string($sri)) {
                $main_js_params['integrity'] = $sri;
                $main_js_params['crossorigin'] = 'anonymous';
            }
            $this->toHead("js/blade_$version.min.js", [], $main_js_params);

            return;
        }

        $assets = $this->theme->getConfig('assets');

        $files = $assets['head']();
        if (isset($files)) {
            foreach ($files as $asset) {
                if (substr($asset, -4) == 'less') {
                    $this->less($asset);
                } else {
                    $this->toHead($asset);
                }
            }
        }

        $files = $assets['footer']();
        if ($files) {
            foreach ($files as $asset) {
                $this->toHead($asset, [], ['defer' => 'true']);
            }
        }

        //enforce general assets to be compiled or included in the frontend
        $enforced = config('assets.enforce', []);
        foreach ($enforced as $item) {
            if (file_exists(public_path($item)))
                $this->toHead(Site::rootify($item), [], ['defer' => 'true']);
        }
    }

    protected function mobile()
    {
        \Config::set('mobile', false);
        if (\Cfg::get('MOBILE_ACTIVE', 0) == 0) return;

        $isTablet = $isMobile = 'NO';
        $savedTheme = Input::get('_theme', Session::get('_theme'));

        if ($savedTheme != null and $savedTheme != '') {
            if ($savedTheme == 'desktop') {
                $savedTheme = 'frontend';
            }
            if ($savedTheme == 'mobile') {
                $device = Session::get('_device');
                $isTablet = ($device == 'tablet') ? 'YES' : 'NO';
                $isMobile = ($device == 'mobile') ? 'YES' : 'NO';
                if ($isTablet == 'NO' and $isMobile == 'NO') {
                    $isMobile = 'YES';
                }
            }
        } else {
            $isTablet = Frontend\Browser::isTablet();
            $isMobile = Frontend\Browser::isMobile();
            $savedTheme = 'frontend';
        }

        if ($isTablet == 'YES' or $isMobile == 'YES') {
            $class = 'dekstop';
            if ($isTablet) {
                $class = 'tablet';
            } elseif ($isMobile) {
                $class = 'mobile';
            }
            $savedTheme = 'mobile';
            \FrontTpl::addBodyClass($class);
            $this->themeFolder = $savedTheme;
            \Config::set('mobile', true);
            Session::set('_device', $class);
        } else {
            \Config::set('mobile', false);
        }
        Session::set('_theme', $savedTheme);
    }

    protected function attachHeaders()
    {
        /** skip under following conditions */
        if (Request::ajax())
            return;

        if (\Frontend\Browser::isBot()) {
            return;
        }

        if (\Frontend\Browser::is('YSlow') || \Frontend\Browser::is('PageSpeed')) {
            return;
        }

        $version = $this->asset_versioning;
        $storage_mode = 'session';

        // HTTP/2 PUSH RESOURCES
        if ($this->asset_min === true) {

            $theme = $this->theme->getThemeName();

            $flag_key = ($storage_mode === 'cookie')
                ? 'push_mtx_' . config('elasticquent.prefix') . '_' . \App::environment() . '_' . $theme
                : 'push_mtx_' . $theme;

            $skip_push = false;

            if ($storage_mode === 'cookie' && (string)Request::cookie($flag_key) === $version) {
                $skip_push = true;
            }
            if ($storage_mode === 'session' && (string)Request::session()->get($flag_key) === $version) {
                $skip_push = true;
            }
            $this->response->header('X-H2-Mode', $storage_mode)->header('X-H2-Push', $skip_push ? 'No' : 'Yes');

            if (false === $skip_push) {
                $asset_url = Theme::getConfig('assetUrl');

                $push_resources = [
                    ["/themes/$theme/assets/css/blade_$version.min.css", 'style'],
                    ["/themes/$theme/assets/js/blade_$version.min.js", 'script'],
                ];

                $links = [];

                foreach ($push_resources as $resource) {
                    list($link, $type) = $resource;

                    $sri = \Utils::getMetaSri($theme, $type);
                    $uri = ($asset_url === '/' ? Site::rootify(\Utils::http2LinkUrlToRelativePath($link), false) : $asset_url . substr($link, 1));

                    // we have a sri
                    if (is_string($sri)) {
                        $header = sprintf(
                            '<%s>; rel=preload; integrity=%s; crossorigin=anonymous; as=%s',
                            $uri,
                            $sri,
                            $type
                        );
                    } else {
                        $header = sprintf(
                            '<%s>; rel=preload; as=%s',
                            $uri,
                            $type
                        );
                    }

                    //header('Link: ' . $header, false);
                    $links[] = $header;
                }
                //audit(compact('asset_url', 'links'));
                $this->response->header('Link', implode(', ', $links), false);
                if ($storage_mode === 'cookie') {
                    $this->response->withCookie(\Cookie::make($flag_key, $version, 60 * 24 * 7));
                }
                if ($storage_mode === 'session') {
                    Session::put($flag_key, $version);
                }
            }

        }

        //$this->response->header('Cache-Control', 'no-store, no-cache, must-revalidate, max-age=0')->header('Pragma', 'cache');
    }
}

