<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 15.08.42
 */

class BackendController extends BaseController
{

    public $component_id = 0;
    public $themeFolder = 'admin';
    public $theme;
    public $title;
    public $pageheader;
    public $page;
    public $bodyClass = 'default';
    public $themeLayout = 'default';
    public $widgets = array();
    public $isPopup = false;
    public $model;
    protected $rules = array();
    protected $messages = array();
    protected $friendly_names = array();
    protected $errors = array();
    protected $actions = array();
    public $className;

    public $blockAllActivities = false;

    function __construct()
    {
        parent::__construct(false);
        define("USE_MIN", false);
        if (isset($_REQUEST['popup'])) {
            $this->themeLayout = 'popup';
            $this->isPopup = TRUE;
        }
        if ($this->blockAllActivities AND $this->themeLayout == 'default') {
            $this->themeLayout = 'blank';
        }
        $this->theme = Theme::theme($this->themeFolder)->layout($this->themeLayout);
        $this->_assets();
        $this->_widgets_default();

        if ($this->component_id > 0) {
            $this->component = $this->_getAdminComponent($this->component_id);
            if ($this->component->parent_id > 0) {
                $this->component_parent = $this->_getAdminComponent($this->component->parent_id);
                $this->addBreadcrumb($this->component_parent->name, '#');
            }
            $this->addBreadcrumb($this->component->name, $this->component->getUrl());
        }
        //\Utils::log($this->component->name,"COMPONENT NAME");

        $routeName = Route::currentRouteAction();
        if ($routeName) {
            $controller = explode("@", $routeName)[0];
            \AdminTpl::setScope($controller);
        }
    }

    function _getAdminComponent($id)
    {
        $obj = Cache::remember('admin_component_' . $id, 60, function () use ($id) {
            $obj = AdminComponent::find($id);
            return $obj;
        });
        return $obj;
    }

    function action($action, $urlify = false, $parameter = null)
    {
        $r = "{$this->className}@{$action}";
        if ($urlify) {
            $r = ($parameter === null) ? \URL::action($r) : \URL::action($r, $parameter);
        }
        return $r;
    }

    protected function addBreadcrumb($label, $url = null)
    {
        $this->theme->breadcrumb()->add($label, $url);
    }

    protected function _widgets_default()
    {
        $this->addWidget('WidgetNav');

        $this->theme->breadcrumb()->setTemplate('
    <ul class="breadcrumbs navigation">
    @foreach ($crumbs as $i => $crumb)
        @if ($i != (count($crumbs) - 1))
        <li><a href="{{ $crumb["url"] }}">{{ $crumb["label"] }}</a></li><span class="arrow-right"></span>
        @else
        <li class="active"><a>{{ $crumb["label"] }}</a></li>
        @endif
    @endforeach
    </ul>
');
    }

    protected function addWidget($widget)
    {
        $this->widgets[] = $widget;
    }

    protected function prependWidget($widget)
    {
        $temp = array();
        $temp[] = $widget;
        $this->widgets = array_merge($temp, $this->widgets);
    }

    protected function setupLayout()
    {
        parent::setupLayout();
    }

    protected function toHead($asset)
    {
        $key = Str::camel($asset);
        $this->theme->asset()->usePath()->add($key, $asset);
    }

    protected function toFooter($asset, $container = 'footer')
    {
        $this->loadAsset($container, $asset);
    }

    protected function toQueue($asset, $queueName = "echoBO")
    {
        //return $this->toFooter($asset);
        $key = Str::camel($asset);
        $this->theme->asset()->queue($queueName)->usePath()->add($key, $asset);
    }

    private function loadAsset($container, $asset)
    {
        $key = Str::camel($asset);
        if (Str::startsWith($asset, ['//', 'http'])) {
            $this->theme->asset()->container($container)->add($key, $asset);
        } else {
            $this->theme->asset()->container($container)->usePath()->add($key, $asset);
        }
    }

    protected function beforeRender()
    {
        if (USE_MIN === true) {
            $this->toFooter('js/blade.1.2.js');
        } else {
            $this->toFooter('js/functions/custom.js?v=3');
            $this->toFooter('js/echo/mainframe.js');
            $this->toFooter('js/echo/mainframe.shared.js?v=3');
        }


        $this->theme->partialComposer('flash', function ($view) {
            global $actions;
            $view['actions'] = $actions;
        });
    }


    protected function actions_create($params = [])
    {
        $index = \URL::action($this->action("getIndex"));

        $actions = array(
            new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata', 'update'),
            new AdminAction('Salva ed esci', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record', 'save'),
            new AdminAction('Torna indietro', $index, 'font-arrow-left', 'btn-default', 'Torna indietro alla pagina principale del Componente', 'back'),
        );

        if ($this->isPopup) {
            $actions = array(
                new AdminAction('Salva', "javascript:Echo.submitForm('reopen');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e riapre questa schermata'),
                new AdminAction('Chiudi', "javascript:window.close();", 'font-remove'),
            );
        }
        if (isset($params['only'])) {
            foreach ($actions as $key => $a) {
                if (!in_array($a->id, $params['only'])) {
                    unset($actions[$key]);
                }
            }
        }
        if (isset($params['exclude'])) {
            foreach ($actions as $key => $a) {
                if (in_array($a->id, $params['exclude'])) {
                    unset($actions[$key]);
                }
            }
        }
        return $actions;
    }

    protected function actions_edit($params = [])
    {
        $index = \URL::action($this->action("getIndex"));

        $actions = array(
            new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata', 'update'),
            new AdminAction('Salva ed esci', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record', 'save'),
            new AdminAction('Torna indietro', $index, 'font-arrow-left', 'btn-default', 'Torna indietro alla pagina principale del Componente', 'back'),
        );

        if ($this->isPopup) {
            $actions = array(
                new AdminAction('Salva', "javascript:Echo.submitForm('reopen');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e riapre questa schermata'),
                new AdminAction('Chiudi', "javascript:window.close();", 'font-remove'),
            );
        }
        if (isset($params['only'])) {
            foreach ($actions as $key => $a) {
                if (!in_array($a->id, $params['only'])) {
                    unset($actions[$key]);
                }
            }
        }
        if (isset($params['exclude'])) {
            foreach ($actions as $key => $a) {
                if (in_array($a->id, $params['exclude'])) {
                    unset($actions[$key]);
                }
            }
        }
        return $actions;
    }


    protected function actions_trash()
    {
        $index = $this->action("getIndex", TRUE);
        $restore = $this->action("postRestoreMulti", TRUE, 0);
        $destroy = $this->action("postDestroyMulti", TRUE);
        $actions = array(
            new AdminAction('Ripristina', $restore, 'font-undo', 'btn-success confirm-action-multi', 'Recupera i record selezionati'),
            new AdminAction('Elimina', $destroy, 'font-minus-sign', 'btn-danger confirm-action-multi', 'Elimina definitivamente i record selezionati'),
            new AdminAction('Indietro', $index, 'font-arrow-left'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
        );
        return $actions;
    }


    protected function actions_export()
    {
        $index = $this->action("getIndex", TRUE);
        $actions = array(
            new AdminAction('Indietro', $index, 'font-arrow-left'),
            new AdminAction('Scarica', "javascript:Echo.submitForm('submit');", 'font-download', 'btn-success', 'Conferma i parametri e crea il formato di esportazione'),
        );
        return $actions;
    }


    protected function actions_default($params = [])
    {
        $trash = ($this->action("getTrash", TRUE));
        $create = ($this->action("getCreate", TRUE));
        $flagMultiON = ($this->action("postFlagMulti", TRUE, 1));
        $flagMultiOff = ($this->action("postFlagMulti", TRUE, 0));
        $deleteMulti = ($this->action("postDeleteMulti", TRUE));
        $cloneMulti = ($this->action("postCloneMulti", TRUE));
        $actions = array(
            new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record', 'new'),
            new AdminAction('Abilita', $flagMultiON, 'font-ok', 'btn-warning action-multi', 'Abilita uno o più record selezionati', 'enable'),
            new AdminAction('Disabilita', $flagMultiOff, 'font-off', 'btn-warning action-multi', 'Disabilita uno o più record selezionati', 'disable'),
            new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati', 'remove'),
            new AdminAction('Clona', $cloneMulti, 'font-copy', 'btn-info confirm-action-multi', 'Clona uno o più record selezionati', 'clone'),
            new AdminAction('Cestino', $trash, 'font-trash', '', 'Mostra tutti gli elementi eliminati', 'trash'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente', 'reload'),
        );
        if (isset($params['only'])) {
            foreach ($actions as $key => $a) {
                if (!in_array($a->id, $params['only'])) {
                    unset($actions[$key]);
                }
            }
        }
        if (isset($params['exclude'])) {
            foreach ($actions as $key => $a) {
                if (in_array($a->id, $params['exclude'])) {
                    unset($actions[$key]);
                }
            }
        }
        return $actions;
    }


    protected function toolbar($what = 'default')
    {
        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $actions = $this->actions_create();
                break;

            case 'edit':
                $actions = $this->actions_edit();
                break;

            case 'trash':
                $actions = $this->actions_trash();
                break;

            case 'export':
                $actions = $this->actions_export();
                break;

            default:
                $actions = $this->actions_default();
                break;
        }
    }

    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);
        $clone = \URL::action($this->action("postClone"), $data['id']);

        $flag_label = 'Attiva';
        $flag_icon = 'icon-ok';
        $flag_status = 1;

        if (isset($data['active'])) {
            if ($data['active'] == '1') {
                $flag_label = 'Disattiva';
                $flag_icon = 'icon-off';
                $flag_status = 0;
            }
        }

        if (isset($data['published'])) {
            if ($data['published'] == '1') {
                $flag_label = 'Disattiva';
                $flag_icon = 'icon-off';
                $flag_status = 0;
            }
        }
        $status = \URL::action($this->action("postFlag"), array($data['id'], $flag_status));

        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="javascript:;" onclick="EchoTable.action('$status');" class="btn" title="$flag_label"><i class="$flag_icon"></i></a> </li>
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$clone');" class="btn" title="Clona"><i class="icon-share"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function column_position($data, $index, $obj)
    {
        $position = $data['position'];
        $ret = "<input type='text' class='iPosition' name='position[]' value='" . $position . "' />";
        $up = \URL::action($this->action("postUp"), $data['id']);
        $down = \URL::action($this->action("postDown"), $data['id']);

        $total = $obj->count_all - 1;

        $upArrowClass = '';
        $downArrowClass = '';
        if ($index == 0) {
            $upArrowClass = 'disabled';
            $up = 'null';
        }
        if ($index == $total) {
            $downArrowClass = 'disabled';
            $down = 'null';
        }

        $actions = <<<HTML
<div class="input-prepend position-controls">
<span class="add-on $upArrowClass">
    <a href="javascript:;" onclick="EchoTable.action('$up');" title="Sposta su"><i class="font-arrow-up"></i></a>
</span>
    $ret
<span class="add-on $downArrowClass">
    <a href="javascript:;" onclick="EchoTable.action('$down');" title="Sposta giù"><i class="font-arrow-down"></i></a>
</span>
</div>
HTML;
        return $actions;
    }

    protected function column_trash_actions($data)
    {
        $destroy = \URL::action($this->action("postDestroy"), $data['id']);
        $restore = \URL::action($this->action("postRestore"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="#" onclick="EchoTable.action('$restore');" class="btn" title="Ripristina"><i class="icon-share"></i></a> </li>
    <li><a href="#" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function checkbox($data, $key = '')
    {

        $v = (int)$data[$key];
        $a = \URL::action($this->action("postSwitchFlag"), array($data['id'], $key));

        $actions = <<<HTML
<img src="/media/admin/mark$v.gif" onclick="EchoTable.action('$a');" style="cursor:pointer;" />
HTML;
        return $actions;
    }

    protected function boolean($data, $key = '')
    {

        if (is_numeric($data[$key])) {
            $v = (int)$data[$key];
        } else {
            $v = (int)($data[$key] != "");
        }

        $actions = <<<HTML
<img src="/media/admin/markbool$v.gif">
HTML;
        return $actions;
    }

    public function postFlag($id, $status)
    {
        $model = $this->model;
        $obj = $model::find($id);

        if (isset($obj->active)) {
            $obj->update(array("active" => $status));
        } else {
            $obj->update(array('published' => $status));
        }
        try {
            $obj->uncache();
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), __METHOD__);
        }

        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

    public function postFlagMulti($status)
    {
        $ids = $_POST['ids'];
        $model = $this->model;
        $rows = $model::whereIn('id', $ids)->get();
        foreach ($rows as $obj) {
            if (isset($obj->active)) {
                $obj->update(array("active" => $status));
            } else {
                $obj->update(array('published' => $status));
            }
            try {
                $obj->uncache();
            } catch (\Exception $e) {
                \Utils::log($e->getMessage(), __METHOD__);
            }
        }
        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

    public function postDelete($id)
    {
        \Utils::log("DELETED ID: $id");
        $model = $this->model;
        \Utils::log("MODEL: $model");
        $obj = $model::find($id);
        \Utils::log("Calling canDelete");
        $deleteMsg = $obj->canDelete();
        if ($deleteMsg === true) {
            $obj->delete();
            $data = array('success' => true, 'msg' => 'Elemento spostato nel cestino con successo');
        } else {
            $data = array('success' => false, 'error' => $deleteMsg);
        }

        return Json::encode($data);
    }

    public function postDeleteMulti()
    {
        $ids = $_POST['ids'];
        $model = $this->model;
        $rows = $model::whereIn('id', $ids)->get();
        foreach ($rows as $obj) {
            $deleteMsg = $obj->canDelete();
            if ($deleteMsg === true) {
                $obj->delete();
                $data = array('success' => true, 'msg' => 'Elementi spostati nel cestino con successo');
            } else {
                $data = array('success' => false, 'error' => $deleteMsg);
            }
        }
        return Json::encode($data);
    }

    public function postDestroy($id)
    {
        //\Utils::log("DESTROY ID: $id");
        $model = $this->model;
        try {
            $obj = $model::withTrashed()->find($id);
            //$obj->translations()->forceDelete();
            $obj->forceDelete();
        } catch (\Exception $e) {
            $obj = $model::find($id);
            //$obj->translations()->forceDelete();
            $obj->forceDelete();
        }


        $data = array('success' => true, 'msg' => 'Elemento cancellato con successo');
        return Json::encode($data);
    }

    public function postDestroyMulti()
    {
        $ids = $_POST['ids'];
        $model = $this->model;
        $rows = $model::withTrashed()->whereIn('id', $ids)->get();
        foreach ($rows as $row) {
            //$row->translations()->forceDelete();
            $row->forceDelete();
        }

        $data = array('success' => true, 'msg' => 'Elementi cancellati con successo');
        return Json::encode($data);
    }

    public function postCloneMulti()
    {
        $ids = $_POST['ids'];
        $model = $this->model;
        $rows = $model::withTrashed()->whereIn('id', $ids)->get();
        foreach ($rows as $row) {
            $row->cloneMe();
        }
        $data = array('success' => true, 'msg' => 'Elementi clonati con successo');
        return Json::encode($data);
    }

    public function postRestore($id)
    {
        //\Utils::log("RESTORE ID: $id");
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->restore();

        $data = array('success' => true, 'msg' => 'Elemento ripristinato con successo');
        return Json::encode($data);
    }

    public function postRestoreMulti()
    {
        $ids = $_POST['ids'];
        $model = $this->model;
        $obj = $model::withTrashed()->whereIn('id', $ids);
        $obj->restore();

        $data = array('success' => true, 'msg' => 'Elementi ripristinati con successo');
        return Json::encode($data);
    }

    public function postUp($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->moveUp();
        $data = array('success' => true, 'msg' => 'Ordine elemento aggiornato con successo');
        return Json::encode($data);
    }

    public function postDown($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->moveDown();
        $data = array('success' => true, 'msg' => 'Ordine elemento aggiornato con successo');
        return Json::encode($data);
    }

    public function __postClone($id)
    {
        $model = $this->model;
        $source = $model::withTrashed()->find($id);
        $tr = $source->translations;
        $lang_model = $source->getLangModel();
        $obj = $source->replicate();

        //$relations = $source->getRelations();
        //\Utils::log($relations,"RELATIONS");


        $this->_before_clone($obj);
        $obj->save();
        foreach ($tr as $langObj) {
            \Utils::log($langObj->toArray(), "LANG OBJ");
            \Utils::log($lang_model, "LANG MODEL");
            $data = $langObj->toArray();

            $trModel = new $lang_model($data);
            $obj->translations()->save($trModel);
        }
        $this->_after_clone($obj);
        $data = array('success' => true, 'msg' => 'Elemento clonato con successo');
        return Json::encode($data);
    }

    public function getJson($id)
    {
        $model = $this->model;
        $obj = $model::getObj($id);
        return $obj->toJson();
    }

    public function postClone($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->cloneMe();
        $data = array('success' => true, 'msg' => 'Elemento clonato con successo');
        return Json::encode($data);
    }

    public function postSavePositions()
    {
        $model = $this->model;
        $ids = $_POST['ids'];
        $position = $_POST['position'];

        //\Utils::log($_POST,"postSavePositions");

        foreach ($ids as $i => $id) {
            $p = (int)$position[$i];
            $model::where('id', $id)->update(array('position' => $p));
        }

        $data = array('success' => true, 'msg' => 'Posizioni salvate con successo');
        return Json::encode($data);
    }

    public function postReorderPositions()
    {
        $parent_id = isset($_REQUEST['parent_id']) ? $_REQUEST['parent_id'] : false;
        $model = $this->model;
        $model::reorder($parent_id);

        $data = array('success' => true, 'msg' => 'Posizioni ordinate con successo');
        return Json::encode($data);
    }

    public function postSwitchFlag($id, $flag)
    {
        $model = $this->model;
        $obj = $model::find($id);

        if (isset($obj->$flag)) {
            if ($obj->$flag == 1) {
                $obj->update(array($flag => 0));
            } else {
                $obj->update(array($flag => 1));
            }
        }

        $data = array('success' => true, 'msg' => 'Flag modificato con successo per il record ' . $id);

        return Json::encode($data);
    }

    public function postSave()
    {
        $this->_before_create();
        if (count($this->errors) > 0) {
            $url = URL::action($this->action("getCreate"));
            if ($this->isPopup) {
                $url .= "?popup=Y";
            }
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($this->errors);
        }

        //Flash current values to session
        //Input::flash();
        \Input::flashExcept("_lang_fields[]", "_lang_fields");

        $task = Input::get('task');

        $rules = $this->getValidatorRules();
        $messages = $this->getValidatorMessages();
        $friendly_names = $this->getValidatorFriendlyNames();

        \Utils::log($rules, 'VALIDATOR RULES');

        //Get all inputs fields
        $input = Input::all();
        \Utils::log($input, "INPUT ALL");

        //Apply validaiton rules
        $validation = \Validator::make($input, $rules, $messages);
        if (count($friendly_names) > 0) {
            $validation->setAttributeNames($friendly_names);
        }
        //Validate rules
        if ($validation->fails()) {
            $url = URL::action($this->action("getCreate"));
            if ($this->isPopup) {
                $url .= "?popup=Y";
            }
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }
        $this->_prepare_data();
        $this->_prepare_data_create();

        if (count($this->errors) > 0) {
            $url = URL::action($this->action("getCreate"));
            if ($this->isPopup) {
                $url .= "?popup=Y";
            }
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($this->errors);
        }

        $input = Input::all();
        $model = $this->model;
        $obj = new $model;
        $obj->trigger_events = TRUE;
        $obj->save();
        $obj->trigger_events = FALSE;

        Input::flush();

        $id = $obj->id;

        $this->_after_create($obj);

        if ($task == 'reopen') {
            $url = URL::action($this->action("getEdit"), array($id));
            if ($this->isPopup) {
                $url .= "?popup=Y";
            }
            return Redirect::to($url);
            //return Redirect::action($this->action("getEdit"), array($id))->w
        } else {
            $redirectUrl = $this->redirectUrl();
            if ($redirectUrl) {
                return Redirect::to($redirectUrl);
            }
            return Redirect::action($this->redirectAction());
        }
    }


    protected function redirectAction()
    {
        return $this->action("getIndex");
    }

    protected function redirectUrl()
    {
        return false;
    }

    /**
     * @return Validator
     */
    protected function getValidator()
    {
        $rules = $this->getValidatorRules();
        $messages = $this->getValidatorMessages();
        $friendly_names = $this->getValidatorFriendlyNames();

        //Get all inputs fields
        $input = Input::all();

        //Apply validation rules
        $validation = \Validator::make($input, $rules, $messages);
        if (count($friendly_names) > 0) {
            $validation->setAttributeNames($friendly_names);
        }
        return $validation;
    }


    public function getRecord($id)
    {
        $success = true;
        $model = $this->model;
        $obj = $model::find($id);
        $msg = '';
        if (!$obj) {
            $success = false;
            $msg = "Could not find record with id [$id]";
        }
        return Json::encode(compact('success', 'obj', 'msg'));
    }


    public function postUpdateAjax($id)
    {
        $success = true;
        $model = $this->model;
        $obj = $model::find($id);
        $obj->trigger_events = TRUE;
        $this->_before_update($obj);

        $rules = $this->getValidatorRules();
        $messages = $this->getValidatorMessages();
        $friendly_names = $this->getValidatorFriendlyNames();
        //Get all inputs fields
        $input = Input::all();
        //Apply validaiton rules
        $validation = \Validator::make($input, $rules, $messages);
        if (count($friendly_names) > 0) {
            $validation->setAttributeNames($friendly_names);
        }
        //Validate rules
        if ($validation->fails()) {
            $success = false;
            $errors = $validation->messages()->toArray();
            return Json::encode(compact("success", "errors"));
        }
        $this->_prepare_data();
        $input = Input::all();
        $obj->update();
        $obj->trigger_events = FALSE;

        $this->_after_update($obj);

        return Json::encode(compact("success"));
    }


    public function postUpdate($id)
    {
        $model = $this->model;
        $obj = $model::find($id);
        $obj->trigger_events = TRUE;
        $this->_before_update($obj);
        if (count($this->errors) > 0) {
            return Redirect::action($this->action("getEdit"), array($id))->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($this->errors);
        }

        //Input::flash();
        \Input::flashExcept("_lang_fields[]");

        $task = Input::get('task');
        $rules = $this->getValidatorRules();
        $messages = $this->getValidatorMessages();
        $friendly_names = $this->getValidatorFriendlyNames();
        //Get all inputs fields
        $input = Input::all();
        //Apply validaiton rules
        $validation = \Validator::make($input, $rules, $messages);
        if (count($friendly_names) > 0) {
            $validation->setAttributeNames($friendly_names);
        }
        //Validate rules
        if ($validation->fails()) {
            return Redirect::action($this->action("getEdit"), array($id))->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }
        $this->_prepare_data();

        if (count($this->errors) > 0) {
            $url = URL::action($this->action($this->action("getEdit"), array($id)));
            if ($this->isPopup) {
                $url .= "?popup=Y";
            }
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($this->errors);
        }

        $obj->update();
        $obj->trigger_events = FALSE;

        Input::flush();

        $this->_after_update($obj);

        if ($task == 'reopen') {
            return Redirect::action($this->action("getEdit"), array($id));
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }

    protected function position($parent_id = false)
    {
        $instance = new $this->model;
        $query = $instance->where('id', '>', 0);
        if ($parent_id != false) {
            $query->where('parent_id', $parent_id);
        }
        $position = $query->max('position');
        return $position + 1;
    }

    protected function getValidatorRules()
    {
        $rules = $this->rules;
        return $rules;
    }

    protected function getValidatorMessages()
    {
        $messages = $this->messages;
        return $messages;
    }

    protected function getValidatorFriendlyNames()
    {
        $messages = $this->friendly_names;
        return $messages;
    }

    function _before_create()
    {

    }

    function _prepare_data()
    {

    }

    function _prepare_data_create()
    {

    }

    function _before_update($model)
    {

    }

    function _after_create($model)
    {

    }

    function _after_update($model)
    {

    }

    /* public function missingMethod($parameters) {
      \Utils::log($parameters,"MISSING METHOD");
      } */

    protected function render($data)
    {
        $this->beforeRender();
        $this->theme->setTitle($this->title . " | " . Cfg::get("application_title"));
        $this->theme->setBodyClass($this->bodyClass);

        $this->theme->partialComposer('pageheader', function ($view) {
            $view->title = $this->pageheader;
            $view->iconClass = isset($this->iconClass) ? $this->iconClass : 'font-home';
        });

        $this->theme->partialComposer('sidebar', function ($view) {
            $view->widgets = $this->widgets;
        });

        return $this->theme->scope($this->page, $data)->render();
    }

    protected function _assets()
    {


        if (USE_MIN === true) {
            $this->toFooter('js/min.1.1.js', 'mootools');
            $this->toFooter('ckeditor/ckeditor.js');
            $this->toHead('css/min.1.0.css');
        } else {
            /* $this->toFooter('js/plugins/imagepicker/mootools-core-1.3.1.js', 'mootools');
             $this->toFooter('js/plugins/imagepicker/mootools-more-1.3.1.1.js', 'mootools');
             $this->toFooter('js/plugins/imagepicker/Source/FileManager.js', 'mootools');
             $this->toQueue('js/plugins/imagepicker/Source/Uploader/Fx.ProgressBar.js', 'mootools');
             $this->toQueue('js/plugins/imagepicker/Source/Uploader/Swiff.Uploader.js', 'mootools');
             $this->toQueue('js/plugins/imagepicker/Source/Uploader.js', 'mootools');
             $this->toQueue('js/plugins/imagepicker/Language/Language.it.js', 'mootools');*/


            //$this->toFooter('js/jquery.min.js','jquery');
            $this->toFooter('js/jquery-1.11.1.min.js', 'jquery');
            $this->toFooter('js/jquery-migrate-1.2.1.min.js', 'jquery');
            //$this->toFooter('js/jquery_ui_custom.js', 'jquery');
            $this->toFooter('js/jquery-ui-1.10.3.custom.min.js', 'jquery');
            $this->toHead('css/jqueryui/jquery-ui-1.9.2.custom.min.css');
            //$this->toFooter('js/jquery-ui-1.8.2.custom.min.js', 'jquery');

            $this->toQueue('js/echo/jquery.noConflict.js');
            $this->toQueue('js/prototype.js');
            $this->toQueue('js/plugins/underscore-min.js');
            $this->toQueue('js/echo/jquery.json.js');
            $this->toQueue('js/echo/jquery.rest.js');
            $this->toQueue('js/jquery.tmpl.min.js');
            $this->toQueue('js/plugins/charts/excanvas.min.js');

            $this->toQueue('js/plugins/cookie/jquery.cookie.js');
            $this->toQueue('js/plugins/charts/jquery.flot.js');
            $this->toQueue('js/plugins/charts/jquery.flot.resize.js');
            $this->toQueue('js/plugins/charts/jquery.sparkline.min.js');


            $this->toQueue('js/plugins/forms/jquery.tagsinput.min.js');
            //$this->toQueue('js/plugins/forms/jquery.inputlimiter.min.js');
            //$this->toQueue('js/plugins/forms/jquery.maskedinput.min.js');
            //$this->toQueue('js/plugins/forms/jquery.autosize.js');
            $this->toQueue('js/plugins/forms/jquery.ibutton.js');
            //$this->toQueue('js/plugins/forms/jquery.dualListBox.js');
            //$this->toQueue('js/plugins/forms/jquery.validate.js');
            $this->toQueue('js/plugins/forms/jquery.uniform.min.js');
            $this->toQueue('js/plugins/forms/jquery.select2.min.js');
            //$this->toQueue('js/plugins/forms/select2_locale_it.js');
            //$this->toQueue('js/plugins/forms/jquery.cleditor.js');

            $this->toQueue('js/plugins/uploader/jquery.upload.js');
            $this->toQueue('js/plugins/uploader/plupload.js');
            $this->toQueue('js/plugins/uploader/plupload.html4.js');
            $this->toQueue('js/plugins/uploader/plupload.html5.js');
            $this->toQueue('js/plugins/uploader/jquery.plupload.queue.js');

            //$this->toQueue('js/plugins/wizard/jquery.form.wizard.js');
            //$this->toQueue('js/plugins/wizard/jquery.form.js');

            $this->toQueue('js/plugins/ui/jquery.collapsible.min.js');
            $this->toQueue('js/plugins/ui/jquery.timepicker.min.js');
            $this->toQueue('js/plugins/ui/jquery.ui.datepicker-it.js');
            $this->toQueue('js/plugins/ui/jquery.jgrowl.min.js');
            //$this->toQueue('js/plugins/ui/jquery.pie.chart.js');
            //$this->toQueue('js/plugins/ui/jquery.fullcalendar.min.js');
            //$this->toFooter('js/plugins/ui/elfinder.min.js');
            //$this->toQueue('js/plugins/ui/jquery.fancybox.js');
            $this->toHead('js/plugins/fancybox/jquery.fancybox.css');
            $this->toQueue('js/plugins/fancybox/jquery.fancybox.pack.js');

            $this->toQueue('js/plugins/datetimepicker/jquery-ui-timepicker-addon.js');
            $this->toQueue('js/plugins/datetimepicker/jquery-ui-sliderAccess.js');
            $this->toQueue('js/plugins/datetimepicker/i18n/jquery-ui-timepicker-it.js');

            $this->toQueue('js/plugins/validation-engine/jquery.validationEngine-it.js');
            $this->toQueue('js/plugins/validation-engine/jquery.validationEngine.min.js');


            $this->toFooter('js/plugins/tables/jquery.dataTables.min.js');

            $this->toQueue('js/plugins/bootstrap/bootstrap.js');
            $this->toQueue('js/plugins/bootstrap/bootstrap-bootbox.min.js');
            $this->toQueue('js/plugins/bootstrap/bootstrap-progressbar.js');
            $this->toQueue('js/plugins/bootstrap/bootstrap-colorpicker.js');


            //$this->toFooter('js/plugins/jquery.sticky-kit.min.js');
            $this->toQueue('js/plugins/jquery.sticky.js');
            $this->toQueue('js/plugins/jquery.tablednd.js');

            $this->toQueue('js/plugins/jquery-scrollto.js');


            // FANCY TREE
            $this->toHead("js/plugins/fancytree/skin-lion/ui.fancytree.css");
            $this->toQueue("js/plugins/fancytree/jquery.fancytree-all.min.js");
            //$this->toQueue("js/plugins/fancytree/jquery.fancytree.js");
            //$this->toQueue("js/plugins/fancytree/jquery.fancytree.persist.js");
            //$this->toQueue("js/plugins/fancytree/jquery.fancytree.filter.js");


            // DATATABLES FILTERS
            //$this->toHead("js/plugins/tables/jquery.dataTables.yadcf.css");
            //$this->toQueue("js/plugins/tables/jquery.dataTables.yadcf.js");
            //$this->toFooter("js/plugins/tables/jquery.dataTables.yadcf.js");
            $this->toFooter("js/plugins/tables/jquery.dataTables.columnFilter.js");
            $this->toFooter("js/plugins/tables/jquery.dataTables.colVis.js");
            $this->toHead("js/plugins/tables/jquery.dataTables.colVis.css");
            $this->toFooter('ckeditor/ckeditor.js');
            $this->toFooter('js/plugins/jquery.charactercounter.js');


            //ELFINDER
            //$this->toHead('js/plugins/validation-engine/css/validationEngine.jquery.css');
            $this->toFooter("js/plugins/elfinder/js/elfinder.min.js");
            $this->toFooter("js/plugins/elfinder/js/i18n/elfinder.it.js");
            $this->toHead("js/plugins/elfinder/css/elfinder.min.css");
            $this->toHead("js/plugins/elfinder/css/theme.css");


            $this->toHead('css/main.css');
            $this->toHead('js/plugins/datetimepicker/jquery-ui-timepicker-addon.css');
            $this->toHead('js/plugins/validation-engine/css/validationEngine.jquery.css');
            $this->toHead('css/echo.2.css');
        }

        $this->toFooter('https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js');
    }


    protected function checkRecords(&$messages, $obj)
    {
        if ($obj) {
            if (isset($obj->active) AND $obj->active == 0) {
                $messages[] = "lo status di attivazione del record è impostato su <strong class='label label-important'>NON ATTIVO</strong>";
            }
        }
    }


    public function getCheck($id)
    {
        $messages = [];
        $model = $this->model;
        $obj = $model::find($id);
        $this->checkRecords($messages, $obj);
        if (count($messages) > 0) {
            $data = ['success' => true, 'html' => '<li>' . implode('</li><li>', $messages) . '</li>'];
        } else {
            $data = ['success' => false, 'html' => ''];
        }
        return Response::json($data);
    }

}

