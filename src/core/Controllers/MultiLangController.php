<?php

class MultiLangController extends BackendController {

    public $model;    
    protected $lang_rules = array();
    protected $lang_messages = array();
    protected $lang_friendly_names = array();
    public $className;

    /* function _before_clone(&$model) {

      }

      function _after_clone($model) {

      } */


    public function getRecord($id){
        $success = true;
        $model = $this->model;
        $lang = Core::getLang();
        $obj = $model::getObj($id,$lang);
        $msg = '';
        if(!$obj){
            $success = false;
            $msg = "Could not find record with id [$id]";
        }
        return Json::encode(compact('success','obj','msg'));
    }

    protected function getValidatorRules() {
        $rules = $this->rules;
        $lang_rules = array();
        $languages = \Mainframe::languagesCodes();
        if (count($this->lang_rules) > 0) {
            foreach ($languages as $lang) {
                foreach ($this->lang_rules as $field => $rule) {
                    $key = $field . "_" . $lang;
                    $lang_rules[$key] = $rule;
                }
            }
        }
        return array_merge($rules, $lang_rules);
    }
    
    protected function getValidatorMessages() {
        $messages = $this->messages;
        $lang_messages = array();
        $languages = \Mainframe::languagesCodes();
        if (count($this->lang_messages) > 0) {
            foreach ($languages as $lang) {
                foreach ($this->lang_messages as $field => $rule) {
                    $key = $field . "_" . $lang;
                    $lang_messages[$key] = $rule;
                }
            }
        }
        return array_merge($messages, $lang_messages);
    }
    
    protected function getValidatorFriendlyNames() {
        $messages = $this->friendly_names;
        $lang_messages = array();
        $languages = \Mainframe::languagesCodes();
        if (count($this->lang_friendly_names) > 0) {
            foreach ($languages as $lang) {
                foreach ($this->lang_friendly_names as $field => $rule) {
                    $key = $field . "_" . $lang;
                    $lang_messages[$key] = $rule. " (".\Str::upper($lang).")";
                }
            }
        }
        return array_merge($messages, $lang_messages);
    }

    public function postFlagSingle($id, $lang, $status) {
        $model = $this->model;
        $obj = $model::find($id);

        if (isset($obj->active)) {
            $obj->setLangFields(array());
            $obj->update(array("active" => $status));
        } else {
            $tr = $obj->translations();
            $q = $tr->getQuery();
            $q->where('lang_id', $lang);
            $tr->update(array('published' => $status));
        }

        try{
            $obj->uncache();
        }catch(\Exception $e){
            \Utils::log($e->getMessage(),__METHOD__);
        }


        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

    public function postFlagMulti($status) {
        $ids = $_POST['ids'];
        $lang_id = $_POST['lang_id'];
        $model = $this->model;
        $rows = $model::whereIn('id', $ids)->get();
        foreach ($rows as $row) {
            if (isset($row->active)) {
                $row->setLangFields(array());
                $row->update(array("active" => $status));
            } else {
                $tr = $row->translations();
                $q = $tr->getQuery();
                $q->where('lang_id', $lang_id);
                $tr->update(array('published' => $status));
            }
            try{
                $row->uncache();
            }catch(\Exception $e){
                \Utils::log($e->getMessage(),__METHOD__);
            }
        }

        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

    /* public function missingMethod($parameters) {
      \Utils::log($parameters, "MISSING METHOD PARAMETER");
      } */

    protected function column_actions($data) {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);
        $clone = \URL::action($this->action("postClone"), $data['id']);

        $flag_label = 'Attiva';
        $flag_icon = 'icon-ok';
        $flag_status = 1;

        if (isset($data['active'])) {
            if ($data['active'] == '1') {
                $flag_label = 'Disattiva';
                $flag_icon = 'icon-off';
                $flag_status = 0;
            }
        }

        if (isset($data['published'])) {
            if ($data['published'] == '1') {
                $flag_label = 'Disattiva';
                $flag_icon = 'icon-off';
                $flag_status = 0;
            }
        }


        $status = \URL::action($this->action("postFlagSingle"), array($data['id'], $data['lang_id'], $flag_status));

        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="javascript:;" onclick="EchoTable.action('$status');" class="btn btn-action-status" title="$flag_label"><i class="$flag_icon"></i></a> </li>
    <li><a href="$edit" class="btn btn-action-edit" title="Modifica"><i class="icon-edit"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$clone');" class="btn btn-action-clone" title="Clona"><i class="icon-share"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn btn-action-trash" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function checkboxLang($data, $key, $lang) {

        $v = (int) $data[$key];
        $a = \URL::action($this->action("postSwitchFlagLang"), array($data['id'], $key, $lang));

        $actions = <<<HTML
<img src="/media/admin/mark$v.gif" onclick="EchoTable.action('$a');" style="cursor:pointer;" />
HTML;
        return $actions;
    }

    protected function renderBoolean($data, $key, $lang) {

        $v = (int) $data[$key];

        $actions = <<<HTML
<img src="/media/admin/mark$v.gif" />
HTML;
        return $actions;
    }

    public function postSwitchFlagLang($id, $flag, $lang) {
        $model = $this->model;
        $obj = $model::find($id);



        if (isset($obj->$lang->$flag)) {
            if ($obj->$lang->$flag == 1) {
                $tr = $obj->translations();
                $q = $tr->getQuery();
                $q->where('lang_id', $lang);
                $tr->update(array($flag => 0));
            } else {
                $tr = $obj->translations();
                $q = $tr->getQuery();
                $q->where('lang_id', $lang);
                $tr->update(array($flag => 1));
            }
            try{
                $obj->uncache();
            }catch(\Exception $e){
                \Utils::log($e->getMessage(),__METHOD__);
            }
        }

        $data = array('success' => true, 'msg' => 'Flag modificato con successo per il record ' . $id . " (Lingua: $lang)");

        return Json::encode($data);
    }


    protected function checkRecords(&$messages,$obj){
        if($obj){
            $translations = $obj->translations;
            if(!$translations)return;
            foreach($translations as $tr){
                if(isset($tr->published) AND $tr->published == 0){
                    $messages[] = "lo status di pubblicazione per la lingua <strong class='label label-info'>$tr->lang_id</strong> è impostato su <strong class='label label-important'>NON PUBBLICATO</strong>";
                }
                if(isset($tr->name) AND \Str::contains($tr->name,'-copy')){
                    $messages[] = "il campo <strong class='label'>NOME</strong> per la lingua <strong class='label label-info'>$tr->lang_id</strong> contiene la stringa <strong class='label label-important'>-copy</strong>";
                }
                if(isset($tr->slug) AND \Str::contains($tr->slug,'-copy')){
                    $messages[] = "il campo <strong class='label'>PERMALINK</strong> per la lingua <strong class='label label-info'>$tr->lang_id</strong> contiene la stringa <strong class='label label-important'>-copy</strong>";
                }
                if(isset($tr->metatitle) AND \Str::length($tr->metatitle) > 70){
                    $messages[] = "il campo <strong class='label'>SEO - TITLE</strong> per la lingua <strong class='label label-info'>$tr->lang_id</strong> è superiore ai <strong class='label label-inverse'>70</strong> caratteri consigliati";
                }
                if(isset($tr->metadescription) AND \Str::length($tr->metadescription) > 160){
                    $messages[] = "il campo <strong class='label'>SEO - META DESCRIPTION</strong> per la lingua <strong class='label label-info'>$tr->lang_id</strong> è superiore ai <strong class='label label-inverse'>160</strong> caratteri consigliati";
                }
            }
        }
    }

}

?>
