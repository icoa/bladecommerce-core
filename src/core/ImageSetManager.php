<?php

namespace Core;

use DB;
use Product;

class ImageSetManager
{

    private static $instance = null;
    private $data = [];
    private $ids_add = [];
    private $ids_remove = [];
    private $valid_attributes = [];
    private $ids_unwanted = [];
    private $cacheLifeTime = 60;
    private $attributes_options = [];
    private $rules = [];

    /**
     * @return ImageSetManager
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }


    function __construct()
    {
        $this->rules = DB::table("images_sets_rules")->get();
        foreach($this->rules as $rule){
            if($rule->target_mode == '*' and $rule->target_id == 0){
                $this->ids_add[] = $rule->image_set_id;
            }
            if($rule->target_mode == '-' and $rule->target_id == 0){
                $this->ids_remove[] = $rule->image_set_id;
            }
            if($rule->target_type == 'ATT'){
                $this->valid_attributes[] = $rule->target_id;
            }
        }
        /*$this->ids_add = DB::table("images_sets_rules")->where("target_mode", "*")->where("target_id", 0)->lists("image_set_id");
        $this->ids_remove = DB::table("images_sets_rules")->where("target_mode", "-")->where("target_id", 0)->lists("image_set_id");
        $this->valid_attributes = DB::table("images_sets_rules")->where("target_type", "ATT")->lists("target_id");*/
        $this->ids_unwanted = DB::table("images_sets")->where("published", 0)->orWhereNotNull("deleted_at")->lists("id");
        //audit($this, 'ImageSetManager');
    }

    function getValidAttributes()
    {
        return $this->valid_attributes;
    }


    protected function find($params, $return_obj = false){
        $results = [];
        $cache_key = null;
        foreach($params as $key => $value){
            $cache_key .= $key.'-'.$value.'|';
        }
        if($this->get($cache_key) !== null){
            return $this->get($cache_key);
        }
        foreach($this->rules as $rule){
            $conditions = 0;
            $total = count($params);
            foreach($params as $field => $value){
                if($rule->$field == $value)
                    $conditions++;
            }
            if($conditions == $total)
                $results[] = ($return_obj) ? $rule : $rule->image_set_id;
        }
        $this->set($cache_key, $results);
        return $results;
    }


    function bindIds($type, $id)
    {
        if ($id > 0) {
            //$this->ids_add = array_merge($this->ids_add, DB::table("images_sets_rules")->remember($this->cacheLifeTime, 'images_sets_rules_' . $type . '_plus_' . $id)->where("target_type", $type)->where("target_mode", "+")->where("target_id", $id)->lists("image_set_id"));
            //$this->ids_remove = array_merge($this->ids_remove, DB::table("images_sets_rules")->remember($this->cacheLifeTime, 'images_sets_rules_' . $type . '_minus_' . $id)->where("target_type", $type)->where("target_mode", "-")->where("target_id", $id)->lists("image_set_id"));
            $this->ids_add  = array_merge($this->ids_add, $this->find([
                'target_mode' => '+',
                'target_type' => $type,
                'target_id' => $id,
            ]));

            $this->ids_remove  = array_merge($this->ids_remove, $this->find([
                'target_mode' => '-',
                'target_type' => $type,
                'target_id' => $id,
            ]));
        }
    }

    function bindProductAttributes(Product $obj)
    {
        $attributes_options = $obj->getProductAttributesOptionsIds(true, ['whitelist' => $this->getValidAttributes()]);
        if (!empty($attributes_options)) {
            $this->ids_add = array_merge($this->ids_add, DB::table("images_sets_rules")->where("target_type", "ATT")->where("target_mode", "+")->whereIn("target_id", $attributes_options)->lists("image_set_id"));
            $this->ids_remove = array_merge($this->ids_remove, DB::table("images_sets_rules")->where("target_type", "ATT")->where("target_mode", "-")->whereIn("target_id", $attributes_options)->lists("image_set_id"));
            $this->set(implode('-', $attributes_options), 1);
        }
        $this->attributes_options = $attributes_options;
    }

    function set($key, $value)
    {
        $this->data[$key] = $value;
    }

    function get($key)
    {
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }

    function getImageSets(Product $obj)
    {
        $attributes_options = $this->attributes_options;
        $ids_remove = array_merge($this->ids_remove, $this->ids_unwanted);

        $ids_add = array_unique($this->ids_add);
        $ids_remove = array_unique($ids_remove);
        $ids = array_diff($ids_add, $ids_remove);
        //audit($ids, "IDS getRelatedImagesForProducts");
        //check if every rule of every set is satisfied
        $final_ids = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                //$rules = \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_' . $id)->where("image_set_id", $id)->get();
                $rules = $this->find(['image_set_id' => $id], true);
                $pivot = [];
                foreach ($rules as $rule) {
                    //\Utils::log($rule,"RULE");
                    switch ($rule->target_mode) {
                        case "+":
                            switch ($rule->target_type) {
                                case "CAT":
                                    $pivot[] = ($obj->default_category_id == $rule->target_id OR $obj->main_category_id == $rule->target_id) ? 1 : 0;
                                    break;
                                case "BRA":
                                    $pivot[] = ($obj->brand_id == $rule->target_id) ? 1 : 0;
                                    break;
                                case "COL":
                                    $pivot[] = ($obj->collection_id == $rule->target_id) ? 1 : 0;
                                    break;
                                case "ATT":
                                    $pivot[] = (in_array($rule->target_id, $attributes_options)) ? 1 : 0;
                                    break;
                            }
                            break;

                        case "-":
                            switch ($rule->target_type) {
                                case "CAT":
                                    $pivot[] = ($obj->default_category_id != $rule->target_id AND $obj->main_category_id != $rule->target_id) ? 1 : 0;
                                    break;
                                case "BRA":
                                    $pivot[] = ($obj->brand_id != $rule->target_id) ? 1 : 0;
                                    break;
                                case "COL":
                                    $pivot[] = ($obj->collection_id != $rule->target_id) ? 1 : 0;
                                    break;
                                case "ATT":
                                    $pivot[] = (!in_array($rule->target_id, $attributes_options)) ? 1 : 0;
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                    //\Utils::log($pivot,"PIVOT");
                }

                if (array_sum($pivot) == count($rules)) {
                    $final_ids[] = $id;
                }
            }
        }
        return $final_ids;
    }

}
