<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 24/02/2015
 * Time: 11:56
 */

namespace Core;

use Carbon\Carbon;
use Frontend\Catalog as FrontCatalog;
use Underscore\Types\Arrays;
use View;
use NavType;

class SitemapBuilder
{

    protected $urlset = [];
    private $cache = [];
    private $xml;
    protected $config;
    protected $now;
    protected $lang;
    protected $debug = false;
    /** @var \Illuminate\Console\Command|null */
    protected $command;

    function __construct($lang)
    {
        //ini_set('max_execution_time',0);
        $this->lang = $lang;
        $this->config = \Cfg::getGroup('rss');
        $this->now = date('Y-m-d');
        $this->command = \Registry::console();
    }

    function buildSitemap($file = null)
    {
        //audit_watch();
        \Cache::setDefaultDriver(config('cache.product_driver', 'apc'));
        if ($file != null) {
            $method = \Str::camel("handle " . str_replace(".xml", "", $file));
            if (method_exists($this, $method)) {
                $this->$method();
            }
        } else {
            $this->handleBoilerplates();
            $this->handleCategories();
            $this->handleProducts();
        }

    }

    private function reset()
    {
        $this->urlset = [];
        $this->cache = [];
        $this->xml = null;
    }


    private function handleHomepage()
    {
        $frequency = $this->cfg('SITEMAP_HOMEPAGE_FREQUENCY', 'weekly');
        $priority = $this->cfg('SITEMAP_HOMEPAGE_PRIORITY', 0.5);
        $url = \Link::create('base', 1, $this->lang)->absolute()->getLink();
        $set = new SitemapLink($url, $this->now, $frequency, $priority);
        $this->addSet($set);
    }

    /**
     * php5 artisan cron:task sitemap --lang=it --locale=IT --type=categories
     */
    private function handleCategories()
    {
        $this->reset();
        $frequency = $this->cfg('SITEMAP_PRODUCT_CATEGORIES_FREQUENCY', 'weekly');
        $priority = $this->cfg('SITEMAP_PRODUCT_CATEGORIES_PRIORITY', 0.5);
        $attribute_frequency = $this->cfg('SITEMAP_ATTRIBUTES_FREQUENCY', 'weekly');
        $attribute_priority = $this->cfg('SITEMAP_ATTRIBUTES_PRIORITY', 0.5);

        //categories
        $builder = \Category::rows($this->lang)->where('published', 1)->orderBy('position');
        if ($this->debug) {
            $builder->take(5);
        }
        $categories = $builder->get();
        foreach ($categories as $row) {

            $conditions = ['category' => $row->id];
            if ($this->hasProducts($conditions)):
                list($frequency, $priority) = $this->getSitemapData($conditions);
                if ($this->debug) {
                    audit(compact('conditions', 'frequency', 'priority'), 'CATEGORY');
                }
                $url = \Link::create('category', $row->id, $this->lang)->absolute()->getLink();
                $set = new SitemapLink($url, $this->now, $frequency, $priority);
                $images_field = ['image_default', 'image_thumb', 'image_zoom'];
                foreach ($images_field as $image) {
                    if ($row->$image != '') {
                        $image_url = \Site::img($row->$image, true);
                        $title = $row->name;
                        $caption = $row->name;
                        $set->addImage($image_url, $title, $caption);
                    }
                }
                $this->addSet($set);

            endif;
        }


        //brands
        $builder = \Brand::rows($this->lang)->where('published', 1)->orderBy('position');
        if ($this->debug) {
            $builder->take(5);
        }
        $brands = $builder->get();
        foreach ($brands as $row) {
            $conditions = ['brand' => $row->id];
            if ($this->hasProducts($conditions)):
                list($frequency, $priority) = $this->getSitemapData($conditions);
                if ($this->debug) {
                    audit(compact('conditions', 'frequency', 'priority'), 'BRAND');
                }
                $row->setCategories();
                $url = \Link::create('brand', $row->id, $this->lang)->absolute()->getLink();
                $set = new SitemapLink($url, $this->now, $frequency, $priority);
                $images_field = ['image_default', 'image_thumb', 'image_zoom'];
                foreach ($images_field as $image) {
                    if ($row->$image != '') {
                        $image_url = \Site::img($row->$image, true);
                        $title = $row->name;
                        $caption = $row->name;
                        $set->addImage($image_url, $title, $caption);
                    }
                }
                $this->addSet($set);
            endif;

            $collections = $row->getCollections($this->debug);
            foreach ($collections as $collection) {
                $conditions = ['brand' => $row->id, 'collection' => $collection->id];
                if ($this->hasProducts($conditions)):
                    list($frequency, $priority) = $this->getSitemapData($conditions);
                    if ($this->debug) {
                        audit(compact('conditions', 'frequency', 'priority'), 'BRAND + COLLECTION');
                    }
                    $url = \Link::create('brand', $row->id, $this->lang)->addModifier('collection', $collection->id)->absolute()->getLink();
                    $set = new SitemapLink($url, $this->now, $frequency, $priority);
                    $images_field = ['image_default', 'image_thumb', 'image_zoom'];
                    foreach ($images_field as $image) {
                        if ($collection->$image != '') {
                            $image_url = \Site::img($collection->$image, true);
                            $title = $collection->name;
                            $caption = $collection->name;
                            $set->addImage($image_url, $title, $caption);
                        }
                    }
                    $this->addSet($set);
                endif;
            }
        }


        //trends
        $builder = \Trend::rows($this->lang)->where('published', 1)->orderBy('position');
        if ($this->debug) {
            $builder->take(5);
        }
        $trends = $builder->get();
        foreach ($trends as $row) {
            $conditions = ['trend' => $row->id];
            if ($this->hasProducts($conditions)):
                list($frequency, $priority) = $this->getSitemapData($conditions);
                if ($this->debug) {
                    audit(compact('conditions', 'frequency', 'priority'), 'TREND');
                }
                $url = \Link::create('trend', $row->id, $this->lang)->absolute()->getLink();
                $set = new SitemapLink($url, $this->now, $frequency, $priority);
                $images_field = ['image_default', 'image_thumb', 'image_zoom'];
                foreach ($images_field as $image) {
                    if ($row->$image != '') {
                        $image_url = \Site::img($row->$image, true);
                        $title = $row->name;
                        $caption = $row->name;
                        $set->addImage($image_url, $title, $caption);
                    }
                }
                $this->addSet($set);
            endif;
        }


        //category+brand
        foreach ($brands as $brand) {
            foreach ($categories as $category) {
                if (in_array($category->id, $brand->getCategories())) {

                    $conditions = ['category' => $category->id, 'brand' => $brand->id];
                    if ($this->hasProducts($conditions)):
                        list($frequency, $priority) = $this->getSitemapData($conditions);
                        if ($this->debug) {
                            audit(compact('conditions', 'frequency', 'priority'), 'CATEGORY + BRAND');
                        }
                        $url = \Link::create('brand', $brand->id, $this->lang)->addModifier('category', $category->id)->absolute()->getLink();
                        $set = new SitemapLink($url, $this->now, $frequency, $priority);
                        $this->addSet($set);

                    endif;
                }
            }
        }


        //main-attributes
        $all_options = [];
        $xref_options = [];
        $attributes = \Attribute::rows($this->lang)->where('is_nav', 1)->where('is_visible_in_advanced_search', 1)->where('published', 1)->orderBy('position')->get();
        foreach ($attributes as $attribute) {
            $options = $attribute->options($this->lang);

            $xref_options[] = $options;

            $sitemapConditions = ['attribute' => $attribute->id];
            if ($attribute->code == 'gender')
                $sitemapConditions = ['gender' => 1];

            list($frequency, $priority) = $this->getSitemapData($sitemapConditions);
            if ($this->debug) {
                audit(compact('sitemapConditions', 'frequency', 'priority'), 'ATTRIBUTE');
            }

            foreach ($options as $option) {
                $conditions = ['attribute' => $option->id];

                if ($this->hasProducts($conditions)):
                    $url = \Link::create($attribute->code, $option->id, $this->lang)->absolute()->getLink();
                    $set = new SitemapLink($url, $this->now, $frequency, $priority);
                    $this->addSet($set);
                    $option->setCategories();
                    $option->setBrands();
                    $option->isGender = $attribute->code == 'gender';
                    $all_options[] = $option;

                endif;
            }
        }

        //category+attributes
        foreach ($all_options as $option) {
            foreach ($categories as $category) {
                if (in_array($category->id, $option->getCategories())) {

                    $conditions = ['attribute' => $option->id, 'category' => $category->id];
                    $sitemapConditions = ($option->isGender) ? ['gender' => 1, 'category' => $category->id] : $conditions;

                    if ($this->hasProducts($conditions)):
                        list($frequency, $priority) = $this->getSitemapData($sitemapConditions);
                        if ($this->debug) {
                            audit(compact('sitemapConditions', 'frequency', 'priority'), 'CATEGORY + ATTRIBUTE');
                        }
                        $url = \Link::create($option->code, $option->id, $this->lang)->addModifier('category', $category->id)->absolute()->getLink();
                        $set = new SitemapLink($url, $this->now, $frequency, $priority);
                        $this->addSet($set);

                        //category + attributes +  brands
                        foreach ($brands as $brand) {
                            $conditions = ['attribute' => $option->id, 'category' => $category->id, 'brand' => $brand->id];
                            $sitemapConditions = ($option->isGender) ? ['gender' => 1, 'category' => $category->id, 'brand' => $brand->id] : $conditions;

                            if ($this->hasProducts($conditions)):
                                list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                                if ($this->debug) {
                                    audit(compact('sitemapConditions', 'frequency', 'priority'), 'CATEGORY + BRAND + ATTRIBUTE');
                                }

                                $url = \Link::create($option->code, $option->id, $this->lang)
                                    ->addModifier('category', $category->id)
                                    ->addModifier('brand', $brand->id)
                                    ->absolute()->getLink();
                                $set = new SitemapLink($url, $this->now, $frequency, $priority);
                                $this->addSet($set);

                            endif;
                        }

                    endif;
                }
            }
        }


        //brand+attributes
        foreach ($all_options as $option) {
            foreach ($brands as $brand) {
                if (in_array($brand->id, $option->getBrands())) {

                    $conditions = ['attribute' => $option->id, 'brand' => $brand->id];
                    $sitemapConditions = ($option->isGender) ? ['gender' => 1, 'brand' => $brand->id] : $conditions;

                    if ($this->hasProducts($conditions)):
                        list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                        if ($this->debug) {
                            audit(compact('sitemapConditions', 'frequency', 'priority'), 'BRAND + ATTRIBUTE');
                        }

                        $url = \Link::create($option->code, $option->id, $this->lang)->addModifier('brand', $brand->id)->absolute()->getLink();
                        $set = new SitemapLink($url, $this->now, $frequency, $priority);
                        $this->addSet($set);

                    endif;
                }
            }
        }

        //options+options
        $keys = array_keys($xref_options);
        foreach ($keys as $key) {
            $options = $xref_options[$key];
            foreach ($xref_options as $xref_key => $xref_value) {
                if ($xref_key != $key) {
                    foreach ($options as $o) {
                        foreach ($xref_value as $v) {
                            if ($this->debug) {
                                //audit(['left' => $o->toArray(), 'right' => $v->toArray()], 'OPTION + OPTION');
                            }

                            if ($o->isGender) {
                                $sitemapConditions = ['gender' => 1, 'attribute' => $v->id];
                            } elseif ($v->isGender) {
                                $sitemapConditions = ['gender' => 1, 'attribute' => $o->id];
                            } else {
                                $sitemapConditions = [];
                            }

                            list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                            if ($this->debug) {
                                audit(compact('sitemapConditions', 'frequency', 'priority'), 'OPTION + OPTION');
                            }

                            $url = \Link::create($o->code, $o->id, $this->lang)->addModifier($v->code, $v->id)->absolute()->getLink();
                            $set = new SitemapLink($url, $this->now, $frequency, $priority);
                            $this->addSet($set);
                        }
                    }
                }
            }
        }

        $navs = \Nav::rows($this->lang)->where('published', 1)->where('indexable', 1)->where('navtype_id', '>', 2)->orderBy('navtype_id')->orderBy('position')->get();

        //navs
        foreach ($navs as $nav) {
            $sitemapConditions = $this->getNavTypeSitemapConditions($nav);
            $conditions = $this->getNavTypeConditions($nav);
            if ($this->hasProducts($conditions)):
                list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                if ($this->debug) {
                    audit(compact('sitemapConditions', 'frequency', 'priority'), 'NAV');
                }

                $url = \Link::create('nav', $nav->id, $this->lang)->absolute()->getLink();
                $set = new SitemapLink($url, $this->now, $frequency, $priority);
                $this->addSet($set);

            endif;
        }


        //navs+categories
        foreach ($navs as $nav) {
            foreach ($categories as $category) {
                $sitemapConditions = $this->getNavTypeSitemapConditions($nav);
                $conditions = $this->getNavTypeConditions($nav);
                $conditions = array_merge($conditions, ['category' => $category->id]);
                $sitemapConditions = array_merge($sitemapConditions, ['category' => $category->id]);
                if ($this->hasProducts($conditions)):
                    list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                    if ($this->debug) {
                        audit(compact('sitemapConditions', 'frequency', 'priority'), 'NAV + CATEGORY');
                    }

                    $url = \Link::create('nav', $nav->id, $this->lang)->addModifier('category', $category->id)->absolute()->getLink();
                    $set = new SitemapLink($url, $this->now, $frequency, $priority);
                    $this->addSet($set);

                endif;
            }
        }

        //navs+brands
        foreach ($navs as $nav) {
            foreach ($brands as $brand) {
                $sitemapConditions = $this->getNavTypeSitemapConditions($nav);
                $conditions = $this->getNavTypeConditions($nav);
                $conditions = array_merge($conditions, ['brand' => $brand->id]);
                $sitemapConditions = array_merge($sitemapConditions, ['brand' => $brand->id]);
                if ($this->hasProducts($conditions)):
                    list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                    if ($this->debug) {
                        audit(compact('sitemapConditions', 'frequency', 'priority'), 'NAV + BRAND');
                    }

                    $url = \Link::create('nav', $nav->id, $this->lang)->addModifier('brand', $brand->id)->absolute()->getLink();
                    $set = new SitemapLink($url, $this->now, $frequency, $priority);
                    $this->addSet($set);

                endif;
            }
        }

        //navs+attributes
        foreach ($all_options as $option) {
            foreach ($navs as $nav) {
                $sitemapConditions = $this->getNavTypeSitemapConditions($nav);
                $conditions = $this->getNavTypeConditions($nav);
                $local_conditions = $option->isGender ? ['gender' => 1] : ['attribute' => $option->id];
                $conditions = array_merge($conditions, $local_conditions);
                $sitemapConditions = array_merge($sitemapConditions, $local_conditions);
                if ($this->hasProducts($conditions)):
                    list($frequency, $priority) = $this->getSitemapData($sitemapConditions);

                    if ($this->debug) {
                        audit(compact('sitemapConditions', 'frequency', 'priority'), 'NAV + ATTRIBUTE');
                    }

                    $url = \Link::create($option->code, $option->id, $this->lang)->addModifier('nav', $nav->id)->absolute()->getLink();
                    $set = new SitemapLink($url, $this->now, $frequency, $priority);
                    $this->addSet($set);

                endif;
            }
        }


        $this->store('categories');

    }


    private function handleBoilerplates()
    {
        $this->reset();
        $this->handleHomepage();
        $frequency = $this->cfg('SITEMAP_CMS_CATEGORIES_FREQUENCY', 'weekly');
        $priority = $this->cfg('SITEMAP_CMS_CATEGORIES_PRIORITY', 0.5);

        $navs = \Nav::rows($this->lang)->where('published', 1)->where('indexable', 1)->orderBy('navtype_id')->orderBy('position')->get();
        foreach ($navs as $nav) {
            if ($nav->id > 1) {
                $url = \Link::create('nav', $nav->id, $this->lang)->absolute()->getLink();
                if ($nav->shortcut == 'collections') {
                    $set = new SitemapLink($url, $this->now, 'weekly', 0.7);
                } else {
                    $set = new SitemapLink($url, $this->now, $frequency, $priority);
                }
                $this->addSet($set);
            }
        }

        $sections = \Section::rows($this->lang)->where('published', 1)->where('indexable', 1)->orderBy('position')->get();
        foreach ($sections as $section) {
            $url = \Link::create('section', $section->id, $this->lang)->absolute()->getLink();
            $set = new SitemapLink($url, $this->now, $frequency, $priority);
            $images_field = ['image_default', 'image_thumb', 'image_zoom'];
            foreach ($images_field as $image) {
                if ($section->$image != '') {
                    $image_url = \Site::img($section->$image, true);
                    $title = $section->name;
                    $caption = $section->name;
                    $set->addImage($image_url, $title, $caption);
                }
            }
            $this->addSet($set);
        }


        $frequency = $this->cfg('SITEMAP_CMS_FREQUENCY', 'weekly');
        $priority = $this->cfg('SITEMAP_CMS_PRIORITY', 0.5);


        $pages = \Page::rows($this->lang)->where('published', 1)->where('indexable', 1)->orderBy('position')->get();
        foreach ($pages as $page) {
            $url = \Link::create('page', $page->id, $this->lang)->absolute()->getLink();
            $set = new SitemapLink($url, $this->now, $frequency, $priority);
            $images_field = ['image_default', 'image_thumb', 'image_zoom'];
            foreach ($images_field as $image) {
                if ($page->$image != '') {
                    $image_url = \Site::img($page->$image, true);
                    $title = $page->name;
                    $caption = $page->name;
                    $set->addImage($image_url, $title, $caption);
                }
            }
            $this->addSet($set);
        }


        $pages = \PriceRule::rows($this->lang)->where('active', 1)->orderBy('position')->get();
        foreach ($pages as $page) {
            $url = \Link::create('promo', $page->id, $this->lang)->absolute()->getLink();
            $set = new SitemapLink($url, $this->now, $frequency, $priority);
            $images_field = ['image_list', 'image_content'];
            foreach ($images_field as $image) {
                if ($page->$image != '') {
                    $image_url = \Site::img($page->$image, true);
                    $title = $page->name;
                    $caption = $page->name;
                    $set->addImage($image_url, $title, $caption);
                }
            }
            $this->addSet($set);
        }
        $this->store('boilerplates');
    }


    private function handleProducts()
    {
        $this->reset();
        $frequency = $this->cfg('SITEMAP_PRODUCTS_FREQUENCY', 'weekly');
        $priority = $this->cfg('SITEMAP_PRODUCTS_PRIORITY', 0.5);

        $products = \Product::rows($this->lang)
            ->where('indexed', 1)
            ->where('published', 1)
            ->where('visibility', '<>', 'none')
            ->where('is_out_of_production', 0)
            ->orderBy('created_at', 'desc')
            ->select(['id', 'name', 'slug'])->get();

        $many = count($products);
        if($this->command)
            $this->command->comment("Found $many products");
        $counter = 0;

        foreach ($products as $product) {
            $counter++;
            if($this->command)
                $this->command->comment("Adding product $product->id ($counter/$many)");
            /** @var \Product $feed */
            $feed = \Product::getProductFeed($product->id, $this->lang);
            //audit($feed, __METHOD__);
            if ($feed) {
                $url = $feed->link_absolute;
                $images = $feed->getImages();
                $product = $feed;
            } else {
                $url = \Link::create('product', $product->id, $this->lang)->absolute()->getLink();
                $images = $product->getImages();
            }

            $set = new SitemapLink($url, $this->now, $frequency, $priority);
            foreach ($images as $image) {
                $image_url = \Site::img($image->defaultImg, true);
                $title = $product->name;
                $caption = ($image->legend == '') ? $product->name : $image->legend;
                $set->addImage($image_url, $title, $caption);
                $image_url = $title = $caption = $image = null;
            }
            $this->addSet($set);
            $url = $images = null;
        }
        $this->store('products');
    }


    function cfg($key, $default = null)
    {
        if (isset($this->config[$key]) AND trim($this->config[$key]) != '') {
            return $this->config[$key];
        }
        return $default;
    }

    function addSet($set)
    {
        if (!isset($this->cache[$set->url])) {
            $this->urlset[] = $set;
            $this->cache[$set->url] = true;
        }

    }

    function getSet()
    {
        return $this->urlset;
    }


    function toXml()
    {
        if ($this->xml != null) {
            return $this->xml;
        }
        $set = $this->urlset;
        $content = '';
        $date = date("Y-m-d H:i:s");
        foreach ($set as $url) {
            $content .= $url->toXml();
        }
        $this->xml = View::make('xml.sitemap', ['content' => $content, 'date' => $date])->render();
        return $this->xml;
    }


    function store($file)
    {
        $path = storage_path("xml/sitemap/{$this->lang}_{$file}.xml");
        //$path = public_path("{$this->lang}_{$file}.xml");
        try {
            \File::put($path, $this->toXml());
        } catch (\Exception $e) {
            \Utils::log($e->getMessage());
        }
    }

    function hasProducts($conditions)
    {
        $catalog = new FrontCatalog();
        //\Utils::log($conditions, __METHOD__);
        $products = [];
        foreach ($conditions as $key => $value) {
            $catalog->addCondition($key, $value);
        }
        $catalog->setup();
        $catalog->loadProducts();
        $products = $catalog->getProducts();
        unset($catalog);

        //\Utils::log($products, __METHOD__);
        return count($products) > 0;
    }

    function getNavTypeConditions($nav)
    {
        $conditions = [];
        if ($nav->navtype_id == 5) {
            if ($nav->minprice > 0) {
                $conditions['minprice'] = $nav->minprice;
            }
            if ($nav->maxprice > 0) {
                $conditions['maxprice'] = $nav->maxprice;
            }
        } else {
            $conditions['nav'] = $nav->id;
        }
        return $conditions;
    }

    function getNavTypeSitemapConditions($nav)
    {
        $conditions = [];
        $conditions['nav'] = $nav->id;
        if (NavType::navTypeIdToCode($nav->navtype_id) !== NavType::TYPE_CODE_DEFAULT) {
            $conditions[NavType::navTypeIdToCode($nav->navtype_id)] = 1;
        }
        switch ($nav->shortcut) {
            case 'outlet':
                $conditions['outlet'] = 1;
                break;
            case 'collections':
                $conditions['collections'] = 1;
                break;
        }
        return $conditions;
    }

    private function checksum($keys, $conditions)
    {
        $total = count($conditions);
        $totalKeys = count($keys);
        $totalOk = 0;
        $totalKo = 0;
        foreach ($conditions as $name => $id) {
            if (in_array($name, $keys) and $id > 0) {
                $totalOk++;
            } else {
                $totalKo++;
            }
        }
        return $totalOk == $totalKeys;
    }

    function getSitemapData($conditions)
    {

        $priority = '0.5';
        $frequency = 'monthly';

        if ($this->checksum(['category'], $conditions)) {
            $priority = '0.9';
            $frequency = 'weekly';
        }
        if ($this->checksum(['brand'], $conditions)) {
            $priority = '0.7';
            $frequency = 'weekly';
        }
        if ($this->checksum(['collection'], $conditions)) {
            $priority = '0.6';
            $frequency = 'monthly';
        }
        if ($this->checksum(['attribute'], $conditions)) {
            $priority = '0.6';
            $frequency = 'monthly';
        }
        if ($this->checksum(['gender'], $conditions)) {
            $priority = '0.6';
            $frequency = 'monthly';
        }
        if ($this->checksum(['nav'], $conditions)) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if ($this->checksum(['outlet'], $conditions)) {
            $priority = '0.8';
            $frequency = 'weekly';
        }
        if ($this->checksum(['trend'], $conditions)) {
            $priority = '0.7';
            $frequency = 'weekly';
        }
        if ($this->checksum(['category', 'brand'], $conditions)) {
            $priority = '0.9';
            $frequency = 'daily';
        }
        if ($this->checksum(['category', 'gender'], $conditions)) {
            $priority = '0.9';
            $frequency = 'daily';
        }
        if ($this->checksum(['category', 'attribute'], $conditions)) {
            $priority = '0.9';
            $frequency = 'daily';
        }
        if ($this->checksum(['category', 'nav'], $conditions)) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if ($this->checksum(['category', 'outlet'], $conditions)) {
            $priority = '0.8';
            $frequency = 'weekly';
        }
        if ($this->checksum(['brand', 'collection'], $conditions)) {
            $priority = '0.6';
            $frequency = 'weekly';
        }
        if ($this->checksum(['brand', 'nav'], $conditions)) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if ($this->checksum(['brand', 'gender'], $conditions)) {
            $priority = '0.6';
            $frequency = 'weekly';
        }
        if ($this->checksum(['brand', 'outlet'], $conditions)) {
            $priority = '0.8';
            $frequency = 'weekly';
        }
        if ($this->checksum(['attribute', 'nav'], $conditions)) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if ($this->checksum(['gender', 'nav'], $conditions)) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if ($this->checksum(['category', 'brand', 'attribute'], $conditions)) {
            $priority = '0.6';
            $frequency = 'monthly';
        }
        if ($this->checksum(['category', 'brand', 'gender'], $conditions)) {
            $priority = '0.8';
            $frequency = 'weekly';
        }
        if ($this->checksum(['category', 'nav', 'attribute'], $conditions)) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if ($this->checksum(['brand', 'collection', 'attribute'], $conditions)) {
            $priority = '0.8';
            $frequency = 'weekly';
        }
        if (isset($conditions['pricerange'])) {
            $priority = '0.5';
            $frequency = 'monthly';
        }
        if (isset($conditions['collections'])) {
            $priority = '0.7';
            $frequency = 'weekly';
        }
        if (isset($conditions['outlet'])) {
            $priority = '0.8';
            $frequency = 'weekly';
        }

        return [$frequency, $priority];
    }

    function __getSitemapData($conditions)
    {
        //categories, brands
        $priority = '0.9';
        $frequency = 'weekly';

        $withCategory = array_get($conditions, 'category') > 0;
        $withBrand = array_get($conditions, 'brand') > 0;
        $withAttribute = array_get($conditions, 'attribute') > 0;
        $withNav = array_get($conditions, 'nav') > 0;
        $withCollection = array_get($conditions, 'collection') > 0;
        $withTrend = array_get($conditions, 'trend') > 0;
        $withPrice = array_get($conditions, 'minprice') > 0 or array_get($conditions, 'maxprice') > 0;
        $navIsOutlet = isset($conditions['nav']) and $conditions['nav'] == 19;

        //every combination with trend
        if ($withTrend) {
            $priority = '0.7';
            $frequency = 'weekly';
        }

        //if only 1 condition and nav
        if ($withNav or $withPrice) {
            $priority = '0.5';
            $frequency = 'monthly';
        }

        //categories + brands
        if ($withCategory and $withBrand) {
            $priority = '0.9';
            $frequency = 'daily';
        }

        //categories + attribute - brand
        if ($withCategory and $withAttribute and !$withBrand) {
            $priority = '0.9';
            $frequency = 'daily';
        }

        //categories + brands + attributes
        if ($withCategory and $withBrand and $withAttribute) {
            $priority = '0.8';
            $frequency = 'weekly';
        }

        //categories + brands + nav - attributes
        if ($withCategory and $withBrand and $withNav and !$withAttribute) {
            $priority = '0.7';
            $frequency = 'weekly';
        }

        //categories + nav - brands
        if ($withCategory and !$withBrand and $withNav) {
            $priority = '0.8';
            $frequency = 'weekly';
        }

        //categories + nav + attributes - brands
        if ($withCategory and !$withBrand and $withNav and $withAttribute) {
            $priority = '0.7';
            $frequency = 'weekly';
        }

        //every combination with collection
        if ($withCollection) {
            $priority = '0.6';
            $frequency = 'monthly';
        }

        //only attribute
        if ($withAttribute
            and !$withBrand
            and !$withCollection
            and !$withNav
            and !$withCategory
            and !$withTrend
            and !$withPrice
        ) {
            $priority = '0.6';
            $frequency = 'monthly';
        }

        //prices
        if ($withPrice) {
            $priority = '0.5';
            $frequency = 'monthly';
        }

        //outlet
        if ($navIsOutlet) {
            $priority = '0.8';
            $frequency = 'weekly';
        }

        return [$frequency, $priority];
    }

}


class SitemapLink
{
    public $url;
    public $lastmod;
    public $frequency;
    public $priority;
    public $images = [];

    function __construct($url = null, $lastmod = null, $frequency = null, $priority = null)
    {
        $this->url = $url;
        $this->lastmod = $lastmod;
        $this->frequency = $frequency;
        $this->priority = $priority;
        return $this;
    }

    function addImage($url = null, $title = null, $caption = null)
    {
        $this->images[] = new SitemapImage($url, $title, $caption);
    }

    function toXml()
    {
        $template = '<url><loc>%s</loc><lastmod>%s</lastmod><changefreq>%s</changefreq><priority>%s</priority>%s</url>';
        $url = \Utils::xml_entities($this->url);
        $images = '';
        foreach ($this->images as $img) {
            $images .= $img->toXml();
        }
        $ret = sprintf($template, $url, $this->lastmod, $this->frequency, $this->priority, $images);
        $template = $url = null;
        return $ret;
    }


}


class SitemapImage
{
    public $url;
    public $title;
    public $caption;

    function __construct($url = null, $title = null, $caption = null)
    {
        $this->url = $url;
        $this->title = $title;
        $this->caption = $caption;
        return $this;
    }

    function toXml()
    {
        $template = '<image:image><image:loc>%s</image:loc>%s</image:image>';
        $url = \Utils::xml_entities($this->url);
        $extra = '';
        $extra .= ($this->title != '') ? '<image:title>' . htmlspecialchars($this->title, ENT_QUOTES) . '</image:title>' : '';
        $extra .= ($this->caption != '') ? '<image:caption>' . htmlspecialchars($this->title, ENT_QUOTES) . '</image:caption>' : '';
        return sprintf($template, $url, $extra);
    }

}
