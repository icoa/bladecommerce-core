<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * CustomerGroup
 *
 * @property integer $id
 * @property float $reduction
 * @property boolean $price_display_method
 * @property boolean $show_prices
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup whereReduction($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup wherePriceDisplayMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup whereShowPrices($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup whereUpdatedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class CustomerGroup extends MultiLangModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'customers_groups';
    protected $guarded = array();
    protected $softDelete = false;

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array('name', 'welcome_text', 'welcome_mail');
    protected $db_fields = array('reduction',
        'price_display_method',
        'show_prices',
        'code',
        'redirect',
        );
    protected $lang_model = 'CustomerGroup_Lang';
    protected $lang_fields_cloning = array(
        'name' => SingleModel::CLONE_UNIQUE_TEXT
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CustomerGroupPresenter($this);
    }

    /**
     * @param $code
     * @return self|null
     */
    public static function findByCode($code){
        $model = self::where('code', $code)->first();
        if($model){
            return self::getObj($model->id);
        }
        return null;
    }

    /**
     * @return string|null
     */
    public function getRedirectLinkAttribute(){
        try{
            if($this->redirect != ''){
                list($type, $id) = explode('|', $this->redirect);
                return \Link::to($type, $id);
            }
        }catch (Exception $e){

        }
        return null;
    }

}

class CustomerGroupPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}



/**
 * CustomerGroup_Lang
 *
 * @property integer $customer_group_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup_Lang whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\CustomerGroup_Lang whereName($value)
 * @property-read \Customer $customer
 */
class CustomerGroup_Lang extends Eloquent  {


    protected $table = 'customers_groups_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function customer()
    {
        return $this->belongsTo('Customer');
    }


}