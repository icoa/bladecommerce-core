<?php


/**
 * Email
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $sdesc
 * @property string $sender
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Email whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereSender($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Email whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Email extends MultiLangModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'emails';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("subject", "body", "sender_name", "sender_cc", "sender_bcc");
    protected $db_fields = array("sdesc", "active", "code", "name", "sender");
    protected $lang_model = 'Email_Lang';
    protected $db_fields_cloning = array(
        "active" => 0,
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "code" => SingleModel::CLONE_UNIQUE_TEXT,
    );
    public $has_published = false;

    protected $customer;

    /**
     * @param $code
     * @param string $lang_id
     * @return Email|null
     */
    static function getByCode($code, $lang_id = 'default')
    {
        $query = "SELECT id FROM emails WHERE UPPER(code)=?";
        $row = DB::selectOne($query, [\Str::upper($code)]);
        if ($row and $row->id) {
            return Email::getObj($row->id, $lang_id);
        }
        return null;
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Customer|null
     */
    public function getCustomer()
    {
        return (isset($this->customer) and $this->customer instanceof Customer) ? $this->customer : null;
    }

    /**
     * @param $data
     * @param $to
     * @param array $attachments
     */
    public function send($data, $to, $attachments = [])
    {
        $email = $this;
        $config = Cfg::getMultiple("OGP_IMAGE,DEFAULT_LOGO_ALT,DEFAULT_LOGO_TITLE,SITE_TITLE");
        $logo = public_path($config['OGP_IMAGE']);
        if (!file_exists($logo)) {
            $logo = null;
        }
        $data['SITE'] = $config['SITE_TITLE'];

        //bind customer data
        $customer = $this->getCustomer();
        if($customer){
            $data['MAIL_CUSTOMER_CARD_CODE'] = $customer->card_code;
            $data['MAIL_CUSTOMER_BARCODE_IMG'] = $customer->card_barcode_url;
            $data['MAIL_CUSTOMER_CARD_HTML'] = $customer->card_html;
            $data['MAIL_CUSTOMER_NAME'] = $customer->name;
            $data['MAIL_CUSTOMER_EMAIL'] = $customer->email;
            $data['MAIL_CUSTOMER_ACCOUNT_PANEL_URL'] = $customer->account_panel_url;
        }

        $url = Link::absolute()->shortcut('home');
        $info = Core::getShopInfo();

        $footer = $info['name'] . '<br>' . strip_tags($info['full_address']) . '<br>' . $info['full_phone'] . ' ' . $info['details'];

        $mail_data = ['body' => $this->parse($this->body, $data), 'logo' => $logo, 'footer' => $footer, 'url' => $url];
        $subject = $this->parse($this->subject, $data);

        if (App::environment() == 'local') {
            $email->sender_cc = '';
            $email->sender_bcc = '';
        }
        try {
            Mail::send('emails.mail', $mail_data, function ($message) use ($to, $email, $subject, $attachments) {
                $message->from($email->sender, $email->sender_name);
                if (Str::contains($to, ',')) {
                    $recipients = explode(',', $to);
                    foreach ($recipients as $recipient) {
                        if (Utils::isEmail($recipient)) {
                            $message->to($recipient)->subject($subject);
                        } else {
                            audit_error("$recipient is not a valid email recipient", __METHOD__);
                            audit_error($email, __METHOD__, 'Email');
                        }
                    }
                } else {
                    if (Utils::isEmail($to)) {
                        $message->to($to)->subject($subject);
                    } else {
                        audit_error("$to is not a valid email recipient", __METHOD__);
                        audit_error($email, __METHOD__, 'Email');
                    }
                }

                if ($email->sender_cc != '') {
                    $recipients = explode(',', $email->sender_cc);
                    foreach ($recipients as $recipient) {
                        if (Utils::isEmail($recipient)) {
                            $message->cc($recipient);
                        } else {
                            audit_error("$recipient is not a valid email recipient", __METHOD__);
                            audit_error($email, __METHOD__, 'Email');
                        }
                    }
                }
                if ($email->sender_bcc != '') {
                    $recipients = explode(',', $email->sender_bcc);
                    foreach ($recipients as $recipient) {
                        if (Utils::isEmail($recipient)) {
                            $message->bcc($recipient);
                        } else {
                            audit_error("$recipient is not a valid email recipient", __METHOD__);
                            audit_error($email, __METHOD__, 'Email');
                        }
                    }
                }
                $replyTo = config('mail.replyTo');
                if ($replyTo and \Utils::isEmail($replyTo)) {
                    $message->replyTo($replyTo);
                }
                if (!empty($attachments)) {
                    foreach ($attachments as $attachment) {
                        if (File::exists($attachment['file'])) {
                            $message->attach($attachment['file'], ['as' => $attachment['as']]);
                        }
                    }
                }
            });
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }

    }

    /**
     * @param $data
     */
    public function autosend($data)
    {
        return $this->send($data, $this->sender);
    }

    /**
     * @param $string
     * @param $data
     * @return mixed
     */
    private function parse($string, $data)
    {
        foreach ($data as $key => $val) {
            $pattern = "[" . \Str::upper(trim($key)) . "]";
            $string = str_replace($pattern, $val, $string);
        }
        return $string;
    }


}


/**
 * Email_Lang
 *
 * @property integer $email_id
 * @property integer $shop_id
 * @property string $lang_id
 * @property string $subject
 * @property string $body
 * @property string $sender_name
 * @property string $sender_cc
 * @property string $sender_bcc
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereEmailId($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereSubject($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereBody($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereSenderName($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereSenderCc($value)
 * @method static \Illuminate\Database\Query\Builder|\Email_Lang whereSenderBcc($value)
 * @property-read \Email $email
 */
class Email_Lang extends Eloquent
{


    protected $table = 'emails_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function email()
    {
        return $this->belongsTo('Email');
    }


}