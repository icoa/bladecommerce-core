<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Traits\HasParam;

/**
 * CartRule
 *
 * @property integer $id
 * @property string $date_from
 * @property string $date_to
 * @property string $sdesc
 * @property integer $quantity
 * @property integer $quantity_per_user
 * @property integer $priority
 * @property boolean $stop_other_rules
 * @property boolean $has_coupon
 * @property string $code
 * @property boolean $country_restriction
 * @property boolean $carrier_restriction
 * @property boolean $group_restriction
 * @property boolean $shop_restriction
 * @property boolean $visible
 * @property string $conditions
 * @property string $actions
 * @property boolean $active
 * @property boolean $highlight
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereDateFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereDateTo($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereQuantityPerUser($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereStopOtherRules($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereHasCoupon($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereCountryRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereCarrierRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereGroupRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereShopRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereConditions($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereActions($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereHighlight($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule wherePosition($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @method static \CartRule active()
 * @method static \CartRule available()
 * @property string $params 
 * @property boolean $repeatable 
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule whereRepeatable($value)
 */
class CartRule extends MultiLangModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'cart_rules';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "description", "image_list", "image_content", "carttext");
    protected $db_fields = array(
        "sdesc",
        "date_from",
        "date_to",
        "active",
        "position",
        "quantity",
        "quantity_per_user",
        "priority",
        "code",
        "country_restriction",
        "carrier_restriction",
        "group_restriction",
        //"shop_restriction",
        "visible",
        "highlight",
        "stop_other_rules",
        "conditions",
        "actions",
        "has_coupon",
        "repeatable",
        "params",
        "campaign_id",
        "campaign_name",
    );


    protected $lang_model = 'CartRule_Lang';
    protected $db_fields_cloning = array(
        "active" => 0,
        "code" => ""
    );
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;

    use HasParam;

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new CartRulePresenter($this);
    }

    function _after_clone(&$source, &$model)
    {
        \Utils::watch();
        $source_id = $source->id;
        $cart_rule_id = $model->id;

        //cloning rules carrier
        $rows = \DB::table("cart_rules_carrier")->where("cart_rule_id", $source_id)->get();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $carrier_id = $row->carrier_id;
                \DB::table("cart_rules_carrier")->insert(compact("cart_rule_id", "carrier_id"));
            }
        }

        //cloning rules country
        $rows = \DB::table("cart_rules_country")->where("cart_rule_id", $source_id)->get();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $country_id = $row->country_id;
                \DB::table("cart_rules_country")->insert(compact("cart_rule_id", "country_id"));
            }
        }

        //cloning rules group
        $rows = \DB::table("cart_rules_group")->where("cart_rule_id", $source_id)->get();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $group_id = $row->group_id;
                \DB::table("cart_rules_group")->insert(compact("cart_rule_id", "group_id"));
            }
        }

        \Utils::unwatch();

    }


    function _before_delete()
    {
        if ($this->forceDeleting == false) {
            return;
        }
        $cart_rule_id = $this->id;
        \Utils::watch();
        \DB::table("cart_rules_carrier")->where("cart_rule_id", $cart_rule_id)->delete();
        \DB::table("cart_rules_country")->where("cart_rule_id", $cart_rule_id)->delete();
        \DB::table("cart_rules_group")->where("cart_rule_id", $cart_rule_id)->delete();
        \Utils::unwatch();
    }


    public function rebindAttributes()
    {
        $this->date_from = Format::datetime($this->date_from);
        $this->date_to = Format::datetime($this->date_to);
    }

    /**
     * @param Builder $builder
     */
    public function scopeActive(Builder $builder)
    {
        $builder->where('active', 1);
    }

    /**
     * @param Builder $builder
     */
    public function scopeAvailable(Builder $builder)
    {
        $now = Carbon::now()->format('Y-m-d H:i:s');
        $builder->where(function ($query) use ($now) {
            $query->where('date_from', '>=', $now)->orWhereNull('date_from');
        })->where(function ($query) use ($now) {
            $query->where('date_to', '<=', $now)->orWhereNull('date_to');
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function campaign(){
        return $this->belongsTo(Campaign::class);
    }


    /**
     * @return Campaign|null
     */
    function getCampaignEntity()
    {
        if (isset($this->_campaign))
            return $this->_campaign;

        $this->_campaign = Campaign::getObj($this->campaign_id);
        return $this->_campaign;
    }


}

class CartRulePresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}


/**
 * CartRule_Lang
 *
 * @property integer $cart_rule_id
 * @property string $lang_id
 * @property string $name
 * @property string $description
 * @property string $carttext
 * @property string $image_list
 * @property string $image_content
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereCartRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereCarttext($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereImageList($value)
 * @method static \Illuminate\Database\Query\Builder|\CartRule_Lang whereImageContent($value)
 * @property-read \CartRule $cartrule
 */
class CartRule_Lang extends Eloquent
{


    protected $table = 'cart_rules_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function cartrule()
    {
        return $this->belongsTo('CartRule');
    }


}