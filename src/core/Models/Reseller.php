<?php

use Illuminate\Console\Command;
use services\Morellato\Geo\AddressResolver;

/**
 * Reseller
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $country_id
 * @property string $country_text
 * @property string $province
 * @property string $city
 * @property string $region
 * @property string $address
 * @property string $email
 * @property float $latitude
 * @property float $longitude
 * @property string $telephone
 * @property integer $brand
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $map
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereCountryText($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereProvince($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereTelephone($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereBrand($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereDeletedAt($value)
 * @property string $link
 * @method static \Illuminate\Database\Query\Builder|\Reseller whereLink($value)
 */
class Reseller extends SingleModel
{
    protected $table = 'resellers';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    public $db_fields = array(
        "code",
        "country_id",
        "company_name",
        "country_text",
        "province",
        "localization",
        "region",
        "address",
        "mail",
        "lat",
        "lng",
        "telephone",
        "link"
    );

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    private function getCountryId($nation)
    {

        if (strlen($nation) == 2) {
            $country = \Country::where('iso_code', $nation)->first();
            return isset($country) ? $country->id : false;
        } elseif (strlen($nation) == 2) {
            $this->console->comment(substr($nation, 0, 1));
            $country = \Country::where('iso_code', substr($nation, 0, 1))->first();

            return isset($country) ? $country->id : false;
        } else {
            $country = \Country::rows('it')->where('name', $nation)->first();

            return isset($country) ? $country->country_id : false;
        }
    }

    function _before_create()
    {
        $this->country_id = $this->getCountryId($this->country_text);
    }

    function resolveAddress(Command $console)
    {
        if ((float)($this->latitude) == 0 or (float)($this->longitude) == 0) {
            $address = $this->getFullAddress();
            $console->line("Resolving address [$address]");
            $service = new AddressResolver();
            $geo = $service->make($address);
            if ($geo and isset($geo['lat']) and isset($geo['long'])) {
                $this->latitude = $geo['lat'];
                $this->longitude = $geo['long'];
                $this->save(['timestamps' => false]);
                return true;
            }
            //try only with the city
            $geo = $service->make($this->city);
            $console->line("Resolving city [$this->city]");
            if ($geo and isset($geo['lat']) and isset($geo['long'])) {
                $this->latitude = $geo['lat'];
                $this->longitude = $geo['long'];
                $this->save(['timestamps' => false]);
                return true;
            }
        }
        return false;
    }

    function getFullAddress()
    {
        $tokens = [];
        if (strlen(trim($this->address)) > 0) {
            $tokens[] = $this->address;
        }
        if (strlen(trim($this->city)) > 0) {
            $tokens[] = $this->city;
        }
        return implode(' ', $tokens);
    }


    function getMapAttribute()
    {
        $address = $this->getFullAddress();
        $address = urlencode($address);
        return "https://www.google.it/maps/place/$address/@{$this->latitude},{$this->longitude},16z";
    }
}