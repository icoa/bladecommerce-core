<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Gender
 *
 * @property boolean $id
 * @property boolean $active
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Gender whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Gender whereActive($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Gender extends MultiLangModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'genders';
    protected $guarded = array();

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array(
        "name",
    );
    protected $db_fields = array(
        "active",
    );    

    protected $lang_model = 'Gender_Lang';
}

/**
 * Gender_Lang
 *
 * @property boolean $gender_id
 * @property string $lang_id
 * @property string $name
 * @property string $formal
 * @method static \Illuminate\Database\Query\Builder|\Gender_Lang whereGenderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Gender_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Gender_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Gender_Lang whereFormal($value)
 * @property-read \Gender $gender
 */
class Gender_Lang extends Eloquent  {


    protected $table = 'genders_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function gender()
    {
        return $this->belongsTo('Gender');
    }

}
