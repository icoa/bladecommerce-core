<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Fees\Traits\TraitFeesPayment;

/**
 * Payment
 *
 * @property integer $id
 * @property string $sdesc
 * @property boolean $active
 * @property float $price
 * @property boolean $online
 * @property boolean $is_default
 * @property string $module
 * @property integer $payment_status
 * @property integer $position
 * @property string $logo
 * @property boolean $carrier_restriction
 * @property boolean $country_restriction
 * @property boolean $group_restriction
 * @property boolean $shop_restriction
 * @property string $options
 * @property integer $email_ok
 * @property integer $email_ko
 * @property integer $email_wait
 * @property boolean $receipt
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @property-read mixed $fees_reason
 * @method static \Illuminate\Database\Query\Builder|\Payment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereOnline($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereModule($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment wherePaymentStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereCarrierRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereCountryRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereGroupRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereShopRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereOptions($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereEmailOk($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereEmailKo($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereEmailWait($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereReceipt($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read mixed $module_name
 */
class Payment extends MultiLangModel implements PresentableInterface
{

    use TraitFeesPayment;

    const TYPE_PAYPAL = 1;
    const TYPE_CREDITCARD = 2;
    const TYPE_BANKTRANSFER = 3;
    const TYPE_CASH = 4;
    const TYPE_SHIPPING_POINT = 6;
    const TYPE_FAKE1 = 7;
    const TYPE_FAKE2 = 8;
    const TYPE_FAKE3 = 9;
    const TYPE_FAKE4 = 10;
    const TYPE_SHOP_ANTICIPATED = 11;
    const TYPE_WITHDRAWAL = 12;
    const TYPE_GIFTCARD = 13;
    const TYPE_RIBA = 14;

    const MODULE_PAYPAL = 'paypal';
    const MODULE_CREDIT_CARD = 'safepay';
    const MODULE_BANKWIRE = 'bankwire';
    const MODULE_CASHONDELIVERY = 'cash';
    const MODULE_SHOP_PICK = 'pick';
    const MODULE_SHOP_PREPAID = 'premature';
    const MODULE_SHOP_POSTPAID = 'postpone';
    const MODULE_GIFTCARD = 'giftcard';
    const MODULE_RIBA = 'riba';

    /* LARAVEL PROPERTIES */

    protected $table = 'payments';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "carttext", "aftertext");
    protected $db_fields = array("payment_status", "sdesc", "active", "position", "price", "online", "is_default", "logo", "carrier_restriction", "country_restriction", "group_restriction", "shop_restriction", "options", "email_ok", "email_ko", "email_wait");
    protected $lang_model = 'Payment_Lang';
    protected $db_fields_cloning = array(
        "active" => 0,
    );
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
    );
    public $has_published = false;

    /**
     * @return array
     */
    static function moduleToArray()
    {
        return [
            self::MODULE_PAYPAL => 'PAYPAL',
            self::MODULE_CREDIT_CARD => 'CREDIT_CARD',
            self::MODULE_BANKWIRE => 'BANKWIRE',
            self::MODULE_CASHONDELIVERY => 'CASHONDELIVERY',
            self::MODULE_SHOP_PICK => 'SHOP_PICK',
            self::MODULE_SHOP_PREPAID => 'SHOP_PREPAID',
            self::MODULE_SHOP_POSTPAID => 'SHOP_POSTPAID',
            self::MODULE_GIFTCARD => 'GIFTCARD',
            self::MODULE_RIBA => 'RIBA',
        ];
    }

    /**
     * @return array
     */
    static function statusToArray()
    {
        return [
            self::TYPE_PAYPAL => self::MODULE_PAYPAL,
            self::TYPE_CREDITCARD => self::MODULE_CREDIT_CARD,
            self::TYPE_BANKTRANSFER => self::MODULE_BANKWIRE,
            self::TYPE_CASH => self::MODULE_CASHONDELIVERY,
            self::TYPE_SHIPPING_POINT => self::MODULE_SHOP_PICK,
            self::TYPE_SHOP_ANTICIPATED => self::MODULE_SHOP_PREPAID,
            self::TYPE_WITHDRAWAL => self::MODULE_SHOP_POSTPAID,
            self::TYPE_GIFTCARD => self::MODULE_GIFTCARD,
            self::TYPE_RIBA => self::MODULE_RIBA,
        ];
    }

    /**
     * @return array
     */
    static function statusToModule()
    {
        $modules = self::moduleToArray();
        return [
            self::TYPE_PAYPAL => $modules[self::MODULE_PAYPAL],
            self::TYPE_CREDITCARD => $modules[self::MODULE_CREDIT_CARD],
            self::TYPE_BANKTRANSFER => $modules[self::MODULE_BANKWIRE],
            self::TYPE_CASH => $modules[self::MODULE_CASHONDELIVERY],
            self::TYPE_SHIPPING_POINT => $modules[self::MODULE_SHOP_PICK],
            self::TYPE_SHOP_ANTICIPATED => $modules[self::MODULE_SHOP_PREPAID],
            self::TYPE_WITHDRAWAL => $modules[self::MODULE_SHOP_POSTPAID],
            self::TYPE_GIFTCARD => $modules[self::MODULE_GIFTCARD],
            self::TYPE_RIBA => $modules[self::MODULE_RIBA],
        ];
    }

    /**
     * @return mixed
     */
    function getModuleNameAttribute()
    {
        $names = self::moduleToArray();
        if(array_key_exists($this->module, $names))
            return $names[$this->module];
        return 'Undefined';
    }

    /**
     * @param $status
     * @return int
     */
    static function statusToInt($status){
        $array = self::statusToModule();
        foreach($array as $key => $value){
            if($value === $status)
                return $key;
        }
        return 0;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new PaymentPresenter($this);
    }

    function _after_clone(&$source, &$model)
    {
        \Utils::watch();
        $source_id = $source->id;
        $target_id = $model->id;

    }


    function _before_delete()
    {
        if ($this->forceDeleting == false) {
            return;
        }
        $id = $this->id;
    }

    /**
     * @return null|string
     */
    function hasGateway()
    {
        $file = app_path() . "/gateways/$this->module/gateway.{$this->module}.php";

        if ($this->module != '' AND \File::exists($file)) {
            return $file;
        }
        return null;
    }

    /**
     * @return \Frontend\Gateway\Process|null
     */
    function gateway()
    {
        $gateway = null;
        if ($file = $this->hasGateway()) {
            require_once($file);
            try {
                $class_name = "\Frontend\Gateway\\" . ucfirst($this->module);
                $gateway = new $class_name($this->id);
            } catch (\Exception $e) {
                $class_name = "\Echo1\Gateway\\" . ucfirst($this->module);
                $gateway = new $class_name($this->id);
            }
        } else {
            $gateway = new \Frontend\Gateway\Process($this->id);
        }
        return $gateway;
    }

    /**
     * @return object
     */
    function getOptions()
    {
        return (object)unserialize($this->options);
    }


}

class PaymentPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}


/**
 * Payment_Lang
 *
 * @property integer $payment_id
 * @property integer $shop_id
 * @property string $lang_id
 * @property string $name
 * @property string $carttext
 * @property string $aftertext
 * @method static \Illuminate\Database\Query\Builder|\Payment_Lang wherePaymentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment_Lang whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment_Lang whereCarttext($value)
 * @method static \Illuminate\Database\Query\Builder|\Payment_Lang whereAftertext($value)
 * @property-read \Payment $payment 
 */
class Payment_Lang extends Eloquent
{


    protected $table = 'payments_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function payment()
    {
        return $this->belongsTo('Payment');
    }


}
