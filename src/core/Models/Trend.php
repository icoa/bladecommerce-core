<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Trend
 *
 * @property integer $id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Trend whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend wherePosition($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class Trend extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'trends';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "ldesc", "image_default", "image_thumb", "image_zoom", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical", "head", "footer");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("position","robots", "ogp", "ogp_type", "ogp_image", "public_access");
    protected $lang_model = 'Trend_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    protected $defaults = [
        'robots' => 'global',
        'ogp' => '0',
        'ogp_type' => 'product',
        'public_access' => 'P',
    ];

    protected function getDefaults(){
        $defaults = $this->defaults;
        $defaults['position'] = Brand::max('position') + 1;
        return $defaults;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new TrendPresenter($this);
    }

}

class TrendPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}



/**
 * Trend_Lang
 *
 * @property integer $trend_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @property string $canonical
 * @property string $metafacebook
 * @property string $metatwitter
 * @property string $metagoogle
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereTrendId($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereImageZoom($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Trend_Lang whereFooter($value)
 * @property-read \Trend $trend
 */
class Trend_Lang extends Model_Lang  {


    protected $table = 'trends_lang';


    public function trend()
    {
        return $this->belongsTo('Trend');
    }


}
