<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * MagicTokenTemplate
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate wherePosition($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class MagicTokenTemplate extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'magic_tokens_templates';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("content");
    
    protected $db_fields = array("name","position","active");
    protected $lang_model = 'MagicTokenTemplate_Lang';

    
    protected $db_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,        
        "active" => 0,
    );
    

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new MagicTokenTemplatePresenter($this);
    }

}

class MagicTokenTemplatePresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}


/**
 * MagicTokenTemplate_Lang
 *
 * @property integer $magic_token_template_id
 * @property string $lang_id
 * @property string $content
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate_Lang whereMagicTokenTemplateId($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\MagicTokenTemplate_Lang whereContent($value)
 * @property-read \MagicTokenTemplate $seoblock
 */
class MagicTokenTemplate_Lang extends Eloquent  {


    protected $table = 'magic_tokens_templates_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function seoblock()
    {
        return $this->belongsTo('MagicTokenTemplate');
    }


}