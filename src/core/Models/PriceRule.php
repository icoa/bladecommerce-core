<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Traits\HasParam;

/**
 * PriceRule
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $date_from
 * @property string $date_to
 * @property string $sdesc
 * @property integer $priority
 * @property boolean $stop_other_rules
 * @property integer $from_quantity
 * @property boolean $currency_restriction
 * @property boolean $country_restriction
 * @property boolean $group_restriction
 * @property boolean $product_restriction
 * @property boolean $shop_restriction
 * @property boolean $affiliate_restriction
 * @property string $reduction_type
 * @property boolean $reduction_target
 * @property float $reduction_value
 * @property integer $reduction_currency
 * @property integer $reduction_tax
 * @property boolean $free_shipping
 * @property string $conditions
 * @property array $params
 * @property boolean $active
 * @property boolean $visible
 * @property integer $position
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property boolean $global
 * @property boolean $repeatable
 * @property integer $affiliate_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereDateFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereDateTo($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule wherePriority($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereStopOtherRules($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereFromQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereCurrencyRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereCountryRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereGroupRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereProductRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereShopRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereAffiliateRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereReductionType($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereReductionTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereReductionValue($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereReductionCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereReductionTax($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereFreeShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereConditions($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereGlobal($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule whereRepeatable($value)
 */
class PriceRule extends MultiLangModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'price_rules';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;

    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array(
        "name",
        "description",
        "image_list",
        "image_content",
        "label",
        "slug",
        "metatitle",
        "metakeywords",
        "metadescription",
        "canonical",
        "h1",
        "metafacebook",
        "metagoogle",
        "metatwitter",
        "head",
        "footer",
    );
    protected $db_fields = array(
        "sdesc",
        "date_from",
        "date_to",
        "active",
        "visible",
        "from_quantity",
        "currency_restriction",
        "country_restriction",
        "group_restriction",
        "affiliate_restriction",
        "free_shipping",
        "priority",
        "stop_other_rules",
        "reduction_type",
        "reduction_value",
        "reduction_currency",
        "reduction_tax",
        "reduction_target",
        "conditions",
        "robots",
        "ogp",
        "ogp_type",
        "ogp_image",
        "position",
        "global",
        "affiliate_id",
        "repeatable",
        "params",
    );
    protected $lang_model = 'PriceRule_Lang';
    protected $db_fields_cloning = array(
        "active" => 0
    );
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG
    );
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    public $has_published = false;

    use HasParam;


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new PriceRulePresenter($this);
    }

    function _after_clone(&$source, &$model)
    {
        \Utils::watch();
        $source_id = $source->id;
        $price_rule_id = $model->id;

        //cloning rules currency
        $rows = \DB::table("price_rules_currency")->where("price_rule_id", $source_id)->get();
        if (!empty($rows)) {
            DB::beginTransaction();
            foreach ($rows as $row) {
                $currency_id = $row->currency_id;
                \DB::table("price_rules_currency")->insert(compact("price_rule_id", "currency_id"));
            }
            DB::commit();
        }

        //cloning rules country
        $rows = \DB::table("price_rules_country")->where("price_rule_id", $source_id)->get();
        if (!empty($rows)) {
            DB::beginTransaction();
            foreach ($rows as $row) {
                $country_id = $row->country_id;
                \DB::table("price_rules_country")->insert(compact("price_rule_id", "country_id"));
            }
            DB::commit();
        }

        //cloning rules group
        $rows = \DB::table("price_rules_group")->where("price_rule_id", $source_id)->get();
        if (!empty($rows)) {
            DB::beginTransaction();
            foreach ($rows as $row) {
                $group_id = $row->group_id;
                \DB::table("price_rules_group")->insert(compact("price_rule_id", "group_id"));
            }
            DB::commit();
        }

        //cloning specific price
        $rows = \ProductPrice::where("price_rule_id", $source_id)->get(['id']);
        if (!empty($rows)) {
            foreach ($rows as $row) {
                DB::beginTransaction();
                $obj = \ProductPrice::find($row->id);
                $ProductPrice = $obj->replicate();
                $ProductPrice->price_rule_id = $price_rule_id;
                $ProductPrice->save();
                DB::commit();
            }
        }


        \Utils::unwatch();

    }


    function _before_delete()
    {
        if ($this->forceDeleting == false) {
            return;
        }
        $price_rule_id = $this->id;
        \Utils::watch();
        \DB::table("price_rules_currency")->where("price_rule_id", $price_rule_id)->delete();
        \DB::table("price_rules_country")->where("price_rule_id", $price_rule_id)->delete();
        \DB::table("price_rules_group")->where("price_rule_id", $price_rule_id)->delete();
        \DB::table("products_specific_prices")->where("price_rule_id", $price_rule_id)->delete();
        \Utils::unwatch();
    }


    public function rebindAttributes()
    {
        $this->date_from = Format::datetime($this->date_from);
        $this->date_to = Format::datetime($this->date_to);
    }

    public function getRuleExceptions()
    {
        $price_rule_id = $this->id;
        $model = $this;

        $currencies = [];
        $countries = [];
        $groups = [];

        if ($model->currency_restriction == 1) {
            $currencies = \Currency::where('active',1)->whereNotIn("id", function ($query) use ($price_rule_id) {
                $query->select(['currency_id'])
                    ->from('price_rules_currency')
                    ->where('price_rule_id', $price_rule_id);
            })->lists('id');
        }

        if ($model->country_restriction == 1) {
            $countries = \Country::where('active',1)->whereNotIn("id", function ($query) use ($price_rule_id) {
                $query->select(['country_id'])
                    ->from('price_rules_country')
                    ->where('price_rule_id', $price_rule_id);
            })->lists('id');
        }

        if ($model->group_restriction == 1) {
            $groups = \CustomerGroup::whereNotIn("id", function ($query) use ($price_rule_id) {
                $query->select(['group_id'])
                    ->from('price_rules_group')
                    ->where('price_rule_id', $price_rule_id);
            })->lists('id');
        }

        $rules = [];
        if (count($currencies) == 0 AND count($countries) == 0 AND count($groups) == 0) {
            $obj = new \stdClass();
            $obj->customer_group_id = 0;
            $obj->currency_id = 0;
            $obj->country_id = 0;
            $rules[] = $obj;
        }

        if (count($currencies) > 0 AND count($countries) == 0 AND count($groups) == 0) {
            foreach ($currencies as $id) {
                $obj = new \stdClass();
                $obj->customer_group_id = 0;
                $obj->currency_id = $id;
                $obj->country_id = 0;
                $rules[] = $obj;
            }
        }

        if (count($currencies) == 0 AND count($countries) > 0 AND count($groups) == 0) {
            foreach ($countries as $id) {
                $obj = new \stdClass();
                $obj->customer_group_id = 0;
                $obj->currency_id = 0;
                $obj->country_id = $id;
                $rules[] = $obj;
            }
        }

        if (count($currencies) == 0 AND count($countries) == 0 AND count($groups) > 0) {
            foreach ($groups as $id) {
                $obj = new \stdClass();
                $obj->customer_group_id = $id;
                $obj->currency_id = 0;
                $obj->country_id = 0;
                $rules[] = $obj;
            }
        }

        if (count($currencies) > 0 AND count($countries) > 0 AND count($groups) == 0) {
            foreach ($currencies as $id) {
                foreach ($countries as $id2) {
                    $obj = new \stdClass();
                    $obj->customer_group_id = 0;
                    $obj->currency_id = $id;
                    $obj->country_id = $id2;
                    $rules[] = $obj;
                }
            }
        }

        if (count($currencies) > 0 AND count($countries) == 0 AND count($groups) > 0) {
            foreach ($currencies as $id) {
                foreach ($groups as $id2) {
                    $obj = new \stdClass();
                    $obj->customer_group_id = $id2;
                    $obj->currency_id = $id;
                    $obj->country_id = 0;
                    $rules[] = $obj;
                }
            }
        }

        if (count($currencies) == 0 AND count($countries) > 0 AND count($groups) > 0) {
            foreach ($countries as $id) {
                foreach ($groups as $id2) {
                    $obj = new \stdClass();
                    $obj->customer_group_id = $id2;
                    $obj->currency_id = 0;
                    $obj->country_id = $id;
                    $rules[] = $obj;
                }
            }
        }

        if (count($currencies) > 0 AND count($countries) > 0 AND count($groups) > 0) {
            foreach ($countries as $id) {
                foreach ($groups as $id2) {
                    foreach ($currencies as $id3) {
                        $obj = new \stdClass();
                        $obj->customer_group_id = $id2;
                        $obj->currency_id = $id3;
                        $obj->country_id = $id;
                        $rules[] = $obj;
                    }
                }
            }
        }

        \Utils::log($rules, "RULES TO BUILD");
        return $rules;
    }

    public function applyRuleForProduct($product_id)
    {

        $price_rule_id = $this->id;
        $rules = $this->getRuleExceptions();
        $model = $this;

        $position = 0;
        if (count($rules) > 0) {
            $position++;
            //$position = \ProductPrice::where("product_id", $product_id)->max('position');

            foreach ($rules as $rule) {
                $PriceRule = new \ProductPrice();
                $PriceRule->product_id = $product_id;
                $PriceRule->price_rule_id = $price_rule_id;

                $PriceRule->customer_group_id = $rule->customer_group_id;
                $PriceRule->currency_id = $rule->currency_id;
                $PriceRule->country_id = $rule->country_id;

                $PriceRule->date_from = $model->date_from;
                $PriceRule->date_to = $model->date_to;
                $PriceRule->from_quantity = $model->from_quantity;
                $PriceRule->position = ($model->priority * 10) + ($position);
                $PriceRule->reduction_type = $model->reduction_type;
                $PriceRule->reduction_value = $model->reduction_value;
                $PriceRule->reduction_currency = $model->reduction_currency;
                $PriceRule->reduction_target = $model->reduction_target;
                $PriceRule->reduction_tax = $model->reduction_tax;
                $PriceRule->stop_other_rules = $model->stop_other_rules;
                $PriceRule->free_shipping = $model->free_shipping;
                $PriceRule->global = $model->global;
                $PriceRule->affiliate_id = $model->affiliate_id;
                $PriceRule->repeatable = $model->repeatable;

                $PriceRule->save();
                unset($PriceRule);
            }
        }
    }


    public static function getSlug($id, $lang_id)
    {
        return "promotion/" . parent::getSlug($id, $lang_id);
    }

    function expand()
    {
        //$link = Site::root()."/promotion/".$this->slug;
        $link = Link::to('promo',$this->id);
        $imageListFile = ($this->image_list != '') ? public_path($this->image_list) : null;
        $imageContentFile = ($this->image_content != '') ? public_path($this->image_content) : null;
        $hasImageList = \File::exists($imageListFile);
        $hasImageContent = \File::exists($imageContentFile);
        $from = Format::lang_date($this->date_from);
        $to = Format::lang_date($this->date_to);
        $period = '';
        if ($from != null AND $to != null) {
            $period = trans('template.valid_from_to',['from' => $from, 'to' => $to]);
        }
        if ($from == null AND $to != null) {
            $period = trans('template.valid_to',['from' => $from, 'to' => $to]);
        }
        if ($from != null AND $to == null) {
            $period = trans('template.valid_from',['from' => $from, 'to' => $to]);
        }

        $attributes = [
            'link' => $link,
            'hasImageList' => $hasImageList,
            'hasImageContent' => $hasImageContent,
            'period' => $period,
        ];
        $this->setAttributes($attributes);
        return $this;
    }

    function getProducts(){
        $page = \Input::get('page',1);
        $pagesize = floor( \Cfg::get('PRODUCTS_PER_PAGE') / 4 );
        $lang = \Core::getLang();
        $queryCount = "SELECT count(id) as aggregate FROM cache_products WHERE published_{$lang}=1 AND id IN (SELECT product_id FROM products_specific_prices WHERE active=1 AND price_rule_id=$this->id)";
        $total = DB::selectOne($queryCount)->aggregate;
        $totalPages = floor($total / $pagesize);
        $currentPage = $page;
        $start = ($page - 1) * $pagesize;
        $query = "SELECT id FROM cache_products WHERE published_{$lang}=1 AND id IN (SELECT product_id FROM products_specific_prices WHERE active=1 AND price_rule_id=$this->id) ORDER BY position LIMIT $start,$pagesize";
        $results = DB::select($query);
        $products = [];
        foreach($results as $result){
            $products[] = $result->id;
        }

        return compact('currentPage','total','totalPages','products','start');
        //$total = DB::table('cache_products')->where()
    }


}

class PriceRulePresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}



/**
 * PriceRule_Lang
 *
 * @property integer $price_rule_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $label
 * @property string $image_list
 * @property string $image_content
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $canonical
 * @property string $h1
 * @property string $metafacebook
 * @property string $metagoogle
 * @property string $metatwitter
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang wherePriceRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereImageList($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereImageContent($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\PriceRule_Lang whereFooter($value)
 * @property-read \PriceRule $pricerule
 */
class PriceRule_Lang extends Eloquent  {


    protected $table = 'price_rules_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function pricerule()
    {
        return $this->belongsTo('PriceRule');
    }


}