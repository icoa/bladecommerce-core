<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use Core\Condition\RuleResolver;

/**
 * SeoBlock
 *
 * @property integer $id
 * @property string $xtype
 * @property string $context
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property string $conditions
 * @property boolean $isDefault
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereXtype($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereContext($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereConditions($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock whereIsDefault($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class SeoBlock extends MultiLangModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'seo_blocks';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("content");
    protected $db_fields = array("xtype", "context", "position", "active", "conditions", "isDefault");
    protected $lang_model = 'SeoBlock_Lang';
    protected $db_fields_cloning = array(
        "active" => 0
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new SeoBlockPresenter($this);
    }

    function _after_clone(&$source, &$model) {
        $source_id = $source->id;
        $target_id = $model->id;

        /* CLONING RULES */
        $rules = DB::table('seo_blocks_rules')->where('seo_block_id', $source->id)->get();

        foreach ($rules as $c) {
            DB::table('seo_blocks_rules')->insert([
                'seo_block_id' => $model->id,
                'target_type' => $c->target_type,
                'target_mode' => $c->target_mode,
                'target_id' => $c->target_id,
                'position' => $c->position,
            ]);
        }
    }

    function solveByProduct($product){
        if($this->conditions == null OR trim($this->conditions) == ''){
            return false;
        }

        $rr = new RuleResolver();
        //$this->conditions = str_replace('site|','product|',$this->conditions);
        $rr->setRules($this->conditions);
        $assert = $rr->bindRules($product);
        return ($assert == 'true');
    }


    function solveByObject($obj){
        if($this->conditions == null OR trim($this->conditions) == ''){
            return false;
        }

        $rr = new RuleResolver();
        //$this->conditions = str_replace('site|','product|',$this->conditions);
        $rr->setRules($this->conditions);
        //\Utils::log($obj->toArray());
        $assert = $rr->bindRules($obj);
        return ($assert == 'true');
    }

}

class SeoBlockPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}



/**
 * SeoBlock_Lang
 *
 * @property integer $seo_block_id
 * @property string $lang_id
 * @property string $content
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock_Lang whereSeoBlockId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoBlock_Lang whereContent($value)
 * @property-read \SeoBlock $seoblock
 */
class SeoBlock_Lang extends Eloquent  {


    protected $table = 'seo_blocks_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function seoblock()
    {
        return $this->belongsTo('SeoBlock');
    }


}