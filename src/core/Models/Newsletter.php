<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Observers\NewsletterObserver;

/**
 * Newsletter
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $email
 * @property string $lang_id
 * @property string $user_ip
 * @property boolean $marketing
 * @property boolean $profile
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property boolean $created_by
 * @property boolean $updated_by
 * @property boolean $deleted_by
 * @property-read \Customer $customer
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereUserIp($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereMarketing($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereProfile($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Newsletter whereDeletedBy($value)
 */
class Newsletter extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'newsletters';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $db_fields = array(
        "email",
        "user_ip",
        "customer_id",
        "marketing",
        "profile",
        "lang_id",
    );

    public static function boot()
    {
        parent::boot();

        // Setup event bindings...
        self::observe(new NewsletterObserver());
    }

    // RELATIONS
    public function customer()
    {
        return $this->belongsTo('Customer', 'customer_id');
    }

    static function register($params = [])
    {
        if (!isset($params['email'])) {
            return false;
        }
        $email = $params['email'];
        $marketing = (isset($params['marketing'])) ? $params['marketing'] : 0;
        $profile = (isset($params['profile'])) ? $params['profile'] : 0;
        $customer_id = (isset($params['customer_id'])) ? $params['customer_id'] : 0;
        $user_ip = $_SERVER['REMOTE_ADDR'];
        $lang_id = (isset($params['lang_id'])) ? $params['lang_id'] : \Core::getLang();

        $record = \Newsletter::whereEmail($email)->first();
        if (!$record) {
            $record = new \Newsletter();
        } else {
            $user_ip = $record->user_ip;
            $lang_id = $record->lang_id;
        }
        $customer = Customer::whereEmail($email)->whereActive(1)->first();
        if ($customer) {
            $customer_id = $customer->id;
            $customer->newsletter = 1;
            $customer->ip_registration_newsletter = \Core::ip();
            $customer->newsletter_date_add = \Format::now();
            $customer->save();
        }
        $record->email = $email;
        $record->marketing = $marketing;
        $record->profile = $profile;
        $record->user_ip = $user_ip;
        $record->lang_id = $lang_id;
        $record->customer_id = $customer_id;
        $record->save();
        return $record ? $record->id : null;
    }

    static function unregister($email)
    {
        $record = \Newsletter::whereEmail($email)->first();
        if ($record) {
            $record->delete();
            $customer = Customer::whereEmail($email)->whereActive(1)->first();
            if ($customer) {
                $customer->newsletter = 0;
                $customer->save();
            }
            return true;
        }
        return false;
    }

}