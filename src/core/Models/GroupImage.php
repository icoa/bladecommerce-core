<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 28-nov-2013 18.05.30
 */

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * GroupImage
 *
 * @property integer $id
 * @property string $filename
 * @property integer $set_id
 * @property integer $position
 * @property boolean $active
 * @property integer $updated_by
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereSetId($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage whereCreatedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class GroupImage extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'images_groups';
    protected $guarded = array();
    protected $softDelete = false;
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("legend");
    protected $db_fields = array("filename","position","set_id");
    protected $lang_model = 'GroupImage_Lang';



    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new GroupImagePresenter($this);
    }

}

class GroupImagePresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}


/**
 * GroupImage_Lang
 *
 * @property integer $group_image_id
 * @property string $lang_id
 * @property string $legend
 * @method static \Illuminate\Database\Query\Builder|\GroupImage_Lang whereGroupImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\GroupImage_Lang whereLegend($value)
 * @property-read \GroupImage $image
 */
class GroupImage_Lang extends Eloquent  {


    protected $table = 'images_groups_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function image()
    {
        return $this->belongsTo('GroupImage');
    }


}