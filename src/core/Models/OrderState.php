<?php


/**
 * OrderState
 *
 * @property integer $id
 * @property boolean $invoice
 * @property boolean $send_email
 * @property string $module_name
 * @property string $icon
 * @property string $color
 * @property boolean $unremovable
 * @property boolean $hidden
 * @property boolean $logable
 * @property boolean $delivery
 * @property boolean $shipped
 * @property boolean $paid
 * @property boolean $active
 * @property boolean $deleted
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereInvoice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereSendEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereModuleName($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereIcon($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereUnremovable($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereLogable($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereDelivery($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereShipped($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState wherePaid($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState whereDeleted($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read mixed $code
 */
class OrderState extends MultiLangModel
{

    const STATUS_NEW = 1;                   // Nuovo
    const STATUS_PENDING = 2;               // In attesa
    const STATUS_WORKING = 3;               // In lavorazione
    const STATUS_SHIPPED = 4;               // Spedito
    const STATUS_CLOSED = 5;                // Chiuso
    const STATUS_CANCELED = 6;              // Annullato
    const STATUS_REFUNDED = 7;              // Rimborsato
    const STATUS_INVOICE = 8;               // Da fatturare
    const STATUS_OUTOFSTOCK = 9;            // In rifornimento
    const STATUS_REFUNDED_PARTIALLY = 10;   // Rimborsato parzialmente
    const STATUS_DELIVERED = 11;            // Consegnato
    const STATUS_WITHDRAWED = 12;           // Ritirato dal cliente (consegnato/chiuso)
    const STATUS_REJECTED = 13;             // Sostituito
    const STATUS_READY = 14;                // Pronto per il ritiro
    const STATUS_PENDING_DELIVERY = 15;     // In consegna
    const STATUS_CANCELED_AFFILIATE = 16;   // Annullato da affiliato
    const STATUS_CANCELED_SAP = 17;         // Annullato - non ritirato
    const STATUS_READY_PICKUP = 18;         // Pronto per il ritiro (in store)

    /* LARAVEL PROPERTIES */
    protected $table = 'order_states';
    protected $guarded = array();
    protected $blameable = [];
    public $timestamps = false;


    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    protected $db_fields = array("invoice", "send_email", "module_name", "color", "unremovable", "logable", "delivery", "shipped", "paid");
    protected $lang_model = 'OrderState_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;

    /**
     * @return array
     */
    static function statusToArray()
    {
        return [
            self::STATUS_NEW => 'STATUS_NEW',
            self::STATUS_PENDING => 'STATUS_PENDING',
            self::STATUS_WORKING => 'STATUS_WORKING',
            self::STATUS_SHIPPED => 'STATUS_SHIPPED',
            self::STATUS_CLOSED => 'STATUS_CLOSED',
            self::STATUS_CANCELED => 'STATUS_CANCELED',
            self::STATUS_REFUNDED => 'STATUS_REFUNDED',
            self::STATUS_INVOICE => 'STATUS_INVOICE',
            self::STATUS_OUTOFSTOCK => 'STATUS_OUTOFSTOCK',
            self::STATUS_REFUNDED_PARTIALLY => 'STATUS_REFUNDED_PARTIALLY',
            self::STATUS_DELIVERED => 'STATUS_DELIVERED',
            self::STATUS_WITHDRAWED => 'STATUS_WITHDRAWED',
            self::STATUS_REJECTED => 'STATUS_REJECTED',
            self::STATUS_READY => 'STATUS_READY',
            self::STATUS_PENDING_DELIVERY => 'STATUS_PENDING_DELIVERY',
            self::STATUS_CANCELED_AFFILIATE => 'STATUS_CANCELED_AFFILIATE',
            self::STATUS_CANCELED_SAP => 'STATUS_CANCELED_SAP',
            self::STATUS_READY_PICKUP => 'STATUS_READY_PICKUP',
        ];
    }

    /**
     * @param $status
     * @return string
     */
    static function statusToString($status)
    {
        $array = self::statusToArray();
        return array_key_exists($status, $array) ? $array[$status] : 'STATUS_UNKNOWN';
    }

    /**
     * @param $status
     * @return int
     */
    static function statusToInt($status)
    {
        $array = self::statusToArray();
        foreach ($array as $key => $value) {
            if ($value === $status)
                return $key;
        }
        return 0;
    }

    function getName($formatted = false, $icon = 'font')
    {
        $record = $this;
        if ($record == null) {
            return null;
        }
        if ($formatted) {
            $icon = str_replace('font', $icon, $record->icon);
            $str = "<span class='label' style='background-color: $record->color'><i class='fa $icon'></i> $record->name</span>";
        } else {
            $str = $record->name;
        }
        return $str;
    }

    /**
     * @return string
     */
    function getCodeAttribute()
    {
        return self::statusToString($this->id);
    }

}

/**
 * OrderState_Lang
 *
 * @property integer $order_state_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\OrderState_Lang whereOrderStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderState_Lang whereName($value)
 * @property-read \OrderState $orderState 
 */
class OrderState_Lang extends Eloquent
{


    protected $table = 'order_states_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function orderState()
    {
        return $this->belongsTo('OrderState');
    }


}