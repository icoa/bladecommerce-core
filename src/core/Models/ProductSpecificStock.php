<?php


/**
 * ProductSpecificStock
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_combination_id
 * @property string $warehouse
 * @property integer $quantity_24
 * @property integer $quantity_48
 * @property string $available_at
 * @property integer $quantity_shop
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereProductCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereWarehouse($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereQuantity24($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereQuantity48($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereAvailableAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductSpecificStock whereQuantityShop($value)
 */
class ProductSpecificStock extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'products_specific_stocks';
    protected $guarded = array('id');
    protected $isBlameable = false;
    public $timestamps = false;
    public $softDelete = false;
    public $db_fields = array(
        "product_id",
        "product_combination_id",
        "quantity_24",
        "quantity_48",
        "available_at",
        "quantity_shop",
        "warehouse",
    );

    protected $defaults = [
        'product_combination_id' => null,
    ];


    function setProductQuantity()
    {
        $qty = $this->getProductQuantity();
        $is_out_of_production = 0;
        $updated_at = \Format::now();
        DB::table('products')->where('id', $this->product_id)->update(compact('is_out_of_production', 'qty', 'updated_at'));
    }

    function getProductQuantity()
    {
        return $this->quantity_24 + $this->quantity_48 + $this->quantity_shop;
    }

    static function getOverallProductQuantity($product_id)
    {
        $rows = ProductSpecificStock::where('product_id', $product_id)->get();
        $sum = 0;
        $has_combinations = false;
        foreach ($rows as $row) {
            if ($row->product_combination_id > 0)
                $has_combinations = true;
        }
        foreach ($rows as $row) {
            if ($has_combinations) {
                if ($row->product_combination_id > 0)
                    $sum += $row->getProductQuantity();
            } else {
                $sum += $row->getProductQuantity();
            }
        }
        return $sum;
    }
}