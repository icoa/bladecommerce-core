<?php


/**
 * AddressTracking
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $result
 * @property integer $product_id
 * @property integer $shipping_number
 * @property string $url
 * @property string $tracking
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereResult($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereShippingNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereTracking($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressTracking whereUpdatedAt($value)
 */
class AddressTracking extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'mi_address_trackings';
    protected $guarded = array('id');
    protected $isBlameable = false;
    public $timestamps = false;
    public $softDelete = false;
    public $db_fields = array(
        "order_id",
        "product_id",
        "result",
        "shipping_number",
        "url",
        "tracking",
    );

    function getTrackingAttribute($value){
        return unserialize($value);
    }
}