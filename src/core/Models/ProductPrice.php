<?php

use Illuminate\Database\Eloquent\Builder;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Traits\TraitActive;

/**
 * ProductPrice
 *
 * @property integer $id
 * @property integer $price_rule_id
 * @property integer $product_id
 * @property integer $shop_id
 * @property integer $customer_group_id
 * @property integer $currency_id
 * @property integer $country_id
 * @property integer $customer_id
 * @property integer $combination_id
 * @property integer $affiliate_id
 * @property string $description
 * @property float $price
 * @property integer $from_quantity
 * @property float $reduction_value
 * @property string $reduction_type
 * @property boolean $reduction_target
 * @property integer $reduction_currency
 * @property integer $reduction_tax
 * @property boolean $free_shipping
 * @property string $date_from
 * @property string $date_to
 * @property integer $position
 * @property boolean $stop_other_rules
 * @property boolean $active
 * @property boolean $global
 * @property boolean $repeatable
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice wherePriceRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereCustomerGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereFromQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereReductionValue($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereReductionType($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereReductionTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereReductionCurrency($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereReductionTax($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereFreeShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereDateFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereDateTo($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereStopOtherRules($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereGlobal($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductPrice whereRepeatable($value)
 * @method static \ProductPrice available()
 * @method static \ProductPrice withProduct($product_id)
 * @method static \ProductPrice active()
 */
class ProductPrice extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'products_specific_prices';
    protected $guarded = array();
    protected $softDelete = false;
    protected $isBlameable = false;
    public $timestamps = false;

    use TraitActive;


    public $db_fields = array(
        "price_rule_id",
        "product_id",
        "shop_id",
        "customer_group_id",
        "currency_id",
        "country_id",
        "customer_id",
        "combination_id",
        "price",
        "from_quantity",
        "reduction_value",
        "reduction_type",
        "reduction_currency",
        "reduction_tax",
        "reduction_target",
        "free_shipping",
        "date_from",
        "date_to",
        "position",
        "stop_other_rules",
        "global",
        "repeatable",
    );


    protected $defaults = [
        "shop_id" => 1,
        "customer_group_id" => 0,
        "currency_id" => 0,
        "country_id" => 0,
        "customer_id" => 0,
        "combination_id" => 0,
        "from_quantity" => 1,
        "reduction_currency" => 1,
        "reduction_tax" => 1,
        "reduction_target" => 2,
        "free_shipping" => 0,
        "date_from" => null,
        "date_to" => null,
        "stop_other_rules" => 1,
        "repeatable" => 0,
    ];

    protected function getDefaults(){
        $defaults = $this->defaults;
        $defaults['position'] = ProductPrice::max('position') + 1;
        return $defaults;
    }


    public static function position($parent_id = false) {
        $instance = new static;
        $query = $instance->where('id', '>', 0)->where('price_rule_id',0);
        if ($parent_id != false) {
            $query->where('parent_id', $parent_id);
        }
        $position = $query->max('position');
        return $position + 1;
    }

    /**
     * @return \PriceRule|null
     */
    public function getPriceRule(){
        try{
            $priceRule = $this->price_rule_id > 0 ? \PriceRule::getPublicObj($this->price_rule_id) : null;
            if($priceRule){
                return $priceRule;
            }
            $priceRule = $this->price_rule_id > 0 ? \PriceRule::getObj($this->price_rule_id) : null;
            if($priceRule and (int)$priceRule->active === 1){
                return $priceRule;
            }
            return null;
        }catch (Exception $e){
            return null;
        }
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeAvailable(Builder $query)
    {
        return $query->whereIn('price_rule_id', function($subquery){
            $subquery->select('id')->from('price_rules')->where('active', 1)->whereNull('deleted_at');
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithProduct(Builder $query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new ProductPricePresenter($this);
    }
    
}

class ProductPricePresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}
