<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * MenuType
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $override
 * @property string $params
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\MenuType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MenuType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\MenuType whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\MenuType whereOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\MenuType whereParams($value)
 */
class MenuType extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'menu_types';
    protected $guarded = array();
    protected $isBlameable = FALSE;
    public $timestamps = FALSE;
    public $softDelete = false;
    public $db_fields = array(
        "name", "description", "override", "params"
    );

    public function canDelete(){
        $id = $this->id;
        $total = \Menu::where("menutype_id",$id)->count("id");
        if($total > 0){
            return "Non è possibile eliminare questo elemento in quanto è riferito da altre entità";
        }
        return true;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new MenuTypePresenter($this);
    }

}

class MenuTypePresenter extends Presenter {

    public function presentMenus() {
        $id = $this->id;
        $total = \Menu::where("menutype_id",$id)->count("id");
        return $total;
    }

}