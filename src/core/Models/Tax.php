<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Tax
 *
 * @property integer $id
 * @property float $rate
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Tax whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereRate($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class Tax extends MultiLangModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'taxes';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    
    protected $db_fields = array("rate", "active");
    protected $lang_model = 'Tax_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );

    // RELATIONS
    

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new TaxPresenter($this);
    }

}

class TaxPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}



/**
 * Tax_Lang
 *
 * @property integer $tax_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\Tax_Lang whereTaxId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tax_Lang whereName($value)
 * @property-read \Tax $tax
 */
class Tax_Lang extends Eloquent  {


    protected $table = 'taxes_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function tax()
    {
        return $this->belongsTo('Tax');
    }


}
