<?php



/**
 * Lexicon
 *
 * @property integer $id
 * @property string $code
 * @property string $ldesc
 * @property boolean $is_shortcode
 * @property boolean $is_deep
 * @property boolean $is_javascript
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereIsShortcode($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereIsDeep($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereIsJavascript($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Lexicon extends MultiLangModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'lexicons';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array(
        "name",
        "name_plural",
    );
    protected $db_fields = array(
        "code",
        "ldesc",
        "is_shortcode",
        "is_deep",
    );    
    protected $db_fields_cloning = array(
        "code" => SingleModel::CLONE_UNIQUE_SLUG
    );
    protected $lang_model = 'Lexicon_Lang';


    protected function initCache(){
        $instance = new static;
        $rows = $instance::get();

        foreach($rows as $row){
            $master = $row->getRedisMaster();
            $row->cacheRedisObject($master);
        }
    }


    protected function cacheRedisObject($master){

        $translations = $this->translations()->get();

        foreach($translations as $tr){

            $text = $tr->name;
            if(trim($tr->name_plural) != ''){
                $text .= "|".$tr->name_plural;
            }

            \Redis::hset($master,'name_'.$tr->lang_id,$text);
            //\Utils::log("master:$master | var: name_{$tr->lang_id} | value: $text","SAVING REDIS OBJ");

        }
    }


    public function getRedisMaster(){
        $className = trim(strtolower($this->code));
        //return 'lexicon-'.$className;
        return \Utils::redisPrefix('lexicon-'.$className);
    }



    public function flushCache(){
        $master = $this->getRedisMaster();
        $this->cacheRedisObject($master);
    }

    protected function equivalentCache(){
        $master = $this->getRedisMaster();
        $this->cacheRedisObject($master);
    }

    protected function redisCache($code, $lang = 'default'){
        $instance = new static;
        $code = trim(strtolower($code));
        $master = 'lexicon-'.$code;
        $master = \Utils::redisPrefix($master);
        $lang = \Core::getLang($lang);
        $field = "name_".$lang;
        try{
            $value = \Redis::hget($master, $field);
            if($value == null OR $value == ''){
                throw new \Exception("Could not find a valid Redis cache: [master:$master] [field:$field]");
            }
            return $value;
        }catch(\Exception $e){
            //\Utils::log($e->getMessage());
            $obj = $instance::where('code',$code)->first();
            if($obj){
                $obj->cacheRedisObject($master);
                $value = \Redis::hget($master, $field);
            }else{
                $value = $code;
            }

            return $value;
        }
        return null;
    }
    


}


/**
 * Lexicon_Lang
 *
 * @property integer $lexicon_id
 * @property string $lang_id
 * @property string $name
 * @property string $name_plural
 * @method static \Illuminate\Database\Query\Builder|\Lexicon_Lang whereLexiconId($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Lexicon_Lang whereNamePlural($value)
 * @method static \Lexicon_Lang lang($lang_id)
 * @property-read \Lexicon $lexicon
 */
class Lexicon_Lang extends Eloquent  {


    protected $table = 'lexicons_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function lexicon()
    {
        return $this->belongsTo('Lexicon');
    }

    public function scopeLang($query,$lang_id)
    {
        return $query->where('lang_id',$lang_id);
    }

}