<?php


/**
 * Wishlist
 *
 * @property integer $id
 * @property string $name
 * @property integer $customer_id
 * @property string $session_id
 * @property boolean $is_default
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereSessionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Wishlist whereDeletedAt($value)
 */
class Wishlist extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'wishlists';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public $db_fields = array();

    private $products = null;

    const CACHE_NAME = 'wishlist';


    static function getBySession($session_id)
    {
        return \Wishlist::where('session_id', $session_id)->where('is_default', 1)->first();
    }

    static function getByCustomer($customer_id)
    {
        return \Wishlist::where('customer_id', $customer_id)->where('is_default', 1)->first();
    }

    static function getAllWishlistsBySession($session_id, $ids_only = false)
    {
        $builder = \Wishlist::where('session_id', $session_id);
        return $ids_only ? $builder->lists('id') : $builder->get();
    }

    static function getAllWishlistsByCustomer($customer_id, $ids_only = false)
    {
        $builder = \Wishlist::where('customer_id', $customer_id);
        return $ids_only ? $builder->lists('id') : $builder->get();
    }

    static function getProductsBySession($session_id, $ids_only = false)
    {
        $wishlist_ids = self::getAllWishlistsBySession($session_id, true);
        if (empty($wishlist_ids)) {
            return [];
        }
        $builder = \DB::table('wishlists_products')->whereIn('wishlist_id', $wishlist_ids);
        return $ids_only ? $builder->lists('product_id') : $builder->get();
    }

    static function getProductsByCustomer($customer_id, $ids_only = false)
    {
        $wishlist_ids = self::getAllWishlistsByCustomer($customer_id, true);
        if (empty($wishlist_ids)) {
            return [];
        }
        $builder = \DB::table('wishlists_products')->whereIn('wishlist_id', $wishlist_ids);
        return $ids_only ? $builder->lists('product_id') : $builder->get();
    }

    static function getAllProductsIds()
    {
        $customer_id = \FrontUser::auth();
        $session_id = \Session::getId();
        if ($customer_id > 0) {
            return self::getProductsByCustomer($customer_id, true);
        }
        return self::getProductsBySession($session_id, true);
    }


    static function getReference()
    {
        $customer_id = \FrontUser::auth();
        $session_id = \Session::getId();
        //get or create a wishlist
        if ($customer_id > 0) {
            $wishlist = self::getByCustomer($customer_id);
            if ($wishlist) {
                return $wishlist;
            }
        }
        /*$wishlist = self::getBySession($session_id);
        if ($wishlist) {
            return $wishlist;
        }*/
        return self::createNewReference();
    }

    static function createNewReference()
    {
        $customer_id = \FrontUser::auth();
        $session_id = \Session::getId();
        $record = new self();
        $record->customer_id = $customer_id;
        $record->session_id = $session_id;
        $record->is_default = 1;
        $record->save();
        return $record;
    }


    static function getSize()
    {
        $user = \FrontUser::get();
        if ($user) {
            $obj = self::getByCustomer($user->id);
            if ($obj) {
                return $obj->size();
            }
            return 0;
        }
        return 0;
    }

    public function size()
    {
        return count($this->getProducts());
    }

    public function getName()
    {
        if ($this->name == null) {
            return 'Default';
        }
        return $this->name;
    }

    public function getProducts()
    {
        if ($this->products) {
            return $this->products;
        }
        $data = [];
        $rows = \DB::table('wishlists_products')->where('wishlist_id', $this->id)->get();
        foreach ($rows as $row) {
            $data[] = $row->product_id;
        }
        $this->products = $data;
        return $data;
    }

    public function addProduct($id)
    {
        $many = \DB::table('wishlists_products')->where('wishlist_id', $this->id)->where('product_id', $id)->count();
        if ($many > 0) {
            return -1;
        }
        \DB::table('wishlists_products')->insert(['wishlist_id' => $this->id, 'product_id' => $id, 'created_at' => \Format::now()]);
        return 1;
    }

    function addToCart()
    {
        $ids = $this->getProducts();
        foreach ($ids as $id) {
            $product = \Product::getPublicObj($id);
            if ($product) {
                \CartManager::add($product, 1, false);
            }
        }
    }

    function emptyAll()
    {
        \DB::table('wishlists_products')->where('wishlist_id', $this->id)->delete();
    }

    function removeProduct($id)
    {
        \DB::table('wishlists_products')->where('wishlist_id', $this->id)->where('product_id', $id)->delete();
    }


}