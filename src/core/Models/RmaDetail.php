<?php


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use services\Traits\TraitProductRow;

/**
 * OrderDetail
 *
 * @property integer $id
 * @property integer $rma_id
 * @property integer $product_id
 * @property integer $product_quantity
 * @property integer $order_details_id
 */
class RmaDetail extends SingleModel
{
    use TraitProductRow;

    /* LARAVEL PROPERTIES */

    protected $table = 'rma_details';
    protected $guarded = array();

    public $db_fields = array();

    // RELATIONS

    /**
     * @return BelongsTo
     */
    public function rma()
    {
        return $this->belongsTo('Rma', 'rma_id');
    }

    /**
     * @return BelongsTo
     */
    public function order_detail()
    {
        return $this->belongsTo('OrderDetail', 'order_details_id');
    }

    /**
     * @return Rma|null
     **/
    public function getRma()
    {
        return $this->rma;
    }

    /**
     * @return OrderDetail|null
     **/
    public function getOrderDetail()
    {
        return $this->order_detail;
    }

    /**
     * @return Product|null
     */
    function getProduct()
    {
        return Product::getObj($this->product_id);
    }

    /**
     * @param Builder $builder
     * @param $rma_id
     */
    public function scopeWithRma(Builder $builder, $rma_id)
    {
        $builder->where('rma_id', $rma_id);
    }

    /**
     * @param Builder $builder
     * @param array $rmas
     */
    public function scopeWithRmas(Builder $builder, array $rmas)
    {
        $builder->whereIn('rma_id', $rmas);
    }
}