<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 28-nov-2013 18.05.30
 */

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * ProductImage
 *
 * @property integer $id
 * @property string $filename
 * @property integer $product_id
 * @property integer $position
 * @property boolean $cover
 * @property boolean $active
 * @property string $md5
 * @property integer $var
 * @property integer $version
 * @property integer $updated_by
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $created_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @property-read string $path
 * @property-read string $full_path
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereFilename($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereCover($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereMd5($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereVar($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereVersion($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage whereCreatedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class ProductImage extends MultiLangModel implements PresentableInterface
{

    /* LARAVEL PROPERTIES */
    protected $table = 'images';
    protected $guarded = array();
    protected $softDelete = false;

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("legend", "published");
    protected $db_fields = array("filename", "position", "product_id", "cover", "md5", "var", "version");
    protected $lang_model = 'ProductImage_Lang';

    protected $defaults = [
        'cover' => 0,
        'active' => 1,
        'position' => 1,
        'md5' => null,
        'var' => null,
        'version' => null,
    ];


    function getImgId()
    {
        $file = strtolower($this->filename);
        return (int)str_replace(['.jpg', '.png', '.jpeg', '.gif'], '', $file);
    }

    /**
     * @param $md5
     * @return ProductImage|null
     */
    static function getByMd5($md5)
    {
        return ProductImage::where('md5', $md5)->first();
    }

    /**
     * @param int $product_id
     * @param int $position
     * @return ProductImage|null
     */
    static function getByPosition($product_id, $position)
    {
        return ProductImage::where(compact('product_id', 'position'))->first();
    }

    /**
     * @return string
     */
    public function getPathAttribute()
    {
        return "assets/products/{$this->filename}";
    }

    /**
     * @return string
     */
    public function getFullPathAttribute()
    {
        return public_path($this->getPathAttribute());
    }

    /**
     * @return bool
     */
    public function fileExists()
    {
        return file_exists($this->getFullPathAttribute());
    }

    /**
     * @return bool
     */
    public function isCover()
    {
        return (int)$this->cover === 1;
    }

    /**
     * @return bool
     */
    public function isImage(){
        if(false === $this->fileExists())
            return false;

        $full_path = $this->getFullPathAttribute();
        return \Utils::isImage($full_path);
    }

    /**
     *
     */
    public function purgeFromDisk(){
        \File::delete($this->getFullPathAttribute());
        if($this->isCover()){
            DB::table('products')->where('id', $this->product_id)->update(['default_img' => null]);
        }
        \Product::forgetFullCache($this->product_id);
        $this->forceDelete();
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new ImagePresenter($this);
    }

}

class ImagePresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}


/**
 * ProductImage_Lang
 *
 * @property integer $product_image_id
 * @property string $lang_id
 * @property string $legend
 * @property boolean $published
 * @method static \Illuminate\Database\Query\Builder|\ProductImage_Lang whereProductImageId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage_Lang whereLegend($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductImage_Lang wherePublished($value)
 * @property-read \ProductImage $image
 */
class ProductImage_Lang extends Eloquent
{


    protected $table = 'images_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function image()
    {
        return $this->belongsTo('ProductImage');
    }


}
