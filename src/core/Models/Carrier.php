<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Carrier
 *
 * @property integer $id
 * @property integer $reference_id
 * @property integer $tax_rules_group_id
 * @property string $url
 * @property boolean $active
 * @property boolean $shipping_handling
 * @property boolean $range_behavior
 * @property boolean $shipping_custom
 * @property boolean $is_free
 * @property boolean $shipping_external
 * @property boolean $need_range
 * @property string $external_module_name
 * @property integer $shipping_method
 * @property integer $position
 * @property integer $max_width
 * @property integer $max_height
 * @property integer $max_depth
 * @property float $max_weight
 * @property integer $grade
 * @property string $logo
 * @property boolean $group_restriction
 * @property boolean $skip_shipping
 * @property boolean $shops_required
 * @property boolean $virtual
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereReferenceId($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereTaxRulesGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereShippingHandling($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereRangeBehavior($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereShippingCustom($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereIsFree($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereShippingExternal($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereNeedRange($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereExternalModuleName($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereShippingMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereMaxWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereMaxHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereMaxDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereMaxWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereGrade($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereLogo($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereGroupRestriction($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereSkipShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereShopsRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereVirtual($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read mixed $type_name
 */
class Carrier extends MultiLangModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    const TYPE_VIRTUAL_DELIVERY = 1;
    const TYPE_NATIONAL_DELIVERY = 5;
    const TYPE_INTERNATIONAL_DELIVERY = 2;
    const TYPE_SHOP_WITHDRAWAL = 4;
    const TYPE_B2B_SHOP_WITHDRAWAL = 11;
    const TYPE_B2B_NATIONAL_DELIVERY = 12;
    const TYPE_B2B_SHOP_AVAILABLE = 13;
    const TYPE_B2B_INTERNATIONAL_DELIVERY = 14;

    protected $table = 'carriers';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $has_published = false;

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("delay", "name", "carttext");
    protected $db_fields = array("tax_rules_group_id", "url", "active", "position", "max_width", "max_height", "max_depth", "max_weight", "range_behavior", "shipping_method", "is_free", "shipping_custom", "shipping_handling", "logo", "grade", "group_restriction");
    protected $lang_model = 'Carrier_Lang';
    protected $db_fields_cloning = array(
        "active" => 0,
    );
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );

    /**
     * @return array
     */
    static function typeToArray(){
        return [
            self::TYPE_VIRTUAL_DELIVERY => 'VIRTUAL_DELIVERY',
            self::TYPE_NATIONAL_DELIVERY => 'NATIONAL_DELIVERY',
            self::TYPE_INTERNATIONAL_DELIVERY => 'INTERNATIONAL_DELIVERY',
            self::TYPE_SHOP_WITHDRAWAL => 'SHOP_WITHDRAWAL',
            self::TYPE_B2B_SHOP_WITHDRAWAL => 'B2B_SHOP_WITHDRAWAL',
            self::TYPE_B2B_NATIONAL_DELIVERY => 'B2B_NATIONAL_DELIVERY',
            self::TYPE_B2B_SHOP_AVAILABLE => 'B2B_SHOP_AVAILABLE',
            self::TYPE_B2B_INTERNATIONAL_DELIVERY => 'B2B_INTERNATIONAL_DELIVERY',
        ];
    }

    /**
     * @return mixed
     */
    function getTypeNameAttribute(){
        $names = self::typeToArray();
        if(array_key_exists($this->id, $names))
            return $names[$this->id];
        return 'Undefined';
    }

    /**
     * @param $status
     * @return int
     */
    static function statusToInt($status){
        $array = self::typeToArray();
        foreach($array as $key => $value){
            if($value === $status)
                return $key;
        }
        return 0;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new CarrierPresenter($this);
    }

    function _after_clone(&$source, &$model)
    {
        \Utils::watch();
        $source_id = $source->id;
        $target_id = $model->id;

        $tables = ["range_weight", "range_price"];

        foreach ($tables as $table) {
            $key_id = $table . "_id";
            $rows = \DB::table($table)->where("carrier_id", $source_id)->get();
            if (count($rows) > 0) {
                foreach ($rows as $row) {
                    $data = [
                        "carrier_id" => $target_id,
                        "delimiter1" => $row->delimiter1,
                        "delimiter2" => $row->delimiter2,
                    ];
                    $range_id = \DB::table($table)->insertGetId($data);
                    $dRows = \DB::table("delivery")->where("carrier_id", $source_id)->where($key_id, $row->id)->get();
                    foreach ($dRows as $dd) {
                        $data = [
                            "carrier_id" => $target_id,
                            $key_id => $range_id,
                            "price" => $dd->price,
                            "zone_id" => $dd->zone_id,
                        ];
                        \DB::table("delivery")->insert($data);
                    }
                }
            }
        }
    }


    function _before_delete()
    {
        if ($this->forceDeleting == false) {
            return;
        }
        $id = $this->id;
        \Utils::watch();
        $tables = ["range_weight", "range_price"];

        foreach ($tables as $table) {
            \DB::table($table)->where("carrier_id", $id)->delete();
        }
        \DB::table("delivery")->where("carrier_id", $id)->delete();
    }

    static function generateZonesTable($id = 0, $mode = 'weight')
    {
        \Utils::watch();
        $field = ($mode == 'weight') ? "range_weight_id" : "range_price_id";
        $rules = \DB::table("delivery")->where("carrier_id", $id)->where($field, ">", 0)->count();

        $addOn = ($mode == 'weight') ? \Cfg::get("WEIGHT_UNIT") : \Core::getCurrencySign();
        $addOnLabel = ($mode == 'weight') ? "peso" : "prezzo";
        $currency = \Core::getCurrencySign();

        $range_inf = <<<INPUT
<th class="center rInput"><div class="controls input-append"><input value="0.00" type="text"  name="range_inf[0]"  class="align-right" autocomplete="off"><span class="add-on">$addOn</span></div></th>
INPUT;
        $range_sup = <<<INPUT
<th class="center rInput"><div class="controls input-append"><input type="text" value="" name="range_sup[0]"  class="align-right" autocomplete="off"><span class="add-on">$addOn</span></div></th>
INPUT;

        $fees_all = <<<INPUT
<td class="center rInput"><div class="controls input-append"><input disabled="disabled" type="text" data-index="0" class="align-right allPrice" autocomplete="off"><span class="add-on">$currency</span></div></td>
INPUT;

        $delete_td = "<td class=\"center\">&nbsp;</td>";
        $range_rows = [];
        if ($rules > 0) {
            $table = ($mode == 'weight') ? 'range_weight' : 'range_price';
            $range_rows = \DB::table($table)->where("carrier_id", $id)->get();
            $counter = 0;
            $range_inf = $range_sup = $fees_all = "";
            foreach ($range_rows as $row) {
                $row->delimiter1 = \Format::float($row->delimiter1, 3);
                $row->delimiter2 = \Format::float($row->delimiter2, 3);
                $range_inf .= <<<INPUT
<th class="center rInput"><div class="controls input-append"><input value="$row->delimiter1" type="text"  name="range_inf[$row->id]"  class="align-right" autocomplete="off"><span class="add-on">$addOn</span></div></th>
INPUT;
                $range_sup .= <<<INPUT
<th class="center rInput"><div class="controls input-append"><input type="text" value="$row->delimiter2" name="range_sup[$row->id]"  class="align-right" autocomplete="off"><span class="add-on">$addOn</span></div></th>
INPUT;

                $fees_all .= <<<INPUT
<td class="center rInput"><div class="controls input-append"><input disabled="disabled" type="text" data-index="$counter" class="align-right allPrice" autocomplete="off"><span class="add-on">$currency</span></div></td>
INPUT;
                if ($counter > 0) {
                    $delete_td .= "<td class=\"center\"><button type='button' class='mini-button' onclick='Carriers.removeColumn($counter);'>ELIMINA</button></td>";
                }
                $counter++;
            }
        }

        $tpl = <<<HTML
<div id="zone_ranges" style="float:left">
        <table cellspacing="0" cellpadding="5" id="zones_table" class="table table-striped table-bordered">
                <tbody>
                    <tr class="range_inf">
                        <th class="range_type">Sarà applicato quando il $addOnLabel è</th>
                        <th class="range_sign center">&gt;=</th>
                        $range_inf
                    </tr>
                <tr class="range_sup">
                        <th class="center range_type">Sarà applicato quando il $addOnLabel è</th>
                        <th class="range_sign center">&lt;</th>
                        $range_sup
                </tr>
                <tr class="fees_all">
                        <td class="border_top border_bottom border_bold"><b>TUTTI</b></td>
                        <td clasS="range_sign center"><input type="checkbox" onclick="Carriers.checkAllZones(this);" value="" autocomplete="off"></td>
                        $fees_all
                </tr>
HTML;

        $rows = \Zone::where("active", 1)->orderBy("name")->get();


        foreach ($rows as $row) {
            $link = \URL::action("ZonesController@getPreview", $row->id);
            $delivery_rows = \DB::table("delivery")->where("carrier_id", $id)->where("zone_id", $row->id)->where($field, ">", 0)->get();


            $checked = "";
            $disabled = "disabled";
            $price = "";
            if (count($delivery_rows) > 0) {
                $price = "";
                $checked = "checked";
                $disabled = "";
                foreach ($delivery_rows as $dr) {
                    $float_price = (float)$dr->price;
                    $dr->price = \Format::money($dr->price);
                    $key_id = ($mode == 'weight') ? $dr->range_weight_id : $dr->range_price_id;
                    $price .= <<<TPL
<td class="center rInput"><div class="controls input-append"><input $disabled type="text" value="$dr->price" name="fees[$row->id][$key_id]" rel="$row->id" class="align-right price" autocomplete="off"><span class="add-on">$currency</span></div></td>         
TPL;
                }
            } else {
                if (count($range_rows) > 0) {
                    foreach ($range_rows as $rr) {
                        $checked = "";
                        $price .= <<<TPL
<td class="center rInput"><div class="controls input-append"><input disabled="disabled" type="text"  name="fees[$row->id][$rr->id]" rel="$row->id" class="align-right price" autocomplete="off"><span class="add-on">$currency</span></div></td>
TPL;
                    }
                } else {
                    $checked = "";
                    $price .= <<<TPL
<td class="center rInput"><div class="controls input-append"><input disabled="disabled" type="text"  name="fees[$row->id][0]" rel="$row->id" class="align-right price" autocomplete="off"><span class="add-on">$currency</span></div></td>
TPL;
                }
            }

            $tpl .= <<<TPL
<tr data-zoneid="$row->id" class="fees ">
<td><label for="zone_$row->id">$row->name</label> <a class='ml5' href="javascript:Echo.remoteModal('$link');">(Anteprima)</a></td>
<td class="zone range_sign center">
        <input type="checkbox" value="1" name="zone_$row->id" id="zone_$row->id" class="input_zone" autocomplete="off" zone="$row->id" $checked>
</td>
$price
</tr>
TPL;
        }

        $tpl .= <<<TPL
                <tr class="delete_range">
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        $delete_td
                </tr>
        </tbody></table>
</div>    
<div class="ml10 pull-left">
      <button type="button" class="mini-button" onclick="Carriers.addColumn();">AGGIUNGI NUOVA FASCIA</button>          
</div>
TPL;

        return $tpl;
    }


    function getShippingCost($shipping_cost, $withTaxes = true)
    {
        if ($this->is_free == 1) {
            $shipping_cost = 0;
        } else {
            if ($withTaxes AND $this->tax_rules_group_id > 0) {
                $taxGroup = \TaxGroup::getPublicObj($this->tax_rules_group_id);
                $ratio = $taxGroup->getDefaultTaxRatio();
                $shipping_cost = $shipping_cost * $ratio;
            }
        }

        return $shipping_cost;
    }

    function getTaxRate()
    {
        if ($this->tax_rules_group_id > 0) {
            $taxGroup = \TaxGroup::getPublicObj($this->tax_rules_group_id);
            $ratio = $taxGroup->getDefaultTaxRatio();
            return $ratio;
        }
        return 0;
    }

    function getDelimiterForFreeShipping($zone_id, $from)
    {
        if ($from == 'range_price') {
            $query = "SELECT delimiter1 as delimiter FROM range_price WHERE id IN (SELECT range_price_id FROM delivery WHERE zone_id=$zone_id AND carrier_id={$this->id} AND price=0)";
        }
        if ($from == 'range_weight') {
            $query = "SELECT delimiter1 as delimiter FROM range_weight WHERE id IN (SELECT range_weight_id FROM delivery WHERE zone_id=$zone_id AND carrier_id={$this->id} AND price=0)";
        }
        $row = \DB::selectOne($query);
        if ($row AND $row->delimiter > 0) {
            return $row->delimiter;
        }
        return 0;
    }

    /**
     * @param $id
     * @return bool
     */
    function isType($id){
        return (int)$this->id === $id;
    }

    /**
     * @param $ids
     * @return bool
     */
    function isAnyType($ids = []){
        return in_array((int)$this->id, $ids);
    }

}

class CarrierPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}


/**
 * Carrier_Lang
 *
 * @property integer $carrier_id
 * @property integer $shop_id
 * @property string $lang_id
 * @property string $name
 * @property string $delay
 * @property string $carttext
 * @method static \Illuminate\Database\Query\Builder|\Carrier_Lang whereCarrierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier_Lang whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier_Lang whereDelay($value)
 * @method static \Illuminate\Database\Query\Builder|\Carrier_Lang whereCarttext($value)
 * @property-read \Carrier $carrier
 */
class Carrier_Lang extends Eloquent
{


    protected $table = 'carriers_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function carrier()
    {
        return $this->belongsTo('Carrier');
    }


}