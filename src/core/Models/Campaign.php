<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Campaign
 *
 * @property integer $id
 * @property integer $campaign_type
 * @property string $name
 * @property string $lang_id
 * @property string $network
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereCampaignType($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereNetwork($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Campaign whereDeletedAt($value)
 */
class Campaign extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'campaigns';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];


    
    protected $db_fields = array("name", "active", "lang_id", "campaign_type", "network");
    protected $db_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "active" => 0
    );



    // RELATIONS
    

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CampaignPresenter($this);
    }

}

class CampaignPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }



}
