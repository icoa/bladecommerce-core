<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * People
 *
 * @property boolean $id
 * @property boolean $active
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\People whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\People whereActive($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class People extends MultiLangModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'peoples';
    protected $guarded = array();

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array(
        "name",
    );
    protected $db_fields = array(
        "active",
    );    

    protected $lang_model = 'People_Lang';

}

/**
 * People_Lang
 *
 * @property boolean $people_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\People_Lang wherePeopleId($value)
 * @method static \Illuminate\Database\Query\Builder|\People_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\People_Lang whereName($value)
 * @property-read \People $people
 */
class People_Lang extends Eloquent  {


    protected $table = 'peoples_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function people()
    {
        return $this->belongsTo('People');
    }

}