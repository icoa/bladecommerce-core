<?php

/**
 * Nav
 *
 * @property integer $id
 * @property string $shortcut
 * @property string $ucode
 * @property integer $navtype_id
 * @property float $minprice
 * @property float $maxprice
 * @property integer $currency_id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property integer $position
 * @property boolean $indexable
 * @property boolean $feedable
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Nav whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereShortcut($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereUcode($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereNavtypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereMinprice($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereMaxprice($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereIndexable($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereFeedable($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class Nav extends MultiLangModel{

    /* LARAVEL PROPERTIES */
    protected $table = 'navs';
    protected $guarded = array();
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "published", "menuname", "slug", "ldesc", "h1", "canonical", "metatitle", "metakeywords", "metadescription", "metafacebook", "metatwitter", "metagoogle");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("robots", "ogp", "ogp_type", "ogp_image", "navtype_id", "minprice", "maxprice","currency_id", "position");
    protected $lang_model = 'Nav_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );


    function getPublicName($lang = null){
        //\Utils::log($this->getAttributes(),"NAV getPublicName");
        if($lang){
            if(isset($this->$lang->menuname) AND $this->$lang->menuname != ''){
                return $this->$lang->menuname;
            }
            return $this->$lang->name;
        }else{
            if(isset($this->menuname) AND $this->menuname != ''){
                return $this->menuname;
            }
            return $this->name;
        }
    }


    static function getSlug($id, $lang_id){
        if($id <= 1){
            return "";
        }
        return parent::getSlug($id, $lang_id);
    }

    static function hasType($id,$type){
        $instance = new static;
        $navtype_id = $instance::where("id",$id)->pluck("navtype_id");
        return ($navtype_id == $type);
    }

    public function expand()
    {
        $this->link = \Site::root()."/". $this->slug;
        if($this->ucode != ''){
            $this->link.="-".$this->ucode.".htm";
        }
        return $this;
    }

}


/**
 * Nav_Lang
 *
 * @property integer $nav_id
 * @property string $lang_id
 * @property string $name
 * @property string $menuname
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $canonical
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $metafacebook
 * @property string $metagoogle
 * @property string $metatwitter
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereNavId($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMenuname($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Nav_Lang whereFooter($value)
 * @property-read \Nav $nav
 */
class Nav_Lang extends Eloquent  {


    protected $table = 'navs_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function nav()
    {
        return $this->belongsTo('Nav');
    }


}