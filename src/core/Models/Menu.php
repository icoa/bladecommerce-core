<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Menu
 *
 * @property integer $id
 * @property string $description
 * @property integer $menutype_id
 * @property integer $parent_id
 * @property integer $position
 * @property boolean $showname
 * @property boolean $showtitle
 * @property string $override
 * @property string $acl_groups
 * @property string $starting_at
 * @property string $finishing_at
 * @property boolean $inherit_context
 * @property string $link_type
 * @property string $link_params
 * @property string $content_type
 * @property boolean $content_link
 * @property string $content_module_position
 * @property string $params
 * @property boolean $is_temp
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Menu whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereMenutypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereShowname($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereShowtitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereAclGroups($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereStartingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereFinishingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereInheritContext($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereLinkType($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereLinkParams($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereContentType($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereContentLink($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereContentModulePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereIsTemp($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Menu extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'menus';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "subtitle", "published", "content_image", "content_html");
    protected $db_fields = array( "description", "menutype_id", "parent_id", "position", "showtitle", "showname", "inherit_context", "starting_at", "finishing_at", "override", "acl_groups", "params", "link_params", "link_type", "content_type", "content_link", "content_module_position");
    protected $lang_model = 'Menu_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "published" => 0
    );



    public function setPosition(){
        if($this->parent_id){
            $position = \Menu::where("parent_id",$this->parent_id)->max("position");
            $this->position = $position + 1;
        }
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new MenuPresenter($this);
    }

}

class MenuPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

    public function presentStarting() {
        return Format::datetime($this->starting_at);
    }

    public function presentFinishing() {
        return Format::datetime($this->finishing_at);
    }

}
