<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * TaxGroup
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property string $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroup whereDeletedAt($value)
 */
class TaxGroup extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'taxes_rules_group';
    protected $guarded = array();

    
    public $db_fields = array(
        "name",
        "active",
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new TaxGroupPresenter($this);
    }


    
    public function getDefaultTaxRate(){
        $instance = new static;
        $country = \Cfg::get("COUNTRY_DEFAULT");
        //\Utils::watch();
        $key = "tax-rate-$country-{$this->id}";
        $obj = \Cache::remember($key, 60, function() use($country,$instance){
            $obj = \Tax::whereIn('id', function($query) use($country,$instance){
                $query->select(['tax_id'])
                    ->from('taxes_rules')
                    ->where('country_id', $country)
                    ->where('tax_rules_group_id', $this->id);
            })->first();
            return $obj;
        });

        return $obj->rate;
    }


    public function getDefaultTaxRatio(){
        $rate = $this->getDefaultTaxRate();
        $ratio = 1 + ($rate / 100);
        return $ratio;
    }
    
}

class TaxGroupPresenter extends Presenter {
    

}
