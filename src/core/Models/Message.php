<?php


/**
 * Message
 *
 * @property integer $id
 * @property integer $thread_id
 * @property string $type
 * @property integer $customer_id
 * @property integer $rma_id
 * @property integer $order_id
 * @property string $message
 * @property boolean $private
 * @property string $reason
 * @property \Carbon\Carbon $created_at
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $updated_by
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Message whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereThreadId($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereRmaId($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\Message wherePrivate($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereReason($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Message whereUpdatedBy($value)
 */
class Message extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'messages';
    protected $guarded = array();

    
    public $db_fields = array(
        "reason",
        "thread_id",
        "customer_id",
        "type",
        "message",
        "private",
    );


    public function sender(){
        $user = $this->getUser();
        $customer = $this->getCustomer();
        if($user){
            return $user->first_name." ".$user->last_name;
        }else{
            return ($customer) ? $customer->getName() : null;
        }
        return null;
    }

    public function image(){
        $user = $this->getUser();
        $customer = $this->getCustomer();
        if($user){
            $email = $user->email;
        }else{
            $email = ($customer) ? $customer->email : null;
        }
        $live_config = include(app_path('config/live/app.php'));
        $default = $live_config['url']."/media/avatar.png";
        $size = 80;
        $grav_url = "//www.gravatar.com/avatar/" . md5( strtolower( trim( $email ) ) ) . "?d=" . urlencode( $default ) . "&s=" . $size;
        return $grav_url;
    }


    public function getCustomer(){
        $customer = Customer::getObj($this->customer_id);
        return $customer;
    }

    public function getOrder(){
        $customer = Order::getObj($this->order_id);
        return $customer;
    }

    public function getUser(){
        $user = $this->created_by > 0 ? Sentry::findUserById($this->created_by) : null;
        return $user;
    }

    public static function getObj($id){
        $obj = parent::getObj($id);
        return $obj->expand();
    }

    public function expand(){

        $this->sender = $this->sender();
        $this->customer = $this->getCustomer();
        $this->image = $this->image();
        $this->date =  Format::lang_date($this->created_at, 'full');
        $this->order = null;
        if($this->order_id > 0){
            $order = Order::find($this->order_id);
            if($order){
                $this->order = $order;
            }
        }
        $this->count = Message::where('thread_id',$this->thread_id)->count('id');
        //\Utils::log($this->toArray(),"MESSAGE");
        return $this;
    }

    public function getAllMessages(){
        $ids = \Message::where('thread_id',$this->thread_id)->orderBy('created_at')->lists('id');
        $data = [];
        foreach($ids as $id){
            $data[] = Message::getObj($id);
        }
        return $data;
    }


    static function generateThreadId(){
        return Message::max('thread_id') + 1;
    }

    static function sendMessage($order_id, $message_id, $message, $customer_id = null){
        $thread_id = self::generateThreadId();
        if($message_id > 0){
            $obj = Message::find($message_id);
            if($obj){
                $thread_id = $obj->thread_id;
                $customer_id = $obj->customer_id;
            }
        }
        $type = 'U';

        if($order_id > 0){
            $order = Order::getObj($order_id);
            $customer_id = $order->customer_id;
            $obj = Message::where('order_id',$order_id)->first();
            if($obj){
                $thread_id = $obj->thread_id;
            }
        }



        $msg = new Message();
        $msg->order_id = (int)$order_id;
        $msg->customer_id = (int)$customer_id;
        $msg->message = $message;
        $msg->type = $type;
        $msg->thread_id = (int)$thread_id;
        $msg->save();

        if($msg->id > 0){
            $msg->sendEmail();
            return $msg->id;
        }
        return 0;
    }

    private function sendEmail(){
        $order = $this->getOrder();
        $customer = $this->getCustomer();
        $message = nl2br($this->message);
        $date = Format::now();
        $details = "<strong>ID messaggio:</strong> $this->id";
        if($order)$details .= "<br><strong>Ordine di riferimento:</strong> ".$order->reference;
        if($customer)$details .= "<br><strong>Cliente:</strong> ".$customer->getName();
        if($this->reason != '')$details .= "<br><strong>Motivo:</strong> ".$this->reason;
        $details .= "<br><br>".$message;
        $link = Link::absolute()->shortcut('account')."?page=send&reply=".$this->id;
        if($this->type == 'C'){
            $link = Site::root()."/admin/messages/edit/".$this->id;
        }

        $data = ['date' => $date, 'details' => $details, 'link' => $link];

        $email = Email::getByCode('MSG');
        $email->setCustomer($customer)->send($data,$customer->email);
    }


    static function privateMessage($customer_id,$message_id,$message,$order_id = 0, $reason){
        $thread_id = self::generateThreadId();
        if($message_id > 0){
            $obj = Message::find($message_id);
            if($obj){
                $thread_id = $obj->thread_id;
            }
        }
        $msg = new Message();
        $type = 'C';
        $msg->order_id = (int)$order_id;
        $msg->customer_id = (int)$customer_id;
        $msg->message = $message;
        $msg->reason = $reason;
        $msg->type = $type;
        $msg->thread_id = (int)$thread_id;
        $msg->save();
        if($msg->id > 0){
            $msg->sendEmail();
            return $msg->id;
        }
        return 0;
    }



    static function getLatestThreads(){



        $rows = Message::leftJoin('customers','messages.customer_id','=','customers.id')
            ->leftJoin('orders','messages.order_id','=','orders.id')
            ->leftJoin('users','messages.created_by','=','users.id')
            ->where('messages.type','C')
            ->where("messages.id","=", DB::raw("(SELECT MAX(id) from messages M2 WHERE messages.thread_id=M2.thread_id AND M2.type='C')"))
            ->groupBy("messages.thread_id")
            ->select( 'thread_id', 'message', 'messages.created_at','messages.id','customers.name','orders.reference','users.email')
            ->orderBy('created_at','desc')
            ->take(5)
            ->get();

        foreach($rows as $row){
            $obj = Message::getObj($row->id);
            $row->created = \Format::datetime($row->created_at);
            $row->reference = ($row->reference != '') ? "<span class='label label-inverse'>{$row->reference}</span>" : '-';
            $row->total = $obj->count;
            $row->sender = $obj->sender;
            //$message = substr($obj->message,0,150)."...";
            $row->link = \URL::action("MessagesController@getEdit", ['id' => $row->id]);
            //$formattedMessage = "<img src='$obj->image' width='32' class='pull-left mt5 mr10 ml5' /><strong><a href='$link'>Inviato da $sender</a></strong><br><em>$message</em>";
            $row->image = $obj->image;
            $row->sender = $obj->sender;
            $row->message = nl2br($row->message);
        }
        return $rows;
    }

    
}