<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Module
 *
 * @property integer $id
 * @property integer $position
 * @property string $mod_position
 * @property string $mod_type
 * @property boolean $showtitle
 * @property string $override
 * @property string $acl_groups
 * @property string $starting_at
 * @property string $finishing_at
 * @property string $content
 * @property string $content_alt
 * @property string $params
 * @property string $conditions
 * @property boolean $cache
 * @property integer $cache_ttl
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Module whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereModPosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereModType($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereShowtitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereOverride($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereAclGroups($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereStartingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereFinishingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereContentAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereConditions($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCache($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCacheTtl($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Module extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'modules';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "published", "input", "input_alt", "input_textarea", "input_textarea_alt", "input_text", "input_text_alt", "input_int", "input_int_alt", "input_textarea_mobile");
    protected $db_fields = array("mod_position", "mod_type", "position", "showtitle", "starting_at", "finishing_at", "override", "acl_groups", "params", "content", "content_alt", "cache", "cache_ttl", "conditions");
    protected $lang_model = 'Module_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "published" => 0
    );

    static function getPosition($mod_position){
        \Utils::watch();
        $position = (int)\Module::where("mod_position",$mod_position)->max("position");
        \Utils::log($position,"CURRENT MAX POSITION");
        return $position + 1;
    }


    public function moveUp() {
        $instance = new static;
        $obj = $this;
        $position = $obj->position;


        //update previous - multi
        $query = $instance::where('position', '<', $position - 1);

        $query->where('mod_position', $obj->mod_position);

        $query->update(array('position' => DB::raw("position-1")));

        //update previous - nearest
        $query = $instance::where('position', '=', $position - 1);

        $query->where('mod_position', $obj->mod_position);

        $query->update(array('position' => $position));

        //update single
        $obj->position = $position - 1;
        $obj->save();

        $instance::reorder($obj->mod_position);
    }

    public function moveDown() {
        $instance = new static;
        $obj = $this;
        $position = $obj->position;
        $parent_id = $obj->parent_id OR false;

        //update next - multi
        $query = $instance::where('position', '>', $position + 1);

        $query->where('mod_position', $obj->mod_position);

        $query->update(array('position' => DB::raw("position+1")));

        //update next - nearest
        $query = $instance::where('position', '=', $position + 1);

        $query->where('mod_position', $obj->mod_position);

        $query->update(array('position' => $position));

        //update single
        $obj->position = $position + 1;
        $obj->save();

        $instance::reorder($obj->mod_position);
    }


    public static function reorder($parent_id = false) {
        $instance = new static;
        $query = $instance->where('id', '>', 0);

        $query->where('mod_position', $parent_id);

        $rows = $query->orderBy('position')->get();
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $instance->where('id', $row->id)->update(array('position' => $counter));
        }
    }




    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new ModulePresenter($this);
    }

}

class ModulePresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

    public function presentStarting() {
        return Format::datetime($this->starting_at);
    }

    public function presentFinishing() {
        return Format::datetime($this->finishing_at);
    }

}



/**
 * Module_Lang
 *
 * @property integer $module_id
 * @property string $lang_id
 * @property string $name
 * @property boolean $published
 * @property string $input
 * @property string $input_alt
 * @property string $input_textarea
 * @property string $input_textarea_alt
 * @property string $input_textarea_mobile
 * @property string $input_text
 * @property string $input_text_alt
 * @property integer $input_int
 * @property integer $input_int_alt
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereModuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInput($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputTextarea($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputTextareaAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputTextareaMobile($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputText($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputTextAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputInt($value)
 * @method static \Illuminate\Database\Query\Builder|\Module_Lang whereInputIntAlt($value)
 * @property-read \Module $module
 */
class Module_Lang extends Eloquent  {


    protected $table = 'modules_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function module()
    {
        return $this->belongsTo('Module');
    }


}