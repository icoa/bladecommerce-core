<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * TaxGroupRule
 *
 * @property integer $id
 * @property integer $tax_rules_group_id
 * @property integer $country_id
 * @property integer $state_id
 * @property string $zipcode
 * @property string $zipcode_from
 * @property string $zipcode_to
 * @property integer $tax_id
 * @property integer $behavior
 * @property string $description
 * @property boolean $active
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereTaxRulesGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereZipcode($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereZipcodeFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereZipcodeTo($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereTaxId($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereBehavior($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\TaxGroupRule whereActive($value)
 */
class TaxGroupRule extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'taxes_rules';
    protected $guarded = array();
    protected $softDelete = false;
    public $timestamps = false;
    protected $isBlameable = false;
    
    
    public $db_fields = array(
        "tax_rules_group_id",
        "country_id",
        "state_id",
        "zipcode_from",
        "zipcode_to",
        "tax_id",
        "behaviour",
        "description",
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new TaxGroupRulePresenter($this);
    }

    
}

class TaxGroupRulePresenter extends Presenter {
    

}
