<?php

use Illuminate\Database\Eloquent\Model;

/**
 * SingleModel
 *
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 */
class SingleModel extends Eloquent
{

    use \Echo1\Blameable;

    const CLONE_UNIQUE_SLUG = 255;
    const CLONE_UNIQUE_TEXT = 254;
    const CLONE_RANDOM_TEXT = 253;

    protected $db_fields_cloning = array();
    protected $blameable = array("created", "updated", "deleted");
    protected $isBlameable = TRUE;
    protected $dataSource = null;
    public $trigger_events = FALSE;
    public $broadcast_events = true;
    protected $defaults = [];


    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
    }

    function fresh()
    {
        if ($this->id > 0) {
            $model = self::find($this->id);
            if ($model) {
                $this->setAttributes($model->getAttributes());
            }
        }
    }

    function setAttributes($attributes)
    {
        if (is_array($attributes)) {
            foreach ($attributes as $key => $value) {
                $this->setAttribute($key, $value);
            }
        }
    }

    function setDataSource($ds)
    {
        /*$languages = \Core::getLanguages();
        if(is_array($ds)){
            foreach($this->dataSource as $key => $value){
                if(in_array($key,$languages) and is_array($value)){

                }
            }
        }      */
        $this->dataSource = is_array($ds) ? $ds : false;
        if ($this->dataSource) {
            $this->trigger_events = TRUE;

        }
    }

    function getDataSource()
    {
        return (is_null($this->dataSource)) ? $_POST : $this->dataSource;
    }

    function _before_create()
    {

    }

    function _after_create()
    {

    }

    function _before_update()
    {

    }

    function _after_update()
    {

    }

    function _before_clone(&$model)
    {
        if (count($this->db_fields_cloning) > 0) {
            foreach ($this->db_fields_cloning as $key => $rule) {
                $this->_handle_cloning_rule($model, $key, $rule);
            }
        }
    }

    function _after_clone(&$source, &$model)
    {

    }

    function _before_delete()
    {
        if ($this->forceDeleting == false) {
            return;
        }
        $this->deleting_id = $this->id;
    }

    function _after_delete()
    {

    }

    private function _handle_cloning_rule(&$model, $key, $rule)
    {
        $class_name = get_class($model);
        //\Utils::log($class_name,"CLASS NAME");
        $instance = new $class_name;
        switch ($rule) {
            case SingleModel::CLONE_UNIQUE_SLUG:
                $query = $instance::where($key, 'LIKE', $model->$key . '%');
                $cnt = $query->count();
                $cnt++;
                $model->$key = $model->$key . "-" . $cnt;
                break;

            case SingleModel::CLONE_UNIQUE_TEXT:
                $query = $instance::where($key, 'LIKE', $model->$key . '%');
                $cnt = $query->count();
                $cnt++;
                $model->$key = $model->$key . " (copy " . $cnt . ")";
                break;

            case SingleModel::CLONE_RANDOM_TEXT:
                $model->$key = \Core::randomString();
                break;

            default:
                $model->$key = $rule;
                break;
        }
    }

    function cloneMe(array $attributes = array())
    {
        $source = $this;
        $obj = $source->replicate();
        $this->_before_clone($obj);
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $val) {
                $obj->setAttribute($key, $val);
            }
        }
        $time = $this->freshTimestamp();
        $obj->setCreatedAt($time);
        $obj->setUpdatedAt($time);
        //$obj->updateTimestamps();
        $obj->save();

        $this->_after_clone($source, $obj);
    }

    function bindModelData()
    {
        $ds = $this->getDataSource();
        //\Utils::log($ds, "bindModelData DATASOURCE");
        if (count($this->db_fields) > 0) {
            foreach ($this->db_fields as $field) {

                if (isset($ds[$field])) {
                    //\Utils::log("a) SETTING ATTRIBUTE $field TO {$ds[$field]}");
                    $this->setAttribute($field, $ds[$field]);
                } else {
                    $attribute = $this->getAttribute($field);
                    if (isset($attribute)) {
                        try {
                            if ($ds[$field] == null AND $attribute != $ds[$field]) {
                                //\Utils::log("b) SETTING ATTRIBUTE $field TO NULL");
                                $this->setAttribute($field, null);
                            } else {
                                //\Utils::log("c) LEAVING ATTRIBUTE $field");
                            }
                        } catch (\Exception $e) {
                            //\Utils::log("d) LEAVING ATTRIBUTE $field");
                            //$this->setAttribute($field, null);
                        }
                    } else {
                        //\Utils::log("f) LEAVING ATTRIBUTE $field");
                        /*if($ds[$field] == null){
                            \Utils::log("e) SETTING ATTRIBUTE $field TO NULL");
                            $this->setAttribute($field, null);
                        }else{

                        }*/

                    }

                }
            }
        }
    }


    function creatingSetData()
    {
        if ($this->trigger_events) {
            $this->_before_create();
            $this->bindModelData();
        }
    }

    function createdSetData()
    {
        if ($this->trigger_events) {
            $this->_after_create();
        }
    }

    function updatingSetData()
    {
        if ($this->trigger_events) {
            $this->_before_update();
            $this->bindModelData();
            $this->_after_update();
        }
    }

    function deletingSetData()
    {

    }

    public static function reorder($parent_id = false)
    {
        $instance = new static;
        $query = $instance->where('id', '>', 0);
        if ($parent_id != false) {
            $query->where('parent_id', $parent_id);
        }
        $rows = $query->orderBy('position')->get();
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $instance->where('id', $row->id)->update(array('position' => $counter));
        }
    }

    public function moveUp()
    {
        $instance = new static;
        $obj = $this;
        $position = $obj->position;
        $parent_id = $obj->parent_id OR false;

        //update previous - multi
        $query = $instance::where('position', '<', $position - 1);
        if ($parent_id) {
            $query->where('parent_id', $obj->parent_id);
        }
        $query->update(array('position' => DB::raw("position-1")));

        //update previous - nearest
        $query = $instance::where('position', '=', $position - 1);
        if ($parent_id) {
            $query->where('parent_id', $obj->parent_id);
        }
        $query->update(array('position' => $position));

        //update single
        $obj->position = $position - 1;
        $obj->save();

        $instance::reorder($parent_id);
    }

    public function moveDown()
    {
        $instance = new static;
        $obj = $this;
        $position = $obj->position;
        $parent_id = $obj->parent_id OR false;

        //update next - multi
        $query = $instance::where('position', '>', $position + 1);
        if ($parent_id) {
            $query->where('parent_id', $obj->parent_id);
        }
        $query->update(array('position' => DB::raw("position+1")));

        //update next - nearest
        $query = $instance::where('position', '=', $position + 1);
        if ($parent_id) {
            $query->where('parent_id', $obj->parent_id);
        }
        $query->update(array('position' => $position));

        //update single
        $obj->position = $position + 1;
        $obj->save();

        $instance::reorder($parent_id);
    }

    public static function position($parent_id = false)
    {
        $instance = new static;
        $query = $instance->where('id', '>', 0);
        if ($parent_id != false) {
            $query->where('parent_id', $parent_id);
        }
        $position = $query->max('position');
        return $position + 1;
    }

    public function canDelete()
    {
        return true;
    }

    protected function expand()
    {
        return $this;
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasObj($id)
    {
        if (is_numeric($id) and $id <= 0)
            return false;

        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-obj-$id");
        return \Registry::has($cache_key);
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection|mixed|SingleModel|null
     */
    public static function getObj($id)
    {
        if (is_numeric($id) and $id <= 0)
            return null;

        if ($id === null)
            return null;

        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-obj-$id");
        if (\Registry::has($cache_key)) {
            return \Registry::get($cache_key);
        }
        $obj = $instance::find($id);
        \Registry::set($cache_key, $obj);
        return $obj;
    }

    public static function getPublicObj($id, $lang_id = 'it')
    {
        if (is_numeric($id) and $id <= 0)
            return null;

        if ($id === null)
            return null;

        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-public-obj-$id");
        //\Cache::forget($cache_key);

        if (\Registry::has($cache_key)) {
            //\Utils::log($cache_key,"getPublicObj CACHE HIT");
            return \Registry::get($cache_key);
        }
        $obj = \Cache::remember($cache_key, 60 * 6, function () use ($id, $instance, $cache_key) {
            //audit($cache_key, 'SAVE');
            $obj = $instance::where("active", 1)->where("id", $id)->first();
            if ($obj) {
                return $obj->expand();
            }
            return null;
        });

        \Registry::set($cache_key, $obj);


        return $obj;
    }

    public function uncache()
    {
        $id = $this->id;
        $cache_key = \Str::lower(get_class($this) . "-public-obj-$id");
        \Cache::forget($cache_key);
    }

    /**
     * Overrides the model's booter to register the event hooks
     */
    public static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if ($model->exists) {
                $model->updateBlameables();
                $model->updatingSetData();
            } else {
                $model->updateBlameables();
                $model->creatingSetData();
            }
            \Utils::unwatch();
        });

        static::created(function ($model) {
            $model->createdSetData();
        });


        static::deleting(function ($model) {
            $model->_before_delete();
            if ($model->softDelete == true) {
                if ($model->deleted_at == null) {
                    $model->updateDeleteBlameable();
                }
            }
        });

        static::deleted(function ($model) {
            $model->_after_delete();
        });
    }


    protected function getDefaults()
    {
        return $this->defaults;
    }


    /**
     * @param $id
     * @param string $lang_id
     */
    public function hydrateAttributes($id, $lang_id = 'default')
    {
        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-obj-$id");
        \Registry::set($cache_key, $this);
    }

    /**
     * @param $class
     * @param string $lang_id
     * @param null $relation
     * @param null $foreignKey
     * @return Model|null
     */
    public function loadMissing($class, $lang_id = 'default', $relation = null, $foreignKey = null)
    {
        $relation = is_null($relation) ? snake_case($class) : $relation;

        if (is_null($foreignKey)) {
            $foreignKey = snake_case($relation) . '_id';
        }

        if ($class::hasObj($this->$foreignKey, $lang_id))
            return $class::getObj($this->$foreignKey, $lang_id);

        $this->load($relation);
        if (isset($this->$relation) and !is_null($this->$relation)) {
            /** @var SingleModel|MultiLangModel $loaded_relation */
            $loaded_relation = $this->$relation;
            $loaded_relation->hydrateAttributes($this->id, $lang_id);
            return $loaded_relation;
        }

        return null;
    }


    function make($attributes, $returnOnly = false)
    {

        $defaults = $this->getDefaults();

        $attributes = array_merge($defaults, $attributes);

        $this->setDataSource($attributes);
        $this->fill($attributes);

        if ($returnOnly)
            return $attributes;

        $this->save();
    }

}
