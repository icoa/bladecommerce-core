<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * ImageSet
 *
 * @property integer $id
 * @property string $uname
 * @property string $youtube
 * @property string $video
 * @property integer $position
 * @property boolean $published
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereUname($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereYoutube($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereVideo($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\ImageSet whereDeletedAt($value)
 */
class ImageSet extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'images_sets';
    protected $guarded = array();

    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $db_fields = array(
        "uname",
        "youtube",
        "video",
        "published",
        "position",
    );
    protected $db_fields_cloning = array(
        "uname" => SingleModel::CLONE_UNIQUE_TEXT,
        "published" => 0
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new ImageSetPresenter($this);
    }

    function _after_clone(&$source, &$model) {
        $source_id = $source->id;
        $target_id = $model->id;

        /* CLONING RULES */
        $rules = DB::table('images_sets_rules')->where('image_set_id', $source->id)->get();

        foreach ($rules as $c) {
            DB::table('images_sets_rules')->insert([
                'image_set_id' => $model->id,
                'target_type' => $c->target_type,
                'target_mode' => $c->target_mode,
                'target_id' => $c->target_id,
                'position' => $c->position,
            ]);
        }

        /* CLONING IMAGES */
        $rows = DB::table('images_groups')->where("set_id", $source_id)->orderBy('position')->get();
        DB::table('images_groups')->where('set_id', $target_id)->delete();
        $savePath = public_path() . "/assets/groups/";
        foreach ($rows as $row) {

            $image_file = $savePath . $row->filename;
            $extension = File::extension($image_file);

            try {

                $insert_id = DB::table('images_groups')->insertGetId([
                    'set_id' => $target_id,
                    'filename' => "",
                    'position' => $row->position,
                    'active' => $row->active,
                    'updated_by' => $row->updated_by,
                    'updated_at' => $row->updated_at,
                    'created_by' => $row->created_by,
                    'created_at' => $row->created_at,
                ]);

                $target_file = $savePath . $insert_id.".".$extension;

                File::copy($image_file, $target_file);

                DB::table('images_groups')->where('id',$insert_id)->update(['filename' => $insert_id.".".$extension]);

                $images_lang = DB::table('images_groups_lang')->where("group_image_id", $row->id)->get();

                foreach ($images_lang as $il) {
                    DB::table('images_groups_lang')->insert([
                        'group_image_id' => $insert_id,
                        'lang_id' => $il->lang_id,
                        'legend' => $il->legend,
                    ]);
                }
            } catch (Exception $ex) {
                
            }
        }

        $products = DB::table('cache_products_sets')->where('image_set_id', $source->id)->get();
        foreach($products as $p){
            DB::table('cache_products_sets')->insert(['image_set_id' => $target_id, 'product_id' => $p->product_id]);
        }
    }

    function _before_delete() {
        if ($this->forceDeleting == false) {
            return;
        }
        $id = $this->id;
        \Utils::watch();
        DB::table('images_sets_rules')->where('image_set_id', $id)->delete();
        DB::table('cache_products_sets')->where('image_set_id', $id)->delete();

        $rows = DB::table('images_groups')->where("set_id", $id)->orderBy('position')->get();
        $savePath = public_path() . "/assets/groups/";

        foreach ($rows as $row) {
            try {
                $file = $savePath . $row->filename;
                \File::delete($file);
                $obj = \GroupImage::destroy($row->id);
            } catch (Exception $exc) {
                //echo $exc->getTraceAsString();
            }
        }
        \Utils::unwatch();
    }

}

class ImageSetPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}
