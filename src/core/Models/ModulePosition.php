<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * ModulePosition
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property boolean $is_design
 * @property integer $row_index
 * @property integer $position
 * @property boolean $width
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition whereIsDesign($value)
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition whereRowIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\ModulePosition whereWidth($value)
 */
class ModulePosition extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'module_positions';
    protected $guarded = array();
    protected $isBlameable = FALSE;
    public $timestamps = FALSE;
    public $softDelete = false;
    public $db_fields = array(
        "name"
    );

    public function canDelete(){
        $total = \Module::where("mod_position",$this->name)->count("id");
        if($total > 0){
            return "Non è possibile eliminare questo elemento in quanto è riferito da altre entità";
        }
        return true;
    }

    public function getPublicName($lang){
        return $this->name;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new ModulePositionPresenter($this);
    }

}

class ModulePositionPresenter extends Presenter {


}