<?php
namespace Echo1;

class Translatable extends \Eloquent
{
    /**
     * The current language of the model.
     *
     * @var Language
     */
    public $lang;
    
    
     /**
     * Overrides the model's booter to register the event hooks
     */
    /*public static function boot()
    {
        parent::boot();        
        self::setTranslationTable();
    }*/
    function __construct(array $attributes = array()) {
        parent::__construct($attributes);
        $this->setTranslationTable();
    }
    
    
    public function setTranslationTable(){
        \Utils::log($this->table_lang);
        \Utils::log($this->table_lang_column);
        \Utils::log($this->id);
        $rows = \DB::table($this->table_lang)->where($this->table_lang_column, $this->id)->get();
        \Utils::log($rows);
        foreach($rows as $row){
            $lang = $row->lang_id;
            foreach($this->translatable as $field){
                $key = $field."_".$lang;
                $this->{$key} = $row->{$field};
            }
        }
    }
    

    /**
     * Set the modelâ€™s current language.
     *
     * @param Language $lang  An instance of Language
     */
    public function setLanguage($lang = null)
    {
        $this->lang = $lang;
    }

    /**
     * Magic getter.
     *
     * @param  string $key  The translation or attribute to get
     * @return mixed
     */
    public function __get($key)
    {
        // If the requested attribute is translatable, try to get a translation
        if ($this->isTranslatable($key) && $translation = $this->getTranslation($key)) {
            return $translation;
        }
        // Fall back on Illuminate\Database\Eloquent\Model::getAttribute()
        else {
            return $this->getAttribute($key);
        }
    }

    /**
     * Magic setter.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function __set($key, $value)
    {
        // If the requested attribute is translatable, set a translation
        if ($this->isTranslatable($key)) {
            $this->setTranslation($key, $value);
        }
        // Fall back on Illuminate\Database\Eloquent\Model::setAttribute()
        else {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * Check if a key is translatable.
     *
     * @param  string $key
     * @return boolean
     */
    public function isTranslatable($key)
    {
        return in_array($key, $this->translatable);
    }

    /**
     * Check if a translation exist.
     *
     * @param  string   $key
     * @param  Language $lang
     * @return boolean
     */
    public function hasTranslation($key, $lang = null)
    {
        return !! ($this->getTranslation($key, $lang));
    }

    /**
     * Get a Translation instance for the given key and lang.
     *
     * @param  string   $key   The key to translate
     * @param  Language $lang  The language to get a translation for
     * @return Translation|null
     */
    public function getTranslation($key, $lang = null)
    {
        
        if ($this->isTranslatable($key)) {
            $field = $key."_".$lang;
            return $this->{$field};
        }
    }

    /**
     * Set a translation for the given key and lang.
     *
     * @param string $key    The key to translate
     * @param string $value  The translation
     * @param Language $lang
     */
    public function setTranslation($key, $value, $lang = null)
    {
        

        if ($this->isTranslatable($key)) {

            $field = $key."_".$lang;
            $this->{$field} = $value;
        }
    }

    /**
     * Delete a translation for the given key and lang.
     *
     * @param  string   $key
     * @param  Language $lang
     */
    public function forgetTranslation($key, Language $lang = null)
    {
        $lang_id = $lang ? $lang->id : $this->lang->id;

        if ($this->isTranslatable($key)) {

            if ($translation = $this->getTranslation($key, $lang)) {
                $translation->delete();
            }
        }
    }
}