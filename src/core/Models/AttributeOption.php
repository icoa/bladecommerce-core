<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * AttributeOption
 *
 * @property integer $id
 * @property integer $nav_id
 * @property string $uname
 * @property string $uslug
 * @property integer $attribute_id
 * @property boolean $is_default
 * @property integer $position
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read mixed $title
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereNavId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereUname($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereUslug($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereAttributeId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption whereUpdatedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class AttributeOption extends MultiLangModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'attributes_options';
    protected $guarded = array();
    public $has_slug = false;

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array(
        "name",
        "slug",
        "special"
    );
    protected $db_fields = array(
        "uname",
        "uslug",
        "attribute_id",
        "is_default",
        "position",
        "nav_id",
    );
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $lang_model = 'AttributeOption_Lang';

    protected $defaults = [
        'is_default' => 0,
        'active' => 1,
    ];

    protected function getDefaults(){
        $defaults = $this->defaults;
        $defaults['position'] = AttributeOption::max('position') + 1;
        return $defaults;
    }

    function _before_delete()
    {
        $obj = $this;
        if ($this->forceDeleting == false) {
            return;
        }
        \Utils::log("BEFORE FORCE DELETE");
        \DB::table("products_attributes")->where('attribute_val', $obj->id)->where('attribute_id', $obj->attribute_id)->delete();
        \DB::table("seo_texts_rules")->where('target_id', $obj->id)->where('target_type', 'ATT')->delete();
        \DB::table("seo_blocks_rules")->where('target_id', $obj->id)->where('target_type', 'ATT')->delete();
        \DB::table("images_sets_rules")->where('target_id', $obj->id)->where('target_type', 'ATT')->delete();
        \DB::table("products_combinations_attributes")->where('option_id', $obj->id)->delete();
    }

    function getPublicName($lang = null)
    {

        if ($lang) {
            if (isset($this->$lang->name) AND $this->$lang->name != '') {
                return $this->$lang->name;
            }
            return $this->uname;
        } else {
            if (isset($this->name) AND $this->name != '') {
                return $this->name;
            }
            return $this->uname;
        }
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new AttributeOptionPresenter($this);
    }


    public static function reinitCache($attribute_id)
    {
        $instance = new static;
        $rows = $instance::where('attribute_id', $attribute_id)->get();

        foreach ($rows as $row) {
            $row->flushCache();
        }
    }


    function setCategories(){
        $query = "select distinct(category_id) from categories_products where product_id in (select product_id from products_attributes where attribute_id=? AND attribute_val=?)";
        $rows = DB::select($query,[$this->attribute_id,$this->id]);
        $data = [];
        foreach($rows as $row){
            $data[] = $row->category_id;
        }
        $this->categories = $data;
    }

    function setBrands(){
        $query = "select distinct(brand_id) from products where id in (select product_id from products_attributes where attribute_id=? AND attribute_val=?)";
        $rows = DB::select($query,[$this->attribute_id,$this->id]);
        $data = [];
        foreach($rows as $row){
            $data[] = $row->brand_id;
        }
        $this->brands = $data;
    }

    function getCategories(){
        return isset($this->categories) ? $this->categories : [];
    }

    function getBrands(){
        return isset($this->brands) ? $this->brands : [];
    }

    function getTitleAttribute(){
        if (isset($this->name) AND $this->name != '') {
            return $this->name;
        }
        return $this->uname;
    }

}

class AttributeOptionPresenter extends Presenter
{

    public function presentCdate()
    {
        return Format::datetime($this->created_at);
    }

    public function presentFullname($lang_id)
    {
        if ($this->{$lang_id}->name == "") {
            return $this->uname;
        } else {
            return $this->{$lang_id}->name;
        }
    }

    public function presentCompletename()
    {
        if ($this->name == "") {
            return $this->uname;
        }
        return $this->name;
    }

}
