<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Tag
 *
 * @property integer $id
 * @property string $lang_id
 * @property string $tag
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Tag whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Tag whereTag($value)
 */
class Tag extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'tags';
    protected $guarded = array();
    protected $isBlameable = FALSE;
    public $timestamps = FALSE;
    public $softDelete = false;
    public $db_fields = array(
        "lang_id",
        "tag",
    );
    
    function _before_delete() {
        $id = $this->id;
        \Utils::watch();
        DB::table('products_tags')->where('tag_id', $id)->delete();
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new TagPresenter($this);
    }

}

class TagPresenter extends Presenter {

    function presentProductsList() {
        $lang = \Core::getLang();
        $tag_id = $this->id;

        $options = [];
        $data = \Product::whereIn("id", function($query) use($tag_id) {
                    $query->select('product_id')
                            ->from("products_tags")
                            ->where('tag_id', $tag_id);
                })->get();



        if ($data->count() > 0) {
            foreach ($data as $row) {
                $options[$row->id] = $row->{$lang}->name. " ($row->sku)";
            }
        }
        return $options;
    }

    function presentProducts() {
        $lang = \Core::getLang();
        $tag_id = $this->id;
        $data = DB::table("products_tags")->where('tag_id', $tag_id)->lists('product_id');
        return $data;
    }

}
