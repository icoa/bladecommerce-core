<?php



/**
 * GoogleAnalytic
 *
 * @property integer $id
 * @property string $account
 * @property string $name
 * @property boolean $active
 * @property boolean $overall
 * @property boolean $debug
 * @property string $domains
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $updated_by
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereAccount($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereOverall($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereDebug($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereDomains($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GoogleAnalytic whereUpdatedBy($value)
 */
class GoogleAnalytic extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'google_analytics';
    protected $guarded = array();
    public $paramsLoaded;

    
    public $db_fields = array(
        "account",
        "name",
        "active",
        "overall",
        "debug",
        "domains",
        "params",
    );

    function expand(){
        if($this->params != ''){
            $params = ModuleHelper::getParams($this->params);
            $this->paramsLoaded = $params;
        }
        return $this;
    }

    function getParam($key,$default = null){
        if(!isset($this->paramsLoaded)){
            $this->expand();
        }
        if(!isset($this->paramsLoaded[$key]) OR ($this->paramsLoaded[$key] == null OR $this->paramsLoaded[$key] == '')){
            return $default;
        }
        return $this->paramsLoaded[$key];
    }
    
}