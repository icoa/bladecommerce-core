<?php

/**
 * MultiLangModel
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class MultiLangModel extends SingleModel
{

    protected $lang_fields = array();
    protected $lang_fields_cloning = array();
    protected $_lang_setted = false;
    protected $lang_model = 'Generic_Lang';
    public $has_published = true;
    public $has_slug = true;

    public function translations()
    {
        return $this->hasMany($this->lang_model);
    }

    public function translation($lang_id)
    {
        return $this->hasOne($this->lang_model)->where('lang_id', $lang_id);
    }

    public function current_translation()
    {
        return $this->hasOne($this->lang_model)->where('lang_id', \Core::getLang());
    }

    public function it()
    {
        return $this->hasOne($this->lang_model)->where('lang_id', 'it');
    }

    public function en()
    {
        return $this->hasOne($this->lang_model)->where('lang_id', 'en');
    }

    public function es()
    {
        return $this->hasOne($this->lang_model)->where('lang_id', 'es');
    }

    public function fr()
    {
        return $this->hasOne($this->lang_model)->where('lang_id', 'fr');
    }

    public function de()
    {
        return $this->hasOne($this->lang_model)->where('lang_id', 'de');
    }

    public function scopeLang($query, $lang_id)
    {
        return $query->where('lang_id', $lang_id);
    }

    function getPublicName($lang = null)
    {
        if ($lang) {
            if (isset($this->$lang->name) AND $this->$lang->name != '') {
                return $this->$lang->name;
            }
            return "";
        } else {
            if (isset($this->name) AND $this->name != '') {
                return $this->name;
            }
            return "";
        }
    }

    public function scopeRows($query, $lang_id = null)
    {
        $langModel = new $this->lang_model;
        $lang_id = ($lang_id != null) ? $lang_id : \Core::getLang();
        $table = $langModel->getTable();
        $key = $this->getKeyName();
        $foreignKey = $this->getForeignKey();
        return $query->join($table, $key, '=', $foreignKey)->where('lang_id', '=', $lang_id);
    }


    public static function getSlug($id, $lang_id)
    {
        $instance = new static;
        $slug = $instance->fromCache($id, 'slug', $lang_id);
        if ($slug == '') {
            $slug = false;
        }
        return $slug;
    }

    public static function getUslug($id, $lang_id)
    {
        $slug = self::getSlug($id, $lang_id);
        //\Utils::log("[$slug]",__METHOD__);
        if ($slug == null or $slug == false or $slug == '') {
            $instance = new static;
            $slug = $instance->fromCache($id, 'uslug');
            if ($slug == '') {
                $obj = $instance::find($id);
                if ($obj) {
                    $obj->flushCache();
                    return self::getUslug($id, $lang_id);
                } else {
                    return null;
                }
            }
        }

        return $slug;
    }

    public function getLangModel()
    {
        return $this->lang_model;
    }

    public function getLangFields()
    {
        return $this->lang_fields;
    }


    public static function getFullObj($id, $lang_id = 'it')
    {
        $obj = self::getObj($id, $lang_id);
        return $obj->expand();
    }


    public static function getPublicObj($id, $lang_id = 'default', $avoid_cache = false)
    {

        if ($lang_id == 'default') {
            $lang_id = \Core::getLang();
        }
        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-public-obj-$lang_id-$id");
        //\Utils::log($cache_key,__METHOD__);
        //\Cache::forget($cache_key);

        if (\Registry::has($cache_key)) {
            //\Utils::log($cache_key,"getPublicObj CACHE HIT");
            return \Registry::get($cache_key);
        }
        $obj = \Cache::remember($cache_key, 60 * 6, function () use ($lang_id, $id, $instance, $cache_key) {
            //audit($cache_key, 'SAVE');
            $field = $instance->has_published ? 'published' : 'active';
            $obj = $instance::rows($lang_id)->where($field, 1)->where("id", $id)->first();
            if ($obj) {
                return $obj->expand();
            }
            return null;
        });

        if ($obj == null and !$avoid_cache) {
            \Cache::forget($cache_key);
            return self::getPublicObj($id, $lang_id, true);
        }

        \Registry::set($cache_key, $obj);


        return $obj;
    }

    public function uncache()
    {
        $languages = \Core::getLanguages();
        foreach ($languages as $lang_id) {
            $id = $this->id;
            $cache_key = \Str::lower(get_class($this) . "-public-obj-$lang_id-$id");
            \Cache::forget($cache_key);
        }
    }

    function _after_update()
    {
        try {
            $this->uncache();
            $this->flushCache();
        } catch (\Exception $e) {

        }

    }

    public static function unpublic($id)
    {
        $instance = new static;
        $languages = \Core::getLanguages();
        foreach ($languages as $lang_id) {
            $cache_key = \Str::lower(get_class($instance) . "-public-obj-$lang_id-$id");
            \Cache::forget($cache_key);
        }
    }

    /**
     * @param $id
     * @param string $lang_id
     * @return bool
     */
    public static function hasObj($id, $lang_id = 'default'){
        if(is_numeric($id) and $id <= 0)
            return false;

        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-obj-$lang_id-$id");
        return \Registry::has($cache_key);
    }


    public static function getObj($id, $lang_id = 'default', $withTrashed = false)
    {
        if ($lang_id == 'default') {
            $lang_id = \Core::getLang();
        }
        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-obj-$lang_id-$id");
        if (\Registry::has($cache_key)) {
            return \Registry::get($cache_key);
        }
        $queryBuilder = $instance::with(array('translations' => function ($query) use ($lang_id) {
            $query->where('lang_id', '=', $lang_id);
        }));
        $obj = $withTrashed ? $queryBuilder->withTrashed()->find($id) : $queryBuilder->find($id);

        if ($obj) {
            $langData = $obj->translations[0];
            $attributes = $langData->getAttributes();

            foreach ($attributes as $key => $value) {
                $obj->setAttribute($key, $value);
            }
        }

        \Registry::set($cache_key, $obj);

        return $obj;
    }

    public function hydrateWithTranslation(){
        if(isset($this->current_translation)){
            $attributes = $this->current_translation->getAttributes();

            foreach ($attributes as $key => $value) {
                $this->setAttribute($key, $value);
            }
        }else{
            $this->hydrateAttributes($this->id);
        }
        return $this;
    }

    /**
     * @param $id
     * @param string $lang_id
     */
    public function hydrateAttributes($id, $lang_id = 'default')
    {
        if ($lang_id == 'default') {
            $lang_id = \Core::getLang();
        }
        $langData = $this->translations()->where('lang_id', '=', $lang_id)->first();
        if ($langData) {
            $attributes = $langData->getAttributes();

            foreach ($attributes as $key => $value) {
                $this->setAttribute($key, $value);
            }
        }
        $instance = new static;
        $cache_key = \Str::lower(get_class($instance) . "-obj-$lang_id-$id");
        \Registry::set($cache_key, $this);
    }

    public static function getObj2($id, $lang_id = 'it')
    {
        $instance = new static;
        return $instance::find($id)->translations()->where('lang_id', '=', $lang_id)->first();
    }

    protected function setLangAttributes()
    {
        if ($this->_lang_setted == false) {
            $rows = $this->translations;
            foreach ($rows as $row) {
                //$attributes = $row->getAttributes();
                $key = $row->lang_id;
                foreach ($this->lang_fields as $field) {
                    $this->setAttribute("{$field}_{$key}", $row->{$field});
                }
            }
        }
    }

    protected function getRedisPrefix($instance, $id = null)
    {
        $key = \Config::get('cache.prefix') . "-redis-" . strtolower(get_class($instance));
        if ($id) {
            $key .= '-' . $id;
        }
        return $key;
    }

    protected function initCache()
    {
        $instance = new static;
        $rows = $instance::get();
        //$className = strtolower(get_class($instance));
        foreach ($rows as $row) {

            //$master = 'redis-'.$className.'-'.$row->id;
            $master = $this->getRedisPrefix($instance, $row->id);
            $row->cacheRedisObject($master);

        }
    }

    public function getRedisMaster()
    {
        /*$className = strtolower(get_class($this));
        return 'redis-'.$className.'-'.$this->id;*/
        return $this->getRedisPrefix($this, $this->id);
    }

    protected function cacheRedisObject($master)
    {
        //\Utils::log($this->toArray(),"cacheRedisObject [$master]");
        if (isset($this->code)) {
            \Redis::hset($master, 'code', $this->code);
        }
        if (isset($this->uname)) {
            \Redis::hset($master, 'uname', $this->uname);
        }
        if (isset($this->uslug)) {
            \Redis::hset($master, 'uslug', $this->uslug);
        }

        $translations = $this->translations()->get();

        foreach ($translations as $tr) {
            //\Utils::log($tr->toArray(),"cacheRedisObjectLang [$tr->lang_id]");
            \Redis::hset($master, 'name_' . $tr->lang_id, $tr->name);
            if (isset($tr->slug)) {
                \Redis::hset($master, 'slug_' . $tr->lang_id, $tr->slug);
            }
        }
    }

    public function flushCache()
    {
        $master = $this->getRedisMaster();
        $this->cacheRedisObject($master);
    }

    protected function equivalentCache()
    {
        $master = $this->getRedisMaster();
        $this->cacheRedisObject($master);
    }

    protected function fromCache($id, $field, $lang_id = null)
    {
        //\Utils::log("id: $id | field: $field",__METHOD__);
        $instance = new static;
        //$className = strtolower(get_class($instance));
        //$master = 'redis-'.$className.'-'.$id;
        $master = $this->getRedisPrefix($instance, $id);
        if ($lang_id != null) {
            $field = $field . "_" . $lang_id;
        }
        try {
            $value = \Redis::hget($master, $field);
            //\Utils::log("master: $master | field: $field | value: $value",__METHOD__);
            if ($this->has_slug AND ($value == null OR $value == '')) {
                throw new \Exception("Could not find a valid Redis cache: Master: $master | Field: $field");
            }
            if ($field == 'uname' AND ($value == null OR $value == '')) {
                throw new \Exception("Could not find a valid Redis cache: Master: $master | Field: $field");
            }
            return $value;
        } catch (\Exception $e) {
            //\Utils::log($e->getMessage());
            $obj = $instance::find($id);
            if ($obj) {
                $obj->cacheRedisObject($master);
                $value = \Redis::hget($master, $field);
                return $value;
            }
        }
        return null;
    }

    /**
     * Fill the model with the languages attributes.
     *
     * @param array $attributes
     * @return \Illuminate\Database\Eloquent\Model|static
     */
    public function fillLanguages($setToModel = true)
    {
        $rows = $this->translations;
        $temp = array();
        foreach ($rows as $row) {
            //$attributes = $row->getAttributes();
            $key = $row->lang_id;
            foreach ($this->lang_fields as $field) {
                if ($setToModel) {
                    $this->setAttribute("{$field}_{$key}", $row->{$field});
                } else {
                    $temp["{$field}_{$key}"] = $row->{$field};
                }
            }
            if ($setToModel) {
                $this->setAttribute($key, true);
            } else {
                $temp[$key] = true;
            }
        }

        return ($setToModel) ? $this : $temp;
    }

    function _before_delete()
    {
        \Utils::log("CALLING BEFORE DELETE FOR MODEL", "_before_delete");
        if ($this->forceDeleting == false) {
            return;
        }
        $this->translations()->forceDelete();
    }

    function _before_clone_lang(&$model)
    {
        if (count($this->lang_fields_cloning) > 0) {
            foreach ($this->lang_fields_cloning as $key => $rule) {
                $this->_handle_cloning_rule($model, $key, $rule);
            }
        }
    }

    private function _handle_cloning_rule(&$model, $key, $rule)
    {
        $class_name = get_class($model);
        //\Utils::log($class_name,"CLASS NAME");
        $instance = new $class_name;
        switch ($rule) {
            case SingleModel::CLONE_UNIQUE_SLUG:
                $query = $instance::where($key, 'LIKE', $model->$key . '%');
                if (isset($model->lang_id)) {
                    $query->where('lang_id', $model->lang_id);
                }
                $cnt = $query->count();
                $cnt++;
                $model->$key = $model->$key . "-" . $cnt;
                break;

            case SingleModel::CLONE_UNIQUE_TEXT:
                $query = $instance::where($key, 'LIKE', $model->$key . '%');
                if (isset($model->lang_id)) {
                    $query->where('lang_id', $model->lang_id);
                }
                $cnt = $query->count();
                $cnt++;
                $model->$key = $model->$key . " (copy " . $cnt . ")";
                break;

            default:
                $model->$key = $rule;
                break;
        }
    }

    function cloneMe(array $attributes = array())
    {
        $source = $this;
        $tr = $source->translations;
        $lang_model = $source->getLangModel();
        $obj = $source->replicate();
        $this->_before_clone($obj);
        if (count($attributes) > 0) {
            foreach ($attributes as $key => $val) {
                $obj->setAttribute($key, $val);
            }
        }
        $time = $this->freshTimestamp();
        $obj->setCreatedAt($time);
        $obj->setUpdatedAt($time);
        //$obj->updateTimestamps();
        $obj->save();
        foreach ($tr as $langObj) {
            //\Utils::log($langObj->toArray(),"LANG OBJ");
            //\Utils::log($lang_model,"LANG MODEL");
            $data = $langObj->toArray();

            $trModel = new $lang_model($data);
            $this->_before_clone_lang($trModel);
            $obj->translations()->save($trModel);
        }
        $this->_after_clone($source, $obj);
    }


    function setLangFields(Array $a)
    {
        $this->lang_fields = $a;
    }

    function bindModelLangData($update = false)
    {
        $languages = \Mainframe::languagesCodes();
        $ds = $this->getDataSource();


        if (count($this->lang_fields) > 0) {
            $data = array();

            $hasSlug = false;
            if (is_array($this->lang_slaggable)) {
                $hasSlug = true;
                $slugFromField = $this->lang_slaggable['from'];
                $slugSaveField = $this->lang_slaggable['saveTo'];
            }

            foreach ($languages as $lang) {
                $data['lang_id'] = $lang;
                foreach ($this->lang_fields as $field) {
                    $key = $field . "_" . $lang;
                    //if (isset($ds[$key])) {
                    if (array_key_exists($key, $ds)) {
                        $value = $ds[$key];
                        $data[$field] = $value;
                        if ($hasSlug) {
                            if ($field == $slugSaveField) {
                                if ($value == '') { //set the slug only when the slug from the form is empty
                                    $data[$field] = \Str::slug($data[$slugFromField]);
                                }
                            }
                        }
                    } else {
                        //$data[$field] = null;
                    }
                }
                //\Utils::log($data, "bindModelLangData DATA");
                if (count($data) > 1) { //if the array is filled with only 'lang_id' no update or save should be done do the database
                    $tr = $this->translations();
                    $lang_record = $tr->where('lang_id', $lang)->count();
                    if ($lang_record == 0) { //SAVE NEW RECORD
                        $model = new $this->lang_model($data);
                        $tr->save($model);
                    } else { //UPDATE RECORD
                        //$q = $tr->getQuery();
                        //$q->where('lang_id', $lang);
                        $tr->update($data);
                    }
                }
            }
        }
    }

    function createdSetData()
    {
        if ($this->trigger_events) {
            $this->bindModelLangData();
            $this->_after_create();
        }
    }

    function updatingSetData()
    {
        if ($this->trigger_events) {
            $this->_before_update();
            $this->bindModelData();
            $this->bindModelLangData(true);
            $this->_after_update();
        }

    }


    protected function getDefaults()
    {
        return $this->defaults;
    }


    function make($attributes, $returnOnly = false)
    {
        $langData = [];
        $languages = \Core::getLanguages();
        $defaults = $this->getDefaults();
        $props = array_merge($defaults, $this->toArray());

        $attributes = array_merge($props, $attributes);

        foreach ($languages as $language) {
            if (isset($attributes[$language])) {
                $langData[$language] = $attributes[$language];
                unset($attributes[$language]);
                foreach ($langData[$language] as $key => $value) {
                    $attributes[$key . '_' . $language] = $value;
                }
            }
        }

        $this->setDataSource($attributes);

        if ($returnOnly)
            return $attributes;

        $this->save();
    }

}
