<?php
/**
 * Created by PhpStorm.
 * 
 * User: f.politi
 * Date: 01/09/14
 * Time: 14.53
 *
 */


class Model_Lang extends Eloquent {

    protected $guarded = array();
    public $timestamps = false;

    protected $isLang = true;

}