<?php


/**
 * PaymentState
 *
 * @property integer $id
 * @property boolean $invoice
 * @property boolean $send_email
 * @property string $module_name
 * @property string $color
 * @property string $icon
 * @property boolean $unremovable
 * @property boolean $hidden
 * @property boolean $logable
 * @property boolean $delivery
 * @property boolean $shipped
 * @property boolean $paid
 * @property boolean $failed
 * @property boolean $wait
 * @property boolean $active
 * @property boolean $deleted
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereInvoice($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereSendEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereModuleName($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereIcon($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereUnremovable($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereLogable($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereDelivery($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereShipped($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState wherePaid($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereFailed($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereWait($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState whereDeleted($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read mixed $code
 */
class PaymentState extends MultiLangModel
{

    const STATUS_PENDING_CHEQUE = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_PENDING_SHOP = 3;
    const STATUS_REFUND_REQUESTED = 4;
    const STATUS_DELIVERED = 5;
    const STATUS_USER_CANCELED = 6;
    const STATUS_REFUNDED = 7;
    const STATUS_ERROR = 8;
    const STATUS_REVERSED = 9;
    const STATUS_PENDING_BANKTRANSFER = 10;
    const STATUS_PENDING_PAYPAL = 11;
    const STATUS_REFUSED = 12;
    const STATUS_ACCEPTED_PAYPAL = 13;
    const STATUS_PENDING_CASH = 14;
    const STATUS_PENDING_CREDITCARD = 15;
    const STATUS_EXPIRED = 16;
    const STATUS_REFUNDED_PARTIALLY = 17;
    const STATUS_PENDING_RIBA = 18;

    /* LARAVEL PROPERTIES */
    protected $table = 'payment_states';
    protected $guarded = array();
    public $timestamps = false;


    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    protected $db_fields = array("invoice", "send_email", "module_name", "color", "unremovable", "logable", "delivery", "shipped", "paid");
    protected $lang_model = 'PaymentState_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;

    /**
     * @return array
     */
    static function statusToArray(){
        return [
            self::STATUS_PENDING_CHEQUE => 'STATUS_PENDING_CHEQUE',
            self::STATUS_COMPLETED => 'STATUS_COMPLETED',
            self::STATUS_PENDING_SHOP => 'STATUS_PENDING_SHOP',
            self::STATUS_REFUND_REQUESTED => 'STATUS_REFUND_REQUESTED',
            self::STATUS_DELIVERED => 'STATUS_DELIVERED',
            self::STATUS_USER_CANCELED => 'STATUS_USER_CANCELED',
            self::STATUS_REFUNDED => 'STATUS_REFUNDED',
            self::STATUS_ERROR => 'STATUS_ERROR',
            self::STATUS_REVERSED => 'STATUS_REVERSED',
            self::STATUS_PENDING_BANKTRANSFER => 'STATUS_PENDING_BANKTRANSFER',
            self::STATUS_PENDING_PAYPAL => 'STATUS_PENDING_PAYPAL',
            self::STATUS_REFUSED => 'STATUS_REFUSED',
            self::STATUS_ACCEPTED_PAYPAL => 'STATUS_ACCEPTED_PAYPAL',
            self::STATUS_PENDING_CASH => 'STATUS_PENDING_CASH',
            self::STATUS_PENDING_CREDITCARD => 'STATUS_PENDING_CREDITCARD',
            self::STATUS_EXPIRED => 'STATUS_EXPIRED',
            self::STATUS_REFUNDED_PARTIALLY => 'STATUS_REFUNDED_PARTIALLY',
            self::STATUS_PENDING_RIBA => 'STATUS_PENDING_RIBA',
        ];
    }

    /**
     * @param $status
     * @return string
     */
    static function statusToString($status){
        $array = self::statusToArray();
        return array_key_exists($status, $array) ? $array[$status] : 'STATUS_UNKNOWN';
    }

    /**
     * @param $status
     * @return int
     */
    static function statusToInt($status){
        $array = self::statusToArray();
        foreach($array as $key => $value){
            if($value === $status)
                return $key;
        }
        return 0;
    }

    function getName($formatted = false, $icon = 'font')
    {
        $record = $this;
        if ($record == null) {
            return null;
        }
        if ($formatted) {
            $icon = str_replace('font', $icon, $record->icon);
            $str = "<span class='label' style='background-color: $record->color'><i class='fa $icon'></i> $record->name</span>";
        } else {
            $str = $record->name;
        }
        return $str;
    }


    /**
     * @return string
     */
    function getCodeAttribute(){
        return self::statusToString($this->id);
    }

}

/**
 * PaymentState_Lang
 *
 * @property integer $payment_state_id
 * @property string $lang_id
 * @property string $name
 * @property string $template
 * @method static \Illuminate\Database\Query\Builder|\PaymentState_Lang wherePaymentStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\PaymentState_Lang whereTemplate($value)
 * @property-read \PaymentState $paymentState 
 */
class PaymentState_Lang extends Eloquent
{


    protected $table = 'payment_states_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function paymentState()
    {
        return $this->belongsTo('PaymentState');
    }


}