<?php


/**
 * AttributeOption_Lang
 *
 * @property integer $attribute_option_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property string $special
 * @property boolean $published
 * @property-read \AttributeOption $attributeOption
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption_Lang whereAttributeOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption_Lang whereSpecial($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeOption_Lang wherePublished($value)
 */
class AttributeOption_Lang extends Model_Lang  {


    protected $table = 'attributes_options_lang';

    
    public function attributeOption()
    {
        return $this->belongsTo('AttributeOption');
    }
    

}