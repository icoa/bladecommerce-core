<?php


/**
 * RmaState
 *
 * @property integer $id
 * @property boolean $send_email
 * @property string $module_name
 * @property string $icon
 * @property string $color
 * @property boolean $unremovable
 * @property boolean $hidden
 * @property boolean $slip
 * @property boolean $pending
 * @property boolean $confirmed
 * @property boolean $refused
 * @property boolean $active
 * @property boolean $deleted
 * @property boolean $position
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereSendEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereModuleName($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereIcon($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereUnremovable($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereHidden($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereSlip($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState wherePending($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereConfirmed($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereRefused($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState whereDeleted($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState wherePosition($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class RmaState extends MultiLangModel {


    const STATUS_WAITING_CONFIRMATION = 1;
    const STATUS_WAITING_PACKAGE = 2;
    const STATUS_RECEIVED_PACKAGE = 3;
    const STATUS_RETURN_REFUSED = 4;
    const STATUS_COMPLETED = 5;

    /* LARAVEL PROPERTIES */
    protected $table = 'rma_states';
    protected $guarded = array();

    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    protected $db_fields = array("send_email", "module_name", "color", "unremovable");
    protected $lang_model = 'RmaState_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;

    function getName( $formatted = false, $icon = 'font' ){
        $record = $this;
        if($record == null){
            return null;
        }
        if($formatted){
            $icon = str_replace('font',$icon,$record->icon);
            $str = "<span class='label' style='background-color: $record->color'><i class='fa $icon'></i> $record->name</span>";
        }else{
            $str = $record->name;
        }
        return $str;
    }

    /**
     * @return array
     */
    static function statusToArray()
    {
        return [
            self::STATUS_WAITING_CONFIRMATION => 'STATUS_WAITING_CONFIRMATION',
            self::STATUS_WAITING_PACKAGE => 'STATUS_WAITING_PACKAGE',
            self::STATUS_RECEIVED_PACKAGE => 'STATUS_RECEIVED_PACKAGE',
            self::STATUS_RETURN_REFUSED => 'STATUS_RETURN_REFUSED',
            self::STATUS_COMPLETED => 'STATUS_COMPLETED',
        ];
    }

    /**
     * @param $status
     * @return string
     */
    static function statusToString($status)
    {
        $array = self::statusToArray();
        return array_key_exists($status, $array) ? $array[$status] : 'STATUS_UNKNOWN';
    }

    /**
     * @param $status
     * @return int
     */
    static function statusToInt($status)
    {
        $array = self::statusToArray();
        foreach ($array as $key => $value) {
            if ($value === $status)
                return $key;
        }
        return 0;
    }

    /**
     * @return string
     */
    function getCodeAttribute()
    {
        return self::statusToString($this->id);
    }

}


/**
 * RmaState_Lang
 *
 * @property integer $rma_state_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\RmaState_Lang whereRmaStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaState_Lang whereName($value)
 * @property-read \RmaState $rmaState
 */
class RmaState_Lang extends Eloquent  {


    protected $table = 'rma_states_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function rmaState()
    {
        return $this->belongsTo('RmaState');
    }


}
