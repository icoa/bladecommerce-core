<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Bluespirit\Negoziando\Fidelity\FidelityPoint;
use Core\Barcode;
use services\Observers\CustomerObserver;
use services\Strap\Traits\TraitStrapsB2BCustomer;
use services\Traits\TraitActive;
use Illuminate\Database\Eloquent\Builder;

/**
 * Customer
 *
 * @property integer $id
 * @property integer $import_id
 * @property integer $shop_group_id
 * @property integer $shop_id
 * @property integer $gender_id
 * @property boolean $people_id
 * @property integer $default_group_id
 * @property integer $alt_group_id
 * @property string $lang_id
 * @property integer $risk_id
 * @property string $company
 * @property string $siret
 * @property string $ape
 * @property string $name
 * @property string $firstname
 * @property string $lastname
 * @property string $email
 * @property string $passwd
 * @property string $passwd_md5
 * @property string $last_passwd_gen
 * @property string $birthday
 * @property boolean $newsletter
 * @property string $ip_registration_newsletter
 * @property string $newsletter_date_add
 * @property boolean $optin
 * @property string $website
 * @property float $outstanding_allow_amount
 * @property boolean $show_public_prices
 * @property integer $max_payment_days
 * @property string $secure_key
 * @property string $note
 * @property boolean $active
 * @property boolean $guest
 * @property boolean $deleted
 * @property string $card_code
 * @property string $customer_code
 * @property boolean $synced
 * @property boolean $marketing
 * @property boolean $profile
 * @property \Carbon\Carbon $created_at
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @property-read mixed $gateway
 * @property-read mixed $day
 * @property-read mixed $month
 * @property-read mixed $year
 * @property-read \Illuminate\Database\Eloquent\Collection|FidelityPoint::class)->orderBy('crea[] $fidelity_points
 * @property-read mixed $card_barcode
 * @property-read mixed $card_html
 * @property-read mixed $account_panel_url
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Customer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereImportId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereShopGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereGenderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer wherePeopleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereDefaultGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereRiskId($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereSiret($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereApe($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer wherePasswd($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer wherePasswdMd5($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereLastPasswdGen($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereBirthday($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereNewsletter($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereIpRegistrationNewsletter($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereNewsletterDateAdd($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereOptin($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereWebsite($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereOutstandingAllowAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereShowPublicPrices($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereMaxPaymentDays($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereSecureKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereNote($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereGuest($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereDeleted($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereCardCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereCustomerCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereSynced($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereMarketing($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereProfile($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Customer whereDeletedBy($value)
 * @method static \Customer withGroup($groups)
 * @method static \Customer active()
 * @property-read mixed $card_barcode_url
 * @property-read mixed $comunicazione
 * @property-read mixed $segmentazione
 * @property-read mixed $uuid
 * @property-read mixed $gender_short
 * @property-read mixed $type
 * @property-read mixed $group_name
 * @property-read mixed $alt_group_name
 * @property-read mixed $shop_name
 * @property-read mixed $shop_code
 */
class Customer extends SingleModel implements PresentableInterface
{
    use TraitActive;
    use TraitStrapsB2BCustomer;

    const TYPE_PERSON = 1;
    const TYPE_COMPANY = 2;

    /* LARAVEL PROPERTIES */

    protected $table = 'customers';
    protected $guarded = array('disable_broadcast');
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $db_fields = array(
        "shop_group_id",
        "shop_id",
        "gender_id",
        "default_group_id",
        "alt_group_id",
        "lang_id",
        "risk_id",
        "company",
        "siret",
        "ape",
        "name",
        "firstname",
        "lastname",
        "email",
        "passwd",
        "last_passwd_gen",
        "birthday",
        "newsletter",
        "ip_registration_newsletter",
        "newsletter_date_add",
        "optin",
        /*"website",
        "outstanding_allow_amount",
        "show_public_prices",
        "max_payment_days",*/
        "secure_key",
        "note",
        "active",
        "guest",
        "card_code",
        "customer_code",
        "marketing",
        "profile",
    );

    public static function boot()
    {
        parent::boot();

        // Setup event bindings...
        self::observe(new CustomerObserver());
    }

    /**
     * @return mixed|string
     */
    function getName()
    {
        $name = ($this->people_id == 1) ? $this->firstname . " " . $this->lastname : $this->company;
        if (trim($name) == '' AND $this->guest == 1) {
            $name = lex('lbl_guest');
        }
        return $name;
    }

    /**
     * @return $this
     */
    function expand()
    {
        if (trim($this->name) == '') {
            $this->name = ($this->people_id == 1) ? $this->firstname . " " . $this->lastname : $this->company;
        }
        return $this;
    }

    function getHeader()
    {
        $name = $this->getName();
        if ($this->gender_id > 0) {
            $gender = Gender::getObj($this->gender_id);
            $name = $gender->formal . " " . $name;
        }
        return $name;
    }

    function full()
    {
        $day = 0;
        $month = 0;
        $year = 0;
        $time = strtotime($this->birthday);
        if ($time > 0) {
            $day = date("d", $time);
            $month = date("m", $time);
            $year = date("Y", $time);
        }
        $this->setAttribute('day', $day);
        $this->setAttribute('month', $month);
        $this->setAttribute('year', $year);

        $address = $this->getAddress();
        if ($address) {
            if ($this->firstname == '') {
                $this->firstname = $address->firstname;
            }
            if ($this->lastname == '') {
                $this->lastname = $address->lastname;
            }
            if ($this->company == '') {
                $this->company = $address->company;
            }
            $this->setAttribute('address_id', $address->id);
            $this->setAttribute('cf', $address->cf);
            $this->setAttribute('vat_number', $address->vat_number);
            $this->setAttribute('address1', $address->address1);
            $this->setAttribute('address2', $address->address2);
            $this->setAttribute('address', $address->address1 . (($address->address2 != '') ? " " . $address->address2 : ""));

            $this->setAttribute('postcode', $address->postcode);
            $this->setAttribute('city', $address->city);
            $this->setAttribute('state_id', $address->state_id);
            $this->setAttribute('stateName', $address->stateName());
            $this->setAttribute('stateCode', $address->stateCode());
            $this->setAttribute('extrainfo', $address->extrainfo);
            $this->setAttribute('phone', $address->phone);
            $this->setAttribute('phone_mobile', $address->phone_mobile);
            $this->setAttribute('fax', $address->fax);
            $this->setAttribute('country_id', $address->country_id);
            $this->setAttribute('countryName', $address->countryName());
            $this->setAttribute('countryCode', $address->countryCode());

            $address_full = $address->fullAddress();
            $address_full = str_replace('<br>', ' -', $address_full);
            $this->setAttribute('address_full', $address_full);

        }
    }

    /**
     * @return Address|null
     */
    function getAddress($billing = 0)
    {
        return \Address::where('customer_id', $this->id)->whereBilling($billing)->first();
    }

    function countAddress($billing = 0)
    {
        return count($this->getAddresses($billing));
    }

    /**
     * @param int $billing
     * @return Collection
     */
    function getAddresses($billing = 0)
    {
        return \Address::where('customer_id', $this->id)->whereBilling($billing)->get();
    }

    /**
     * @return Address|null
     */
    function getShippingAddress()
    {
        return $this->getAddress();
    }

    /**
     * @return Address|null
     */
    function getBillingAddress()
    {
        return $this->getAddress(1);
    }

    /**
     * @param int $status
     * @return Collection|null|static[]
     */
    function getOrders($status = 0)
    {
        $rows = null;
        if ($status > 0) {
            $rows = \Order::forCustomer($this->id)->whereStatus($status)->orderBy('id', 'desc')->get();
        } else {
            $rows = \Order::forCustomer($this->id)->orderBy('id', 'desc')->get();
        }
        return $rows;
    }

    function selectOrders($placeholder = '', $status = 0)
    {
        $data = ['' => $placeholder];
        $orders = $this->getOrders($status);
        foreach ($orders as $order) {
            $data[$order->id] = $order->reference . " - " . \Format::currency($order->total_order_tax_incl, true);
        }
        return $data;
    }

    function selectOrdersByFlag($placeholder = '', $flag = 'active')
    {
        $data = ['' => $placeholder];
        $orders = \Order::forCustomer($this->id)
            ->whereIn('status', function ($query) use ($flag) {
                $query->select('id')->from('order_states')->where('active', 1)->where($flag, 1);
            })
            ->orderBy('id', 'desc')->get();
        foreach ($orders as $order) {
            $data[$order->id] = $order->reference . " - " . \Format::currency($order->total_order_tax_incl, true);
        }
        return $data;
    }


    function selectOrdersForRma($placeholder = '')
    {
        $data = ['' => $placeholder];
        $flag = 'shipped';
        audit_watch();
        $orders = \Order::forCustomer($this->id)
            ->whereIn('status', function ($query) use ($flag) {
                $query->select('id')->from('order_states')->where('active', 1)->where($flag, 1);
            })
            ->orderBy('id', 'desc')->get();

        foreach ($orders as $index => $order) {
            $products_available = $order->getProductsAvailableForRma();
            if (count($products_available) == 0) {
                unset($orders[$index]);
            }
        }
        foreach ($orders as $order) {
            $data[$order->id] = $order->reference . " - " . \Format::currency($order->total_order_tax_incl, true);
        }
        return $data;
    }


    function getMessages()
    {

        $rows = \Message::where('customer_id', $this->id)->orderBy('id', 'desc')->get();
        foreach ($rows as $index => $row) {
            $row->expand();
        }
        return $rows;
    }

    function getThreads($order_id = 0)
    {
        $query = \Message::where('customer_id', $this->id)->where("id", "=", DB::raw("(SELECT MAX(id) from messages M2 WHERE messages.thread_id=M2.thread_id)"));

        if ($order_id > 0) {
            $query->where('order_id', $order_id);
        }

        $rows = $query->groupBy("messages.thread_id")
            ->orderBy('created_at', 'desc')
            ->select('id')->get();

        $data = [];
        foreach ($rows as $row) {
            $data[] = Message::getObj($row->id);
        }
        return $data;
    }


    function getProfiles()
    {
        return \Profile::where('customer_id', $this->id)->get();
    }

    /*function getTotalOrdersCount(){
        return count($this->getOrders());
    }*/

    function getTotalAddressCount()
    {
        return $this->countAddress() + $this->countAddress(1);
    }

    function getTotalProfilesCount()
    {
        return count($this->getProfiles());
    }

    function getTotalOrdersCount($status = 0)
    {
        return count($this->getOrders($status));
    }

    function getTotalOrdersAmount($formatted = false, $status = 5)
    {
        $amount = 0;
        $orders = $this->getOrders($status);
        foreach ($orders as $o) {
            $amount += $o->total_order;
        }
        if ($formatted) {
            $amount = Format::currency($amount, true);
        }
        return $amount;
    }

    function handleNewsletter()
    {
        if ($this->newsletter == 1) {
            \Newsletter::register([
                'email' => $this->email,
                'customer_id' => $this->id,
                'marketing' => $this->marketing,
                'profile' => $this->profile,
                'user_ip' => $this->ip_registration_newsletter,
            ]);
        } else {
            \Newsletter::unregister($this->email);
        }
    }

    function getLastOrder()
    {
        $cache_key = 'customer_last_order';
        if (\Registry::has($cache_key)) {
            return \Registry::get($cache_key);
        }
        $obj = \Order::forCustomer($this->id)->orderBy('id', 'desc')->first();
        \Registry::set($cache_key, $obj);
        return $obj;
    }

    function getRmas()
    {
        $rows = \Rma::where('customer_id', $this->id)->orderBy('id', 'desc')->get();
        foreach ($rows as $index => $row) {
            $rows[$index] = $row->expand();
        }
        return $rows;
    }

    function hasAddress($address_id)
    {
        return \Address::where('customer_id', $this->id)->where('id', $address_id)->count() > 0;
    }

    function getGatewayAttribute()
    {
        $uri = "/rest/data/authenticate/{$this->secure_key}/{$this->shop_id}";
        if ($this->getShippingAddress()) {
            $uri .= "?address=1";
        }
        return $uri;
    }

    function getShop()
    {
        return \MorellatoShop::getObj($this->shop_id);
    }

    function isB2B()
    {
        if (config('negoziando.b2b_enabled') == false)
            return false;

        return $this->default_group_id == \Config::get('negoziando.b2b_group');
    }

    function getDayAttribute()
    {
        if ($this->birthday) {
            $birthday = Carbon::parse($this->birthday);
            return (int)$birthday->day;
        }
        return null;
    }

    function getMonthAttribute()
    {
        if ($this->birthday) {
            $birthday = Carbon::parse($this->birthday);
            return (int)$birthday->month;
        }
        return null;
    }

    function getYearAttribute()
    {
        if ($this->birthday) {
            $birthday = Carbon::parse($this->birthday);
            return (int)$birthday->year;
        }
        return null;
    }

    /**
     * @return bool
     */
    function hasNewsletter()
    {
        return $this->newsletter == 1;
    }

    /**
     * @return array
     */
    static function getDefaultAttributes()
    {
        return [
            'shop_group_id' => 1,
            'shop_id' => 1,
            'default_group_id' => 1,
            'alt_group_id' => null,
            'lang_id' => 'it',
            'risk_id' => 1,
            'company' => '',
            'siret' => null,
            'ape' => null,
            'newsletter' => 1,
            'optin' => 1,
            'marketing' => 0,
            'profile' => 0,
            'people_id' => 1,
            'guest' => 0,
            'deleted' => 0,
            'ip_registration_newsletter' => '127.0.0.1',
            'secure_key' => \Format::secure_key(),
        ];
    }

    /**
     * @return null|\services\Bluespirit\Negoziando\Fidelity\FidelityCustomer
     */
    function getFidelityCustomer()
    {
        $fidelityCustomer = config('negoziando.fidelity_enabled') ? fidelityCustomer(['email' => $this->email, 'card_code' => $this->card_code, 'customer_code' => $this->customer_code]) : null;
        if ($fidelityCustomer) {
            $fidelityCustomer->setCustomer($this);
        }
        return $fidelityCustomer;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function fidelity_points()
    {
        return $this->hasMany(FidelityPoint::class)->orderBy('created_at', 'desc');
    }

    function getCardBarcodeAttribute()
    {
        if (Str::length($this->card_code) <= 10) {
            return null;
        }
        $filename = storage_path("files/barcodes/$this->card_code.png");
        if (File::exists($filename)) {
            $content = File::get($filename);
        } else {
            $barcode = new Barcode([
                'text' => $this->card_code,
                'size' => 60,
            ]);
            $barcode->save($filename);
            $content = File::get($filename);
        }
        $base64 = 'data:image/png;base64,' . base64_encode($content);
        return "<img src='$base64' />";
    }

    function getCardBarcodeUrlAttribute()
    {
        if (Str::length($this->card_code) <= 10) {
            return null;
        }
        $this->getCardBarcodeAttribute();
        $hash = \Crypt::encrypt($this->card_code);
        $path = \Site::rootify('files/barcode/' . $hash);
        return "<img src='$path' />";
    }

    function getCardHtmlAttribute()
    {
        if (Str::length($this->card_code) <= 10) {
            return null;
        }

        return View::make('pdf.customer_card', ['user' => $this]);
    }

    function forceLogin()
    {
        Session::forget('blade_auth');
        CartManager::forgetCheckout();
        CartManager::onCustomerLogin();
        Session::put("blade_auth", $this->id);
    }

    /**
     * @return bool
     */
    function isCompany()
    {
        return $this->people_id == self::TYPE_COMPANY;
    }

    /**
     * @return bool
     */
    function isPerson()
    {
        return $this->people_id == self::TYPE_PERSON;
    }

    /**
     * @param Builder $query
     * @param $groups
     * @return Builder
     */
    function scopeWithGroup(Builder $query, $groups)
    {
        return $this->scopeWithAnyGroup($query, $groups);
    }

    /**
     * @param Builder $query
     * @param $groups
     * @return Builder
     */
    function scopeWithDefaultGroup(Builder $query, $groups)
    {
        return is_array($groups) ? $query->whereIn('default_group_id', $groups) : $query->where('default_group_id', $groups);
    }

    /**
     * @param Builder $query
     * @param $groups
     * @return Builder
     */
    function scopeWithAltGroup(Builder $query, $groups)
    {
        return is_array($groups) ? $query->whereIn('alt_group_id', $groups) : $query->where('alt_group_id', $groups);
    }

    /**
     * @param Builder $query
     * @param $groups
     * @return Builder
     */
    function scopeWithAnyGroup(Builder $query, $groups)
    {
        return is_array($groups) ? $query->where(function($subquery) use($groups){
            $subquery->whereIn('default_group_id', $groups)->orWhereIn('alt_group_id', $groups);
        }) : $query->where(function($subquery) use($groups){
            $subquery->where('default_group_id', $groups)->orWhere('alt_group_id', $groups);
        });
    }

    /**
     * @param $group
     * @return bool
     */
    function hasGroup($group)
    {
        if (is_array($group)) {
            foreach ($group as $group_id) {
                if ((int)$this->default_group_id === (int)$group_id) {
                    return true;
                }
                if ((int)$this->alt_group_id === (int)$group_id) {
                    return true;
                }
            }
            return false;
        }
        return (int)$this->default_group_id === (int)$group || (int)$this->alt_group_id === (int)$group;
    }

    /**
     * @return int
     */
    function getComunicazioneAttribute(){
        return (int)$this->marketing;
    }

    /**
     * @return int
     */
    function getSegmentazioneAttribute(){
        return (int)$this->profile;
    }

    /**
     * @return string
     */
    function getUuidAttribute(){
        return (string)md5(trim($this->email));
    }

    /**
     * @return string|null
     */
    function getAccountPanelUrlAttribute(){
        return \Link::absolute()->shortcut('account');
    }

    /**
     * @return string|null
     */
    function getGenderShortAttribute(){
        switch ($this->gender_id){
            case 0:
                return null;
            case 1:
                return 'M';
            case 2:
                return 'F';
        }
        return null;
    }

    /**
     * @return string|null
     */
    function getTypeAttribute(){
        switch ($this->people_id){
            case 0:
                return null;
            case 1:
                return 'people';
            case 2:
                return 'company';
        }
        return null;
    }

    /**
     * @return string|null
     */
    function getGroupNameAttribute(){
        $group = CustomerGroup::getObj($this->default_group_id);
        return ($group) ? $group->name : null;
    }

    /**
     * @return string|null
     */
    function getAltGroupNameAttribute(){
        if(null === $this->alt_group_id)
            return null;

        $group = CustomerGroup::getObj($this->alt_group_id);
        return ($group) ? $group->name : null;
    }

    /**
     * @return string|null
     */
    function getShopNameAttribute(){
        if($this->shop_id == 1)
            return null;

        $shop = MorellatoShop::getObj($this->shop_id);
        return ($shop) ? $shop->name : null;
    }

    /**
     * @return string|null
     */
    function getShopCodeAttribute(){
        if($this->shop_id == 1)
            return null;

        $shop = MorellatoShop::getObj($this->shop_id);
        return ($shop) ? $shop->cd_neg : null;
    }

    /**
     * @param $group_id
     * @return $this
     */
    public function addAltGroup($group_id){
        if((int)$group_id !== (int)$this->alt_group_id){
            $this->alt_group_id = $group_id;
            $this->save(['timestamps' => false]);
        }
        return $this;
    }

    /**
     * @param
     * @return CustomerGroup|null
     */
    public function getDefaultCustomerGroup(){
        return $this->default_group_id > 0 ? CustomerGroup::getObj($this->default_group_id) : null;
    }

    /**
     * @param
     * @return CustomerGroup|null
     */
    public function getAltCustomerGroup(){
        return $this->alt_group_id > 0 ? CustomerGroup::getObj($this->alt_group_id) : null;
    }

    /**
     * @param
     * @return \Illuminate\Support\Collection|CustomerGroup[]
     */
    public function getCustomerGroups(){
        $items = collect([]);
        if($group = $this->getDefaultCustomerGroup()){
            $items->push($group);
        }
        if($group = $this->getAltCustomerGroup()){
            $items->push($group);
        }
        return $items;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wishlists(){
        return $this->hasMany(Wishlist::class);
    }

    /**
     * @return Wishlist[]
     */
    public function getWishlists(){
        return $this->wishlists()->orderBy('created_at', 'desc')->get();
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new CustomerPresenter($this);
    }


}

class CustomerPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

    public function presentBirthdate()
    {
        return Format::date($this->birthday);
    }

}
