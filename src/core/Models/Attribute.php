<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Attribute
 *
 * @property integer $id
 * @property string $code
 * @property string $alias
 * @property string $ldesc
 * @property string $frontend_input
 * @property boolean $is_boolean
 * @property boolean $is_unique
 * @property boolean $is_required
 * @property string $frontend_class
 * @property string $nav_code
 * @property boolean $is_nav
 * @property boolean $is_searchable
 * @property boolean $is_visible_in_advanced_search
 * @property boolean $is_comparable
 * @property boolean $is_filterable
 * @property boolean $is_filterable_in_search
 * @property boolean $is_used_for_promo_rules
 * @property boolean $is_visible_on_front
 * @property boolean $is_shortcode
 * @property boolean $used_in_product_listing
 * @property boolean $used_for_sort_by
 * @property integer $position
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereAlias($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereFrontendInput($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsBoolean($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsUnique($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereFrontendClass($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereNavCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsNav($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsSearchable($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsVisibleInAdvancedSearch($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsComparable($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsFilterable($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsFilterableInSearch($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsUsedForPromoRules($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsVisibleOnFront($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereIsShortcode($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereUsedInProductListing($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereUsedForSortBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Attribute extends MultiLangModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'attributes';
    protected $guarded = array();
    protected $softDelete = true;
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array(
        "name",
        "slug",
        "default_value_text",
        "default_value_yesno",
        "default_value_date",
        "default_value_textarea",
        "published",
        "txt_prefix",
        "txt_suffix",
        "txt_dropdown",
        "tooltip_label",
        "tooltip_value",
    );
    protected $db_fields = array(
        "code",
        "ldesc",
        "frontend_input",
        "is_unique",
        "is_boolean",
        "is_required",
        "frontend_class",
        "nav_code",
        "is_nav",
        "is_searchable",
        "is_visible_in_advanced_search",
        "is_comparable",
        "is_filterable",
        //"is_filterable_in_search",
        "is_used_for_promo_rules",
        //"is_visible_on_front",
        "is_shortcode",
        //"used_in_product_listing",
        "used_for_sort_by",
        "position",
    );

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "published" => 0
    );
    protected $db_fields_cloning = array(
        "code" => SingleModel::CLONE_UNIQUE_SLUG
    );
    protected $lang_model = 'Attribute_Lang';


    protected $defaults = [
        'frontend_input' => 'select',
        'is_unique' => 1,
        "is_boolean" => 0,
        "is_required" => 0,
        "frontend_class" => '',
        "nav_code" => null,
        "is_nav" => 0,
        "is_searchable" => 1,
        "is_visible_in_advanced_search" => 0,
        "is_comparable" => 0,
        "is_filterable" => 0,
        "is_used_for_promo_rules" => 0,
        "is_shortcode" => 0,
        "used_for_sort_by" => 0,
    ];

    protected function getDefaults()
    {
        $defaults = $this->defaults;
        $defaults['position'] = Attribute::max('position') + 1;
        return $defaults;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new AttributePresenter($this);
    }


    function _after_clone(&$source, &$model)
    {
        $options = AttributeOption::where('attribute_id', $source->id)->get();
        if (count($options) > 0) {
            \Utils::log('Cloning attribute options');
            foreach ($options as $ao) {
                $ao->cloneMe(array('attribute_id' => $model->id));
            }
        }
    }

    function _before_delete()
    {
        $obj = $this;
        if ($this->forceDeleting == false) {
            return;
        }
        \Utils::watch();
        \Utils::log("BEFORE FORCE DELETE");
        $options = AttributeOption::where('attribute_id', $obj->id)->get();
        if (count($options) > 0) {
            \Utils::log('Deleting attribute options');
            foreach ($options as $ao) {
                $ao->delete();
            }
        }
        \DB::table("attributes_sets_content")->where('attribute_id', $obj->id)->delete();
        \DB::table("attributes_sets_content_cache")->where('attribute_id', $obj->id)->delete();
        \DB::table("products_attributes")->where('attribute_id', $obj->id)->delete();
        \DB::table("products_attributes_position")->where('attribute_id', $obj->id)->delete();
        \DB::table("products_combinations_attributes")->where('attribute_id', $obj->id)->delete();
        \Utils::unwatch();
    }

    public function options($lang_id = 'default')
    {
        $lang = \Core::getLang($lang_id);
        $key = "options_attributes_{$this->id}_lang_{$lang}";
        if(Registry::has($key)){
            return Registry::get($key);
        }

        $rows = AttributeOption::leftJoin('attributes_options_lang', 'id', '=', 'attributes_options_lang.attribute_option_id')
            ->where('lang_id', $lang)->where('attribute_id', '=', $this->id)->select(array('id', 'uname', 'uslug', 'name', 'slug', 'is_default', 'attribute_id'))->orderBy('name')->orderBy('uname')->get();

        $many = count($rows);
        for ($i = 0; $i < $many; $i++) {
            $rows[$i]->name = ($rows[$i]->name != '') ? $rows[$i]->name : $rows[$i]->uname;
            $rows[$i]->code = $this->code;
        }

        Registry::set($key, $rows);

        return $rows;
    }


    public static function dataset($lang_id = 'default', $exclude_ids = false, $include_ids = false)
    {
        $lang = \Core::getLang($lang_id);

        $instance = new static;

        $query = $instance::leftJoin('attributes_lang', 'id', '=', 'attributes_lang.attribute_id')
            ->where('lang_id', $lang)->select(array(DB::raw("attributes.*,attributes_lang.*")))->orderBy('name');

        if ($exclude_ids) {
            if (!is_array($exclude_ids)) {
                $exclude_ids = explode(",", $exclude_ids);
            }
            $query->whereNotIn("id", $exclude_ids);
        }

        if ($include_ids) {
            if (!is_array($include_ids)) {
                $include_ids = explode(",", $include_ids);
            }
            $query->whereIn("id", $include_ids);
        }

        return $query->get();
    }


    public function addSimpleOption($option, $code = null, $nav_id = null)
    {
        $attribute_id = $this->id;
        $uname = is_null($code) ? ucfirst(Str::lower($option)) : $code;
        $uslug = Str::slug($uname);
        $data = compact('attribute_id', 'uname', 'uslug', 'nav_id');
        $name = $option;
        $slug = Str::slug($option);

        foreach (Core::getLanguages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => $slug,
                'published' => 1,
            ];
        }

        audit($data, __METHOD__);
        $existing = AttributeOption::where(compact('attribute_id', 'uname'))->first();

        $model = ($existing) ? $existing : new AttributeOption();
        $model->make($data);
        return $model->id;
    }


    /**
     * @param string $name
     * @return string
     */
    public function transformApiLabelAttribute($name){
        $label = preg_replace('/[0-9]+/', '', $name);
        $label = \Str::slug(trim($label), '_');
        return $label;
    }


}

class AttributePresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

    public function presentFullname()
    {
        if ($this->name == '') {
            return $this->uname;
        }
        return $this->name;
    }

}
