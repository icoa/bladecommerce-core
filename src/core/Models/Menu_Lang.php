<?php


/**
 * Menu_Lang
 *
 * @property integer $menu_id
 * @property string $lang_id
 * @property string $name
 * @property string $subtitle
 * @property boolean $published
 * @property string $content_image
 * @property string $content_html
 * @property-read \Menu $menu
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang whereMenuId($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang whereSubtitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang whereContentImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Menu_Lang whereContentHtml($value)
 */
class Menu_Lang extends Eloquent  {


    protected $table = 'menus_lang';
    protected $guarded = array();
    public $timestamps = false;
    
    public function menu()
    {
        return $this->belongsTo('Menu');
    }
    

}