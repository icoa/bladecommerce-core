<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Zone
 *
 * @property integer $id
 * @property string $name
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Zone whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Zone whereDeletedAt($value)
 */
class Zone extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'zones';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    
    public $db_fields = array(
        "name",
        "active",
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new ZonePresenter($this);
    }
    
    static function countriesCheckBoxes($ids){
        \Utils::log($ids,"IDS");
        \Utils::watch();
        $lang_id = \Core::getLang();
        $rows = \Country::with(array('translations' => function($query) use($lang_id) {
                $query->where('lang_id', '=', $lang_id);
            }))->join('countries_lang', 'countries.id', '=', 'countries_lang.country_id')->where('lang_id', '=', $lang_id)->orderBy("name")->get();
            
            $html = '<button type"button" class="btn btn-small mr5" onclick="Zones.selectAll();">Seleziona tutti</button><button type"button" class="btn btn-small" onclick="Zones.deselectAll();">Deseleziona tutti</button><br><br><div class="clearfix"></div>';
        foreach($rows as $row){
            $attr = ["autocomplete" => "off", "class" => "country"];
            $checked = false;
            if(in_array($row->id, $ids)){
                $checked = true;
            }            
            $html .= "<label>" . \Form::checkbox("countries[]",$row->id,$checked,$attr). " {$row->translations[0]->name}</label><br>";
        }    
        \Utils::unwatch();
        return $html;
    }
    
    
    static function countriesList($id = 0){
        $lang_id = \Core::getLang();
        $rows = \Country::with(array('translations' => function($query) use($lang_id) {
                $query->where('lang_id', '=', $lang_id);
            }))->join('countries_lang', 'countries.id', '=', 'countries_lang.country_id')->where('lang_id', '=', $lang_id)->where("zone_id",$id)->orderBy("name")->get();
            
        $options = [];
        foreach($rows as $row){
            //$options[$row->id] = $row->translations[0]->name." ".(($row->active) ? "(Attiva)" : "(Disattiva)") ." ".(($row->eu) ? "(EU)" : "");
            $options[$row->id] = $row->translations[0]->name;
        }   
        return $options;
    }
    
}

class ZonePresenter extends Presenter {
    
    /*function presentCountries(){
        $id = $this->id;
        $data = \DB::table("countries")->where("zone_id",$id)->lists("id");
        return $data;
    }*/
}
