<?php

use Core\Barcode;
use Illuminate\Database\Eloquent\Builder;
use services\Fees\Traits\TraitFeesRma;

/**
 * Rma
 *
 * @property integer $id
 * @property string $secure_key
 * @property integer $shop_id
 * @property string $lang_id
 * @property string $reference
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $option_id
 * @property integer $status
 * @property string $reason
 * @property string $response
 * @property string $rma_date
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Rma whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereSecureKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereReference($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereReason($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereResponse($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereRmaDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Rma whereDeletedBy($value)
 */
class Rma extends SingleModel
{

    use TraitFeesRma;

    /* LARAVEL PROPERTIES */

    protected $table = 'rmas';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $lang = null;
    protected $has_published = false;


    public $db_fields = array(
        'shop_id',
        'customer_id',
        'product_id',
        'lang_id',
        'order_id',
        'status',
        'reason',
        'response',
    );


    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->lang = \Core::getLang();
    }

    /**
     * @return Customer|null
     */
    function getCustomer()
    {
        $customer = \Customer::find($this->customer_id);
        return $customer;
    }

    /**
     * @return Order|null
     */
    function getOrder()
    {
        $order = \Order::find($this->order_id);
        return $order;
    }

    /**
     * @return RmaState|null
     */
    function getStatus()
    {
        $status = \RmaState::getObj($this->status);
        return $status;
    }

    /**
     * @return bool
     */
    function isConfirmed(){
        $status = $this->getStatus();
        return ($status and $status->confirmed == 1);
    }

    /**
     * @return RmaOption|null
     */
    function getOption()
    {
        $option = \RmaOption::getObj($this->option_id);
        return $option;
    }

    function updateStatus($status)
    {
        if ($this->status == $status) {
            return false;
        }
        $this->status = $status;
        $this->save();
        $this->setStatus($status);
        return true;
    }

    /**
     * @return string|null
     */
    function customerName()
    {
        $record = $this->getCustomer();
        return ($record) ? $record->name : null;
    }

    /**
     * @param bool $formatted
     * @return null|string
     */
    function statusName($formatted = false)
    {
        $record = $this->getStatus();
        if ($record == null) {
            return null;
        }
        if ($formatted) {
            $icon = str_replace('font', 'fa', $record->icon);
            $str = "<span class='label' style='background-color: $record->color'><i class='fa $icon'></i> $record->name</span>";
        } else {
            $str = $record->name;
        }
        return $str;
    }

    /**
     * @param $status_id
     * @param string $response
     */
    function setStatus($status_id, $response = '')
    {
        if ($this->status != $status_id) {
            $statusObj = RmaState::getObj($status_id);
            $this->status = $status_id;

            if ($statusObj->slip == 1) {
                try {
                    Rma::createDocument($this->id);
                } catch (Exception $e) {
                    audit_exception($e, __METHOD__);
                }
            }

            if ($statusObj->send_email == 1) {
                try {
                    $email_data = $this->getEmailData();
                    $email = Email::getByCode('RMA_UPDATE_STATUS');
                    $email->setCustomer($this->getCustomer())->send($email_data, $email_data['customer_email']);
                } catch (\Exception $e) {
                    audit_exception($e, __METHOD__);
                }
            }
        }
        $this->response = $response;
        $this->save();
    }

    /**
     * @return null|string
     */
    function getFrontUrl()
    {
        $url = Link::absolute()->shortcut('account');
        $url .= "?page=rmaview&id=$this->id";
        return $url;
    }

    /**
     * @param $data
     */
    static function sendRma($data)
    {

        $reference_number = \Cfg::get('ORDER_RMA_NUMBER') + 1;
        $reference = OrderManager::generateReference($reference_number, 'rma');

        $secure_key = \Core::randomString(8) . 'RRMA';

        $rmaData = [
            'lang_id' => \Core::getLang(),
            'customer_id' => $data['customer_id'],
            'order_id' => $data['order_id'],
            'option_id' => $data['option_id'],
            'reason' => $data['reason'],
            'status' => 1,
            'reference' => $reference,
            'rma_date' => date("Y-m-d"),
            'secure_key' => $secure_key
        ];

        $rma = new Rma();
        $rma->setAttributes($rmaData);
        $rma->save();


        if ($rma and $rma->id > 0) {
            \Cfg::save('ORDER_RMA_NUMBER', $reference_number);
            $ids = $data['rma']['id'];
            $qty = $data['rma']['qty'];
            $detail = $data['rma']['detail'];
            foreach ($ids as $key => $id) {
                $quantity = $qty[$key];
                $order_detail_id = $detail[$key];
                $rmaDetailsData = [
                    'rma_id' => $rma->id,
                    'product_id' => $id,
                    'product_quantity' => $quantity,
                    'order_details_id' => $order_detail_id,
                ];

                DB::table('rma_details')->insert($rmaDetailsData);
            }

            $email_data = $rma->getEmailData();
            $email = Email::getByCode('RMA');
            $email->setCustomer($rma->getCustomer())->send($email_data, $email_data['customer_email']);
        }


    }

    /**
     * @return OrderDetail[]
     */
    function getProducts()
    {
        $rows = DB::table('rma_details')->where('rma_id', $this->id)->get();
        foreach ($rows as $row) {
            $detail = OrderDetail::getObj($row->order_details_id);
            if ($detail) {
                $row->detail = $detail;
            }
            $product = Product::getObj($row->product_id);
            if ($product) {
                $product->setFullData();
                $row->product = $product;
            }
        }
        return $rows;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|RmaDetail[]
     */
    function getRmaDetails(){
        return RmaDetail::with('order_detail')->where('rma_id', $this->id)->get();
    }

    /**
     * @return array
     */
    function getEmailData()
    {
        \Utils::watch();
        $products = $this->getProducts();
        $customer = $this->getCustomer();
        $status = $this->getStatus();
        $option = $this->getOption();
        $order = $this->getOrder();

        $ddt = '';
        $invoice = '';
        if ($order) {
            if ($order->invoice_number > 0) {
                $inv_str = lex('invoice') . " nr. %s - %s";
                $invoice = sprintf($inv_str, $order->getFeeCodeAttribute(), Format::lang_date($order->invoice_date));
            }
            if ($order->delivery_number > 0) {
                $ddt_str = "Documento di trasporto nr. %s del %s";
                $ddt = sprintf($ddt_str, OrderManager::generateReference($order->delivery_number, 'delivery'), Format::lang_date($order->delivery_date));
            }
        }

        $data = [
            'rma_number' => $this->reference,
            'rma_date' => Format::lang_date($this->rma_date),
            'date' => Format::lang_date($this->created_at, 'full'),
            'option_name' => ($option) ? $option->name : '',
            'status_name' => ($status) ? $status->name : '',
            'customer_name' => ($customer) ? $customer->getName() : '',
            'customer_email' => ($customer) ? $customer->email : '',
            'customer_link' => Link::absolute()->shortcut('account'),
            'order_number' => ($order) ? $order->reference : '',
            'reason' => $this->reason,
            'ddt' => $ddt,
            'invoice' => $invoice,
            'details' => View::make('emails.partials.rmatable', ['rows' => $products])->render()
        ];

        return $data;

    }

    /**
     * @return array
     */
    function getDocumentData()
    {
        $shop = \Core::getShopInfo();

        $data = [
            'shop_header' => $shop['name'],
            'shop_address' => $shop['full_address'],
            'shop_phone' => $shop['full_phone'],
            'shop_details' => $shop['details'],
            'shop_email' => "<a href='mailto:{$shop['email']}'>" . $shop['email'] . "</a>",
        ];

        $rma = \Core::getRmaContacts();
        $data += [
            'rma_shop_header' => $rma['name'],
            'rma_shop_address' => $rma['full_address'],
            'rma_shop_phone' => $rma['full_phone'],
            'rma_shop_details' => $rma['details'],
            'rma_shop_email' => "<a href='mailto:{$rma['email']}'>" . $rma['email'] . "</a>",
        ];

        $data['rma'] = $rma;

        $data['logo'] = Site::root() . \Cfg::get('DEFAULT_LOGO');
        $data['url'] = Site::root() . "/media/pdf";
        $data['logo'] = $data['url'] . '/' . 'logo.png';
        $data['site'] = Site::root();
        $data['short_name'] = \Cfg::get('SHORTNAME');

        return $data;
    }

    /**
     * @return mixed
     */
    function getFileData()
    {
        $filename = 'RMA_' . $this->reference;
        $filepath = storage_path("files/rmas/$filename.pdf");
        $hasFile = File::exists($filepath);
        $url = Site::root() . "/files/rma/{$this->id}?secure_key=" . $this->secure_key;

        $data['filename'] = $filename;
        $data['filepath'] = $filepath;
        $data['hasFile'] = $hasFile;
        $data['url'] = $url;
        return $data;
    }

    /**
     * @return array|mixed
     */
    function getSlipData()
    {
        $data = $this->getDocumentData();

        $products = $this->getProducts();
        $customer = $this->getCustomer();
        $status = $this->getStatus();
        $option = $this->getOption();
        $order = $this->getOrder();
        $shipping = $order->getShippingAddress();

        $data += [
            'rma_number' => $this->reference,
            'secure_key' => $this->secure_key,
            'rma_date' => Format::lang_date($this->rma_date),
            'option_name' => ($option) ? $option->name : '',
            'status_name' => ($status) ? $status->name : '',
            'customer_name' => ($customer) ? $customer->getName() : '',
            'customer_email' => ($customer) ? $customer->email : '',
            'order_number' => ($order) ? $order->reference : '',
            'reason' => $this->reason,
            'shipping' => $shipping->toArray(),
            'details' => View::make('emails.partials.rmatable', ['rows' => $products])->render()
        ];

        $data += $this->getFileData();

        $data['title'] = "Reso N° " . $data['rma_number'] . " del " . $data['rma_date'];

        return $data;
    }

    /**
     * @return $this
     */
    public function expand()
    {
        $this->customer = $this->getCustomer();
        $this->date = Format::lang_date($this->rma_date);
        $this->order = null;
        if ($this->order_id > 0) {
            $order = $this->getOrder();
            if ($order) {
                $this->order = $order;
            }
        }
        if ($this->option_id > 0) {
            $option = $this->getOption();
            if ($option) {
                $this->option = $option;
            }
        }
        if ($this->status > 0) {
            $status = $this->getStatus();
            if ($status) {
                $this->statusObj = $status;
                $this->statusName = $this->statusName(true);
            }
        }
        $details = '';
        $products = $this->getProducts();
        foreach ($products as $p) {
            if(isset($p->detail) and $p->detail !== null){
                $details .= "{$p->product_quantity} X {$p->detail->product_name} (Cod. {$p->detail->product_reference})<br>";
            }else{
                $product_obj = \Product::getObj($p->product_id);
                if($product_obj)
                    $details .= "{$p->product_quantity} X {$product_obj->name} (Cod. {$product_obj->sku})<br>";
            }
        }
        $this->details = $details;
        $this->count = count($products);

        $filedata = $this->getFileData();
        $this->hasFile = false;
        if ($this->statusObj) {
            if ($this->statusObj->slip == 1) {
                $this->hasFile = $filedata['hasFile'];
                $this->fileUrl = $filedata['url'];
            }
        }


        return $this;
    }

    /**
     *
     */
    function updateOrderStatus()
    {
        $statusObj = $this->getStatus();
        $config = \Cfg::getGroup('order');
        $order = $this->getOrder();
        $order_state_refunded = $config['RMA_ORDER_STATE_REFUNDED'];
        $order_state_partially_refunded = $config['RMA_ORDER_STATE_PARTIALLY_REFUNDED'];

        if ($statusObj->confirmed == 1) {
            $checksum = 0;
            $rma_products = $this->getProducts();
            $order_products = $order->getProducts();
            foreach ($rma_products as $rp) {
                foreach ($order_products as $op) {
                    if ($rp->product_id == $op->product_id and $rp->product_quantity == $op->product_quantity) {
                        $checksum++;
                    }
                }
            }
            $order_state = $order_state_partially_refunded;
            if ($checksum == count($order_products)) {
                $order_state = $order_state_refunded;
            }

            //if 'fees' or 'sap_fees' are enabled, then is better to force avoid the 'partially' state
            if(feats()->receipts()){
                $order_state = $order_state_refunded;
            }

            audit($order_state, __METHOD__ . '::settedOrderState');
            $order->setStatus($order_state, true);

        }
    }

    /**
     *
     */
    function updateOrderPaymentStatus()
    {
        $statusObj = $this->getStatus();
        $order = $this->getOrder();

        $payment_state_refunded = PaymentState::STATUS_REFUNDED;
        $payment_state_partially_refunded = PaymentState::STATUS_REFUNDED_PARTIALLY;

        if ($statusObj->confirmed == 1) {
            $checksum = 0;
            $rma_products = $this->getProducts();
            $order_products = $order->getProducts();
            foreach ($rma_products as $rp) {
                foreach ($order_products as $op) {
                    if ($rp->product_id == $op->product_id and $rp->product_quantity == $op->product_quantity) {
                        $checksum++;
                    }
                }
            }
            $payment_state = $payment_state_partially_refunded;
            if ($checksum == count($order_products)) {
                $payment_state = $payment_state_refunded;
            }

            //if 'fees' or 'sap_fees' are enabled, then is better to force avoid the 'partially' state
            if(feats()->receipts()){
                $payment_state = $payment_state_refunded;
            }

            //save the RMA id on the order, since an order can have multiple RMAs, and this value will be used in the Fee management
            $order->setParam('latest_rma_id', $this->id);
            $order->save();

            audit($payment_state, __METHOD__ . '::settedOrderPaymentState');
            $order->setPaymentStatus($payment_state, true);

        }
    }

    /**
     * @return Message[]|null
     */
    function getMessages()
    {
        if (isset($this->_messages)) {
            return $this->_messages;
        }
        $rows = Message::where('rma_id', $this->id)->orderBy('created_at')->get();
        $this->_messages = $rows;
        return $rows;
    }

    /**
     * @param $id
     */
    static function createDocument($id)
    {

        $rma = Rma::getObj($id);
        $data = $rma->getSlipData();
        $secure_key = $data['secure_key'];
        //first - create barcode
        $barcode = new Barcode(['text' => $secure_key]);
        $barCodeFile = storage_path("files/barcodes/$secure_key.png");
        if (File::exists($barCodeFile)) {
            File::delete($barCodeFile);
        }
        $barcode->save($barCodeFile);

        $pdf = PDF::loadView('pdf.rma', $data);
        $pdf->setPaper('a4')->setOrientation('landscape');
        if (File::exists($data['filepath'])) {
            File::delete($data['filepath']);
        }
        $output = $pdf->output();
        File::put($data['filepath'], $output);
    }

    /**
     * @return string
     */
    static function getCmsUrl()
    {
        return Link::absolute()->to('page', \Cfg::get('CMS_RMA_PAGE'));
    }

    /**
     * @param Builder $query
     * @param $status
     * @return Builder
     */
    public function scopeWithStatus(Builder $query, $status)
    {
        if ($status instanceof RmaState)
            $status = $status->id;

        if (is_numeric($status))
            return $query->where('status', $status);

        if (is_array($status))
            return $query->whereIn('status', $status);

        return $query;
    }

    /**
     * @param Builder $query
     * @param $option
     * @return Builder
     */
    public function scopeWithRmaOption(Builder $query, $option)
    {
        if ($option instanceof RmaOption)
            $option = $option->id;

        if (is_numeric($option))
            return $query->where('option_id', $option);

        if (is_array($option))
            return $query->whereIn('option_id', $option);

        return $query;
    }

    /**
     * @param Builder $query
     * @param $order
     * @return Builder
     */
    public function scopeWithOrder(Builder $query, $order)
    {
        if ($order instanceof Order)
            $order = $order->id;

        if (is_numeric($order))
            return $query->where('order_id', $order);

        if (is_array($order))
            return $query->whereIn('order_id', $order);

        return $query;
    }

    /**
     * @param Builder $query
     * @param $customer
     * @return Builder
     */
    public function scopeWithCustomer(Builder $query, $customer)
    {
        if ($customer instanceof Customer)
            $customer = $customer->id;

        if (is_numeric($customer))
            return $query->where('customer_id', $customer);

        if (is_array($customer))
            return $query->whereIn('customer_id', $customer);

        return $query;
    }

}