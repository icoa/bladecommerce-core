<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Traits\HasParam;
use services\Traits\TraitCartOrderAttributes;
use services\Traits\TraitOrderAvailabilityManager;
use services\Traits\TraitOrderCartRelationManager;

/**
 * Cart
 *
 * @property integer $id
 * @property integer $shop_group_id
 * @property integer $shop_id
 * @property integer $carrier_id
 * @property integer $payment_id
 * @property integer $cart_rule_id
 * @property string $coupon_code
 * @property string $delivery_option
 * @property integer $campaign_id
 * @property string $lang_id
 * @property integer $shipping_address_id
 * @property integer $billing_address_id
 * @property integer $currency_id
 * @property integer $customer_id
 * @property integer $guest_id
 * @property string $secure_key
 * @property boolean $recyclable
 * @property boolean $gift
 * @property string $gift_message
 * @property string $notes
 * @property boolean $allow_seperated_package
 * @property string $origin
 * @property string $ip
 * @property string $availability_mode
 * @property string $availability_shop_id
 * @property integer $delivery_store_id
 * @property string $receipt
 * @property boolean $invoice_required
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Cart whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereShopGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCarrierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart wherePaymentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCartRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCouponCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereDeliveryOption($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCampaignId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereShippingAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereBillingAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereGuestId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereSecureKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereRecyclable($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereGift($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereGiftMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereAllowSeperatedPackage($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereOrigin($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereIp($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereAvailabilityMode($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereAvailabilityShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereDeliveryStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereReceipt($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereInvoiceRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Cart whereDeletedAt($value)
 * @property-read Customer $customer
 * @property-read Order $order
 * @property-read Carrier $carrier
 * @property-read Payment $payment
 * @property-read Campaign $campaign
 * @property-read CartRule $cart_rule
 * @property-read \Illuminate\Database\Eloquent\Collection|CartProduct[] $cart_products
 * @property-read Address $shipping_address
 * @property-read Address $billing_address
 * @property-read mixed $recovery_url
 * @method static \Cart withAvailabilityModes($modes)
 * @method static \Cart withoutAvailabilityModes($modes)
 * @method static \Cart available()
 * @method static \Cart withBasicAvailability()
 * @method static \Cart forCustomer($customer_id = null)
 * @method static \Cart withPayment($payment)
 * @method static \Cart withoutPayment($payment)
 * @method static \Cart withCarrier($carrier)
 * @method static \Cart withoutCarrier($carrier)
 * @method static \Cart withCustomer($customer)
 * @method static \Cart withoutCustomer($customer)
 * @method static \Cart withDeliveryStore($store = null)
 * @method static \Cart withAvailabilityStore($store = null)
 */
class Cart extends SingleModel implements PresentableInterface
{

    use HasParam;
    use TraitOrderCartRelationManager, TraitCartOrderAttributes, TraitOrderAvailabilityManager;

    const DISCOUNT_TARGET_LIST_PRICE = 'list';
    const DISCOUNT_TARGET_FINAL_PRICE = 'final';
    const DISCOUNT_TARGET_ORDER = 'order';

    /* LARAVEL PROPERTIES */

    protected $table = 'cart';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $blameable = [];
    protected $isBlameable = false;
    public $db_fields = array(
        "shop_group_id",
        "shop_id",
        "carrier_id",
        "delivery_option",
        "lang_id",
        "address_delivery_id",
        "address_invoice_id",
        "currency_id",
        "customer_id",
        "guest_id",
        "secure_key",
        "recyclable",
        "cart_rule_id",
        "gift",
        "gift_message",
        "allow_seperated_package",
    );

    private $products;

    /**
     * @return BelongsTo
     */
    function customer()
    {
        return $this->belongsTo(Customer::class);
    }

    /**
     * @return HasOne
     */
    function order()
    {
        return $this->hasOne(Order::class);
    }

    /**
     * @return BelongsTo
     */
    function carrier()
    {
        return $this->belongsTo(Carrier::class);
    }

    /**
     * @return BelongsTo
     */
    function payment()
    {
        return $this->belongsTo(Payment::class);
    }

    /**
     * @return BelongsTo
     */
    function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * @return BelongsTo
     */
    function cart_rule()
    {
        return $this->belongsTo(CartRule::class);
    }

    /**
     * @return HasMany
     */
    function cart_products()
    {
        return $this->hasMany(CartProduct::class);
    }

    /**
     * @return BelongsTo
     */
    function shipping_address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * @return BelongsTo
     */
    function billing_address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * @return Customer|Model|null
     */
    function getCustomer()
    {
        return $this->loadMissing(Customer::class);
    }

    /**
     * @return Order|Model|null
     */
    function getOrder()
    {
        return $this->loadMissing(Order::class);
    }

    /**
     * @return Carrier|Model|null
     */
    function getCarrier()
    {
        return $this->loadMissing(Carrier::class);
    }

    /**
     * @return Payment|Model|null
     */
    function getPayment()
    {
        return $this->loadMissing(Payment::class);
    }

    /**
     * @return Campaign|Model|null
     */
    function getCampaign()
    {
        return $this->loadMissing(Campaign::class);
    }

    /**
     * @return CartRule|Model|null
     */
    function getPriceRule()
    {
        return $this->loadMissing(CartRule::class);
    }

    /**
     * @return CartRule|Model|null
     */
    function getCartRule()
    {
        return $this->loadMissing(CartRule::class);
    }

    /**
     * @return CartProduct[]|\Illuminate\Database\Eloquent\Collection
     */
    function getProducts()
    {
        if (is_array($this->products) and count($this->products) > 0) return $this->products;
        $rows = CartProduct::where('cart_id', $this->id)->get();
        foreach ($rows as $row) {
            $product = \Product::getPublicObj($row->product_id);
            if ($product) {
                $product->setFullData();
                $row->product = $product;
                $row->price = ((float)$row->price_tax_incl > 0) ? $row->price_tax_incl : $product->price;
            }
        }
        $this->products = $rows;
        return $rows;
    }

    /**
     * @return float|int
     */
    function getTotal()
    {
        $total = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            $total += (float)$product->price;
        }
        return $total;
    }

    /**
     * @return int
     */
    function getCount()
    {
        return count($this->getProducts());
    }

    /**
     * @param $id
     * @return Cart
     */
    static function getObj($id)
    {

        $obj = parent::getObj($id);
        if ($obj) {
            if ($obj->guest_id > 0) {
                $obj->customer_id = $obj->guest_id;
            }
        }
        return $obj;
    }

    /**
     * @return $this|SingleModel
     */
    function expand()
    {
        $this->order_id = \Order::where('cart_id', $this->id)->pluck('id');
        $this->payment = $this->getPayment();
        $this->carrier = $this->getCarrier();
        $this->campaign = $this->getCampaign();
        $this->count = $this->getCount();
        $this->total = $this->getTotal();
        $this->order = $this->getOrder();
        $this->customer = $this->getCustomer();
        $this->rule = $this->getPriceRule();
        return $this;
    }

    /**
     * @param $type
     * @return array
     */
    function getUserDiscountsByType($type)
    {
        $user_discounts = $this->getUserDiscounts();
        $rows = [];
        if (!empty($user_discounts)) {
            foreach ($user_discounts as $key => $user_discount) {
                if ($user_discount->type == $type) {
                    $rows[] = $user_discount;
                }
            }
        }
        return $rows;
    }

    /**
     * @return array
     */
    function getUserDiscounts()
    {
        $params = $this->getAttribute('params');
        return isset($params['user_discounts']) ? $params['user_discounts'] : [];
    }

    /**
     * @param null $type
     * @return bool
     */
    function hasUserDiscounts($type = null)
    {
        return ($type) ? !empty($this->getUserDiscountsByType($type)) : !empty($this->getUserDiscounts());
    }

    /**
     * @return int
     */
    function getPoints()
    {
        $rows = $this->getUserDiscountsByType('fidelity');
        if (!empty($rows))
            return $rows[0]->value;

        return 0;
    }

    /**
     * @return int
     */
    function getPointsAmount()
    {
        $rows = $this->getUserDiscountsByType('fidelity');
        if (!empty($rows))
            return $rows[0]->amount;

        return 0;
    }

    /**
     * @return int
     */
    function getTotalUserDiscounts()
    {
        $userTotalDiscounts = 0;
        $user_discounts = $this->getUserDiscounts();
        if (!empty($user_discounts)) {
            foreach ($user_discounts as $user_discount) {
                $userTotalDiscounts += $user_discount->amount;
            }
        }

        return $userTotalDiscounts;
    }

    /**
     * @return array
     */
    function getVouchers()
    {
        return $this->getUserDiscountsByType('voucher');
    }

    /**
     * @return Address|null
     */
    function getShippingAddress()
    {
        $obj = $this->shipping_address_id > 0 ? \Address::getObj($this->shipping_address_id, 'default', true) : null;
        return $obj;
    }

    /**
     * @return Address|null
     */
    function getBillingAddress()
    {
        $obj = $this->billing_address_id > 0 ? \Address::getObj($this->billing_address_id, 'default', true) : null;
        return $obj;
    }

    /**
     * @param bool $relation_loaded
     * @return bool
     */
    function isAbandoned($relation_loaded = false)
    {
        /**
         * a cart is abandoned where:
         * - it has not an order;
         * - the order exists, but the payment status is among [STATUS_USER_CANCELED,STATUS_ERROR,STATUS_EXPIRED]
         */

        //via relation
        if ($relation_loaded) {
            /** @var Order $order */
            $order = $this->order;
            if (!isset($this->order))
                return true;

        } else {
            $order = $this->getOrder();

            if ($order === null)
                return true;
        }

        if ($order and $order->hasCurrentPaymentStatus([PaymentState::STATUS_USER_CANCELED, PaymentState::STATUS_ERROR, PaymentState::STATUS_EXPIRED]))
            return true;

        return false;
    }

    /**
     * @return string
     */
    function getRecoveryUrlAttribute()
    {
        $url = \Link::absolute()->shortcut('cart') . "?cart={$this->id}&secure_key={$this->secure_key}&currency_id=$this->currency_id&lang_id=$this->lang_id";
        return $url;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new CartPresenter($this);
    }

}

class CartPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}
