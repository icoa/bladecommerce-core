<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Banner
 *
 * @property integer $id
 * @property string $row_index
 * @property string $content
 * @property string $params
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Banner whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereRowIndex($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner whereUpdatedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Banner extends MultiLangModel implements PresentableInterface
{

    /* LARAVEL PROPERTIES */
    protected $table = 'banners';
    protected $guarded = array();

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("image", "published", "input", "input_alt", "input_textarea", "input_textarea_alt", "input_text", "input_text_alt", "input_int", "input_int_alt");
    protected $db_fields = array("row_index", "params", "content");
    protected $lang_model = 'Banner_Lang';
    protected $lang_fields_cloning = array(
        "published" => 0
    );

    static function getRowIndex()
    {
        return \Utils::randomString();
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new BannerPresenter($this);
    }

}

class BannerPresenter extends Presenter
{


}


/**
 * Banner_Lang
 *
 * @property integer $banner_id
 * @property string $lang_id
 * @property boolean $published
 * @property string $image
 * @property string $input
 * @property string $input_alt
 * @property string $input_textarea
 * @property string $input_textarea_alt
 * @property string $input_text
 * @property string $input_text_alt
 * @property integer $input_int
 * @property integer $input_int_alt
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereBannerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInput($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputTextarea($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputTextareaAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputText($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputTextAlt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputInt($value)
 * @method static \Illuminate\Database\Query\Builder|\Banner_Lang whereInputIntAlt($value)
 * @property-read \Banner $banner
 */
class Banner_Lang extends Eloquent
{


    protected $table = 'banners_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function banner()
    {
        return $this->belongsTo('Banner');
    }


}