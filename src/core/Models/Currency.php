<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Currency
 *
 * @property integer $id
 * @property string $name
 * @property string $iso_code
 * @property string $iso_code_num
 * @property string $sign
 * @property boolean $blank
 * @property boolean $format
 * @property boolean $decimals
 * @property string $dec_point
 * @property string $thousan_sep
 * @property float $conversion_rate
 * @property boolean $deleted
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Currency whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereIsoCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereIsoCodeNum($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereSign($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereBlank($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereFormat($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereDecimals($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereDecPoint($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereThousanSep($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereConversionRate($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereDeleted($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Currency whereDeletedAt($value)
 */
class Currency extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'currencies';    
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $has_published = false;

    
    
    public $db_fields = array(        
        "name",
        "iso_code",
        "iso_code_num",
        "sign",
        "blank",
        "format",
        "decimals",
        "dec_point",
        "thousan_sep",
        "conversion_rate",
        "active",
    );

    protected function initCache(){
        $instance = new static;
        $rows = $instance::get();

        foreach($rows as $row){
            $row->uncache();
        }

        \Cache::forget("echo_currencies");
        \Cache::forget("default_country");
        $languages = \Core::getLanguages();
        foreach($languages as $lang){
            \Cache::forget("currency_".$lang);
        }
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CurrencyPresenter($this);
    }
    
}

class CurrencyPresenter extends Presenter {
    

}
