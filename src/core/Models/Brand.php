<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Brand
 *
 * @property integer $id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property boolean $is_visible
 * @property integer $page_id
 * @property string|null $page_url
 * @property string|null $image_default_url
 * @property string|null $image_thumb_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Brand whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand whereIsVisible($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand wherePageId($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read mixed $image_webp_default_url 
 * @property-read mixed $image_webp_thumb_url 
 */
class Brand extends MultiLangModel implements PresentableInterface
{

    /* LARAVEL PROPERTIES */
    protected $table = 'brands';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "ldesc", "image_default", "image_thumb", "image_zoom", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical", "head", "footer");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("position", "robots", "ogp", "ogp_type", "ogp_image", "public_access", "page_id");
    protected $lang_model = 'Brand_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    protected $defaults = [
        'robots' => 'global',
        'ogp' => '0',
        'ogp_type' => 'product',
        'public_access' => 'P',
    ];

    protected function getDefaults()
    {
        $defaults = $this->defaults;
        $defaults['position'] = Brand::max('position') + 1;
        return $defaults;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new BrandPresenter($this);
    }


    function setCategories()
    {
        $query = "select distinct(category_id) from categories_products where product_id in (select id from products where brand_id = ?)";
        $rows = DB::select($query, [$this->id]);
        $data = [];
        foreach ($rows as $row) {
            $data[] = $row->category_id;
        }
        $this->categories = $data;
    }

    function getCategories()
    {
        return isset($this->categories) ? $this->categories : [];
    }

    function getCollections($debug = false)
    {
        $lang = \Core::getLang();
        $builder = \Collection::rows($lang)->where('brand_id', $this->id)->where('published', 1)->orderBy('name');
        if ($debug) {
            $builder->take(5);
        }
        return $builder->get();
    }

    /**
     * @return string|null
     */
    function getImageDefaultUrlAttribute()
    {
        if (!isset($this->image_default)) {
            $obj = self::getObj($this->id);
            $this->image_default = $obj->image_default;
        }
        return (is_null($this->image_default) or $this->image_default == '') ? null : \Site::img($this->image_default, true);
    }

    /**
     * @return string|null
     */
    function getImageThumbUrlAttribute()
    {
        if (!isset($this->image_thumb)) {
            $obj = self::getObj($this->id);
            $this->image_thumb = $obj->image_thumb;
        }
        return (is_null($this->image_thumb) or $this->image_thumb == '') ? null : \Site::img($this->image_thumb, true);
    }

    /**
     * @return string|null
     */
    function getImageWebpDefaultUrlAttribute()
    {
        $url = $this->getImageDefaultUrlAttribute();
        if (null === $url) {
            return null;
        }
        return $url . '.webp';
    }

    /**
     * @return string|null
     */
    function getImageWebpThumbUrlAttribute()
    {
        $url = $this->getImageThumbUrlAttribute();
        if (null === $url) {
            return null;
        }
        return $url . '.webp';
    }

    /**
     * @return string|null
     */
    function getPageUrlAttribute()
    {
        return $this->page_id > 0 ? \Link::absolute()->to('page', $this->page_id) : null;
    }

    /**
     * @param string $lang_id
     * @return string|null
     */
    function getPageUrl($lang_id = 'default')
    {
        return $this->page_id > 0 ? \Link::absolute()->to('page', $this->page_id, $lang_id) : null;
    }

}

class BrandPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}


/**
 * Brand_Lang
 *
 * @property integer $brand_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @property string $canonical
 * @property string $metafacebook
 * @property string $metatwitter
 * @property string $metagoogle
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereImageZoom($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Brand_Lang whereFooter($value)
 * @property-read \Brand $brand
 */
class Brand_Lang extends Model_Lang
{


    protected $table = 'brands_lang';


    public function brand()
    {
        return $this->belongsTo('Brand');
    }


}