<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * SeoText
 *
 * @property integer $id
 * @property integer $category_id
 * @property integer $brand_id
 * @property integer $collection_id
 * @property integer $trend_id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereCollectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereTrendId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class SeoText extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'seo_texts';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("content","published", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical","image_default","image_big","image_mid", "head", "footer");
    protected $db_fields = array("robots", "ogp", "ogp_type", "ogp_image", "category_id", "brand_id", "collection_id", "trend_id");
    protected $lang_model = 'SeoText_Lang';

    protected $lang_fields_cloning = array(
        "content" => SingleModel::CLONE_UNIQUE_TEXT,        
        "published" => 0
    );


    function _after_clone(&$source, &$model) {
        $source_id = $source->id;
        $target_id = $model->id;

        \Utils::watch();

        /* CLONING CATEGORIES */
        $rows = DB::table('seo_texts_rules')->where("seo_text_id", $source_id)->get();

        foreach ($rows as $row) {
            DB::table('seo_texts_rules')->insert([
                'seo_text_id' => $target_id,
                'target_type' => $row->target_type,
                'target_mode' => $row->target_mode,
                'target_id' => $row->target_id,
                'position' => $row->position,
            ]);
        }
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new SeoTextPresenter($this);
    }


    /*function setCategoriesReverse($category_id,&$data){
        \Utils::log($category_id,__METHOD__);
        if($category_id > 0){
            $obj = \Category::getObj($category_id);
            if($obj){
                $data[] = $obj->id;
                if($obj->parent_id > 0)
                    $this->setCategoriesReverse($obj->parent_id,$data);
            }
        }
    }*/


    function expandIf(){
        if(isset($this->expanded) AND $this->expanded == true)return $this;
        \Utils::log($this->toArray(),__METHOD__);
        $category_ids = [];
        $supplier_ids = [];
        $attribute_option_ids = [];
        $attribute_ids = [];
        $attributes_schema = [];
        $this->main_category_id = $this->category_id;
        if($this->category_id > 0){
            \Helper::setCategoriesReverse($this->category_id,$category_ids);
        }
        $options = DB::table('seo_texts_rules')->where('seo_text_id',$this->id)->where('target_type','ATT')->where('target_mode','+')->lists('target_id');
        if(count($options)){
            $ao_list = AttributeOption::whereIn('attributes_options.id',$options)
                ->leftJoin('attributes','attributes_options.attribute_id','=','attributes.id')
                ->select(['attributes_options.id','attributes_options.attribute_id','attributes.code'])
                ->get();
            foreach($ao_list as $ao){
                $attribute_option_ids[] = $ao->id;
                $attribute_ids[] = $ao->attribute_id;
                $attributes_schema[$ao->code] = $ao->id;
            }
        }
        $this->category_ids = $category_ids;
        $this->supplier_ids = $supplier_ids;
        $this->attribute_option_ids = $attribute_option_ids;
        $this->attribute_ids = $attribute_ids;
        $this->attributes_schema = $attributes_schema;
        $this->expanded = true;
    }

}

class SeoTextPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}



/**
 * SeoText_Lang
 *
 * @property integer $seo_text_id
 * @property string $lang_id
 * @property string $canonical
 * @property boolean $published
 * @property string $content
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $metafacebook
 * @property string $metagoogle
 * @property string $metatwitter
 * @property string $image_default
 * @property string $image_big
 * @property string $image_mid
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereSeoTextId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereImageBig($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereImageMid($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\SeoText_Lang whereFooter($value)
 * @property-read \SeoText $seotext
 */
class SeoText_Lang extends Eloquent  {


    protected $table = 'seo_texts_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function seotext()
    {
        return $this->belongsTo('SeoText');
    }


}