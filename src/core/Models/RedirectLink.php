<?php


/**
 * RedirectLink
 *
 * @property integer $id
 * @property string $source
 * @property string $target
 * @property boolean $isRegExp
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereIsRegExp($value)
 */
class RedirectLink extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'redirect_links';
    protected $guarded = array();
    protected $isBlameable = FALSE;
    public $timestamps = FALSE;
    public $softDelete = false;
    public $db_fields = array(
        "source",
        "target",
        "isRegExp"
    );

    const CACHE_KEY = 'redirect_301';

    protected static $enabled = false;


    static function rebuildSchema(){
        if(false === self::$enabled)
            return;
        $key = \Utils::redisPrefix(self::CACHE_KEY);
        Redis::del($key);
        $rows = RedirectLink::where('isRegExp',0)->get();
        foreach($rows as $row){
            self::addEntry($row->source,$row->target);
        }
    }

    static function getEntry($source){
        if(false === self::$enabled)
            return null;
        $key = \Utils::redisPrefix(self::CACHE_KEY);
        $md5 = md5(trim($source));
        $target = Redis::hget($key,$md5);
        if ($target AND $target != ''){
            return $target;
        }
        $obj = RedirectLink::where('isRegExp',0)->where('source')->first();
        if($obj){
            self::addEntry($obj->source, $obj->target);
            return $obj->target;
        }
        return null;
    }


    static function addEntry($source,$target){
        if(false === self::$enabled)
            return;
        $key = \Utils::redisPrefix(self::CACHE_KEY);

        $md5 = md5(trim($source));
        \Utils::log(compact("source","target","md5"),__METHOD__);
        \Redis::hset($key,$md5,$target);
    }

    static function removeEntry($source){
        $key = \Utils::redisPrefix(self::CACHE_KEY);
        $source = md5(trim($source));
        \Redis::hdel($key,$source);
    }

    function _before_delete()
    {
        RedirectLink::removeEntry($this->source);
    }


}
