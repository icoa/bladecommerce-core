<?php


/**
 * RmaOption
 *
 * @property integer $id
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $updated_by
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\RmaOption whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption whereUpdatedBy($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class RmaOption extends MultiLangModel {

    const BY_CREDIT = 1;
    const BY_SUBSTITUTION = 2;

    /* LARAVEL PROPERTIES */
    protected $table = 'rma_options';
    protected $guarded = array();

    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    protected $db_fields = array("active");
    protected $lang_model = 'RmaOption_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;


    static function selectHtml(){
        $rows = RmaOption::rows(\Core::getLang())->where('active',1)->get();
        $data = [];
        foreach($rows as $row){
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    /**
     * @return array
     */
    static function statusToArray()
    {
        return [
            self::BY_CREDIT => 'BY_CREDIT',
            self::BY_SUBSTITUTION => 'BY_SUBSTITUTION',
        ];
    }

    /**
     * @param $status
     * @return string
     */
    static function statusToString($status)
    {
        $array = self::statusToArray();
        return array_key_exists($status, $array) ? $array[$status] : 'BY_UNKNOWN';
    }

    /**
     * @param $status
     * @return int
     */
    static function statusToInt($status)
    {
        $array = self::statusToArray();
        foreach ($array as $key => $value) {
            if ($value === $status)
                return $key;
        }
        return 0;
    }

    /**
     * @return string
     */
    function getCodeAttribute()
    {
        return self::statusToString($this->id);
    }


}


/**
 * RmaOption_Lang
 *
 * @property integer $rma_option_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\RmaOption_Lang whereRmaOptionId($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\RmaOption_Lang whereName($value)
 * @property-read \RmaOption $rmaOption
 */
class RmaOption_Lang extends Eloquent  {


    protected $table = 'rma_options_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function rmaOption()
    {
        return $this->belongsTo('RmaOption');
    }


}
