<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Page
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $position
 * @property string $layout
 * @property string $starting_at
 * @property string $finishing_at
 * @property boolean $indexable
 * @property boolean $feedable
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $link
 * @property-read mixed $permalink
 * @property-read mixed $excerpt
 * @property-read mixed $cover
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereSectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Page wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Page wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereLayout($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereStartingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereFinishingAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereIndexable($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereFeedable($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Page whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class Page extends MultiLangModel implements PresentableInterface
{

    /* LARAVEL PROPERTIES */
    protected $table = 'pages';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "sdesc", "ldesc", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical", "head", "footer", "image_default", "image_thumb", "image_zoom", "ldesc_mobile");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("robots", "ogp", "ogp_type", "ogp_image", "public_access", "starting_at", "finishing_at", "section_id", "position", "layout", "indexable");
    protected $lang_model = 'Page_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    static function slug($id, $lang_id)
    {
        $instance = new static;
        $obj = $instance::getPublicObj($id, $lang_id);
        $slug = null;
        if ($obj) {
            $slug = $obj->slug;
            if ($obj->section_id > 0) {
                $section_slug = \Section::getSlug($obj->section_id, $lang_id);
                $slug = $section_slug . "/" . $obj->slug;
            }
        }
        return $slug;
    }

    public static function getSlug($id, $lang_id)
    {
        return self::slug($id, $lang_id);
    }

    public function expand()
    {
        $slug = $this->slug;
        $lang_id = \Core::getLang();
        if ($this->section_id > 0) {
            $section_slug = \Section::getSlug($this->section_id, $lang_id);
            $slug = $section_slug . "/" . $this->slug;
        }
        $this->link = Site::root() . "/" . $slug . ".html";

        return $this;
    }


    public static function position($parent_id = false)
    {
        $instance = new static;
        $query = $instance->where('id', '>', 0);
        if ($parent_id != false) {
            $query->where('section_id', $parent_id);
        }
        $position = $query->max('position');
        return $position + 1;
    }

    function getSection()
    {
        if ($this->section_id > 0) {
            return \Section::getPublicObj($this->section_id);
        }
        return null;
    }


    function getLinkAttribute()
    {
        return \Link::to('page', $this->id);
    }


    function getPermalinkAttribute()
    {
        return \Link::to('page', $this->id);
    }


    function getExcerptAttribute()
    {
        return strlen(trim($this->sdesc)) > 0 ? trim(strip_tags($this->sdesc)) : trim(strip_tags($this->ldesc));
    }

    function getCoverAttribute(){
        if(trim($this->image_thumb) != ''){
            return $this->image_thumb;
        }
        return $this->image_default;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new PagePresenter($this);
    }

}

class PagePresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

    public function presentStarting()
    {
        return Format::datetime($this->starting_at);
    }

    public function presentFinishing()
    {
        return Format::datetime($this->finishing_at);
    }

}


/**
 * Page_Lang
 *
 * @property integer $page_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $ldesc_mobile
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $canonical
 * @property string $h1
 * @property string $metafacebook
 * @property string $metagoogle
 * @property string $metatwitter
 * @property string $head
 * @property string $footer
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang wherePageId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereLdescMobile($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereFooter($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Page_Lang whereImageZoom($value)
 * @property-read \Page $page
 */
class Page_Lang extends Eloquent
{


    protected $table = 'pages_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function page()
    {
        return $this->belongsTo('Page');
    }


}