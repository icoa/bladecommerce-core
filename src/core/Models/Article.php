<?php namespace App\Models;
 
/**
 * App\Models\Article
 *
 * @property-read \User $author
 */
class Article extends \Eloquent {
 
    protected $table = 'articles';
 
    public function author()
    {
        return $this->belongsTo('User');
    }
 
}