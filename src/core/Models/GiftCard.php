<?php

use Carbon\Carbon;

/**
 * GiftCard
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $customer_name
 * @property integer $order_id
 * @property integer $order_detail_id
 * @property integer $product_id
 * @property float $amount
 * @property float $original_amount
 * @property string $expires_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $expires
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereCustomerName($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereOrderDetailId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereOriginalAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereExpiresAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCard whereDeletedAt($value)
 * @property-read mixed $html_status 
 * @property-read mixed $switch_status 
 */
class GiftCard  extends SingleModel
{
    protected $table = 'gift_cards';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    
    public $db_fields = array(
        "code",
        "name",
        "customer_name",
        "order_id",
        "order_detail_id",
        "product_id",
        "amount",
        "original_amount",
        "expires_at",
    );

    public function getExpiresAttribute(){
        return Carbon::parse($this->expires_at)->format('d/m/Y');
    }

    public function getHtmlStatusAttribute(){
        $expires = Carbon::parse($this->expires_at);
        $now = Carbon::now();
        if($now->gte($expires)){
            return "<span class='label label-inverse'>SCADUTA</span>";
        }
        if(null !== $this->deleted_at)
            return "<span class='label label-important'>DISABILITATA</span>";

        return "<span class='label label-success'>ABILITATA</span>";
    }

    public function getSwitchStatusAttribute(){
        $expires = Carbon::parse($this->expires_at);
        $now = Carbon::now();
        if($now->gte($expires)){
            return null;
        }

        if(null !== $this->deleted_at){
            $flag_label = 'Abilita';
            $flag_icon = 'icon-ok';
            $flag_status = 1;
        }else{
            $flag_label = 'Disabilita';
            $flag_icon = 'icon-off';
            $flag_status = 0;
        }

        $status = \URL::action('GiftCardController@postFlag', array($this->id, $flag_status));

        return "<ul class='table-controls' style='display: table; float:right;'><li><a href=\"javascript:;\" onclick=\"EchoTable.confirmAction('$status');\" class=\"btn\" title=\"$flag_label\"><i class=\"$flag_icon\"></i></a></li></ul>";
    }
}