<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Traits\HasParam;
use services\Traits\TraitProductRow;

/**
 * CartProduct
 *
 * @property integer $id
 * @property integer $cart_id
 * @property integer $product_id
 * @property integer $address_delivery_id
 * @property integer $shop_id
 * @property integer $product_combination_id
 * @property integer $affiliate_id
 * @property string $combinations
 * @property float $weight
 * @property integer $quantity
 * @property boolean $special
 * @property boolean $currency_id
 * @property float $product_price
 * @property float $price
 * @property boolean $reduction
 * @property float $price_tax_incl
 * @property float $price_tax_excl
 * @property float $reduction_percent
 * @property float $reduction_amount
 * @property float $reduction_amount_tax_excl
 * @property integer $cart_rule_id
 * @property string $availability_mode
 * @property string $availability_shop_id
 * @property string $availability_solvable_shops
 * @property string $warehouse
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $label
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereCartId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereAddressDeliveryId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereProductCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereCombinations($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereSpecial($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereProductPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereReduction($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct wherePriceTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct wherePriceTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereReductionPercent($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereReductionAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereReductionAmountTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereCartRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereAvailabilityMode($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereAvailabilityShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereAvailabilitySolvableShops($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereWarehouse($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\CartProduct whereDeletedAt($value)
 */
class CartProduct extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    use HasParam;
    use TraitProductRow;

    protected $table = 'cart_product';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $blameable = [];
    protected $isBlameable = false;
    public $db_fields = array(
        "cart_id",
        "product_id",
        "address_delivery_id",
        "shop_id",
        "product_attribute_id",
        "quantity",
        "price",
        "special",
        "reduction",
        "weight",
        "combinations",
        "params",
    );


    function getCombinationsAttribute($value){
        if($value != null){
            $value = unserialize($value);
        }
        return $value;
    }

    function getLabelAttribute($value){
        $combinations = $this->getAttribute('combinations');
        if($combinations){
            return $combinations->label;
        }
        return null;
    }

    function getGiftCardDetails(){
        $params = $this->getAttribute('params');
        if(!empty($params)){
            $html = '';
            $html .= "<strong>". trans('template.giftcard.gc_recipient_name') .":</strong> " . $params['gc_recipient_name'] . '<br>';
            $html .= "<strong>". trans('template.giftcard.gc_recipient_email') .":</strong> " . $params['gc_recipient_email'];
            $url = Link::to('product', $this->product_id) . '?cart_detail=' . $this->id;
            $html .= " <a style='text-decoration: underline; margin-left: 10px' href='$url'>(MODIFICA)</a>";
            return $html;
        }
        return null;
    }

    function hasGiftCardAsGift(){
        $params = $this->getAttribute('params');
        if(!empty($params)){
            return isset($params['gift_selected']) and $params['gift_selected'] == 1;
        }
        return false;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CartProductPresenter($this);
    }

}

class CartProductPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}
