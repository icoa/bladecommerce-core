<?php


/**
 * MessageTemplate
 *
 * @property integer $id
 * @property boolean $active
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate whereDeletedBy($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class MessageTemplate extends MultiLangModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'message_templates';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name","message");
    protected $db_fields = array( "active" );
    protected $lang_model = 'MessageTemplate_Lang';
    protected $db_fields_cloning = array(
        "active" => 0,
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;



}


/**
 * MessageTemplate_Lang
 *
 * @property integer $message_template_id
 * @property string $lang_id
 * @property string $name
 * @property string $message
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate_Lang whereMessageTemplateId($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\MessageTemplate_Lang whereMessage($value)
 */
class MessageTemplate_Lang extends Eloquent  {


    protected $table = 'message_templates_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function message()
    {
        return $this->belongsTo('MessageTemplate');
    }


}