<?php

use App\Models\User;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use services\Morellato\Geo\AddressResolver;
use Illuminate\Support\Str;

/**
 * MorellatoShop
 *
 * @property integer $id
 * @property string $cd_neg
 * @property string $cd_internal
 * @property string $name
 * @property string $description
 * @property string $ie
 * @property string $tel
 * @property string $fax
 * @property string $area_mng
 * @property string $area
 * @property string $city
 * @property string $address
 * @property string $cap
 * @property string $state
 * @property string $cd_sped
 * @property string $email
 * @property string $excerpt
 * @property string $link
 * @property float $latitude
 * @property float $longitude
 * @property string $source
 * @property boolean $featured
 * @property boolean $engraving
 * @property boolean $active
 * @property integer $affiliate_id
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read mixed $engraving_label
 * @property-read mixed $height
 * @property-read string $code
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCdNeg($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCdInternal($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereIe($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereTel($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereAreaMng($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereArea($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCap($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCdSped($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereExcerpt($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereLatitude($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereLongitude($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereFeatured($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereEngraving($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\MorellatoShop whereDeletedAt($value)
 * @property-read mixed $marker 
 */
class MorellatoShop extends SingleModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'mi_shops';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        'cd_neg',
        'cd_internal',
        'name',
        'description',
        'ie',
        'tel',
        'fax',
        'area_mng',
        'area',
        'city',
        'address',
        'cap',
        'state',
        'cd_sped',
        'email',
        'latitude',
        'longitude',
        'featured',
        'engraving',
        'active',
        'excerpt',
        'link',
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new MorellatoShopPresenter($this);
    }


    static function options()
    {
        $rows = MorellatoShop::orderBy('cd_neg')->select('id', 'name')->get();
        $data = ['' => 'Seleziona...'];
        foreach ($rows as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    static function codes()
    {
        $rows = MorellatoShop::orderBy('cd_neg')->select('id', 'name', 'cd_neg')->get();
        $data = ['' => 'Seleziona...'];
        foreach ($rows as $row) {
            $data[$row->cd_neg] = $row->name;
        }
        return $data;
    }


    static function getByCode($code)
    {
        return MorellatoShop::where('cd_neg', trim($code))->first();
    }

    /**
     * Try to create a backend user automatically
     */
    function createUser()
    {
        if ($this->email != '') {
            $user = User::where('email', $this->email)->first();
            if (is_null($user)) {
                //create the user
                $data = [
                    'email' => $this->email,
                    'activated' => true,
                    'negoziando' => 1,
                    'first_name' => $this->name,
                    'last_name' => '',
                    'shop_id' => $this->id,
                    'password' => 'bs' . $this->cd_neg,
                ];

                $user = Sentry::createUser($data);

                // Find the group using the group id
                $adminGroup = Sentry::findGroupById(2);

                // Assign the group to the user
                $user->addGroup($adminGroup);
            }
        }
    }

    /**
     * return User
     */
    function getUser(){
        User::where('shop_id', $this->id)
            ->where('negoziando', 1)
            ->where('activated', 1)
            ->first();
    }

    function getFullAddress()
    {
        $tokens = [];
        if (strlen(trim($this->address)) > 0) {
            $tokens[] = $this->address;
        }
        if (strlen(trim($this->cap)) > 0) {
            $tokens[] = $this->cap;
        }
        if (strlen(trim($this->city)) > 0) {
            $tokens[] = $this->city;
        }
        if (strlen(trim($this->state)) > 0) {
            $tokens[] = $this->state;
        }
        return implode(' ', $tokens);
    }

    function getHtmlAddress($withBr = false)
    {
        $address = '';
        if (strlen(trim($this->address)) > 0) {
            $address = $this->address;
        }
        if (strlen(trim($this->cap)) > 0) {
            $address .= ($withBr ? '<br>' : ', ') . $this->cap;
        }
        if (strlen(trim($this->city)) > 0) {
            $address .= ' - ' . $this->city;
        }
        if (strlen(trim($this->state)) > 0) {
            $address .= ' (' . $this->state . ')';
        }
        return $address;
    }

    function resolveAddress()
    {
        if (is_null($this->latitude) or is_null($this->longitude)) {
            $address = $this->getFullAddress();
            $service = new AddressResolver();
            $geo = $service->make($address);
            if ($geo and isset($geo['lat']) and isset($geo['long'])) {
                $this->latitude = $geo['lat'];
                $this->longitude = $geo['long'];
                $this->save(['timestamps' => false]);
            }
        }
    }


    function getDistance()
    {
        return number_format($this->distance, 1, ',', '') . ' km';
    }

    function getDescription($withBr = false, $withExtra = true)
    {
        $desc = '';
        if (strlen(trim($this->excerpt)) > 0) {
            $desc = "<h5>$this->excerpt</h5>";
        }
        $address = $this->getHtmlAddress($withBr);
        $desc .= '<strong>' . $address . '</strong>' . ( $withBr ? '<br>' : ', ');
        if ($this->tel != '') {
            $desc .= "<b>Tel:</b> $this->tel";
        } else {
            $desc .= '';
        }
        if($withExtra){
            $desc .= $this->getExtraDescription();
        }
        return $desc;
    }

    function getExtraDescription() {
        $desc = '';
        /*if($this->email != ''){
            $desc .= "Email: <a href='mailto:$this->email'>$this->email</>";
        }*/
        if (Theme::getThemeName() != 'admin' and $this->engraving == 1) {
            $text = $this->getEngravingLabelAttribute();
            $style = \FrontTpl::isMobileTheme() ? "font-size: 9px" : "text-transform: uppercase; color:#fff; font-size:100%; font-weight: normal";
            $desc .= "<div style='margin-top:3px;'><span class='label' style='$style; background-color: #222'><img style='vertical-align: middle;height: 16px; margin-right: 5px; width:auto !important; border-radius: 4px; background-color: #fff' src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYBAMAAAFlXhBdAAAAA3NCSVQICAjb4U/gAAAAMFBMVEX///8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAv3aB7AAAAEHRSTlMAESIzRFVmd4iZqrvM3e7/dpUBFQAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAADLSURBVBiVTU9LDgFBEK3xDSLtACRzA26ApcTC1s4JWDkPWzsysZ6xs+sb+GxstUgIwVNVMxPeot+r16+6qokYjjxSLAlCIMNcuor2WTgi49SbsgzncXhfk3OCD5E3cXfpA1+TWeDA1BtRioGe3kvJINIZuIlnwa9kbZ8bC8CFrSo0YBxaTE2jG0U4CdV9+kdj9dMVYJPqIhhBUljW76Rv3LF4iMjszmx31+qWJY5k3TaCma6mvc98iGOsC88Spw5xMSxj20nH5X4/pS92o1n1Z0yLDAAAAABJRU5ErkJggg=='/> $text</span></div>";
        }
        return $desc;
    }

    function getMapsLink()
    {
        $pattern = 'http://www.google.com/maps/place/{lat},{long}';
        $data = [
            '{lat}' => $this->latitude,
            '{long}' => $this->longitude,
        ];
        $url = str_replace(array_keys($data), array_values($data), $pattern);
        if (strlen(trim($this->link)) > 0 and \Str::startsWith($this->link, 'https')) {
            $url = $this->link;
        }
        $btn = trans('template.omni.routes');
        return "<a target='_blank' class='viewExtMap btn' href='$url'>$btn</a>";
    }

    function getButton($class = null, $isShort = false)
    {
        $selected = \CartManager::getDeliveryStoreId();
        $labelDefault = $isShort ? trans('template.omni.short.btn_select') : trans('template.omni.btn_select');
        $labelSelected = $isShort ? trans('template.omni.short.btn_selected') : trans('template.omni.btn_selected');
        $label = $labelDefault;
        if ($this->id == $selected) {
            $class .= ' selected btn-selected';
            $label = $labelSelected;
        }
        return "<span style='background-color: #388e3c' class='btnHolder $class checkout-action' data-action='selectStoreFromMap' data-text='$labelSelected' data-textdefault='$labelDefault' data-id='{$this->id}'>$label</span>";
    }

    function infoWindow($simple = false)
    {
        $desc = $this->getDescription(false, false);
        $extra = $this->getExtraDescription();
        $btn = $simple ? $this->getMapsLink() : $this->getButton();
        $html = "<div class='infoWindow'><h3 style='text-align: left;line-height: 1.5;'>{$this->name}</h3><p>$desc</p><p>$btn</p><p>$extra</p></div>";
        return $html;
    }

    function toHtml( $trans_key = 'template.omni.html_email')
    {
        $title = trans($trans_key);
        $html = "<h5>$title</h5>";
        $desc = $this->getDescription(true);
        $link = $this->getMapsLink();
        $html .= "<div class='infoWindow'><h3 style='text-align: left;line-height: 1.5;'>{$this->name}</h3><p>$desc</p><p>$link</p></div>";
        return $html;
    }

    function getEngravingLabelAttribute()
    {
        if ($this->engraving == 0)
            return null;

        return lex('lbl_engraving');
    }

    function hasExcerpt()
    {
        if (strlen(trim($this->excerpt)) > 0) {
            return true;
        }
        return false;
    }

    function hasEngraving()
    {
        return $this->engraving == 1;
    }

    function getHeightAttribute()
    {
        $height = 50;

        if ($this->hasEngraving() or $this->hasExcerpt()) {
            $height = 60;
        }

        if ($this->hasEngraving() and $this->hasExcerpt()) {
            $height = 80;
        }

        return $height;
    }

    function getMarkerAttribute()
    {
        $marker = ($this->ie === null) ? 'marker' : $this->ie;
        return "/media/images/map/{$marker}@2x.png";
    }

    /**
     * @return string|null
     */
    function getEmailAddress(){
        if(filter_var($this->getAttribute('email'), FILTER_VALIDATE_EMAIL)){
            return $this->getAttribute('email');
        }
        $user = $this->getUser();
        if($user){
            return $user->email;
        }
        return null;
    }

    /**
     * @return array
     */
    public function asJson(){
        return [
            'id' => $this->id,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'marker' => $this->getMarkerAttribute(),
        ];
    }

    /**
     * @return string
     */
    public function getCodeAttribute(){
        return Str::upper(trim($this->cd_neg));
    }

}

class MorellatoShopPresenter extends Presenter
{


}
