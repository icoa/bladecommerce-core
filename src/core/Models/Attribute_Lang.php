<?php


/**
 * Attribute_Lang
 *
 * @property integer $attribute_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property string $default_value_text
 * @property boolean $default_value_yesno
 * @property string $default_value_date
 * @property string $default_value_textarea
 * @property string $txt_prefix
 * @property string $txt_suffix
 * @property string $txt_dropdown
 * @property string $tooltip_label
 * @property string $tooltip_value
 * @property boolean $published
 * @property-read \Attribute $attribute
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereAttributeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereDefaultValueText($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereDefaultValueYesno($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereDefaultValueDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereDefaultValueTextarea($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereTxtPrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereTxtSuffix($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereTxtDropdown($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereTooltipLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang whereTooltipValue($value)
 * @method static \Illuminate\Database\Query\Builder|\Attribute_Lang wherePublished($value)
 * @method static \Attribute_Lang lang($lang_id)
 */
class Attribute_Lang extends Model_Lang  {


    protected $table = 'attributes_lang';

    
    public function attribute()
    {
        return $this->belongsTo('Attribute');
    }
    
    public function scopeLang($query,$lang_id)
    {
        return $query->where('lang_id',$lang_id);
    }

}