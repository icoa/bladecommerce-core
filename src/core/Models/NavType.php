<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * NavType
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property boolean $isModifier
 * @property boolean $isBase
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\NavType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\NavType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\NavType whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\NavType whereIsModifier($value)
 * @method static \Illuminate\Database\Query\Builder|\NavType whereIsBase($value)
 */
class NavType extends SingleModel implements PresentableInterface
{

    const TYPE_DEFAULT = 1;
    const TYPE_LIST = 2;
    const TYPE_STATUS = 3;
    const TYPE_QUANTITY = 4;
    const TYPE_PRICERANGE = 5;

    const TYPE_CODE_DEFAULT = 'default';
    const TYPE_CODE_LIST = 'list';
    const TYPE_CODE_STATUS = 'status';
    const TYPE_CODE_QUANTITY = 'quantity';
    const TYPE_CODE_PRICERANGE = 'pricerange';

    /* LARAVEL PROPERTIES */
    protected $table = 'nav_types';
    protected $guarded = array();
    protected $isBlameable = FALSE;
    public $timestamps = FALSE;
    public $softDelete = false;
    public $db_fields = array(
        "name"
    );

    /**
     * @return array
     */
    static function getNavTypesCodes()
    {
        return [
            self::TYPE_DEFAULT => self::TYPE_CODE_DEFAULT,
            self::TYPE_LIST => self::TYPE_CODE_LIST,
            self::TYPE_STATUS => self::TYPE_CODE_STATUS,
            self::TYPE_QUANTITY => self::TYPE_CODE_QUANTITY,
            self::TYPE_PRICERANGE => self::TYPE_CODE_PRICERANGE,
        ];
    }

    /**
     * @param $id
     * @return string
     */
    static function navTypeIdToCode($id)
    {
        $codes = self::getNavTypesCodes();
        return isset($codes[$id]) ? $codes[$id] : self::TYPE_CODE_DEFAULT;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new NavTypePresenter($this);
    }

}

class NavTypePresenter extends Presenter
{


}