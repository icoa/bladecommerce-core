<?php

/**
 * GiftCardMovement
 *
 * @property integer $id
 * @property integer $gift_card_id
 * @property integer $customer_id
 * @property integer $order_id
 * @property float $amount
 * @property \Carbon\Carbon $created_at
 * @property integer $created_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $updated_by
 * @property \Carbon\Carbon $deleted_at
 * @property integer $deleted_by
 * @property-read GiftCard $giftCard
 * @property-read Order $order
 * @property-read Customer $customer
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereGiftCardId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\GiftCardMovement whereDeletedBy($value)
 */
class GiftCardMovement  extends SingleModel
{
    protected $table = 'gift_cards_movements';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    
    public $db_fields = array(
        'gift_card_id',
        'customer_id',
        'order_id',
        'amount',
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function giftCard(){
        return $this->belongsTo(GiftCard::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function order(){
        return $this->belongsTo(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    function customer(){
        return $this->belongsTo(Customer::class);
    }
}