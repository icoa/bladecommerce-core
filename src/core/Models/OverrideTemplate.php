<?php

use Core\Condition\RuleResolver;

/**
 * OverrideTemplate
 *
 * @property integer $id
 * @property string $name
 * @property string $sdesc
 * @property string $view
 * @property integer $position
 * @property boolean $active
 * @property string $mode
 * @property string $actions
 * @property string $conditions
 * @property boolean $apply
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereView($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereMode($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereActions($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereConditions($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereApply($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\OverrideTemplate whereDeletedAt($value)
 */
class OverrideTemplate extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'override_rules';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $db_fields = array(
        "name",
        "sdesc",
        "view",
        "active",
        "position",
        "actions",
        "conditions",
        "mode",
        "apply",
    );


    public function __resolve(array $resolvable){
        $actions = unserialize($this->actions);
        $mode = $this->mode;
        $asserts = [];
        \Utils::log($resolvable,__METHOD__);
        \Utils::log($actions,__METHOD__);
        if(is_array($actions) AND count($actions) > 0){
            foreach($actions as $action){
                $action = (object)$action;
                if(isset($resolvable[$action->type]) AND $resolvable[$action->type] == $action->id){
                    $asserts[] = 1;
                }else{
                    $asserts[] = 0;
                }
            }
        }
        $size = count($asserts);
        $checksum = $size > 0 ? array_sum($asserts) : -1;
        $isValid = ($mode == 'and') ? $checksum === $size : $checksum >= 1;
        return $isValid ? $this->view : false;
    }

    public function canLoad(){
        if($this->conditions == null OR trim($this->conditions) == ''){
            return false;
        }

        $rr = new RuleResolver();
        $rr->setRules($this->conditions);
        $assert = $rr->bindRules(null);
        return ($assert == 'true');
    }

    public function resolve(){
        return $this->canLoad() ? $this->view : false;
    }


}