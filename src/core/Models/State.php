<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * State
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $zone_id
 * @property string $name
 * @property string $iso_code
 * @property string $alias
 * @property integer $tax_behavior
 * @property boolean $active
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\State whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereZoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereIsoCode($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereAlias($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereTaxBehavior($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\State whereDeletedAt($value)
 */
class State extends SingleModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'states';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        "country_id",
        "name",
        "iso_code",
        "active",
    );

    /**
     * @param $iso
     * @param $country_id
     *
     * @return State|null
     */
    static function getStateByIso($iso, $country_id = null)
    {
        $key = is_null($country_id) ? 'state-' . $iso : 'state-' . $iso . '-' . $country_id;
        if (\Registry::has($key)) {
            return \Registry::get($key);
        }
        $state = is_null($country_id) ? State::where('iso_code', $iso)->first() : State::where('iso_code', $iso)->where('country_id', $country_id)->first();
        \Registry::set($key, $state);
        return $state;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new StatePresenter($this);
    }

}

class StatePresenter extends Presenter
{


}
