<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Language
 *
 * @property string $id
 * @property string $name
 * @property boolean $active
 * @property string $language_code
 * @property string $locale
 * @property string $date_format_lite
 * @property string $date_format_full
 * @property boolean $is_rtl
 * @property boolean $position
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Language whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereLanguageCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereLocale($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereDateFormatLite($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereDateFormatFull($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereIsRtl($value)
 * @method static \Illuminate\Database\Query\Builder|\Language wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Language whereDeletedAt($value)
 */
class Language extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'languages';    
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    
    public $db_fields = array(        
        "name",
        "language_code",
        "date_format_lite",
        "date_format_full",
        "is_rtl",
        "position",
        "active",
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new LanguagePresenter($this);
    }
    
}

class LanguagePresenter extends Presenter {
    

}
