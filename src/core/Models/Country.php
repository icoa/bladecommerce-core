<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Country
 *
 * @property integer $id
 * @property integer $zone_id
 * @property integer $currency_id
 * @property string $iso_code
 * @property string $iso_code3
 * @property integer $iso_number
 * @property integer $call_prefix
 * @property boolean $active
 * @property boolean $contains_states
 * @property boolean $need_identification_number
 * @property boolean $need_zip_code
 * @property string $zip_code_format
 * @property boolean $display_tax_label
 * @property boolean $eu
 * @property string $default_lang
 * @property string $accepted_lang
 * @property boolean $gm
 * @property string $address_format
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Country whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereZoneId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereIsoCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereIsoCode3($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereIsoNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereCallPrefix($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereContainsStates($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereNeedIdentificationNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereNeedZipCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereZipCodeFormat($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereDisplayTaxLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereEu($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereDefaultLang($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereAcceptedLang($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereGm($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereAddressFormat($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Country whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Country extends MultiLangModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'countries';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;
    public $has_published = false;



    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    protected $db_fields = array("zone_id", "currency_id", "iso_code", "call_prefix", "active", "contains_states", "need_identification_number", "need_zip_code", "zip_code_format", "display_tax_label", "default_lang", "eu", "address_format");
    protected $lang_model = 'Country_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CountryPresenter($this);
    }

    /**
     * @return bool
     */
    function hasStates(){
        $rows = \State::where('country_id',$this->id)->whereActive(1)->get();
        return count($rows) > 0;
    }

    /**
     * @return bool
     */
    function isEuropean(){
        return $this->eu == 1;
    }

    /**
     * The default country
     *
     * @return bool
     */
    function isLocalCountry(){
        $defaultCountry = \Core::getDefaultCountry();
        if($defaultCountry and $this->id === $defaultCountry->id)
            return true;

        if($this->default_lang == 'it')
            return true;

        return false;
    }

}

class CountryPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}

/**
 * Country_Lang
 *
 * @property integer $country_id
 * @property string $lang_id
 * @property string $name
 * @method static \Illuminate\Database\Query\Builder|\Country_Lang whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Country_Lang whereName($value)
 * @property-read \Country $country
 */
class Country_Lang extends Eloquent  {


    protected $table = 'countries_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function country()
    {
        return $this->belongsTo('Country');
    }


}