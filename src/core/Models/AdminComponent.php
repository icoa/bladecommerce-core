<?php  
/**
 * AdminComponent
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $controller
 * @property string $namedRoute
 * @property boolean $isNav
 * @property boolean $active
 * @property boolean $disabled
 * @property string $iconImg
 * @property boolean $exact
 * @property integer $position
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereController($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereNamedRoute($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereIsNav($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereDisabled($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereIconImg($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent whereExact($value)
 * @method static \Illuminate\Database\Query\Builder|\AdminComponent wherePosition($value)
 */
class AdminComponent extends \Eloquent {
 
    protected $table = 'widgets';

    public function getUrl(){
        try{
            $url = ($this->namedRoute != '') ? URL::route($this->namedRoute,[],false) : URL::action($this->controller,[],false);
        } catch (Exception $ex) {
            $url = "";
        }
        
        return $url;
    }
}