<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Category
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property-read mixed $link
 * @property-read \Illuminate\Database\Eloquent\Collection|\Category[] $subcategories
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Category whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Category wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Category whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Category wherePosition($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Category extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'categories';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name","singular", "slug", "published", "ldesc", "image_default", "image_thumb", "image_zoom", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical", "head", "footer");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("parent_id","position","robots", "ogp", "ogp_type", "ogp_image", "public_access");
    protected $lang_model = 'Category_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    protected $defaults = [
        'robots' => 'global',
        'ogp' => '0',
        'ogp_type' => 'product',
        'public_access' => 'P',
    ];

    protected function getDefaults(){
        $defaults = $this->defaults;
        $defaults['position'] = Brand::max('position') + 1;
        return $defaults;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CategoryPresenter($this);
    }

    public function hasAttributeOption($id){
        $query = "select distinct(category_id) from categories_products where product_id in (select id from products where brand_id = ?)";
        $rows = DB::select($query,[$this->id]);
        $data = [];
        foreach($rows as $row){
            $data[] = $row->category_id;
        }
        $this->categories = $data;
    }

    public function getLinkAttribute(){
        return Link::absolute()->to('category',$this->id);
    }

    public function subcategories(){
        return $this->hasMany('Category','parent_id');
    }

    public function getAncestor(){
        if($this->parent_id > 0){
            $this->ancestor = Category::getObj($this->parent_id);
            if($this->ancestor){
                $this->ancestor->getAncestor();
            }
        }else{
            $this->ancestor = null;
        }
        return $this->ancestor;
    }

    public function mapAncestors(&$ancestors){
        $ancestor = $this->getAncestor();
        if($ancestor){
            $ancestors[] = $ancestor;
            $ancestor->mapAncestors($ancestors);
        }
    }

}

class CategoryPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

    public function presentStarting() {
        return Format::datetime($this->starting_at);
    }

    public function presentFinishing() {
        return Format::datetime($this->finishing_at);
    }

}


/**
 * Category_Lang
 *
 * @property integer $category_id
 * @property string $lang_id
 * @property string $name
 * @property string $singular
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @property string $canonical
 * @property string $metafacebook
 * @property string $metatwitter
 * @property string $metagoogle
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereSingular($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereImageZoom($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Category_Lang whereFooter($value)
 * @property-read \Category $category
 */
class Category_Lang extends Eloquent  {


    protected $table = 'categories_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function category()
    {
        return $this->belongsTo('Category');
    }


}