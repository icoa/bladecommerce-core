<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 1-apr-2014 9.50.36
 */

/**
 * Employer
 *
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $permissions
 * @property boolean $activated
 * @property string $activation_code
 * @property string $activated_at
 * @property string $last_login
 * @property string $persist_code
 * @property string $reset_password_code
 * @property string $first_name
 * @property string $last_name
 * @property string $remember_token
 * @property string $profile
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property boolean $negoziando
 * @property boolean $clerk
 * @property integer $shop_id
 * @property integer $affiliate_id
 * @method static \Illuminate\Database\Query\Builder|\Employer whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer wherePermissions($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereActivated($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereActivationCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereActivatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereLastLogin($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer wherePersistCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereResetPasswordCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereProfile($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereNegoziando($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereClerk($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereAffiliateId($value)
 * @property boolean $has_2fa
 * @property string $gauth_token
 * @method static \Illuminate\Database\Query\Builder|\Employer whereHas2fa($value)
 * @method static \Illuminate\Database\Query\Builder|\Employer whereGauthToken($value)
 */
class Employer extends Eloquent {
    
    protected $table = 'users';
    
    protected $hidden = array('password');
    
    protected $guarded = array();
}