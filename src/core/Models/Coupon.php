<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Coupon
 *
 * @property integer $id
 * @property integer $cart_rule_id
 * @property string $code
 * @property boolean $active
 * @property integer $used_by
 * @property string $used_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereCartRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereUsedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereUsedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Coupon whereDeletedBy($value)
 */
class Coupon extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'coupons';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    public $db_fields = array(
        "code",
        "cart_rule_id",
    );


    
}