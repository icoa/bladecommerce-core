<?php

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use services\Observers\OrderHistoryObserver;
use services\Traits\HasParam;
use Illuminate\Database\Eloquent\Builder;


/**
 * OrderHistory
 *
 * @property integer $id
 * @property string $type
 * @property integer $order_id
 * @property integer $status_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property-read \Order $order
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereUpdatedBy($value)
 * @property string $params 
 * @property-read \OrderState $order_state 
 * @property-read \PaymentState $payment_state 
 * @method static \Illuminate\Database\Query\Builder|\OrderHistory whereParams($value)
 * @method static \OrderHistory onlyOrder()
 * @method static \OrderHistory onlyPayment()
 * @method static \OrderHistory withOrder($order)
 */
class OrderHistory extends SingleModel
{

    const TYPE_ORDER = 'O';
    const TYPE_PAYMENT = 'P';

    /* LARAVEL PROPERTIES */

    protected $table = 'order_history';
    protected $guarded = array();

    public $db_fields = array(
        'order_id',
        'type',
        'status_id',
        'params',
    );

    use HasParam;



    public static function boot()
    {
        parent::boot();

        // Setup event bindings...
        self::observe(new OrderHistoryObserver());
    }

    // RELATIONS

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('Order', 'order_id');
    }

    /**
     * @return BelongsTo
     */
    public function order_state()
    {
        return $this->belongsTo('OrderState', 'status_id');
    }

    /**
     * @return BelongsTo
     */
    public function payment_state()
    {
        return $this->belongsTo('PaymentState', 'status_id');
    }

    /**
     * @return OrderState|PaymentState
     */
    public function getState()
    {
        return $this->type === self::TYPE_ORDER ? $this->order_state : $this->payment_state;
    }

    /**
     * @return object
     */
    public function getUser()
    {
        return $this->createdBy();
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOnlyOrder(Builder $query)
    {
        return $query->where('type', self::TYPE_ORDER);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeOnlyPayment(Builder $query)
    {
        return $query->where('type', self::TYPE_PAYMENT);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeWithOrder(Builder $query, $order)
    {
        if ($order instanceof Order)
            $order = $order->id;

        if (is_numeric($order))
            return $query->where('order_id', $order);

        if (is_array($order))
            return $query->whereIn('order_id', $order);

        return $query;
    }

    /**
     * @param string $reason
     * @param null|array $tracking_by
     */
    public function setReason($reason, $tracking_by = null)
    {
        if (null === $reason or trim($reason) === '')
            return;

        $this->setParam('reason', (string)$reason);

        if ($tracking_by !== null && is_array($tracking_by) && !empty($tracking_by))
            $this->setParam('tracking_by', $tracking_by);

        //audit($this->getAttribute('params'), __METHOD__);
    }

    /**
     * @return string|null
     */
    public function getReason()
    {
        return $this->getParam('reason', null);
    }

    /**
     * @return array
     */
    public function getTrackingBy()
    {
        return $this->getParam('tracking_by', []);
    }


}

