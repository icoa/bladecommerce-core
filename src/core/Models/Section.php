<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Section
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $position
 * @property boolean $indexable
 * @property boolean $feedable
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Section whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Section wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Section wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereIndexable($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereFeedable($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Section whereDeletedAt($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class Section extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'sections';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "ldesc", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical", "head", "footer", "image_default", "image_thumb", "image_zoom");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("robots", "ogp", "ogp_type", "ogp_image", "public_access","parent_id","position");
    protected $lang_model = 'Section_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    function canDelete(){
        $obj = new SectionPresenter($this);
        if($obj->categories > 0 OR $obj->pages > 0){
            return "Non è possibile eliminare questo record perchè è riferito da altre entità";
        }
        return true;
    }

    static function slug($id, $lang_id){
        $slugs = [];
        self::getSlugs($id, $lang_id, $slugs);
        return implode("/",array_reverse($slugs));
    }

    static function getSlugs($id, $lang_id, &$slugs){
        $instance = new static;
        $obj = $instance::getPublicObj($id, $lang_id);
        $slugs[] = $obj->slug;
        if($obj->parent_id == 0){
            return;
        }
        return self::getSlugs($obj->parent_id, $lang_id, $slugs);
    }


    function getPages(array $params = []){

        $defaults = [
          'orderBy' => 'position',
          'orderDir' => 'asc',
          'limit' => 99,
          'start' => 0,
          'lang' => \Core::getLang(),
        ];
        $options = (object)array_merge($defaults,$params);
        \Utils::log($options,"getPages OPTIONS");

        $ids = \Page::rows($options->lang)
            ->where('section_id',$this->id)
            ->where('published',1)
            ->orderBy($options->orderBy, $options->orderDir)
            ->skip($options->start)
            ->take($options->limit)->lists('id');

        $rows = [];
        foreach($ids as $id){
            $rows[] = \Page::getPublicObj($id, $options->lang);
        }

        return $rows;
    }




    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new SectionPresenter($this);
    }

}

class SectionPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }


    public function presentCategories(){
        $id = $this->id;
        return \Section::withTrashed()->where("parent_id",$id)->count();
    }

    public function presentPages(){
        $id = $this->id;
        return \Page::withTrashed()->where("section_id",$id)->count();
    }

}



/**
 * Section_Lang
 *
 * @property integer $section_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $canonical
 * @property string $h1
 * @property string $metafacebook
 * @property string $metagoogle
 * @property string $metatwitter
 * @property string $head
 * @property string $footer
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereSectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereFooter($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Section_Lang whereImageZoom($value)
 * @property-read \Section $section
 */
class Section_Lang extends Eloquent  {


    protected $table = 'sections_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function section()
    {
        return $this->belongsTo('Section');
    }


}