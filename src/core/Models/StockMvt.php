<?php


/**
 * StockMvt
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $user_firstname
 * @property string $user_lastname
 * @property integer $stock_reason_id
 * @property integer $product_id
 * @property string $product_reference
 * @property string $product_name
 * @property boolean $sign
 * @property boolean $qty
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereUserFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereUserLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereStockReasonId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereProductReference($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereProductName($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereSign($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\StockMvt whereUpdatedAt($value)
 */
class StockMvt extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'stock_mvts';
    protected $guarded = array();
    protected $isBlameable = TRUE;
    public $timestamps = TRUE;
    public $softDelete = false;
    public $db_fields = array(
        "order_id",
        "user_id",
        "user_firstname",
        "user_firstname",
        "stock_reason_id",
        "product_id",
        "product_reference",
        "sign",
        "qty",
    );


    static function register($code, array $params = [], $force_user = 0){
        try{
            $reason = StockReason::getByCode($code);
            $reason_id = $reason ? $reason->id : 0;
            $sign = $reason ? $reason->sign : 1;
            \Utils::log("REASON ID: $reason_id | SIGN: $sign",__METHOD__);
            if($code == 'order'){
                if(isset($params['order_id']) AND isset($params['product_id'])){
                    $existent = StockMvt::where('order_id',$params['order_id'])->where("product_id",$params['product_id'])->first();
                    if($existent){
                        \Utils::log("EXISTENT ID: $existent->id | SIGN: $existent->sign | $existent->reason_id == $reason_id",'EXISTENT');
                    }
                    if($existent AND $existent->id > 0 AND $existent->reason_id == $reason_id AND $existent->sign == $sign){
                        return;
                    }
                }
            }


            $employer = ($force_user == 0) ? Sentry::getUser() : Sentry::findUserById($force_user);
            $user_firstname = null;
            $user_lastname = null;
            if($employer){
                $user_firstname =  $employer->first_name;
                $user_lastname =  $employer->last_name;
            }
            $params['user_firstname'] = $user_firstname;
            $params['user_lastname'] = $user_lastname;
            $params['stock_reason_id'] = $reason_id;
            $params['sign'] = $sign;
            self::create($params);
            \Utils::log($params);
        }catch(Exception $e){
            Utils::error($e->getMessage(),__METHOD__);
            Utils::error($e->getTraceAsString(),__METHOD__);
        }

    }

    function getReason(){
        $obj = StockReason::getObj($this->stock_reason_id);
        return $obj;
    }




}
