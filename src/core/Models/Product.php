<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use \Michelf\Markdown;
use Carbon\Carbon;
use services\Cache\FileCache;
use services\Elastic\ProductSearchableTrait;
use services\Membership\Traits\TraitAffiliateProduct;
use services\Morellato\Database\Sql\Connection;
use services\Traits\TraitLocalDebug;
use services\Traits\TraitProductBuilder;
use Core\Link;
use Core\Token;
use Core\Condition\RuleResolver;
use services\Traits\TraitProductTrendManager;

/**
 * Product
 *
 * @property integer $id
 * @property string $type
 * @property string $sku
 * @property string $supplier_sku
 * @property string $sap_sku
 * @property string $mastersku
 * @property integer $brand_id
 * @property integer $collection_id
 * @property integer $parent_id
 * @property integer $parent_attribute_id
 * @property integer $default_category_id
 * @property integer $main_category_id
 * @property float $weight
 * @property float $depth
 * @property float $width
 * @property float $height
 * @property float $package_weight
 * @property float $package_depth
 * @property float $package_height
 * @property float $package_width
 * @property string $condition
 * @property boolean $is_featured
 * @property boolean $is_new
 * @property boolean $is_soldout
 * @property string $new_from_date
 * @property string $new_to_date
 * @property string $ean13
 * @property string $upc
 * @property boolean $is_automatic_price
 * @property float $buy_price
 * @property float $sell_price
 * @property float $sell_price_wt
 * @property float $price_nt
 * @property float $price
 * @property float $unit_price
 * @property string $unity
 * @property boolean $show_price
 * @property integer $tax_group_id
 * @property float $additional_shipping_cost
 * @property boolean $indexed
 * @property boolean $feed
 * @property boolean $gift_enabled
 * @property string $visibility
 * @property boolean $manage_stock
 * @property boolean $use_config_manage_stock
 * @property integer $original_inventory_qty
 * @property integer $qty
 * @property integer $min_qty
 * @property boolean $use_config_min_qty
 * @property integer $min_sale_qty
 * @property boolean $use_config_min_sale_qty
 * @property integer $max_sale_qty
 * @property boolean $use_config_max_sale_qty
 * @property string $backorders
 * @property string $notify_stock
 * @property integer $notify_stock_qty
 * @property string $qty_increments
 * @property boolean $is_available
 * @property integer $availability_days
 * @property string $available_from_date
 * @property string $available_to_date
 * @property boolean $is_outlet
 * @property boolean $is_out_of_production
 * @property boolean $is_in_promotion
 * @property integer $wanted
 * @property string $redirect_type
 * @property integer $redirect_product_id
 * @property string $youtube
 * @property string $video
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $position
 * @property integer $default_img
 * @property boolean $export_ean
 * @property integer $cnt_categories
 * @property integer $cnt_images
 * @property integer $cnt_relation_accessories
 * @property integer $cnt_relation_products
 * @property integer $cnt_attributes
 * @property integer $cnt_combinations
 * @property integer $cnt_trends
 * @property string $cache_categories
 * @property string $cache_attributes
 * @property string $cache_attributes_val
 * @property integer $cache_gender
 * @property string $cache_price_rules
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property string $source
 * @property string $spin
 * @property integer $old_id
 * @property-read mixed $builder_image
 * @property-read mixed $permalink
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @property-read BuilderProduct $builder_product
 * @method static \Illuminate\Database\Query\Builder|\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSupplierSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSapSku($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereMastersku($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCollectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereParentAttributeId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereDefaultCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereMainCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePackageWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePackageDepth($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePackageHeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePackageWidth($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCondition($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsFeatured($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsNew($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsSoldout($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereNewFromDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereNewToDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereEan13($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUpc($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsAutomaticPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereBuyPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSellPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSellPriceWt($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePriceNt($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUnitPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUnity($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereShowPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereTaxGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereAdditionalShippingCost($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIndexed($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereFeed($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereGiftEnabled($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereVisibility($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereManageStock($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUseConfigManageStock($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereOriginalInventoryQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereMinQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUseConfigMinQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereMinSaleQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUseConfigMinSaleQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereMaxSaleQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUseConfigMaxSaleQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereBackorders($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereNotifyStock($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereNotifyStockQty($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereQtyIncrements($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsAvailable($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereAvailabilityDays($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereAvailableFromDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereAvailableToDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsOutlet($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsOutOfProduction($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereIsInPromotion($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereWanted($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereRedirectType($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereRedirectProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereYoutube($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereVideo($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Product wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereDefaultImg($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereExportEan($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntCategories($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntImages($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntRelationAccessories($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntRelationProducts($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntAttributes($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntCombinations($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCntTrends($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCacheCategories($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCacheAttributes($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCacheAttributesVal($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCacheGender($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCachePriceRules($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereSpin($value)
 * @method static \Illuminate\Database\Query\Builder|\Product whereOldId($value)
 * @method static \Product withCategories($categories)
 * @method static \Product withoutCategories($categories)
 * @method static \Product withBrands($brands)
 * @method static \Product withoutBrands($brands)
 * @method static \Product withCollections($collections)
 * @method static \Product withoutCollections($collections)
 * @method static \Product withAttributes($attributes)
 * @method static \Product withoutAttributes($attributes)
 * @method static \Product withAttributeOptions($options)
 * @method static \Product withoutAttributeOptions($options)
 * @method static \Product inStocks($is_out_of_production = 0)
 * @method static \Product outlet($is_outlet = 1)
 * @method static \Product soldout($is_soldout = 1)
 * @method static \Product withSource($source = 'local')
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @method static \Product withBuilderType($type)
 * @method static \Product onlyBases()
 * @method static \Product onlyDroplets()
 * @method static \Product withBuilders()
 * @method static \Product inStocksForAffiliates()
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read mixed $trends_codes
 * @method static \Product withTrends($trends = null)
 * @method static \Product withCover()
 * @method static \Product published($lang = 'default')
 * @method static \Product elasticIndexable()
 * @method static \Product withPriceRule($rules)
 */
class Product extends MultiLangModel implements PresentableInterface
{
    /* CONTANTS */
    const AVAILABILITY_TYPE_LOCAL = 'local';
    const AVAILABILITY_TYPE_SHOP = 'shop';
    const AVAILABILITY_TYPE_ONLINE = 'online';
    const AVAILABILITY_TYPE_OFFLINE = 'offline';
    const AVAILABILITY_TYPE_MIXED = 'mixed';
    const AVAILABILITY_TYPE_UNSOLVED = 'unsolved';
    const AVAILABILITY_TYPE_STAR = '*';

    const TYPE_DEFAULT = 'default';
    const TYPE_VIRTUAL = 'virtual';
    const TYPE_SERVICE = 'service';
    const TYPE_PACK = 'pack';

    /* LARAVEL PROPERTIES */
    use TraitLocalDebug;
    protected $localDebug = false;

    use TraitProductBuilder;
    use TraitAffiliateProduct;
    use TraitProductTrendManager;
    use ProductSearchableTrait;

    protected $table = 'products';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "sdesc", "ldesc", "metatitle", "metakeywords", "metadescription", "metafacebook", "metatwitter", "metagoogle", "gift_message", "h1", "head", "footer");
    //protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $lang_slaggable = null;
    protected $db_fields = array(
        "old_id",
        "position",
        "type",
        "sku",
        "supplier_sku",
        "brand_id",
        "collection_id",
        "parent_id",
        "parent_attribute_id",
        "default_category_id",
        "main_category_id",
        "weight",
        "depth",
        "width",
        "height",
        "condition",
        "is_new",
        "is_featured",
        "new_from_date",
        "new_to_date",
        "ean13",
        "upc",
        "is_automatic_price",
        "price",
        "price_nt",
        "sell_price",
        "sell_price_wt",
        "buy_price",
        "unit_price",
        "unity",
        "show_price",
        "tax_group_id",
        "additional_shipping_cost",
        "indexed",
        "feed",
        "gift_enabled",
        "visibility",
        "manage_stock",
        "use_config_manage_stock",
        "original_inventory_qty",
        "qty",
        "min_qty",
        "min_sale_qty",
        "max_sale_qty",
        "backorders",
        "notify_stock_qty",
        "notify_stock",
        "qty_increments",
        "is_available",
        "availability_days",
        "available_from_date",
        "available_to_date",
        "is_outlet",
        "is_out_of_production",
        "is_in_promotion",
        "redirect_type",
        "redirect_product_id",
        "robots",
        "ogp",
        "ogp_type",
        "ogp_image",
        "public_access",
        "youtube",
        "video",
        "package_weight",
        "package_depth",
        "package_width",
        "package_height",
        "source",
        "spin",
        "sap_sku",
        "is_soldout",
    );
    protected $lang_model = 'Product_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        //"slug" => SingleModel::CLONE_UNIQUE_SLUG,
        /*"sdesc" => "",
        "ldesc" => "",
        "h1" => "",
        "metatitle" => "",
        "metakeywords" => "",
        "metadescription" => "",*/
        "published" => 0
    );
    protected $db_fields_cloning = array(
        "sku" => SingleModel::CLONE_UNIQUE_TEXT,
        "ean13" => SingleModel::CLONE_UNIQUE_TEXT,
        "cnt_images" => 0,
    );

    protected $defaults = [
        "old_id" => null,
        "supplier_sku" => null,
        "parent_id" => 0,
        "parent_attribute_id" => 0,
        "weight" => 0,
        "depth" => 0,
        "width" => 0,
        "height" => 0,
        "new_from_date" => null,
        "new_to_date" => null,
        "ean13" => null,
        "upc" => null,
        "is_automatic_price" => null,
        "price" => null,
        "price_nt" => null,
        "sell_price" => null,
        "sell_price_wt" => null,
        "buy_price" => null,
        "unit_price" => null,
        "unity" => null,
        "original_inventory_qty" => null,
        "qty" => null,
        "min_sale_qty" => null,
        "max_sale_qty" => 0,
        "redirect_type" => null,
        "redirect_product_id" => null,
        "youtube" => null,
        "video" => null,
        "package_weight" => 0,
        "package_depth" => 0,
        "package_width" => 0,
        "package_height" => 0,
        'type' => 'default',
        'condition' => 'N',
        'is_featured' => 0,
        'is_new' => 0,
        'show_price' => 1,
        'tax_group_id' => 1,
        'indexed' => 1,
        'feed' => 1,
        'gift_enabled' => 1,
        'addition_shipping_cost' => 0,
        'visibility' => 'both',
        'manage_stock' => 1,
        'use_config_manage_stock' => 1,
        'min_qty' => 1,
        'is_available' => 1,
        'availability_days' => 16,
        'is_outlet' => 0,
        'robots' => 'global',
        'ogp' => 0,
        'ogp_type' => 'product',
        'public_access' => 'P',
        'export_ean' => 1,
        'is_in_promotion' => 0,
        'is_out_of_production' => 0,
        'available_from_date' => null,
        'available_to_date' => null,
        'qty_increments' => 'default',
        'notify_stock_qty' => 2,
        'notify_stock' => 'default',
        'backorders' => 'default',
        'source' => 'default',
        'is_soldout' => 0,
    ];


    public function getProductAttributesIds()
    {
        $rows = \DB::table("products_attributes")->where("product_id", $this->id)->lists("attribute_id");
        return $rows;
    }

    public function getProductAttributesOptionsIds($select_only = false, $params = [])
    {
        $builder = $select_only ? \DB::table("products_attributes")->whereIn('attribute_id', function ($query) {
            $query->select('id')->from('attributes')->whereNull('deleted_at')->whereIn('frontend_input', ['select', 'multiselect']);
        })->where("product_id", $this->id) : \DB::table("products_attributes")->where("product_id", $this->id);

        if (isset($params['whitelist']) and !empty($params['whitelist'])) {
            $builder->whereIn('attribute_id', $params['whitelist']);
        }

        return $builder->lists("attribute_val");
    }

    function setAllIds()
    {
        $data = [
            'category_ids' => $this->getCategoriesIds(),
            'carrier_ids' => $this->getCarriersIds(),
            'supplier_ids' => $this->getSuppliersIds(),
            'trend_ids' => $this->getTrendsIds(),
            'related_ids' => $this->getRelationsIds('related', true),
            'accessory_ids' => $this->getRelationsIds('accessory', true),
            'attribute_ids' => $this->getProductAttributesIds(),
            'attribute_option_ids' => $this->getProductAttributesOptionsIds(),
            'hasIds' => true,
        ];
        $this->setAttributes($data);

    }

    /*function expand(){
        $this->setAllIds();
        return $this;
    }*/

    public function setFullData()
    {

        if ($this->already_full) return;

        $categories = [];

        switch ($this->condition) {
            case 'N':
                $this->condition = 'new';
                break;
            case 'U':
                $this->condition = 'used';
                break;
            case 'R':
                $this->condition = 'refurbished';
                break;
        }

        if ($this->main_category_id > 0) {
            try {
                $category = \Category::getPublicObj($this->main_category_id);
                if (is_null($category)) {
                    $category = \Category::getObj($this->main_category_id);
                }
                $this->setAttribute('main_category_name', $category->name);
                $this->setAttribute('main_category_name_singular', ($category->singular != '') ? $category->singular : $category->name);
                $categories[] = $category->name;
                unset($category);
            } catch (Exception $e) {
                \Utils::log("Category [$this->main_category_id] not recognized", "Product expand ID: $this->id");
                $this->setAttribute('main_category_name', null);
                $this->setAttribute('main_category_name_singular', null);
            }
        }

        if ($this->default_category_id > 0) {
            try {
                $category = \Category::getPublicObj($this->default_category_id);
                if (is_null($category)) {
                    $category = \Category::getObj($this->default_category_id);
                }
                $this->setAttribute('category_name', $category->name);
                $this->setAttribute('category_name_singular', ($category->singular != '') ? $category->singular : $category->name);
                $categories[] = $category->name;
                unset($category);
            } catch (Exception $e) {
                \Utils::log("Category [$this->default_category_id] not recognized", "Product expand ID: $this->id");
                $this->setAttribute('category_name', null);
                $this->setAttribute('category_name_singular', null);
            }
        }

        $this->setAttribute('full_category_name', implode("/", array_unique($categories)));

        if ($this->brand_id > 0) {
            try {
                $brand = \Brand::getPublicObj($this->brand_id);
                if (is_null($brand)) {
                    $brand = \Brand::getObj($this->brand_id);
                }
                $this->setAttribute('brand_name', trim($brand->name));
                $this->setAttribute('brand_image_thumb', $brand->image_thumb);
                $this->setAttribute('brand_image_default', $brand->image_default);
                $this->setAttribute('brand_image_thumb_webp', $brand->image_thumb . '.webp');
                $this->setAttribute('brand_image_default_webp', $brand->image_default . '.webp');
                unset($brand);
            } catch (Exception $e) {
                \Utils::log("Brand [$this->brand_id] not recognized", "Product expand ID: $this->id");
                $this->setAttribute('brand_name', null);
                $this->setAttribute('brand_image_thumb', null);
                $this->setAttribute('brand_image_default', null);
                $this->setAttribute('brand_image_thumb_webp', null);
                $this->setAttribute('brand_image_default_webp', null);
            }
        }


        if ($this->collection_id > 0) {
            try {
                $collection = \Collection::getPublicObj($this->collection_id);
                if (is_null($collection)) {
                    $collection = \Collection::getObj($this->collection_id);
                }
                $this->setAttribute('collection_name', trim($collection->name));
                $this->setAttribute('collection_image_thumb', $collection->image_thumb);
                $this->setAttribute('collection_image_default', $collection->image_default);
                unset($collection);
            } catch (Exception $e) {
                \Utils::log("Collection [$this->collection_id] not recognized", "Product expand ID: $this->id");
                $this->setAttribute('collection_name', null);
                $this->setAttribute('collection_image_thumb', null);
                $this->setAttribute('collection_image_default', null);
            }
        }


        if ($this->cache_gender > 0) {
            $option = \AttributeOption::getPublicObj($this->cache_gender);
            if ($option) {
                $this->setAttribute("gender", $option->getPublicName());
                $this->setAttribute("gender_code", $option->uname);
                $this->setAttribute("attribute_gender", $option->id);
                unset($option);
            } else {
                $this->setAttribute('gender', null);
                $this->setAttribute('gender_code', null);
                $this->setAttribute('attribute_gender', null);
            }
        }


        $link = \Link::create('product', $this->id)->getLink();
        if ($link) {
            $this->setAttribute('link', $link);
        }
        $link_absolute = \Link::create('product', $this->id)->setScheme('absolute')->getLink();
        if ($link_absolute) {
            $this->setAttribute('link_absolute', $link_absolute);
        }

        $imgModel = ProductImage::getPublicObj($this->default_img);
        if (is_null($imgModel)) {
            $imgModel = ProductImage::getObj($this->default_img);
        }
        if ($imgModel) {
            $id = $imgModel->getImgId();
            $manipulations = ProductHelper::prepareImageResources($id, $this->slug);
            $this->setAttributes($manipulations);
        } else {
            $empty = \Site::emptyImg();
            $manipulations = ProductHelper::prepareImageResources(null, null, true);
            $this->setAttributes($manipulations);
        }

        //set the stocks
        $this->setStocks();
        //set the prices
        $this->setPrices();


        //\Utils::log("SELL_PRICE: $this->sell_price | PRICE: $this->price | PRICE_OFFICIAL: $this->price_official | PRICE FINAL: $this->price_final_raw | PRICE OFFER: $this->price_offer");

        $this->can_buy = true;

        if ($this->is_available == 0 OR $this->is_out_of_production == 1) {
            $this->can_buy = false;
        }

        //determine availabilityStatus for Schema.org
        $this->availabilityStatus = 'in_stock';
        $this->availabilitySchemaValue = 'http://schema.org/InStock';

        if ($this->qty <= 0 OR $this->is_out_of_production == 1) {
            if ($this->is_available == 0 OR $this->is_out_of_production == 1) {
                $this->availabilityStatus = 'out_of_stock';
                $this->availabilitySchemaValue = 'http://schema.org/OutOfStock';
            } else {
                $this->availabilityStatus = 'preorder';
                $this->availabilitySchemaValue = 'http://schema.org/PreOrder';
            }
        }
        $this->availabilitySchema = '<link itemprop="availability" href="' . $this->availabilitySchemaValue . '" />';
        $this->is_orderable = ($this->availabilityStatus == 'preorder');


        //quantity limit
        $this->qty_limit = ($this->max_sale_qty == 0) ? 99 : $this->max_sale_qty;

        $this->cartable = !($this->has_combinations == true or $this->isVirtual() or $this->affiliationEnabled == true or $this->is_soldout == 1);
        $this->already_full = true;

        $returnPayloads = Event::fire('product.full.expand', [&$this]);
        if (is_array($returnPayloads) and !empty($returnPayloads)) {
            //$product = $returnPayloads[0];
        }

        unset(
            $categories,
            $returnPayloads,
            $hasStock24,
            $hasStock48,
            $hasStockShop,
            $stock,
            $stockWarehouse,
            $imgModel,
            $linkAbsolute,
            $link
        );

        //audit($this->toArray(), 'PRODUCT MODEL');
        return $this;
    }

    /**
     * @param null $combination
     * @param null $currency
     * @param null $country
     * @return \Illuminate\Database\Eloquent\Collection|ProductPrice[]
     */
    protected function getProductPrices($combination = null, $currency = null, $country = null)
    {
        $key = "product-price-rule-{$this->id}";
        $cache = self::cache('price-rules-products');

        if(null !== $combination && !is_numeric($combination) && isset($currency->id)){
            $combination = $combination->id;
        }
        if($combination){
            $key .= '-cmb-' . $combination;
        }

        if(null !== $currency && !is_numeric($currency) && isset($currency->id)){
            $currency = $currency->id;
        }
        if($currency){
            $key .= '-cry-' . $currency;
        }

        if(null !== $country && !is_numeric($country) && isset($country->id)){
            $country = $country->id;
        }
        if($country){
            $key .= '-cty-' . $country;
        }

        //audit($key, 'CACHE QUERY KEY', __METHOD__);

        if ($cache->has($key)) {
            //audit('CACHE HIT', __METHOD__);
            return $cache->get($key);
        }
        //audit('exec query', __METHOD__, $this->id);
        $builder = \ProductPrice::where('active', 1)->where('global', 1)->where('product_id', $this->id)->orderBy('position');
        if($combination){
            $builder->where('combination_id', $combination);
        }else{
            $builder->where('combination_id', 0);
        }
        if($currency){
            $builder->whereIn('currency_id', [0, $currency]);
        }else{
            $builder->where('currency_id', 0);
        }
        if($country){
            $builder->whereIn('country_id', [0, $country]);
        }else{
            $builder->where('country_id', 0);
        }
        $items = $builder->get();

        //audit('CACHE MISS', __METHOD__);
        $cache->put($key, $items, 60 * 24);

        return $items;
    }

    /**
     * @param $id
     */
    public static function forgetPriceRulesById($id)
    {
        $key = "product-price-rule-{$id}";
        $cache = self::cache('price-rules-products');
        $cache->forget($key);
    }

    /**
     *
     */
    public function forgetPriceRules()
    {
        $key = "product-price-rule-{$this->id}";
        $cache = self::cache('price-rules-products');
        $cache->forget($key);
    }


    /**
     * @param ProductPrice $rule
     * @return bool
     */
    protected function isProductPriceProcessable(ProductPrice $rule)
    {
        if ($rule === null)
            return false;

        $now = time();
        $process_rule = false;
        if ($rule->date_from) {
            $rule_from = strtotime($rule->date_from);
            if ($now >= $rule_from) {
                $process_rule = true;
            } else {
                $process_rule = false;
            }
            if ($process_rule and $rule->date_to) {
                $rule_to = strtotime($rule->date_to);
                if ($now <= $rule_to) {
                    $process_rule = true;
                } else {
                    $process_rule = false;
                }
            }
        } else {
            if ($rule->date_to) {
                $rule_to = strtotime($rule->date_to);
                if ($now <= $rule_to) {
                    $process_rule = true;
                }
            }
        }
        if ($rule->date_from == null AND $rule->date_to == null) {
            $process_rule = true;
        }
        if ($process_rule) {
            $assert_country = \RestrictionHelper::country_id($rule->country_id);
            $assert_currency = \RestrictionHelper::currency_id($rule->currency_id);
            $assert_group = \RestrictionHelper::group_id($rule->customer_group_id);
            $process_rule = ($assert_country AND $assert_currency AND $assert_group);
        }
        return $process_rule;
    }

    public function setStocks()
    {
        $key = "stocks_{$this->id}";
        if (isset($this->combination) AND $this->combination != null) {
            $key .= "_cmb_{$this->combination->id}";
        }
        //combinations

        if (Registry::has($key) and isset($this->stocksProcessed)) {
            return;
        }
        $this->has_combinations = $this->hasCombinations();

        $hasStock24 = false;
        $hasStock48 = false;
        $hasStockShop = false;
        $stock = null;
        $stockWarehouse = null;

        if ($this->has_combinations == false) {
            $stock = $this->getSpecificStocks();
        } else {
            if (isset($this->combination) AND $this->combination != null) {
                $stock = $this->getSpecificStocks($this->combination->id);
            }
        }

        $this->stocksProcessed = true;
        $this->onlineQty = 0;
        $this->offlineQty = 0;
        if ($stock and $this->isMorellato()) {
            $this->stock = $stock;
            $this->onlineQty = $stock->quantity_24 + $stock->quantity_48;
            $stockInfo = $this->getStockDefinition($stock);
            //if the warehouse is one of this, there is no online qty
            switch ($stockInfo['warehouse']) {
                case 'NG':
                case 'W3':
                    $this->onlineQty = 0;
                    break;
            }
            $hasStock24 = $stockInfo['hasStock24'];
            $hasStock48 = $stockInfo['hasStock48'];
            $hasStockShop = $stockInfo['hasStockShop'];
            $stockWarehouse = $stockInfo['warehouse'];
            if ($stockInfo['sellable'] === false) {
                $this->is_available = 0;
            }
            $this->availability_days = $stockInfo['availability_days'];
            $this->qty = $stockInfo['qty'];
            $this->overall_qty = $stockInfo['overall_qty'];
            $this->offlineQty = $stockInfo['shop_qty'];
            if (isset($this->combination) AND $this->combination != null) {
                $this->combination->quantity = $stockInfo['qty'];
            }
        }

        //if the direct stock is null, and the product is 'morellato' and has combinations, recheck its quantities summing all combinations stocks
        if ($stock === null and $this->isMorellato() and $this->has_combinations) {
            //load the combinations
            $combinationsStocks = $this->getSpecificStocksCombinations();
            //audit($combinationsStocks, 'combinationsStocks');
            $combinationsTotals = [
                'quantity_24' => 0,
                'quantity_48' => 0,
                'quantity_shop' => 0,
            ];
            foreach ($combinationsStocks as $combinationsStock) {
                $combinationsTotals['quantity_24'] += $combinationsStock->quantity_24;
                $combinationsTotals['quantity_48'] += $combinationsStock->quantity_48;
                $combinationsTotals['quantity_shop'] += $combinationsStock->quantity_shop;
            }
            $this->onlineQty = $combinationsTotals['quantity_24'] + $combinationsTotals['quantity_48'];
            $this->offlineQty = $combinationsTotals['quantity_shop'];
            $this->qty = $combinationsTotals['quantity_24'] + $combinationsTotals['quantity_48'] + $combinationsTotals['quantity_shop'];
            $hasStock24 = $combinationsTotals['quantity_24'] > 0;
            $hasStock48 = $combinationsTotals['quantity_48'] > 0;
            $hasStockShop = $combinationsTotals['quantity_shop'] > 0;
        }

        $this->hasStock24 = $hasStock24;
        $this->hasStock48 = $hasStock48;
        $this->hasStockShop = $hasStockShop;
        $this->stockWarehouse = $stockWarehouse;
        //a product with inner stocks can rely only on affiliate availability
        $this->hasInnerStocks = ($this->hasStock24 or $this->hasStock48 or $this->hasStockShop);

        if (false) { //only for debug purpose
            $debugData = [
                'hasStock24' => $hasStock24 ? 'true' : 'false',
                'hasStock48' => $hasStock48 ? 'true' : 'false',
                'qty' => $this->qty,
                'availability_days' => $this->availability_days,
            ];
            Utils::log($debugData, 'MorellatoStocks');
            Utils::log($stock, '$stock');
        }

        //membership/affiliation stocks
        $this->affiliationEnabled = false;
        if (\Core::membership()) {
            $this->hasAffiliatesStocks = $this->hasAffiliateAvailability();
            $this->affiliatesCount = $this->getAffiliatesCount();
            if ($this->hasAffiliatesStocks) {
                //enable general affiliation
                $this->affiliationEnabled = true;
                if ($this->qty <= 0) {
                    $this->qty = $this->getOverallAffiliateStocksQuantity();
                    if ($this->qty > 0) {
                        $this->is_available = 1;
                    }
                }
                //if the product has no inner stocks, then switch scope to the first affiliate
                if ($this->hasInnerStocks === false and !$this->hasCombinations()) {
                    $this->affiliate = $this->getSingleAffiliate();
                    //audit($this->affiliate->prices, 'AFFILIATE BINDABLE PRICES');
                    $this->replacePrices($this->affiliate->prices);
                }
            }
        }

        Registry::set($key, true);
    }

    public function setPrices()
    {
        $key = "prices_{$this->id}";
        //audit_watch();
        $combination_id = null;

        if (isset($this->combination) AND $this->combination != null) {
            $key .= "_cmb_{$this->combination->id}";
            $combination_id = $this->combination->id;
        }

        if (Registry::has($key) and isset($this->price_final_raw)) {
            return;
        }

        //audit($this->toArray(), 'BEFORE SET PRICES');

        //combinations
        $this->has_combinations = $this->hasCombinations();

        if(isset($this->original_sell_price_wt) and (float)$this->original_sell_price_wt >= 0){

        }else{
            $this->setAttributes([
                'original_price' => $this->price,
                'original_sell_price' => $this->sell_price,
                'original_sell_price_wt' => $this->sell_price_wt,
                'original_buy_price' => $this->buy_price,
            ]);
        }

        $this->setAttribute('free_shipping', 0);
        $price_offer = null;
        $rows = $this->getProductPrices($combination_id, \Core::getCurrency(), \Core::getCountry());
        $price_rules = [];
        $price_currency_id = \Core::getDefaultCurrency()->id;
        //audit($this->toArray(), 'BEFORE', __METHOD__);

        foreach ($rows as $rule) {
            //audit($rule, '__RULE__');
            $process_rule = (bool)$this->isProductPriceProcessable($rule);
            //audit($process_rule ? 'TRUE' : 'FALSE', '__PROCESS_RULE__');
            if ($process_rule === true) {
                $price_rules[] = $rule->price_rule_id;
                $public_prices = \ProductHelper::calculateProductPrice($this, $rule);
                $price_with_taxes = $public_prices['price_wt'];
                $price = $public_prices['price'];
                $rule->reduction_value = $public_prices['reduction_value'];
                $this->price = $price_with_taxes;
                $this->sell_price = $price;
                $this->sell_price_wt = $price_with_taxes;
                $this->price_nt = $price;
                if ($rule->reduction_currency > 0) {
                    $price_currency_id = $rule->reduction_currency;
                }
                $price_offer = \ProductHelper::getRuleExplainedPublic($this->original_sell_price_wt, $price_with_taxes, $this->tax_group_id, $rule->reduction_type, $rule->reduction_value, $rule->reduction_currency, $rule->reduction_tax);
                $this->setAttribute('free_shipping', $rule->free_shipping);
                if ($rule->stop_other_rules == 1) {
                    break;
                }
            }
        }

        $this->setAttribute('price_rules', $price_rules);

        //price normal calculation
        $price_label = $this->original_sell_price_wt;
        $price_official = $this->original_sell_price_wt;
        $price_final = $this->price !== null ? $this->price : $this->original_sell_price_wt;
        $price_saving = $price_official - $price_final;
        if (abs($price_saving) <= 0.2) $price_saving = 0;

        $current_currency = \Core::getCurrency();

        $this->setAttribute('price_official', \Format::currency($price_official, true, $current_currency));
        $this->setAttribute('price_final', \Format::currency($price_final, true, $current_currency));
        $this->setAttribute('price_saving', \Format::currency($price_saving, true, $current_currency));
        $this->setAttribute('price_label', \Format::currency($price_label, false, $current_currency));
        $this->setAttribute('price_official_raw', $price_official);
        $this->setAttribute('price_saving_raw', $price_saving);
        $this->setAttribute('price_final_raw', $price_final);
        $this->setAttribute('price_offer', $price_offer);
        $this->setAttribute('price_currency_id', $price_currency_id);
        //alias
        $this->price_list = $this->price_official;
        $this->price_list_raw = $this->price_official_raw;
        $this->price_shop = $this->price_final;
        $this->price_shop_raw = $this->price_final_raw;
        if ($price_saving > 0) {
            $this->is_in_promotion = true;
        }

        unset($price_offer, $rows, $price_rules, $product_prices, $current_currency, $price_official, $price_saving, $price_final, $price_currency_id);

        //audit(__METHOD__.'::EOF');
        //audit($this->toArray(), 'AFTER', __METHOD__);

        Registry::set($key, true);
    }


    public function getPrice($type, $default = null)
    {
        $current_currency = \Core::getCurrency();
        $property = "price_{$type}_raw";
        if (isset($this->$property)) {
            return \Format::currency($this->$property, true, $current_currency);
        }
        return $default;
    }


    public function getLoadedAttributes()
    {
        if (isset($this->loaded_attributes) and count($this->loaded_attributes) > 0) {
            return $this->loaded_attributes;
        }
        $loaded_attributes = [];
        $attributes = $this->getFrontAttributes();
        $query = "SELECT * FROM products_attributes WHERE product_id=$this->id";
        $contextual_attributes = DB::select($query);
        $stored = [];
        foreach ($contextual_attributes as $contextual) {
            if (isset($stored[$contextual->attribute_id]) AND is_array($stored[$contextual->attribute_id])) {
                $stored[$contextual->attribute_id][] = $contextual->attribute_val;
            } else {
                $stored[$contextual->attribute_id] = [$contextual->attribute_val];
            }
        }
        /*foreach($attributes as $attr){
            if($attr->option_id > 0){
                $loaded_attributes[$attr->id] = $attr->option_id;
            }else{
                $loaded_attributes[$attr->id] = $attr->rawValue;
            }
        }*/
        $this->loaded_attributes = $stored;
        //Utils::log($this->loaded_attributes,"LOOOOOOOOOOOOOOAD");
        return $this->loaded_attributes;
    }


    public function setCompleteData()
    {
        //audit(__METHOD__, $this->id);
        $link = \Link::create('nav', 13);
        $related_links = [];
        $loaded_attributes = [];
        $parent_name = '';

        $this->defaultImg = \Site::img($this->defaultImgRelative, true);

        if ($this->main_category_id > 0) {
            $url = $link->setModifier('category', $this->main_category_id)->getLink(true);
            \FrontTpl::addBreadcrumb($this->main_category_name, $url);
            $parent_name .= " " . $this->main_category_name;
            //$total = \Catalog::getTotalBy('category', $this->main_category_id);
            $total = 1;
            if ($total > 0) {
                $absolute_link = new Link();
                $absolute_url = $absolute_link->create('category', $this->main_category_id)->getLink();
                $rl = new stdClass();
                $rl->label = trans('template.category');
                $rl->value = $this->main_category_name;
                $rl->url = $absolute_url;
                $rl->total = $total;
                $related_links[] = $rl;
            }
        }

        if ($this->default_category_id > 0 and $this->default_category_id != $this->main_category_id) {
            $url = $link->setModifier('category', $this->default_category_id)->getLink(true);
            \FrontTpl::addBreadcrumb($this->category_name, $url);
            $parent_name .= " " . $this->category_name;
            //$total = \Catalog::getTotalBy('category', $this->default_category_id);
            $total = 1;
            if ($total > 0) {
                $absolute_link = new Link();
                $absolute_url = $absolute_link->create('category', $this->default_category_id)->getLink();
                $rl = new stdClass();
                $rl->label = trans('template.subcategory');
                $rl->value = $this->category_name;
                $rl->url = $absolute_url;
                $rl->total = $total;
                $related_links[] = $rl;
            }
        }

        if ($this->brand_id > 0) {
            $url = $link->addModifier('brand', $this->brand_id)->getLink(true);
            \FrontTpl::addBreadcrumb($this->brand_name, $url);
            $parent_name .= " " . $this->brand_name;
            //$total = \Catalog::getTotalBy('brand', $this->brand_id);
            $total = 1;
            if ($total > 0) {
                $absolute_link = new Link();
                $absolute_url = $absolute_link->create('brand', $this->brand_id)->getLink();
                $rl = new stdClass();
                $rl->label = trans('template.brand');
                $rl->value = $this->brand_name;
                $rl->url = $absolute_url;
                $rl->total = $total;
                $related_links[] = $rl;
            }
        }

        if ($this->collection_id > 0) {
            $url = $link->addModifier('collection', $this->collection_id)->getLink(true);
            \FrontTpl::addBreadcrumb($this->collection_name, $url);
            $parent_name .= " " . $this->collection_name;
            //$total = \Catalog::getTotalBy('collection', $this->collection_id);
            $total = 1;
            if ($total > 0) {
                $absolute_link = new Link();
                $absolute_url = $absolute_link->create('collection', $this->collection_id)->getLink();
                $rl = new stdClass();
                $rl->label = trans('template.collection');
                $rl->value = $this->collection_name;
                $rl->url = $absolute_url;
                $rl->total = $total;
                $related_links[] = $rl;
            }
        }
        $this->setAttribute("parent_name", $parent_name);

        if ($this->cache_gender > 0) {
            $url = $link->addModifier('gender', $this->cache_gender)->getLink(true);
            $option = \AttributeOption::getPublicObj($this->cache_gender);
            if ($option) {
                \FrontTpl::addBreadcrumb($option->getPublicName(), $url);
                $this->setAttribute("gender", $option->getPublicName());
            }
        }
        \FrontTpl::addBreadcrumb($this->name);

        $this->setFrontAttributes();
        $attributes = \FrontTpl::loadAttributes();

        if ($this->cache_attributes != '') {
            $cache_attributes = explode("|", substr($this->cache_attributes, 1, -1));
            $cache_attributes_val = explode("|", substr($this->cache_attributes_val, 1, -1));
            for ($i = 0, $max = count($cache_attributes); $i < $max; $i++) {
                if (isset($cache_attributes[$i]) AND isset($cache_attributes_val[$i])) {
                    $attr = $cache_attributes[$i];
                    $value = $cache_attributes_val[$i];

                    if (isset($attributes[$attr])) {
                        $attribute = $attributes[$attr];
                        if ($attribute->is_nav == 1 OR $attribute->is_filterable == 1) {
                            //\Utils::log("$attr:$value","PROCESSING");


                            if (isset($this->_attributes_values[$value])) {
                                $loaded_attributes[$attribute->id] = $value;
                                //$total = \Catalog::getTotalBy('attribute', $value);
                                $total = 1;
                                if ($total > 0) {
                                    $absolute_link = new Link();
                                    if ($attribute->is_nav == 1) {
                                        $absolute_url = $absolute_link->create($attribute->code, $value)->getLink();
                                    } else {
                                        $absolute_url = $absolute_link->create('category', $this->main_category_id)->addFilter($attribute->code, $value)->getLink();
                                    }
                                    $rl = new stdClass();
                                    $rl->label = $attribute->name;
                                    $rl->value = $this->_attributes_values[$value];
                                    $rl->url = $absolute_url;
                                    $rl->total = $total;
                                    $related_links[] = $rl;
                                }

                            }
                        }
                    }
                }
            }
        }
        //\Utils::log($related_links,"RELATED LINKS");
        $this->related_links = $related_links;
        //$this->loaded_attributes = $loaded_attributes;
        //Utils::log($this->loaded_attributes,"LOADED ATTRIBUTES");

        // set prev/next products
        $this->setSiblings();

        $current_currency = \FrontTpl::getCurrency();
        // shipping costs
        $cost = $this->getShippingCost();
        $this->shipping_cost_raw = $cost;
        $this->shipping_cost = \Format::currency($cost, true, $current_currency);

        $promo_label = '';
        if (isset($this->price_rules) and count($this->price_rules) > 0) {
            foreach ($this->price_rules as $price_rule_id) {
                $pr = \PriceRule::getPublicObj($price_rule_id);
                if ($pr) $promo_label .= $pr->label . " ";
            }
            $promo_label = trim($promo_label);
        }
        $this->promo_label = $promo_label;

        $this->ogp_image_default = $this->defaultImg;
    }

    function getRelatedLinks()
    {
        return isset($this->related_links) ? $this->related_links : [];
    }


    private function getToken($str)
    {
        $token = new Token($str);
        $token->set('brand', $this->brand_name);
        $token->set('collection', $this->collection_name);
        $token->set('category', $this->main_category_name);
        $token->set('default_category', $this->category_name);
        $token->set('product', $this->name);
        $token->set('sku', $this->sku);
        return $token;
    }

    public function getFrontAttributes()
    {
        if (isset($this->_front_attributes)) {
            return $this->_front_attributes;
        }
        $this->setFrontAttributes();
        return $this->_front_attributes;
    }

    public function getFrontAttributesValues()
    {
        if (isset($this->_attributes_values)) {
            return $this->_attributes_values;
        }
        $this->setFrontAttributes();
        return $this->_attributes_values;
    }

    public function getAttributesMetadata()
    {
        return unserialize($this->metadata);
    }

    private function setFrontAttributes()
    {
        $elements = [];
        $data = unserialize($this->metadata);
        $attributes = \FrontTpl::loadAttributes();
        $attributes_values = [];
        //\Utils::log($data,"setFrontAttributes");
        //\Utils::log($attributes,"loaded setFrontAttributes");

        //$tokens = ['BRAND' => $this->brand_name, 'COLLECTION' => $this->collection_name, 'CATEGORY' => $this->main_category_name, 'DEFAULT_CATEGORY' => ];
        if (isset($data['attributes']) and !empty($data['attributes'])):
            foreach ($data['attributes'] as $obj) {
                //\Utils::log($obj->id,"Getting attribute");
                if (isset($attributes[$obj->id])) {
                    $attribute = $attributes[$obj->id];
                    $obj->tooltip_label = null;
                    $obj->tooltip_value = null;

                    //cache main attributes
                    if ($attribute->is_nav == 1) {
                        $this->setAttribute("attribute_" . $attribute->code, $obj->option_id);
                    }

                    if (trim($attribute->tooltip_label) != '') {
                        $token = $this->getToken($attribute->tooltip_label);
                        $token->set('value', $obj->value);
                        $label = $token->render();
                        $obj->tooltip_label = trim(Markdown::defaultTransform($label));
                    }

                    if (trim($attribute->tooltip_value) != '') {
                        $token = $this->getToken($attribute->tooltip_value);
                        $token->set('value', $obj->value);
                        $label = $token->render();
                        $obj->tooltip_value = trim(Markdown::defaultTransform($label));
                    }

                    if ($attribute->is_boolean == 1) {
                        if (count($obj->structure) > 0) {
                            foreach ($obj->structure as $s) {
                                $newObj = clone $obj;
                                $newObj->label = $s->name;
                                $newObj->value = trans('template.yes');
                                unset($newObj->structure);
                                $elements[$newObj->code . '_' . $s->option_id] = $newObj;
                                $attributes_values[$s->option_id] = $s->name;
                            }
                        }
                    } else {
                        if (count($obj->structure) > 1) {
                            foreach ($obj->structure as $s) {
                                $attributes_values[$s->option_id] = $s->name;
                            }
                        } else {
                            $attributes_values[$obj->option_id] = $obj->value;
                        }
                        unset($obj->structure);
                        $elements[$obj->code] = $obj;
                    }

                }
            }
        endif;
        $this->_front_attributes = $elements;
        $this->_attributes_values = $attributes_values;
        //\Utils::log($this->_front_attributes,"FRONT ATTRIBUTES");
        //\Utils::log($this->_attributes_values,"ATTRIBUTES VALUES");

    }

    public function getImages($with_groups = true)
    {
        if (isset($this->product_images)) {
            return $this->getAttribute('product_images');
        }
        $key = "product-images-{$this->id}";
        $images = \Registry::remember($key, function () use ($with_groups) {
            $lang_id = \Core::getLang();
            $rows = \ProductImage::rows($lang_id)->where('product_id', $this->id)->where('active', 1)->orderBy('cover', 'desc')->orderBy('position')->get();
            $slug = $this->slug;
            $images = [];
            $counter = 0;
            foreach ($rows as $row) {
                $counter++;
                $obj = (object)$row->toArray();
                $row->id = $row->getImgId();
                $name = ($counter > 1) ? $counter . '-' . $slug : $slug;
                $manipulations = ProductHelper::prepareImageResources($row->id, $name);
                foreach ($manipulations as $manipulation => $resource_path) {
                    $obj->$manipulation = $resource_path;
                }

                $images[] = $obj;
            }

            if ($with_groups == false) {
                return $images;
            }

            $product_id = $this->id;
            $related = \GroupImage::rows($lang_id)->whereIn('set_id', function ($query) use ($product_id) {
                $query->select(['image_set_id'])
                    ->from('cache_products_sets')
                    ->where('product_id', $product_id);
            })->where('active', 1)->orderBy('position')->get();

            if (count($related) > 0) {
                $counter = 0;
                foreach ($related as $row) {
                    $counter++;
                    $obj = (object)$row->toArray();
                    $name = ($counter > 1) ? $counter . '-' . $slug : $slug;
                    $manipulations = ProductHelper::prepareImageResources('g' . $row->id, $name);
                    foreach ($manipulations as $manipulation => $resource_path) {
                        $obj->$manipulation = $resource_path;
                    }
                    $obj->cover = 0;
                    $images[] = $obj;
                }
            }

            return $images;
        });

        //\Utils::log($images,"getImages");
        return $images;
    }


    public function getVideos()
    {
        if (isset($this->product_videos)) {
            return $this->product_videos;
        }
        $videos = [];
        $product_id = $this->id;

        $related = ImageSet::whereIn('id', function ($query) use ($product_id) {
            $query->select(['image_set_id'])
                ->from('cache_products_sets')
                ->where('product_id', $product_id);
        })->where('published', 1)->orderBy('position')->get();

        foreach ($related as $rel) {
            $youtube = trim($rel->youtube);
            $video = trim($rel->video);
            if ($youtube != '') {
                $this->youtube .= PHP_EOL . $youtube;
            }
            if ($video != '') {
                $this->video .= PHP_EOL . $video;
            }
        }

        $this->youtube = trim($this->youtube, PHP_EOL);
        $this->video = trim($this->video, PHP_EOL);

        if (trim($this->youtube) != '') {
            $temp = explode(PHP_EOL, $this->youtube);
            foreach ($temp as $v) {
                if ($v != '') {
                    if (\Str::contains($v, 'watch?v=')) {
                        $src = $v;
                    } else {
                        $src = 'http://www.youtube.com/watch?v=' . $v;
                    }
                    $o = new \stdClass();
                    $o->src = $src;
                    $o->type = 'youtube';
                    $videos[] = $o;
                }

            }
        }
        if (trim($this->video) != '') {
            $temp = explode(PHP_EOL, $this->video);
            foreach ($temp as $v) {
                if (\Str::contains($v, '//video')) {
                    $src = $v;
                } else {
                    $src = 'http://video.kronoshop.com/' . $v;
                }
                $o = new \stdClass();
                $o->src = \Site::root() . "/player/index.php?v=" . $src;
                $o->type = 'custom';
                $videos[] = $o;
            }
        }
        return $videos;
    }

    private function setSiblings()
    {
        //disable prev link in product pages
        return;
        $products = [];
        //$query = "SELECT id FROM products WHERE is_out_of_production=0 AND deleted_at is null AND id<>$this->id";
        $lang = \Core::getLang();
        $query = "SELECT id FROM cache_products WHERE id<>$this->id AND published_{$lang}=1";
        if ($this->collection_id > 0) {
            $query .= " AND collection_id=$this->collection_id";
        }
        if ($this->brand_id > 0) {
            $query .= " AND brand_id=$this->brand_id";
        }
        if ($this->default_category_id > 0) {
            $query .= " AND default_category_id=$this->default_category_id";
        }
        if ($this->sku != '') {
            $prev_query = $query . " AND sku <='$this->sku' ORDER BY sku DESC LIMIT 1";
        } else {
            $prev_query = $query . " AND position <=$this->position LIMIT 1";
        }
        $prev = \DB::selectOne($prev_query);
        if ($prev AND $prev->id > 0) {
            $link = \Link::create("product", $prev->id)->getLink(true);
            \FrontTpl::setPrevLink($link);
        }

        if ($this->sku != '') {
            $next_query = $query . " AND sku >='$this->sku' ORDER BY sku ASC LIMIT 1";
        } else {
            $next_query = $query . " AND position >=$this->position ASC LIMIT 1";
        }

        $next = \DB::selectOne($next_query);
        if ($next AND $next->id > 0) {
            $link = \Link::create("product", $next->id)->getLink(true);
            \FrontTpl::setNextLink($link);
        }
        //\Utils::log($next_query,"NEXT QUERY");
        //\Utils::log($prev_query,"PREV QUERY");
    }


    public function getVariantsProducts()
    {
        $max = 12;
        $products = [];
        $lang = \Core::getLang();

        $query = "SELECT id FROM cache_products WHERE id<>$this->id AND published_{$lang}=1";
        $outerQuery = $query;

        $useCollection = $this->collection_id > 0;
        $useDefaultCategory = $this->default_category_id > 0;
        $useBrand = $this->brand_id > 0;
        if ($useCollection > 0) {
            $query .= " AND collection_id=$this->collection_id ORDER BY default_category_id,position";
            if($useBrand){
                $outerQuery .= " AND brand_id=$this->brand_id";
            }
            if($useDefaultCategory){
                $outerQuery .= " AND default_category_id=$this->default_category_id";
            }
            $outerQuery .= " ORDER BY default_category_id,position LIMIT 0,$max";
            $this->useCollection = true;
        } else {

            if ($useBrand) {
                $query .= " AND brand_id=$this->brand_id";

            }
            if ($useDefaultCategory) {
                $query .= " AND default_category_id=$this->default_category_id";
            }

            $query .= " ORDER BY default_category_id,position LIMIT 0,$max";
        }


        //\Utils::log($query,"getVariantsProducts QUERY");
        $count = DB::selectOne(str_replace('SELECT id', 'SELECT count(id) as cnt', $query))->cnt;
        if ($count > 64) {
            $query = str_replace("LIMIT 0,$max", '', $query);
            $query .= ' LIMIT 0,64';
        }
        try {
            $rows = DB::select($query);
        } catch (Exception $e) {
            $rows = [];
        }


        if (count($rows) == 0 AND $useCollection) {
            //\Utils::log($query,"getVariantsProducts OUTER QUERY");
            $rows = DB::select($outerQuery);
            $this->useCollection = false;
        }
        $ids = [];
        $parent_id = (int)$this->parent_id;
        if ($parent_id > 0) {
            $ids[] = $parent_id;
            $otherSizesQuery = "SELECT id FROM cache_products WHERE parent_id=$parent_id AND id<>$this->id AND published_{$lang}=1";

            $otherSizes = DB::select($otherSizesQuery);
            foreach ($otherSizes as $row) {
                $ids[] = $row->id;
            }
        }

        foreach ($rows as $row) {
            $ids[] = $row->id;
        }
        $ids = array_unique($ids);

        if (count($ids) > 0) {
            //$attributes = $this->loaded_attributes;
            $attributes = $this->getLoadedAttributes();
            //\Utils::log($attributes,"getVariantsProducts LOADED ATTRIBUTES");
            foreach ($ids as $id) {
                $query = "SELECT * FROM products_attributes WHERE product_id=$id";
                $contextual_attributes = DB::select($query);

                $variants = [];
                $passed_attributes = 0;
                //\Utils::log($contextual_attributes,"getVariantsProducts CONTEXTUAL ATTRIBUTES");
                foreach ($contextual_attributes as $ca) {
                    if (isset($attributes[$ca->attribute_id])) {
                        /*if($attributes[$ca->attribute_id] != $ca->attribute_val){
                            $variants[] = $ca->attribute_id;
                        }*/
                        if (!in_array($ca->attribute_val, $attributes[$ca->attribute_id])) {
                            $variants[] = $ca->attribute_id;
                        }
                        $passed_attributes++;
                    }
                }
                $passable = abs($passed_attributes - $count) >= 0;
                if ($useCollection OR $passable) {
                    $products[] = ['id' => $id, 'variants' => $variants];
                }
            }
        }
        //\Utils::log($products,"getVariantsProducts PRODUCTS");
        $results = [];
        foreach ($products as $p) {
            $id = $p['id'];
            //$obj = Product::getPublicObj($id);
            $obj = self::getFullProduct($id);
            $variants = $p['variants'];
            if ($obj) {
                //$obj->setFullData();
                $list = [];
                $attributes = $obj->getFrontAttributes();
                foreach ($attributes as $attr) {
                    if (in_array($attr->id, $variants)) {
                        $list[] = $attr->label . ': ' . $attr->value;
                    }
                }
                $obj->variant = '<strong>' . e($obj->name) . '</strong><br>' . implode('<br>', $list);
                $obj->total = $count;
                $results[] = $obj;
                //\Utils::log($attributes,"getVariantsProducts FRONT ATTRIBUTES");
            }
        }

        unset($query, $outerQuery, $useDefaultCategory, $useBrand, $useCollection, $ids, $count, $products);

        return $results;

    }


    public function getCollectionProducts($max = 12, $order_by = 'rand')
    {

        $products = [];
        $lang = \Core::getLang();

        $query = "SELECT id FROM cache_products WHERE id<>$this->id AND published_{$lang}=1";
        if ($this->collection_id > 0) {
            $query .= " AND collection_id=$this->collection_id";
        }
        $large_query = $query;
        if ($this->default_category_id > 0) {
            $query .= " AND default_category_id=$this->default_category_id";
        }
        $query .= " ORDER BY is_soldout asc, is_featured desc, rand()";
        $query .= " LIMIT $max";
        $rows = \DB::select($query);
        foreach ($rows as $row) {
            $products[] = $row->id;
        }
        $cnt = count($products);
        if ($cnt < $max) {
            $limit = $max - $cnt;
            $query = $large_query .= " LIMIT $limit";
            $rows = \DB::select($query);
            foreach ($rows as $row) {
                if (!in_array($row->id, $products))
                    $products[] = $row->id;
            }
        }
        return $products;
    }

    public function getSimilarProducts()
    {
        $max = 6;
        $products = [];
        $lang = \Core::getLang();
        $sorting = 'ORDER BY is_soldout asc, is_featured desc, position desc';
        $query = "SELECT id FROM cache_products WHERE id<>$this->id AND published_{$lang}=1";
        $large_query = $query;
        /*if($this->collection_id > 0){
            $query .= " AND collection_id=$this->collection_id";
        }*/
        if ($this->brand_id > 0) {
            $query .= " AND brand_id=$this->brand_id";
            //$large_query .= " AND brand_id=$this->brand_id";
        }
        if ($this->default_category_id > 0) {
            $query .= " AND default_category_id=$this->default_category_id";
            $large_query .= " AND default_category_id=$this->default_category_id";
        }
        $last_query = $query;
        if (isset($this->attribute_gender)) {
            $query .= " AND (cache_attributes_val LIKE '%|$this->attribute_gender|%' AND cache_attributes LIKE '%|214|%')";
            $large_query .= " AND (cache_attributes_val LIKE '%|$this->attribute_gender|%' AND cache_attributes LIKE '%|214|%')";
            /*$query .= " AND (cache_attributes_val REGEXP '\\\\|$this->attribute_gender\\\\|' AND cache_attributes REGEXP '\\\\|214\\\\|')";
            $large_query .= " AND (cache_attributes_val REGEXP '\\\\|$this->attribute_gender\\\\|' AND cache_attributes REGEXP '\\\\|214\\\\|')";*/
        }
        if (isset($this->attribute_style)) {
            $query .= " AND (cache_attributes_val LIKE '%|$this->attribute_style|%' AND cache_attributes LIKE '%|215|%')";
            $large_query .= " AND (cache_attributes_val LIKE '%|$this->attribute_style|%' AND cache_attributes LIKE '%|215|%')";
            /*$query .= " AND (cache_attributes_val REGEXP '\\\\|$this->attribute_style\\\\|' AND cache_attributes REGEXP '\\\\|215\\\\|')";
            $large_query .= " AND (cache_attributes_val REGEXP '\\\\|$this->attribute_style\\\\|' AND cache_attributes REGEXP '\\\\|215\\\\|')";*/
        }

        $inner_query = '';
        //$rows = $this->loaded_attributes;
        $rows = $this->getLoadedAttributes();
        foreach ($rows as $key => $attr) {
            if ($key != 214 AND $key != 215) {
                foreach ($attr as $id) {
                    $inner_query .= "OR (cache_attributes_val LIKE '%|$id|%' AND cache_attributes LIKE '%|$key|%') ";
                }

                //$inner_query .= "OR (cache_attributes_val REGEXP '\\\\|$attr\\\\|' AND cache_attributes REGEXP '\\\\|$key\\\\|') ";
            }
        }
        if ($inner_query != '') {
            $inner_query = $query . " AND (" . ltrim($inner_query, "OR ") . ") $sorting LIMIT $max";
        } else {
            $inner_query = $query . " $sorting LIMIT $max";
        }
        //\Utils::log($inner_query,"getSimilarProducts QUERY");

        $rows = \DB::select($inner_query);
        foreach ($rows as $row) {
            $products[] = $row->id;
        }
        $cnt = count($products);
        if ($cnt < $max) {
            $limit = $max - $cnt;
            $query = $query .= " $sorting LIMIT $limit";
            //\Utils::log($query,"getSimilarProducts ALTERNATIVE QUERY");
            $rows = \DB::select($query);
            foreach ($rows as $row) {
                if (!in_array($row->id, $products))
                    $products[] = $row->id;
            }
        }
        $cnt = count($products);
        if ($cnt < $max) {
            $limit = $max - $cnt;
            $query = $last_query .= " $sorting LIMIT $limit";
            $query = $large_query .= " $sorting LIMIT $limit";
            //\Utils::log($query,"getSimilarProducts LAST QUERY");
            $rows = \DB::select($query);
            foreach ($rows as $row) {
                if (!in_array($row->id, $products))
                    $products[] = $row->id;
            }
        }

        $this->_similar_products = $products;
        return $products;

    }

    public function getSuggestedProducts()
    {
        //audit($this->toArray(), __METHOD__);
        $attributes = $this->getFrontAttributes();
        $max = 12;
        $products = [];
        $lang = \Core::getLang();
        //$query = "SELECT id FROM products WHERE is_out_of_production=0 AND deleted_at is null AND id<>$this->id";
        $query = "SELECT id FROM cache_products WHERE id<>$this->id AND published_{$lang}=1";
        if (isset($this->_similar_products) AND count($this->_similar_products) > 0) {
            $query .= " AND id NOT IN(" . implode(",", $this->_similar_products) . ")";
        }
        $inner_query = $query;
        if ($this->default_category_id > 0) {
            $inner_query .= " AND default_category_id=$this->default_category_id";
        }
        if ($this->brand_id > 0) {
            $query .= " AND brand_id=$this->brand_id";
        }

        if ($attributes and !empty($attributes)) {
            foreach ($attributes as $attribute) {
                if ($attribute->id == 214) {
                    $partial = " AND (cache_attributes_val LIKE '%|$attribute->option_id|%' AND cache_attributes LIKE '%|$attribute->id|%')";
                    $query .= $partial;
                }
                if ($attribute->id == 215) {
                    $partial = " AND (cache_attributes_val LIKE '%|$attribute->option_id|%' AND cache_attributes LIKE '%|$attribute->id|%')";
                    $query .= $partial;
                    $inner_query .= $partial;
                }
            }
        }

        $inner_query .= " ORDER BY is_soldout asc, is_featured desc, rand() LIMIT $max";
        //audit($inner_query, "getSuggestedProducts INNER QUERY");
        $rows = \DB::select($inner_query);
        foreach ($rows as $row) {
            $products[] = $row->id;
        }
        if (count($products) < $max) {
            $limit = $max - count($products);
            $query = $query .= " ORDER BY rand() LIMIT $limit";
            $rows = \DB::select($query);
            foreach ($rows as $row) {
                if (!in_array($row->id, $products))
                    $products[] = $row->id;
            }
        }
        return $products;
    }


    public function getResolvablePriceRules()
    {
        $rows = \PriceRule::where("active", 1)->get();
        //$rows = \PriceRule::whereIn("id",[18])->limit(1)->get();
        $rr = new RuleResolver();
        $rules = [];
        foreach ($rows as $row) {
            $rr->setRules($row->conditions);
            $assert = $rr->bindRules($this);
            if ($assert == 'true') {
                $rules[] = $row->id;
            }
        }
        return $rules;
    }


    public function expandIf()
    {
        if (!isset($this->hasIds) OR $this->hasIds == false) {
            $this->setAllIds();
        }
    }

    public function rebindAttributes()
    {
        $this->expandIf();
        $this->new_from_date = Format::datetime($this->new_from_date);
        $this->new_to_date = Format::datetime($this->new_to_date);
        $this->available_from_date = Format::date($this->available_from_date);
        $this->available_to_date = Format::date($this->available_to_date);
        $this->category_ids = implode(",", $this->getCategoriesIds());
        $this->carrier_ids = $this->getCarriersIds();
        $this->supplier_ids = $this->getSuppliersIds();
        $this->trends = $this->getTrendsIds();
        $this->related = $this->getRelationsIds('related');
        $this->accessories = $this->getRelationsIds('accessory');
        $this->childrens = $this->getChildrensIds();
        $this->related_inverse = $this->getInversRelations('related');
        $this->accessories_inverse = $this->getInversRelations('accessory');
        $this->setTags();
    }

    function getShippingCost()
    {
        $zone = \Core::getZone();
        //\Utils::log($zone,"ZONE");
        $shipping_cost = 0;
        if ($this->free_shipping == 1) {
            return $shipping_cost;
        }
        if ($zone) {
            $cache = self::cache('price-shipping-products');
            $weight = \Format::float($this->weight);
            $price = \Format::float($this->price);

            $cache_key = "zone-{$zone->id}-weight-{$weight}-price-{$price}";
            if($cache->has($cache_key)){
                return $cache->get($cache_key);
            }

            $query = "SELECT * FROM delivery
            WHERE zone_id=$zone->id
            AND range_weight_id IN (SELECT id FROM range_weight WHERE delimiter1<='$weight' AND delimiter2>'$weight')
            UNION
            SELECT * FROM delivery
            WHERE zone_id=$zone->id
            AND range_price_id IN (SELECT id FROM range_price WHERE delimiter1<='$price' AND delimiter2>'$price')";

            $rows = \DB::select($query);
            if (count($rows) > 0) {
                foreach ($rows as $row) {
                    $carrier = \Carrier::getPublicObj($row->carrier_id);
                    if ($carrier and $carrier->need_range == 1) {
                        $shipping_cost = $row->price;
                        $this->carrier_id = $carrier->id;
                        if ($carrier->is_free == 1) {
                            $shipping_cost = 0;
                        } else {
                            if ($carrier->tax_rules_group_id > 0) {
                                $taxGroup = \TaxGroup::getPublicObj($carrier->tax_rules_group_id);
                                $ratio = $taxGroup->getDefaultTaxRatio();
                                $shipping_cost = $shipping_cost * $ratio;
                            }
                        }
                    }
                }
            }
            $cache->put($cache_key, $shipping_cost, 60 * 24);
        }

        //audit(compact('weight', 'price', 'shipping_cost', 'rows', 'zone'), __METHOD__);

        return $shipping_cost;
    }


    function getShippingCostForCarrier($carrier_id, $zone_id)
    {

        $shipping_cost = 0;
        $weight = \Format::float($this->weight);
        $price = \Format::float($this->price);

        $query = "SELECT * FROM delivery
            WHERE zone_id=$zone_id AND carrier_id=$carrier_id
            AND range_weight_id IN (SELECT id FROM range_weight WHERE delimiter1<='$weight' AND delimiter2>'$weight')
            UNION
            SELECT * FROM delivery
            WHERE zone_id=$zone_id AND carrier_id=$carrier_id
            AND range_price_id IN (SELECT id FROM range_price WHERE delimiter1<='$price' AND delimiter2>'$price')";

        //\Utils::log($query, __METHOD__);

        $rows = \DB::select($query);
        if (count($rows) > 0) {
            foreach ($rows as $row) {
                $shipping_cost = $row->price;
                $carrier = \Carrier::getPublicObj($row->carrier_id);
                if ($carrier) {
                    $this->carrier_id = $carrier->id;
                    if ($carrier->is_free == 1) {
                        $shipping_cost = 0;
                    } else {
                        if ($carrier->tax_rules_group_id > 0) {
                            $taxGroup = \TaxGroup::getPublicObj($carrier->tax_rules_group_id);
                            $ratio = $taxGroup->getDefaultTaxRatio();
                            $shipping_cost = $shipping_cost * $ratio;
                        }
                    }
                }
            }

        }

        return $shipping_cost;
    }


    public function getTaxRate()
    {
        $taxGroup = \TaxGroup::getPublicObj($this->tax_group_id);
        $ratio = ($taxGroup) ? $taxGroup->getDefaultTaxRatio() : 0;
        return $ratio;
    }

    public function getTaxName()
    {
        $taxGroup = \TaxGroup::getPublicObj($this->tax_group_id);
        return ($taxGroup) ? $taxGroup->name : 'NO';
    }

    public function getCategoriesIds()
    {
        return DB::table("categories_products")->where("product_id", $this->id)->lists("category_id");
    }

    public function getCarriersIds()
    {
        return DB::table("products_carriers")->where("product_id", $this->id)->lists("carrier_id");
    }

    public function getSuppliersIds()
    {
        return [];
        //return DB::table("products_suppliers")->where("product_id", $this->id)->lists("supplier_id");
    }

    public function getRelationsIds($relation, $asArray = false)
    {
        $rows = DB::table("products_relations")->whereRelation($relation)->where("master_id", $this->id)->lists("slave_id");
        return ($asArray) ? $rows : implode(",", $rows);
    }

    public function getChildrensIds($asArray = false)
    {
        $rows = DB::table("products")->where('parent_id', $this->id)->lists("id");
        return ($asArray) ? $rows : implode(",", $rows);
    }

    public function getInversRelations($relation, $asArray = false)
    {
        $rows = DB::table("products_relations")->whereRelation($relation)->where("slave_id", $this->id)->lists("master_id");
        return ($asArray) ? $rows : implode(",", $rows);
    }


    public function getShortDescription($onlyAttributes = false)
    {
        $this->setFullData();
        $text = $this->sdesc;
        if ($text == '') {
            $data = [
                $this->main_category_name,
                $this->category_name,
                $this->brand_name,
                $this->collection_name,
            ];
        }
        if ($onlyAttributes) {
            $data[] = $this->getAttribute('attributes');
        }
        if (isset($data)) $text = implode(" ", $data);
        return $text;
    }

    public function getFullDescription($onlyAttributes = false)
    {
        $text = $this->ldesc;
        $this->setFullData();
        if ($onlyAttributes) {
            $text .= " " . $this->getAttribute('attributes');
        } else {
            $attributes = $this->getFrontAttributes();
            if (count($attributes) > 0) {
                $nodes = [];
                foreach ($attributes as $attribute) {
                    $nodes[] = "<strong>{$attribute->label}</strong>: {$attribute->value}";
                }
                $text .= "<ul><li>" . implode('</li><li>', $nodes) . "</li></ul>";
            }
        }

        return $text;
    }


    public function setTags()
    {
        return;
        $languages = \Mainframe::languagesCodes();

        $tags = array();

        $id = $this->id;

        foreach ($languages as $lang) {
            $tags = Tag::whereIn("id", function ($query) use ($id) {
                $query->select(['tag_id'])
                    ->from('products_tags')
                    ->where('product_id', $id);
            })->where("lang_id", $lang)->lists("tag");
            //\Utils::log($tags,"TAGS");
            $this->setAttribute("tags_" . $lang, implode(", ", $tags));
        }
    }

    function _after_clone(&$source, &$model)
    {
        $source_id = $source->id;
        $target_id = $model->id;

        //\Utils::watch();

        /* CLONING CATEGORIES */
        $rows = DB::table('categories_products')->where("product_id", $source_id)->get();
        DB::table('categories_products')->where('product_id', $target_id)->delete();
        foreach ($rows as $row) {
            DB::table('categories_products')->insert([
                'product_id' => $target_id,
                'category_id' => $row->category_id,
            ]);
        }


        /* CLONING ATTRIBUTES */
        $rows = DB::table('products_attributes')->where("product_id", $source_id)->get();
        DB::table('products_attributes')->where('product_id', $target_id)->delete();
        foreach ($rows as $row) {
            DB::table('products_attributes')->insert([
                'product_id' => $target_id,
                'attribute_id' => $row->attribute_id,
                'attribute_val' => $row->attribute_val,
                'lang_id' => $row->lang_id,
            ]);
        }


        /* CLONING ATTRIBUTES POSITION */
        $rows = DB::table('products_attributes_position')->where("product_id", $source_id)->get();
        DB::table('products_attributes_position')->where('product_id', $target_id)->delete();
        foreach ($rows as $row) {
            DB::table('products_attributes_position')->insert([
                'product_id' => $target_id,
                'attribute_id' => $row->attribute_id,
                'position' => $row->position
            ]);
        }


        /* CLONING IMAGES */
        /*$rows = DB::table('images')->where("product_id", $source_id)->orderBy('position')->get();
        DB::table('images')->where('product_id',$target_id)->delete();
        $savePath = public_path() . "/assets/products/";
        $images_positions = array();
        foreach ($rows as $row) {

            $image_file = $savePath . $row->filename;
            $target_file = $savePath . "copy-" . $row->filename;

            try {
                File::copy($image_file, $target_file);
                $insert_id = DB::table('images')->insertGetId([
                    'product_id' => $target_id,
                    'filename' => "copy-" . $row->filename,
                    'position' => $row->position,
                    'cover' => $row->cover,
                    'active' => $row->active,
                    'updated_by' => $row->updated_by,
                    'updated_at' => $row->updated_at,
                    'created_by' => $row->created_by,
                    'created_at' => $row->created_at,
                ]);

                $images_positions[$row->id] = $insert_id;

                $images_lang = DB::table('images_lang')->where("product_image_id", $row->id)->get();

                foreach ($images_lang as $il) {
                    DB::table('images_lang')->insert([
                        'product_image_id' => $insert_id,
                        'lang_id' => $il->lang_id,
                        'legend' => $il->legend,
                    ]);
                }
            } catch (Exception $ex) {

            }
        }*/


        /* CLONING COMBINATIONS */
        $rows = DB::table('products_combinations')->where("product_id", $source_id)->get();
        $combinations = array();
        foreach ($rows as $row) {
            $insert_id = DB::table('products_combinations')->insertGetId([
                'product_id' => $target_id,
                'name' => $row->name,
                'sku' => $row->sku,
                'supplier_sku' => $row->supplier_sku,
                'location' => $row->location,
                'ean13' => $row->ean13,
                'upc' => $row->upc,
                'buy_price' => $row->buy_price,
                'price' => $row->price,
                'ecotax' => $row->ecotax,
                'quantity' => $row->quantity,
                'weight' => $row->weight,
                'unit_price_impact' => $row->unit_price_impact,
                'isDefault' => $row->isDefault,
                'minimal_quantity' => $row->minimal_quantity,
                'available_date_from' => $row->available_date_from,
                'available_date_to' => $row->available_date_to,
            ]);

            $combinations[$row->id] = $insert_id;

            $attributes = DB::table('products_combinations_attributes')->where("combination_id", $row->id)->get();

            foreach ($attributes as $attr) {
                DB::table('products_combinations_attributes')->insert([
                    'combination_id' => $insert_id,
                    'attribute_id' => $attr->attribute_id,
                    'option_id' => $attr->option_id
                ]);
            }

            /*$images = DB::table('products_combinations_images')->where("combination_id", $row->id)->get();
            foreach ($images as $img) {
                DB::table('products_combinations_images')->insert([
                    'combination_id' => $insert_id,
                    'image_id' => $images_positions[$img->image_id]
                ]);
            }*/
        }


        /* CLONING SPECIFIC PRICES */
        $rows = DB::table('products_specific_prices')->where("product_id", $source_id)->get();
        foreach ($rows as $row) {
            DB::table('products_specific_prices')->insert([
                'product_id' => $target_id,
                'shop_id' => $row->shop_id,
                'price_rule_id' => $row->price_rule_id,
                'customer_group_id' => $row->customer_group_id,
                'currency_id' => $row->currency_id,
                'country_id' => $row->country_id,
                'customer_id' => $row->customer_id,
                'combination_id' => ($row->combination_id > 0) ? $combinations[$row->combination_id] : 0,
                'price' => $row->price,
                'description' => $row->description,
                'from_quantity' => $row->from_quantity,
                'reduction_type' => $row->reduction_type,
                'reduction_target' => $row->reduction_target,
                'reduction_currency' => $row->reduction_currency,
                'reduction_tax' => $row->reduction_tax,
                'free_shipping' => $row->free_shipping,
                'date_from' => $row->date_from,
                'date_to' => $row->date_to,
                'position' => $row->position,
                'stop_other_rules' => $row->stop_other_rules,
                'active' => $row->active,
            ]);
        }


        /* CLONING TAGS */
        $rows = DB::table('products_tags')->where("product_id", $source_id)->get();
        foreach ($rows as $row) {
            DB::table('products_tags')->insert([
                'product_id' => $target_id,
                'tag_id' => $row->tag_id,
            ]);
        }


        /* CLONING RELATIONS */
        $rows = DB::table('products_relations')->where("master_id", $source_id)->get();
        foreach ($rows as $row) {
            DB::table('products_relations')->insert([
                'master_id' => $target_id,
                'slave_id' => $row->slave_id,
                'relation' => $row->relation,
                'position' => $row->position,
            ]);
        }
    }

    function _before_delete()
    {
        if ($this->forceDeleting == false) {
            return;
        }
        $id = $this->id;
        //\Utils::watch();
        DB::table('products_relations')->where('master_id', $id)->delete();
        DB::table('products_tags')->where('product_id', $id)->delete();
        DB::table('products_attributes_position')->where('product_id', $id)->delete();
        DB::table('products_attributes')->where('product_id', $id)->delete();
        DB::table('categories_products')->where('product_id', $id)->delete();

        $rows = DB::table('images')->where("product_id", $id)->orderBy('position')->get();
        $savePath = public_path() . "/assets/products/";

        foreach ($rows as $row) {
            try {
                $file = $savePath . $row->filename;
                //TODO: UNIQUE FILE DELETING
                //\File::delete($file);
                $obj = \ProductImage::destroy($row->id);
            } catch (Exception $exc) {
                //echo $exc->getTraceAsString();
            }
        }

        $rows = DB::table('products_combinations')->where("product_id", $id)->get();
        foreach ($rows as $row) {
            DB::table('products_combinations_attributes')->where("combination_id", $row->id)->delete();
            DB::table('products_combinations_images')->where("combination_id", $row->id)->delete();
        }


        DB::table('products_combinations')->where('product_id', $id)->delete();
        DB::table('products_specific_prices')->where('product_id', $id)->delete();

        //\Utils::unwatch();
    }


    function updateQuantity($new_qty)
    {
        if ($this->hasCombinations()) {
            $new_qty = ProductCombination::where('product_id', $this->id)->sum('quantity');
        }
        \Product::where('id', $this->id)->update(['qty' => $new_qty]);
        $this->uncache();
    }


    function decrementQty($qty, $order_id = 0, $combination_id = 0)
    {
        //\Utils::log($qty,"CALLING decrementQty");
        try {
            $qty = abs($qty);
            $new_qty = $this->qty - $qty;
            $stock_name = $this->name;
            $sku = $this->sku;
            //\Utils::log($new_qty,"CALLING decrementQty NEW QTY");
            $this->updateQuantity($new_qty);
            if ($combination_id > 0) {
                $combination = ProductCombination::getObj($combination_id);
                if ($combination) {
                    $stock_name .= '<br>' . $combination->label;
                    if ($combination->sku != '') $sku = $combination->sku;
                    $combination->quantity = $combination->quantity - $qty;
                    $combination->save();
                }
            }
            if ($order_id > 0) {
                $params = [
                    'order_id' => $order_id,
                    'product_id' => $this->id,
                    'product_reference' => $sku,
                    'product_name' => $stock_name,
                    'qty' => $qty,
                ];
                StockMvt::register('order', $params);
            }

        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), "PRODUCT DECREMENT QTY ISSUE");
        }
    }


    function incrementQty($qty, $order_id = 0, $combination_id = 0)
    {
        //\Utils::log($qty,"CALLING incrementQty");
        try {
            $qty = abs($qty);
            $new_qty = $this->qty + $qty;
            $stock_name = $this->name;
            $sku = $this->sku;
            //\Utils::log($new_qty,"CALLING incrementQty NEW QTY");
            $this->updateQuantity($new_qty);
            if ($combination_id > 0) {
                $combination = ProductCombination::getObj($combination_id);
                if ($combination) {
                    $stock_name .= '<br>' . $combination->label;
                    if ($combination->sku != '') $sku = $combination->sku;
                    $combination->quantity = $combination->quantity + $qty;
                    $combination->save();
                }
            }
            if ($order_id > 0) {
                $params = [
                    'order_id' => $order_id,
                    'product_id' => $this->id,
                    'product_reference' => $sku,
                    'product_name' => $stock_name,
                    'qty' => $qty,
                ];
                StockMvt::register('supply', $params);
            }
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), "PRODUCT INCREMENT QTY ISSUE");
        }
    }


    function availabilityAlert($raw = false)
    {
        $class = 'label-success';
        $text = trans('template.available');
        $icon = 'fa-check';
        $cart_details = $this->cart_details;
        $status = 'in_stock';
        $default_days = \Cfg::get('DEFAULT_AVAILABILITY_DAYS', 16);
        //\Utils::log($cart_details,__METHOD__);
        $product_quantity = $this->qty;

        if (isset($cart_details->availability_mode) and $cart_details->availability_mode == 'unsolved') {
            $text = trans('template.unavailable_shipment');
            $icon = 'fa-stop';
            $class = 'label-danger';
            $status = 'out_of_stock';
            return $raw ? $status : "<span class='label $class label-availability'><i class='fa fa-fw $icon'></i> $text</span>";
        }

        if (isset($cart_details->override_availability) and $cart_details->override_availability == true) {
            return $raw ? $status : "<span class='label $class label-availability'><i class='fa fa-fw $icon'></i> $text</span>";
        }

        if (isset($this->combination) AND $this->combination != null) {
            $product_quantity = $this->combination->quantity;
        }

        if (isset($cart_details->availability_mode) and $cart_details->availability_mode == 'offline') {
            $this->is_available = 1;
            $product_quantity = 0;
        }

        if ($product_quantity < $cart_details->quantity) {
            if ($this->is_available == 1) {
                $text = trans('template.bookable_shipment2', ['days' => ($this->availability_days > 0) ? $this->availability_days : $default_days]);
                $icon = 'fa-calendar';
                $class = 'label-info';
                $status = 'preorder';
            } else {
                $text = trans('template.unavailable_shipment');
                $icon = 'fa-stop';
                $class = 'label-danger';
                $status = 'out_of_stock';
            }
        }

        if ($status == 'in_stock') {
            if ($this->hasStock24) {
                $text .= ' 24H';
            }
            if ($this->hasStock48) {
                $text .= ' 48H';
            }
        }

        if (\Core::membership()) {
            if ($this->hasAffiliate()) {
                $status = 'in_stock';
                $class = 'label-success';
                $icon = 'fa-check';
                $text = 'Venduto da ' . $this->affiliate->name;
            }
        }
        return $raw ? $status : "<span class='label $class label-availability'><i class='fa fa-fw $icon'></i> $text</span>";
    }


    function quantityLimit()
    {
        //quantity limit

        if (isset($this->combination) AND $this->combination != null) {
            if ($this->is_available == 1) {
                $qty_limit = ($this->max_sale_qty == 0) ? 99 : $this->max_sale_qty;
            } else {
                $qty_limit = $this->combination->quantity;
            }
        } else {
            if ($this->is_available == 1) {
                $qty_limit = ($this->max_sale_qty == 0) ? 99 : $this->max_sale_qty;
            } else {
                $qty_limit = $this->qty;
            }
        }
        if ($this->hasAffiliate()) {
            $stock = $this->getAffiliateStockById($this->affiliate->id);
            if ($stock) {
                $qty_limit = $stock->quantity;
            }
        }
        return $qty_limit <= 0 ? 0 : $qty_limit;
    }


    function renderAfterList($counter = 0)
    {
        $str = '';
        try {
            $responses = \Event::fire('frontend.product.afterList', [$this, $counter]);
            if (is_array($responses)) {
                $str .= implode(PHP_EOL, $responses);
            }
        } catch (Exception $e) {
            audit($e->getMessage(), __METHOD__);
        }
        echo trim($str);
    }


    function renderAfterProductPage()
    {
        $str = '';
        try {
            $responses = \Event::fire('frontend.product.afterProductPage', [$this]);
            if (is_array($responses)) {
                $str .= implode(PHP_EOL, $responses);
            }
        } catch (Exception $e) {
            audit($e->getMessage(), __METHOD__);
        }
        echo trim($str);
    }


    public function uncache()
    {
        parent::uncache();
        $this->rebuildDbCache();
        $this->purgeFullCache();
        $this->flushCache();
    }

    public function uncacheSimple()
    {
        parent::uncache();
        $this->purgeFullCache();
        $this->flushCache();
    }


    function rebuildDbCache($use_transaction = false)
    {
        $row = $this;
        if ($row->is_out_of_production == 1) {
            DB::table('cache_products')->where('id', $row->id)->delete();
            return true;
        }

        if ($use_transaction)
            DB::beginTransaction();

        try {
            $lang_rows = Product_Lang::where('product_id', $row->id)->get();
            $data = [
                'id' => $row->id,
                'sku' => $row->sku,
                'brand_id' => $row->brand_id,
                'collection_id' => $row->collection_id,
                'default_category_id' => $row->default_category_id,
                'main_category_id' => $row->main_category_id,
                'parent_id' => $row->parent_id,
                'parent_attribute_id' => $row->parent_attribute_id,
                'cache_gender' => $row->cache_gender,
                'cache_categories' => $row->cache_categories,
                'cache_attributes' => $row->cache_attributes,
                'cache_attributes_val' => $row->cache_attributes_val,
                'is_out_of_production' => $row->is_out_of_production,
                'is_outlet' => $row->is_outlet,
                'is_new' => $row->is_new,
                'is_featured' => $row->is_featured,
                'is_in_promotion' => $row->is_in_promotion,
                'qty' => $row->qty,
                'position' => $row->position,
                'price' => $row->price,
                'created_at' => $row->created_at,
                'visibility' => $row->visibility,
            ];

            if (config('ftp.soldOutEnabled')) {
                $data['is_soldout'] = $row->is_soldout;
            }

            foreach ($lang_rows as $lang) {
                $data['name_' . $lang->lang_id] = $lang->name;
                $data['sdesc_' . $lang->lang_id] = $lang->sdesc;
                $data['ldesc_' . $lang->lang_id] = $lang->ldesc;
                $data['indexable_' . $lang->lang_id] = $lang->indexable . ' ' . $row->ean13;
                $data['published_' . $lang->lang_id] = $lang->published;
            }

            if (DB::table('cache_products')->where('id', $row->id)->count('id') > 0) {
                DB::table('cache_products')->where('id', $row->id)->update($data);
            } else {
                DB::table('cache_products')->insert($data);
            }

            if ($use_transaction)
                DB::commit();

            return true;
        } catch (\Exception $e) {
            \Utils::log($e->getMessage());
            if ($use_transaction)
                DB::rollback();
        }
        return false;
    }


    function updateCache($data)
    {
        $row = $this;
        $record_id = DB::table('cache_products')->where('id', $row->id)->pluck('id');

        //update
        if ($record_id and $record_id > 0) {
            DB::table('cache_products')->where('id', $row->id)->update($data);
        } else {
            return $this->rebuildDbCache();
        }
    }

    /**
     *
     *
     * @param string $tag
     * @return \Illuminate\Cache\Repository
     */
    public static function cache($tag = 'products')
    {
        return \Registry::remember('cache_product_driver', static function () use ($tag){
            return Cache::driver(config('cache.product_driver', 'apc'))->tags([$tag]);
        });
    }

    /**
     * @param $id
     * @param string $lang_id
     * @return Product|null
     */
    static function getFullProduct($id, $lang_id = 'default')
    {
        $lang = \Core::getLang($lang_id);
        $key = "product-full-{$lang}-{$id}";

        if (self::cache()->has($key)) {

            $obj = self::cache()->get($key);
            if (isset($obj) AND isset($obj->id)) {
                //audit("READING $key",__METHOD__);
                $obj->setPrices();
                return $obj;
            }
        }
        /** @var Product $obj */
        $obj = self::getPublicObj($id, $lang_id);
        if ($obj) {
            $obj->setFullData();
            self::cache()->put($key, $obj, (int)config('cache.product_ttl_hours', 24) * 60);
            //audit("SAVING $key",__METHOD__);
            return $obj;
        }
        return null;
    }

    static function forgetFullCache($id)
    {
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            $key = "product-full-{$lang}-{$id}";
            self::cache()->forget($key);
        }
    }

    function purgeFullCache()
    {
        self::forgetFullCache($this->id);
    }

    private function optionCombinations($option_id, $combinations_options_cache, $options_cache)
    {
        $branch = isset($options_cache[$option_id]) ? $options_cache[$option_id] : null;
        $values = [];
        if ($branch) {
            foreach ($branch as $branch_pac) {
                $option_attribute_id = $branch_pac->attribute_id;
                $combination_branch = $combinations_options_cache[$branch_pac->combination_id];
                foreach ($combination_branch as $combination_pac) {
                    if ($combination_pac->attribute_id != $option_attribute_id) {
                        $values[] = $combination_pac->option_id;
                    }
                }
            }
            $values = array_unique($values);
            foreach ($values as $index => $value) {
                if ($value == $option_id) unset($values[$index]);
            }
        }
        return $values;
    }


    function setAlternatives($exclude = [])
    {
        if (isset($this->product_alternatives)) {
            return $this->product_alternatives;
        }
        $attribute = null;
        $alternatives = [];
        //this is a parent
        if ($this->parent_attribute_id > 0 AND $this->parent_id == 0) {
            $attribute = Attribute::getPublicObj($this->parent_attribute_id);
            //children
            $children = [];
            $children_ids = Product::where('parent_id', $this->id)->lists('id');
            foreach ($children_ids as $children_id) {
                $children[$children_id] = Product::getFullProduct($children_id);
            }
            if (count($children) > 0) {
                $alternatives[$this->id] = $this;
                $alternatives = array_merge($alternatives, $children);
                if ($attribute) {
                    $attribute->alternatives = $alternatives;
                }
            }
        }
        //this is a child
        if ($this->parent_id > 0 and !in_array($this->parent_id, $exclude)) {
            $parent = Product::getFullProduct($this->parent_id);
            if ($parent) {
                $exclude[] = $this->parent_id;
                $parent->setAlternatives($exclude);
                if ($parent->product_alternatives) {
                    $attribute = $parent->product_alternatives;
                }
            }
        }
        $this->product_alternatives = $attribute;
    }


    function hasCombinations()
    {
        $count = ProductCombination::where('product_id', $this->id)->remember(60 * 24, "product-has-combinations-{$this->id}")->count();
        return $count > 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|ProductCombination[]
     */
    function getCombinations()
    {
        return ProductCombination::where('product_id', $this->id)->get();
    }


    function loadAllRelations($is_product_page = false)
    {

        if ($is_product_page) {
            $this->setCombinationsAttributes(true);
            $this->setAlternatives([$this->id]);
        } else {
            $this->setCombinationsAttributes(false);
        }
        if(isset($this->combinations_attributes) && is_array($this->combinations_attributes) && !empty($this->combinations_attributes)){

        }else{
            $this->has_combinations = false;
        }
    }


    function setCombinationsAttributes($loadAll = true)
    {
        $data = [];
        if (isset($this->combinations_attributes) and is_array($this->combinations_attributes)) {
            return $this->combinations_attributes;
        }
        if (isset($this->has_combinations) and $this->has_combinations == false) {
            $this->combinations_attributes = $data;
            return;
        }
        //Utils::log(($loadAll) ? 'loadAll' : 'NOT loadAll', __METHOD__);
        //Utils::watch();

        $combinations_cache = [];
        $attributes_cache = [];
        $options_cache = [];
        $combinations_options_cache = [];
        $combinations_swatches = [];
        $saved_combinations = Session::get('saved_combinations', []);

        $combinations = ProductCombination::where('product_id', $this->id)->get();
        if (count($combinations) == 0) {
            $this->combinations_attributes = $data;
            return;
        }

        $currency = \Core::getCurrencyObj();
        $price_currency_id = $currency->id;

        if ($loadAll) {
            $theme = FrontTpl::getTheme();

            foreach ($combinations as $combination) {
                //audit($combination->toArray(), __METHOD__ . '::combination');
                $product_prices = $this->getProductPrices($combination->id, \Core::getCurrency(), \Core::getCountry());
                //audit($product_prices, __METHOD__ . '::product_prices');
                $availability_days = $this->availability_days;
                $availability = null;
                $price = $this->price_final_raw;
                $price_official = $this->price_official_raw;
                if ($combination->buy_price != 0 and $price_official != $combination->buy_price) {
                    $price = $combination->buy_price;
                } else {
                    $price += $combination->price;
                }

                if (!empty($product_prices)) {
                    foreach ($product_prices as $product_price) {
                        if ($product_price->combination_id == $combination->id and $product_price->active == 1 and $this->isProductPriceProcessable($product_price)) {

                            $payload = clone $this;
                            $payload->price = $price;
                            $payload->sell_price_wt = $price;
                            $payload->buy_price = $combination->buy_price;
                            $payload->combination = $combination;

                            $public_prices = \ProductHelper::calculateProductPrice($payload, $product_price);

                            $price = $public_prices['price_wt'];

                        }
                    }
                }

                $saving = $price_official - $price;
                $reduction = "(-" . Format::ratio($price_official, $saving) . ")";
                $sku = trim($combination->sku) != '' ? $combination->sku : $this->sku;
                $price = Format::currency($price, true, $price_currency_id);
                $saving = Format::currency($saving, true, $price_currency_id);

                $hasStock24 = 0;
                $hasStock48 = 0;

                $stock = $this->getSpecificStocks($combination->id);
                if ($stock) {
                    $stockInfo = $this->getStockDefinition($stock);
                    /*\Utils::log($stock,'stock');
                    \Utils::log($stockInfo,'stockInfo');*/
                    $hasStock24 = $stockInfo['hasStock24'];
                    $hasStock48 = $stockInfo['hasStock48'];
                    $availability_days = $stockInfo['availability_days'];
                    $qty = $stockInfo['qty'];
                    $combination->quantity = $qty;
                    $combination->stock = $stockInfo;
                }

                if ($combination->quantity < 1) {
                    if ($this->is_available == 1) {
                        //orderable
                        $availability = $theme->partial('product.info.bookable', ['content' => $this->availabilityStatus, 'days' => $availability_days]);
                    } else {
                        //not available
                        $availability = $theme->partial('product.info.unavailable', ['content' => $this->availabilityStatus]);
                    }
                } else {
                    //available
                    if ($hasStock24 == false and $hasStock48 == false) {
                        $availability = $theme->partial('product.info.available', ['content' => $this->availabilityStatus]);
                    } else {
                        if ($hasStock24) {
                            $availability = $theme->partial('product.info.available24', ['content' => $this->availabilityStatus]);
                        }
                        if ($hasStock48) {
                            $availability = $theme->partial('product.info.available48', ['content' => $this->availabilityStatus]);
                        }
                    }

                }
                $price_alt = $price;

                $combination->params = compact('availability', 'price', 'saving', 'reduction', 'sku', 'price_alt');

                $combinations_cache[$combination->id] = $combination;
            }
            $combinations_ids = array_keys($combinations_cache);
            $products_combinations_attributes = DB::table("products_combinations_attributes")->whereIn("combination_id", $combinations_ids)->get();
            foreach ($products_combinations_attributes as $pca) {
                if (isset($attributes_cache[$pca->attribute_id])) {
                    $attributes_cache[$pca->attribute_id][] = $pca->option_id;
                } else {
                    $attributes_cache[$pca->attribute_id] = [];
                    $attributes_cache[$pca->attribute_id][] = $pca->option_id;
                }

                if (isset($options_cache[$pca->option_id])) {
                    $options_cache[$pca->option_id][] = $pca;
                } else {
                    $options_cache[$pca->option_id] = [];
                    $options_cache[$pca->option_id][] = $pca;
                }

                if (isset($combinations_options_cache[$pca->combination_id])) {
                    $combinations_options_cache[$pca->combination_id][] = $pca;
                } else {
                    $combinations_options_cache[$pca->combination_id] = [];
                    $combinations_options_cache[$pca->combination_id][] = $pca;
                }
            }
            //Utils::log($options_cache,"OPTIONS CACHE");
            //Utils::log($combinations_options_cache,"COMBINATIONS CACHE");
            foreach ($combinations_options_cache as $combination_id => $coc) {
                $combination = $combinations_cache[$combination_id];
                $keys = [];
                foreach ($coc as $pca) {
                    $keys[] = $pca->option_id;
                }
                sort($keys);
                $key = implode('-', $keys);
                $combinations_swatches[$key] = $combinations_cache[$combination_id]->toArray();
            }
            //Utils::log($combinations_swatches,"COMBINATIONS SWATCHES");
            $attributes_ids = array_keys($attributes_cache);
            $attributes = Attribute::rows()->whereIn('id', $attributes_ids)->orderBy('position')->get();
            foreach ($attributes as $attribute) {
                $attribute_id = $attribute->id;
                $attribute_options_ids = $attributes_cache[$attribute_id];
                $attribute_options = AttributeOption::rows()->whereIn('id', $attribute_options_ids)->orderBy('position')->get();
                foreach ($attribute_options as $attribute_option) {
                    if (isset($saved_combinations[$attribute_id]) AND $attribute_option->id == $saved_combinations[$attribute_id]) {
                        $attribute_option->selected = true;
                        $attribute->selected_option = $attribute_option;
                    } else {
                        $attribute_option->selected = false;
                    }
                    $attribute_option->swatches = $this->optionCombinations($attribute_option->id, $combinations_options_cache, $options_cache);
                }
                $attribute->options = $attribute_options;
                $attribute->combination_swatches = $combinations_swatches;
                $data[] = $attribute;
            }
        } else {
            foreach ($combinations as $combination) {
                $combinations_cache[$combination->id] = $combination;
            }
            $combinations_ids = array_keys($combinations_cache);
            $products_combinations_attributes = DB::table("products_combinations_attributes")->whereIn("combination_id", $combinations_ids)->get();
            foreach ($products_combinations_attributes as $pca) {
                if (isset($attributes_cache[$pca->attribute_id])) {
                    $attributes_cache[$pca->attribute_id][] = $pca->option_id;
                } else {
                    $attributes_cache[$pca->attribute_id] = [];
                    $attributes_cache[$pca->attribute_id][] = $pca->option_id;
                }
            }

            $attributes_ids = array_keys($attributes_cache);
            $attributes = Attribute::rows()->whereIn('id', $attributes_ids)->orderBy('position')->get();
            foreach ($attributes as $attribute) {
                $attribute_id = $attribute->id;
                $attribute_options_ids = $attributes_cache[$attribute_id];
                $attribute_options = AttributeOption::rows()->whereIn('id', $attribute_options_ids)->orderBy('position')->get();
                $attribute->options = $attribute_options;
                $data[] = $attribute;
            }
        }


        $this->combinations_attributes = $data;
    }


    function getEstimatedDeliveryDate($status = null)
    {
        $availabilityStatus = is_null($status) ? $this->availabilityStatus : $status; //in_stock | out_of_stock | preorder
        if ($availabilityStatus == 'out_of_stock') {
            return null;
        }
        $dayInSecond = (24 * 60 * 60);

        $offsetDays = (int)Cfg::get('BUSINESS_DAYS', 2);
        $defaultAvailabilityDays = Cfg::get('DEFAULT_AVAILABILITY_DAYS', 16);
        $switchHour = Cfg::get('AVAILABILITY_SWITCH_HOUR', 13);
        $dayOfWeek = date('N');
        $today = date('Y-m-d H:i:s');
        $currentHour = date('H');
        $estimatedTime = strtotime($today . " + $offsetDays days");

        $productAvailabilityDays = $this->availability_days > 0 ? $this->availability_days : $defaultAvailabilityDays;
        if ($availabilityStatus == 'preorder') {
            $offsetDays = $productAvailabilityDays;
            /* Delta GG = (2 x (Giorni di disponibilità / 6)) + Giorni di disponibilità
            Data Consegna = Oggi + Delta GG + 1 */
            $offsetDays = floor((2 * ($offsetDays / 6)) + $offsetDays + 1);
            $estimatedTime = strtotime($today . " + $offsetDays days");
        }

        if ($availabilityStatus == 'in_stock') {
            $finalDay = ProductHelper::getDeliveryDate($today, $currentHour);
            $estimatedTime = strtotime($finalDay);
            //morellato 48h
            if ($this->hasStock48) {
                $estimatedTime += $dayInSecond;
            }
        }
        $data = [];
        $eTimeDayofWeek = date('N', $estimatedTime);
        //if delivery date is still Sat or Sun shift it
        if ($eTimeDayofWeek == 6) {
            $estimatedTime += $dayInSecond * 2;
        }
        if ($eTimeDayofWeek == 7) {
            $estimatedTime += $dayInSecond;
        }

        $data['day'] = (strftime("%A %d %B", $estimatedTime)); //Domenica 3 giugno
        if (Core::isWin())
            $data['day'] = utf8_encode($data['day']);
        $data['eta'] = null;
        $data['time'] = $estimatedTime;
        if ($availabilityStatus == 'in_stock' AND $dayOfWeek <= 5 AND $currentHour < $switchHour) {
            $now = time();
            $fixedHour = strtotime(date('Y-m-d ') . $switchHour . ':00:00');
            $data['eta'] = floor(($fixedHour - $now) / 3600 * 60);
        }
        /*$debug = compact('data','availabilityStatus','offsetDays','dayOfWeek','currentHour');
        \Utils::log($debug,__METHOD__);*/
        return $data;
    }


    public function getSpecificStocks($combination_id = null)
    {
        $key = __METHOD__ . "_product_{$this->id}";
        if ($combination_id) {
            $key .= "_comb_{$combination_id}";
        }

        if (Registry::has($key)) {
            return Registry::get($key);
        }

        $builder = DB::table('products_specific_stocks')->where('product_id', $this->id);
        if ($combination_id > 0) {
            $builder->where('product_combination_id', $combination_id);
        } else {
            $builder->whereNull('product_combination_id');
        }

        $builder->orderBy(DB::raw("FIELD(warehouse,'01','03','WM','W3','NG')"));
        $stock = $builder->first();

        $totalStocks = $this->getSpecificStocksTotal($combination_id);
        if ($totalStocks > 1) {
            //if the product has multi stock, and all quantities are 0, check if we can move to the next stock
            if ($stock) {
                if ($stock->quantity_24 == 0 and $stock->quantity_48 == 0 and $stock->quantity_shop == 0) {
                    $builder = DB::table('products_specific_stocks')->where('product_id', $this->id)->where('id', '<>', $stock->id);
                    if ($combination_id > 0)
                        $builder->where('product_combination_id', $combination_id);

                    $builder->orderBy(DB::raw("FIELD(warehouse,'01','03','WM','W3','NG')"));
                    $stock = $builder->first();
                }
            }
        }

        Registry::set($key, $stock);

        return $stock;
    }


    public function getSpecificStocksSet($combination_id = null)
    {
        $key = __METHOD__ . "_product_{$this->id}";
        if ($combination_id) {
            $key .= "_comb_{$combination_id}";
        }

        if (Registry::has($key)) {
            return Registry::get($key);
        }

        $builder = DB::table('products_specific_stocks')->where('product_id', $this->id);
        if ($combination_id > 0) {
            $builder->where('product_combination_id', $combination_id);
        } else {
            $builder->whereNull('product_combination_id');
        }

        $results = $builder->get();

        Registry::set($key, $results);

        return $results;
    }

    function getSpecificStocksCombinations()
    {
        $key = __METHOD__ . "_product_{$this->id}";

        if (Registry::has($key)) {
            return Registry::get($key);
        }

        $results = DB::table('products_specific_stocks')
            ->where('product_id', $this->id)
            ->whereNotNull('product_combination_id')
            ->get();

        Registry::set($key, $results);

        return $results;
    }

    function getSpecificStocksTotal($combination_id = null)
    {
        $key = __METHOD__ . "_product_{$this->id}";
        if ($combination_id) {
            $key .= "_comb_{$combination_id}";
        }

        if (Registry::has($key)) {
            return Registry::get($key);
        }

        $builder = DB::table('products_specific_stocks')->where('product_id', $this->id);
        if ($combination_id > 0) {
            $builder->where('product_combination_id', $combination_id);
        } else {
            $builder->whereNull('product_combination_id');
        }

        $results = $builder->count();

        Registry::set($key, $results);

        return $results;
    }


    function getStockDefinition($stock)
    {
        $hasStock48 = false;
        $hasStock24 = false;
        $hasStockShop = false;
        $qty = 0;
        $overall_qty = isset($stock) ? $stock->quantity_24 + $stock->quantity_48 + $stock->quantity_shop : 0;
        $shop_qty = isset($stock) ? $stock->quantity_shop : 0;
        $availability_days = Cfg::get('DEFAULT_AVAILABILITY_DAYS', 16);
        $sellable = true;
        $warehouse = $stock->warehouse;
        if ($stock->quantity_24 == 0) {
            if ($stock->quantity_48 > 0) {
                $hasStock48 = true;
                //$stockQty48 = $stock->quantity_48;
                $qty = $stock->quantity_48;
            } else { //both 24 and 48 stocks are 0
                //check if the shop quantity is positive
                if ($stock->quantity_shop > 0) {
                    $hasStockShop = true;
                    $qty = $stock->quantity_shop;
                    $warehouse = 'NG';
                } else {
                    $qty = 0;
                    if ($stock->available_at != null and $stock->available_at != '0000-00-00') {
                        //try to calculate the 'availability' date from the available date
                        $dStart = Carbon::now();
                        $dEnd = $stock->available_at;
                        $availability_days = ProductHelper::businessDays($dStart, $dEnd);

                        //standard increment for business days
                        $availability_days += 10;

                        $deliveryDate = $dStart->addWeekdays($availability_days);
                        $dayOfTheWeek = $deliveryDate->format('w');

                        if ($dayOfTheWeek == 0) { //sunday
                            $availability_days++;
                        }
                        if ($dayOfTheWeek == 6) { //saturday
                            $availability_days += 2;
                        }
                    } else {
                        $qty = 0;
                        $hasStock24 = false;
                        $hasStock48 = false;
                        $sellable = true;
                        $availability_days = 30;
                    }
                }
            }
        } else {
            $hasStock24 = true;
            $qty = $stock->quantity_24;
        }
        $data = compact('hasStock24', 'hasStock48', 'hasStockShop', 'qty', 'availability_days', 'sellable', 'warehouse', 'overall_qty', 'shop_qty');
        //\Utils::log($data,__METHOD__.' for product '.$this->id);
        return $data;
    }


    function isMorellato()
    {
        return $this->source == 'morellato' and $this->type == 'default';
    }


    function assignAvailability()
    {

    }


    function getOnlineAvailability()
    {
        if ($this->isMorellato() == false) {
            return $this->qty;
        }
        return (isset($this->onlineQty) and $this->onlineQty > 0) ? $this->onlineQty : 0;
    }


    function getStockWarehouse()
    {
        if (!$this->isMorellato()) {
            return 'LC';
        }

        if (isset($this->stockWarehouse))
            return $this->stockWarehouse;

        $combination_id = null;
        if (isset($this->combination))
            $combination_id = $this->combination->id;

        $stock = $this->getSpecificStocks($combination_id);
        if ($stock) {
            return $stock->warehouse;
        }

        return null;
    }

    function getCombinationId()
    {
        return isset($this->combination) ? $this->combination->id : null;
    }

    /**
     * @param ProductCombination|null $combination
     * @param bool $shallow
     * @return int
     */
    public function getShopAvailability($combination = null, $shallow = false)
    {
        if (isset($this->shopQty)) {
            return $this->shopQty;
        }
        $qty = 0;
        //do not perform real time checks
        if ($shallow === true) {
            $this->setStocks();
            return $this->offlineQty;
        } else {
            if (feats()->switchEnabled(Core\Cfg::SERVICE_SHOP_AVAILABILITY) and $this->isMorellato()) {
                /** @var Connection $conn */
                $conn = Core::getNegoziandoConnection();
                $sku = ($combination) ? \Utils::getSapSku($combination->sku, $combination->sap_sku) : \Utils::getSapSku($this->sku, $this->sap_sku);
                $qty = $conn->getTotalQuantityBySku($sku);
            }
        }
        $this->shopQty = $qty;
        return $qty;
    }

    /**
     * @param ProductCombination|null $combination
     * @return int
     */
    public function getRealTimeOverallQuantity($combination = null)
    {
        $online_quantity = $this->getOnlineAvailability();
        $shop_availability = $this->getShopAvailability($combination, false);
        return $online_quantity + $shop_availability;
    }

    /**
     * @param ProductCombination|null $combination
     * @return int
     */
    public function getOverallQuantity($combination = null)
    {
        $this->setStocks();
        if (isset($this->overall_qty) and $this->overall_qty > 0)
            return $this->overall_qty;

        $online_quantity = $this->getOnlineAvailability();
        $shop_availability = $this->getShopAvailability($combination, true);
        return $online_quantity + $shop_availability;
    }


    function hasSpin()
    {
        return strlen($this->spin) > 0;
    }

    function getSpinEmbed()
    {
        return "<div class=\"Sirv\" data-src=\"$this->spin\"></div><script src=\"https://scripts.sirv.com/sirv.js\"></script>";
    }

    function getSpinThumb()
    {
        return $this->spin . "?scale.width=58&scale.height=58&image=24&quality=60&scale.option=noup";
    }

    function isVirtual()
    {
        return $this->type === 'virtual';
    }

    function isService()
    {
        return $this->type === 'service';
    }

    function scopeWithCategories($query, $categories)
    {
        if (!is_array($categories)) {
            $categories = [$categories];
        }
        return $query->whereIn('id', function ($builder) use ($categories) {
            $builder->select('product_id')->from('categories_products')->whereIn('category_id', $categories);
        });
    }

    function scopeWithoutCategories($query, $categories)
    {
        if (!is_array($categories)) {
            $categories = [$categories];
        }
        return $query->whereNotIn('id', function ($builder) use ($categories) {
            $builder->select('product_id')->from('categories_products')->whereIn('category_id', $categories);
        });
    }

    function scopeWithBrands($query, $brands)
    {
        if (!is_array($brands)) {
            $brands = [$brands];
        }
        return $query->whereIn('brand_id', function ($builder) use ($brands) {
            $builder->select('id')->from('brands')->whereNull('deleted_at')->whereIn('id', $brands);
        });
    }

    function scopeWithoutBrands($query, $brands)
    {
        if (!is_array($brands)) {
            $brands = [$brands];
        }
        return $query->whereNotIn('brand_id', function ($builder) use ($brands) {
            $builder->select('id')->from('brands')->whereNull('deleted_at')->whereIn('id', $brands);
        });
    }

    function scopeWithCollections($query, $collections)
    {
        if (!is_array($collections)) {
            $collections = [$collections];
        }
        return $query->whereIn('collection_id', function ($builder) use ($collections) {
            $builder->select('id')->from('collections')->whereNull('deleted_at')->whereIn('id', $collections);
        });
    }

    function scopeWithoutCollections($query, $collections)
    {
        if (!is_array($collections)) {
            $collections = [$collections];
        }
        return $query->whereNotIn('collection_id', function ($builder) use ($collections) {
            $builder->select('id')->from('collections')->whereNull('deleted_at')->whereIn('id', $collections);
        });
    }

    function scopeWithAttributes($query, $attributes)
    {
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
        return $query->whereIn('id', function ($builder) use ($attributes) {
            $builder->select('product_id')->from('products_attributes')->whereIn('attribute_id', $attributes);
        });
    }

    function scopeWithoutAttributes($query, $attributes)
    {
        if (!is_array($attributes)) {
            $attributes = [$attributes];
        }
        return $query->whereNotIn('id', function ($builder) use ($attributes) {
            $builder->select('product_id')->from('products_attributes')->whereIn('attribute_id', $attributes);
        });
    }

    function scopeWithAttributeOptions($query, $options)
    {
        if (!is_array($options)) {
            $options = [$options];
        }
        return $query->whereIn('id', function ($builder) use ($options) {
            $builder->select('product_id')->from('products_attributes')
                ->whereIn('attribute_val', $options)
                ->whereIn('attribute_id', function ($subquery) {
                    $subquery->select('id')->from('attributes')
                        ->whereNull('deleted_at')
                        ->whereIn('frontend_input', ['select', 'multiselect']);
                });
        });
    }

    function scopeWithoutAttributeOptions($query, $options)
    {
        if (!is_array($options)) {
            $options = [$options];
        }
        return $query->whereNotIn('id', function ($builder) use ($options) {
            $builder->select('product_id')->from('products_attributes')
                ->whereIn('attribute_val', $options)
                ->whereIn('attribute_id', function ($subquery) {
                    $subquery->select('id')->from('attributes')
                        ->whereNull('deleted_at')
                        ->whereIn('frontend_input', ['select', 'multiselect']);
                });
        });
    }

    function scopeInStocks($query, $is_out_of_production = 0)
    {
        return $query->where('is_out_of_production', $is_out_of_production);
    }

    function scopeOnlyAvailableNow($query)
    {
        return $query->where('qty', '>', 0);
    }

    function scopeOutlet($query, $is_outlet = 1)
    {
        return $query->where('is_outlet', $is_outlet);
    }

    function scopeSoldout($query, $is_soldout = 1)
    {
        return $query->where('is_soldout', $is_soldout);
    }

    function scopeWithSource($query, $source = 'local')
    {
        return $query->where('source', $source);
    }

    function scopeWithPriceRule($query, $rules)
    {
        if (!is_array($rules)) {
            $rules = [$rules];
        }
        return $query->whereIn('id', function ($builder) use ($rules) {
            $builder->select('product_id')->from('products_specific_prices')->where('active', 1)->whereIn('price_rule_id', $rules);
        });
    }

    function addAttribute($attribute_id, $attribute_option_ids = [])
    {
        $product_id = $this->id;
        $current_options_ids = DB::table('products_attributes')
            ->where('product_id', $product_id)
            ->where('attribute_id', $attribute_id)
            ->whereIn('attribute_val', $attribute_option_ids)->lists('attribute_val');

        $options_to_insert = array_diff($attribute_option_ids, $current_options_ids);

        if (!empty($options_to_insert)) {
            foreach ($options_to_insert as $attribute_val) {
                DB::table('products_attributes')->insert(compact('attribute_val', 'product_id', 'attribute_id'));
            }
        }

        $exists = DB::table('products_attributes_position')
            ->where('product_id', $product_id)
            ->where('attribute_id', $attribute_id)->first();

        if (is_null($exists)) {
            $position = DB::table('products_attributes_position')
                    ->where('product_id', $product_id)->max('position') + 1;
            DB::table('products_attributes_position')->insert(compact('position', 'product_id', 'attribute_id'));
        }
    }

    function removeAttribute($attribute_id, $attribute_option_ids = [])
    {
        $product_id = $this->id;
        DB::table('products_attributes')
            ->where('product_id', $product_id)
            ->where('attribute_id', $attribute_id)
            ->whereIn('attribute_val', $attribute_option_ids)->delete();

        $remaining = DB::table('products_attributes')
            ->where('product_id', $product_id)
            ->where('attribute_id', $attribute_id)->count();

        if ($remaining == 0) {
            DB::table('products_attributes_position')
                ->where('product_id', $product_id)
                ->where('attribute_id', $attribute_id)->delete();
        }
    }

    function getDefaultCategory()
    {
        return Category::getObj($this->default_category_id);
    }

    function getMainCategory()
    {
        return Category::getObj($this->main_category_id);
    }

    function getBuilderImageAttribute()
    {
        return "/assets/products/builder/$this->sku.png";
    }

    function getPermalinkAttribute()
    {
        $link_absolute = \Link::create('product', $this->id)->setScheme('absolute')->getLink();
        return $link_absolute ? $link_absolute : null;
    }

    /**
     * @param bool $from_cache
     * @return array
     */
    function getCategories($from_cache = true)
    {
        $categories = [];
        $ids = \Utils::delimiterToArray($this->cache_categories);
        if (empty($ids))
            $from_cache = false; //if cache is empty read from database

        if ($from_cache) {

        } else {
            $ids = DB::table('categories_products')->where('product_id', $this->id)->lists('category_id');
        }

        if ($this->default_category_id > 0)
            $ids[] = $this->default_category_id;

        if ($this->main_category_id > 0)
            $ids[] = $this->main_category_id;

        $ids = array_unique($ids);

        foreach ($ids as $id) {
            $category = Category::getObj($id);
            if ($category)
                $categories[] = $category;
        }

        return $categories;
    }

    /**
     * @param $code
     * @return AttributeOption|null
     */
    function getProductAttributeByCode($code)
    {
        $obj = null;
        $attributes = $this->getFrontAttributes();
        foreach ($attributes as $attribute_code => $attribute) {
            if ($code == $attribute_code) {
                $obj = AttributeOption::getObj($attribute->option_id);
                $obj->schema = $attribute;
                $absolute_link = new Link();
                $obj->link = $absolute_link->create($attribute_code, $obj->id)->getLink();
            }
        }
        return $obj;
    }

    /**
     * @return null|object
     */
    public function getNextImage()
    {
        $lang_id = \Core::getLang();
        $row = \ProductImage::rows($lang_id)->where('product_id', $this->id)->where('active', 1)->where('cover', 0)->orderBy('position')->take(1)->first();
        $slug = $this->slug;
        $obj = null;
        if ($row) {
            $obj = (object)$row->toArray();
            $row->id = $row->getImgId();
            $name = '2-' . $slug;
            $manipulations = ProductHelper::prepareImageResources($row->id, $name);
            foreach ($manipulations as $manipulation => $resource_path) {
                $obj->$manipulation = $resource_path;
            }
        }
        return $obj;
    }

    /**
     * @return Attribute[]|null
     */
    public function getProductAttributes()
    {
        $registry_key = __METHOD__ . $this->id;
        if (\Registry::has($registry_key))
            return \Registry::get($registry_key);

        $ids = DB::table('products_attributes_position')
            ->where('product_id', $this->id)
            ->select(DB::raw('distinct(attribute_id)'))
            ->orderBy('position')
            ->lists('attribute_id');

        $attributes = [];
        foreach ($ids as $id) {
            $attribute = Attribute::getObj($id);
            if (!is_null($attribute))
                $attributes[] = $attribute;
        }

        unset($ids);

        $product_attributes = DB::table('products_attributes')
            ->where('product_id', $this->id)
            ->select('attribute_id', 'attribute_val')
            ->get();

        $attributes_options = [];
        foreach ($product_attributes as $product_attribute) {
            if (!isset($attributes_options[$product_attribute->attribute_id])) {
                $attributes_options[$product_attribute->attribute_id] = [];
            }
            $attributes_options[$product_attribute->attribute_id][] = $product_attribute->attribute_val;
        }

        foreach ($attributes as $attribute) {
            $options = [];
            if (in_array($attribute->frontend_input, ['select', 'multiselect']) and isset($attributes_options[$attribute->id])) {
                $ids = $attributes_options[$attribute->id];
                foreach ($ids as $id) {
                    $option = AttributeOption::getObj($id);
                    if (!is_null($option))
                        $options[] = $option;
                }
                unset($ids);
            }
            $attribute->options = $options;
        }

        unset($product_attributes, $attributes_options);

        \Registry::set($registry_key, $attributes);

        return $attributes;
    }

    /**
     * @param $code
     * @return array|AttributeOption[]
     */
    public function getProductAttributesOptionsByCode($code)
    {
        if (!is_array($code))
            $code = [$code];

        $attributes = $this->getProductAttributes();
        foreach ($attributes as $attribute) {
            if (in_array($attribute->code, $code)) {
                return $attribute->options;
            }
        }
        return [];
    }


    /**
     * @return bool
     */
    public function isSubmittableToSap()
    {

        //is SAP receipts are enabled, than every type of product is ok; otherwise only product of type 'morellato'
        if (!feats()->sapReceipts()) {
            if ($this->source != 'morellato') {
                return false;
            }
        }

        //if the product is Service, than is not submittable
        if ($this->type == self::TYPE_SERVICE) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     * @param string $lang_id
     * @param bool $force
     * @return self
     */
    public static function getProductFeed($id, $lang_id = 'default', $force = false)
    {
        $lang = \Core::getLang($lang_id);
        $key = "product-full-{$lang}-{$id}";
        $cache = FileCache::getInstance();

        if($force === true) {
            $cache->forget($key);
        }

        $data = $cache->fetch($key);
        if ($data !== null) {
            //hydrate data
            $instance = new static;
            $instance->setAttributes($data);
            return $instance;
        }

        //store data
        $instance = self::getFullProduct($id, $lang);
        if ($instance) {
            if(!isset($instance->related_entities))
                Event::fire('product.full.expand', [&$instance]);
            $instance->setAttributes([
                'product_images' => $instance->getImages(),
                'product_videos' => $instance->getVideos(),
            ]);
            $instance->getNextImage();
            $payload = $instance->toArray();
            $cache->store($key, $payload, (int)config('cache.product_feed_ttl_hours', 24) * 60);
            unset($payload, $lang);
        }

        return $instance;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new ProductPresenter($this);
    }

}

class ProductPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}
