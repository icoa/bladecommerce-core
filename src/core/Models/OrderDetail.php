<?php


use Illuminate\Database\Eloquent\Builder;
use services\Fees\Traits\TraitFeesOrderDetail;
use services\Traits\HasParam;
use services\Traits\TraitProductRow;

/**
 * OrderDetail
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $warehouse_id
 * @property integer $shop_id
 * @property integer $product_id
 * @property integer $product_combination_id
 * @property integer $affiliate_id
 * @property string $combinations
 * @property string $product_name
 * @property integer $product_quantity
 * @property integer $product_quantity_in_stock
 * @property integer $product_quantity_refunded
 * @property integer $product_quantity_return
 * @property integer $product_quantity_reinjected
 * @property float $product_buy_price
 * @property float $product_sell_price
 * @property float $product_sell_price_wt
 * @property float $product_unit_price
 * @property float $product_price
 * @property float $product_item_price
 * @property float $product_item_price_tax_excl
 * @property float $reduction_percent
 * @property float $reduction_amount
 * @property float $reduction_amount_tax_incl
 * @property float $reduction_amount_tax_excl
 * @property float $product_quantity_discount
 * @property string $product_ean13
 * @property string $product_upc
 * @property string $product_reference
 * @property string $product_supplier_reference
 * @property string $product_sap_reference
 * @property float $product_weight
 * @property boolean $tax_computation_method
 * @property string $tax_name
 * @property float $tax_rate
 * @property boolean $special
 * @property integer $cart_rule_id
 * @property float $ecotax
 * @property float $ecotax_tax_rate
 * @property boolean $discount_quantity_applied
 * @property string $download_hash
 * @property integer $download_nb
 * @property string $download_deadline
 * @property float $total_price_tax_incl
 * @property float $total_price_tax_excl
 * @property float $cart_price_tax_incl
 * @property float $cart_price_tax_excl
 * @property float $total_shipping_price_tax_incl
 * @property float $total_shipping_price_tax_excl
 * @property float $price_tax_incl
 * @property float $price_tax_excl
 * @property string $bda
 * @property string $invoice
 * @property string $shipping_number
 * @property string $status
 * @property string $availability_mode
 * @property string $availability_shop_id
 * @property string $availability_solvable_shops
 * @property string $warehouse
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read \Order $order
 * @property-read mixed $label
 * @property-read mixed $quantity_warning
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @property-read mixed $fees_row_type
 * @property-read mixed $fees_reason
 * @property-read mixed $fees_payment_source
 * @property-read mixed $fees_sku
 * @property-read mixed $fees_product_quantity
 * @property-read mixed $fees_tax_rate
 * @property-read mixed $fees_vat_code
 * @property-read mixed $fees_amount_gross
 * @property-read mixed $fees_amount_net
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereWarehouseId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereCombinations($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductName($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductQuantityInStock($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductQuantityRefunded($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductQuantityReturn($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductQuantityReinjected($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductBuyPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductSellPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductSellPriceWt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductUnitPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductItemPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductItemPriceTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereReductionPercent($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereReductionAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereReductionAmountTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereReductionAmountTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductQuantityDiscount($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductEan13($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductUpc($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductReference($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductSupplierReference($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductSapReference($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereProductWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTaxComputationMethod($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTaxName($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTaxRate($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereSpecial($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereCartRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereEcotax($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereEcotaxTaxRate($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereDiscountQuantityApplied($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereDownloadHash($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereDownloadNb($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereDownloadDeadline($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTotalPriceTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTotalPriceTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereCartPriceTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereCartPriceTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTotalShippingPriceTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereTotalShippingPriceTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail wherePriceTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail wherePriceTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereBda($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereInvoice($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereShippingNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereAvailabilityMode($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereAvailabilityShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereAvailabilitySolvableShops($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereWarehouse($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\OrderDetail whereDeletedBy($value)
 * @method static \OrderDetail withOrder($order_id)
 * @method static \OrderDetail withSku($sku)
 * @method static \OrderDetail withOrders($orders)
 * @method static \OrderDetail withAvailabilityModes($modes)
 * @method static \OrderDetail withoutAvailabilityModes($modes)
 */
class OrderDetail extends SingleModel
{

    const MODE_OFFLINE = 'offline';
    const MODE_LOCAL = 'local';
    const MODE_SHOP = 'shop';
    const MODE_ONLINE = 'online';

    const STATUS_DEFAULT = 'default';
    const STATUS_SHOP_NOT_FOUND = 'SHOP_NOTFOUND';
    const STATUS_NOT_FOUND = 'NOTFOUND';
    const STATUS_SHOP_UNPICKED = 'SHOP_UNPICKED'; //(non ritirato)
    const STATUS_UNPICKED = 'UNPICKED';
    const STATUS_SHOP_SHIPPED = 'SHOP_SHIPPED';
    const STATUS_SHIPPED = 'SHIPPED';
    const STATUS_CANCELLED = 'CANCELLED';
    const STATUS_SHOP_MOVED = 'SHOP_MOVED';
    const STATUS_SHOP_SOLD = 'SHOP_SOLD';

    use HasParam;
    use TraitFeesOrderDetail;
    use TraitProductRow;

    /* LARAVEL PROPERTIES */

    protected $table = 'order_details';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public $db_fields = array();

    // RELATIONS

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('Order', 'order_id');
    }

    /**
     * @return Order|null
     **/
    public function getOrder()
    {
        return $this->order;
    }

    public function ruleName()
    {
        if ($this->cart_rule_id == 0) return "";
        $obj = CartRule::getObj($this->cart_rule_id);
        return ($obj) ? $obj->name : "";
    }

    function getCombinationsAttribute($value)
    {
        if ($value != null) {
            $value = unserialize($value);
        }
        return $value;
    }

    function getLabelAttribute($value)
    {
        $combinations = $this->getAttribute('combinations');
        if ($combinations) {
            return $combinations->label;
        }
        return null;
    }

    function getProductNameAttribute($value)
    {
        $label = $this->getAttribute('label');
        return $label ? "{$value}<br>$label" : $value;
    }

    function getQuantityWarningAttribute($value)
    {
        $class = 'label-success';
        $qty = $this->product->qty;
        if ($this->product_combination_id > 0) {
            $combination = ProductCombination::getObj($this->product_combination_id);
            if ($combination) {
                $qty = $combination->quantity;
            }
        }
        if ($qty <= $this->product_quantity) {
            $class = 'label-warning';
        }
        if ($qty <= 0) {
            $class = 'label-important';
        }

        return "<span class=\"label $class\">$qty</span>";
    }

    function updateTotals()
    {
        /**
         * total_price_tax_incl = cart_price_tax_incl * product_quantity
         * total_price_tax_excl = cart_price_tax_excl * product_quantity
         */
        $this->fill([
            'total_price_tax_incl' => $this->cart_price_tax_incl * $this->product_quantity,
            'total_price_tax_excl' => $this->cart_price_tax_excl * $this->product_quantity,
        ]);
        $this->save(['timestamps' => false]);
    }

    /**
     * @return bool
     */
    function isModeOnline()
    {
        return $this->availability_mode == self::MODE_ONLINE;
    }

    /**
     * @return bool
     */
    function isModeOffline()
    {
        return $this->availability_mode == self::MODE_OFFLINE;
    }

    /**
     * @return bool
     */
    function isModeShop()
    {
        return $this->availability_mode == self::MODE_SHOP;
    }

    /**
     * @return bool
     */
    function isModeLocal()
    {
        return $this->availability_mode == self::MODE_LOCAL;
    }

    /**
     * @return \Product|null
     */
    function getProduct()
    {
        return \Product::getObj($this->product_id);
    }

    /**
     * @param int $amount
     * @param int $amountUntaxed
     * @return array
     */
    static function getGeneralAttributes($amount = 0, $amountUntaxed = 0)
    {
        return [
            'order_id' => 0,
            'warehouse_id' => 0,
            'shop_id' => 1,
            'product_combination_id' => null,
            'product_id' => 0,
            'product_name' => null,
            'product_quantity' => -1,
            'product_quantity_in_stock' => 0,
            'product_quantity_refunded' => 0,
            'product_quantity_return' => 0,
            'product_quantity_reinjected' => 0,
            'product_buy_price' => $amount,
            'product_sell_price' => $amount,
            'product_sell_price_wt' => $amount,
            'product_unit_price' => $amount,
            'product_price' => $amount,
            'product_item_price' => $amount,
            'product_item_price_tax_incl' => $amount,
            'product_item_price_tax_excl' => $amountUntaxed,
            'total_price_tax_incl' => $amount,
            'total_price_tax_excl' => $amountUntaxed,
            'cart_price_tax_incl' => $amount,
            'cart_price_tax_excl' => $amountUntaxed,
            'total_shipping_price_tax_incl' => 0,
            'total_shipping_price_tax_excl' => 0,
            'price_tax_incl' => 0,
            'price_tax_excl' => 0,
            'reduction_percent' => 0,
            'reduction_amount' => 0,
            'reduction_amount_tax_incl' => 0,
            'reduction_amount_tax_excl' => 0,
            'product_quantity_discount' => 0,
            'product_ean13' => null,
            'product_upc' => null,
            'product_reference' => null,
            'product_sap_reference' => null,
            'product_supplier_reference' => null,
            'tax_name' => null,
            'tax_rate' => \Core::getDefaultTaxRate(),
            'special' => 0,
            'cart_rule_id' => 0,
            'ecotax' => 0,
        ];
    }

    /**
     * @param $status
     */
    function setStatus($status)
    {
        $this->status = $status;
        $this->save();
    }

    /**
     * @param Builder $builder
     * @param $order_id
     */
    public function scopeWithOrder(Builder $builder, $order_id)
    {
        $builder->where('order_id', $order_id);
    }

    /**
     * @param Builder $builder
     * @param array $orders
     */
    public function scopeWithOrders(Builder $builder, array $orders)
    {
        $builder->whereIn('order_id', $orders);
    }

    /**
     * @param Builder $builder
     * @param $modes
     */
    public function scopeWithAvailabilityModes(Builder $builder, $modes)
    {
        if (!is_array($modes))
            $modes = [$modes];

        $builder->whereIn('availability_mode', $modes);
    }

    /**
     * @param Builder $builder
     * @param $modes
     */
    public function scopeWithoutAvailabilityModes(Builder $builder, $modes)
    {
        if (!is_array($modes))
            $modes = [$modes];

        $builder->whereNotIn('availability_mode', $modes);
    }

    /**
     * @param Builder $builder
     * @param $sku
     */
    public function scopeWithSku(Builder $builder, $sku)
    {
        $builder->where(function ($query) use ($sku) {
            $query->where('product_reference', $sku)->orWhere('product_sap_reference', $sku);
        });
    }

    /**
     * @return string
     */
    function getSku()
    {
        $combination = $this->combination;
        $sku = ($combination) ? $combination->sku : $this->product_reference;
        return $sku;
    }

    /**
     * @return string
     */
    function getSapSku()
    {
        $combination = $this->combination;
        $sku = ($combination) ? \Utils::getSapSku($combination->sku, $combination->sap_sku) : \Utils::getSapSku($this->product_reference, $this->product_sap_reference);
        return $sku;
    }

    /**
     * @param $state
     * @return bool
     */
    function hasOwnState($state)
    {
        if (!is_array($state))
            $state = [$state];

        return in_array($this->status, $state);
    }

    /**
     * @param array $options
     * @return bool
     */
    function save(array $options = array())
    {
        unset($this->product);
        return parent::save($options);
    }
}