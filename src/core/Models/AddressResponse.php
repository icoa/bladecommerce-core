<?php


/**
 * AddressResponse
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $address_id
 * @property string $result
 * @property string $address_list
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereResult($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereAddressList($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\AddressResponse whereUpdatedAt($value)
 */
class AddressResponse extends SingleModel {
    /* LARAVEL PROPERTIES */

    protected $table = 'mi_address_responses';
    protected $guarded = array('id');
    protected $isBlameable = false;
    public $timestamps = false;
    public $softDelete = false;
    public $db_fields = array(
        "order_id",
        "address_id",
        "result",
        "address_list",
    );


    function getAddressListAttribute($value){
        try{
            return unserialize($value);
        }catch (Exception $e){
            return [];
        }
    }
}