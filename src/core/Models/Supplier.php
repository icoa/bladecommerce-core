<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Supplier
 *
 * @property integer $id
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property-read \Address $address
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier wherePosition($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class Supplier extends MultiLangModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'suppliers';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "ldesc", "metatitle", "metakeywords", "metadescription", "image_default");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("position", "robots", "ogp", "ogp_type", "ogp_image", "public_access");
    protected $lang_model = 'Supplier_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    // RELATIONS
    public function address() {
        return $this->hasOne('Address','supplier_id');
    }

    function _after_create() {
        
        if(\Input::get('country_id') > 0):
            
      
        $lang = \Lang::getLocale();
        \Input::merge(['company' => \Input::get('name_'.$lang), 'alias' => 'supplier', 'firstname' => 'supplier', 'lastname' => 'supplier']);
        
        $post_data = \Input::only(array(
                    "country_id",
                    "state_id",
                    "company",
                    "alias",
                    "firstname",
                    "lastname",
                    "address1",
                    "address2",
                    "postcode",
                    "city",
                    "other",
                    "phone",
                    "phone_mobile",
                    "vat_number",
        ));
        $address = new Address($post_data);
        //$instance = new static;
        $this->address()->save($address);
        //HugeForm::find($id)->update($post_data);
        endif;
    }
    
    
    function _after_update() {
        parent::_after_update();
        if(\Input::get('country_id') > 0):
        $lang = \Lang::getLocale();
        \Input::merge(['company' => \Input::get('name_'.$lang), 'alias' => 'supplier', 'firstname' => 'supplier', 'lastname' => 'supplier']);
        
        $post_data = \Input::only(array(
                    "country_id",
                    "state_id",
                    "company",
                    "alias",
                    "firstname",
                    "lastname",
                    "address1",
                    "address2",
                    "postcode",
                    "city",
                    "other",
                    "phone",
                    "phone_mobile",
                    "vat_number",
        ));
        //$address = new Address($post_data);
        //$instance = new static;
        $this->address()->update($post_data);
        //HugeForm::find($id)->update($post_data);
        endif;
    }
    
    
    function _after_clone(&$source, &$model) {
        $data = $source->address->toArray();
        unset($data['id']);
        $address = new Address($data);   
        $model->address()->save($address);
    }
    
    function _before_delete() {
        parent::_before_delete();

        if ($this->forceDeleting == false) {
            return;
        }
        
        $this->address()->forceDelete();
    
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new SupplierPresenter($this);
    }

}

class SupplierPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}



/**
 * Supplier_Lang
 *
 * @property integer $supplier_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $ldesc
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $image_default
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereSupplierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Supplier_Lang whereImageDefault($value)
 * @property-read \Supplier $supplier
 */
class Supplier_Lang extends Eloquent  {


    protected $table = 'suppliers_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function supplier()
    {
        return $this->belongsTo('Supplier');
    }


}