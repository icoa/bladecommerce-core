<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-nov-2013 18.28.44
 */

/**
 * Product_Lang
 *
 * @property integer $product_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $metafacebook
 * @property string $metagoogle
 * @property string $metatwitter
 * @property string $canonical
 * @property string $head
 * @property string $footer
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @property string $gift_message
 * @property string $available_now
 * @property string $available_later
 * @property string $metadata
 * @property string $indexable
 * @property string $attributes
 * @property-read \Product $product
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereFooter($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereImageZoom($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereGiftMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereAvailableNow($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereAvailableLater($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereMetadata($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereIndexable($value)
 * @method static \Illuminate\Database\Query\Builder|\Product_Lang whereAttributes($value)
 */
class Product_Lang extends Eloquent  {


    protected $table = 'products_lang';
    protected $guarded = array();
    public $timestamps = false;
    
    public function product()
    {
        return $this->belongsTo('Product');
    }
    

}

