<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Address
 *
 * @property integer $id
 * @property integer $country_id
 * @property integer $state_id
 * @property integer $customer_id
 * @property integer $brand_id
 * @property integer $supplier_id
 * @property integer $warehouse_id
 * @property integer $people_id
 * @property boolean $billing
 * @property string $alias
 * @property string $company
 * @property string $lastname
 * @property string $firstname
 * @property string $address1
 * @property string $address2
 * @property string $postcode
 * @property string $city
 * @property string $other
 * @property string $phone
 * @property string $phone_mobile
 * @property string $fax
 * @property string $vat_number
 * @property string $cf
 * @property string $extrainfo
 * @property string $dni
 * @property boolean $published
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read \Supplier $supplier
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Address whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCountryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereStateId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereSupplierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereWarehouseId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address wherePeopleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereBilling($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereAlias($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereLastname($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereFirstname($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereAddress1($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereAddress2($value)
 * @method static \Illuminate\Database\Query\Builder|\Address wherePostcode($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereOther($value)
 * @method static \Illuminate\Database\Query\Builder|\Address wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Address wherePhoneMobile($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereFax($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereVatNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCf($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereExtrainfo($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereDni($value)
 * @method static \Illuminate\Database\Query\Builder|\Address wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Address whereDeletedBy($value)
 * @property-read mixed $state_name
 * @property-read mixed $state_code
 * @property-read mixed $country_name
 * @property-read mixed $country_code
 */
class Address extends SingleModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */

    protected $table = 'address';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];

    public $db_fields = array(
        "country_id",
        "state_id",
        "customer_id",
        "supplier_id",
        "warehouse_id",
        "alias",
        "company",
        "lastname",
        "firstname",
        "address1",
        "address2",
        "postcode",
        "city",
        "other",
        "phone",
        "phone_mobile",
        "vat_number",
        "extrainfo",
        "published",
    );

    const TYPE_PERSON = 1;
    const TYPE_COMPANY = 2;


    // RELATIONS

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function supplier()
    {
        return $this->belongsTo('Supplier', 'supplier_id');
    }

    /**
     * @param string $lang
     * @return \Country|null
     */
    public function country($lang = 'default')
    {
        return Country::getObj($this->country_id, $lang);
    }

    /**
     * @param string $lang
     * @return \State|null
     */
    public function state($lang = 'default')
    {
        return State::getObj($this->state_id, $lang);
    }

    /**
     * @return string
     */
    public function fullname()
    {
        if ($this->people_id == 1) {
            return $this->firstname . " " . $this->lastname;
        }
        return $this->company;
    }

    /**
     * @return string
     */
    public function countryName()
    {
        $state = $this->country();
        return ($state) ? (string)$state->name : "";
    }

    /**
     * @return string
     */
    public function stateName()
    {
        $state = $this->state();
        return ($state) ? (string)$state->name : "";
    }

    /**
     * @return string
     */
    public function countryCode()
    {
        $state = $this->country();
        return ($state) ? (string)$state->iso_code : "";
    }

    /**
     * @return string
     */
    public function stateCode()
    {
        $state = $this->state();
        return ($state) ? (string)$state->iso_code : "";
    }

    /**
     * @return \Customer|null
     */
    public function getCustomer()
    {
        return \Customer::find($this->customer_id);
    }

    function expand()
    {
        $this->countryName = $this->countryName();
        $this->stateName = $this->stateName();
        $this->countryCode = $this->countryCode();
        $this->stateCode = $this->stateCode();
        $this->address = $this->address1 . (($this->address2 != "") ? " " . $this->address2 : "");
        $this->name = $this->fullname();
        if (trim($this->name) == '') {
            $customer = $this->getCustomer();
            if ($customer) {
                $this->name = $customer->getName();
            }
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function isComplete()
    {
        $complete = true;
        $required = ['address1', 'address2', 'postcode', 'city', 'country_id'];
        if ($this->people_id == 1) { //person
            $required = array_merge(['firstname', 'lastname'], $required);
        } else { //company
            $required = array_merge(['company', 'vat_number'], $required);
        }
        if ($this->billing == 0) { //phone is mandatory for shipping
            $required = array_merge(['phone'], $required);
        }
        if ($this->country_id > 0) {
            $country = $this->country();
            if ($country->hasStates()) {
                $required = array_merge(['state_id'], $required);
            }
        }
        foreach ($required as $r) {
            $field = $this->getAttribute($r);
            if (trim($field) == '' OR $field == null) {
                \Utils::log($field, "ADDRESS isComplete INVALIDATION FIELD: [$r]");
                $complete = false;
                break;
            }
        }
        return $complete;
    }

    /**
     * @return bool
     */
    public function isDeletable()
    {
        if ((int)$this->created_by == 0) {
            return false;
        }
        $order_ship = \Order::where('shipping_address_id', $this->id)->count("id");
        $order_bill = \Order::where('billing_address_id', $this->id)->count("id");
        if ($order_ship > 0 OR $order_bill > 0) {
            return false;
        }
        return true;
    }

    public function toHtml($lang = 'default')
    {
        //$lang = ($lang == 'default') ? \Core::getLang() : $lang;
        $country = $this->country($lang);
        $state = $this->state($lang);
        $name = $this->fullname();

        $html = "<strong>$name</strong>";
        if (\Config::get('app.cf_required', true)) {
            $html .= ($this->cf != '') ? "<br><strong>Codice fiscale:</strong> $this->cf" : "";
        }
        $html .= ($this->vat_number != '') ? "<br><strong>Partita IVA:</strong> $this->vat_number" : "";
        $html .= ($this->address1 != '') ? "<br><strong>Indirizzo:</strong> $this->address1" : "";
        $html .= ($this->address2 != '') ? "<br><strong>Civico:</strong> $this->address2" : "";
        $html .= ($this->postcode != '') ? "<br><strong>CAP:</strong> $this->postcode" : "";
        $html .= ($this->city != '') ? "<br><strong>Città:</strong> $this->city" : "";
        $html .= ($this->state_id > 0) ? "<br><strong>Provincia/Stato:</strong> $state->name" : "";
        $html .= ($this->country_id > 0) ? "<br><strong>Nazione:</strong> $country->name" : "";
        $html .= ($this->phone != '') ? "<br><strong>Telefono:</strong> $this->phone" : "";
        $html .= ($this->phone_mobile != '') ? "<br><strong>Cellulare:</strong> $this->phone_mobile" : "";
        $html .= ($this->extrainfo != '') ? "<br><strong>Informazioni aggiuntive:</strong> $this->extrainfo" : "";

        return $html;
    }

    public function toContactHtml($lang = 'default')
    {
        $name = $this->fullname();

        $html = "<strong>$name</strong>";
        $html .= ($this->phone != '') ? "<br><strong>Telefono:</strong> $this->phone" : "";
        $html .= ($this->phone_mobile != '') ? "<br><strong>Cellulare:</strong> $this->phone_mobile" : "";
        $html .= ($this->extrainfo != '') ? "<br><strong>Informazioni aggiuntive:</strong> $this->extrainfo" : "";

        return $html;
    }

    /**
     * @return string
     */
    public function gmap()
    {
        $lang = \Core::getLang();
        $country = $this->country($lang);
        $state = $this->state($lang);
        $base_url = "http://maps.google.com/maps?f=q&hl=$lang&geocode=&q=";
        $str = $this->address1 . " " . $this->address2;
        $str .= ($this->postcode != '') ? " $this->postcode" : "";
        $str .= ($this->city != '') ? " $this->city" : "";
        $str .= ($this->state_id > 0) ? " $state->name" : "";
        $str .= ($this->country_id > 0) ? " $country->name" : "";
        return $base_url . urlencode($str);
    }

    /**
     * @param string $lang
     * @return string
     */
    public function extendedName($lang = 'default')
    {
        $country = $this->country($lang);
        $state = $this->state($lang);
        $name = $this->fullname();

        $html = "$name -";
        $html .= ($this->address1 != '') ? " $this->address1" : "";
        $html .= ($this->address2 != '') ? " $this->address2" : "";
        $html .= ($this->postcode != '') ? " $this->postcode" : "";
        $html .= ($this->city != '') ? " $this->city" : "";
        $html .= ($this->state_id > 0) ? " ($state->name)" : "";
        $html .= ($this->country_id > 0) ? ", $country->name" : "";

        return $html;
    }

    /**
     * @param string $lang
     * @return string
     */
    public function fullAddress($lang = 'default')
    {
        $country = $this->country($lang);
        $state = $this->state($lang);

        $html = "";
        $html .= ($this->address1 != '') ? " $this->address1 $this->address2<br>" : "";
        $html .= ($this->postcode != '') ? " $this->postcode" : "";
        $html .= ($this->city != '') ? " $this->city" : "";
        $html .= ($this->state_id > 0) ? " ($state->iso_code)" : "";
        $html .= ($this->country_id > 0) ? ", $country->name" : "";

        return $html;
    }

    /**
     * @param string $lang
     * @return string
     */
    public function legalInfo($lang = 'default')
    {

        $html = "";
        if (\Config::get('app.cf_required', true)) {
            $html .= ($this->cf != '') ? "C.F: $this->cf " : "";
        }
        $html .= ($this->vat_number != '') ? "P.IVA: $this->vat_number" : "";

        return $html;
    }

    /**
     * @return bool
     */
    public function isPerson()
    {
        return $this->people_id == self::TYPE_PERSON;
    }

    /**
     * @return bool
     */
    public function isCompany()
    {
        return $this->people_id == self::TYPE_COMPANY;
    }

    /**
     * @return bool
     */
    public function isBilling()
    {
        return $this->billing == 1;
    }

    /**
     * @return bool
     */
    public function isCompleteForFee()
    {

        //if 'fees' and 'sap_fees' features are disabled than is always true
        if (feats()->noReceipts())
            return true;

        $hasShopDelivery = CartManager::hasShopDelivery();
        $invoiceRequired = CartManager::isInvoiceRequired();
        $differentBilling = CartManager::hasDifferentBilling();
        $user = \FrontUser::get();
        $billingAddressesCount = 0;
        if ($user) {
            $billingAddressesCount = count($user->getAddresses(1));
        }

        $vat_length = ($this->isPerson()) ? strlen(trim($this->cf)) : strlen(trim($this->vat_number));

        //if "shop delivery" is on, than the "shipping address" is treated like a "billing address"
        if ($invoiceRequired and $hasShopDelivery and $vat_length == 0) {
            return false;
        }

        //if invoice is required and the address is a billing change the vat
        if ($invoiceRequired and $this->isBilling() and $vat_length == 0) {
            return false;
        }

        //if it has not different billing and is not billing and there no "billing addresses"
        if ($invoiceRequired and !$differentBilling and !$this->isBilling() and $vat_length == 0 and $billingAddressesCount == 0) {
            return false;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isFeeShowable()
    {

        $invoiceRequired = CartManager::isInvoiceRequired();

        if ($this->isBilling()) {
            return true;
        }

        return $invoiceRequired;
    }

    /**
     * @return bool
     */
    public function hasFiscalCode(){
        $codeLength = ($this->isPerson()) ? strlen(trim($this->cf)) : strlen(trim($this->vat_number));
        return $codeLength > 0;
    }

    /**
     * @return array
     */
    static function getDefaultAttributes(){
        return [
            'brand_id' => 0,
            'supplier_id' => 0,
            'warehouse_id' => 0,
            'billing' => 0,
            'people_id' => 1,
            'published' => 1,
            'alias' => 'Il mio indirizzo',
            'company' => '',
            'other' => null,
            'fax' => null,
            'dni' => null,
        ];
    }

    /**
     * @return string|null
     */
    function getStateNameAttribute(){
        return $this->stateName();
    }

    /**
     * @return string|null
     */
    function getStateCodeAttribute(){
        return $this->stateCode();
    }

    /**
     * @return string|null
     */
    function getCountryNameAttribute(){
        return $this->countryName();
    }

    /**
     * @return string|null
     */
    function getCountryCodeAttribute(){
        return $this->countryCode();
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new AddressPresenter($this);
    }

}

class AddressPresenter extends Presenter
{

    public function presentCreated()
    {
        return Format::datetime($this->created_at);
    }

}
