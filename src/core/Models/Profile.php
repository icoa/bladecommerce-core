<?php
/**
 * Created by PhpStorm.
 * 
 * User: f.politi
 * Date: 09/01/2015
 * Time: 16:35
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $username
 * @property string $provider
 * @property string $identifier
 * @property string $webSiteURL
 * @property string $profileURL
 * @property string $photoURL
 * @property string $coverInfoURL
 * @property string $displayName
 * @property string $description
 * @property string $firstName
 * @property string $lastName
 * @property string $gender
 * @property string $language
 * @property string $age
 * @property string $birthDay
 * @property string $birthMonth
 * @property string $birthYear
 * @property string $email
 * @property string $emailVerified
 * @property string $phone
 * @property string $address
 * @property string $country
 * @property string $region
 * @property string $city
 * @property string $zip
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\Profile whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereUsername($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereProvider($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereIdentifier($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereWebSiteURL($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereProfileURL($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile wherePhotoURL($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereCoverInfoURL($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereFirstName($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereLastName($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereGender($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereLanguage($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereAge($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereBirthDay($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereBirthMonth($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereBirthYear($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereEmailVerified($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereCountry($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereRegion($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereZip($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Profile whereUpdatedAt($value)
 */

class Profile extends Eloquent{

    function setAttributes($attributes){
        if(is_array($attributes)){
            foreach ($attributes as $key => $value) {
                $this->setAttribute($key, $value);
            }
        }
    }
}