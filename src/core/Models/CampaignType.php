<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * CampaignType
 *
 * @property integer $id
 * @property string $name
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\CampaignType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\CampaignType whereName($value)
 */
class CampaignType extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'campaign_types';
    protected $guarded = array();
    protected $isBlameable = FALSE;
    public $timestamps = FALSE;
    public $softDelete = false;
    public $db_fields = array(
        "name"
    );

    public function canDelete(){
        \Utils::log("canDelete");
        $id = $this->id;
        $total = \Campaign::where("campaign_type",$id)->count("id");
        if($total > 0){
            return "Non è possibile eliminare questo elemento in quanto è riferito da altre entità";
        }
        return true;
    }


    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CampaignTypePresenter($this);
    }

}

class CampaignTypePresenter extends Presenter {

    public function presentCampaigns() {
        \Utils::log("presentCampaigns");
        $id = $this->id;
        $total = \Campaign::where("campaign_type",$id)->count("id");
        return $total;
    }

}