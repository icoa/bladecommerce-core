<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * AttributeSet
 *
 * @property integer $id
 * @property string $uname
 * @property boolean $is_default
 * @property integer $parent_id
 * @property integer $position
 * @property boolean $published
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereUname($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\AttributeSet whereDeletedAt($value)
 */
class AttributeSet extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'attributes_sets';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    protected $db_fields = array(
        "uname",
        "is_default",
        "parent_id",
        "published",
        "position",
    );    
    
    protected $db_fields_cloning = array(
        "uname" => SingleModel::CLONE_UNIQUE_TEXT,        
        "published" => 0
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new AttributeSetPresenter($this);
    }
    
    
    function _after_clone(&$source, &$model) {
        $contents = DB::table('attributes_sets_content')->where('attribute_set_id', $source->id)->get();
        $contents_cache = DB::table('attributes_sets_content_cache')->where('attribute_set_id', $source->id)->get();
        $rules = DB::table('attributes_sets_rules')->where('attribute_set_id', $source->id)->get();
        
        foreach($contents as $c){
            DB::table('attributes_sets_content')->insert([
                    'attribute_set_id' => $model->id,
                    'attribute_id' => $c->attribute_id,
                    'rule' => $c->rule,
                    'position' => $c->position,
                    'default_value' => $c->default_value,
                ]);
        }
        
        foreach ($contents_cache as $c) {
            DB::table('attributes_sets_content_cache')->insert([
                    'attribute_set_id' => $model->id,
                    'attribute_id' => $c->attribute_id,                    
                    'position' => $c->position,
                    'default_value' => $c->default_value,
                ]);
        }
        
        foreach ($rules as $c) {
            DB::table('attributes_sets_rules')->insert([
                    'attribute_set_id' => $model->id,
                    'target_type' => $c->target_type,                    
                    'target_mode' => $c->target_mode,                    
                    'target_id' => $c->target_id,                    
                    'position' => $c->position,
                ]);
        }
    }
    
    function _before_delete() {
        if ($this->forceDeleting == false) {
            return;
        }
        $id = $this->id;
        \Utils::watch();
        DB::table('attributes_sets_content')->where('attribute_set_id', $id)->delete();
        DB::table('attributes_sets_content_cache')->where('attribute_set_id', $id)->delete();
        DB::table('attributes_sets_rules')->where('attribute_set_id', $id)->delete();
        \Utils::unwatch();
    }
}

class AttributeSetPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}
