<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * ProductCombination
 *
 * @property integer $id
 * @property string $name
 * @property integer $product_id
 * @property string $sku
 * @property string $supplier_sku
 * @property string $sap_sku
 * @property string $location
 * @property string $ean13
 * @property string $upc
 * @property float $buy_price
 * @property float $price
 * @property float $ecotax
 * @property integer $quantity
 * @property float $weight
 * @property float $unit_price_impact
 * @property boolean $isDefault
 * @property integer $minimal_quantity
 * @property string $available_date_from
 * @property string $available_date_to
 * @property-read mixed $label
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereSupplierSku($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereSapSku($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereLocation($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereEan13($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereUpc($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereBuyPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereEcotax($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereUnitPriceImpact($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereIsDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereMinimalQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereAvailableDateFrom($value)
 * @method static \Illuminate\Database\Query\Builder|\ProductCombination whereAvailableDateTo($value)
 */
class ProductCombination extends SingleModel implements PresentableInterface {
    /* LARAVEL PROPERTIES */

    protected $table = 'products_combinations';
    protected $guarded = array();
    protected $softDelete = false;
    protected $isBlameable = false;
    public $timestamps = false;


    public $db_fields = array(
        "product_id",
        "sku",
        "supplier_sku",
        "location",
        "ean13",
        "upc",
        "wholesale_price",
        "price",
        "ecotax",
        "quantity",
        "weight",
        "unit_price_impact",
        "isDefault",
        "minimal_quantity",
        "available_date_from",
        "available_date_to"
    );


    public function setPriceAttribute($value){
        $value = (float)$value;
        $this->attributes['price'] = round($value,2);
    }

    public function setBuyPriceAttribute($value){
        $value = (float)$value;
        $this->attributes['buy_price'] = round($value,2);
    }

    public function getAttributes(){
        $data = [];
        $attributes = DB::table("products_combinations_attributes")->where("combination_id", $this->id)->lists('attribute_id');
        $attributes = array_unique($attributes);
        foreach($attributes as $attribute_id){
            $attribute = Attribute::getPublicObj($attribute_id);
            $attribute_options_ids = DB::table("products_combinations_attributes")->where("combination_id", $this->id)->where("attribute_id", $attribute_id)->lists('option_id');
            $attribute_options = AttributeOption::rows()->whereIn('id',$attribute_options_ids)->orderBy('position')->get();
            $attribute->options = $attribute_options;
            $data[] = $attribute;
        }
        return $data;
    }

    public function getOptions(){
        $data = [];
        $combinations_attributes = DB::table("products_combinations_attributes")->where("combination_id", $this->id)->get();
        foreach($combinations_attributes as $ca){
            $data[] = (object)[
                'attribute' => Attribute::getPublicObj($ca->attribute_id),
                'option' => AttributeOption::getPublicObj($ca->option_id),
            ];
        }
        return $data;
    }

    public function getOptionsMap(){
        $data = [];
        $combinations_attributes = DB::table("products_combinations_attributes")->where("combination_id", $this->id)->get();
        foreach($combinations_attributes as $ca){
            if(!isset($data[$ca->attribute_id])){
                $data[$ca->attribute_id] = [];
            }
            $data[$ca->attribute_id] = $ca->option_id;
        }
        return $data;
    }

    public function getLabelAttribute($value){
        $labels = [];
        $options = $this->getOptions();
        foreach($options as $obj){
            $labels[] = $obj->attribute->name.': '.$obj->option->title;
        }
        return implode(', ',$labels);
    }

    static function jsonSwatches($combinations_attributes){
        $schema = [];
        $attribute_options = [];
        $combination_swatches = [];
        foreach ($combinations_attributes as $attribute) {
            $combination_swatches = $attribute->combination_swatches;
            foreach($attribute->options as $option){
                $attribute_options[(string)$option->id] = $option->swatches;
            }
        }
        $schema['options'] = $attribute_options;
        $schema['swatches'] = $combination_swatches;
        //Utils::log($schema,__METHOD__);
        return json_encode($schema);
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new ProductCombinationPresenter($this);
    }

}

class ProductCombinationPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}


ProductCombination::updated(function($model){
    $quantity = (int)$model->getAttribute('quantity');
    $originalQuantity = (int)$model->getOriginal('quantity');
    if( $quantity != $originalQuantity ){
        $product = Product::getObj($model->product_id);
        $product->updateQuantity(1);

        if ($originalQuantity > $quantity) {
            $action = 'decrease';
        } else {
            $action = 'increase';
        }
        $qty = abs($originalQuantity - $quantity);
        $params = [
            'product_id' => $model->product_id,
            'product_reference' => $model->sku == '' ? $product->sku : $model->sku,
            'product_name' => $product->name."<br>".$model->name,
            'qty' => $qty,
        ];
        StockMvt::register($action, $params);
    }
});


ProductCombination::created(function ($model) {
    $product = Product::getObj($model->product_id);
    $product->updateQuantity(1);
    $action = 'increase';
    $qty = $model->quantity;
    $params = [
        'product_id' => $model->product_id,
        'product_reference' => $model->sku == '' ? $product->sku : $model->sku,
        'product_name' => $product->name . "<br>" . $model->name,
        'qty' => $qty,
    ];
    StockMvt::register($action, $params);
});


ProductCombination::deleted(function ($model) {
    $product = Product::getObj($model->product_id);
    $product->updateQuantity(1);
    $action = 'decrease';
    $qty = $model->quantity;
    $params = [
        'product_id' => $model->product_id,
        'product_reference' => $model->sku == '' ? $product->sku : $model->sku,
        'product_name' => $product->name . "<br>" . $model->name,
        'qty' => $qty,
    ];
    StockMvt::register($action, $params);
});