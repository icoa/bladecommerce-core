<?php

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use services\Bluespirit\Negoziando\Fidelity\TraitOrderFidelity;
use services\Bluespirit\Negoziando\Fidelity\TraitOrderVoucher;
use services\Fees\Traits\TraitFeesOrder;
use services\Membership\Traits\TraitAffiliateOrder;
use services\Models\OrderSlip;
use services\Morellato\Soap\Traits\TraitOrderSap;
use services\Repositories\OrderRepository;
use services\Strap\Traits\TraitOrderStrap;
use services\Traits\HasEloquentLogger;
use services\Traits\HasParam;
use services\Traits\TraitCartOrderAttributes;
use services\Traits\TraitOrderAvailabilityManager;
use services\Traits\TraitOrderBuilder;
use services\Traits\TraitOrderCartRelationManager;
use services\Traits\TraitOrderDocumentManager;
use services\Traits\TraitOrderGlsManager;
use services\Traits\TraitOrderProductManager;
use services\Traits\TraitOrderRefundManager;
use services\Traits\TraitOrderRelationManager;
use services\Traits\TraitOrderRmaManager;
use services\Traits\TraitOrderStateManager;
use services\Traits\TraitOrderWarningManager;

/**
 * Order
 *
 * @property integer $id
 * @property string $reference
 * @property integer $parent_id
 * @property integer $fee_parent_id
 * @property integer $shop_group_id
 * @property integer $shop_id
 * @property integer $carrier_id
 * @property integer $payment_id
 * @property string $lang_id
 * @property integer $customer_id
 * @property integer $cart_id
 * @property integer $currency_id
 * @property integer $shipping_address_id
 * @property integer $billing_address_id
 * @property integer $status
 * @property string $secure_key
 * @property string $payment
 * @property integer $payment_status
 * @property float $conversion_rate
 * @property string $module
 * @property boolean $recyclable
 * @property string $direct_name
 * @property boolean $gift
 * @property string $gift_message
 * @property string $gift_box
 * @property string $notes
 * @property string $notes_internal
 * @property string $shipping_number
 * @property string $invoice_code
 * @property string $carrier_shipping
 * @property float $fixed_discounts
 * @property float $total_discounts
 * @property float $total_discounts_tax_incl
 * @property float $total_discounts_tax_excl
 * @property float $total_discounts_extra
 * @property float $total_paid
 * @property float $total_paid_tax_incl
 * @property float $total_paid_tax_excl
 * @property float $total_paid_real
 * @property float $total_products
 * @property float $total_products_tax_incl
 * @property float $total_products_tax_excl
 * @property float $total_shipping
 * @property float $total_shipping_tax_incl
 * @property float $total_shipping_tax_excl
 * @property float $carrier_tax_rate
 * @property float $total_payment
 * @property float $total_payment_tax_incl
 * @property float $total_payment_tax_excl
 * @property float $total_wrapping
 * @property float $total_wrapping_tax_incl
 * @property float $total_wrapping_tax_excl
 * @property float $total_order
 * @property float $total_order_tax_incl
 * @property float $total_order_tax_excl
 * @property float $total_taxes
 * @property float $total_weight
 * @property integer $total_quantity
 * @property integer $invoice_number
 * @property integer $delivery_number
 * @property string $invoice_date
 * @property string $delivery_date
 * @property string $invoice_notes
 * @property string $delivery_notes
 * @property string $invoice_type
 * @property boolean $invoice_sent
 * @property boolean $invoice_required
 * @property boolean $active
 * @property integer $valid
 * @property integer $campaign_id
 * @property string $coupon_code
 * @property integer $cart_rule_id
 * @property boolean $send_purchase
 * @property boolean $send_refund
 * @property boolean $send_confirm
 * @property boolean $flag_warnable
 * @property boolean $verified_address
 * @property string $availability_mode
 * @property string $availability_shop_id
 * @property integer $delivery_store_id
 * @property string $warehouse
 * @property string $receipt
 * @property integer $affiliate_id
 * @property string $tracking_url
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read mixed $backend_link
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @property-read \Illuminate\Database\Eloquent\Collection|\OrderDetail[] $details
 * @property-read mixed $grand_total
 * @property-read mixed $affiliate_tracking_url
 * @property-read Order $child_starling
 * @property-read Order $child_receipt
 * @property-read Order $fee_parent
 * @property-read mixed $fee_org
 * @property-read mixed $fee_number
 * @property-read mixed $fee_code
 * @property-read mixed $local_receipt_fee
 * @property-read mixed $local_receipt_fee_html
 * @property-read mixed $fee_transaction_code
 * @property-read mixed $fee_shipping_country
 * @property-read OrderSlip $order_slip
 * @property-read \Illuminate\Database\Eloquent\Collection|OrderHistory[] $histories
 * @property-read \Illuminate\Database\Eloquent\Collection|OrderHistory::class)->where[] $histories_states
 * @property-read \Illuminate\Database\Eloquent\Collection|OrderHistory::class)->where[] $histories_payment_states
 * @property-read \Illuminate\Database\Eloquent\Collection|Rma[] $rmas
 * @property-read \Customer $customer
 * @method static \Illuminate\Database\Query\Builder|\Order whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereReference($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereFeeParentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereShopGroupId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCarrierId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaymentId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCartId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereShippingAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereBillingAddressId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereSecureKey($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePayment($value)
 * @method static \Illuminate\Database\Query\Builder|\Order wherePaymentStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereConversionRate($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereModule($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereRecyclable($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDirectName($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereGift($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereGiftMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereGiftBox($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereNotesInternal($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereShippingNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCarrierShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereFixedDiscounts($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalDiscounts($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalDiscountsTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalDiscountsTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalDiscountsExtra($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPaid($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPaidTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPaidTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPaidReal($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalProducts($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalProductsTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalProductsTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalShipping($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalShippingTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalShippingTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCarrierTaxRate($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPayment($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPaymentTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalPaymentTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalWrapping($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalWrappingTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalWrappingTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalOrderTaxIncl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalOrderTaxExcl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalTaxes($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalWeight($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTotalQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryNumber($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryDate($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryNotes($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceType($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceSent($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereInvoiceRequired($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereValid($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCampaignId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCouponCode($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCartRuleId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereSendPurchase($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereSendRefund($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereSendConfirm($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereFlagWarnable($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereVerifiedAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereAvailabilityMode($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereAvailabilityShopId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeliveryStoreId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereWarehouse($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereReceipt($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereTrackingUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Order whereDeletedBy($value)
 * @method static \Order withAvailabilityModes($modes)
 * @method static \Order withoutAvailabilityModes($modes)
 * @method static \Order available()
 * @method static \Order withAffiliate($affiliate)
 * @method static \Order withoutAffiliate()
 * @method static \Order withInvoiceType($type)
 * @method static \Order withoutInvoiceType($type)
 * @method static \Order onlyInvoices()
 * @method static \Order onlyReceipts()
 * @method static \Order onlyStarlings()
 * @method static \Order withoutStarlings()
 * @method static \Order withFees()
 * @method static \Order withFeesRequirements()
 * @method static \Order withInvoiceSent()
 * @method static \Order withoutInvoiceSent()
 * @method static \Order withFeesDate($date = null)
 * @method static \Order withFeesWeek($week = null)
 * @method static \Order withFeesYear($year = null)
 * @method static \Order withCorrectDateForFees()
 * @method static \Order withLocalReceiptFeeWarning($includeOnlinePayments = false)
 * @method static \Order forSap()
 * @method static \Order submittableToSap()
 * @method static \Order submittableToSapOrderFlow()
 * @method static \Order migrationableToWorking()
 * @method static \Order withOrderSlipDetailType($type)
 * @method static \Order withoutOrderSlipDetailType($type)
 * @method static \Order withWarnableCustomer($flag = 0)
 * @method static \Order customerWarnableMissedPickup()
 * @method static \Order customerWarnablePackageReady()
 * @method static \Order withStatus($status)
 * @method static \Order withPaymentStatus($status)
 * @method static \Order withoutStatus($status)
 * @method static \Order withoutPaymentStatus($status)
 * @method static \Order withHistoryStates($status, $last_update = null)
 * @method static \Order withHistoryPaymentStates($status, $last_update = null)
 * @method static \Order withoutHistoryStates($status, $last_update = null)
 * @method static \Order withoutHistoryPaymentStates($status, $last_update = null)
 * @method static \Order withRma()
 * @method static \Order withoutRma()
 * @method static \Order withRmaStates($status)
 * @method static \Order withoutRmaStates($status)
 * @method static \Order refundableOrSoftCancelled($date_range = null)
 * @method static \Order trackableWithGls()
 * @method static \Order forCustomer($customer_id = null)
 * @method static \Order withPayment($payment)
 * @method static \Order withoutPayment($payment)
 * @method static \Order withCarrier($carrier)
 * @method static \Order withoutCarrier($carrier)
 * @method static \Order withOrderDetailStatus($status)
 * @method static \Order withoutOrderDetailStatus($status)
 * @method static \Order withWarnableFlag($flag = 0)
 * @method static \Order shopWarnableCarrierShipment()
 * @mixin HasParam
 * @mixin HasEloquentLogger
 * @mixin TraitOrderProductManager
 * @mixin TraitOrderDocumentManager
 * @mixin TraitOrderAvailabilityManager
 * @mixin TraitOrderBuilder
 * @mixin TraitAffiliateOrder
 * @mixin TraitCartOrderAttributes
 * @mixin TraitFeesOrder
 * @mixin TraitOrderSap
 * @mixin TraitOrderFidelity
 * @mixin TraitOrderVoucher
 * @mixin TraitOrderStrap
 * @mixin TraitOrderStateManager
 * @mixin TraitOrderRmaManager
 * @mixin TraitOrderRefundManager
 * @mixin TraitOrderGlsManager
 * @mixin TraitOrderRelationManager
 * @mixin TraitOrderWarningManager
 * @property-read mixed $discount_amount
 * @property-read mixed $products_amount
 * @property-read mixed $shipping_amount
 * @property-read mixed $additional_costs_amount
 * @property-read mixed $total_order_amount
 * @property-read \Illuminate\Database\Eloquent\Collection|OrderSlipDetail[] $order_slip_details
 * @property-read \Illuminate\Database\Eloquent\Collection|CustomerEmailWarning[] $customer_email_warnings
 * @method static \Order withBasicAvailability()
 * @method static \Order withoutSlips()
 * @method static \Order migrationableToClosed()
 * @method static \Order withoutGlsTracking()
 * @method static \Order onlyChildren()
 * @method static \Order withoutChildren()
 * @method static \Order withDeliveryStore($store = null)
 * @method static \Order withAvailabilityStore($store = null)
 * @method static \Order withOrderSlipDetailReason($type)
 * @method static \Order withoutOrderSlipDetailReason($type)
 * @method static \Order withCustomer($customer)
 * @method static \Order withoutCustomer($customer)
 */
class Order extends SingleModel
{
    use HasParam;
    use HasEloquentLogger;
    use TraitOrderProductManager;
    use TraitOrderDocumentManager;
    use TraitOrderAvailabilityManager;
    use TraitOrderBuilder;
    use TraitAffiliateOrder;
    use TraitCartOrderAttributes;
    use TraitFeesOrder;
    use TraitOrderSap;
    use TraitOrderFidelity;
    use TraitOrderVoucher;
    use TraitOrderStrap;
    use TraitOrderStateManager;
    use TraitOrderRmaManager;
    use TraitOrderRefundManager;
    use TraitOrderGlsManager;
    use TraitOrderCartRelationManager;
    use TraitOrderRelationManager;
    use TraitOrderWarningManager;

    const INVOICE_TYPE_INVOICE = 'invoice';
    const INVOICE_TYPE_RECEIPT = 'receipt';
    const INVOICE_TYPE_STARLING = 'starling';
    const INVOICE_TYPE_NONE = null;

    const MODE_MASTER = 'master';
    const MODE_OFFLINE = 'offline';
    const MODE_LOCAL = 'local';
    const MODE_SHOP = 'shop';
    const MODE_ONLINE = 'online';
    const MODE_MIXED = 'mixed';
    const MODE_STAR = '*';

    const SCOPE_DEFAULT = 'default';
    const SCOPE_FEES = 'fees';
    const SCOPE_DOCUMENT = 'document';

    /* LARAVEL PROPERTIES */

    protected $table = 'orders';
    protected $guarded = array('');
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    protected $lang = null;
    protected $has_published = false;


    public $db_fields = array(
        'reference',
        'shop_group_id',
        'shop_id',
        'carrier_id',
        'payment_id',
        'lang_id',
        'customer_id',
        'cart_id',
        'currency_id',
        'shipping_address_id',
        'billing_address_id',
        'status',
        'secure_key',
        'payment',
        'payment_status',
        'conversion_rate',
        'module',
        'recyclable',
        'gift',
        'gift_message',
        'notes',
        'shipping_number',
        'total_discounts',
        'total_discounts_tax_incl',
        'total_discounts_tax_excl',
        'total_shipping',
        'total_shipping_tax_incl',
        'total_shipping_tax_excl',
        'carrier_tax_rate',
        'total_payment',
        'total_payment_tax_incl',
        'total_payment_tax_excl',
        'total_wrapping',
        'total_wrapping_tax_incl',
        'total_wrapping_tax_excl',
        'active',
        'valid',
        'delivery_store_id',
        'send_confirm',
        'parent_id',
        'warehouse',
        'availability_mode',
        'availability_shop_id',
        'invoice_required',
    );

    protected $repository;

    /**
     * @var string
     */
    protected $innerScope = 'default';


    function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->lang = \Core::getLang();
    }

    function setLang($lang)
    {
        $this->lang = $lang;
    }

    function getLang()
    {
        return isset($this->lang) ? $this->lang : \Core::getLang();
    }


    /**
     * @return bool
     */
    function isParent()
    {
        return $this->getMode() == 'master';
    }

    /**
     * @return bool
     */
    function isChildren()
    {
        return $this->parent_id > 0;
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeOnlyChildren(Builder $query)
    {
        return $query->where('parent_id', '>', 0);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithoutChildren(Builder $query)
    {
        return $query->where(function ($subquery) {
            $subquery->whereNull('parent_id')->orWhere('parent_id', 0);
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|\Order[]
     */
    public function getChildren()
    {
        return self::where('parent_id', $this->id)->get();
    }

    /**
     * @return \Order|null
     */
    public function getParent()
    {
        return self::getObj($this->parent_id);
    }

    function getTokens()
    {
        $data = $this->toArray();
        if (isset($data['params']))
            unset($data['params']);


        $shipping = $this->getShippingAddress();
        $billing = $this->getBillingAddress();
        $customer = $this->getCustomer();
        $carrier = $this->getCarrier();
        $status = $this->getStatus();
        $payment_status = $this->getPaymentStatus();
        $products_details = $this->getProductsDetails();

        $data += [
            'order_number' => $data['reference'],
            'order_date' => \Format::datetime($data['created_at']),
            'order_total' => \Format::currency($data['total_order'], $data['currency_id']),
            'order_discount' => \Format::currency($data['total_discounts'], $data['currency_id']),
            'order_notes' => nl2br(trim($data['notes'])),
            'customer_name' => $customer->getName(),
            'customer_email' => $customer->email,
            'billing_details' => $billing->toHtml(),
            'shipping_details' => $shipping->toHtml(),
            'shipping_type' => $carrier->name,
            'shipping_cost' => \Format::currency($data['total_shipping'], $data['currency_id']),
            'payment_type' => $data['payment'],
            'payment_cost' => \Format::currency($data['total_payment'], $data['currency_id']),
            'products_details' => $products_details,
            'order_details' => $products_details,
            'customer_link' => $this->getOrderFrontUrl(),
            'status_name' => $status->name,
            'payment_status_name' => ($payment_status) ? $payment_status->name : 'N/D',
            'shipment_link' => $this->getTrackingUrl(),
            'shipment_css' => ($this->hasShipment()) ? 'block' : 'none',
            'customer_contact' => $shipping->toContactHtml(),
            'sitename' => \Cfg::get('SITE_TITLE'),
            'sitemail' => \Cfg::get('MAIL_ORDERS_ADDRESS'),
        ];

        $store = $this->getDeliveryStore();
        if ($store) {
            $data['shipping_customer_details'] = $data['shipping_details'];

            if ($billing)
                $data['customer_contact'] = $billing->toContactHtml();

            $data['shipping_details'] = $store->toHtml();
            $data['store_name'] = $store->name;
            $data['store_address'] = $store->getFullAddress();
            $data['store_email'] = $store->getEmailAddress();
        }

        return $data;
    }


    function sendEmailById($template_id)
    {
        $data = $this->getTokens();
        $lang = (isset($data['lang_id']) AND \Str::length($data['lang_id']) == 2) ? $data['lang_id'] : \FrontTpl::getLang();
        $recipient = $data['customer_email'];
        /** @var \Email $email */
        $email = \Email::getPublicObj($template_id, $lang);
        if ($email) {
            $email->setCustomer($this->getCustomer())->send($data, $recipient);
            $this->logger()->log("Sent email {$email->code} to recipient $recipient", 'EMAIL_SENT');
        }
    }

    function sendEmail($what = 'confirm')
    {
        $template_id = 0;
        switch ($what) {
            case 'confirm':
                $template_id = 1;
                $this->send_confirm = 1;
                $this->save(['timestamps' => false]);
                break;
            case 'update_status':
                $template_id = 11;
                break;
            default:
                $email = \Email::getByCode($what);
                if ($email) {
                    $template_id = $email->id;
                }
                break;
        }
        return $this->sendEmailById($template_id);
    }


    function getSimpleDescription()
    {
        return \Cfg::get("SHORTNAME") . " - Ordine N°. " . $this->reference;
    }


    function getOrderFrontUrl()
    {
        $url = Link::absolute()->shortcut('order');
        $url .= "?page=details&id=$this->id&secure_key=$this->secure_key";
        return $url;
    }


    function updateTotals($custom = [])
    {
        $products = $this->getProducts();

        $data = [
            'total_paid' => 0,
            'total_paid_tax_incl' => 0,
            'total_paid_tax_excl' => 0,
            'total_paid_real' => 0,
            'total_products' => 0,
            'total_products_tax_incl' => 0,
            'total_products_tax_excl' => 0,
            'total_order' => 0,
            'total_order_tax_incl' => 0,
            'total_order_tax_excl' => 0,
            'total_taxes' => 0,
            'total_weight' => 0,
            'total_quantity' => 0,
        ];

        $tax_rate = \Core::getDefaultTaxRate();

        $discount = $this->fixed_discounts + $this->total_discounts + $this->total_discounts_extra;
        $discount_tax_excl = 0;
        $discount_taxes = 0;

        if ($discount > 0) {
            $discount_tax_excl = \Core::untax($discount, $tax_rate);
            $discount_taxes = $discount - $discount_tax_excl;
        }

        audit(compact('discount', 'discount_tax_excl', 'discount_taxes'), __METHOD__);

        $data = array_merge($data, $custom);

        foreach ($products as $p) {
            $data['total_quantity'] += $p->product_quantity;
            $data['total_weight'] += $p->product_weight;

            $data['total_products'] += $p->total_price_tax_incl;
            $data['total_products_tax_incl'] += $p->total_price_tax_incl;
            $data['total_products_tax_excl'] += $p->total_price_tax_excl;
            $data['total_order'] += $p->product_quantity * $p->cart_price_tax_incl;
            $data['total_order_tax_incl'] += $p->product_quantity * $p->cart_price_tax_incl;
            $data['total_order_tax_excl'] += $p->product_quantity * $p->cart_price_tax_excl;

            $original_item_price = $p->product_unit_price > 0 ? $p->product_unit_price : $p->product->price_official_raw;
            $item_price = $p->product_item_price;
            $data['total_paid'] += $original_item_price * $p->product_quantity;
            $data['total_paid_real'] += $item_price * $p->product_quantity;
            $data['total_paid_tax_incl'] += $data['total_paid'] - $data['total_paid_real'];
            $data['total_paid_tax_excl'] += $data['total_paid_tax_incl'];
        }

        $data['total_taxes'] = round($data['total_products_tax_incl'] - $data['total_products_tax_excl'], 2);

        //add shipping, payment and wrapping
        $data['total_order'] += $this->total_shipping;
        $data['total_order'] += $this->total_payment;
        $data['total_order'] += $this->total_wrapping;
        $data['total_order'] -= $discount;
        if ($data['total_order'] < 0) {
            $data['total_order'] = 0;
        }
        $data['total_order_tax_incl'] += $this->total_shipping_tax_incl;
        $data['total_order_tax_incl'] += $this->total_payment_tax_incl;
        $data['total_order_tax_incl'] += $this->total_wrapping_tax_incl;
        $data['total_order_tax_incl'] -= $discount;
        if ($data['total_order_tax_incl'] < 0) {
            $data['total_order_tax_incl'] = 0;
        }
        $data['total_order_tax_excl'] += $this->total_shipping_tax_excl;
        $data['total_order_tax_excl'] += $this->total_payment_tax_excl;
        $data['total_order_tax_excl'] += $this->total_wrapping_tax_excl;
        $data['total_order_tax_excl'] -= $discount_tax_excl;
        if ($data['total_order_tax_excl'] < 0) {
            $data['total_order_tax_excl'] = 0;
        }

        //\Utils::log($data,"ORDER INT");

        $data['total_taxes'] += ($this->total_shipping_tax_incl - $this->total_shipping_tax_excl);
        $data['total_taxes'] += ($this->total_payment_tax_incl - $this->total_payment_tax_excl);
        $data['total_taxes'] += ($this->total_wrapping_tax_incl - $this->total_wrapping_tax_excl);
        $data['total_taxes'] -= $discount_taxes;
        if ($data['total_taxes'] < 0) {
            $data['total_taxes'] = 0;
        }


        //if this is a children and the parent has a gift card, then we must create a new 'partial' giftCardMovement for the child order and recalculate the totals
        if ($this->isChild()) {
            /* @var Order $parent */
            $parent = $this->getParent();
            $parentGiftCardMovement = $parent->getGiftcardMovement();
            $giftCardMovement = $this->getGiftcardMovement();
            $amount = ($parentGiftCardMovement) ? $parentGiftCardMovement->amount : 0;
            if ($parentGiftCardMovement and is_null($giftCardMovement) and $amount > 0) {
                $totalOrder = \Format::float($data['total_order']);
                $partialAmount = \Format::float($amount - $totalOrder);

                //if the virtual credit is bigger than the actual 'total_order'
                if ($partialAmount > 0) {
                    $parentGiftCardAmount = $partialAmount;
                    $giftCardAmount = $totalOrder;
                    $total_discounts_extra = $totalOrder;
                } else {
                    $parentGiftCardAmount = 0;
                    $giftCardAmount = $amount;
                    $total_discounts_extra = $amount;
                }

                $giftCardMovement = new GiftCardMovement([
                    'gift_card_id' => $parentGiftCardMovement->gift_card_id,
                    'customer_id' => $parentGiftCardMovement->customer_id,
                    'order_id' => $this->id,
                    'amount' => $giftCardAmount,
                ]);
                $giftCardMovement->save();

                if ($giftCardMovement->id > 0) {
                    $parentGiftCardMovement->amount = $parentGiftCardAmount;
                    $parentGiftCardMovement->save();
                    $data['total_discounts_extra'] = $total_discounts_extra;
                    $this->setAttributes($data);
                    $this->save();
                    //re-execute itself
                    usleep(100);
                    $this->total_discounts_extra = $total_discounts_extra;
                    $this->updateTotals();
                    return;
                }

            }
        }

        audit($data, __METHOD__);

        $this->setAttributes($data);
        $this->save();

    }


    function expand()
    {
        $this->total_messages = count($this->getMessages());
        $this->total_rma = count($this->getRma());
        $this->invoice = $this->getInvoice();
        $this->hasInvoice = $this->hasInvoice();
        $this->delivery = $this->getDelivery();
        $this->hasDelivery = $this->hasDelivery();
    }


    static function getLatestOrders()
    {
        $rows = self::orderBy('created_at', 'desc')->take(10)->get();
        foreach ($rows as $row) {
            /** @var Order $order */
            $order = self::getObj($row->id);
            $row->created = \Format::datetime($row->created_at);
            $row->link = \URL::action('OrdersController@getPreview', ['id' => $row->id]);
            $customer = $order->getCustomer();
            $row->customerName = ($customer) ? $customer->getName() : 'N.D.';
            $status = $order->getStatus();
            $row->statusName = ($status) ? "<span class='label' style='background-color:{$status->color};' title='{$status->name}'><i class='{$status->icon}'></i> {$status->name}</span>" : "ND";

            $status = $order->getPaymentStatus();
            $row->paymentStatusName = ($status) ? "<span class='label' style='background-color:{$status->color};' title='{$status->name}'><i class='{$status->icon}'></i> {$status->name}</span>" : "ND";
            $row->total = \Format::currency($row->total_order, true);
        }
        return $rows;
    }


    function setSendFlag($flag, $status = 1)
    {
        try {
            \DB::table('orders')->where('id', $this->id)->update(['send_' . $flag => $status]);
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
            \Utils::error($e->getTraceAsString(), __METHOD__);
        }

    }

    function getSendFlag($flag)
    {
        return isset($this->{'send_' . $flag}) ? (int)$this->{'send_' . $flag} : 0;
    }

    function getFlags()
    {
        $flags = [];
        $order = $this;
        $style = 'font-size:20px; margin-left:8px; vertical-align:middle;';
        if ($order->gift == 1) {
            $flags[] = "<i class='font-gift' style='$style' title='Richiesta confezione regalo $order->gift_box'>";
        }
        if ($order->recyclable == 1) {
            $flags[] = "<i class='font-play' style='$style' title='Richiesta diretta {$order->direct_name}'>";
        }
        $isMorellato = $order->availability_mode != 'local' and $order->availability_mode != 'affiliate';

        if (feats()->sapReceipts())
            $isMorellato = true;

        if ($isMorellato) {
            $flags[] = "<i style='$style color:midnightblue;' title='Ordine Morellato'>M</i>";
            $slip = $order->getMorellatoSlip();
            if ($slip and $slip->delivery_id != '') {
                $flags[] = "<i class='font-check' style='$style color:green;' title='Delivery ID OK'>";
            } else {
                $flags[] = "<i class='font-warning-sign' style='$style color:red;' title='Delivery ID non esistente'>";
            }
            if ($slip and $slip->sales_id != '') {
                $flags[] = "<i class='font-check' style='$style color:green;' title='Sales ID OK'>";
            } else {
                $flags[] = "<i class='font-warning-sign' style='$style color:red;' title='Sales ID non esistente'>";
            }
            if ($slip and $slip->invoice_id != '') {
                $flags[] = "<i class='font-check' style='$style color:green;' title='Invoice ID OK'>";
            } else {
                $flags[] = "<i class='font-warning-sign' style='$style color:red;' title='Invoice ID non esistente'>";
            }
            if (($order->availability_mode == 'shop' or $order->availability_mode == 'mixed') and $order->availability_shop_id != '') {
                $flags[] = "<i style='$style color:purple;' title='Ordine da Negozio Cod. $order->availability_shop_id'>N</i>";
            }
            if ($order->availability_mode == 'mixed') {
                $flags[] = "<i style='$style color:cyan;' title='Ordine misto'>X</i>";
            }
            if ($order->availability_mode == 'unsolved' || $order->availability_mode == 'offline') {
                $flags[] = "<i style='$style color:red;' title='Logica assegnazione prodotti e consegna non risolta in prima istanza'>E</i>";
            }
            if ($order->hasLocalReceiptFeeWarning()) {
                $flags[] = "<i class='blink' style='$style color:black; background-color: #ff6600; padding:0 4px; border-radius: 2px;' title='Attenzione: questo ordine necessita di un codice transazione offline'>B</i>";
            }
            if ($order->isStrapB2B()) {
                $flags[] = "<i class='blink' style='$style color:white; background-color: navy; padding:0 4px; border-radius: 2px;' title='Ordine B2B (Cinturino)'>B</i>";
            }
        }
        return implode('', $flags);
    }


    function getActionsForClerk()
    {
        $btn = null;
        if ($this->status != \Config::get('negoziando.order_status_refunded')) {
            $btn = "<a class='pull-right btn btn-warning' href='javascript:Orders.requestRma({$this->id},\"{$this->reference}\");'>RICHIEDI RIMBORSO</a>";
        }
        return $btn;
    }

    /**
     * @return bool
     */
    function hasGift()
    {
        return $this->gift == 1;
    }

    /**
     * @return bool
     */
    function hasStreaming()
    {
        return $this->recyclable == 1;
    }

    /**
     * @return OrderRepository
     */
    function getRepository()
    {
        if ($this->repository)
            return $this->repository;

        $this->repository = app(OrderRepository::class);
        $this->repository->setOrder($this);
        return $this->repository;
    }

    /**
     * @return string
     */
    public function getBackendLinkAttribute()
    {
        return config('app.url') . "/admin/orders/preview/$this->id";
    }

    /**
     * Determine if the order can be 'modified' (for example add products or something else)
     *
     * @return bool
     */
    public function isEditable($skipFees = false)
    {
        $test = false;

        $status = $this->getStatus();
        if ($status->paid == 0 and $this->getMode() != 'master') {
            $test = true;
        }
        //if the order has a receipt or a starling, it cannot be modified
        if (!$skipFees and ($this->isStarling() or $this->isReceipt())) {
            $test = false;
        }

        return $test;
    }

    /**
     * @return float
     */
    public function getShippingTaxes()
    {
        return round($this->total_shipping_tax_incl - $this->total_shipping_tax_excl, 2);
    }

    /**
     * @return float
     */
    public function getPaymentTaxes()
    {
        return round($this->total_payment_tax_incl - $this->total_payment_tax_excl, 2);
    }

    /**
     * @return float
     */
    public function getWrappingTaxes()
    {
        return round($this->total_wrapping_tax_incl - $this->total_wrapping_tax_excl, 2);
    }

    /**
     * @return float
     */
    public function getExtraCostsTaxes()
    {
        return round($this->getShippingTaxes() + $this->getPaymentTaxes() + $this->getWrappingTaxes(), 2);
    }

    /**
     * @param $scope
     * @return $this
     */
    public function setInnerScope($scope)
    {
        $this->innerScope = $scope;
        return $this;
    }

    /**
     * @return string
     */
    public function getInnerScope()
    {
        return $this->innerScope;
    }

    /**
     * @param $search
     * @return bool
     */
    public function hasInnerScope($search)
    {
        if (!is_array($search))
            $search = [$search];

        return in_array($this->innerScope, $search);
    }

    /**
     * @return float|int|string
     */
    public function getDiscountAmountAttribute()
    {
        $amount = $this->fixed_discounts + $this->total_discounts + $this->total_discounts_extra;
        return \Format::money($amount, $this->currency_id);
    }

    /**
     * @return float|int|string
     */
    public function getProductsAmountAttribute()
    {
        $amount = $this->total_products;
        return \Format::money($amount, $this->currency_id);
    }

    /**
     * @return float|int|string
     */
    public function getShippingAmountAttribute()
    {
        $amount = $this->total_shipping;
        return \Format::money($amount, $this->currency_id);
    }

    /**
     * @return float|int|string
     */
    public function getAdditionalCostsAmountAttribute()
    {
        $amount = $this->total_payment;
        return \Format::money($amount, $this->currency_id);
    }

    /**
     * @return float|int|string
     */
    public function getTotalOrderAmountAttribute()
    {
        $amount = $this->total_order;
        return \Format::money($amount, $this->currency_id);
    }


}