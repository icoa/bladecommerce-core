<?php


/**
 * StockReason
 *
 * @property integer $id
 * @property string $code
 * @property boolean $sign
 * @property string $color
 * @property boolean $active
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\StockReason whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason whereSign($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason whereColor($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason whereActive($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 */
class StockReason extends MultiLangModel {

    /* LARAVEL PROPERTIES */
    protected $table = 'stock_reasons';
    protected $guarded = array();

    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name");
    protected $db_fields = array("active");
    protected $lang_model = 'StockReason_Lang';
    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT
    );
    public $has_published = false;


    static function getByCode($code,$lang = 'default'){
        $id = StockReason::where('code',$code)->pluck('id');
        if($id and $id > 0){
            return self::getObj($id,$lang);
        }
        return null;
    }

    function getName(){
        return "<span class='label' style='background-color:$this->color'>$this->name</span>";
    }

}

/**
 * StockReason_Lang
 *
 * @property integer $stock_reason_id
 * @property string $lang_id
 * @property string $name
 * @property string $template
 * @method static \Illuminate\Database\Query\Builder|\StockReason_Lang whereStockReasonId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\StockReason_Lang whereTemplate($value)
 * @property-read \StockReason $stockReason
 */
class StockReason_Lang extends Eloquent  {


    protected $table = 'stock_reasons_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function stockReason()
    {
        return $this->belongsTo('StockReason');
    }


}