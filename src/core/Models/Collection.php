<?php

use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;

/**
 * Collection
 *
 * @property integer $id
 * @property integer $brand_id
 * @property integer $category_id
 * @property string $category_mode
 * @property string $robots
 * @property boolean $ogp
 * @property string $ogp_type
 * @property string $ogp_image
 * @property string $public_access
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $position
 * @property integer $page_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\$this->lang_model[] $translations
 * @property-read \$this->lang_model)->where('lang_id $translation
 * @property-read \$this->lang_model)->where('lang_id $it
 * @property-read \$this->lang_model)->where('lang_id $en
 * @property-read \$this->lang_model)->where('lang_id $es
 * @property-read \$this->lang_model)->where('lang_id $fr
 * @property-read \$this->lang_model)->where('lang_id $de
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\Collection whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereBrandId($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereCategoryMode($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereRobots($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereOgp($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereOgpType($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereOgpImage($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection wherePublicAccess($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection wherePosition($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection wherePageId($value)
 * @method static \MultiLangModel lang($lang_id)
 * @method static \MultiLangModel rows($lang_id = null)
 */
class Collection extends MultiLangModel implements PresentableInterface {

    /* LARAVEL PROPERTIES */
    protected $table = 'collections';
    protected $guarded = array();
    use Illuminate\Database\Eloquent\SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    
    /* MultiLangModel PROPERTIES */
    protected $lang_fields = array("name", "slug", "published", "ldesc", "image_default", "image_thumb", "image_zoom", "h1", "metatitle", "metakeywords", "metadescription", "metafacebook", "metagoogle", "metatwitter", "canonical", "head", "footer");
    protected $lang_slaggable = array("from" => "name", "saveTo" => "slug");
    protected $db_fields = array("position","robots", "ogp", "ogp_type", "ogp_image", "public_access","brand_id","category_id","category_mode", "page_id");
    protected $lang_model = 'Collection_Lang';

    protected $lang_fields_cloning = array(
        "name" => SingleModel::CLONE_UNIQUE_TEXT,
        "slug" => SingleModel::CLONE_UNIQUE_SLUG,
        "published" => 0
    );

    protected $defaults = [
        'robots' => 'global',
        'ogp' => '0',
        'ogp_type' => 'product',
        'public_access' => 'P',
    ];

    protected function getDefaults(){
        $defaults = $this->defaults;
        $defaults['position'] = Collection::max('position') + 1;
        return $defaults;
    }

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter() {
        return new CollectionPresenter($this);
    }

}

class CollectionPresenter extends Presenter {

    public function presentCreated() {
        return Format::datetime($this->created_at);
    }

}


/**
 * Collection_Lang
 *
 * @property integer $collection_id
 * @property string $lang_id
 * @property string $name
 * @property string $slug
 * @property boolean $published
 * @property string $sdesc
 * @property string $ldesc
 * @property string $h1
 * @property string $metatitle
 * @property string $metakeywords
 * @property string $metadescription
 * @property string $image_default
 * @property string $image_thumb
 * @property string $image_zoom
 * @property string $canonical
 * @property string $metafacebook
 * @property string $metatwitter
 * @property string $metagoogle
 * @property string $head
 * @property string $footer
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereCollectionId($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang wherePublished($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereSdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereLdesc($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereH1($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereMetatitle($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereMetakeywords($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereMetadescription($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereImageDefault($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereImageThumb($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereImageZoom($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereCanonical($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereMetafacebook($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereMetatwitter($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereMetagoogle($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereHead($value)
 * @method static \Illuminate\Database\Query\Builder|\Collection_Lang whereFooter($value)
 * @property-read \Collection $brand
 */
class Collection_Lang extends Eloquent  {


    protected $table = 'collections_lang';
    protected $guarded = array();
    public $timestamps = false;

    public function brand()
    {
        return $this->belongsTo('Collection');
    }


}