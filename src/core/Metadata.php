<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 27-nov-2013 12.34.36
 */
namespace Core;

class Metadata {

    private static $instance = null;
    private $data = null;
    private $index = null;
    private $attributes = null;

    /**
     * @return Metadata
     */
    public static function getInstance() {
        if (self::$instance == null) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }
    
    
    function __construct() {
        $this->data = array();
        $this->index = array();
        $this->attributes = array();
    }
    
    
    function set($key,$value){
        $this->data[$key] = $value;
    }
    
    function get($key){
        return isset($this->data[$key]) ? $this->data[$key] : null;
    }
    
    function index($value){
        $this->index[] = $value;
    }
    
    function attributes($value){
        $this->attributes[] = $value;
    }
    
    function setLang($lang,$key,$value){
        if(isset($this->data[$lang])){
            $this->data[$lang][$key] = $value;
        }else{
            $this->data[$lang] = array();
            $this->data[$lang][$key] = $value;
        }
    }
    
    function getLang($lang,$key){
        return isset($this->data[$lang][$key]) ? $this->data[$lang][$key] : null;
    }
    
    function encode(){
        return serialize($this->data);
    }
    
    function decode($data){
        return unserialize($data);
    }
    
    function update($model,$lang){
        $indexable = implode(" ", $this->index);
        $attributes = implode(", ", $this->attributes);
        $model->$lang()->update(["metadata" => $this->encode(), "indexable" => $indexable, "attributes" => $attributes]);
        $this->index = array();
        $this->data = array();
        $this->attributes = array();
    }
    
    function load($model){
        
        /*foreach($model->translations as $tr){
            $rel = $this->decode( $tr->metadata );
            $model->setAttribute("metadata_".$tr->lang_id , $rel);
        }*/

    }

}
