<?php

use services\Bluespirit\Negoziando\Fidelity\FidelityHelper;
use services\Bluespirit\Negoziando\Fidelity\FidelityRepository;
use services\Bluespirit\Negoziando\Fidelity\FidelityCustomer;
use services\FeatsManager;

/**
 * @return FidelityRepository
 */
function fidelityRepository()
{
    return app('services\Bluespirit\Negoziando\Fidelity\FidelityRepository');
}

/**
 * @param $attributes
 * @return FidelityCustomer
 */
function fidelityCustomer($attributes = [])
{
    return new FidelityCustomer($attributes);
}

/**
 * @param $value
 * @param string $message
 * @param null $method
 */
function audit($value, $message = '', $method = null)
{
    Utils::log($value, $message, $method);
}

/**
 * @param $bool
 * @param $value
 * @param string $message
 * @param null $method
 */
function auditBy($bool, $value, $message = '', $method = null)
{
    if ($bool)
        Utils::log($value, $message, $method);
}

/**
 * @param $value
 * @param string $message
 * @param null $method
 */
function audit_error($value, $message = '', $method = null)
{
    Utils::error($value, $message, $method);
}

/**
 * @param $value
 * @param string $message
 * @param null $method
 */
function audit_all($value, $message = '', $method = null)
{
    Utils::log($value, $message, $method);
    Utils::error($value, $message, $method);
}

/**
 * @param $value
 * @param string $message
 * @param null $method
 */
function audit_track($value, $message = '', $method = null)
{
    Utils::track($value, $message, $method);
}

/**
 * @param $value
 * @param string $message
 * @param Exception $exception
 */
function audit_remote($value, $message = '', Exception $exception = null)
{
    Utils::remote($value, $message, $exception);
}

/**
 * @param Exception $e
 * @param string $message
 * @param null $method
 */
function audit_exception(Exception $e, $message = '', $method = null)
{
    Utils::log($e->getMessage(), $message, $method);
    Utils::log($e->getTraceAsString(), $message, $method);
    Utils::error($e->getMessage(), $message, $method);
    Utils::error($e->getTraceAsString(), $message, $method);
}

/**
 * @param bool $mode
 */
function audit_watch($mode = true)
{
    if ($mode) {
        Utils::watch();
    } else {
        Utils::unwatch();
    }
}

/**
 * @param bool $return_string
 * @return string
 */
function audit_queries($return_string = true)
{
    $queries = Utils::getQueries();
    return $return_string ? implode(PHP_EOL, $queries) : $queries;
}

/**
 * @param $value
 * @param string $message
 * @param null $method
 */
function audit_report($value, $message = '', $method = null)
{
    Utils::report($value, $message, $method);
}

/**
 * @param $value
 * @param string $message
 * @param null $method
 */
function audit_cron($value, $message = '', $method = null)
{
    Utils::cron($value, $message, $method);
}

/**
 * @param $key
 * @param null $default
 * @return mixed
 */
function config($key, $default = null)
{
    return Config::get($key, $default);
}

/**
 * @param $namespace
 * @param $key
 * @param null $default
 * @return mixed
 */
function plugin($namespace, $key, $default = null)
{
    if (Config::get("plugins.$namespace", false) === true) {
        return Config::get("plugins_settings.$key", $default);
    }
    return $default;
}

/**
 * @return FidelityHelper
 */
function fidelityHelper()
{
    $helper = app('services\Bluespirit\Negoziando\Fidelity\FidelityHelper');
    return $helper;
}


if (!function_exists('mb_ucfirst')) {
    function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false)
    {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
        if ($lower_str_end) {
            $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        } else {
            $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }
}

/**
 * @return services\Repositories\FractalRepository
 */
function fractal()
{
    return app('services\Repositories\FractalRepository');
}

/**
 * @return FeatsManager;
 */
function feats()
{
    return Feats::getSelf();
}

function execInBackground($cmd)
{
    if (substr(php_uname(), 0, 7) == "Windows") {
        pclose(popen("start /B " . $cmd, "r"));
    } else {
        exec($cmd . " > /dev/null &");
    }
}

/**
 * @param array $items
 * @return \Illuminate\Support\Collection
 */
function collect($items = [])
{
    return new \Illuminate\Support\Collection($items);
}

/**
 * @param string $path
 * @param string $folder
 * @return string
 */
function blade_vendor_path($path = '', $folder = 'core')
{
    $path = base_path("vendor/bladecommmerce/$folder/src/$folder/$path");
    return $path;
}

if (!function_exists('class_uses_deep')) {
    /**
     * @param mixed $class
     *
     * @param bool $autoload
     * @return array
     */
    function class_uses_deep($class, $autoload = true)
    {
        $traits = [];
        do {
            $traits = array_merge(class_uses($class, $autoload), $traits);
        } while ($class = get_parent_class($class));
        foreach ($traits as $trait => $same) {
            $traits = array_merge(class_uses($trait, $autoload), $traits);
        }
        return array_unique($traits);
    }
}
