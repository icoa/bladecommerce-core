<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */
class EchoIs
{
    
    public static function email($value)
    {        
        return filter_var($value, FILTER_VALIDATE_EMAIL) !== false;        
    }
    
    public static function alpha($value)
    {        
        return preg_match('/^([a-z])+$/i', $value);
    }
    
    public static function alphaNum($value)
    {        
        return preg_match('/^([a-z0-9])+$/i', $value);
    }
    
    public static function alphaDash($value)
    {        
        return preg_match('/^([-a-z0-9_-])+$/i', $value);
    }
    
    public static function IP($value)
    {        
        return filter_var($value, FILTER_VALIDATE_IP) !== false;
    }
    
    public static function url($value)
    {        
        return filter_var($value, FILTER_VALIDATE_URL) !== false;
    }
    
    public static function urlActive($value)
    {        
        $url = str_replace(array('http://', 'https://', 'ftp://'), '', Str::lower($value));

	return (trim($url) !== '') ? checkdnsrr($url) : false;
    }
    
    public static function image($value){
        if($value == '' || $value == null)return false;
        $tokens = explode(".", $value);
        $extension = Str::lower( $tokens[count($tokens) - 1] );
        if(in_array($extension, array('jpg','png','gif','jpeg'))){
            return true;
        }
        return false;
    }

}