<?php

use Illuminate\Database\Query\Builder as QueryBuilder;

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 24/10/14
 * Time: 12.52
 */
class LinkResolver
{

    protected $data;
    protected $url;
    protected $anchor;
    protected $global_params;

    function __construct($content, $autosetup = true)
    {
        //\Utils::watch();
        $obj = (object)unserialize($content);
        $this->data = $obj;
        //\Utils::log($obj);
        if ($autosetup) {
            $this->render();
        }
    }

    function render()
    {
        $obj = $this->data;
        //\Utils::log($obj->rendering_type,"RENDERING TYPE");
        switch ($obj->rendering_type) {
            case 'link':
                $this->_render_link();
                break;
            case 'url':
                $this->_render_url();
                break;
            case 'auto':
                $this->_render_auto();
                break;
            case 'separator':
                $this->_render_separator();
                break;
        }
    }

    function setGlobalParams($params)
    {
        $this->global_params = (object)unserialize($params);
    }

    function getUrl()
    {
        return $this->url;
    }

    function getAnchor()
    {
        return $this->anchor;
    }

    static function url($content)
    {
        $instance = new static($content);
        return $instance->getUrl();
    }

    static function anchor($content)
    {
        $instance = new static($content);
        return $instance->getAnchor();
    }

    private function _render_auto()
    {
        $lang = \Core::getLang();
        //\Utils::log($this->data);
        $auto = (object)$this->data->auto;
        $attribute_id = false;
        $navtype_id = false;
        $target_type = $auto->target_type;

        $columns = ['id', 'name'];

        switch ($auto->target_type) {
            case 'brand':
            case 'category':
            case 'collection':
            case 'trend':
            case 'product':
            case 'page':
            case 'section':
            case 'nav':
                $model = $auto->target_type;
                break;

            case 'base':
                $navtype_id = [1];
                $model = 'nav';
                break;
            case 'general':
                $navtype_id = [2, 3, 4];
                $model = 'nav';
                $target_type = 'nav';
                break;
            case 'pricerange':
                $navtype_id = [5];
                $model = 'nav';
                $target_type = 'nav';
                break;

            default: //assuming it's attribute
                $attribute_id = \Attribute::where('code', $auto->target_type)->remember(60 * 12, "link-{$auto->target_type}-query-builder")->pluck('id');
                $model = 'AttributeOption';
                //$target_type = 'attribute';
                $columns = ['id', 'name', 'uname'];
                break;
        }

        if ($model == 'nav') {
            $columns = ['id', 'name', 'menuname'];
        }
        $model = ucfirst($model);
        /** @var QueryBuilder $query */
        $query = $model::rows($lang)->where('published', 1);
        if ($model === 'Brand') {
            $query->where('is_visible', 1);
        }
        if (is_array($auto->target_id)) {
            $target_ids = $auto->target_id;
        } else {
            $target_ids = ($auto->target_id === '') ? false : explode(",", $auto->target_id);
        }

        if ($navtype_id) {
            $query->whereIn('navtype_id', $navtype_id);
        }
        if ($attribute_id) {
            $query->where('attribute_id', $attribute_id);
        }

        switch ($auto->target_condition) {
            case 'all':

                break;
            case 'in':
                $query->whereIn('id', $target_ids);
                break;
            case 'out':
                $query->whereNotIn('id', $target_ids);
                break;
            case 'children':
                //$parent = ($target_type == 'section') ? "section_"
                $query->whereIn('parent_id', $target_ids);
                break;
        }

        switch ($auto->target_order) {
            case 'alpha':
                $query->orderBy("name");
                break;
            case 'position':
                $query->orderBy("position");
                break;
            case 'given':
                $list = (is_array($auto->target_id)) ? implode(",", $auto->target_id) : $auto->target_id;
                $query->orderBy(\DB::raw("FIND_IN_SET(id,'$list')"));
                break;
        }

        if ((int)$auto->target_limit > 0) {
            $query->take($auto->target_limit);
        }

        $items = $query->select($columns)->get();
        if (count($items) > 0) {
            $this->url = [];
            $this->anchor = [];
            foreach ($items as $item) {
                $name = $item->getPublicName();
                $link = \Link::create($target_type, $item->id, 'default', $this->data->open_mode);
                $this->applyModifiers($link);
                $this->applyFilters($link);
                $this->applyGlobalParams($link);
                $this->url[] = $link->getLink();
                $anchor = $link->getAnchor();
                $anchor->setValue($name);
                $anchor->setAttribute("title", e($name));
                $this->anchor[] = $anchor;
            }
        }
        //\Utils::log($this->url,"RESOLVED URL");
    }

    private function applyGlobalParams($link)
    {
        if (isset($this->global_params)) {
            //\Utils::log("PARSING GLOBAL PARAMS");
            $gp = $this->global_params;
            if (isset($gp->default)) {
                $default = (object)$gp->default;
                $link->addModifier($default->target_type, $default->target_id);
            }

            if (isset($gp->modifiers)) {
                $modifiers = (object)$gp->modifiers;
                $target_type = $modifiers->target_type;
                $target_id = $modifiers->target_id;
                foreach ($target_type as $index => $type) {
                    $id = $target_id[$index];
                    $link->addModifier($type, $id);
                }
            }

            if (isset($gp->filters)) {
                $filters = (object)$gp->filters;
                $target_type = $filters->target_type;
                $target_id = $filters->target_id;
                foreach ($target_type as $index => $type) {
                    $id = $target_id[$index];
                    $link->addFilter($type, $id);
                }
            }
        }
    }


    private function applyModifiers($link)
    {
        if (isset($this->data->modifiers)) {
            $modifiers = (object)$this->data->modifiers;
            $target_type = $modifiers->target_type;
            $target_id = $modifiers->target_id;
            foreach ($target_type as $index => $type) {
                $id = $target_id[$index];
                $link->addModifier($type, $id);
            }
        }
    }

    private function applyFilters($link)
    {
        $lang = \Core::getLang();
        if (isset($this->data->query) and isset($this->data->query[$lang])) {
            $querystring = $this->data->query[$lang];
            if ($querystring != '') {
                if ($querystring[0] == '#') {
                    $link->setAppend($querystring);
                } else {
                    parse_str($querystring, $tokens);
                    foreach ($tokens as $key => $value) {
                        $link->addFilter($key, $value);
                    }
                }
            }
        }

        if (isset($this->data->filters)) {
            $filters = (object)$this->data->filters;
            $target_type = $filters->target_type;
            $target_id = $filters->target_id;
            foreach ($target_type as $index => $type) {
                $id = $target_id[$index];
                $link->addFilter($type, $id);
            }
        }
    }

    private function _render_link()
    {
        $lang = \Core::getLang();
        /*$custom_url = $this->data->url[$lang];
        if(isset($custom_url) AND $custom_url != ''){
            $this->url = $custom_url;
            return;
        }*/
        if (isset($this->data->default)) {
            $default = (object)$this->data->default;
            //\Utils::log($default);
            $link = \Link::create($default->target_type, $default->target_id, 'default', $this->data->open_mode);
        }

        $this->applyModifiers($link);

        $this->applyFilters($link);

        $this->applyGlobalParams($link);

        $this->url = $link->getLink();
        //\Utils::log($this->url,"RESOLVED URL");
        $this->anchor = $link->getAnchor();
    }

    private function _render_url()
    {
        $lang = \Core::getLang();
        if (isset($this->data->url)) {
            $this->url = $this->data->url[$lang];
            $anchor = \HtmlObject\Element::create("a", $this->url, ['href' => e($this->url), 'target' => $this->data->open_mode]);
            $this->anchor = $anchor;
        }
    }

    private function _render_separator()
    {


        $this->url = "";
        $anchor = \HtmlObject\Element::create("a", '', ['href' => 'javascript:;']);
        $this->anchor = $anchor;

    }
}