<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-lug-2013 10.56.37
 */

class Items implements Iterator {

    private $position = 0, $array = array();

    public function __construct($items = array()) {
        $this->position = 0;
        $this->array = $items;
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->array[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->array[$this->position]);
    }

    public function length() {
        return count($this->array);
    }

}