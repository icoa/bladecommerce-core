<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 22-lug-2013 15.21.22
 */

use Carbon\Carbon;

abstract class Format
{

    static function datetime($value, $output = true)
    {
        if ($value == '' OR $value == null OR strtotime($value) <= 0) {
            return null;
        }
        $formatIn = ($output) ? "Y-m-d H:i:s" : "d-m-Y H:i:s";
        $formatOut = ($output) ? "d-m-Y H:i:s" : "Y-m-d H:i:s";
        $dt = Carbon::createFromFormat($formatIn, $value);
        return $dt->format($formatOut);
    }

    static function date($value, $output = true)
    {
        if ($value == '' OR $value == null OR strtotime($value) <= 0) {
            return "";
        }
        $formatIn = ($output) ? "Y-m-d" : "d-m-Y";
        $formatOut = ($output) ? "d-m-Y" : "Y-m-d";
        $time = strtotime($value);
        return date($formatOut, $time);
    }

    static function sqlDate($value)
    {
        if ($value == '' OR $value == null OR strtotime($value) <= 0) {
            return null;
        }
        $time = strtotime($value);
        return date("Y-m-d", $time);
    }

    static function sqlDatetime($value)
    {
        if ($value == '' OR $value == null OR strtotime($value) <= 0) {
            return null;
        }
        $time = strtotime($value);
        return date("Y-m-d H:i:s", $time);
    }

    static function now()
    {
        return date("Y-m-d H:i:s");
    }

    static function convert($value, $to, $from = 'default')
    {
        return \Core::convert($value, $to, $from);
    }

    static function money($value, $currency = 'default')
    {
        if ($value == '')
            return $value;
        if ($currency == 'default') {
            $currencyObj = \Core::getDefaultCurrency();
        } else {
            $currencyObj = \Currency::getObj($currency);
        }
        try {
            $value = round($value, $currencyObj->decimals);
            $money = number_format($value, $currencyObj->decimals, '.', '');
        } catch (\Exception $e) {
            $money = 0;
        }
        return $money;
    }

    static function weight($value, $unit = 'default')
    {
        if ($value == '')
            return $value;
        if ($unit == 'default') {
            $unitSign = \Cfg::get("WEIGHT_UNIT");
        } else {
            $unitSign = $unit;
        }
        $weight = number_format($value, 2);
        if (!is_null($unitSign)) $weight .= " " . $unitSign;
        return $weight;
    }

    static function sign($currency = 'default')
    {
        $defaultObj = \Core::getDefaultCurrency();
        if ($currency == 'default') {
            $currencyObj = $defaultObj;
        } else {
            $currencyObj = \Currency::getObj($currency);
        }
        return $currencyObj->sign;
    }

    static function currency($value, $withSign = false, $currency = 'default', $compatible = false, $convert = true)
    {
        if ($value === '')
            $value = 0;

        $registryKey = 'currency-default';
        if (Registry::has($registryKey)) {
            $defaultObj = Registry::get($registryKey);
        } else {
            $defaultObj = \Core::getDefaultCurrency();
            Registry::set($registryKey, $defaultObj);
        }

        $registryKey = 'selected-currency-' . $currency;
        if (Registry::has($registryKey)) {
            $currencyObj = Registry::get($registryKey);
        } else {
            if ($currency === 'default') {
                $currencyObj = \Core::getCurrencyObj();
            } else {
                $currencyObj = \Currency::getPublicObj($currency);
                if(null === $currencyObj){
                    $currencyObj = \Currency::getObj($currency);
                }
            }
            Registry::set($registryKey, $currencyObj);
        }

        //auto convert
        if ($currencyObj->id != $defaultObj->id and $convert) {
            $value = self::convert($value, $currencyObj->id, $defaultObj->id);
        }

        if ($compatible) {
            $currencyObj->dec_point = '.';
            $currencyObj->thousand_sep = '';
        }

        if ($currencyObj->thousand_sep === null) {
            $currencyObj->thousand_sep = '';
        }
        $value = round($value, $currencyObj->decimals);
        $money = number_format($value, $currencyObj->decimals, $currencyObj->dec_point, $currencyObj->thousand_sep);
        if ($withSign) {
            $money = $currencyObj->sign . ' ' . $money;
        }
        return $money;
    }

    static function currencyMin($value, $withSign = false, $currency = 'default', $compatible = false)
    {
        if ($value === '')
            $value = 0;

        $registryKey = 'selected-currency-' . $currency;
        if (Registry::has($registryKey)) {
            $currencyObj = Registry::get($registryKey);
        } else {
            if ($currency === 'default') {
                $currencyObj = \Core::getCurrencyObj();
            } else {
                $currencyObj = \Currency::getPublicObj($currency);
                if(null === $currencyObj){
                    $currencyObj = \Currency::getObj($currency);
                }
            }
            Registry::set($registryKey, $currencyObj);
        }

        if ($compatible) {
            $currencyObj->dec_point = '.';
            $currencyObj->thousand_sep = '';
        }

        if ($currencyObj->thousand_sep === null) {
            $currencyObj->thousand_sep = '';
        }
        $value = round($value, $currencyObj->decimals);
        $money = number_format($value, 0, $currencyObj->dec_point, $currencyObj->thousand_sep);
        if ($withSign) {
            $money = $money . ' ' . $currencyObj->sign;
        }
        return $money;
    }

    static function rateToPercent($rate, $withSign = false, $decimals = 0)
    {
        $v = ($rate == 0) ? 0 : (self::float($rate) - 1) * 100;
        return self::percentage($v, $withSign, $decimals);
    }

    static function ratio($big, $small)
    {
        //$big : 100 = $small : x
        $v = 100 * $small / $big;
        return self::percentage($v, true);
    }

    static function percentage($value, $withSign = false, $decimals = 0)
    {
        if ($value == '')
            $value = 0;

        $money = number_format($value, $decimals);

        /*$money = number_format($value, 2);
        list($whole, $decimal) = explode('.', $money);
        if((int)$decimal == 0){
            $money = $whole;
        }*/
        if ($withSign) {
            $money = $money . " %";
        }
        return $money;
    }

    public static function bytes($bytes, $force_unit = NULL, $format = NULL, $si = TRUE)
    {
        // Format string
        $format = ($format === NULL) ? '%01.2f %s' : (string)$format;

        // IEC prefixes (binary)
        if ($si == FALSE OR strpos($force_unit, 'i') !== FALSE) {
            $units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
            $mod = 1024;
        } // SI prefixes (decimal)
        else {
            $units = array('B', 'kB', 'MB', 'GB', 'TB', 'PB');
            $mod = 1000;
        }

        // Determine unit to use
        if (($power = array_search((string)$force_unit, $units)) === FALSE) {
            $power = ($bytes > 0) ? floor(log($bytes, $mod)) : 0;
        }

        return sprintf($format, $bytes / pow($mod, $power), $units[$power]);
    }

    public static function float($value, $decimals = 2)
    {
        $value = round($value, $decimals);
        $value = number_format($value, $decimals, ".", "");
        return $value;
    }

    public static function floatCsv($value, $decimals = 2)
    {
        $value = round($value, $decimals);
        $value = number_format($value, $decimals, ",", ".");
        return $value;
    }

    public static function line($str)
    {
        return str_replace(array("\r", "\n"), "", $str);
    }

    static function lang_date($value, $mode = 'lite', $lang = 'default')
    {
        if ($value == '' OR $value == null OR strtotime($value) <= 0) {
            return null;
        }
        if ($lang == 'default') {
            $lang = \Core::getLang();
        }
        $langObj = Language::getPublicObj($lang);
        $time = strtotime($value);
        return date($langObj->{"date_format_$mode"}, $time);
    }

    static function secure_key()
    {
        $plain_string = time();
        $plain_string .= Str::random();
        $plain_string .= rand(0, 9999);
        return md5($plain_string);
    }
}