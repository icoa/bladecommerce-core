<?php
use Illuminate\Console\Command;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-lug-2013 10.52.13
 */

class Registry
{

    private static $data = array();

    public static function get($key, $default = null)
    {
        if (isset(static::$data[$key])) {
            return static::$data[$key];
        }

        return $default;
    }

    public static function prop($object, $key, $default = null)
    {
        if ($obj = static::get($object)) {
            return $obj->{$key};
        }

        return $default;
    }

    public static function set($key, $value)
    {
        static::$data[$key] = $value;
    }

    public static function setReference($key, &$value)
    {
        static::$data[$key] = $value;
    }

    public static function has($key)
    {
        return isset(static::$data[$key]);
    }

    public static function getObj($key, $default = null)
    {
        if (isset(static::$data[$key])) {
            return static::$data[$key];
        }
        return $default;
    }

    public static function setObj($key, $value)
    {
        static::$data[$key] = $value;
    }

    public static function hasObj($key)
    {
        if (isset(static::$data[$key])) {
            return true;
        }
    }

    public static function delObj($key)
    {
        unset(static::$data[$key]);
    }

    /**
     * @return Command|null
     */
    public static function console()
    {
        return self::get('console');
    }

    /**
     * @param $key
     * @param Closure $callback
     * @return mixed|null
     */
    public static function remember($key, Closure $callback)
    {
        if (self::has($key))
            return self::get($key);

        $value = $callback();
        self::set($key, $value);
        return $value;
    }

    /**
     * @param $key
     * @return int
     */
    public static function increment($key)
    {
        if (self::has($key)) {
            $value = (int)self::get($key);
            $value++;
            self::set($key, $value);
            return $value;
        }
        $value = 1;
        self::set($key, $value);
        return $value;
    }
}