<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 20-nov-2013 14.29.38
 */


use Core\Admin\AdminForm;

class AttributeHtml
{

    private $obj = null;
    private $value = '';
    private $model = null;
    private $lang = 'default';
    private $attributes;

    function __construct($id, $lang = 'default')
    {

        $lang = ($lang == 'default') ? \Core::getLang() : $lang;
        $this->lang = $lang;
        //$this->model = \Attribute::find($id);
        $this->obj = \Attribute::getObj($id, $lang);
    }

    function setValue($value)
    {
        $this->value = $value;
    }

    function setDefaultValue($value)
    {
        $this->value = \Helper::parseDefaultValue($value);
    }

    function setProductValue($product_id)
    {
        $rows = is_null($this->attributes) ? DB::table("products_attributes")->where("product_id", $product_id)->where("attribute_id", $this->obj->id)->get() : $this->attributes;
        //\Utils::log($rows,"setProductValue($product_id)");
        if (count($rows) == 0) return;
        if (count($rows) == 1) {
            if ($this->obj->frontend_input == 'multiselect') {
                $this->setValue(array($rows[0]->attribute_val));
            } else {
                $this->setValue($rows[0]->attribute_val);
            }

        } else {
            if ($this->obj->frontend_input === 'text' || $this->obj->frontend_input === 'boolean') {
                $this->setValue($rows[0]->attribute_val);
                $this->attributes = $rows;
                return;
            }
            $data = array();
            foreach ($rows as $row) {
                if ($row->lang_id != null) {
                    if (!isset($data[$row->lang_id])) {
                        $data[$row->lang_id] = array();
                    }
                    $data[$row->lang_id][] = $row->attribute_val;
                } else {
                    $data[] = $row->attribute_val;
                }
            }
            $this->setValue($data);
            //\Utils::log($data, "setProductValue($product_id,{$this->obj->id})");
        }
        $this->attributes = $rows;
    }

    function setOldValue()
    {
        $name = "attr_" . $this->obj->code;
        $this->value = \Input::old($name);
    }

    function getRow()
    {
        $set = $this->getRenders();

        $row = <<<ROW
<tr id="attribute-row-{$set['id']}" rel="{$set['id']}">
    <td class="dragHandle"></td>
    <td class="align-right">
        {$set['labelHtml']}
    </td>
    <td class="center">       
        {$set['html']}
    </td>
</tr>                
ROW;
        return $row;
    }

    function getPreviewRenders()
    {
        $method = "preview_" . $this->obj->frontend_input;
        if (method_exists($this, $method)) {
            //call_user_method($method, $this);
            $this->{$method}();
        }
        $labelHtml = "<label class='align-right' for=\"{$this->obj->code}\"><b>{$this->obj->name}</b> <span class='disabled'>({$this->obj->frontend_input})</span></label>";
        /*if ($this->obj->ldesc) {
            $labelHtml .= "<span class=\"help-block\">{$this->obj->ldesc}</span>";
        }*/
        if ($this->obj->txt_suffix) {
            $this->preview .= " " . $this->obj->txt_suffix;
        }
        if ($this->obj->txt_prefix) {
            $this->preview = $this->obj->txt_prefix . " " . $this->preview;
        }
        $result = array(
            'preview' => $this->preview,
            'label' => $this->obj->name,
            'labelHtml' => $labelHtml,
            'code' => $this->obj->code,
            'id' => $this->obj->id,
        );

        return $result;
    }

    function getFrontRenders()
    {
        $method = "preview_" . $this->obj->frontend_input;
        if (method_exists($this, $method)) {
            //call_user_method($method, $this);
            $this->{$method}();
        }

        if ($this->obj->txt_suffix) {
            $this->preview .= " " . $this->obj->txt_suffix;
        }
        if ($this->obj->txt_prefix) {
            $this->preview = $this->obj->txt_prefix . " " . $this->preview;
        }

        $result = array(
            'preview' => $this->preview,
            'label' => $this->obj->name,
            'tooltip_label' => $this->obj->tooltip_label,
            'tooltip_value' => $this->obj->tooltip_value,
            'code' => $this->obj->code,
            'id' => $this->obj->id,
        );

        return $result;
    }

    function getRenders()
    {
        $method = "render_" . $this->obj->frontend_input;
        if (method_exists($this, $method)) {
            //call_user_method($method, $this);
            $this->{$method}();
        }

        $labelHtml = "<label class='align-right' for=\"{$this->obj->code}\"><b>{$this->obj->name}</b></label>";

        if ($this->obj->txt_suffix) {
            $this->html->append($this->obj->txt_suffix);
        }
        if ($this->obj->txt_prefix) {
            $this->html->prepend($this->obj->txt_prefix);
        }
        if ($this->obj->ldesc) {
            $labelHtml .= "<span class=\"help-block\">{$this->obj->ldesc}</span>";
        }

        $remove_url = \URL::action("ProductsController@postRemoveAttribute", array($this->obj->id));
        $add_option_url = \URL::action("AttributesController@postAddOption", array($this->obj->id));
        $e_name = e($this->obj->name);

        $actions = <<<ACTIONS
<ul class="table-controls type_{$this->obj->frontend_input}">               
    <li><a tabindex="-1" href="javascript:;" onclick="Products.addOption('$add_option_url','{$this->obj->code}','$e_name');" class="btn addOption hovertip" data-placement="top" title="Aggiungi un'opzione"><i class="icon-plus"></i></a></li>
    <li><a tabindex="-1" href="javascript:;" onclick="Products.removeAttribute({$this->obj->id});" class="btn hovertip removeAttribute" data-placement="top" title="Elimina questo attributo"><i class="icon-remove"></i></a></li>
</ul>        
ACTIONS;

        $this->html->after($actions);

        $result = array(
            'html' => $this->html,
            'label' => $this->obj->name,
            'labelHtml' => $labelHtml,
            'code' => $this->obj->code,
            'id' => $this->obj->id,
        );

        return $result;
    }

    function getMeta()
    {
        $meta = new \stdClass;
        $meta->id = $this->obj->id;
        $meta->code = $this->obj->code;
        $meta->label = $this->obj->name;

        $meta->option_id = 0;
        /*$meta->tooltip_label = $this->obj->tooltip_label;
        $meta->tooltip_value = $this->obj->tooltip_value;*/
        $value = $this->value;

        $isNull = false;
        if ($value == '' OR $value == null OR $value == 0) {
            $isNull = true;
        }
        if (is_array($value) AND (count($value) == 0 OR $value[0] == "")) {
            $isNull = true;
        }

        $metavalue = '';
        $structure = array();


        if ($this->obj->is_unique == 0) {
            if ($this->obj->frontend_input == 'multiselect') {
                $value = $value[$this->lang];
            } else {
                $value = $value[$this->lang][0];
            }
        }

        if ($this->obj->frontend_input == 'multiselect') {
            $names = array();
            foreach ($value as $option_id) {
                if ($option_id > 0) {
                    $tmp = new \stdClass();
                    $tmp->option_id = $option_id;
                    $meta->option_id = $option_id;
                    $option = AttributeOption::getObj($option_id, $this->lang);
                    if ($option->name != '') {
                        $tmp->name = $option->name;
                        $tmp->slug = $option->slug;
                    } else {
                        $tmp->name = $option->uname;
                        $tmp->slug = $option->uslug;
                    }
                    $structure[] = $tmp;
                    $names[] = $tmp->name;
                }

            }
            $metavalue = implode(", ", $names);
        }

        //\Utils::log($value, "Analyzing value form attribute {$this->obj->id}");

        if ($this->obj->frontend_input == 'select') {

            if ($value > 0) {
                $tmp = new \stdClass();
                $tmp->option_id = $value;
                $meta->option_id = $value;
                $option = AttributeOption::getObj($value, $this->lang);
                if ($option->name != '') {
                    $tmp->name = $option->name;
                    $tmp->slug = $option->slug;
                } else {
                    $tmp->name = $option->uname;
                    $tmp->slug = $option->uslug;
                }
                $structure[] = $tmp;
                $metavalue = $tmp->name;
            }

        }

        if ($this->obj->frontend_input == 'text') {
            $metavalue = $value;
        }

        if ($this->obj->frontend_input == 'boolean') {
            $metavalue = ($value == 1) ? trans('template.yes') : trans('template.no');
        }


        if ($this->obj->txt_suffix) {
            $metavalue = $metavalue . " " . $this->obj->txt_suffix;
        }
        if ($this->obj->txt_prefix) {
            $metavalue = $this->obj->txt_prefix . " " . $metavalue;
        }

        $meta->value = $metavalue;
        $meta->raw_value = $metavalue;
        $meta->structure = $structure;

        return $meta;

    }

    function getValue($default = '')
    {
        if ($this->value == '') {
            return $default;
        }
        return $this->value;
    }

    private function getClass($custom)
    {
        $class = '';
        $validates = array();
        if ($this->obj->is_required) {
            $validates[] = 'required';
        }
        if ($this->obj->frontend_class) {
            $validates[] = $this->obj->frontend_class;
        }
        if (count($validates) > 0) {
            $c = 0;
            foreach ($validates as $v) {
                if ($v == 'required') {
                    $class .= 'validate[required';
                } else {
                    if ($c > 0) {
                        $class .= ",custom[$v]";
                    } else {
                        $class .= "validate[custom[$v]";
                    }
                }
                $c++;
            }
            $class .= "]";
        }
        $class .= ' attribute ' . $custom;
        //$class .= ($this->obj->frontend_class) ? ' ' . $this->obj->frontend_class : '';
        //$class .= ($this->obj->is_required) ? ' required' : '';
        $class .= ($this->obj->is_unique) ? ' unique' : '';
        return trim($class);
    }

    private function render_text()
    {
        $value = $this->getValue($this->obj->default_value_text);
        $name = "attr_" . $this->obj->code;
        $attributes = ['class' => $this->getClass("text")];

        $af = $this->getAdminForm($value, $name);

        $node = ($this->obj->is_unique == 0) ? $af->lang_text($name, $attributes) : $af->text($name, $attributes);

        $this->html = $node;
    }


    private function preview_text()
    {
        $value = $this->getValue($this->obj->default_value_text);
        $this->preview = $value;
    }


    private function preview_boolean()
    {
        $value = $this->getValue($this->obj->default_value_yesno);
        $this->preview = ($value == 1) ? 'Sì' : 'No';
    }


    private function preview_select()
    {
        $options = $this->obj->options();
        $defaults = array();
        foreach ($options as $opt) {
            if ($opt->is_default) {
                $defaults[] = $opt->id;
            }
        }
        $data = \EchoArray::set($options)->keyIndex('id', 'name')->get();

        $value = $this->getValue($defaults);
        $tokens = [];

        if (is_array($value)) {
            //$value = implode(",",$value);
            foreach ($data as $id => $name) {
                if (in_array($id, $value)) $tokens[] = $name;
            }
            $value = implode(", ", $tokens);
        } else {
            $label = '';
            foreach ($data as $id => $name) {
                if ($id == $value) $label = $name;
            }
            $value = $label;
        }
        $this->preview = $value;
    }


    private function preview_multiselect()
    {
        $options = $this->obj->options();
        $defaults = array();
        foreach ($options as $opt) {
            if ($opt->is_default) {
                $defaults[] = $opt->id;
            }
        }
        $value = $this->getValue($defaults);
        $data = \EchoArray::set($options)->keyIndex('id', 'name')->keysForcedSort($value)->get();
        $data = \EchoArray::set($options)->keyIndex('id', 'name')->get();

        $tokens = [];
        if (is_array($value)) {
            //$value = implode(",",$value);
            foreach ($data as $id => $name) {
                if (in_array($id, $value)) $tokens[] = $name;
            }
            $value = implode(", ", $tokens);
        } else {
            $label = '';
            foreach ($data as $id => $name) {
                if ($id == $value) $label = $name;
            }
            $value = $label;
        }
        $this->preview = $value;
    }


    private function getAdminForm($value, $name, $type = 'single')
    {
        $af = new AdminForm();
        if ($this->obj->is_unique == 0) {
            $data = array();
            $languages = \Mainframe::languagesCodes();
            $value = $this->value;
            foreach ($languages as $lang_id) {
                if (isset($value[$lang_id])) {
                    $data[$name . "_" . $lang_id] = ($type == 'single') ? $value[$lang_id][0] : $value[$lang_id];
                }
            }
            $af->populate($data);
        } else {
            $af->populate([$name => $value]);
        }
        return $af;
    }

    private function render_select()
    {
        $options = $this->obj->options();
        $defaults = array();
        foreach ($options as $opt) {
            if ($opt->is_default) {
                $defaults[] = $opt->id;
            }
        }
        $data = array('' => 'Scegli...') + \EchoArray::set($options)->keyIndex('id', 'name')->get();

        $value = $this->getValue($defaults);
        $name = "attr_" . $this->obj->code;
        $attributes = ['class' => $this->getClass("selectSimple")];

        $af = $this->getAdminForm($value, $name);

        $node = ($this->obj->is_unique == 0) ? $af->lang_select($name, $data, $attributes) : $af->select($name, $data, $attributes);

        $this->html = $node;

        //$this->html = \Form::select($this->obj->code, $data,  $this->getValue($defaults), ['class' => $this->getClass("selectSimple"), 'autocomplete' => 'off', 'id' => $this->obj->code]);
    }

    private function render_multiselect()
    {
        $options = $this->obj->options();
        $defaults = array();
        foreach ($options as $opt) {
            if ($opt->is_default) {
                $defaults[] = $opt->id;
            }
        }
        $value = $this->getValue($defaults);
        $data = \EchoArray::set($options)->keyIndex('id', 'name')->keysForcedSort($value)->get();

        $name = "attr_" . $this->obj->code;
        $attributes = ['class' => $this->getClass("selectMultiple")];

        $af = $this->getAdminForm($value, $name, 'multi');

        $node = ($this->obj->is_unique == 0) ? $af->lang_selectMulti($name, $data, $attributes) : $af->selectMulti($name, $data, $attributes);

        $this->html = $node;

        //$this->html = \Form::select($this->obj->code."_ids", $data, $this->getValue($defaults), ['class' => $this->getClass("selectMultiple"), 'multiple' => 'multiple', 'autocomplete' => 'off', 'id' => $this->obj->code]);
    }


    private function render_boolean()
    {
        $value = $this->getValue($this->obj->default_value_yesno);
        $name = "attr_" . $this->obj->code;
        $attributes = ['class' => 'boolean'];

        $af = $this->getAdminForm($value, $name);

        $node = ($this->obj->is_unique == 0) ? $af->lang_onOff($name, 1, $attributes) : $af->onOff($name, 1, $attributes);

        $this->html = $node;
    }

}
