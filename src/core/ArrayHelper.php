<?php

namespace Core;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 11.33.03
 */

use Config,
    Log,
    DB;

class ArrayHelper {

    protected $array = [];

    function set($array) {
        $this->array = $array;
        return $this;
    }

    function keyIndex($key, $value = false) {
        $data = [];
        $array = $this->get();
        foreach ($array as $row) {
            $data[$row->{$key}] = ($value) ? $row->{$value} : $row;
        }
        $this->set($data);
        return $this;
    }
    
    function keys($key) {
        $data = [];
        $array = $this->get();
        foreach ($array as $row) {
            $data[] = $row->{$key};
        }
        $this->set($data);
        return $this;
    }
    
    function keysForcedSort($keys) {
        if(is_array($keys) and count($keys) > 0){
            $data = [];
            $array = $this->get();
            foreach($keys as $key){
                if(isset($array[$key])){
                    $val = $array[$key];
                    unset($array[$key]);
                    $data[$key] = $val;
                }
            }
            
            $data = $data + $array;            
            $this->set($data);
        }
        return $this;
    }

    function get() {
        return $this->array;
    }

    function extend(array $b) {
        $a = $this->get();
        foreach ($b as $k => $v)
            $a[$k] = is_array($v) && isset($a[$k]) ?
                    array_extend(is_array($a[$k]) ?
                                    $a[$k] : array(), $v) :
                    $v;
        $this->set($a);
        return $this;
    }

    /**
     *
     * Transforms all objects of an array by a given function and puts them in an array
     * @param mixed $array The array with the object to transform
     * @param function $function The function which transforms the given objects
     * @param $keyFunction Function is called with the current element. If provided an associative arrays is created with the return value of the function as key. If not provided a normal arrays is created.
     * @return mixed
     */
    public function transform($array, $function, $keyFunction = null) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }
        $result = array();
        foreach ($array as $toTransform) {
            if ($keyFunction != null) {
                $key = call_user_func($keyFunction, $toTransform);
                $result[$key] = call_user_func($function, $toTransform);
            } else {
                array_push($result, call_user_func($function, $toTransform));
            }
        }
        return $result;
    }

    /**
     *
     * Get the first matching element or null if no match.
     *
     * Function needs the signature of function($item) where $item is the current element int the list.
     * If you provide additionalParameters the signature needs to be function($item, $params)
     *
     * @param mixed $array 
     * @param Predicate $predicate Method must return true or false!!! If this method returns true this is the found element.
     * @param $additionalParameters Additional parameters. Will be set to call in the callback. If not they will not be attached.
     * @throws Exception If the array is not an array
     * @return The first found element or null
     */
    public function first($array, $predicate, $additionalParameters = null) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }

        $result = null;

        foreach ($array as $toCheck) {
            if ($additionalParameters == null) {
                $userResult = call_user_func($predicate, $toCheck);
            } else {
                $userResult = call_user_func($predicate, $toCheck, $additionalParameters);
            }
            if ($userResult) {
                $result = $toCheck;
            }
        }

        return $result;
    }

    /**
     * Delete elements of an array. 
     * If the given predicate returns true, this element is deleted from the array
     * 
     * @param Array $array The array to delete. Will be given to this method as reference.
     * @param function $predicate The predicate which defines what has to be deleted. If it returns true the element will be delted.
     * @param mixed $additionalParameters Additional parameters. Will be set to call in the callback. If not they will not be attached.
     * @throws Exception If the array is not an array
     * @return The array without the deleted elements
     */
    public function delete(&$array, $predicate, $additionalParameters = null) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }

        $result = & $array;
        $toUnset = array();

        $i = 0;
        foreach ($array as $toCheck) {
            if ($additionalParameters == null) {
                $userResult = call_user_func($predicate, $toCheck);
            } else {
                $userResult = call_user_func($predicate, $toCheck, $additionalParameters);
            }
            if ($userResult) {
                array_push($toUnset, $i);
            }
            $i++;
        }

        foreach ($toUnset as $t) {
            unset($result[$t]);
        }
        return $result;
    }

    /**
     *
     * Cheks if the given predicate matches any of the array. If one matches, will return true. If non matches => false
     *
     * Function needs the signature of function($item) where $item is the current element int the list.
     * If you provide additionalParameters the signature needs to be function($item, $params)
     *
     * @param mixed $array
     * @param Predicate $predicate function must return true or false!
     * @param mixed $additionalParameters Additional parameters
     * @return Boolean  If one (or more) matches, will return true. If non matches => false
     */
    public function any($array, $predicate, $additionalParameters = null) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }


        foreach ($array as $toCheck) {
            if ($additionalParameters == null) {
                $userResult = call_user_func($predicate, $toCheck);
                //true => get out
                if ($userResult) {
                    return true;
                }
            } else {
                $userResult = call_user_func($predicate, $toCheck, $additionalParameters);
                //true => get out
                if ($userResult) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * Get the index of an item where the predicate applies first
     * @param mixed $array
     * @param Predicate $predicate
     * @param mixed $additionalParameters
     * @return Integer The index or -1 if not found
     */
    public function indexOf($array, $predicate, $additionalParameters = null) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }

        $currentIndex = 0;

        foreach ($array as $item) {
            if ($additionalParameters == null) {
                $userResult = call_user_func($predicate, $item);
                //true => get out
                if ($userResult) {
                    return $currentIndex;
                }
            } else {
                $userResult = call_user_func($predicate, $item, $additionalParameters);
                //true => get out
                if ($userResult) {
                    return $currentIndex;
                }
            }
            $currentIndex++;
        }


        return -1;
    }

    /**
     *
     * Get a value of an array if the key exisits. If not return the given default value
     * @param mixed $array
     * @param String $key
     * @return The found key or null if not found
     */
    public function getIfKeyExists(&$array, $key) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }

        if (array_key_exists($key, $array)) {
            return $array[$key];
        }
        return null;
    }

    /**
     *
     * Filter an array by the given predicate
     *
     * Function needs the signature of function($item) where $item is the current element int the list.
     * If you provide additionalParameters the signature needs to be function($item, $params)
     *
     * @param mixed $array
     * @param Predicate $predicate function must return true or false!
     * @param mixed $additionalParameters Additional parameters
     * @return mixed Array of the items the predicate applies to
     */
    public function filter($array, $predicate, $additionalParameters = null) {
        if (!is_array($array)) {
            throw new Exception("\$array is not an array");
        }
        $result = array();

        foreach ($array as $item) {
            if ($additionalParameters == null) {
                $userResult = call_user_func($predicate, $item);
                if ($userResult) {
                    array_push($result, $item);
                }
            } else {
                $userResult = call_user_func($predicate, $item, $additionalParameters);
                if ($userResult) {
                    array_push($result, $item);
                }
            }
        }
        return $result;
    }

}

?>