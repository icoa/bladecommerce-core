<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\ArrayHelper;

class ArrayHelperServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echoarrayhelper'] = $this->app->share(function ($app) {
            return new ArrayHelper;
        });
    }

}