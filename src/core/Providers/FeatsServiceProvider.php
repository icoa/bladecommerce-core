<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use services\FeatsManager;

class FeatsServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echofeats'] = $this->app->share(function ($app) {
            return new FeatsManager;
        });
    }

}