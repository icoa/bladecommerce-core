<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Utils;

class UtilsServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echoutils'] = $this->app->share(function ($app) {
            return new Utils;
        });
    }

}