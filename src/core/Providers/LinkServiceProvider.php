<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Link;

class LinkServiceProvider extends ServiceProvider {


  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echolink'] = $this->app->share(function($app)
        {
            return new Link;
        });
    }

}