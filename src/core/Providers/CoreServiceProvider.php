<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Core;

class CoreServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echocore'] = $this->app->share(function ($app) {
            return new Core;
        });
    }

}