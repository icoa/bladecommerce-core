<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Mainframe;

class MainframeServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['echomainframe'] = $this->app->share(function ($app) {
            return new Mainframe;
        });
    }

}