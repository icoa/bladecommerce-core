<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Cfg;

class CfgServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app->singleton('echocfg', function ($app) {
            return new Cfg;
        });
    }

}