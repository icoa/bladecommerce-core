<?php

namespace Core\Providers;

use Illuminate\Support\ServiceProvider;
use Core\Admin\AdminTemplate;

class AdminTemplateServiceProvider extends ServiceProvider
{


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // Register 'underlyingclass' instance container to our UnderlyingClass object
        $this->app['admintemplate'] = $this->app->share(function ($app) {
            return new AdminTemplate;
        });

    }

}