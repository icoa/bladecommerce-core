<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 11/05/2015
 * Time: 15:44
 */

namespace Core\Condition;

class LocalDebug
{

    protected $localDebug = false;

    /**
     * @param bool $enabled
     * @return $this
     */
    function setDebug($enabled = true)
    {
        $this->localDebug = $enabled;
        return $this;
    }

    function log($obj, $pre = null, $before = null)
    {
        if ($this->localDebug === true) {
            if(\App::environment() === 'live'){
                \Utils::track($obj, $pre);
            }else{
                \Utils::log($obj, $pre, $before);
            }
        }
    }

    function error($obj, $pre = null, $before = null)
    {
        \Utils::error($obj, $pre, $before);
    }
}