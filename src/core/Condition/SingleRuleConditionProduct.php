<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionProduct extends SingleRuleCondition
{

    protected $code = "SRCP";
    protected $fields = ['value', 'operator', 'attribute'];
    protected $localDebug = false;

    function solve()
    {
        $method_name = "_condition_" . $this->attribute;
        $this->log(__METHOD__);
        $this->product->expandIf();

        if (method_exists($this, $method_name)) {
            $this->log($method_name, "CALLING METHOD");
            call_user_func_array(array($this, $method_name), [null]);
        } else {
            $this->log($method_name, "SingleRuleConditionProduct|METHOD DOES NOT EXIST");
            $this->log("_condition_attribute_ids", "CALLING METHOD");
            call_user_func_array(array($this, "_condition_attribute_ids"), [null]);
        }
    }

    private function _condition_category_ids()
    {
        $value = $this->value;
        $ids = $this->product->category_ids;
        $main_id = $this->product->main_category_id;

        $this->log($ids, __METHOD__);
        $this->log("MAIN ID: $main_id | OPERATOR: $this->operator | VALUE: $value", __METHOD__);
        $assert = false;
        switch ($this->operator) {
            case '==':
                $assert = ($main_id == $value OR in_array($value, $ids));
                break;

            case '!=':
                $assert = ($main_id != $value AND !in_array($value, $ids));
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($main_id == $v OR in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($main_id == $v OR in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($main_id == $v OR in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter > 0);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($main_id == $v OR in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '+=': // qualsiasi
                $assert = ($main_id > 0 OR count($ids) > 0);
                break;

            case '-=': // nessuno
                $assert = ((int)$main_id <= 0 AND count($ids) == 0);
                break;
        }
        $this->log("ASSERT: " . ($assert ? 'TRUE' : 'FALSE'), __METHOD__);
        $this->setAssert($assert);
    }

    private function _condition_brand_ids()
    {
        $value = $this->value;
        $pivot = $this->product->brand_id;
        $assert = false;
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;

            case '+=': // qualsiasi
                $assert = ($pivot > 0);
                break;

            case '-=': // nessuno
                $assert = ((int)$pivot <= 0);
                break;
        }
        $this->log("ASSERT: " . ($assert ? 'TRUE' : 'FALSE'), __METHOD__);
        $this->setAssert($assert);
    }

    private function _condition_collection_ids()
    {
        $value = $this->value;
        $pivot = $this->product->collection_id;
        $assert = false;
        $this->log("PIVOT: $pivot | OPERATOR: $this->operator | VALUE: $value", __METHOD__);
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;

            case '+=': // qualsiasi
                $assert = ((int)$pivot > 0);
                break;

            case '-=': // nessuno
                $assert = ((int)$pivot <= 0);
                break;
        }
        $this->log("ASSERT: " . ($assert ? 'TRUE' : 'FALSE'), __METHOD__);
        $this->setAssert($assert);
    }

    private function _condition_supplier_ids()
    {
        $value = $this->value;
        $ids = $this->product->supplier_ids;
        $assert = false;
        switch ($this->operator) {
            case '==':
                $assert = in_array($value, $ids);
                break;

            case '!=':
                $assert = !in_array($value, $ids);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter > 0);
                break;

            case '!()': // non è almeno uno tra
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '+=': // qualsiasi
                $assert = (count($ids) > 0);
                break;

            case '-=': // nessuno
                $assert = (count($ids) == 0);
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_attribute_ids()
    {
        //$attribute = $this->attribute;
        $value = $this->value;
        //$obj = \Attribute::where("code", $attribute)->first();
        //$attribute_id = $obj->id;

        $product_options = $this->product->attribute_option_ids;
        $product_attributes = $this->product->attribute_ids;
        //if we have a selected combination, then add options and attributes
        if (isset($this->product->combination)) {
            $master_product_attributes = $product_attributes;
            $master_product_options = $product_options;
            $combination_id = $this->product->combination->id;
            $combination_options = \DB::table('products_combinations_attributes')->where('combination_id', $combination_id)->lists('option_id');
            $combination_attributes = \DB::table('products_combinations_attributes')->where('combination_id', $combination_id)->lists('attribute_id');
            $this->log($master_product_attributes, __METHOD__, 'MASTER_PRODUCT_ATTRIBUTES');
            $this->log($master_product_options, __METHOD__, 'MASTER_PRODUCT_OPTIONS');
            $this->log($combination_attributes, __METHOD__, 'COMBINATION_PRODUCT_ATTRIBUTES');
            $this->log($combination_options, __METHOD__, 'COMBINATION_PRODUCT_OPTIONS');
            $correct_attributes = array_unique(array_merge($combination_attributes, $master_product_attributes));
            $correct_options = array_unique(array_merge($combination_options, $master_product_options));
            $this->log(compact('correct_attributes', 'correct_options'));
            $product_options = $correct_options;
            $product_attributes = $correct_attributes;
        }
        $assert = false;
        $attribute_id = null;
        if (isset($this->rule->attribute)) {
            $attribute_id = \Attribute::where('code', $this->rule->attribute)->remember(60, 'attribute_code_' . $this->rule->attribute)->pluck('id');
            $this->log("ATTRIBUTE_ID: $attribute_id", __METHOD__);
        }
        switch ($this->operator) {
            case '==': // è almeno uno tra
                $assert = in_array($value, $product_options);
                break;

            case '!=': // non è almeno uno tra
                $assert = !in_array($value, $product_options);
                break;

            case '+=': // qualsiasi
                $assert = (($attribute_id > 0) AND in_array($attribute_id, $product_attributes));
                break;

            case '-=': // nessuno
                $assert = (($attribute_id > 0) AND !in_array($attribute_id, $product_attributes));
                break;
        }
        $this->log("VALUE: $value", __METHOD__);
        $this->log("ATTR: " . $this->rule->attribute, __METHOD__);
        $this->log("ASSERT: " . ($assert ? 'TRUE' : 'FALSE'), __METHOD__);
        $this->log($product_attributes, "product_attributes");
        $this->log($product_options, "product_options");
        $this->log($this->product, "MASTER_PRODUCT");
        $this->setAssert($assert);
    }

    private function _condition_trend_ids()
    {
        $value = $this->value;
        $ids = $this->product->trend_ids;
        $assert = false;
        switch ($this->operator) {
            case '==':
                $assert = in_array($value, $ids);
                break;

            case '!=':
                $assert = !in_array($value, $ids);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter > 0);
                break;

            case '!()': // non è almeno uno tra
                $value = is_array($value) ? $value : explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if (in_array($v, $ids)) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '+=': // qualsiasi
                $assert = (count($ids) > 0);
                break;

            case '-=': // nessuno
                $assert = (count($ids) == 0);
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_price()
    {
        $value = (float)$this->value;
        $pivot = (float)$this->product->buy_price;
        $assert = false;
        switch ($this->operator) {
            case '>':
                $assert = ($pivot > $value);
                break;
            case '>=':
                $assert = ($pivot >= $value);
                break;
            case '<':
                $assert = ($pivot < $value);
                break;
            case '<=':
                $assert = ($pivot <= $value);
                break;
                break;
            case '==':
                $assert = ($pivot == $value);
                break;
                break;
            case '!=':
                $assert = ($pivot != $value);
                break;
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_price_wt()
    {
        $value = (float)$this->value;
        $pivot = (float)$this->product->price;
        $assert = false;
        switch ($this->operator) {
            case '>':
                $assert = ($pivot > $value);
                break;
            case '>=':
                $assert = ($pivot >= $value);
                break;
            case '<':
                $assert = ($pivot < $value);
                break;
            case '<=':
                $assert = ($pivot <= $value);
                break;
                break;
            case '==':
                $assert = ($pivot == $value);
                break;
                break;
            case '!=':
                $assert = ($pivot != $value);
                break;
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_sku()
    {
        $value = $this->value;
        $pivot = $this->product->id;
        $assert = false;
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;

            case '+=': // qualsiasi
                $assert = ($pivot > 0);
                break;

            case '-=': // nessuno
                $assert = ((int)$pivot <= 0);
                break;
        }
        $this->setAssert($assert);
    }


    private function _condition_flag_new()
    {
        $value = (int)$this->value;
        $pivot = (int)$this->product->is_new;
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_flag_outlet()
    {
        $value = (int)$this->value;
        $pivot = (int)$this->product->is_outlet;
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_flag_featured()
    {
        $value = (int)$this->value;
        $pivot = (int)$this->product->is_featured;
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_flag_fp()
    {
        $value = (int)$this->value;
        $pivot = (int)$this->product->is_out_of_production;
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_flag_promotion()
    {
        //$this->log($this->product,'_condition_flag_promotion');
        $value = (int)$this->value;
        $pivot = (int)$this->product->is_in_promotion;
        if (isset($this->product->price_saving_raw)) {
            if ($value == 0) {
                $pivot = $this->product->price_saving_raw <= 0.5 ? 0 : 1;
            } else {
                $pivot = $this->product->price_saving_raw > 0.5 ? 1 : 0;
            }
        }
        $this->processDefaultAssert($pivot, $value);
    }
} 