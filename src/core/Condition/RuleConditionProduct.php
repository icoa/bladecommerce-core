<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 16/10/14
 * Time: 18.01
 */
namespace Core\Condition;

class RuleConditionProduct extends RuleCondition
{
    protected $localDebug = false;
    protected $code = "RCP";
    protected $fields = ['value', 'operator', 'attribute'];


    function solve()
    {
        $method_name = "_condition_" . $this->attribute;

        if (method_exists($this, $method_name)) {
            $this->log($method_name, "CALLING METHOD");
            call_user_func_array(array($this, $method_name), [null]);
        } else {
            $this->log("_condition_attribute_ids", "CALLING METHOD");
            call_user_func_array(array($this, "_condition_attribute_ids"), [null]);
        }
    }


    private function _condition_category_ids()
    {
        $value = $this->value;
        switch ($this->operator) {
            case '==':
                $ids = \Product::whereIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('categories_products')
                        ->where('category_id', $value);
                })->orWhere("main_category_id", $value)->lists("id");
                break;

            case '!=':
                $ids = \Product::whereNotIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('categories_products')
                        ->where('category_id', $value);
                })->where("main_category_id", "<>", $value)->lists("id");
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $ids = \Product::whereIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('categories_products')
                        ->whereIn('category_id', $value);
                })->orWhereIn("main_category_id", $value)->lists("id");
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $ids = \Product::whereNotIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('categories_products')
                        ->whereIn('category_id', $value);
                })->whereIn("main_category_id", $value)->lists("id");
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->orWhere("main_category_id", $v);
                    }
                    $query->orWhereIn("id", function ($query) use ($value) {
                        $query->select(['product_id'])
                            ->from('categories_products')
                            ->whereIn('category_id', $value);
                    });
                });
                $ids = $query->lists("id");
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->where("main_category_id", "<>", $v);
                    }
                    $query->whereIn("id", function ($query) use ($value) {
                        $query->select(['product_id'])
                            ->from('categories_products')
                            ->whereNotIn('category_id', $value);
                    });
                });
                $ids = $query->lists("id");
                break;

            case '+=': // qualsiasi
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    $query->whereIn("id", function ($query) use ($value) {
                        $query->select([DB::raw('distinct product_id')])
                            ->from('categories_products');
                    });
                });
                $ids = $query->lists("id");
                break;

            case '-=': // nessuno
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    $query->whereNotIn("id", function ($query) use ($value) {
                        $query->select([DB::raw('distinct product_id')])
                            ->from('categories_products');
                    });
                });
                $ids = $query->lists("id");
                break;
        }
        $this->setResult($ids);
    }

    private function _condition_brand_ids()
    {
        $value = $this->value;
        switch ($this->operator) {
            case '==':
                $ids = \Product::where("brand_id", $value)->lists("id");
                break;

            case '!=':
                $ids = \Product::where("brand_id", "<>", $value)->lists("id");
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $ids = \Product::whereIn("brand_id", $value)->lists("id");
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $ids = \Product::whereNotIn("brand_id", $value)->lists("id");
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->orWhere("brand_id", $v);
                    }
                });
                $ids = $query->lists("id");
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->where("brand_id", "<>", $v);
                    }
                });
                $ids = $query->lists("id");
                break;
            case '+=': // qualsiasi
                $query = \Product::where("id", ">", 0)->where("brand_id", '>', 0);
                $ids = $query->lists("id");
                break;

            case '-=': // nessuno
                $query = \Product::where("id", ">", 0)->where("brand_id", '=', 0);
                $ids = $query->lists("id");
                break;
        }
        $this->setResult($ids);
    }

    private function _condition_collection_ids()
    {
        $value = $this->value;
        switch ($this->operator) {
            case '==':
                $ids = \Product::where("collection_id", $value)->lists("id");
                break;

            case '!=':
                $ids = \Product::where("collection_id", "<>", $value)->lists("id");
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $ids = \Product::whereIn("collection_id", $value)->lists("id");
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $ids = \Product::whereNotIn("collection_id", $value)->lists("id");
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->orWhere("collection_id", $v);
                    }
                });
                $ids = $query->lists("id");
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->where("collection_id", "<>", $v);
                    }
                });
                $ids = $query->lists("id");
                break;
            case '+=': // qualsiasi
                $query = \Product::where("id", ">", 0)->where("collection_id", '>', 0);
                $ids = $query->lists("id");
                break;

            case '-=': // nessuno
                $query = \Product::where("id", ">", 0)->where("collection_id", '=', 0);
                $ids = $query->lists("id");
                break;
        }
        $this->setResult($ids);
    }

    private function _condition_supplier_ids()
    {
        $value = $this->value;
        switch ($this->operator) {
            case '==':
                $ids = \Product::whereIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('products_suppliers')
                        ->where('supplier_id', $value);
                })->lists("id");
                break;

            case '!=':
                $ids = \Product::whereNotIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('products_suppliers')
                        ->where('supplier_id', $value);
                })->lists("id");
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $ids = \Product::whereIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('products_suppliers')
                        ->whereIn('supplier_id', $value);
                })->lists("id");
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $ids = \Product::whereNotIn("id", function ($query) use ($value) {
                    $query->select(['product_id'])
                        ->from('products_suppliers')
                        ->whereIn('supplier_id', $value);
                })->lists("id");
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    $query->orWhereIn("id", function ($query) use ($value) {
                        $query->select(['product_id'])
                            ->from('products_suppliers')
                            ->whereIn('supplier_id', $value);
                    });
                });
                $ids = $query->lists("id");
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    $query->whereIn("id", function ($query) use ($value) {
                        $query->select(['product_id'])
                            ->from('products_suppliers')
                            ->whereNotIn('supplier_id', $value);
                    });
                });
                $ids = $query->lists("id");
                break;
        }
        $this->setResult($ids);
    }

    private function _condition_attribute_ids()
    {
        $attribute = $this->attribute;
        $value = $this->value;
        if (substr($attribute, 0, 5) == 'flag_') {
            return $this->_condition_flag();
        }
        $obj = \Attribute::where("code", $attribute)->first();
        $attribute_id = $obj->id;
        switch ($this->operator) {
            case '==':
                $ids = \DB::table("products_attributes")->where("attribute_id", $attribute_id)->where("attribute_val", $value)->lists("product_id");
                break;

            case '!=':
                $ids = \DB::table("products_attributes")->where("attribute_id", $attribute_id)->where("attribute_val", "<>", $value)->lists("product_id");
                break;

            case '+=':
                $ids = \DB::table("products_attributes")->where("attribute_id", $attribute_id)->lists("product_id");
                break;

            case '-=':
                $ids = \DB::table("products_attributes")->where("attribute_id", '<>', $attribute_id)->lists("product_id");
                break;
        }
        $this->log($ids, __METHOD__);
        $this->setResult($ids);
    }


    private function _condition_flag()
    {
        $attribute = $this->attribute;
        $value = $this->value;
        $flag = null;
        $ids = null;
        //is_featured is_new is_outlet
        switch ($attribute) {
            case 'flag_outlet':
                $flag = 'is_outlet';
                break;
            case 'flag_new':
                $flag = 'is_new';
                break;
            case 'flag_featured':
                $flag = 'is_featured';
                break;
            case 'flag_fp':
                $flag = 'is_out_of_production';
                break;
        }
        if ($flag) {
            switch ($this->operator) {
                case '==':
                    $ids = \Product::where($flag, '=', (int)$value)->lists("id");
                    break;

                case '!=':
                    $ids = \Product::where($flag, '<>', (int)$value)->lists("id");
                    break;
            }
        }
        if ($ids) $this->setResult($ids);
    }


    private function _condition_price()
    {
        $value = (float)$this->value;
        switch ($this->operator) {
            case '>':
            case '>=':
            case '<':
            case '<=':
                $ids = \Product::where("buy_price", $this->operator, $value)->lists("id");
                break;
            case '==':
                $ids = \Product::where("buy_price", "=", $value)->lists("id");
                break;
            case '!=':
                $ids = \Product::where("buy_price", "<>", $value)->lists("id");
                break;
        }
        $this->setResult($ids);
    }

    private function _condition_price_wt()
    {
        $value = (float)$this->value;
        switch ($this->operator) {
            case '>':
            case '>=':
            case '<':
            case '<=':
                $ids = \Product::where("price", $this->operator, $value)->lists("id");
                break;
            case '==':
                $ids = \Product::where("price", "=", $value)->lists("id");
                break;
            case '!=':
                $ids = \Product::where("price", "<>", $value)->lists("id");
                break;
        }
        $this->setResult($ids);
    }

    private function _condition_sku()
    {
        $value = $this->value;
        switch ($this->operator) {
            case '==':
                $ids = \Product::where("id", "=", $value)->lists("id");
                break;
            case '!=':
                $ids = \Product::where("id", "<>", $value)->lists("id");
                break;
            case '{}': //contiene
                $value = explode(",", $value);
                $ids = \Product::whereIn("id", $value)->lists("id");
                break;
            case '!{}': //non contiene
                $value = explode(",", $value);
                $ids = \Product::whereNotIn("id", $value)->lists("id");
                break;
            case '()': //è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->orWhere("id", $v);
                    }
                });
                $ids = $query->lists("id");
                break;
            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $query = \Product::where("id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->where("id", "<>", $v);
                    }
                });
                $ids = $query->lists("id");
                break;
        }
        $this->setResult($ids);
    }


    private function _condition_trend_ids()
    {
        $value = $this->value;
        switch ($this->operator) {
            case '==':
                $ids = \DB::table('trends_products')->where("trend_id", $value)->lists("product_id");
                break;

            case '!=':
                $ids = \DB::table('trends_products')->where("trend_id", "<>", $value)->lists("product_id");
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $ids = \DB::table('trends_products')->whereIn("trend_id", $value)->lists("product_id");
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $ids = \DB::table('trends_products')->whereNotIn("trend_id", $value)->lists("product_id");
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);

                $query = \DB::table('trends_products')->where("trend_id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->orWhere("trend_id", $v);
                    }
                });
                $ids = $query->lists("product_id");
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);

                $query = \DB::table('trends_products')->where("trend_id", ">", 0);
                $query->whereNested(function ($query) use ($value) {
                    foreach ($value as $v) {
                        $query->where("trend_id", "<>", $v);
                    }
                });
                $ids = $query->lists("product_id");

                break;
            case '+=': // qualsiasi
                $query = \Product::whereIn("id", function ($query) {
                    $query->select('product_id')->from('trends_products');
                });
                $ids = $query->lists("id");
                break;

            case '-=': // nessuno
                $query = \Product::whereNotIn("id", function ($query) {
                    $query->select('product_id')->from('trends_products');
                });
                $ids = $query->lists("id");
                break;
        }
        $this->setResult($ids);
    }

}