<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionCartProduct extends SingleRuleCondition
{

    protected $code = "SRCCP";
    protected $fields = ['value', 'aggregator', 'conditions'];
    protected $localDebug = false;

    function solve()
    {
        $this->log($this->conditions, "CONDITIONS SingleRuleConditionCartProduct");
        $products_asserts = [];
        if ($this->conditions AND is_array($this->conditions)) {

            $cart = $this->product;
            $products = $cart->getProducts();
            foreach ($products as $product) {

                if ($product->special == 0) {

                    foreach ($this->conditions as $rule) {
                        $rule = (object)$rule;
                        $ruletype = $this->getRuleFromString($rule->type);

                        switch ($ruletype) {
                            case 'rule_condition_product':
                                $rcp = new SingleRuleConditionProduct($rule, $product);
                                $rcp->solve();
                                $this->add($rcp);
                                break;
                            case 'rule_condition_combine':
                                $rcc = new SingleRuleConditionCombine($rule, $product);
                                $rcc->solve();
                                $this->add($rcc);
                                break;

                            case 'rule_condition_cart':
                                $product->products = $products;
                                $product->queue = $this->queue;
                                $rcc = new SingleRuleConditionCart($rule, $product);
                                $rcc->solve();
                                $this->add($rcc);
                                break;

                        }
                    }


                    $assert = false;
                    $asserts = [];

                    if ($this->aggregator == 'all') { //all conditions are true
                        $i = 0;
                        foreach ($this->queue as $condition) {
                            $a = $condition->getAssert();
                            $asserts[] = $a;
                            if ($a == 'true') {
                                $i++;
                            }
                        }
                        $assert = ($i == count($this->queue));
                    }

                    if ($this->aggregator == 'any') { //any one of all conditions is true
                        $i = 0;
                        foreach ($this->queue as $condition) {
                            $a = $condition->getAssert();
                            $asserts[] = $a;
                            if ($a == 'true') {
                                $i++;
                            }
                        }
                        $assert = ($i > 0);
                    }

                    $products_asserts[] = ($assert) ? 'true' : 'false';
                    $this->queue = [];
                    $this->log($products_asserts, "PRODUCTS ASSERTS");
                }
            }
        }


        $must_found = ($this->value == 1);
        $founded = 0;
        foreach ($products_asserts as $single_assert) {
            if ($single_assert == 'true') {
                $founded++;
            }
        }

        $assert = ($must_found) ? $founded > 0 : $founded == 0;

        $this->setAssert($assert);

        $this->log($this->getAssert(), "FINAL ASSERT");

    }

} 