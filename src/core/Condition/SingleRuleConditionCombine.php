<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionCombine extends SingleRuleCondition
{

    protected $code = "SRCC";
    protected $fields = ['value', 'aggregator', 'conditions'];
    protected $localDebug = false;

    function solve()
    {
        $this->log($this->conditions, "CONDITIONS", __METHOD__);
        if ($this->conditions AND is_array($this->conditions)) {
            foreach ($this->conditions as $rule) {
                $rule = (object)$rule;
                $ruletype = $this->getRuleFromString($rule->type);
                switch ($ruletype) {
                    case 'rule_condition_product':
                        $rcp = new SingleRuleConditionProduct($rule, $this->product);
                        $rcp->solve();
                        $this->add($rcp);
                        break;

                    case 'rule_condition_combine':
                        $rcc = new SingleRuleConditionCombine($rule, $this->product);
                        $rcc->solve();
                        $this->add($rcc);
                        break;

                    case 'rule_condition_cart':
                        $rcc = new SingleRuleConditionCart($rule, $this->product);
                        $rcc->solve();
                        $this->add($rcc);
                        break;

                    case 'rule_condition_product_found':
                        $rcc = new SingleRuleConditionCartProduct($rule, $this->product);
                        $rcc->solve();
                        $this->add($rcc);
                        break;

                    case 'rule_condition_product_subselect':
                        $rcc = new SingleRuleConditionProductSubselect($rule, $this->product);
                        $rcc->solve();
                        $this->add($rcc);
                        break;

                    case 'rule_condition_scope':
                        $rcp = new SingleRuleConditionScope($rule, $this->product);
                        $rcp->solve();
                        $this->add($rcp);
                        break;

                    case 'rule_condition_self':
                        $rcp = new SingleRuleConditionGeneral($rule, $this->product);
                        $rcp->solve();
                        $this->add($rcp);
                        break;
                }
            }
        }

        $assert = false;
        $asserts = [];
        if ($this->aggregator == 'all' AND $this->value == 1) { //all conditions are true
            $i = 0;
            foreach ($this->queue as $condition) {
                $a = $condition->getAssert();
                $asserts[] = $a;
                if ($a == 'true') {
                    $i++;
                }
            }
            $assert = ($i == count($this->queue));
        }

        if ($this->aggregator == 'any' AND $this->value == 1) { //any one of all conditions is true
            $i = 0;
            foreach ($this->queue as $condition) {
                $a = $condition->getAssert();
                $asserts[] = $a;
                if ($a == 'true') {
                    $i++;
                }
            }
            $assert = ($i > 0);
        }

        if ($this->aggregator == 'all' AND $this->value == 0) { //all conditions are false
            $i = 0;
            foreach ($this->queue as $condition) {
                $a = $condition->getAssert();
                $asserts[] = $a;
                if ($a == 'false') {
                    $i++;
                }
            }
            $assert = ($i == count($this->queue));
        }

        if ($this->aggregator == 'any' AND $this->value == 0) { //any one of all conditions is false
            $i = 0;
            foreach ($this->queue as $condition) {
                $a = $condition->getAssert();
                $asserts[] = $a;
                if ($a == 'false') {
                    $i++;
                }
            }
            $assert = ($i > 0);
        }

        $this->setAssert($assert);
        $this->log($asserts, "ASSERTS");
        $this->log($this->getAssert(), "FINAL ASSERT");


    }

} 