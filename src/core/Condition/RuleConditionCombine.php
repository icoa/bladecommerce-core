<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 16/10/14
 * Time: 18.01
 */

namespace Core\Condition;

class RuleConditionCombine extends RuleCondition
{

    protected $code = "RCC";
    protected $fields = ['value','aggregator','conditions'];

    function solve(){
        if($this->conditions AND is_array($this->conditions)){
            foreach($this->conditions as $rule){
                $rule = (object)$rule;
                $ruletype = $this->getRuleFromString($rule->type);
                switch($ruletype){
                    case 'rule_condition_product':
                        $rcp = new RuleConditionProduct($rule);
                        $rcp->setDebug($this->localDebug);
                        $rcp->solve();
                        $this->add( $rcp );
                        break;

                    case 'rule_condition_combine':
                        $rcc = new RuleConditionCombine($rule);
                        $rcc->setDebug($this->localDebug);
                        $rcc->solve();
                        $this->add( $rcc );
                        break;
                }
            }
        }

        $query = [];
        if($this->aggregator == 'all' AND $this->value == 1){ //all conditions are true
            $i = 0;
            foreach($this->queue as $condition){
                $table = $condition->getTable();
                $query[] = ($i == 0) ? "SELECT DISTINCT id FROM $table" : "INNER JOIN $table USING (id)";
                $i++;
            }
        }

        if($this->aggregator == 'any' AND $this->value == 1){ //any one of all conditions is true
            $i = 0;
            foreach($this->queue as $condition){
                $table = $condition->getTable();
                $query[] = ($i == 0) ? "SELECT DISTINCT id FROM $table" : "UNION SELECT DISTINCT id FROM $table";
                $i++;
            }
        }

        if($this->aggregator == 'all' AND $this->value == 0){ //all conditions are false
            $i = 0;
            foreach($this->queue as $condition){
                $table = $condition->getTable();
                $query[] = ($i == 0) ? "SELECT DISTINCT id FROM $table" : "UNION SELECT DISTINCT id FROM $table";
                $i++;
            }
            $queryAll = implode(" ",$query);
            $fullQuery = "SELECT id FROM products WHERE id NOT IN ($queryAll)";
            unset($query);
            $query = [ $fullQuery ];
        }

        if($this->aggregator == 'any' AND $this->value == 0){ //any one of all conditions is false
            $i = 0;
            foreach($this->queue as $condition){
                $table = $condition->getTable();
                $query[] = ($i == 0) ? "SELECT DISTINCT id FROM $table" : "INNER JOIN $table USING (id)";
                $i++;
            }
            $queryAll = implode(" ",$query);
            $fullQuery = "SELECT id FROM products WHERE id NOT IN ($queryAll)";
            unset($query);
            $query = [ $fullQuery ];
        }

        $ids = [];
        $queryAll = implode(' ',$query);
        $this->log($queryAll, 'queryAll', __METHOD__);
        $data = \DB::select($queryAll);
        foreach($data as $obj){
            $ids[] = $obj->id;
        }
        //\Utils::log($ids,"FINAL IDS");
        $this->setResult($ids);
        //$this->insertIds($ids);

    }


}