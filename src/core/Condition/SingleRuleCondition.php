<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 16.25
 */

namespace Core\Condition;

class SingleRuleCondition extends LocalDebug{

    public $rule;
    public $product;
    public $assert;
    public $queue = [];
    private $table;
    protected $code = "RC";
    protected $fields = ['value'];
    public $conditions = null;

    function __construct($rule, $product)
    {
        $this->rule = (object)$rule;
        $this->product = $product;

        foreach($this->fields as $f){
            if(isset($this->rule) and isset($this->rule->$f)){
                $this->$f = $this->rule->$f;
            }
        }

        return $this;
    }

    protected function getRuleFromString($str){
        $tokens = explode("|",$str);
        return $tokens[1];
    }

    function add( $rule ){
        $this->queue[] = $rule;
    }

    protected function setAssert($assert){
        $this->assert = $assert;
    }

    function getAssert(){
        return ($this->assert) ? 'true' : 'false';
    }

    function getAssertRaw(){
        return $this->assert;
    }

    function processDefaultAssert($pivot,$value){
        $assert = false;
        if(str_contains($pivot, ',')){
            $this->mutateOperatorForMultiValue();
        }
        $this->log("Pivot: $pivot | Value: $value | Operator: $this->operator",__METHOD__);
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(',',$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter === count($value));
                break;

            case '!{}': // non contiene
                $value = explode(',',$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter === 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(',',$value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(',',$value);
                $assert = !in_array($pivot, $value);
                break;

            case '~()': // è almeno uno tra | il pivot è un array
                $pivot = explode(',',$pivot);
                $assert = in_array($value, $pivot);
                break;

            case '~!()': // non è almeno uno tra | il pivot è un array
                $pivot = explode(',',$pivot);
                $assert = !in_array($value, $pivot);
                break;

            case '+=': // qualsiasi
                $assert = (int)$pivot > 0;
                break;

            case '-=': // nessuno
                $assert = (int)$pivot <= 0;
                break;
        }
        $this->setAssert($assert);
    }


    function mutateOperatorForMultiValue(){
        switch ($this->operator) {
            case '==':
                $this->operator = '~()';
                break;

            case '!=':
                $this->operator = '~!()';
                break;
        }
    }


    protected function _condition_site_scope()
    {
        $value = $this->value;
        $pivot = \FrontTpl::getScope();

        $this->log("VALUE: $value | PIVOT: $pivot",__METHOD__);


        $this->processDefaultAssert($pivot,$value);
    }

    protected function _condition_customer_registered()
    {
        $value = $this->value;
        $logged = \FrontUser::logged();
        $is_guest = \FrontUser::is_guest();
        $pivot = ($logged AND !$is_guest) ? 1 : 0;

        $this->log("VALUE: $value | PIVOT: $pivot",__METHOD__);

        $this->processDefaultAssert($pivot,$value);
    }

    protected function _condition_customer_guest()
    {
        $value = $this->value;
        $is_guest = \FrontUser::is_guest();
        $pivot = ($is_guest) ? 1 : 0;

        $this->log("VALUE: $value | PIVOT: $pivot",__METHOD__);

        $this->processDefaultAssert($pivot,$value);
    }

    protected function _condition_customer_default()
    {
        $value = $this->value;
        $logged = \FrontUser::logged();
        $pivot = (!$logged) ? 1 : 0;

        $this->log("VALUE: $value | PIVOT: $pivot",__METHOD__);

        $this->processDefaultAssert($pivot,$value);
    }

    protected function _condition_customer_group()
    {
        $value = $this->value;
        $user = \FrontUser::get();
        $pivot = ($value > 0 && $user) ? $user->default_group_id : -1;
        if($pivot > 0 && $user && $user->alt_group_id > 0) {
            $pivot .= ',' . $user->alt_group_id;
        }

        $this->log("VALUE: $value | PIVOT: $pivot", __METHOD__);

        $this->processDefaultAssert($pivot, $value);
    }

}