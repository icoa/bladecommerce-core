<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionScope extends SingleRuleCondition
{

    protected $localDebug = false;
    protected $code = "SRCS";
    protected $fields = ['value', 'operator', 'attribute'];

    function solve()
    {
        $method_name = "_condition_" . $this->attribute;
        $scope = \FrontTpl::getScope();
        $product_conditions = [
            '_condition_section_ids',
            '_condition_page_ids',
        ];
        if ($scope == 'product' AND !in_array($method_name, $product_conditions)) {
            $this->log('SingleRuleConditionProduct', "MOCKING CONDITION for $method_name");
            $srcp = new SingleRuleConditionProduct($this->rule, \FrontTpl::getData('model'));
            $srcp->solve();
            $this->setAssert($srcp->getAssert() == 'true' ? true : false);
            return;
        }

        if (method_exists($this, $method_name)) {
            $this->log($method_name, "CALLING METHOD");
            call_user_func_array(array($this, $method_name), [null]);
        } else {
            $this->log("_condition_attribute_ids", "CALLING METHOD");
            call_user_func_array(array($this, "_condition_attribute_ids"), [null]);
        }
    }

    private function _condition_category_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->category_id > 0)
                $pivot = $catalog->category_id;
        } else {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_brand_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->brand_id > 0)
                $pivot = $catalog->brand_id;
        } else {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_collection_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->collection_id > 0)
                $pivot = $catalog->collection_id;
        } else {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_supplier_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->collection_id > 0)
                $pivot = $catalog->collection_id;
        } else {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }

    private function _condition_attribute_ids()
    {
        $value = $this->value;
        $assert = false;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if (count($catalog->attributes) > 0) {
                $pivot = array_values($catalog->attributes);
                switch ($this->operator) {
                    case '==': // è almeno uno tra
                        $assert = in_array($value, $pivot);
                        break;

                    case '!=': // non è almeno uno tra
                        $assert = !in_array($value, $pivot);
                        break;
                }
            }
        } else {
            $this->setAssert(false);
            return;
        }

        $this->setAssert($assert);
    }


    private function _condition_sku()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }


    private function _condition_nav_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = (int)\FrontTpl::getScopeId();
        $name = \FrontTpl::getScopeName();

        $this->log("SCOPE: $scope | PIVOT: $pivot | VALUE: $value | NAME: $name", __METHOD__);

        if($scope == 'catalog'){
            $catalog = \Catalog::getObj();
            if($catalog->special_id > 0)
                $pivot = $catalog->special_id;
        }elseif($scope == 'list'){
            $this->setAssert($pivot == $value);
            return;
        }elseif($scope == $name){
            $assert = ($pivot == 0) ? true : $value == $pivot;
            $this->setAssert($assert);
            return;
        }elseif($scope == 'nav'){
            $this->setAssert(false);
            return;
        }else{
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }


    private function _condition_trend_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope == 'catalog') {
            $catalog = \Catalog::getObj();
            if ($catalog->trend_id > 0) {
                $pivot = $catalog->trend_id;
            }else{
                $pivot = null;
            }
        } else {
            $this->setAssert(false);
            return;
        }

        $this->processDefaultAssert($pivot, $value);
    }


    private function _condition_pricerule_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();
        $this->log("SCOPE: $scope | PIVOT: $pivot", __METHOD__);

        if ($scope != 'promo') {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }


    private function _condition_section_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();

        if ($scope != 'section') {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }


    private function _condition_page_ids()
    {
        $value = $this->value;
        $scope = \FrontTpl::getScope();
        $pivot = \FrontTpl::getScopeId();
        $this->log("SCOPE: $scope | PIVOT: $pivot", __METHOD__);

        if ($scope != 'page') {
            $this->setAssert(false);
            return;
        }
        $this->processDefaultAssert($pivot, $value);
    }

} 