<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 18/09/14
 * Time: 15.28
 */
namespace Core\Condition;

class RuleResolver extends LocalDebug
{
    protected $localDebug = false;
    private $memory = [];
    private $rules = [];
    private $data = [];

    function getSerializable($array = [])
    {
        /*$array = ["1" => array(
            "type" => "rule_condition_combine",
            "aggregator" => "all",
            "value" => "1"
        ),

            "1--1" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "category_ids",
                "operator" => "==",
                "value" => 9
            ),

            "1--2" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "brand_ids",
                "operator" => "==",
                "value" => 9
            ),

            "1--3" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "sku",
                "operator" => "==",
                "value" => 9
            ),

            "1--4" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "gender",
                "operator" => "==",
                "value" => 25
            ),

            "1--5" => array
            (
                "type" => "rule_condition_combine",
                "aggregator" => "all",
                "value" => 0
            ),

            "1--5--1" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "category_ids",
                "operator" => "==",
                "value" => 10
            ),

            "1--5--2" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "brand_ids",
                "operator" => "==",
                "value" => 3
            ),

            "1--5--3" => array
            (
                "type" => "rule_condition_combine",
                "aggregator" => "all",
                "value" => 0
            ),

            "1--5--3--1" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "category_ids",
                "operator" => "!=",
                "value" => 7
            ),

            "1--5--3--2" => array
            (
                "new_child" => ""
            ),

            "1--5--4" => array
            (
                "new_child" => ""
            ),

            "1--6" => array
            (
                "type" => "rule_condition_product",
                "attribute" => "gender",
                "operator" => "!=",
                "value" => 26
            ),

            "1--7" => array
            (
                "new_child" => ""
            ),

        ];
*/


        if (isset($array["1"])) {


            if (!empty($array)) {


                $temp = [];
                foreach ($array as $key => $item) {
                    if (!isset($item["new_child"])) {
                        $parent = $this->getParent($key);

                        if (isset($array[$parent])) {
                            if (!isset($array[$parent]["conditions"])) {
                                $array[$parent]["conditions"] = [];
                            }
                            $array[$parent]["conditions"][$key] = $item;
                        }
                    }
                }

                foreach ($array as $key => $item) {
                    if (!isset($item["new_child"])) {
                        if (isset($item["conditions"]) and $key != "1") {
                            $this->insertRecur($array["1"]["conditions"], $key, $item);
                        }
                        if ($key != "1") unset($array[$key]);
                    } else {
                        unset($array[$key]);
                    }
                }

                if (!isset($array[1]["conditions"])) {
                    $array = [];
                }

            }

        }

        $this->log($array, "RULES ARRAY");


        return (empty($array)) ? "" : serialize($array);

    }


    public function toArray($str)
    {
        return unserialize($str);
    }

    private function insertRecur(&$array, $key, $item)
    {
        foreach ($array as $__key => $__item) {
            if ($key == $__key) {
                $array[$__key] = $item;
            }
            if (isset($__item["conditions"])) {
                $this->insertRecur($array[$__key]["conditions"], $key, $item);
            }
        }
    }

    private function getParent($key)
    {
        $tokens = explode("--", $key);
        unset($tokens[count($tokens) - 1]);
        return implode("--", $tokens);
    }


    public function bindProducts()
    {
        try {
            $root = array_values($this->rules)[0];
            $this->log($root, "RULES bindProducts");
            if ($root['type'] == 'product|rule_condition_combine') {
                $rc = new RuleConditionCombine($root);
                $rc->setDebug($this->localDebug);
                $rc->solve();
                return $rc->ids;
            }
            return [];
        } catch (\Exception $e) {
            $this->error($e->getMessage(), __METHOD__);
            $this->error("File: " . $e->getFile() . ' | Line:' . $e->getLine(), __METHOD__);
            $this->error($e->getTraceAsString(), __METHOD__);
        }
        return [];
    }

    public function bindRules($product)
    {
        try {
            $root = array_values($this->rules)[0];
            $this->log($root, "RULES bindRules");
            if ($root['type'] == 'product|rule_condition_combine') {
                $rc = new SingleRuleConditionCombine($root, $product);
                $rc->solve();
                return $rc->getAssert();
            }
            if ($root['type'] == 'cart|rule_condition_combine') {
                $rc = new SingleRuleConditionCombine($root, $product);
                $rc->solve();
                return $rc->getAssert();
            }
            if ($root['type'] == 'site|rule_condition_combine') {
                $rc = new SingleRuleConditionCombine($root, $product);
                $rc->solve();
                return $rc->getAssert();
            }
            if ($root['type'] == 'general|rule_condition_combine') {
                $rc = new SingleRuleConditionCombine($root, $product);
                $rc->solve();
                return $rc->getAssert();
            }
            return 'false';
        } catch (\Exception $e) {
            $this->error($e->getMessage(), __METHOD__);
            $this->error("File: " . $e->getFile() . ' | Line:' . $e->getLine(), __METHOD__);
            $this->error($this->rules, 'RULES');
            $this->error($product, 'PRODUCT');
            $this->error($e->getTraceAsString(), __METHOD__);
        }
        return 'false';
    }


    public function setRules($rules)
    {
        if (is_array($rules) OR is_object($rules)) {
            $this->rules = $rules;
        } else {
            $this->rules = $this->toArray($rules);
        }
    }


}
