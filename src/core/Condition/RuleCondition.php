<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 16.25
 */

namespace Core\Condition;

class RuleCondition extends LocalDebug {

    public $rule;
    public $ids = [];
    public $queue = [];
    private $table;
    protected $code = "RC";
    protected $fields = ['value'];

    function __construct($rule)
    {
        $this->rule = (object)$rule;

        foreach($this->fields as $f){
            $this->$f = $this->rule->$f;
        }

        $this->table = $this->code. '_' .\Utils::randomString();
        $this->createTempTable();
        return $this;
    }

    protected function createTempTable(){
        $query = "CREATE TEMPORARY TABLE {$this->table}(id INT, PRIMARY KEY(id), INDEX(id)) ENGINE=MEMORY;";
        \DB::statement($query);
    }

    protected function insertIds($ids){
        $data = [];
        $current_ids = \DB::table($this->table)->lists('id');
        $ids = array_diff($ids, $current_ids);
        $ids = array_unique($ids);
        foreach($ids as $id){
            $data[] = ['id' => $id];
        }
        \DB::beginTransaction();
        \DB::table($this->table)->insert($data);
        \DB::commit();
    }

    function getTable()
    {
        return $this->table;
    }

    protected function getRuleFromString($str){
        $tokens = explode("|",$str);
        return $tokens[1];
    }

    function add( $rule ){
        $this->queue[] = $rule;
    }

    protected function setResult($ids)
    {
        $this->ids = $ids;
        $this->insertIds($this->ids);
    }

    function __destruct()
    {
        $query = "DROP TEMPORARY TABLE {$this->table};";
        \DB::statement($query);
        \Utils::log($query);
    }
}