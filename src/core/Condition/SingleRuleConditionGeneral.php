<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionGeneral extends SingleRuleCondition {

    protected $code = "SRCG";
    protected $fields = ['value','operator','attribute'];

    function solve()
    {
        $method_name = "_condition_".$this->attribute;

        $this->log(get_class($this->product), __METHOD__);
        if($this->product == null){
            $rcp = new SingleRuleConditionScope($this->rule, $this->product);
            $rcp->solve();
            $this->setAssert( $rcp->getAssert() );
            return;
        }
        if(get_class($this->product) == 'Product'){
            $rcp = new SingleRuleConditionProduct($this->rule, $this->product);
            $rcp->solve();
            $this->setAssert( $rcp->getAssertRaw() );
            return;
        }
        if(get_class($this->product) == 'SeoText'){
            $rcp = new SingleRuleConditionProduct($this->rule, $this->product);
            $rcp->solve();
            $this->setAssert( $rcp->getAssertRaw() );
            return;
        }
        if(get_class($this->product) == 'Frontend\Catalog'){
            $rcp = new SingleRuleConditionProduct($this->rule, $this->product);
            $rcp->solve();
            $this->setAssert( $rcp->getAssertRaw() );
            return;
        }


        if (method_exists($this, $method_name)) {
            $this->log($method_name, "CALLING METHOD");
            //call_user_func_array(array($this, $method_name), [null]);
        } else {
            $this->log("_condition_attribute_ids", "CALLING METHOD");
            //call_user_func_array(array($this, "_condition_attribute_ids"), [null]);
        }
    }

    private function _condition_category_ids()
    {
        $value = $this->value;
        $ids = $this->product->category_ids;
        $main_id = $this->product->main_category_id;
        switch ($this->operator) {
            case '==':
                $assert = ($main_id == $value OR in_array($value,$ids));
                break;

            case '!=':
                $assert = ($main_id != $value AND !in_array($value,$ids));
                break;

            case '{}': // contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($main_id == $value OR in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($main_id == $value OR in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($main_id == $value OR in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter > 0);
                break;

            case '!()': // non è almeno uno tra
                $counter = 0;
                foreach($value as $v){
                    if($main_id == $value OR in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_brand_ids()
    {
        $value = $this->value;
        $pivot = $this->product->brand_id;
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",",$value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",",$value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_collection_ids()
    {
        $value = $this->value;
        $pivot = $this->product->collection_id;
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",",$value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",",$value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_supplier_ids()
    {
        $value = $this->value;
        $ids = $this->product->supplier_ids;
        switch ($this->operator) {
            case '==':
                $assert = in_array($value,$ids);
                break;

            case '!=':
                $assert = !in_array($value,$ids);
                break;

            case '{}': // contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if(in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if(in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if(in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter > 0);
                break;

            case '!()': // non è almeno uno tra
                $counter = 0;
                foreach($value as $v){
                    if(in_array($value,$ids)){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;
        }
        $this->setAssert($assert);
    }

    private function _condition_attribute_ids()
    {
        //$attribute = $this->attribute;
        $value = $this->value;
        //$obj = \Attribute::where("code", $attribute)->first();
        //$attribute_id = $obj->id;
        $product_id = $this->product->id;
        $product_options = $this->product->attribute_option_ids;
        switch ($this->operator) {
            case '==': // è almeno uno tra
                $assert = in_array($value, $product_options);
                break;

            case '!=': // non è almeno uno tra
                $assert = !in_array($value, $product_options);
                break;
        }
        $this->setAssert($assert);
    }



    private function _condition_sku(){
        $value = $this->value;
        $pivot = $this->product->id;
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",",$value);
                $counter = 0;
                foreach($value as $v){
                    if($pivot == $v){
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",",$value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",",$value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }
} 