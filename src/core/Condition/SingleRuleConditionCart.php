<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionCart extends SingleRuleCondition
{

    protected $code = "SRCC";
    protected $fields = ['value', 'operator', 'attribute'];

    function solve()
    {
        $method_name = "_condition_" . $this->attribute;

        if (method_exists($this, $method_name)) {
            $this->log($method_name, "CALLING METHOD");
            call_user_func_array(array($this, $method_name), [null]);
        } else {
            $this->log("_condition_attribute_ids", "CALLING METHOD");
            call_user_func_array(array($this, "_condition_attribute_ids"), [null]);
        }

    }

    private function _condition_base_subtotal()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getTotalProductsRealNoTaxes();
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }

    private function _condition_base_subtotal_wt()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getTotalProductsReal();
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }

    private function _condition_weight()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getTotalWeight();
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }


    private function _condition_total_qty()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getSize();
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }


    private function _condition_total_row()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getRows();
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }


    private function _condition_order_amount()
    {
        $assert = false;
        $value = $this->value;
        $user = \FrontUser::get();
        if ($user) {
            $orders = $user->getOrders(\OrderState::STATUS_CLOSED);
            $pivot = count($orders);
            switch ($this->operator) {
                case '==':
                    $assert = ($pivot == $value);
                    break;

                case '!=':
                    $assert = ($pivot != $value);
                    break;

                case '>':
                    $assert = ($pivot > $value);
                    break;

                case '>=':
                    $assert = ($pivot >= $value);
                    break;

                case '<':
                    $assert = ($pivot < $value);
                    break;

                case '<=':
                    $assert = ($pivot <= $value);
                    break;

            }
        } else {
            $assert = false;
        }
        $this->setAssert($assert);
    }


    private function _condition_order_cnt()
    {
        $assert = false;
        $value = $this->value;
        $user = \FrontUser::get();
        if ($user) {
            $orders = $user->getOrders(\OrderState::STATUS_CLOSED);
            $pivot = 0;
            if (!empty($orders)) {
                foreach ($orders as $order) {
                    $pivot += $order->total_order;
                }
            }

            switch ($this->operator) {
                case '==':
                    $assert = ($pivot == $value);
                    break;

                case '!=':
                    $assert = ($pivot != $value);
                    break;

                case '>':
                    $assert = ($pivot > $value);
                    break;

                case '>=':
                    $assert = ($pivot >= $value);
                    break;

                case '<':
                    $assert = ($pivot < $value);
                    break;

                case '<=':
                    $assert = ($pivot <= $value);
                    break;

            }
        } else {
            $assert = false;
        }
        $this->setAssert($assert);
    }


    private function _condition_payment_ids()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getPaymentId();
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }


    private function _condition_carrier_ids()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getCarrierId();
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }


    private function _condition_country_ids()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getCountryId();
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }


    private function _condition_state_ids()
    {
        $value = $this->value;
        $cart = $this->product;
        $pivot = $cart->getStateId();
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '{}': // contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == count($value));
                break;

            case '!{}': // non contiene
                $value = explode(",", $value);
                $counter = 0;
                foreach ($value as $v) {
                    if ($pivot == $v) {
                        $counter++;
                    }
                }
                $assert = ($counter == 0);
                break;

            case '()': // è almeno uno tra
                $value = explode(",", $value);
                $assert = in_array($pivot, $value);
                break;

            case '!()': // non è almeno uno tra
                $value = explode(",", $value);
                $assert = !in_array($pivot, $value);
                break;
        }
        $this->setAssert($assert);
    }


    private function _condition_quote_item_qty()
    {
        $value = $this->value;
        $product = $this->product;
        $pivot = $product->cart_quantity;
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }

    private function _condition_quote_item_price()
    {
        $value = $this->value;
        $product = $this->product;
        $pivot = $product->price_final_raw;
        $assert = false;
        $this->log("pivot: $pivot | value: $value", __METHOD__);
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->log("assert: " . ($assert ? 'true' : 'false'), __METHOD__);
        $this->setAssert($assert);
    }


    private function _condition_quote_item_listprice()
    {
        $value = $this->value;
        $product = $this->product;
        $pivot = $product->price_list_raw;
        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }


    private function _condition_quote_item_row_total()
    {
        $value = $this->value;
        $product = $this->product;

        $products = $product->products;
        $queue = $product->queue;

        $assert = false;

        $pivot = 0;

        foreach ($products as $p) {
            if (!isset($p->assert))
                $p->assert = false;
        }

        $i = 0;
        foreach ($queue as $condition) {
            $a = $condition->getAssert();
            if ($a == 'true') {
                $i++;
            }
        }
        $product->assert = ($i == count($queue));

        for ($c = 0, $l = count($products); $c < $l; $c++) {
            if ($products[$c]->id == $product->id) {
                $products[$c] = $product;
            }
        }
        foreach ($products as $p) {
            if ($p->assert) {
                $pivot++;
            }
        }


        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);
    }
} 