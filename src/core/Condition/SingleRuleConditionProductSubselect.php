<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 17/10/14
 * Time: 17.15
 */
namespace Core\Condition;

class SingleRuleConditionProductSubselect extends SingleRuleCondition
{

    protected $code = "SRCPSS";
    protected $fields = ['value', 'aggregator', 'conditions', 'attribute', 'operator'];
    protected $localDebug = false;

    function solve()
    {
        $this->log($this->conditions, "CONDITIONS SingleRuleConditionProductSubselect");
        $products_asserts = [];
        if ($this->conditions AND is_array($this->conditions)) {

            $cart = $this->product;
            $products = $cart->getProducts();
            foreach ($products as $product) {

                foreach ($this->conditions as $rule) {
                    $rule = (object)$rule;
                    $ruletype = $this->getRuleFromString($rule->type);

                    switch ($ruletype) {
                        case 'rule_condition_product':
                            $rcp = new SingleRuleConditionProduct($rule, $product);
                            $rcp->solve();
                            $this->add($rcp);
                            break;
                        case 'rule_condition_combine':
                            $rcc = new SingleRuleConditionCombine($rule, $product);
                            $rcc->solve();
                            $this->add($rcc);
                            break;

                        case 'rule_condition_cart':
                            $product->products = $products;
                            $product->queue = $this->queue;
                            $rcc = new SingleRuleConditionCart($rule, $product);
                            $rcc->solve();
                            $this->add($rcc);
                            break;

                    }
                }


                $assert = false;
                $asserts = [];

                if ($this->aggregator == 'all') { //all conditions are true
                    $i = 0;
                    foreach ($this->queue as $condition) {
                        $a = $condition->getAssert();
                        $asserts[] = $a;
                        if ($a == 'true') {
                            $i++;
                        }
                    }
                    $assert = ($i == count($this->queue));
                }

                if ($this->aggregator == 'any') { //any one of all conditions is true
                    $i = 0;
                    foreach ($this->queue as $condition) {
                        $a = $condition->getAssert();
                        $asserts[] = $a;
                        if ($a == 'true') {
                            $i++;
                        }
                    }
                    $assert = ($i > 0);
                }

                if($assert){
                    $products_asserts[] = $product;
                }

                $this->queue = [];

            }
        }

        $check = $this->attribute;
        $operator = $this->operator;
        $totalQty = 0;
        $totalAmount = 0;

        foreach($products_asserts as $product){
            $totalQty += $product->cart_quantity;
            $totalAmount += $product->cart_price_raw;
        }

        $pivot = ($check == 'qty') ? $totalQty : $totalAmount;
        $value = $this->value;

        $assert = false;
        $this->log("pivot: $pivot | value: $value");
        switch ($this->operator) {
            case '==':
                $assert = ($pivot == $value);
                break;

            case '!=':
                $assert = ($pivot != $value);
                break;

            case '>':
                $assert = ($pivot > $value);
                break;

            case '>=':
                $assert = ($pivot >= $value);
                break;

            case '<':
                $assert = ($pivot < $value);
                break;

            case '<=':
                $assert = ($pivot <= $value);
                break;

        }
        $this->setAssert($assert);

        $this->log($this->getAssert(), "FINAL ASSERT");

    }

} 