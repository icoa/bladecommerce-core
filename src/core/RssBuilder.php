<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 24/02/2015
 * Time: 11:56
 */
namespace Core;

use Carbon\Carbon;
use Underscore\Types\Arrays;
use URL;
use View;
use Category;
use Brand;

class RssBuilder
{

    protected $nodes = [];
    private $cache = [];
    private $titles = [];
    private $xml;
    protected $config;
    protected $now;
    protected $lang;
    public static $ALLOWED_TAGS = array('B','P','I','A','EM','STRONG','H1','H2','H3','UL','LI');
    protected $category_id;
    protected $brand_id;
    protected $attribute_id;
    protected $source;

    function __construct($lang)
    {
        ini_set('max_execution_time',0);
        $this->lang = $lang;
        $this->config = \Cfg::getGroup('rss');
        $this->titles[] = \Cfg::get('SITE_TITLE');
        $this->now = date("Y-m-d");

        $this->category_id = \Input::get('c',false);
        $this->brand_id = \Input::get('b',false);
        $this->attribute_id = \Input::get('a',false);
        $this->source = \Input::get('source','product');
    }

    function build(){
        switch($this->source){
            case 'product':
                $this->handleProducts();
                break;
            case 'featured':
                $this->handleProducts( 'featured' );
                break;
            case 'promo':
                $this->handlePromo();
                break;
        }

    }

    function getTitle(){
        return implode(" ",$this->titles);
    }


    function handleProducts( $source = 'new' ){
        //\Utils::log(\Core::getLang(),__METHOD__);
        $limit = \Cfg::get('RSS_SIZE',100);
        $query = \Product::rows($this->lang)->where('feed', 1)->where('published', 1)->where('is_out_of_production', 0);

        switch($source){
            case 'featured':
                $this->titles[] = 'Featured';
                $query->where("is_featured",1);
                break;

            default:
            case 'new':
                $query->where('is_new', 1);
                break;
        }

        if($this->category_id){
            $id = $this->category_id;
            $query->where(function($q) use ($id){
                $q->where('default_category_id',$id)->orWhere('main_category_id',$id);
            });
            $obj = Category::getPublicObj($id);
            $this->titles[] = $obj->name;
        }

        if($this->brand_id){
            $id = $this->brand_id;
            $query->where('brand_id', $id);
            $obj = Brand::getPublicObj($id);
            $this->titles[] = $obj->name;
        }


        $products = $query->orderBy('created_at', 'desc')->take($limit)->get();
        foreach ($products as $product) {
            $url = \Link::create('product', $product->id, $this->lang)->absolute()->getLink();
            $data = [
                'url' => $url,
                'title' => $product->name,
                'content' => $product->getFullDescription(true),
                'description' => $product->getShortDescription(true) . " - ". \Lex::get('lbl_price').": {$product->price_final}",
                'price' => $product->price_final,
                'pubDate' => $product->created_at,
            ];
            $node = new RssNode($data);
            $images = $product->getImages();
            foreach ($images as $image) {
                $image_url = \Site::img($image->defaultImg, true);
                $caption = ($image->legend == '') ? $product->name : $image->legend;
                $node->addImage($image_url, $caption);
            }
            $this->addNode($node);
        }

    }



    function handlePromo(){
        $limit = \Cfg::get('RSS_SIZE',100);
        $this->titles[] = Lex::get('lbl_ongoing_promo');
        $rows = \PriceRule::rows($this->lang)->where('active', 1)->where('visible', 1)->orderBy('created_at', 'desc')->take($limit)->get();
        foreach ($rows as $row) {
            $row->expand();
            $url = \Link::create('promo', $row->id, $this->lang)->absolute()->getLink();
            $data = [
                'url' => $url,
                'title' => $row->name,
                'content' => $row->description,
                'description' => $row->period,
                'pubDate' => $row->created_at,
            ];
            $node = new RssNode($data);

            if($row->hasImageList){
                $image_url = \Site::img($row->image_list, true);
                $caption = $row->name;
                $node->addImage($image_url, $caption);
            }
            if($row->hasImageContent){
                $image_url = \Site::img($row->image_content, true);
                $caption = $row->name;
                $node->addImage($image_url, $caption);
            }
            $this->addNode($node);
        }
    }



    function cfg($key, $default = null)
    {
        if (isset($this->config[$key]) AND trim($this->config[$key]) != '') {
            return $this->config[$key];
        }
        return $default;
    }

    function addNode($node)
    {
        if (!isset($this->cache[$node->url])) {
            $this->nodes[] = $node;
            $this->cache[$node->url] = true;
        }

    }

    function getNodes()
    {
        return $this->nodes;
    }


    function toXml(){
        if($this->xml != null){
            return $this->xml;
        }
        $nodes = $this->nodes;
        $content = '';
        $date = date("r");
        foreach($nodes as $node){
            $content .= $node->toXml();
        }

        $siteLink = \Link::shortcut('home');
        $feedLink = URL::full();
        $qs = str_replace($siteLink,'',$feedLink);
        $feedLink = $siteLink.urlencode($qs);

        $data = [
          'content' => $content,
          'title' => $this->getTitle(),
          'feedLink' => $feedLink,
          'siteLink' => $siteLink,
          'description' => \Cfg::get('META_DESCRIPTION'),
          'pubDate' => $date,
          'generator' => \Cfg::get('application_version'),
          'lang' => $this->lang,
          'updatePeriod' => 'hourly',
          'updateFrequency' => '1',
        ];
        $this->xml = View::make('xml.rss', $data )->render();
        return $this->xml;
    }



}


class RssNode
{
    public $url;
    public $title;
    public $content;
    public $description;
    public $images = [];

    function __construct(array $params = null)
    {
        if($params){
            foreach($params as $key => $value){
                $this->$key = $value;
            }
        }
        return $this;
    }

    function addImage($url = null, $title = null, $caption = null)
    {
        $this->images[] = new RssImage($url, $title, $caption);
    }

    function toXml(){

        $template = <<<TPL
<item>
    <title>:title</title>
    <link>:url</link>
    <description><![CDATA[:description]]></description>
    <content:encoded><![CDATA[:content]]></content:encoded>
    <pubDate>:pubDate</pubDate>
    <comments>:url#comments</comments>
    <guid isPermaLink="false">:url</guid>:images:categories
</item>
TPL;

        $title = htmlspecialchars($this->title, ENT_QUOTES);
        $content = htmlspecialchars($this->content, ENT_QUOTES);
        $description = htmlspecialchars($this->description, ENT_QUOTES);
        $url = \Utils::xml_entities($this->url);
        $pubDate = date("r",strtotime($this->pubDate));
        $images = ''; $categories = '';
        foreach($this->images as $img){
            $images .= $img->toXml();
        }

        $data = compact('title','content','description','url','images','pubDate','images','categories');
        $keys = array_keys($data);
        $values = array_values($data);
        foreach($keys as $index => $key){
            $keys[$index] = ':'.$key;
        }
        return str_replace($keys,$values,$template).PHP_EOL;
    }


}


class RssImage
{
    public $url;
    public $caption;

    function __construct($url = null, $caption = null)
    {
        $this->url = $url;
        $this->caption = $caption;
        return $this;
    }

    function toXml(){
        $template = '<media:content type="image/jpeg" media="image" url="%s">%s</media:content>';
        $url = \Utils::xml_entities($this->url);
        $extra = '';
        $extra .= ($this->caption != '') ? '<media:description type="plain">'.htmlspecialchars($this->caption, ENT_QUOTES).'</media:description>' : '';
        return sprintf($template,$url,$extra);
    }

}
