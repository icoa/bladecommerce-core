<?php
namespace Core\Validators;

use Illuminate\Validation\Validator;
use DB;
/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 2-ago-2013 12.30.44
 */

class UniqueLangValidator extends Validator
{

    public function validateUniqueLang($attribute, $value, $parameters)
    {
        $id = 0;
        if(count($parameters) == 4){
            list($table, $column, $key, $id) = $parameters;
        }else{
            list($table, $column, $key) = $parameters;
        }

        $lang_id = substr($attribute, -2);
        $master_table = str_replace('_lang',null,$table);

        $query = DB::table($table)->leftJoin($master_table,'id','=',$key)->whereNull('deleted_at')->where($column, $value)->where('lang_id',$lang_id);
        if($id > 0){
            $query->where($key,'<>',$id);
        }
        $many = $query->count();

        return $many == 0;
    }

}