<?php

namespace Core;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 11.33.03
 */


use Config, DB, Log;
use Event;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\SlackWebhookHandler;
use Exception;

class Utils
{

    public $isWatching = false;
    public $canWrite = false;
    public $canRemote = false;
    protected $errorLog;
    protected $trackLog;
    protected $slackLog;
    protected $reportLog;
    protected $cronLog;
    protected $queries = [];

    function __construct()
    {
        $this->canWrite = Config::get('app.writelogs');
        $this->canRemote = Config::get('logging.remote.enabled', false);

        $this->errorLog = new Logger('error_log'); //The error logger
        $errorHandler = new RotatingFileHandler(storage_path('logs/error.log'), 60, Logger::ERROR, true, 0775);
        $errorHandler->getFormatter()->allowInlineLineBreaks(true);
        $this->errorLog->pushHandler($errorHandler);

        $this->trackLog = new Logger('track_log'); //The track logger
        $trackHandler = new RotatingFileHandler(storage_path('logs/track.log'), 60, Logger::INFO, true, 0775);
        $trackHandler->getFormatter()->allowInlineLineBreaks(true);
        $this->trackLog->pushHandler($trackHandler);

        $this->reportLog = new Logger('report_log'); //The report logger
        $reportHandler = new RotatingFileHandler(storage_path('logs/report.log'), 60, Logger::INFO, true, 0775);
        $reportHandler->getFormatter()->allowInlineLineBreaks(true);
        $this->reportLog->pushHandler($reportHandler);

        $this->cronLog = new Logger('cron_log'); //The cron logger
        $cronHandler = new RotatingFileHandler(storage_path('logs/cron.log'), 60, Logger::INFO, true, 0775);
        $cronHandler->getFormatter()->allowInlineLineBreaks(true);
        $this->cronLog->pushHandler($cronHandler);

        $webHookHandler = config('logging.remote.slack.webHook');
        $channel = config('logging.remote.slack.channel');
        $username = config('logging.remote.slack.username', 'Monolog');

        $this->slackLog = new Logger('slack_log'); //The slack logger
        $slackHandler = new SlackWebhookHandler($webHookHandler, $channel, $username);
        $this->slackLog->pushHandler($slackHandler);
    }

    function quote($str)
    {
        return strtr($str, '"', '\"');
    }

    private function _getLog($value, $message = '', $method = null)
    {
        if (!$this->canWrite) return false;
        return $this->_format($value, $message, $method);
    }

    private function _recurPrintToArray($value)
    {
        if (is_object($value) and method_exists($value, 'toArray')) {
            $value = $value->toArray();
        }
        if (is_array($value) and !empty($value)) {
            foreach ($value as $key => $val) {
                $value[$key] = $this->_recurPrintToArray($val);
            }
        }
        return $value;
    }

    private function _format($value, $message = '', $method = null)
    {
        if (is_object($value) || is_array($value)) {
            try {
                if (is_object($value) and method_exists($value, 'toArray')) {
                    $value = $value->toArray();
                }
                $value = print_r($value, true);
            } catch (Exception $e) {
                $value = 'Could not represents object since is too memory consumption';
                audit_exception($e, __METHOD__);
                audit(class_basename($value), 'CLASS BASENAME');
            }
        }

        if (is_bool($value)) {
            $value = ($value) ? 'TRUE' : 'FALSE';
        }

        if ($method and $message != '')
            $message = $message . '::' . $method;

        if ($message != '')
            $value = $message . ": " . $value;

        $elapsed = self::microtime();
        $value = "(Elapsed: $elapsed) " . $value;
        return $value;
    }

    public function log($value, $message = '', $method = null)
    {
        $m = $this->_getLog($value, $message, $method);
        if ($m != false) {
            Log::info($m);
        }
    }

    public function warning($value, $message = '', $method = null)
    {
        $value = $this->_format($value, $message, $method);
        try {
            $this->errorLog->addWarning($value);
        } catch (Exception $e) {
            $this->reportLogIncident($value, $e);
        }
    }

    public function error($value, $message = '', $method = null)
    {
        $value = $this->_format($value, $message, $method);
        try {
            $this->errorLog->error($value);
        } catch (Exception $e) {
            $this->reportLogIncident($value, $e);
        }
    }

    public function track($value, $message = '', $method = null)
    {
        $value = $this->_format($value, $message, $method);
        try {
            $this->trackLog->addInfo($value);
        } catch (Exception $e) {
            $this->reportLogIncident($value, $e);
        }
    }

    public function report($value, $message = '', $method = null)
    {
        $value = $this->_format($value, $message, $method);
        try {
            $this->reportLog->addInfo($value);
        } catch (Exception $e) {
            $this->reportLogIncident($value, $e);
        }
    }

    public function cron($value, $message = '', $method = null)
    {
        $value = $this->_format($value, $message, $method);
        try {
            $this->cronLog->addInfo($value);
        } catch (Exception $e) {
            $this->reportLogIncident($value, $e);
        }
    }

    public function remote($value, $message = '', \Exception $exception = null)
    {
        if ($this->canRemote === false)
            return;

        $value = $this->_format($value, $message);
        try {
            $username = (string)config('logging.remote.slack.username', 'Monolog');

            if ($exception) {
                $code = $this->randomString();
                $this->slackLog->addCritical("$username says: " . $value . PHP_EOL . "Incident reported as [$code]");
                $this->error($exception->getMessage(), $code);
                $this->error($exception->getTraceAsString(), $code);
            } else {
                $this->slackLog->addCritical("$username says: " . $value);
            }

        } catch (\Exception $e) {
            $this->error($e->getMessage(), __METHOD__);
            $this->error($e->getTraceAsString(), __METHOD__);
        }
    }

    public function logSql(&$queryObj, $message = '')
    {
        if (!$this->canWrite) return false;
        try {
            $queryObj->explain();
        } catch (\Exception $ex) {
            Log::info($ex->getMessage());
        }
    }

    private function reportLogIncident($value, Exception $e = null)
    {
        $message = '=== reportLogIncident::START ===' . PHP_EOL;
        $message .= $value . PHP_EOL;
        if ($e) {
            $message .= "ERROR: " . $e->getMessage() . PHP_EOL;
            $message .= "TRACE: " . $e->getTraceAsString() . PHP_EOL;
        }
        $message .= '=== reportLogIncident::END ===' . PHP_EOL;
        Log::info($message);
    }


    public function watch()
    {
        if (!$this->canWrite OR $this->isWatching) return;
        \Utils::log("Calling Utils::watch");
        $this->isWatching = true;
        \Event::listen('illuminate.query', function ($sql, $bindings, $time) {
            foreach ($bindings as $binding) {
                $binding = \DB::connection()->getPdo()->quote($binding);
                $sql = preg_replace('/\?/', $binding, $sql, 1);
            }
            Log::info("SQL WATCH($time): " . $sql);
            $this->queries[] = $sql;
        });
    }

    public function unwatch()
    {
        \Event::forget('illuminate.query');
        $this->isWatching = false;
    }

    /**
     * @return array
     */
    public function getQueries()
    {
        return $this->queries;
    }

    /**
     * @param $query
     */
    public function addQuery($query)
    {
        $this->queries[] = $query;
    }

    public function microtime()
    {
        global $___start;
        $s = microtime(true) - $___start;
        return round($s, 3);
    }

    function arrayToObject($d)
    {
        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return (object)array_map(array($this, __FUNCTION__), $d);
        } else {
            // Return object
            return $d;
        }
    }

    function objectToArray($d)
    {
        if (is_object($d)) {
            // Gets the properties of the given object
            // with get_object_vars function
            $d = get_object_vars($d);
        }

        if (is_array($d)) {
            /*
            * Return array converted to object
            * Using __FUNCTION__ (Magic constant)
            * for recursive call
            */
            return array_map(array($this, __FUNCTION__), $d);
        } else {
            // Return array
            return $d;
        }
    }


    function randomString($size = 10, $uppercase = true)
    {
        //$str = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $size);
        $str = \Str::random($size);
        if ($uppercase) $str = \Str::upper($str);
        return $str;

    }


    static function parseAttributes($string)
    {
        //Initialize variables
        $attr = array();
        $retarray = array();

        // Lets grab all the key/value pairs using a regular expression
        preg_match_all('/([\w:-]+)[\s]?=[\s]?"([^"]*)"/i', $string, $attr);

        if (is_array($attr)) {
            $numPairs = count($attr[1]);
            for ($i = 0; $i < $numPairs; $i++) {
                $retarray[$attr[1][$i]] = $attr[2][$i];
            }
        }
        return $retarray;
    }

    static function getAttributes($string)
    {
        return self::parseAttributes(self::unhtmlentities($string));
    }


    static function unhtmlentities($chaineHtml)
    {
        $tmp = get_html_translation_table(HTML_ENTITIES);
        $tmp = array_flip($tmp);
        $chaineTmp = strtr($chaineHtml, $tmp);

        return $chaineTmp;
    }

    static function xml_entities($str)
    {

        $str = preg_replace('/&/', '&amp;', $str);
        $str = preg_replace('/>/', '&gt;', $str);
        $str = preg_replace('/</', '&lt;', $str);
        $str = preg_replace('/"/', '&quot;', $str);
        $str = preg_replace('/\'/', '&apos;', $str);
        //$str = preg_replace('#\/#', '&#47;', $str);

        return $str;

    }

    static function trimSpaces($text)
    {
        $text = mb_ereg_replace('\s+', ' ', $text);
        return trim($text);
    }

    function stripTags($content)
    {
        $matches = array();
        if (preg_match_all("#\[blade:(.*)\](.*)\[\/blade:(.*)\]#isU", $content, $matches)) {
            $this->log($matches, __METHOD__);
            if (isset($matches[2])) {
                $content = $matches[2][0];
            }
        }
        return strip_tags($content);
    }


    function redisPrefix($key = null)
    {
        $prefix = \Config::get('cache.prefix');
        if ($key) $prefix .= "-" . $key;
        return $prefix;
    }

    function makeClickableLinks($s)
    {
        return preg_replace('@(https?://([-\w\.]+[-\w])+(:\d+)?(/([\w/_\.#-]*(\?\S+)?[^\.\s])?)?)@', '<a rel="nofollow" href="$1" target="_blank">$1</a>', $s);
    }

    function pagination($total, $pagesize, $current = 1, $size = 10)
    {
        $result = [];
        if ($total == 0)
            return $result;
        $maxpages = ceil($total / $pagesize);

        $half = ceil($size / 2);
        //echo "Total: $total | pagesize: $pagesize | current: $current | size:$size | maxpages: $maxpages | half: $half".PHP_EOL;
        $rangeMin = $current - $half;
        if ($rangeMin <= 0) {
            $rangeMin = 1;
        } else {
            $result[] = 1;
            $result[] = -1;
        }

        $rangeMax = $current + $half;
        if ($rangeMax >= $maxpages) {
            $rangeMax = $maxpages;
        }
        if ($rangeMax - $rangeMin <= $size) {
            $rangeMax = $rangeMin + $size - 1;
        }
        $rangeMin += count($result);

        //echo "rangeMin: $rangeMin | rangeMax: $rangeMax".PHP_EOL;
        //https://bitbucket.org/N3m3s7s/blade-ecommerce-morellato/commits/1cf2ed575ea78006b39c2b4ba0410db8362d2429
        if ($rangeMax > $maxpages) {
            $offset = $rangeMax - $maxpages;
            $rangeMin -= $offset;
            $rangeMax = $maxpages;
            if ($rangeMin < 1) $rangeMin = 1;
        }
        //echo "AFTER => rangeMin: $rangeMin | rangeMax: $rangeMax".PHP_EOL;
        $result = array_merge($result, range($rangeMin, $rangeMax));
        if (count($result) > $size OR end($result) != $maxpages) {
            $result = array_slice($result, 0, $size - 2);
            $result[] = -1;
            $result[] = $maxpages;
        }
        return $result;
    }


    function getSapSku($sku, $sap_sku)
    {
        $rightSku = $sku;
        if (strlen(trim($sap_sku)) > 0)
            $rightSku = $sap_sku;

        //$rightSku = str_replace('.','',$rightSku);
        return $rightSku;
    }

    /**
     * @param $text
     * @param string $delimiter
     * @return array
     */
    function delimiterToArray($text, $delimiter = '|')
    {
        if ($text == '' or is_null($text)) {
            return [];
        }
        $list = explode($delimiter, trim($text, $delimiter));
        return $list;
    }

    /**
     * @param $email
     * @return mixed
     */
    function isEmail($email)
    {
        $email = \Str::lower(trim($email));
        $basic = filter_var($email, FILTER_VALIDATE_EMAIL);
        if (false === $basic)
            return false;

        $domain = last(explode('@', $email));
        $black_list = (array)config('auth.customer.blacklist_domains', []);
        if (in_array($domain, $black_list))
            return false;

        return true;
    }

    /**
     * @param $lang
     * @return string
     */
    function getIsoCodeFromLang($lang)
    {
        $isoLang = strtolower($lang);
        $isoLocale = strtoupper($lang);
        if ($isoLang == 'en') {
            $isoLocale = 'US';
        }
        return "$isoLang-$isoLocale";
    }

    /**
     * @param $filePath
     * @return bool
     */
    function isImage($filePath)
    {
        if (!file_exists($filePath)) {
            return false;
        }

        $valid_types = [
            IMAGETYPE_GIF,
            IMAGETYPE_PNG,
            IMAGETYPE_JPEG,
            //IMAGETYPE_WEBP,
        ];
        $type = null;
        if (function_exists('exif_imagetype')) {
            $type = exif_imagetype($filePath);
        } else {
            $a = getimagesize($filePath);
            $type = $a[2];
        }

        return in_array($type, $valid_types);
    }

    /**
     * @param $path
     * @param string $algorithm
     * @return string
     */
    function getSriHash($path, $algorithm = 'sha384')
    {
        $algorithm = in_array($algorithm, ['sha256', 'sha384', 'sha512'])
            ? $algorithm
            : 'sha256';
        if (!file_exists($path)) {
            throw new \RuntimeException(sprintf('File "%s" does not exist', $path));
        }
        return base64_encode(hash($algorithm, file_get_contents($path), true));
    }

    /**
     * @param $theme
     * @param string $type
     * @return string|null
     */
    function getMetaSri($theme, $type = 'js')
    {
        if (true !== config('assets.sri.enabled', false)) {
            return null;
        }

        // aliasing
        if($type === 'script')
            $type = 'js';

        if($type === 'style')
            $type = 'css';

        $metadata_path = storage_path('files/themes');
        $metadata_file = $metadata_path . DIRECTORY_SEPARATOR . 'meta.json';
        try {
            $json = [];
            if (file_exists($metadata_file)) {
                $json = json_decode(file_get_contents($metadata_file), JSON_FORCE_OBJECT | JSON_OBJECT_AS_ARRAY);
            }
            return array_get($json, "{$theme}.{$type}");
        } catch (Exception $e) {
            return null;
        }
        return null;
    }


    /**
     * We'll only compress a view if none of the following conditions are met.
     * 1) <pre> or <textarea> tags
     * 2) Embedded javascript (opening <script> tag not immediately followed
     * by </script>)
     * 3) Value attribute that contains 2 or more adjacent spaces
     *
     * @param string $value the contents of the view file
     *
     * @return bool
     */
    public function shouldMinify($value)
    {
        $enabled = \Registry::remember('template.compress_html', static function () {
            return (bool)config('template.compress_html', false);
        });
        if ($enabled === false) return false;
        if (false !== strpos($value, 'skipmin')
            || preg_match('/<(pre|textarea)/', $value)
            || preg_match('/(script|var)/', $value)
            || preg_match('/value=(["\'])(.*)([ ]{2,})(.*)(["\'])/', $value)
        ) {
            return false;
        }

        return true;
    }

    /**
     * Compress the HTML output before saving it
     *
     * @param string $value the contents of the view file
     *
     * @return string
     */
    public function compileMinify($value)
    {
        if ($this->shouldMinify($value)) {
            $replace = array(
                '/<!--[^\[](.*?)[^\]]-->/s' => '',
                "/\n([\S])/" => ' $1',
                "/\r/" => '',
                "/\n/" => '',
                "/\t/" => ' ',
                '/ +/' => ' ',
            );

            return preg_replace(
                array_keys($replace), array_values($replace), $value
            );
        }

        return $value;
    }


    /**
     * Convert an URL with authority to a relative path
     *
     * @param string $src URL
     *
     * @return string mixed relative path
     */
    public function http2LinkUrlToRelativePath($src)
    {
        return strpos($src, '//') === 0 ? preg_replace('/^\/\/([^\/]*)\//', '/', $src) : preg_replace('/^http(s)?:\/\/[^\/]*/', '', $src);
    }


    /**
     * @param bool $md5
     * @return string
     */
    public function uuid($md5 = true)
    {
        $uuid = sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );

        return $md5 ? str_replace('-', '', $uuid) : $uuid;
    }

    /**
     * @param Exception $exception
     * @return string
     */
    public function getExceptionTraceAsString(Exception $exception)
    {
        $rtn = '';
        $count = 0;
        foreach ($exception->getTrace() as $frame) {
            $args = '';
            if (isset($frame['args'])) {
                $args = array();
                foreach ($frame['args'] as $arg) {
                    if (is_string($arg)) {
                        $args[] = "'" . $arg . "'";
                    } elseif (is_array($arg)) {
                        $args[] = 'Array';
                    } elseif ($arg === null) {
                        $args[] = 'NULL';
                    } elseif (is_bool($arg)) {
                        $args[] = ($arg) ? 'true' : 'false';
                    } elseif (is_object($arg)) {
                        $args[] = get_class($arg);
                    } elseif (is_resource($arg)) {
                        $args[] = get_resource_type($arg);
                    } else {
                        $args[] = $arg;
                    }
                }
                $args = implode(', ', $args);
            }
            $rtn .= sprintf("#%s %s(%s): %s(%s)\n",
                $count,
                isset($frame['file']) ? $frame['file'] : 'unknown file',
                isset($frame['line']) ? $frame['line'] : 'unknown line',
                (isset($frame['class'])) ? $frame['class'] . $frame['type'] . $frame['function'] : $frame['function'],
                $args);
            $count++;
        }
        return $rtn;
    }
}

