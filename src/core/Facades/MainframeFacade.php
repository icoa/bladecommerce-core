<?php
namespace Core\Facades;

use Illuminate\Support\Facades\Facade;

class MainframeFacade extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'echomainframe';
    }

}
