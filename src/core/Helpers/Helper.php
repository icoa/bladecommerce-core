<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 30-lug-2013 15.17.43
 */

use Carbon\Carbon;
use services\Cache\Sfc\SfcCache;
use services\Morellato\ResponseCache\ResponseCacheRepository;
use Underscore\Types\Arrays;
use Core\Admin\AdminForm;
use Core\Token;

abstract class Helper
{

    static function getAttributesTableSummary($id)
    {
        $languages = \Mainframe::languages();

        $lang_th = '';
        $lang_td = '';
        foreach ($languages as $lang) {
            $src = \AdminTpl::img("images/flags/{$lang->id}.jpg");
            $lang_th .= "<th class='center'><img class='mr5 valign_m' src='$src' /></th>";
            $lang_td .= '<td>{{ $name_' . $lang->id . ' }}</td>';
        }
        $tbody = '';

        $tmpl = '<tr><td>{{ $uname }}</td>' . $lang_td . '</tr>';

        $rows = AttributeOption::where('attribute_id', $id)->select('*')->orderBy('position')->orderBy('uname')->get();
        $rows->load('translations');
        //\Utils::log($rows->toArray());
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $row->fillLanguages();

            $data = array(
                'uname' => $row->uname
            );

            foreach ($languages as $lang) {
                $lang_id = $lang->id;
                $data['name_' . $lang_id] = $row->{"name_" . $lang_id};
            }

            $tbody .= \Theme::blader($tmpl, $data);
        }

        $table = <<<TABLE
<div class='preview preview-attribute-set'>
<table class="table table-striped">
    <thead>
        <tr>
            <th> </th>
            $lang_th            
        </tr>
    </thead>
    <tbody>
        $tbody
    </tbody>
</table> </div>
TABLE;
        return $table;
    }

    static function getAttributesTable($id, $fullTable = true)
    {

        $languages = \Mainframe::languages();

        $lang_th = '';
        $lang_td = '';
        foreach ($languages as $lang) {
            $src = \AdminTpl::img("images/flags/{$lang->id}.jpg");
            $lang_th .= "<th><img class='mr5 valign_m' src='$src' />$lang->name</th>";
            $lang_td .= '<td><input type="text" class="span12" value="{{ $name_' . $lang->id . ' }}" name="option[value][{{ $option }}][' . $lang->id . ']"></td>';
        }
        $tbody = '';

        $tmpl = '
<tr class="option-row {{ $optionClass }}" id="{{ $option }}">
    <td class="align-center"><b>{{ $id }}</b></td>
    <td class="align-center"><b>{{ $nav_id }}</b></td>
    <td><input type="text" class="span12 required" value="{{ $uname }}" name="option[value][{{ $option }}][u]"></td>
    ' . $lang_td . '
    <td class="center"  width="100"><input type="text" value="{{ $position }}" name="option[order][{{ $option }}]" class="span12 align_c"></td>
    <td class="center"  width="100"><input type="{{ $type }}" {{ $checked }} value="{{ $option }}" name="default[]" class="input-predefined"></td>
    <td class="left">
        <input type="hidden" value="{{ $delete }}" name="option[delete][{{ $option }}]" class="delete-flag">
        <button style="" onclick="deleteRow(\'{{ $option }}\');" class="btn btn-mini btn-danger" type="button"><span>Elimina</span></button>
    </td>
    <input type="hidden" value="{{ $id }}" name="option[ids][{{ $option }}]">
</tr>
';

        $old = \Input::old('option');
        if (isset($old)) {
            $type = (\Input::old('frontend_input') == 'multiselect') ? 'checkbox' : 'radio';
            $default = \Input::old('default', []);
            $value = $old['value'];
            $order = $old['order'];
            $delete = $old['delete'];
            $ids = $old['ids'];
            foreach ($value as $option => $obj) {
                $data = array(
                    'option' => $option,
                    'uname' => $obj['u'],
                    'position' => $order[$option],
                    'type' => $type,
                    'delete' => $delete[$option],
                    'optionClass' => ($delete[$option] == '1') ? 'hidden' : '',
                    'checked' => (in_array($option, $default)) ? 'checked' : '',
                    'id' => $ids[$option],
                    'nav_id' => null
                );
                foreach ($languages as $lang) {
                    $data['name_' . $lang->id] = $obj[$lang->id];
                }

                $tbody .= \Theme::blader($tmpl, $data);
            }
        } elseif ($id > 0) {
            //getting tbody from database
            $parent = Attribute::find($id);
            //$defaults =  AttributeOption::where('attribute_id', $id)->where('is_default',1)->count('id');
            $type = ($parent->frontend_input == 'multiselect') ? 'checkbox' : 'radio';

            $rows = AttributeOption::where('attribute_id', $id)->select('*')->orderBy('position')->orderBy('uname')->get();
            $rows->load('translations');
            //\Utils::log($rows->toArray());
            $counter = 0;
            foreach ($rows as $row) {
                $counter++;
                $row->fillLanguages();

                $option = "option_" . $counter;
                $id = ($fullTable) ? $row->id : 0;
                $data = array(
                    'option' => $option,
                    'uname' => $row->uname,
                    'position' => $row->position,
                    'type' => $type,
                    'delete' => 0,
                    'optionClass' => '',
                    'checked' => ($row->is_default == 1) ? 'checked' : '',
                    'id' => $id,
                    'nav_id' => $row->nav_id
                );

                foreach ($languages as $lang) {
                    $lang_id = $lang->id;
                    $data['name_' . $lang_id] = e($row->{"name_" . $lang_id});
                }

                $tbody .= \Theme::blader($tmpl, $data);
            }
        }

        if (!$fullTable) {
            return $tbody;
        }

        $order_options = '';
        foreach ($languages as $lang) {
            $lang_id = $lang->id;
            $order_options .= "<option value=\"$lang_id\">$lang->name</option>";
        }

        $select = \Form::select("import_dataset_from", \Mainframe::selectAttributesMulti($id), 0, ['class' => 'style', 'id' => 'import_dataset_from']);

        $table = <<<TABLE
<div class="control-group form-inline hidden" id="import_dataset_container">
    <div class="alert mb20">        
        Puoi scegliere un attributo di tipo "Elenco a discesa" per importare tutte le sue voci nella tabella corrente. Attenzione: questa operazione resetterà totalmente la tabella esistente.
    </div>    
    <label>Importa set di dati da</label>   
    $select   
    <button class='btn btn-small btn-success' id='import_dataset_btn'>Avvia operazione</button>
</div>
<table class="table table-striped" id="dynamicTable">
    <thead>
        <tr>
            <th class="align-center">ID</th>
            <th class="align-center">NAV ID</th>
            <th>Universale</th>
            $lang_th
            <th class="center" width="100">Posizione</th>
            <th class="center" width="100">È predefinito <button type='button' class="mini-button" onclick="javascript:Attributes.resetDefaults();">Reset</button></th>
            <th><button type="button" class="btn btn-mini btn-info addOption">Aggiungi opzione</button></th>
        </tr>
    </thead>
    <tfoot>
        <tr>
        <th>ID</th>
            <th class="align-center">NAV ID</th>
            <th class="align-center">Universale</th>
            $lang_th
            <th class="center" width="100">Posizione</th>
            <th class="center" width="100">È predefinito <button type='button' class="mini-button" onclick="javascript:Attributes.resetDefaults();">Reset</button></th>
            <th><button type="button" class="btn btn-mini btn-info addOption">Aggiungi opzione</button></th>
        </tr>
    </tfoot>
    <tbody id="dynamicTableBody">
        $tbody
    </tbody>
    
</table> 
<div class="control-group form-inline">
    <label class="checkbox"><input autocomplete="off" type="checkbox" name="auto_position" class="style" value="Y" >Imposta automaticamente le posizioni in base ai valori della colonna</label>   
    <select name="auto_position_by" class="style"  autocomplete="off" >        
        <option value="u">Universale</option>
        $order_options
    </select>    
</div>
TABLE;
        return $table;
    }

    static function getAttributesSetTable($set_id)
    {

        $table = <<<TABLE
    <thead>
        <tr>
            <th width="30" class="center"></th>
            <th width="250" class="center">Attributo</th>
            <th class="center">Valori di default</th>
        </tr>
    </thead>
    <tbody>     
TABLE;


        $attributes_position = \Input::old('attributes_position');
        //\Utils::log($attributes_position,"attributes_position OLD");
        $from_old = false;
        $from_product = (int)\Input::get('from_product', 0);
        if ($attributes_position != '') {
            //\Utils::log("FROM IDS");
            $ids = explode(",", \Input::old('attributes_position'));
            $rows = \Mainframe::attributes_by_ids($ids, 'position');
            $from_old = true;
        } else {
            //\Utils::log("FROM SET");
            if ($from_product > 0) {
                $rows = DB::table('products_attributes_position')->where("product_id", $from_product)->orderBy('position')->select('attribute_id as id')->get();
            } else {
                $rows = \Mainframe::attributes_by_set($set_id, 'position');
            }
        }


        $html = $table;
        $ids = array();
        foreach ($rows as $row) {
            $obj = new AttributeHtml($row->id);
            if ($from_old) {
                $obj->setOldValue();
            } else {
                if ($from_product > 0) {
                    $obj->setProductValue($from_product);
                } else {
                    $default_value = self::getAttributeDefaultValue($set_id, $row->id);
                    $obj->setValue($default_value);
                }
            }
            $render = $obj->getRenders();
            //Utils::log($render, "ATTRIBUTE RENDER");
            $html .= <<<ROW
<tr id='$row->id' rel='$row->id'>
    <td class="dragHandle"></td>
    <td class="align-right">
        {$render['labelHtml']}
    </td>
    <td class="center">       
        {$render['html']}
    </td>
</tr>                
ROW;
            //$html .= "<tr id='$row->id'><td>$row->name ($row->frontend_input)</td></tr>";
            $ids[] = $row->id;
        }
        $html .= '<input autocomplete="off" type="hidden" name="attributes_position" id="attributes_position" value="' . implode(",", $ids) . '" />';
        return $html;
    }

    static function getSimpleAttributesSetTable($set_id)
    {

        $table = <<<TABLE
    <thead>            
                <tr>
                <th class='center'>Attributo</th>
                <th class='center'>Valore</th>
                </tr>
    </thead>            
    <tbody>     
TABLE;

        $rows = \Mainframe::attributes_by_set($set_id, 'position');

        $html = $table;

        foreach ($rows as $row) {
            $obj = new AttributeHtml($row->id);

            $default_value = self::getAttributeDefaultValue($set_id, $row->id);
            $obj->setValue($default_value);
            $render = $obj->getPreviewRenders();
            //Utils::log($render, "ATTRIBUTE RENDER");
            $html .= <<<ROW
<tr>
    <td class="align-right width50">
        {$render['labelHtml']}
    </td>
    <td class="center">       
        {$render['preview']}
    </td>
</tr>                
ROW;
        }

        $html .= '</tbody>';

        return $html;
    }

    static function getSimpleAttributesProductTable($product_id)
    {

        $table = <<<TABLE
    <thead>            
                <tr>
                <th class='center'>Attributo</th>
                <th class='center'>Valore</th>
                </tr>
    </thead>            
    <tbody>     
TABLE;

        $query = "SELECT * FROM products_attributes_position WHERE product_id=$product_id ORDER BY position";
        $rows = DB::select($query);

        $html = $table;

        foreach ($rows as $row) {
            $obj = new AttributeHtml($row->attribute_id);
            $obj->setProductValue($product_id);
            $render = $obj->getPreviewRenders();
            //Utils::log($render, "ATTRIBUTE RENDER");
            $html .= <<<ROW
<tr>
    <td class="align-right width50">
        {$render['labelHtml']}
    </td>
    <td class="center">       
        {$render['preview']}
    </td>
</tr>                
ROW;
        }

        $html .= '</tbody>';

        return $html;
    }

    static function __getAttributesSetTable($set_id)
    {
        $attributes_position = \Input::old('attributes_position');
        //\Utils::log($attributes_position,"attributes_position OLD");
        if ($attributes_position != '') {
            \Utils::log("FROM IDS");
            $ids = explode(",", \Input::old('attributes_position'));
            $rows = \Mainframe::attributes_by_ids($ids, 'position');
        } else {
            \Utils::log("FROM SET");
            $rows = \Mainframe::attributes_by_set($set_id, 'position');
        }

        //$rows = isset(\Input::old('attributes_position')) ? \Mainframe::attributes_by_ids(\Input::old('attributes_position'),'position') : \Mainframe::attributes_by_set($set_id,'position');
        $html = '';
        $ids = array();
        foreach ($rows as $row) {
            $html .= "<tr id='$row->id'><td>$row->name ($row->frontend_input)</td></tr>";
            $ids[] = $row->id;
        }
        $html .= '<input autocomplete="off" type="hidden" name="attributes_position" id="attributes_position" value="' . implode(",", $ids) . '" />';
        return $html;
    }

    static function getAttributesSetRulesTable($set_id)
    {
        $query = "SELECT * FROM attributes_sets_rules WHERE attribute_set_id=? ORDER BY position";
        $rows = DB::select($query, [$set_id]);

        $table = '';

        foreach ($rows as $row) {
            switch ($row->target_mode) {

                case "*":
                    switch ($row->target_type) {
                        case 'CAT':
                            $msg = '<span class="label label-success">ABILITA</span> per TUTTE le <span class="label">CATEGORIE</span>';
                            break;
                        case 'COL':
                            $msg = '<span class="label label-success">ABILITA</span> per TUTTE le <span class="label">COLLEZIONI</span>';
                            break;
                        case 'BRA':
                            $msg = '<span class="label label-success">ABILITA</span> per TUTTI i <span class="label">BRANDS</span>';
                            break;
                    }
                    break;
                case "+":
                    switch ($row->target_type) {
                        case 'CAT':
                            $obj = \Category::getObj($row->target_id);
                            $msg = '<span class="label label-success">ABILITA</span> se la <span class="label">CATEGORIA</span> è <span class="label label-info">' . $obj->name . '</span>';
                            break;
                        case 'COL':
                            $obj = \Collection::getObj($row->target_id);
                            $msg = '<span class="label label-success">ABILITA</span> se la <span class="label">COLLEZIONE</span> è <span class="label label-info">' . $obj->name . '</span>';
                            break;
                        case 'BRA':
                            $obj = \Brand::getObj($row->target_id);
                            $msg = '<span class="label label-success">ABILITA</span> se il <span class="label">BRAND</span> è <span class="label label-info">' . $obj->name . '</span>';
                            break;
                    }
                    break;
                case "-":
                    switch ($row->target_type) {
                        case 'CAT':
                            $obj = \Category::getObj($row->target_id);
                            $msg = '<span class="label label-important">DISABILITA</span> se la <span class="label">CATEGORIA</span> è <span class="label label-info">' . $obj->name . '</span>';
                            break;
                        case 'COL':
                            $obj = \Collection::getObj($row->target_id);
                            $msg = '<span class="label label-important">DISABILITA</span> se la <span class="label">COLLEZIONE</span> è <span class="label label-info">' . $obj->name . '</span>';
                            break;
                        case 'BRA':
                            $obj = \Brand::getObj($row->target_id);
                            $msg = '<span class="label label-important">DISABILITA</span> se il <span class="label">BRAND</span> è <span class="label label-info">' . $obj->name . '</span>';
                            break;
                    }
                    break;

                default:
                    break;
            }

            $delete = \URL::action("AttributesSetController@postRemoveRule", $row->id);
            $table .= <<<ROW
                    <tr><td>$msg</td><td class="center">
            <ul class="table-controls">               
                <li><a title="Elimina" class="btn" onclick="EchoTable.confirmAction('$delete');" href="#"><i class="icon-remove"></i></a> </li>
            </ul></tr>
ROW;
        }


        return $table;
    }

    static function getAttributesTableForProduct($product_id, $categories_ids, $brand_id, $collection_id)
    {
        $ah = new AttributeHelper($product_id, $categories_ids, $brand_id, $collection_id);
        $attribute_sets = $ah->getForSets();

        //\Utils::log($attribute_sets);

        $table = <<<TABLE
<table class="table table-striped table-input" id="attributesTable">
    <thead>
        <tr>
            <th width="30" class="center"></th>
            <th width="250" class="center">Attributo</th>
            <th class="center">Valori</th>
        </tr>
    </thead>
    <tbody>     
TABLE;

        $ids = array();
        if ($attribute_sets):
            foreach ($attribute_sets as $set) {
                $ids[] = $set['id'];

                $table .= <<<ROW
<tr id="attribute-row-{$set['id']}"  rel="{$set['id']}">
    <td class="dragHandle"></td>
    <td class="align-right">
        {$set['labelHtml']}
    </td>
    <td class="center">       
        {$set['html']}
    </td>
</tr>                
ROW;
            }
        endif;

        $table .= "</tbody></table>";
        $table .= \Form::hidden("attribute_ids", implode(",", $ids), ['id' => 'attribute_ids', 'autocomplete' => 'off']);

        return $table;
    }

    static function getAttributesTableByTemplate($template_id)
    {
        $rows = DB::table("attributes_sets_content_cache")->where("attribute_set_id", $template_id)->orderBy("position")->select("attribute_id", "default_value")->get();

        //\Utils::log($attribute_sets);

        $table = <<<TABLE
<table class="table table-striped table-input" id="attributesTable">
    <thead>
        <tr>
            <th width="30" class="center"></th>
            <th width="250" class="center">Attributo</th>
            <th class="center">Valori</th>
        </tr>
    </thead>
    <tbody>     
TABLE;

        $ids = array();

        foreach ($rows as $row) {
            $obj = new AttributeHtml($row->attribute_id);
            $obj->setDefaultValue($row->default_value);
            $set = $obj->getRenders();
            $ids[] = $set['id'];

            $table .= <<<ROW
<tr id="attribute-row-{$set['id']}"  rel="{$set['id']}">
    <td class="dragHandle"></td>
    <td class="align-right">
        {$set['labelHtml']}
    </td>
    <td class="center">       
        {$set['html']}
    </td>
</tr>                
ROW;
        }


        $table .= "</tbody></table>";
        $table .= \Form::hidden("attribute_ids", implode(",", $ids), ['id' => 'attribute_ids', 'autocomplete' => 'off']);

        return $table;
    }

    static function getAttributeValues($id)
    {
        $query = "SELECT * FROM attributes WHERE id=$id";
        $row = DB::selectOne($query);
        $value = \Input::get("attr_" . $row->code, "");
        $data = "";
        $data_array = [];
        if (is_array($value)) {
            foreach ($value as $v) {
                $data_array[] = $v;
            }
            $data = "|" . implode("|", $data_array) . "|";
        } else {
            $data = "|$value|";
            if ($value == '')
                $data = '';
        }
        return $data;
    }

    static function parseDefaultValue($default_value)
    {
        if ($default_value == null OR $default_value == '') {
            return '';
        }
        $default_value = ltrim($default_value, "|");
        $default_value = rtrim($default_value, "|");
        $tokens = explode("|", $default_value);
        if (count($tokens) == 1) {
            return $tokens[0];
        }
        return $tokens;
    }

    static function getAttributeDefaultValue($set_id, $id)
    {
        $query = "SELECT * FROM attributes_sets_content_cache WHERE attribute_set_id=$set_id AND attribute_id=$id";
        $obj = DB::selectOne($query);
        $default_value = $obj->default_value;
        return self::parseDefaultValue($default_value);
    }

    static function getAttributesByProduct($product_id, $exclude_set_id)
    {
        $excludes = \Mainframe::attributes_by_set($exclude_set_id);
        $exclude_array = [];
        foreach ($excludes as $e) {
            $exclude_array[] = $e->id;
        }
        //$query = "SELECT attribute_id FROM products_attributes_position WHERE product_id=$product_id ORDER BY position";
        return DB::table('products_attributes_position')->where("product_id", $product_id)->whereNotIn("attribute_id", $exclude_array)->orderBy('position')->lists("attribute_id");
    }


    static function getImagesTableForSet($id)
    {
        $rows = \GroupImage::where("set_id", $id)->orderBy('position')->get();

        $path = public_path() . "/assets/groups/";
        $uri = Site::root();

        $table = <<<TABLE
<table id="imagesTable" class="table table-striped table-input">
    <thead>
        <tr>
            <th width="30" class="center"></th>
            <th width="100" class="center">Preview</th>
            <th class="center">Legenda / Dettagli</th>            
            <th width="30" class="center">Visibile</th>
            <th width="30" class="center">Azioni</th>
        </tr>
    </thead>
    <tbody> 
TABLE;
        $ids = array();
        if (count($rows) == 0) {
            $table .= "<tr><td colspan=99><p>Non sono presenti immagini per questo Set. Utilizza l'uploader di Immagini per caricare il materiale.</p></td></tr>";
        } else {

            $af = new AdminForm();

            if ($rows):
                //\Utils::log($rows,"IMAGE ROWS");

                foreach ($rows as $row) {

                    //\Utils::log($row->filename,"IMAGE ROWS");
                    $ids[] = $row->id;

                    $translations = $row->translations;
                    $ds = array();
                    foreach ($translations as $tr) {
                        //\Utils::log($tr,"IMAGE TRANSLATIONS");
                        //$ds[$tr->lang_id."_".$row->id."_legend"] = $tr->legend;
                        $ds[$row->id . "_legend_" . $tr->lang_id] = $tr->legend;
                    }


                    $realfile = $path . $row->filename;
                    $af->populate($ds);
                    $input = $af->lang_textarea($row->id . "_legend", ['class' => "span12", 'rows' => 2]);
                    $image = \Image::make($realfile);

                    //\Utils::log($image,"IMAGE OBJ");
                    $dimensions = $image->getWidth() . " X " . $image->getHeight();
                    $type = Str::upper($image->extension);
                    $size = File::size($realfile);
                    $size_str = Format::bytes($size);


                    $active_action = ($row->active == 1) ? '<a data-placement="top" class="btn hovertip btn-success" data-action="' . URL::action("GroupsImagesController@postChangeFlag", [$row->id, "active", 0]) . '" href="javascript:;" onclick="Echo.xhrAction(this,false,ImagesSets.saveImagesTable);" tabindex="-1" data-original-title="Disattiva immagine"><i class="icon-ok"></i></a>' : '<a data-placement="top" class="btn hovertip" data-action="' . URL::action("GroupsImagesController@postChangeFlag", [$row->id, "active", 1]) . '" href="javascript:;" onclick="Echo.xhrAction(this,false,ImagesSets.saveImagesTable);" tabindex="-1" data-original-title="Abilita immagine"><i class="icon-remove"></i></a>';
                    $remove_action = '<a data-placement="top" class="btn hovertip btn-danger" data-action="' . URL::action("GroupsImagesController@postRemove", $row->id) . '" href="javascript:;" onclick="Echo.xhrAction(this,true,ImagesSets.saveImagesTable);" tabindex="-1" data-original-title="Elimina"><i class="icon-minus"></i></a>';

                    $table .= <<<ROW
<tr id="image-row-{$row->id}"  rel="{$row->id}">
    <td class="dragHandle"></td>
    <td class="align-left">
        <a href="$uri/assets/groups/{$row->filename}" class="fancybox">
            <img class="valign_middle tblImgPreview" src="$uri/images/small/assets/groups/{$row->filename}" />
        </a>
    </td>
    <td class="align-left">
        $input
        <div class="clear"></div>
        <strong>Dimensioni:</strong> $dimensions <strong class="ml20">Tipo:</strong> $type <strong class="ml20">Taglia:</strong> $size_str
    </td>    
    <td class="center">  
        <ul class="table-controls">                                                                                           
                <li>$active_action</li>
            </ul>
    </td>
    <td class="center">
        <ul class="table-controls">                                                                                           
                <li>$remove_action</li>
            </ul>
    </td>
</tr>
ROW;
                }
            endif;
        }

        $table .= "</tbody></table>";
        $ids_val = (is_array($ids)) ? implode(",", $ids) : "";
        $table .= \Form::hidden("image_ids", $ids_val, ['id' => 'image_ids', 'autocomplete' => 'off']);

        return $table;

    }


    static function getGeneralRuleMessage($row)
    {
        switch ($row->target_mode) {

            case "*":
                switch ($row->target_type) {
                    case 'CAT':
                        $msg = '<span class="label label-success">ABILITA</span> per TUTTE le <span class="label">CATEGORIE</span>';
                        break;
                    case 'COL':
                        $msg = '<span class="label label-success">ABILITA</span> per TUTTE le <span class="label">COLLEZIONI</span>';
                        break;
                    case 'BRA':
                        $msg = '<span class="label label-success">ABILITA</span> per TUTTI i <span class="label">BRANDS</span>';
                        break;
                    case 'ATT':
                        $msg = '<span class="label label-success">ABILITA</span> per tutti gli <span class="label">ATTRIBUTI</span>';
                        break;
                }
                break;
            case "+":
                switch ($row->target_type) {
                    case 'CAT':
                        $obj = \Category::getObj($row->target_id);
                        $msg = '<span class="label label-success">ABILITA</span> se la <span class="label">CATEGORIA</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                    case 'COL':
                        $obj = \Collection::getObj($row->target_id);
                        $msg = '<span class="label label-success">ABILITA</span> se la <span class="label">COLLEZIONE</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                    case 'BRA':
                        $obj = \Brand::getObj($row->target_id);
                        $msg = '<span class="label label-success">ABILITA</span> se il <span class="label">BRAND</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                    case 'ATT':
                        $obj = \AttributeOption::getObj($row->target_id);
                        $att_obj = \Attribute::getObj($obj->attribute_id);
                        $msg = '<span class="label label-success">ABILITA</span> se l\'attributo <span class="label">' . $att_obj->name . '</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                }
                break;
            case "-":
                switch ($row->target_type) {
                    case 'CAT':
                        $obj = \Category::getObj($row->target_id);
                        $msg = '<span class="label label-important">DISABILITA</span> se la <span class="label">CATEGORIA</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                    case 'COL':
                        $obj = \Collection::getObj($row->target_id);
                        $msg = '<span class="label label-important">DISABILITA</span> se la <span class="label">COLLEZIONE</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                    case 'BRA':
                        $obj = \Brand::getObj($row->target_id);
                        $msg = '<span class="label label-important">DISABILITA</span> se il <span class="label">BRAND</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                    case 'ATT':
                        $obj = \AttributeOption::getObj($row->target_id);
                        $att_obj = \Attribute::getObj($obj->attribute_id);
                        $msg = '<span class="label label-important">DISABILITA</span> se l\'attributo <span class="label">' . $att_obj->name . '</span> è <span class="label label-info">' . $obj->name . '</span>';
                        break;
                }
                break;

            default:
                break;
        }
        return $msg;
    }


    static function getImagesSetRulesTable($set_id)
    {
        $query = "SELECT * FROM images_sets_rules WHERE image_set_id=? ORDER BY position";
        $rows = DB::select($query, [$set_id]);

        $table = '';

        foreach ($rows as $row) {
            $msg = self::getGeneralRuleMessage($row);

            $delete = \URL::action("ImagesSetController@postRemoveRule", $row->id);
            $table .= <<<ROW
                    <tr><td>$msg</td><td class="center">
            <ul class="table-controls">               
                <li><a title="Elimina" class="btn" onclick="EchoTable.confirmAction('$delete');" href="#"><i class="icon-remove"></i></a> </li>
            </ul></tr>
ROW;
        }

        return $table;
    }

    static function getSeoBlocksRulesTable($block_id)
    {
        $query = "SELECT * FROM seo_blocks_rules WHERE seo_block_id=? ORDER BY position";
        $rows = DB::select($query, [$block_id]);

        $table = '';

        foreach ($rows as $row) {
            $msg = self::getGeneralRuleMessage($row);

            $delete = \URL::action("SeoBlocksController@postRemoveRule", $row->id);
            $table .= <<<ROW
                    <tr><td>$msg</td><td class="center">
            <ul class="table-controls">               
                <li><a title="Elimina" class="btn" onclick="EchoTable.confirmAction('$delete');" href="#"><i class="icon-remove"></i></a> </li>
            </ul></tr>
ROW;
        }

        return $table;
    }

    static function getSeoTextsRulesTable($block_id)
    {
        $query = "SELECT * FROM seo_texts_rules WHERE seo_text_id=? ORDER BY position";
        $rows = DB::select($query, [$block_id]);

        $table = '';

        foreach ($rows as $row) {
            $msg = self::getGeneralRuleMessage($row);

            $delete = \URL::action("SeoTextsController@postRemoveRule", $row->id);
            $table .= <<<ROW
                    <tr><td>$msg</td><td class="center">
            <ul class="table-controls">               
                <li><a title="Elimina" class="btn" onclick="EchoTable.confirmAction('$delete');" href="#"><i class="icon-remove"></i></a> </li>
            </ul></tr>
ROW;
        }

        return $table;
    }


    static function getPreviewImagesForSet($obj)
    {
        $query = "SELECT * FROM images_groups WHERE set_id=$obj->id ORDER BY position";
        $rows = DB::select($query);
        $table = '';
        $thead = '<thead><tr><th width="100" class="center">Immagine</th><th class="center">Dettagli</th><th width="50" class="center">Posizione</th></tr></thead>';
        $tbody = '<tbody>';
        if (count($rows) > 0) {
            $path = public_path() . "/assets/groups/";
            $uri = Site::root();
            foreach ($rows as $row) {
                $realfile = $path . $row->filename;

                $dimensions = $type = $size = $size_str = 'N.D.';

                if (File::exists($realfile) AND is_readable($realfile)) {
                    try {
                        $image = \Image::make($realfile);
                        $dimensions = $image->getWidth() . " X " . $image->getHeight();
                        $type = Str::upper($image->extension);
                        $size = File::size($realfile);
                        $size_str = Format::bytes($size);
                    } catch (Exception $e) {

                    }
                }


                $tbody .= <<<ROW
<tr><td class="center">        
            <img style="max-width:80px" class="valign_middle tblImgPreview" src="$uri/images/small/assets/groups/{$row->filename}" />
    </td>
    <td class="center">        
        <strong>Dimensioni:</strong> $dimensions <strong class="ml20">Tipo:</strong> $type <strong class="ml20">Taglia:</strong> $size_str
    </td>   
    <td class="center">        
        $row->position
    </td>  
</tr>
ROW;
            }
        } else {
            $tbody .= '<tr><td colspan=9>Nessun immagine presente per questo Set</td></tr>';
        }
        $tbody .= '</tbody>';
        $table = $thead . $tbody;

        return $table;
    }


    static function setCategoriesReverse($category_id, &$data)
    {
        if ($category_id > 0) {
            $obj = \Category::getObj($category_id);
            if ($obj) {
                $data[] = $obj->id;
                if ($obj->parent_id > 0)
                    self::setCategoriesReverse($obj->parent_id, $data);
            }
        }
    }


    static public function newSeoBlocksForSeoTexts($seo_text_id)
    {
        $obj = \SeoText::withTrashed()->find($seo_text_id);
        $ids_add = [];
        $blocks = \SeoBlock::where("active", 1)->where("context", "seotext")->orderBy("xtype")->orderBy("position")->get();
        foreach ($blocks as $block) {
            if (($block->xtype == 'h1' OR $block->xtype == 'title') AND $block->isDefault == 1) {
                continue;
            }
            if ($block->solveByObject($obj)) {
                $block->solved = true;
                $ids_add[] = $block;
                //\Utils::log("RESOLVED: $block->id", __METHOD__);
            } else {
                if ($block->conditions == null OR trim($block->conditions) == '') {
                    $block->solved = false;
                    $ids_add[] = $block;
                }
            }
        }
        return $ids_add;
    }

    static public function getSeoBlocksForSeoTexts($seo_text_id)
    {
        $blocks = self::newSeoBlocksForSeoTexts($seo_text_id);
        return count($blocks) ? $blocks : null;
        $obj = \SeoText::withTrashed()->find($seo_text_id);
        $ids_add = [];
        $ids_remove = [];
        //\Utils::watch();

        $ids_add = array_merge($ids_add, \DB::table("seo_blocks_rules")->where("target_mode", "*")->where("target_id", 0)->lists("seo_block_id"));
        $ids_remove = array_merge($ids_remove, \DB::table("seo_blocks_rules")->where("target_mode", "-")->where("target_id", 0)->lists("seo_block_id"));


        if ($obj->category_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("seo_blocks_rules")->where("target_type", "CAT")->where("target_mode", "+")->where("target_id", $obj->category_id)->lists("seo_block_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("seo_blocks_rules")->where("target_type", "CAT")->where("target_mode", "-")->where("target_id", $obj->category_id)->lists("seo_block_id"));
        }

        if ($obj->collection_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("seo_blocks_rules")->where("target_type", "COL")->where("target_mode", "+")->where("target_id", $obj->collection_id)->lists("seo_block_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("seo_blocks_rules")->where("target_type", "COL")->where("target_mode", "-")->where("target_id", $obj->collection_id)->lists("seo_block_id"));
        }
        if ($obj->brand_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("seo_blocks_rules")->where("target_type", "BRA")->where("target_mode", "+")->where("target_id", $obj->brand_id)->lists("seo_block_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("seo_blocks_rules")->where("target_type", "BRA")->where("target_mode", "-")->where("target_id", $obj->brand_id)->lists("seo_block_id"));
        }

        $ids_add = array_unique($ids_add);
        $ids_remove = array_unique($ids_remove);
        $ids = array_diff($ids_add, $ids_remove);

        //check if every rule of every set is satisfied
        $final_ids = [];
        if (count($ids) > 0) {
            foreach ($ids as $id) {
                $rules = \DB::table("seo_blocks_rules")->where("seo_block_id", $id)->get();
                $pivot = [];
                foreach ($rules as $rule) {
                    //\Utils::log($rule, "RULE");
                    switch ($rule->target_mode) {
                        case "+":
                            switch ($rule->target_type) {
                                case "CAT":
                                    $pivot[] = ($obj->category_id == $rule->target_id) ? 1 : 0;
                                    break;
                                case "BRA":
                                    $pivot[] = ($obj->brand_id == $rule->target_id) ? 1 : 0;
                                    break;
                                case "COL":
                                    $pivot[] = ($obj->collection_id == $rule->target_id) ? 1 : 0;
                                    break;
                            }
                            break;

                        case "-":
                            switch ($rule->target_type) {
                                case "CAT":
                                    $pivot[] = ($obj->category_id != $rule->target_id) ? 1 : 0;
                                    break;
                                case "BRA":
                                    $pivot[] = ($obj->brand_id != $rule->target_id) ? 1 : 0;
                                    break;
                                case "COL":
                                    $pivot[] = ($obj->collection_id != $rule->target_id) ? 1 : 0;
                                    break;
                            }
                            break;

                        case "*":
                            switch ($rule->target_type) {
                                case "CAT":
                                    $pivot[] = ($obj->category_id > 0) ? 1 : 0;
                                    break;
                                case "BRA":
                                    $pivot[] = ($obj->brand_id > 0) ? 1 : 0;
                                    break;
                                case "COL":
                                    $pivot[] = ($obj->collection_id > 0) ? 1 : 0;
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                    //\Utils::log($pivot, "PIVOT");
                }

                if (array_sum($pivot) == count($rules)) {
                    $final_ids[] = $id;
                }

            }
        }

        //\Utils::log($final_ids, "final IDS");
        if (count($final_ids) > 0) {

            $blocks = \SeoBlock::whereIn("id", $final_ids)->where("active", 1)->where("context", "seotext")->orderBy("xtype")->orderBy("position")->get();
            //\Utils::log($blocks, "final blocks");
            return $blocks;
        }

        return null;
    }


    static function generateSeo($product_id)
    {
        $blocks = self::getSeoBlocksForSeoTexts($product_id);
        $xtypes = \Mainframe::seoXTypes();
        $data = [];
        $languages = \Mainframe::languagesCodes();

        $model = \SeoText::find($product_id);
        $token = new Token("");
        $token->set("brand_id", $model->brand_id);
        $token->set("collection_id", $model->collection_id);
        $token->set("category_id", $model->category_id);
        $token->set("trend_id", $model->trend_id);
        $token->set("default_category_id", $model->category_id);

        $model->expandIf();
        //\Utils::log($model->toArray(),"AAAAAAAAAAAAA");
        if (isset($model->attributes_schema)) {
            foreach ($model->attributes_schema as $key => $value) {
                $token->set("attr:" . $key, $value);
            }
        }


        foreach ($languages as $lang) {
            $data[$lang] = [];
            foreach ($xtypes as $key => $value) {
                $data[$lang][$key] = "";
            }
        }

        $fields = [];
        if ($blocks):
            foreach ($blocks as $block) {
                $key = $block->xtype;
                foreach ($languages as $lang) {
                    $data[$lang][$key] .= " " . $block->{$lang}->content;
                    $data[$lang][$key] = ltrim($data[$lang][$key], " ");
                    $token->setLang($lang);
                    $token->rebind($data[$lang][$key]);
                    try {
                        $data[$lang][$key] = $token->render();
                    } catch (Exception $ex) {
                        $data[$lang][$key] = "";
                    }

                }
            }

            foreach ($data as $lang => $arr) {
                foreach ($arr as $key => $value) {
                    if ($key != 'h1') {
                        $key = "meta" . $key;
                    }
                    $fields[$key . "_" . $lang] = $value;
                }
            }
        endif;
        return $fields;
    }


    static function uncache()
    {
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            $master = \Utils::redisPrefix("catalog-" . $lang);
            \Redis::del($master);
            \Cache::forget("catalog-seo-blocks-{$lang}");
        }
        $cache = \Cache::driver(config('cache.queryStore', 'redis'))->tags(['catalogQueries']);
        $cache->flush();
    }

    static function purgeAllTaggedCached($with_frontend = false)
    {
        \Cache::driver(config('cache.queryStore', 'redis'))->tags(['catalogQueries'])->flush();
        \Cache::driver(config('cache.product_driver', 'apc'))->tags(['price-rules-products'])->flush();
        \Cache::driver(config('cache.product_driver', 'apc'))->tags(['products'])->flush();
        if ($with_frontend) {
            app(ResponseCacheRepository::class)->flush();
            app(SfcCache::class)->flush();
        }
        $log_file = storage_path('logs/usage.log');
        $msg = 'All tagged cache have been purged';

        $msg = date('Y-m-d h:i:s') . ' - ' . $msg;
        try{
            $user = Sentry::getUser();
            if($user){
                $msg .= ' by ' . $user->getLoginName();
            }
        }catch (Exception $e){

        }
        File::append($log_file,  $msg . PHP_EOL);
    }

    static function copy($source, $target)
    {
        if (!is_dir($source)) {//it is a file, do a normal copy
            $fileDir = dirname($target);
            @mkdir(rtrim($fileDir, "/"), 0777, true);
            copy($source, $target);
            return;
        }

        //it is a folder, copy its files & sub-folders
        @mkdir(rtrim($target, "/"), 0777, true);
        $d = dir($source);
        $navFolders = array('.', '..');
        while (false !== ($fileEntry = $d->read())) {//copy one by one
            //skip if it is navigation folder . or ..
            if (in_array($fileEntry, $navFolders)) {
                continue;
            }

            //do copy
            $s = "$source/$fileEntry";
            $t = "$target/$fileEntry";
            self::copy($s, $t);
        }
        $d->close();
    }


    static function less($asset)
    {
        $path = 'public/' . \Theme::path();
        $assetTarget = str_replace(['css', '.less'], ['cache', '.css'], $asset);
        $file = $path . "/assets/" . $asset;

        if (file_exists($file)) {
            $less = new lessc;
            $target = $path . "/assets/" . $assetTarget;

            try {
                $less->checkedCompile($file, $target);
                return $assetTarget;
            } catch (\Exception $e) {
                \Utils::error($e->getMessage());
                \Utils::error($e->getTraceAsString());
            }
        }
    }

    static function rewrite($f, $root)
    {
        $docRoot = $root;
        $tempDir = "C:/Temp/";

        $cssFile = $root . $f;
        $info = pathinfo($cssFile);
        $currentDir = $info["dirname"];
        $style = Cssurirewriter::rewrite(file_get_contents($cssFile), $currentDir, $docRoot);
        $style = str_replace(["/js/", "url('/", 'url("/'], ["../js/", "url('../", 'url("../'], $style);
        //echo $style;
        file_put_contents($tempDir . $info["basename"], $style);
        return $tempDir . $info["basename"];

    }

    static function publishAssets($folder = 'frontend')
    {

        $commands = [];

        Theme::theme($folder)->layout('default');

        $version = Theme::getConfig('assetVersioning');
        $assets = Theme::getConfig('assets');

        $files = array();
        $rows = $assets['footer']();
        foreach ($rows as $asset) {
            $files[] = $asset;
        }

        $root = public_path(\Theme::path() . '/assets/');
        $minFile = $root . "js/blade_$version.js";

        //$cmd = (bool)config('plugins.FlyingPages') ? "terser -c toplevel,sequences=false --mangle --" : "uglifyjs";
        $cmd = "uglifyjs";
        foreach ($files as $f) {
            $cmd .= " " . $root . $f;
        }

        //enforce general assets to be compiled or included in the frontend
        $enforced = config('assets.enforce', []);
        foreach ($enforced as $item) {
            if (file_exists(public_path($item)))
                $cmd .= " " . public_path($item);
        }

        //$char = (bool)config('plugins.FlyingPages') ? ">" : "-o";
        $char = "-o";

        $cmd .= " $char $minFile";
        $commands[] = $cmd;

        $css = array();
        $rows = $assets['head']();
        $rewrite = $assets['rewrite']();
        foreach ($rows as $asset) {
            //\Utils::log( $asset );
            if (substr($asset, -4) == 'less') {
                $asset = self::less($asset);
            }
            if (in_array($asset, $rewrite)) {
                $css[] = self::rewrite($asset, $root);
            } else {
                $css[] = $asset;
            }
        }


        $minFile = $root . "css/blade_$version.css";


        $cmd = "uglifycss";
        foreach ($css as $f) {
            if (substr($f, 0, 2) == "C:") {
                $cmd .= " " . $f;
            } else {
                $cmd .= " " . $root . $f;
            }

        }
        $cmd .= " > $minFile";
        $commands[] = $cmd;


        $minFile = $root . "css/ie1_blade_$version.css";


        $cmd = "uglifycss";
        foreach ($css as $f) {
            if ($f == 'js/plugins/revslider/css/captions-original.css') {
                $cmd .= " > $minFile";
                $commands[] = $cmd;
                $cmd = "uglifycss";
                $minFile = $root . "css/ie2_blade_$version.css";
            }
            if ($f == 'css/topnav2.css') {
                $cmd .= " > $minFile";
                $commands[] = $cmd;
                $cmd = "uglifycss";
                $minFile = $root . "css/ie3_blade_$version.css";
            }
            if (substr($f, 0, 2) == "C:") {
                $cmd .= " " . $f;
            } else {
                $cmd .= " " . $root . $f;
            }
        }
        $cmd .= " > $minFile";
        $commands[] = $cmd;

        return $commands;

    }

}


class Cssurirewriter
{

    /**
     * rewrite() and rewriteRelative() append debugging information here
     * @var string
     */
    public static $debugText = '';

    /**
     * In CSS content, rewrite file relative URIs as root relative
     *
     * @param string $css
     *
     * @param string $currentDir The directory of the current CSS file.
     *
     * @param string $docRoot The document root of the web site in which
     * the CSS file resides (default = $_SERVER['DOCUMENT_ROOT']).
     *
     * @param array $symlinks (default = array()) If the CSS file is stored in
     * a symlink-ed directory, provide an array of link paths to
     * target paths, where the link paths are within the document root. Because
     * paths need to be normalized for this to work, use "//" to substitute
     * the doc root in the link paths (the array keys). E.g.:
     * <code>
     * array('//symlink' => '/real/target/path') // unix
     * array('//static' => 'D:\\staticStorage')  // Windows
     * </code>
     *
     * @return string
     */
    public static function rewrite($css, $currentDir, $docRoot = null, $symlinks = array())
    {
        self::$_docRoot = self::_realpath(
            $docRoot ? $docRoot : $_SERVER['DOCUMENT_ROOT']
        );
        self::$_currentDir = self::_realpath($currentDir);
        self::$_symlinks = array();

        // normalize symlinks
        foreach ($symlinks as $link => $target) {
            $link = ($link === '//') ? self::$_docRoot : str_replace('//', self::$_docRoot . '/', $link);
            $link = strtr($link, '/', DIRECTORY_SEPARATOR);
            self::$_symlinks[$link] = self::_realpath($target);
        }

        self::$debugText .= "docRoot    : " . self::$_docRoot . "\n"
            . "currentDir : " . self::$_currentDir . "\n";
        if (self::$_symlinks) {
            self::$debugText .= "symlinks : " . var_export(self::$_symlinks, 1) . "\n";
        }
        self::$debugText .= "\n";

        $css = self::_trimUrls($css);

        // rewrite
        $css = preg_replace_callback('/@import\\s+([\'"])(.*?)[\'"]/'
            , array(self::$className, '_processUriCB'), $css);
        $css = preg_replace_callback('/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
            , array(self::$className, '_processUriCB'), $css);

        return $css;
    }

    /**
     * In CSS content, prepend a path to relative URIs
     *
     * @param string $css
     *
     * @param string $path The path to prepend.
     *
     * @return string
     */
    public static function prepend($css, $path)
    {
        self::$_prependPath = $path;

        $css = self::_trimUrls($css);

        // append
        $css = preg_replace_callback('/@import\\s+([\'"])(.*?)[\'"]/'
            , array(self::$className, '_processUriCB'), $css);
        $css = preg_replace_callback('/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
            , array(self::$className, '_processUriCB'), $css);

        self::$_prependPath = null;
        return $css;
    }

    /**
     * Get a root relative URI from a file relative URI
     *
     * <code>
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       '../img/hello.gif'
     *     , '/home/user/www/css'  // path of CSS file
     *     , '/home/user/www'      // doc root
     * );
     * // returns '/img/hello.gif'
     *
     * // example where static files are stored in a symlinked directory
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       'hello.gif'
     *     , '/var/staticFiles/theme'
     *     , '/home/user/www'
     *     , array('/home/user/www/static' => '/var/staticFiles')
     * );
     * // returns '/static/theme/hello.gif'
     * </code>
     *
     * @param string $uri file relative URI
     *
     * @param string $realCurrentDir realpath of the current file's directory.
     *
     * @param string $realDocRoot realpath of the site document root.
     *
     * @param array $symlinks (default = array()) If the file is stored in
     * a symlink-ed directory, provide an array of link paths to
     * real target paths, where the link paths "appear" to be within the document
     * root. E.g.:
     * <code>
     * array('/home/foo/www/not/real/path' => '/real/target/path') // unix
     * array('C:\\htdocs\\not\\real' => 'D:\\real\\target\\path')  // Windows
     * </code>
     *
     * @return string
     */
    public static function rewriteRelative($uri, $realCurrentDir, $realDocRoot, $symlinks = array())
    {
        // prepend path with current dir separator (OS-independent)
        $path = strtr($realCurrentDir, '/', DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR . strtr($uri, '/', DIRECTORY_SEPARATOR);

        self::$debugText .= "file-relative URI  : {$uri}\n"
            . "path prepended     : {$path}\n";

        // "unresolve" a symlink back to doc root
        foreach ($symlinks as $link => $target) {
            if (0 === strpos($path, $target)) {
                // replace $target with $link
                $path = $link . substr($path, strlen($target));

                self::$debugText .= "symlink unresolved : {$path}\n";

                break;
            }
        }
        // strip doc root
        $path = substr($path, strlen($realDocRoot));

        self::$debugText .= "docroot stripped   : {$path}\n";

        // fix to root-relative URI

        $uri = strtr($path, '/\\', '//');

        $uri = self::removeDots($uri);

        self::$debugText .= "traversals removed : {$uri}\n\n";

        return $uri;
    }

    /**
     * Remove instances of "./" and "../" where possible from a root-relative URI
     * @param string $uri
     * @return string
     */
    public static function removeDots($uri)
    {
        $uri = str_replace('/./', '/', $uri);
        // inspired by patch from Oleg Cherniy
        do {
            $uri = preg_replace('@/[^/]+/\\.\\./@', '/', $uri, 1, $changed);
        } while ($changed);
        return $uri;
    }

    /**
     * Defines which class to call as part of callbacks, change this
     * if you extend Minify_CSS_UriRewriter
     * @var string
     */
    protected static $className = 'Cssurirewriter';

    /**
     * Get realpath with any trailing slash removed. If realpath() fails,
     * just remove the trailing slash.
     *
     * @param string $path
     *
     * @return mixed path with no trailing slash
     */
    protected static function _realpath($path)
    {
        $realPath = realpath($path);
        if ($realPath !== false) {
            $path = $realPath;
        }
        return rtrim($path, '/\\');
    }

    /**
     * @var string directory of this stylesheet
     */
    private static $_currentDir = '';

    /**
     * @var string DOC_ROOT
     */
    private static $_docRoot = '';

    /**
     * @var array directory replacements to map symlink targets back to their
     * source (within the document root) E.g. '/var/www/symlink' => '/var/realpath'
     */
    private static $_symlinks = array();

    /**
     * @var string path to prepend
     */
    private static $_prependPath = null;

    private static function _trimUrls($css)
    {
        return preg_replace('/
            url\\(      # url(
            \\s*
            ([^\\)]+?)  # 1 = URI (assuming does not contain ")")
            \\s*
            \\)         # )
        /x', 'url($1)', $css);
    }

    private static function _processUriCB($m)
    {
        // $m matched either '/@import\\s+([\'"])(.*?)[\'"]/' or '/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
        $isImport = ($m[0][0] === '@');
        // determine URI and the quote character (if any)
        if ($isImport) {
            $quoteChar = $m[1];
            $uri = $m[2];
        } else {
            // $m[1] is either quoted or not
            $quoteChar = ($m[1][0] === "'" || $m[1][0] === '"') ? $m[1][0] : '';
            $uri = ($quoteChar === '') ? $m[1] : substr($m[1], 1, strlen($m[1]) - 2);
        }
        // analyze URI
        if ('/' !== $uri[0]                  // root-relative
            && false === strpos($uri, '//')  // protocol (non-data)
            && 0 !== strpos($uri, 'data:')   // data protocol
        ) {
            // URI is file-relative: rewrite depending on options
            if (self::$_prependPath === null) {
                $uri = self::rewriteRelative($uri, self::$_currentDir, self::$_docRoot, self::$_symlinks);
            } else {
                $uri = self::$_prependPath . $uri;
                if ($uri[0] === '/') {
                    $root = '';
                    $rootRelative = $uri;
                    $uri = $root . self::removeDots($rootRelative);
                } elseif (preg_match('@^((https?\:)?//([^/]+))/@', $uri, $m) && (false !== strpos($m[3], '.'))) {
                    $root = $m[1];
                    $rootRelative = substr($uri, strlen($root));
                    $uri = $root . self::removeDots($rootRelative);
                }
            }
        }
        return $isImport ? "@import {$quoteChar}{$uri}{$quoteChar}" : "url({$quoteChar}{$uri}{$quoteChar})";
    }


}
