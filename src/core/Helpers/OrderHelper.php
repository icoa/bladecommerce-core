<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-gen-2014 15.29.08
 */

use Carbon\Carbon;
use Underscore\Types\Arrays;
use services\Morellato\Gls\Handler\OrderAddressHandler;

abstract class OrderHelper
{

    /**
     * @param $order_id
     * @return Order|null
     */
    static function getOrder($order_id)
    {
        return Order::getObj($order_id);
    }

    static function getRma($rma_id)
    {
        $rma = Rma::getObj($rma_id);
        if ($rma == null) {
            return null;
        }
        $rma->expand();
        return $rma;
    }

    static function getStatusTable($order_id, $type = 'O')
    {
        $order = self::getOrder($order_id);
        $rows = OrderHistory::where('order_id', $order_id)->whereType($type)->orderBy('id', 'desc')->get();
        foreach ($rows as $row) {
            $user = $row->created_by > 0 ? Sentry::findUserById($row->created_by) : null;
            $row->status = ($type == 'O') ? OrderState::getObj($row->status_id) : PaymentState::getObj($row->status_id);
            $row->user = ($user) ? $user->first_name . " " . $user->last_name : "";
        }
        return Theme::partial('order.status', ['rows' => $rows]);
    }

    static function getStatusSelect($order_id, $acl = false)
    {
        $order = self::getOrder($order_id);
        $html = Form::select('status_id', \Mainframe::selectOrderStates($acl, $acl), $order->status);
        return $html;
    }

    static function getPaymentStatusSelect($order_id, $acl = false)
    {
        $order = self::getOrder($order_id);
        $html = Form::select('payment_status_id', \Mainframe::selectPaymentStates($acl, $acl), $order->payment_status);
        return $html;
    }

    static function getCustomerInfo($customer_id)
    {
        $customer = Customer::getObj($customer_id);
        if ($customer == null) {
            return null;
        }
        return Theme::partial('order.customer', ['obj' => $customer]);
    }

    static function getOrderInfo($order_id)
    {
        $order = Order::getObj($order_id);
        if ($order == null) {
            return null;
        }
        $order->expand();
        return Theme::partial('shared.order', ['obj' => $order]);
    }

    static function getRmaDetails($id)
    {
        $rma = Rma::getObj($id);
        if ($rma == null) {
            return null;
        }
        $rma->expand();
        return Theme::partial('shared.rma', ['obj' => $rma]);
    }

    static function getOrderRma($order_id)
    {
        $order = self::getOrder($order_id);
        $rows = $order->getRma();
        return Theme::partial('order.rmas', ['rows' => $rows]);
    }

    static function getCartDetails($id)
    {
        $cart = Cart::getObj($id);
        if ($cart == null) {
            return null;
        }
        $cart->expand();
        return Theme::partial('shared.cart', ['obj' => $cart]);
    }

    static function getAddressSelect($order_id, $billing = 0)
    {
        $order = self::getOrder($order_id);
        $customer = $order->getCustomer();
        if ($customer == null) {
            return null;
        }
        $rows = $customer->getAddresses($billing);
        if (count($rows) == 0 AND $billing == 1) {
            $rows = $customer->getAddresses(0);
        }

        $data = [];
        foreach ($rows as $row) {
            $data[$row->id] = $row->extendedName();
        }
        $selected = ($billing == 0) ? $order->shipping_address_id : $order->billing_address_id;
        if ($billing == 1 AND $order->billing_address_id == 0) {
            $selected = $order->shipping_address_id;
        }
        $name = ($billing == 0) ? "shipping_address_id" : "billing_address_id";
        $html = Form::select($name, $data, $selected);
        return $html;
    }

    static function address($order_id, $billing = 0)
    {
        $order = self::getOrder($order_id);
        $address = ($billing == 0) ? $order->getShippingAddress() : $order->getBillingAddress();
        $html = $address->toHtml();

        $url_edit = URL::action("CustomersController@getEdit", $order->customer_id) . "?update_address=" . $address->id;
        $url_map = $address->gmap();

        $url_gls = OrderAddressHandler::getUrlForXml($order_id);

        $table = <<<TABLE
<div>
<span class="pull-right">
<a target="_blank" href="$url_edit">Modifica</a> | <a target="_blank" href="$url_map">Apri su Google Maps</a> | <a target="_blank" href="$url_gls">Apri XML GLS</a>
</span>
$html
</div>
TABLE;

        return $table;
    }


    static function getAddressVerification($order_id)
    {
        $order = self::getOrder($order_id);
        $address = $order->getShippingAddress();
        $address_responses = $order->getAddressResponses($address->id);
        $address_trackings = $order->getAddressTrackings();
        return Theme::partial('order.address_verification', compact('address', 'order', 'address_responses', 'address_trackings'));
    }

    static function getGlsTrackingInfo($order_id)
    {
        $order = self::getOrder($order_id);
        $trackings = $order->getAddressTrackings();
        return Theme::partial('order.tracking_info', compact('order', 'trackings'));
    }


    static function getProductsTable($order_id)
    {
        $order = self::getOrder($order_id);
        $products = $order->getProducts();
        return Theme::partial('order.products', ['rows' => $products, 'order' => $order]);
    }

    static function getNegoziandoProductsTable($order_id)
    {
        $order = self::getOrder($order_id);
        $products = $order->getProducts();
        return Theme::partial('order.products_negoziando', ['rows' => $products, 'order' => $order]);
    }

    static function getRmaProductsTable($order_id)
    {
        $rma = self::getRma($order_id);
        $products = $rma->getProducts();
        return Theme::partial('rma.products', ['rows' => $products]);
    }

    static function getCartProductsTable($cart_id)
    {
        $cart = Cart::getObj($cart_id);
        $products = $cart->getProducts();
        return Theme::partial('cart.products', ['rows' => $products]);
    }

    static function getCartProductsNegoziandoTable($cart_id)
    {
        $cart = Cart::getObj($cart_id);
        $products = $cart->getProducts();
        return Theme::partial('cart.products_negoziando', ['rows' => $products]);
    }


    static function getProductsSummary($order_id)
    {
        $order = self::getOrder($order_id);
        return Theme::partial('order.summary', ['obj' => $order]);
    }

    static function getPromo($order_id)
    {
        $order = self::getOrder($order_id);
        return Theme::partial('order.promo', ['obj' => $order]);
    }

    static function getPayments($order_id)
    {
        $order = self::getOrder($order_id);
        $transactions = DB::table('transactions')->where('order_id', $order->id)->orderBy('created_at', 'desc')->get();
        foreach ($transactions as $tr) {
            $view = 'gateway::' . $tr->module . '.details';
            if (\View::exists($view)) {
                $tr->details = \View::make($view, ['obj' => $tr])->render();
            } else {
                $tr->details = 'None registered';
            }
        }
        return Theme::partial('order.payments', ['obj' => $order, 'rows' => $transactions]);
    }

    static function getShipment($order_id)
    {
        $order = self::getOrder($order_id);
        return Theme::partial('order.shipment', ['obj' => $order]);
    }

    static function getMorellatoDetails($order_id)
    {
        $order = self::getOrder($order_id);
        return Theme::partial('order.morellato', ['obj' => $order]);
    }


    static function getDocuments($order_id)
    {

        $order = self::getOrder($order_id);
        $rows = [];
        $invoice = $order->getInvoice();
        if ($invoice) {
            $invoice = (object)$invoice;
            $rows[] = $invoice;
        }
        $delivery = $order->getDelivery();
        if ($delivery) {
            $delivery = (object)$delivery;
            $rows[] = $delivery;
        }
        return Theme::partial('order.docs', ['obj' => $order, 'rows' => $rows, 'invoice' => $invoice, 'delivery' => $delivery]);
    }


    static function createDocument($order_id, $params)
    {
        $prefix = $params['type'];
        $data = [
            $prefix . "_number" => $params['number'],
            $prefix . "_date" => Format::sqlDate($params['date']),
            $prefix . "_notes" => $params['notes'],
        ];

        Order::where('id', $order_id)->update($data);

        self::createDocumentFile($prefix, $order_id);

        if ($prefix == 'invoice') {
            \Cfg::save('ORDER_INVOICE_NUMBER', $params['number']);
        } else {
            \Cfg::save('ORDER_DTD_NUMBER', $params['number']);
        }
    }

    static function createDocumentFile($type, $order_id)
    {
        /* @var $order Order */
        $order = Order::getObj($order_id);

        if ($type == 'invoice' and feats()->bladeReceipts()) {
            $order->createFeeDocumentFile();
            return;
        }

        $data = ($type == 'invoice') ? $order->getInvoiceData() : $order->getDeliveryData();
        //$filename = $data['filename'];
        audit($order->toArray(), __METHOD__);
        audit($data, __METHOD__);
        $pdf = PDF::loadView('pdf.' . $type, $data);
        if (File::exists($data['filepath'])) {
            File::delete($data['filepath']);
        }
        $output = $pdf->output();
        File::put($data['filepath'], $output);
    }

    static function getMessages($order_id)
    {
        $order = self::getOrder($order_id);
        $messages = $order->getMessages();
        return Theme::partial('order.messages', ['obj' => $order, 'rows' => $messages]);
    }

    static function getRmaMessages($rma_id)
    {
        $rma = self::getRma($rma_id);
        $messages = $rma->getMessages();
        return Theme::partial('rma.messages', ['obj' => $rma, 'rows' => $messages]);
    }

    static function getRmaTable($order_id)
    {
        $order = self::getOrder($order_id);
        $rma = $order->getRma();
        return Theme::partial('order.rmas', ['obj' => $order, 'rows' => $rma]);
    }


    static function getWarehouseByOrder(\Order $order)
    {
        $mode = $order->getMode();
        if ($mode == 'local') {
            return 'LC';
        }
        if ($mode == 'shop') {
            return 'NG';
        }
        if ($mode == 'online') {
            return $order->warehouse;
        }
        return null;
    }

    /**
     * @param Order $order
     * @param $receipt
     * @return bool
     */
    static function isReceiptValid(Order $order, $receipt)
    {
        $valid = false;
        $receipt = trim($receipt);
        if ($receipt != '') {
            $len = strlen($receipt);
            //offline payment
            if ($order->getPayment()->online == 0) {
                //check if the code is only numeric, and the "valid" lenghts are 11 (CRO), 13 (Scontrino) and 15 (CRO by Poste)
                /*if (ctype_digit($receipt) and ($len == 11 or $len == 13 or $len >= 15)) {
                    $valid = true;
                }*/
                if ($len >= 10) {
                    $valid = true;
                }
            } else {
                //online payment
                if ($len >= 10) {
                    $valid = true;
                }
            }

        } else {
            $valid = true;
        }

        return $valid;
    }


    static function findBestShopByOrderDetails(Order $order, &$solvable = null)
    {
        if(feats()->switchDisabled(Core\Cfg::SERVICE_BESTSHOP_MIGRATION)){
            return null;
        }
        $products = $order->getProducts(false);
        /** @var services\Morellato\Database\Sql\Connection $connection */
        $connection = Core::getNegoziandoConnection();

        $connection->setDebug(true);
        $connection->setExcludeOrder($order->id);

        //cycle through the array to get the selected shop
        $avoid_shops = [];
        foreach ($products as $product) {
            if ($product->isModeShop()) {
                if ($product->availability_shop_id != '') {
                    $avoid_shops[] = $product->availability_shop_id;
                }
            }
        }

        $deliveryStore = $order->getDeliveryStore();
        if ($deliveryStore) {
            $connection->addPreferredShop($deliveryStore->cd_neg);
        }
        $connection->setUnusedShops($avoid_shops);

        foreach ($products as $product) {
            if ($product->isModeShop()) {
                $row_solvable = $connection->addProduct($product->getSapSku(), $product->product_quantity, $product->product_id);
                if (is_array($solvable) and !empty($row_solvable)) {
                    $solvable[$product->getSapSku()] = $row_solvable;
                }
            }
        }

        //return $connection->getBestMapping();
        return $connection->getBestShop(true);
    }

    static function assignBestShopToOrderDetails(Order $order, $best_shop)
    {
        if ($best_shop === null or trim($best_shop) === '')
            return;

        if(feats()->switchDisabled(Core\Cfg::SERVICE_BESTSHOP_MIGRATION)){
            return;
        }

        $products = $order->getProducts(false);
        /** @var services\Morellato\Database\Sql\Connection $connection */
        $connection = Core::getNegoziandoConnection();

        $connection->setDebug(true);
        $connection->setExcludeOrder($order->id);

        foreach ($products as $product) {
            if ($product->isModeShop()) {
                $connection->incrementTakenQuantity($product->getSapSku(), $best_shop, $product->product_quantity, true);
            }
        }
    }

    /**
     * @param $code
     * @return bool
     */
    static function isValidGlsCode($code)
    {
        $code = trim($code);
        $check = strlen($code) >= 6;
        if (true === $check)
            return true;

        return false;
    }

    /**
     * @param $code
     * @return bool
     */
    static function isNegCode($code)
    {
        $code = trim($code);

        //check if 'NEG'
        if (stripos($code, 'NEG') === 0)
            return true;

        return false;
    }

}
