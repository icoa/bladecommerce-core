<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 03/12/2014
 * Time: 17:42
 */
class RestrictionHelper
{

    static function country($id, $type = 'price')
    {
        //\Utils::log("Verifing COUNTRIES for $id | $type");
        $table = $type . "_rules_country";
        $key = $type . "_rule_id";
        if($type == 'payment'){
            $table = $type . "s_country";
            $key = $type . "_id";
        }

        //$country = \Core::getCountry();
        $country = \CartManager::getDeliveryCountry();
        if($country){
            $country_id = $country->id;
            $count = \DB::table($table)->where($key, $id)->where('country_id', $country_id)->count($key);
        }else{
            $count = -1;
        }


        return ($count == 0) ? true : false;
    }

    static function group($id, $type = 'price')
    {
        //\Utils::log("Verifing GROUPS for $id | $type");
        $table = $type . '_rules_group';
        $key = $type . '_rule_id';
        if($type == 'payment' or $type == 'carrier'){
            $table = $type . 's_group';
            $key = $type . '_id';
        }
        $alt_group_id = null;
        if(\FrontUser::auth()) {
            $user = \FrontUser::get();
            if($user === null)
                return false;

            $group_id = $user->default_group_id;
            $alt_group_id = $user->alt_group_id;
            if(config('services.b2b')){
                //if B2B is enabled and the group is equal to the designated B2B Group
                if(!\Site::isB2B() and $group_id == config('negoziando.b2b_group')){
                    $group_id = 3;
                }
            }
        }else{
            $group_id = (\FrontUser::is_guest()) ? 2 : 1;
        }
        $groups = $alt_group_id === null ? [$group_id] : [$group_id, $alt_group_id];
        foreach($groups as $group){
            $count = \DB::table($table)->where($key, $id)->where('group_id', $group)->count($key);
            if($count === 0){
                return true;
            }
        }

        return false;
    }

    static function currency($id)
    {
        //\Utils::log("Verifing CURRENCY for $id");
        $currency_id = \FrontTpl::getCurrency();
        $table = "price_rules_currency";
        $key = "price_rule_id";
        $count = \DB::table($table)->where($key, $id)->where('currency_id', $currency_id)->count($key);
        return ($count == 0) ? true : false;
    }

    static function carrier($id, $type = 'cart')
    {
        //\Utils::log("Verifing CARRIER for $id");
        $carrier_id = \CartManager::getCarrierId();
        $table = $type . "_rules_carrier";
        $key = $type . "_rule_id";
        if($type == 'payment'){
            $table = $type . "s_carrier";
            $key = $type . "_id";
        }
        $count = \DB::table($table)->where($key, $id)->where('carrier_id', $carrier_id)->count($key);
        return ($count == 0) ? true : false;
    }

    static function country_id($id)
    {
        if($id <= 0){
            return true;
        }
        $country = \Core::getCountry();
        $country_id = $country->id;
        return $country_id == $id;
    }

    static function group_id($id)
    {
        if($id <= 0){
            return true;
        }
        $groups = [];
        if(\FrontUser::auth()) {
            $user = \FrontUser::get();
            if($user){
                $groups[] = (int)$user->default_group_id;
                if($user->alt_group_id > 0){
                    $groups[] = (int)$user->alt_group_id;
                }
            }
        }else{
            $groups[] = (\FrontUser::is_guest()) ? 2 : 1;
        }
        return in_array((int)$id, $groups, true);
    }

    static function currency_id($id)
    {
        if($id <= 0){
            return true;
        }
        $currency_id = \FrontTpl::getCurrency();
        return $currency_id == $id;
    }
}