<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 20-nov-2013 13.31.57
 */

use Carbon\Carbon;
use Underscore\Types\Arrays;

class AttributeHelper
{

    private $categories_ids;
    private $brand_id;
    private $collection_id;
    private $product_id;

    function __construct($product_id, $categories_ids, $brand_id, $collection_id)
    {
        $this->categories_ids = $categories_ids;
        $this->brand_id = $brand_id;
        $this->collection_id = $collection_id;
        $this->product_id = $product_id;
    }

    function resolveSetRules()
    {

        $categories_ids = $this->categories_ids;
        $brand_id = $this->brand_id;
        $collection_id = $this->collection_id;

        //\Utils::watch();
        $attribute_sets = array();

        //CATEGORIES
        $categories_ids = \Mainframe::categoriesFlatten(array(), $categories_ids);
        if (is_array($categories_ids)) {
            $categories_ids = implode(",", $categories_ids);
        }
        if ($categories_ids == '') $categories_ids = 0;

        $query = "SELECT * FROM attributes_sets_rules WHERE target_type='CAT' AND target_id IN (0,$categories_ids) ORDER BY position";
        $rows = DB::select($query);
        foreach ($rows as $row) {

            switch ($row->target_mode) {
                case '*':
                    $attribute_sets[] = $row->attribute_set_id;
                    break;

                case '+':
                    $attribute_sets[] = $row->attribute_set_id;
                    break;

                case '-':
                    if (count($attribute_sets) > 0) {
                        $attribute_sets = Arrays::without($attribute_sets, $row->attribute_set_id);
                    }
                    break;
            }
        }
        \Utils::log($attribute_sets, "ATTRIBUTES SETS");
        $attribute_sets = Arrays::unique($attribute_sets);

        //BRANDS
        if ($brand_id > 0):
            $query = "SELECT * FROM attributes_sets_rules WHERE target_type='BRA' AND target_id IN (0,$brand_id) ORDER BY position";
            $rows = DB::select($query);
            foreach ($rows as $row) {

                switch ($row->target_mode) {
                    case '*':
                        $attribute_sets[] = $row->attribute_set_id;
                        break;

                    case '+':
                        $attribute_sets[] = $row->attribute_set_id;
                        break;

                    case '-':
                        if (count($attribute_sets) > 0) {
                            $attribute_sets = Arrays::without($attribute_sets, $row->attribute_set_id);
                        }
                        break;
                }
            }
            $attribute_sets = Arrays::unique($attribute_sets);
        endif;

        //COLLECTIONS
        if ($collection_id > 0):
            $query = "SELECT * FROM attributes_sets_rules WHERE target_type='COL' AND target_id IN (0,$collection_id) ORDER BY position";
            $rows = DB::select($query);
            foreach ($rows as $row) {

                switch ($row->target_mode) {
                    case '*':
                        $attribute_sets[] = $row->attribute_set_id;
                        break;

                    case '+':
                        $attribute_sets[] = $row->attribute_set_id;
                        break;

                    case '-':
                        if (count($attribute_sets) > 0) {
                            $attribute_sets = Arrays::without($attribute_sets, $row->attribute_set_id);
                        }
                        break;
                }
            }
            $attribute_sets = Arrays::unique($attribute_sets);
        endif;


        //NOW EXAMINE THE COHERENCE OF THE RULES
        $categories_ids = explode(",", $categories_ids);
        $attribute_list = implode(",", $attribute_sets);
        $attribute_sets = array();
        $query = "SELECT DISTINCTROW target_type,target_mode,target_id,attribute_set_id FROM attributes_sets_rules WHERE attribute_set_id IN ($attribute_list) AND target_mode!='*' ORDER BY attribute_set_id,target_type";
        $rows = DB::select($query);

        $attribute_set_pivot = 0;
        $passable = array();

        foreach ($rows as $row) {

            if ($attribute_set_pivot != $row->attribute_set_id) {

                \Utils::log("PIVOT IS " . $attribute_set_pivot);
                if (Arrays::size($passable) > 0) {
                    \Utils::log($passable, "PASSABLE");
                    if (Arrays::sum($passable) == Arrays::size($passable)) { //CRC SUM
                        $attribute_sets[] = $attribute_set_pivot;
                    }
                }
                $attribute_set_pivot = $row->attribute_set_id;
                $passable = array();
            }


            switch ($row->target_type) {
                case 'CAT':
                    switch ($row->target_mode) {
                        case '+':
                            if (Arrays::contains($categories_ids, $row->target_id)) {
                                \Utils::log("Adding CAT " . $row->target_id);
                                $passable[] = 1;
                            } else {
                                $passable[] = 0;
                            }
                            break;

                        case '-':
                            if (!Arrays::contains($categories_ids, $row->target_id)) {
                                $passable[] = 1;
                                \Utils::log("Removing CAT " . $row->target_id);
                            } else {
                                $passable[] = 0;
                            }
                            break;
                    }
                    break;

                case 'BRA':
                    if ($brand_id > 0) {
                        switch ($row->target_mode) {
                            case '+':
                                if ($brand_id == $row->target_id) {
                                    $passable[] = 1;
                                    \Utils::log("Adding BRA " . $row->target_id);
                                } else {
                                    $passable[] = 0;
                                }
                                break;

                            case '-':
                                if ($brand_id != $row->target_id) {
                                    $passable[] = 1;
                                    \Utils::log("Removing BRA " . $row->target_id);
                                } else {
                                    $passable[] = 0;
                                }
                                break;
                        }
                    } else {
                        $passable[] = 0;
                    }
                    break;

                case 'COL':
                    if ($collection_id > 0) {
                        switch ($row->target_mode) {
                            case '+':
                                if ($collection_id == $row->target_id) {
                                    $passable[] = 1;
                                    \Utils::log("Adding COL " . $row->target_id);
                                } else {
                                    $passable[] = 0;
                                }
                                break;

                            case '-':
                                if ($collection_id != $row->target_id) {
                                    $passable[] = 1;
                                    \Utils::log("Removing COL " . $row->target_id);
                                } else {
                                    $passable[] = 0;
                                }
                                break;
                        }
                    } else {
                        $passable[] = 0;
                    }
                    break;
            }
        }
        if (Arrays::size($passable) > 0) {
            \Utils::log($passable, "PASSABLE FINISH");
            if (Arrays::sum($passable) == Arrays::size($passable)) { //CRC SUM
                $attribute_sets[] = $attribute_set_pivot;
            }
        }
        $attribute_sets = Arrays::unique($attribute_sets);

        \Utils::log($attribute_sets, "PRE-RETURNED ATTRIBUTES SETS");

        //now resolve rule ancestors
        if (Arrays::size($attribute_sets) > 1) {
            $attribute_sets_list = implode(",", $attribute_sets);
            $query = "SELECT * FROM attributes_sets WHERE id IN ($attribute_sets_list)";
            $rows = DB::select($query);
            foreach ($rows as $row) {
                if (Arrays::contains($attribute_sets, $row->parent_id)) {
                    $attribute_sets = Arrays::without($attribute_sets, $row->parent_id);
                    \Utils::log($attribute_sets, "NEW ATTRIBUTES SET");
                }
            }
        }

        \Utils::log($attribute_sets, "RETURNED ATTRIBUTES SETS");

        return $attribute_sets;
    }


    function getForSets()
    {
        $old = \Input::old("attribute_ids");
        if (isset($old)) {
            \Utils::log("Setting attributes by OLD");
            return $this->getFromOldInput();
        }

        if ($this->product_id > 0) {
            \Utils::log("Setting attributes by product $this->product_id");
            return $this->getTableForProduct();
        }
        \Utils::log("Setting attributes by default");
        return $this->getPrimaryRuleSet();

    }


    function getFromOldInput()
    {
        $attribute_ids = \Input::old("attribute_ids");
        $attribute_ids = trim($attribute_ids, ',');
        $attribute_ids = trim($attribute_ids);
        $lang = \Core::getLang();
        if ($attribute_ids == "") $attribute_ids = 0;
        $query = "SELECT DISTINCT(attribute_id),C.position,A.code,A.ldesc,A.frontend_class,A.frontend_input,A.is_required,A.is_unique FROM attributes_sets_content_cache C LEFT JOIN attributes A ON C.attribute_id=A.id WHERE A.id IN (SELECT DISTINCT(attribute_id) FROM attributes_lang WHERE published=1 AND lang_id='$lang' AND attribute_id IN($attribute_ids)) GROUP BY attribute_id ORDER BY FIELD(attribute_id, $attribute_ids)";
        $rows = DB::select($query);
        $renders = array();
        foreach ($rows as $row) {
            $obj = new AttributeHtml($row->attribute_id);
            $obj->setOldValue();
            $render = $obj->getRenders();
            //\Utils::log($render,"RENDER");
            $renders[] = $render;
        }

        return $renders;
    }

    private function sort_rule_set($a, $b)
    {
        if ($a->position > $b->position) {
            return true;
        }
        return false;
    }


    function getPrimaryRuleSet()
    {
        $categories_ids = $this->categories_ids;
        $brand_id = $this->brand_id;
        $collection_id = $this->collection_id;

        if ($categories_ids == '' AND $brand_id == '' AND $collection_id == '') {
            return false;
        }

        $attribute_sets = $this->resolveSetRules();

        $storage = [];

        foreach ($attribute_sets as $as) {
            $query = "SELECT DISTINCT(attribute_id),default_value,position FROM attributes_sets_content_cache WHERE attribute_set_id=$as ORDER BY position";
            $cache_rows = DB::select($query);
            foreach ($cache_rows as $cr) {
                $storage[$cr->attribute_id] = $cr;
            }
        }

        usort($storage, array($this, "sort_rule_set"));

        //\Utils::log($storage,"STORAGE");

        /*$attribute_sets_list = implode(",", $attribute_sets);
        $lang = \Core::getLang();



        $query = "SELECT DISTINCT(attribute_id),C.position,C.default_value,A.code,A.ldesc,A.frontend_class,A.frontend_input,A.is_required,A.is_unique FROM attributes_sets_content_cache C LEFT JOIN attributes A ON C.attribute_id=A.id WHERE attribute_set_id IN ($attribute_sets_list) AND A.id IN (SELECT DISTINCT(attribute_id) FROM attributes_lang WHERE published=1 AND lang_id='$lang') GROUP BY attribute_id ORDER BY attribute_set_id,C.position";
        \Utils::log($query);
        $rows = DB::select($query);
        $renders = array();
        foreach($rows as $row){
            $obj = new AttributeHtml($row->attribute_id);
            if($row->default_value != null AND $row->default_value != ''){
                $obj->setDefaultValue($row->default_value);
            }
            $render = $obj->getRenders();
            //\Utils::log($render,"RENDER");
            $renders[] = $render;
        }*/
        foreach ($storage as $row) {
            $obj = new AttributeHtml($row->attribute_id);
            if ($row->default_value != null AND $row->default_value != '') {
                $obj->setDefaultValue($row->default_value);
            }
            $render = $obj->getRenders();
            //\Utils::log($render,"RENDER");
            $renders[] = $render;
        }

        return $renders;
    }


    function getTableForProduct()
    {
        $id = $this->product_id;

        $lang = \Core::getLang();

        $query = "SELECT attribute_id FROM products_attributes_position WHERE product_id=$id ORDER BY position";
        Utils::log($query);
        $rows = DB::select($query);
        $renders = array();
        foreach ($rows as $row) {
            $obj = new AttributeHtml($row->attribute_id);
            $obj->setProductValue($id);
            $render = $obj->getRenders();
            //\Utils::log($render,"RENDER");
            $renders[] = $render;
        }

        return $renders;
    }


}