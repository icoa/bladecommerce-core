<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 30-gen-2014 15.30.43
 */

use Carbon\Carbon;
use Underscore\Types\Arrays;

abstract class TaxHelper {
    
    
    static public function getRuleForm($tax_rules_group_id, $id = 0) {        
        return Theme::scope("taxgroups.ruleform", ["tax_rules_group_id" => $tax_rules_group_id, "id" => $id])->content();
    }
}