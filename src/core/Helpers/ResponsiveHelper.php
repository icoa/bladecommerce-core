<?php
use HtmlObject\Element;
use HtmlObject\Image;

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 14/10/14
 * Time: 17.35
 */
class ResponsiveHelper
{
    static $row_counter = 0;
    static $col_counter = 0;
    static $col_total = 0;
    static $lazyLoad = true;

    static function block($span, $mode, $boxes = '')
    {
        $css = '';
        $values = [
            'span' => $span,
            'span-lg' => 4,
            'span-md' => 4,
            'span-sd' => 6,
            'span-xs' => 12,
        ];

        if (is_array($mode)) {
            $values = $mode;
            $css = $mode['css'];
        } else {
            $values['span-' . $mode] = $span;
        }


        $tpl = <<<TPL
<div class="span{$span} well" data-css="$css" data-span="{$values['span']}" data-span-lg="{$values['span-lg']}" data-span-md="{$values['span-md']}" data-span-sd="{$values['span-sd']}" data-span-xs="{$values['span-xs']}">
    <div class="navbar">
        <div class="navbar-inner"><h5><span>{$span}</span>/12</h5>
            <ul class="icons">
                <li><a data-action="minus-block" href="#" title="Diminuisci"><i class="font-step-backward"></i></a></li>
                <li><a data-action="plus-block" href="#" title="Aumenta"><i class="font-step-forward"></i></a></li>
                <li><a data-action="edit-block" href="#" title="Modifica parametri di questa colonna"><i class="font-cog"></i></a></li>
                <li><a data-action="add-box" href="#" title="Aggiungi banner"><i class="font-plus"></i></a></li>
                <li><a data-action="remove-block" href="#" title="Elimina"><i class="font-remove"></i></a></li>
            </ul>
        </div>
    </div>
    <div class="box-container">$boxes</div>
</div>
TPL;

        return $tpl;
    }

    static function row($row_index, $content)
    {
        $tpl = <<<TPL
<div class="row-fluid nested" data-id="$row_index">
    $content
    <div class="row-buttons">
        <ul class="icons">
            <li><a data-action="up-row" href="#" title="Sposta su"><i class="font-arrow-up"></i></a></li>
            <li><a data-action="down-row" href="#" title="Sposta giù"><i class="font-arrow-down"></i></a></li>
            <li><a data-action="add-block" href="#" title="Aggiungi blocco"><i class="font-plus-sign"></i></a></li>
            <li><a data-action="remove-row" href="#" title="Elimina"><i class="font-remove"></i></a></li>
        </ul>
    </div>
</div>
TPL;

        return $tpl;
    }

    static function box($id)
    {
        $tpl = '';
        try {
            $banner = Banner::getObj($id);
            $img = ($banner->image == "") ? "/media/no.gif" : $banner->image;

            $tpl = <<<TPL
<div class="box draggable" data-id="$id">
    <div class="portlet-header">
        <ul class="icons">
            <li><a data-action="edit-box" href="#" title="Impostazioni"><i class="font-cog"></i></a></li>
            <li><a data-action="remove-box" href="#" title="Elimina"><i class="font-remove"></i></a></li>
        </ul>
    </div>
    <img src="$img" />
</div>
TPL;
        } catch (\Exception $e) {

        }

        return $tpl;
    }


    static function render($data)
    {
        if ($data == null OR $data == '') return "";
        $obj = unserialize($data);
        //\Utils::log($obj, "ResponsiveGrid OBJ");
        $html = '';
        if (is_array($obj)) {
            foreach ($obj as $row) {
                $row_index = $row->id;
                $blocks = $row->blocks;
                $blocks_html = '';
                if (is_array($blocks)) {
                    foreach ($blocks as $block) {
                        $values = [
                            'span' => $block->lg,
                            'span-lg' => $block->lg,
                            'span-md' => $block->md,
                            'span-sd' => $block->sd,
                            'span-xs' => $block->xs,
                            'css' => $block->css
                        ];
                        $boxes = $block->boxes;
                        $boxes_html = '';
                        if (is_array($boxes)) {
                            foreach ($boxes as $box) {
                                $boxes_html .= self::box($box->id);
                            }
                        }
                        $blocks_html .= self::block($block->lg, $values, $boxes_html);
                    }
                    $html .= self::row($row_index, $blocks_html);
                }
            }
        }
        return $html;
    }


    static function front_render($data)
    {
        if ($data == null OR $data == '') return "";
        $obj = unserialize($data);
        //\Utils::log($obj, "ResponsiveGrid OBJ");

        $html = '';
        if (is_array($obj)) {
            foreach ($obj as $row) {
                self::$row_counter++;
                $row_index = $row->id;
                $blocks = $row->blocks;
                $blocks_html = '';
                if (is_array($blocks)) {
                    foreach ($blocks as $block) {
                        $boxes = $block->boxes;
                        $boxes_html = '';
                        if (is_array($boxes)) {
                            foreach ($boxes as $box) {
                                self::$col_counter++;
                                $boxes_html .= self::front_box($box->id);
                            }
                        }
                        $blocks_html .= self::front_block($block, $boxes_html);
                    }
                    self::$col_total = count($blocks);
                    $html .= self::front_row($row_index, $blocks_html);
                }
            }
        }

        return $html;
    }

    static function get_classes($block)
    {
        $classes = [];
        if ($block->lg != '') {
            $classes[] = ($block->lg == 'hidden') ? "hidden-lg" : "col-lg-" . $block->lg;
        }
        if ($block->md != '') {
            $classes[] = ($block->md == 'hidden') ? "hidden-md" : "col-md-" . $block->md;
        }
        if ($block->sd != '') {
            $classes[] = ($block->sd == 'hidden') ? "hidden-sm" : "col-sm-" . $block->sd;
        }
        if ($block->xs != '') {
            $classes[] = ($block->xs == 'hidden') ? "hidden-xs" : "col-xs-" . $block->xs;
        }
        if ($block->css != '') {
            $classes[] = $block->css;
        }
        $classes[] = "col-item-" . self::$col_counter;
        return implode(" ", $classes);
    }

    static function front_box($id)
    {
        $tpl = '';
        try {
            $banner = Banner::getObj($id);
            if ($banner->published == 0)
                return null;

            $params = unserialize($banner->params);
            $src = ($banner->image == "") ? "/media/no.gif" : $banner->image;

            if (!file_exists(public_path($banner->image))) {
                $src = "/media/no.gif";
            }

            $altText = $banner->input;
            $btnText = $banner->input_alt;
            $bannerText = $banner->input_textarea;
            $jsCode = $banner->input_textarea_alt;
            $box = Element::create("div", null, ['class' => 'box']);

            $meta = (object)\Site::imageMeta($src);
            $gif = false;

            if ($meta->width > 0 AND $meta->height > 0) {
                $inject = "/images/5a/$meta->width/$meta->height";
                if (\Str::contains($src, '.gif')) {
                    $gif = true;
                    $inject = null;
                }
                $src = $src != '' ? FrontTpl::img($src, false, $inject) : null;
            }

            $alt = strip_tags($bannerText);

            $img = Image::create($src, $alt, ['class' => 'banner', 'width' => $meta->width, 'height' => $meta->height]);

            if(self::$lazyLoad){
                $default = Image::create('data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==', $alt, [
                    'class' => 'img-responsive unveil',
                    'width' => $meta->width,
                    'height' => $meta->height,
                    'data-src' => $src,
                    'data-src-webp' => $src .  ($gif ? '' : '.webp'),
                ]);

                $default .= "<noscript>$img</noscript>";
                $img = $default;
            }

            if (isset($params->banner_css) AND $params->banner_css != '') {
                $box->addClass($params->banner_css);
            }

            if (isset($params->banner_id) AND $params->banner_id != '') {
                $box->setAttribute("id", $params->banner_id);
            }

            $link = new \LinkResolver($banner->content);
            $anchor = $link->getAnchor();
            $anchor->addClass("overlay");
            $overlay = "";


            if ($bannerText != '' OR $btnText != '') {
                $overlay = <<<HTML
<span class="overlayContainer">
    <span class="overlayAligner">
        <span class="headline">$bannerText</span>
        <span class="ctabutton"><span>$btnText</span></span>
    </span>
</span>
HTML;
            }
            $anchor->nest($overlay);
            $box->nest($img);
            $box->nest($anchor);

            return $box;
        } catch (\Exception $e) {

        }

        return $tpl;
    }


    static function front_block($block, $boxes = '')
    {

        $div = Element::create("div", $boxes, ['class' => self::get_classes($block)]);

        return $div;
    }

    static function front_row($row_index, $content)
    {
        $total = self::$col_total;
        $classes = ['row', "row-index-$row_index", "row-with-$total", "row-item-" . self::$row_counter];
        $css = implode(' ', $classes);
        $tpl = <<<TPL
<div class="$css">
    <div class="block-content">
        <div class="custom-block">
            $content
        </div>
        <div class="clear clr"></div>
    </div>
</div>
TPL;

        return $tpl;
    }
}