<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 02/10/14
 * Time: 17.41
 */
class LinkHelper
{

    private $defaults = [
        "rendering_type" => "link",
        "open_mode" => "_self",
        /*"url_it" => "IT",
        "url_en" => "EN",*/
        "url" => array
        (
            "it" => "",
            "en" => ""
        ),
        "default" => array
        (
            "target_type" => "base",
            "target_id" => 1
        ),
        "auto" => array
        (
            "target_type" => "base",
            "target_condition" => "in",
            "target_order" => "given",
            "target_id" => 1
        ),
        "modifiers" => [

        ],
        "filters" => [

        ]
    ];

    private $name;
    private $values;
    private $mode;


    function __construct($name, $values = null, $mode = 'full')
    {
        $this->mode = $mode;
        $this->name = $name;
        $this->values = ($values == null OR $values == '') ? $this->defaults : unserialize($values);
        //$this->values = (object)$this->values;
        \Utils::log($this->values,"LinkBuilder");
    }

    function setValues($values)
    {
        $this->values = $values;
    }

    function getDbValues()
    {
        return serialize($this->values);
    }

    function render()
    {
        $view = [];
        $view['name'] = $this->name;
        $view['obj'] = $this->values;
        $view['mode'] = $this->mode;
        return Theme::scope("utils.link", $view)->content();
    }


    static function getHtmlInputByType($type, $field, $name, $mode = 'single', $value = null)
    {
        $rawName = $name;
        $name = $field . "[$name]";
        $name = $name . "[target_id]";
        if ($rawName == 'modifiers' OR $rawName == 'filters') {
            $name .= "[]";
        }
        AdminForm::setValue($name, $value);
        switch ($type) {
            case 'product':
                $html = (string)AdminForm::hidden($name, ['class' => 'selectAjaxProducts span12'])->plain();
                break;
            case 'minprice':
            case 'maxprice':
                $html = (string)AdminForm::float($name, ['class' => 'span12'])->plain();
                break;
            default:
                if ($mode == 'single') {
                    $html = (string)\AdminForm::select($name, \Mainframe::selectNavsTarget($type), ['class' => 'span12 select'])->plain();
                } else {
                    //$html = (string)\AdminForm::selectMulti($name, \Mainframe::selectNavsTarget($type), ['class' => 'span12 select'])->plain();
                    $html = (string) \AdminForm::hidden($name,['class' => 'span12 selectMultipleReorder', 'rel' => $type ])->plain();
                }
                break;
        }
        return $html;
    }


    static function getModifier($field, $target_type = "category", $target_id = null)
    {
        $name = "{$field}[modifiers][target_type][]";
        AdminForm::setValue($name, $target_type);
        $navs = (string)AdminForm::select($name, \Mainframe::selectNavs(true), ['class' => 'span12 target_type', 'rel' => 'modifiers'])->plain();
        $target = self::getHtmlInputByType($target_type, $field, "modifiers", "single", $target_id);

        $tpl = <<<TPL
<div class="modifier link-container clearfix">
    <div class="target_type_cnt span4">
        <label>Tipo collegamento</label>
        $navs
    </div>
    <div class="span4">
        <label>Target collegamento</label>
        <div class="target_id_cnt">
        $target
        </div>
    </div>
    <div class="span4">
        <label>Azioni</label>
        <button type="button" class="btn btn-mini btn-danger remove-modifier action">RIMUOVI</button>
    </div>
</div>
TPL;

        return $tpl;

    }

    static function getFilter($field, $target_type = "minprice", $target_id = null)
    {
        $name = "{$field}[filters][target_type][]";
        AdminForm::setValue($name, $target_type);
        $navs = (string)AdminForm::select($name, \Mainframe::selectNavsFilter(), ['class' => 'span12 target_type', 'rel' => 'filters'])->plain();
        $target = self::getHtmlInputByType($target_type, $field, "filters", "single", $target_id);

        $tpl = <<<TPL
<div class="filterEl link-container clearfix">
    <div class="target_type_cnt span4">
        <label>Tipo filtro</label>
        $navs
    </div>
    <div class="span4">
        <label>Target filtro</label>
        <div class="target_id_cnt">
            $target
        </div>
    </div>
    <div class="span4">
        <label>Azioni</label>
        <button type="button" class="btn btn-mini btn-danger remove-filter action">RIMUOVI</button>
    </div>
</div>
TPL;

        return $tpl;

    }


    static function getTypename($type)
    {
        $name = 'N/D';
        switch ($type) {
            case 'base':
                $name = 'Base';
                break;
            case 'list':
                $name = 'Elem. di raccordo';
                break;
            case 'auto':
                $name = 'Lista autom.';
                break;
            case 'section':
                $name = 'Sezione';
                break;
            case 'page':
                $name = 'Pagina';
                break;
            case 'category':
                $name = 'Categoria';
                break;
            case 'brand':
                $name = 'Brand';
                break;
            case 'collection':
                $name = 'Collezione';
                break;
            case 'trend':
                $name = 'Gruppo pers.';
                break;
            case 'status':
                $name = 'Status prodotto';
                break;
            case 'qty':
                $name = 'Quantità prodottoo';
                break;
            case 'pricerange':
                $name = 'Fascia di prezzo';
                break;
            case 'url':
                $name = 'URL pers.';
                break;
            case 'html':
                $name = 'HTML';
                break;
            case 'separator':
                $name = 'Separatore';
                break;
            case 'container':
                $name = 'Contenitore';
                break;
            case 'product':
                $name = 'Prodotto singolo';
                break;
            case 'modulePosition':
                $name = 'Posizione moduli';
                break;
            default: //assuming attribute
                $lang = \Core::getLang();
                $attribute = \Attribute::where("code",$type)->first();
                if($attribute){
                    $name = $attribute->$lang->name;
                }
                break;
        }
        return $name;
    }


    static function getTypeByLinkParams($params)
    {
        $type = 'undefined';
        if (is_array($params)) {
            switch ($params['rendering_type']) { //'url','html','link','list'
                case 'url':
                case 'separator':
                case 'auto':
                case 'container':
                case 'html':
                case 'modulePosition':
                    $type = $params['rendering_type'];
                    break;
                case 'link':
                    $type = $params['default']['target_type'];
                    break;
                case 'list':
                    $type = $params['list']['target_type'];
                    break;
            }
        }
        return $type;
    }


    static function getHtmlTemplate($record_id, $itemname, $typename)
    {
        $html = <<<HTML
<li class="dd-item dd3-item" data-id="$record_id" id="node-$record_id">
    <div class="dd-handle dd3-handle">Drag</div><div class="dd3-content"><span class="itemname">$itemname</span><span class="dd-toolbar"><em>$typename</em><a class="dd-action" data-action="edit" href="javascript:;"><i class="icon-edit"></i></a><a class="dd-action" data-action="remove" href="javascript:;"><i class="icon-remove"></i></a></span></div>
</li>
HTML;
        return $html;
    }


    static function getBuilderAccordion( $mode = 'menu')
    {
        $html = '';
        $btn = ($mode == 'menu') ? "Aggiungi al menù" : "Aggiungi allo schema";
        $tpl = '
<div class="accordion-group">
    <div class="accordion-heading">
        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse{{ $type }}">{{ $title }}</a>
    </div>
    <div id="collapse{{ $type }}" class="accordion-body collapse {{ $mode }}">
        <div class="accordion-inner">
            {{ $select }}
            <div class="bottom-toolbar clearfix">
                <a href="javascript:;" class="pull-left select-all btn btn-mini" rel="{{ $type }}">seleziona tutto</a><a href="javascript:;" class="pull-left unselect-all ml10 btn btn-mini" rel="{{ $type }}">deseleziona tutto</a>
                <button type="button" class="btn btn-primary btn-mini pull-right add-menu" rel="{{ $type }}">'.$btn.'</button>
            </div>
        </div>
    </div>
</div>';

        $panels = [
            [
                "title" => "Base/Sito",
                "type" => "base"
            ],
            [
                "title" => "Elementi di raccordo",
                "type" => "list"
            ],
            [
                "title" => "Sezioni",
                "type" => "section"
            ],
            [
                "title" => "Pagine",
                "type" => "page"
            ],
            [
                "title" => "Categorie",
                "type" => "category"
            ],
            [
                "title" => "Brands",
                "type" => "brand"
            ],
            [
                "title" => "Collezioni",
                "type" => "collection"
            ],
            [
                "title" => "Gruppi personalizzati",
                "type" => "trend"
            ],
            [
                "title" => "Status prodotto",
                "type" => "status"
            ],
            [
                "title" => "Quantità prodotto",
                "type" => "qty"
            ],
            [
                "title" => "Fascie di prezzo",
                "type" => "pricerange"
            ],
        ];

        $lang = \Core::getLang();
        $attributes = \Attribute::rows($lang)->where("is_nav",1)->get();

        foreach($attributes as $a){
            $panels[] = ["title" => $a->name, "type" => $a->code];
        }

        //$panels[] = ["title" => 'Posizioni moduli', "type" => 'modulePosition'];

        if($mode == 'menu'){
            $panels[] = ["title" => 'Altro', "type" => 'other'];
        }

        foreach($panels as $panel){
            $panel['mode'] = ($panel['type'] == 'base') ? 'in' : '';
            $panel['select'] = (string)\AdminForm::selectMulti($panel['type'], \Mainframe::selectNavsTarget($panel['type']), ['class' => 'span12 multiple', 'size' => 20, 'data-type' => $panel['type'], 'data-name' => self::getTypename($panel['type'])])->plain();
            $html .= \Theme::blader($tpl,$panel);
        }

        if($mode != 'menu' ){
            $p = (string)\AdminForm::hidden("product_id", ['class' => 'selectAjaxProducts span12'])->plain();
            $data = ['mode' => '', 'title' => 'Prodotti', 'type' => 'product', 'select' => $p."<hr>"];
            $html .= \Theme::blader($tpl,$data);
        }

        return $html;
    }
}