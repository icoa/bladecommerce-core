<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-gen-2014 15.29.08
 */

use Carbon\Carbon;
use Core\ImageSetManager;
use services\Morellato\Database\Sql\Connection;
use Underscore\Types\Arrays;
use services\BusinessDay;
use Core\Admin\AdminForm;
use Core\Token;
use Frontend\Catalog;
use Core\Metadata;
use Illuminate\Support\Arr as ArraySupport;

abstract class ProductHelper
{

    static public function getCombinationsTable($product_id)
    {
        $table = <<<TABLE
<table class="table table-striped table-input" id="combinationsTable">
    <thead>
        <tr>
            <th width="40">ID</th>
            <th>Variante</th>
            <th width="120" class="center">Quantità</th>
            <th width="120" class="center">Override Prezzo</th>
            <th width="120" class="center">Impatto Prezzo</th>
            <th width="120" class="center">Impatto Peso</th>
            <th width="80" class="center">SKU</th>
            <th width="80" class="center">EAN13</th>
            <th width="80" class="center">UPC</th>
            <th width="120" class="center">Azioni</th>
        </tr>
    </thead>
    <tbody>
TABLE;

        $rows = ProductCombination::where("product_id", $product_id)->get();
        if ($rows->count() == 0) {
            $table .= "<tr><td colspan=99>Questo prodotto non ha varianti. Clicca sul pulsante <b>AGGIUNTI VARIANTE</b> per creare una variante di questo prodotto.</td></tr>";
        } else {
            foreach ($rows as $row) {
                $price = Format::currency($row->price, true);
                if ((float)$row->price == 0) {
                    $price = 'Nessuno';
                } else {
                    $price = ((float)$row->price > 0) ? 'Aumento di ' . $price : 'Riduzione di ' . $price;
                }

                $weight = Format::weight($row->weight);
                if ((float)$row->weight == 0) {
                    $weight = 'Nessuno';
                } else {
                    $weight = ((float)$row->weight > 0) ? 'Aumento di ' . $weight : 'Riduzione di ' . $weight;
                }

                $quantity = (int)$row->quantity;
                $css = ($quantity >= 1) ? 'label-success' : 'label-important';
                $quantity = '<span class="label ' . $css . '">' . $quantity . '</span>';

                $buy_price = ($row->buy_price > 0) ? Format::currency($row->buy_price, true) : 'Nessuno';


                $action_delete = URL::action("ProductsCombinationsController@postDestroy", $row->id);
                $action_default = URL::action("ProductsCombinationsController@postDefault", $row->id);
                $action_edit = URL::action("ProductsCombinationsController@postLoadForm", $row->id);
                $defaultCls = ($row->isDefault == 1) ? 'hide' : '';
                $defaultText = ($row->isDefault == 1) ? '<span class="label label-info ml15">Variante di default</span>' : "";

                $table .= <<<ROW
<tr>
<td><strong>$row->id</strong></td>
<td><strong>$row->name $defaultText</strong></td>
<td class="center">$quantity</td>
<td class="center">$buy_price</td>
<td class="center">$price</td>
<td class="center">$weight</td>
<td class="center">$row->sku</td>        
<td class="center">$row->ean13</td>        
<td class="center">$row->upc</td>        
<td class="center">
<div class="btn-group">
<button type="button" title="Predefinito" onclick="Echo.xhrAction(this,false,Combinations.reloadTable);" data-action="$action_default" class="btn hovertip $defaultCls" data-placement="top"><i class="icon-check"></i></button>
<button type="button" title="Modifica" onclick="Echo.xhrAction(this,false,Combinations.loadRecord);" data-action="$action_edit" class="btn hovertip" data-placement="top"><i class="icon-edit"></i></button>
<button type="button" title="Elimina" onclick="Echo.xhrAction(this,true,Combinations.reloadTable);" data-action="$action_delete" class="btn hovertip" data-placement="top"><i class="icon-remove"></i></button>
</div>
</td>        
</tr>
ROW;
            }
        }

        $table .= "</tbody></table>";

        return $table;
    }

    static public function getCombinationForm($product_id, $id = 0)
    {
        return Theme::scope("products.combination", ["product_id" => $product_id, "id" => $id])->content();
    }

    static public function getCombinationQuickForm($product_id, $id = 0)
    {
        return Theme::scope("products.combination_quick", ["product_id" => $product_id, "id" => $id])->content();
    }

    static public function combinations($id)
    {
        if ($id == 0)
            return array();
        $rows = DB::table("products_combinations_attributes")->where("combination_id", $id)->get();
        if (count($rows) == 0)
            return array();

        $default_lang = \Core::getLang();

        $data = array();

        foreach ($rows as $row) {
            $attribute = Attribute::find($row->attribute_id);
            $option = AttributeOption::find($row->option_id);
            $optionText = $attribute->{$default_lang}->name . ": " . (($option->{$default_lang}->name == "") ? $option->uname : $option->{$default_lang}->name);
            $optionValue = $row->attribute_id . "-" . $row->option_id;

            $data[$optionValue] = $optionText;
        }

        return $data;
    }

    static function getImagesCombinations($product_id, $combination_id)
    {
        $rows = \ProductImage::where("product_id", $product_id)->orderBy('position')->get();
        $selected_images = DB::table("products_combinations_images")->where("combination_id", $combination_id)->lists('image_id');

        $path = public_path() . "/assets/products/";
        $uri = Site::root();

        $html = '';

        if (count($rows) == 0) {
            $html .= "<p>Non sono presenti immagini per questo Prodotto. Utilizza l'uploader di Immagini per caricare il materiale.</p>";
        } else {
            foreach ($rows as $row) {

                $checked = (in_array($row->id, $selected_images)) ? 'checked' : '';

                $html .= <<<ROW
<div class="image-block">
    <input type="checkbox" $checked name="combination_images[]" autocomplete="off" value='$row->id' />
    <a class="fancybox" href="{$uri}/assets/products/{$row->filename}">
        <img src="{$uri}/images/small/assets/products/{$row->filename}" class="valign_middle tblImgPreview">
    </a>
</div>                    
ROW;
            }
        }

        return $html;
    }

    static function getImagesTableForProduct($product_id)
    {
        $rows = \ProductImage::where("product_id", $product_id)->orderBy('position')->get();

        $path = public_path() . "/assets/products/";
        $uri = Site::root();

        $table = <<<TABLE
<table id="imagesTable" class="table table-striped table-input">
    <thead>
        <tr>
            <th width="30" class="center"></th>
            <th width="100" class="center">Preview</th>
            <th class="center">Legenda / Dettagli</th>
            <th width="30" class="center">Cover</th>
            <th width="30" class="center">Visibile</th>
            <th width="30" class="center">Azioni</th>
        </tr>
    </thead>
    <tbody> 
TABLE;
        $ids = array();
        if (count($rows) == 0) {
            $table .= "<tr><td colspan=99><p>Non sono presenti immagini per questo Prodotto. Utilizza l'uploader di Immagini per caricare il materiale.</p></td></tr>";
        } else {

            $af = new AdminForm();

            if ($rows) {
                //\Utils::log($rows,"IMAGE ROWS");

                foreach ($rows as $row) {

                    //\Utils::log($row->filename,"IMAGE ROWS");
                    $ids[] = $row->id;

                    $translations = $row->translations;
                    $ds = array();
                    foreach ($translations as $tr) {
                        //\Utils::log($tr,"IMAGE TRANSLATIONS");
                        //$ds[$tr->lang_id."_".$row->id."_legend"] = $tr->legend;
                        $ds[$row->id . "_legend_" . $tr->lang_id] = $tr->legend;
                    }


                    $realfile = $path . $row->filename;
                    $af->populate($ds);
                    $input = $af->lang_textarea($row->id . "_legend", ['class' => "span12", 'rows' => 2]);

                    $dimensions = $type = $size = $size_str = 'N.D.';

                    if (File::exists($realfile) AND is_readable($realfile)) {
                        try {
                            $image = \Image::make($realfile);
                            $dimensions = $image->getWidth() . " X " . $image->getHeight();
                            $type = Str::upper($image->extension);
                            $size = File::size($realfile);
                            $size_str = Format::bytes($size);
                        } catch (Exception $e) {

                        }
                    }


                    $cover_action = ($row->cover == 1) ? '<a data-placement="top" class="btn hovertip btn-success" data-action="" href="javascript:;" tabindex="-1" data-original-title="Immagine di cover"><i class="icon-ok"></i></a>' : '<a data-placement="top" class="btn hovertip" data-action="' . URL::action("ProductsImagesController@postChangeFlag", [$row->id, "cover", 1]) . '" href="javascript:;" onclick="Echo.xhrAction(this,false,Products.saveImagesTable);" tabindex="-1" data-original-title="Imposta come cover"><i class="icon-remove"></i></a>';
                    $active_action = ($row->active == 1) ? '<a data-placement="top" class="btn hovertip btn-success" data-action="' . URL::action("ProductsImagesController@postChangeFlag", [$row->id, "active", 0]) . '" href="javascript:;" onclick="Echo.xhrAction(this,false,Products.saveImagesTable);" tabindex="-1" data-original-title="Disattiva immagine"><i class="icon-ok"></i></a>' : '<a data-placement="top" class="btn hovertip" data-action="' . URL::action("ProductsImagesController@postChangeFlag", [$row->id, "active", 1]) . '" href="javascript:;" onclick="Echo.xhrAction(this,false,Products.saveImagesTable);" tabindex="-1" data-original-title="Abilita immagine"><i class="icon-remove"></i></a>';
                    $remove_action = '<a data-placement="top" class="btn hovertip btn-danger" data-action="' . URL::action("ProductsImagesController@postRemove", $row->id) . '" href="javascript:;" onclick="Echo.xhrAction(this,true,Products.saveImagesTable);" tabindex="-1" data-original-title="Elimina"><i class="icon-minus"></i></a>';
                    $touch_action = '<a data-placement="top" class="btn hovertip btn-info" data-action="' . URL::action("ProductsImagesController@postTouch", $row->id) . '" href="javascript:;" onclick="Echo.xhrAction(this,true,Products.saveImagesTable);" tabindex="-1" data-original-title="Forza rigenerazione asset frontend"><i class="icon-edit"></i></a>';

                    $table .= <<<ROW
<tr id="image-row-{$row->id}"  rel="{$row->id}">
    <td class="dragHandle"></td>
    <td class="align-left">
        <a href="$uri/images/preview/assets/products/{$row->filename}" class="fancybox">
            <img class="valign_middle tblImgPreview" src="$uri/images/small/assets/products/{$row->filename}" />
        </a>
    </td>
    <td class="align-left">
        $input
        <div class="clear"></div>
        <strong>Dimensioni:</strong> $dimensions <strong class="ml20">Tipo:</strong> $type <strong class="ml20">Taglia:</strong> $size_str
    </td>
    <td class="center">
        <ul class="table-controls">
                <li>$cover_action</li>
            </ul>
    </td>
    <td class="center">
        <ul class="table-controls">
                <li>$active_action</li>
            </ul>
    </td>
    <td class="center">
        <ul class="table-controls">
                <li>$remove_action</li>
                <li>$touch_action</li>
            </ul>
    </td>
</tr>
ROW;
                }
            }
        }


        $table .= "</tbody></table>";
        $ids_val = (is_array($ids)) ? implode(",", $ids) : "";
        $table .= \Form::hidden("image_ids", $ids_val, ['id' => 'image_ids', 'autocomplete' => 'off']);

        //$table .= self::getRelatedImagesForProductsTable($product_id);

        return $table;
    }

    public static function getRelatedImagesForProducts($product_id)
    {
        $key = 'related_images_product_' . $product_id;
        $final_ids = [];
        if (Registry::has($key)) {
            $final_ids = Registry::get($key);
        }

        if (empty($final_ids)) {
            $obj = \Product::getObj($product_id);

            $add_queries = [];
            $remove_queries = [];

            $add_queries[] = DB::table('images_sets_rules')->where('target_mode', '*')->where('target_id', 0)->select('image_set_id');
            $remove_queries[] = DB::table('images_sets_rules')->where('target_mode', '-')->where('target_id', 0)->select('image_set_id');

            if ($obj->default_category_id > 0) {
                $add_queries[] = DB::table('images_sets_rules')->where('target_type', 'CAT')->where('target_mode', '+')->where('target_id', $obj->default_category_id)->select('image_set_id');
                $remove_queries[] = DB::table('images_sets_rules')->where('target_type', 'CAT')->where('target_mode', '-')->where('target_id', $obj->default_category_id)->select('image_set_id');
            }
            if ($obj->main_category_id > 0) {
                $add_queries[] = DB::table('images_sets_rules')->where('target_type', 'CAT')->where('target_mode', '+')->where('target_id', $obj->main_category_id)->select('image_set_id');
                $remove_queries[] = DB::table('images_sets_rules')->where('target_type', 'CAT')->where('target_mode', '-')->where('target_id', $obj->main_category_id)->select('image_set_id');
            }
            if ($obj->collection_id > 0) {
                $add_queries[] = DB::table('images_sets_rules')->where('target_type', 'COL')->where('target_mode', '+')->where('target_id', $obj->collection_id)->select('image_set_id');
                $remove_queries[] = DB::table('images_sets_rules')->where('target_type', 'COL')->where('target_mode', '-')->where('target_id', $obj->collection_id)->select('image_set_id');
            }
            if ($obj->brand_id > 0) {
                $add_queries[] = DB::table('images_sets_rules')->where('target_type', 'BRA')->where('target_mode', '+')->where('target_id', $obj->brand_id)->select('image_set_id');
                $remove_queries[] = DB::table('images_sets_rules')->where('target_type', 'BRA')->where('target_mode', '-')->where('target_id', $obj->brand_id)->select('image_set_id');
            }
            $attributes_options = $obj->getProductAttributesOptionsIds();
            if (count($attributes_options) > 0) {
                $add_queries[] = DB::table('images_sets_rules')->where('target_type', 'ATT')->where('target_mode', '+')->whereIn('target_id', $attributes_options)->select('image_set_id');
                $remove_queries[] = DB::table('images_sets_rules')->where('target_type', 'ATT')->where('target_mode', '-')->whereIn('target_id', $attributes_options)->select('image_set_id');
            }

            $remove_queries[] = DB::table('images_sets')->where('published', 0)->orWhereNotNull('deleted_at')->selectRaw('id as image_set_id');

            $builder = array_shift($add_queries);
            array_map(static function ($query) use ($builder) {
                $builder->union($query);
            }, $add_queries);
            $add_items = $builder->selectRaw('distinct(image_set_id)')->lists('image_set_id');
            //audit($add_items, '$add_items');

            $builder = array_shift($remove_queries);
            array_map(static function ($query) use ($builder) {
                $builder->union($query);
            }, $remove_queries);
            $remove_items = $builder->selectRaw('distinct(image_set_id)')->lists('image_set_id');
            //audit($remove_items, '$remove_items');
            $image_set_ids = array_diff($add_items, $remove_items);
            //audit($image_set_ids, '$image_set_ids');

            $all_rules = DB::table('images_sets_rules')
                ->whereIn('image_set_id', $image_set_ids)
                ->orderBy('image_set_id')
                ->orderBy('position')
                ->get();

            $all_rules_grouped = [];
            foreach ($all_rules as $rule) {
                if (!isset($all_rules_grouped[$rule->image_set_id])) {
                    $all_rules_grouped[$rule->image_set_id] = [];
                }
                $all_rules_grouped[$rule->image_set_id][] = $rule;
            }
            unset($all_rules);

            //audit($all_rules_grouped, '$all_rules_grouped');

            foreach ($all_rules_grouped as $image_set_id => $rules) {
                $pivot = [];
                foreach ($rules as $rule) {
                    switch ($rule->target_mode) {
                        case '+':
                            switch ($rule->target_type) {
                                case 'CAT':
                                    $pivot[] = ((int)$obj->default_category_id === (int)$rule->target_id OR (int)$obj->main_category_id === (int)$rule->target_id) ? 1 : 0;
                                    break;
                                case 'BRA':
                                    $pivot[] = ((int)$obj->brand_id === (int)$rule->target_id) ? 1 : 0;
                                    break;
                                case 'COL':
                                    $pivot[] = ((int)$obj->collection_id === (int)$rule->target_id) ? 1 : 0;
                                    break;
                                case 'ATT':
                                    $pivot[] = (in_array((int)$rule->target_id, $attributes_options, true)) ? 1 : 0;
                                    break;
                            }
                            break;

                        case '-':
                            switch ($rule->target_type) {
                                case 'CAT':
                                    $pivot[] = ((int)$obj->default_category_id !== (int)$rule->target_id AND (int)$obj->main_category_id !== (int)$rule->target_id) ? 1 : 0;
                                    break;
                                case 'BRA':
                                    $pivot[] = ((int)$obj->brand_id !== (int)$rule->target_id) ? 1 : 0;
                                    break;
                                case 'COL':
                                    $pivot[] = ((int)$obj->collection_id !== $rule->target_id) ? 1 : 0;
                                    break;
                                case 'ATT':
                                    $pivot[] = (!in_array((int)$rule->target_id, $attributes_options, true)) ? 1 : 0;
                                    break;
                            }
                            break;

                        default:
                            break;
                    }
                }

                if (array_sum($pivot) === count($rules)) {
                    $final_ids[] = $image_set_id;
                }
            }
        }

        //audit($final_ids, '$final_ids');

        if (count($final_ids) > 0) {
            $images = DB::table('images_groups')->leftJoin('images_sets', 'images_groups.set_id', '=', 'images_sets.id')->whereIn('images_sets.id', $final_ids)->where('active', 1)->orderBy('images_groups.position')->select('images_groups.id', 'images_groups.filename', 'images_groups.position', 'images_groups.set_id', 'images_sets.uname')->get();
            return $images;
        }
        return null;
    }

    static public function getRelatedImagesForProductsTable($product_id)
    {
        $ids = self::getRelatedImagesForProducts($product_id);
        $path = public_path() . "/assets/groups/";
        $uri = Site::root();
        $table = "";
        if ($ids) {
            $table = <<<TABLE
<div>
<div class="navbar">
    <div class="navbar-inner">
        <h5>Elementi ereditati da Set Immagini</h5>                                                            
    </div>
</div>
    <table class="table table-striped table-input">
    <thead>
        <tr>            
            <th width="100" class="center">Preview</th>
            <th class="center">Dettagli</th>
            <th width="100" class="center">Eredita da</th>
            <th width="200" class="center">Azioni</th>
        </tr>
    </thead>
    <tbody>         
TABLE;
            foreach ($ids as $row) {
                $realfile = $path . $row->filename;

                $dimensions = $type = $size = $size_str = 'N.D.';

                if (File::exists($realfile) AND is_readable($realfile)) {
                    try {
                        $image = \Image::make($realfile);
                        $dimensions = $image->getWidth() . " X " . $image->getHeight();
                        $type = Str::upper($image->extension);
                        $size = File::size($realfile);
                        $size_str = Format::bytes($size);
                    } catch (Exception $e) {

                    }
                }


                $link = \URL::action("ImagesSetController@getPreview", $row->set_id);
                $edit = \URL::action("ImagesSetController@getEdit", $row->set_id);

                $table .= <<<TABLE
<tr><td class="align-center">
    <a class="fancybox" href="$uri/assets/groups/{$row->filename}">
        <img  style="max-width:80px" src="$uri/images/small/assets/groups/{$row->filename}" class="valign_middle tblImgPreview">
    </a>
</td>                  
<td class="align-left">
    <strong>Dimensioni:</strong> $dimensions <strong class="ml20">Tipo:</strong> $type <strong class="ml20">Taglia:</strong> $size_str
</td>
<td class="align-center"><span class="label">{$row->uname}</span></td>
<td class="align-center"><a href="javascript:Echo.remoteModal('$link');" class="remote-modal btn btn-small mr5">Anteprima</a><a target="_blank" href="$edit" class="btn btn-small">Modifica</a></td></tr>
TABLE;
            }
            $table .= "</tbody></table></div>";
        }
        return $table;
    }

    /**
     * @param int $quantity
     * @param array $repeatable_rows
     * @return float|null
     */
    static public function getReductionValueByRepeatableRows($quantity = 1, $repeatable_rows = [])
    {
        $reduction_value = null;

        //audit($repeatable_rows);
        if (!empty($repeatable_rows)) {
            $satisfiable_row = ArraySupport::where($repeatable_rows, function ($key, $value) use ($quantity) {
                return $quantity >= $value['from_quantity'] and $quantity < $value['to_quantity'];
            });
            //audit($satisfiable_row, __METHOD__, '$satisfiable_row for quantity = ' . $quantity);
            if (!empty($satisfiable_row)) {
                $new_reduction_value = (float)last($satisfiable_row)['amount'];
                //audit("Overriding reduction_value from [$reduction_value] to [$new_reduction_value]");
                $reduction_value = $new_reduction_value;
            }
        }

        return $reduction_value;
    }


    public static function calculateProductPrice(Product $product, ProductPrice $rule)
    {
        $newPrice = 0;
        $newPriceWt = 0;
        $tax = \TaxGroup::getPublicObj($product->tax_group_id);
        $ratio = ($tax) ? $tax->getDefaultTaxRatio() : 1;
        $priceRule = $rule->getPriceRule();

        $sell_price = (isset($product->original_sell_price) and $product->original_sell_price > 0) ? $product->original_sell_price : $product->sell_price;
        $price = (isset($product->original_price) and $product->original_price > 0) ? $product->original_price : $product->price;
        $lis_price = (isset($product->original_sell_price_wt) and $product->original_sell_price_wt > 0) ? $product->original_sell_price_wt : $product->sell_price_wt;
        $buy_price = (isset($product->original_buy_price) and $product->original_buy_price > 0) ? $product->original_buy_price : $product->buy_price;

        $reduction_target = $rule->reduction_target;
        $reduction_value = $rule->reduction_value;
        $reduction_currency = $rule->reduction_currency;
        $reduction_type = $rule->reduction_type;

        if ($priceRule and (int)$priceRule->repeatable === 1 and is_array($priceRule->getParam('rules'))) {
            $quantity = $product->getOverallQuantity();
            $repeatable_rows = $priceRule->getParam('rules');
            $repeatable_reduction_value = self::getReductionValueByRepeatableRows($quantity, $repeatable_rows);
            if ($repeatable_reduction_value !== null)
                $reduction_value = $repeatable_reduction_value;
        }

        if ($reduction_target == 1) { //prezzo finale tasse incluse
            switch ($reduction_type) {
                case 'amount':
                    $newPriceWt = $price - $reduction_value;
                    $newPrice = $newPriceWt / $ratio;
                    break;

                case 'percent':
                    $newPriceWt = $price - ($price * $reduction_value / 100);
                    $newPrice = $newPriceWt / $ratio;
                    break;

                case 'fixed_amount':
                    $newPriceWt = $reduction_value;
                    $newPrice = $newPriceWt / $ratio;
                    break;

                case 'fixed_percent':
                    $newPriceWt = ($price * $reduction_value / 100);
                    $newPrice = $newPriceWt / $ratio;
                    break;
            }
        }

        if ($reduction_target == 2) { //prezzo listine (tax included)
            switch ($reduction_type) {
                case 'amount':
                    $newPriceWt = $lis_price - $reduction_value;
                    $newPrice = $newPriceWt / $ratio;
                    break;

                case 'percent':
                    $newPriceWt = $lis_price - ($lis_price * $reduction_value / 100);
                    $newPrice = $newPriceWt / $ratio;
                    break;

                case 'fixed_amount':
                    $newPriceWt = $reduction_value;
                    $newPrice = $newPriceWt / $ratio;
                    break;

                case 'fixed_percent':
                    $newPriceWt = ($lis_price * $reduction_value / 100);
                    $newPrice = $newPriceWt / $ratio;
                    break;
            }
        }

        if ($reduction_target == 3) { //prezzo di acquisto (tax excluded)
            switch ($reduction_type) {
                case 'amount':
                    $newPrice = $buy_price - $reduction_value;
                    $newPriceWt = $newPriceWt * $ratio;
                    break;

                case 'percent':
                    $newPrice = $buy_price - ($buy_price * $reduction_value / 100);
                    $newPriceWt = $newPriceWt * $ratio;
                    break;

                case 'fixed_amount':
                    $newPrice = $reduction_value;
                    $newPriceWt = $newPriceWt * $ratio;
                    break;

                case 'fixed_percent':
                    $newPrice = ($buy_price * $reduction_value / 100);
                    $newPriceWt = $newPriceWt * $ratio;
                    break;
            }
        }

        if ($reduction_target == 4) { //prezzo finale tasse escluse
            switch ($reduction_type) {
                case 'amount':
                    $newPrice = $sell_price - $reduction_value;
                    $newPriceWt = $newPrice * $ratio;
                    break;

                case 'percent':
                    $newPrice = $sell_price - ($sell_price * $reduction_value / 100);
                    $newPriceWt = $newPrice * $ratio;
                    break;

                case 'fixed_amount':
                    $newPrice = $reduction_value;
                    $newPriceWt = $newPrice * $ratio;
                    break;

                case 'fixed_percent':
                    $newPrice = ($sell_price * $reduction_value / 100);
                    $newPriceWt = $newPrice * $ratio;
                    break;
            }
        }

        if ($reduction_currency > 0) {
            $newPrice = \Format::convert($newPrice, 1, $reduction_currency);
            $newPriceWt = \Format::convert($newPriceWt, 1, $reduction_currency);
        }

        //$debug = compact('tax', 'ratio', 'sell_price', 'price', 'lis_price', 'buy_price', 'reduction_currency', 'reduction_target', 'reduction_type', 'reduction_value', 'quantity', 'newPrice', 'newPriceWt');
        //audit($debug, __METHOD__, '$debug');

        unset($tax, $ratio, $sell_price, $price, $lis_price, $buy_price, $reduction_currency, $reduction_target, $reduction_type, $quantity);

        return ['price' => $newPrice, 'price_wt' => $newPriceWt, 'reduction_value' => $reduction_value];
    }

    static public function getSpecificPricesTable($product_id)
    {

        $default_lang = \Core::getLang();

        $action = URL::action("ProductsPricesController@postOrder");

        $table = <<<TABLE
<table class="table table-striped table-input" id="specificPricesTable" rel="$action">
    <thead>
        <tr>
            <th width="50" class="center"></th>
            <th>Descrizione</th>
            <th>Variante</th>
            <th>Valuta</th>
            <th>Nazione</th>
            <th>Gruppo</th>
            <th>Cliente</th>
            <th class="center">Regola</th>
            <th class="center">Prezzo (t.e.)</th>
            <th class="center">Prezzo (t.i.)</th>
            <th class="center">Stop?</th>
            <th class="center">Attiva?</th>
            <th>Periodo</th>
            <th width="80" class="center">Q.tà minima</th>
            <th width="50" class="center">Azioni</th>
        </tr>
    </thead>
    <tbody>
TABLE;

        $ids = array();
        $rows = ProductPrice::where("product_id", $product_id)->orderBy("position")->orderBy("price_rule_id")->get();
        if ($rows->count() == 0) {
            $table .= "<tr><td colspan=99>Questo prodotto non ha prezzi specifici e non eredita regole prezzi catalogo. Clicca sul pulsante <b>AGGIUNGI PREZZO SPECIFICO</b> per creare una nuova regola per il prezzo di questo prodotto.</td></tr>";
        } else {
            foreach ($rows as $row) {
                $ids[] = $row->id;
                $price = Format::currency($row->price, true);
                $action_delete = URL::action("ProductsPricesController@postDestroy", $row->id);
                $action_edit = URL::action("ProductsPricesController@postLoadForm", $row->id);

                $combinationTxt = "Tutte le varianti";
                $currencyTxt = "Tutte le valute";
                $countryTxt = "Tutte le nazioni";
                $customerGroupTxt = "Tutti i Gruppi";
                $customerTxt = "Tutti i Clienti";
                $periodTxt = "Illimitato";

                if ($row->combination_id > 0) {
                    $combination = ProductCombination::find($row->combination_id);
                    $combinationTxt = $combination->name;
                }
                if ($row->currency_id > 0) {
                    $currency = Currency::find($row->currency_id);
                    $currencyTxt = $currency->iso_code . " ($currency->sign)";
                }
                if ($row->country_id > 0) {
                    $country = Country::find($row->country_id);
                    $countryTxt = $country->{$default_lang}->name;
                }
                if ($row->customer_group_id > 0) {
                    $group = CustomerGroup::find($row->customer_group_id);
                    if (!is_null($group)) {
                        $customerGroupTxt = ($group) ? $group->{$default_lang}->name : '';
                    }
                }

                $rule_explained = (int)$row->repeatable === 1 ? '<b>Condizioni in base a soglie/valori multipli</b>' : \PriceRulesController::getRuleExplainedData($row->reduction_type, $row->reduction_value, $row->reduction_currency, $row->reduction_tax, $row->reduction_target, $row->free_shipping);

                //$price_final = Format::currency($price_final, true);

                if ($row->date_from != null OR $row->date_to != null) {
                    $periodTxt = "";
                }

                if ($row->date_from != null) {
                    $periodTxt .= "Da: " . Format::datetime($row->date_from, true);
                }

                if ($row->date_to != null) {
                    $periodTxt .= "a: " . Format::datetime($row->date_to, true);
                }

                $description = 'N/D';
                $button_class = '';
                if ($row->price_rule_id > 0) {
                    $button_class = 'hide';
                    $PriceRule = \PriceRule::withTrashed()->find($row->price_rule_id);
                    $description = "<a target='_blank' href='/admin/pricerules/edit/$row->price_rule_id'>Ereditata da <b>" . $PriceRule->{$default_lang}->name . "</b></a>";
                }

                $stop = $row->stop_other_rules == 1 ? 'Sì' : 'No';
                $product = \Product::find($product_id);
                //$prices = self::calculateProductPrice($product->sell_price, $product->price, $product->tax_group_id, $row->reduction_type, $row->reduction_value, $row->reduction_currency, $row->reduction_tax, $row->reduction_target);
                $prices = self::calculateProductPrice($product, $row);
                $price = Format::currency($prices['price'], true, $row->reduction_currency);
                $price_wt = Format::currency($prices['price_wt'], true, $row->reduction_currency);

                $active_txt = $row->active == 1 ? '<span class="label label-success">SI</span>' : '<span class="label label-important">NO</span>';


                $table .= <<<ROW
<tr id="$row->id" rel="$row->id">
<td class="dragHandle"></td>
<td>$description</td>
<td>$combinationTxt</td>
<td>$currencyTxt</td>
<td>$countryTxt</td>        
<td>$customerGroupTxt</td>        
<td>$customerTxt</td>        
<td class="center">$rule_explained</td>
<td class="center"><b>$price</b></td>
<td class="center"><b>$price_wt</b></td>
<td class="center">$stop</td>
<td class="center">$active_txt</td>
<td>$periodTxt</td>
<td class="center">$row->from_quantity</td>              
<td class="center">
<div class="btn-group $button_class">
<button type="button" title="Modifica" onclick="Echo.xhrAction(this,false,Prices.loadRecord);" data-action="$action_edit" class="btn hovertip" data-placement="top"><i class="icon-edit"></i></button>
<button type="button" title="Elimina" onclick="Echo.xhrAction(this,true,Prices.reloadTable);" data-action="$action_delete" class="btn hovertip" data-placement="top"><i class="icon-remove"></i></button>
</div>
</td>        
</tr>
ROW;
            }
        }

        $table .= "</tbody></table>";

        $table .= Form::hidden("specific_prices_ids", implode(",", $ids), ["autocomplete" => "off"]);

        return $table;
    }

    static public function getSpecificPriceForm($product_id, $id = 0)
    {
        return Theme::scope("products.price", ["product_id" => $product_id, "id" => $id])->content();
    }

    static public function getGroupActionsPanel()
    {
        return Theme::scope("products.groups")->content();
    }

    static public function performGroupActions()
    {
        $data = \Input::all();
        //Utils::log($data,"performGroupActions");
        parse_str($data['data'], $group_options);
        //Utils::log($group_options, "performGroupActions");
        $ids = $data['ids'];
        //Utils::log($ids, 'IDS');
        $messages = array();
        if (isset($group_options['chk_categories']) AND $group_options['chk_categories'] == 'on') {
            $categories = $group_options['categories'];
            if (count($categories) > 0) {
                $default_category_id = $categories[0];
                foreach ($ids as $id) {
                    DB::table("categories_products")->where("product_id", $id)->delete();
                    foreach ($categories as $cat_id) {
                        DB::table("categories_products")->insert(['product_id' => $id, 'category_id' => $cat_id]);
                    }
                    DB::table("products")->where("id", $id)->update(['default_category_id' => $default_category_id, 'cnt_categories' => count($categories)]);
                }
                $messages[] = 'Assegnazione categorie avvenuta con successo';
            } else {
                $messages[] = 'Assegnazione categorie non avvenuta: set di categorie fornite vuoto';
            }
        }

        if (isset($group_options['chk_brand']) AND $group_options['chk_brand'] == 'on') {
            $brand_id = $group_options['brand_id'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['brand_id' => $brand_id]);
            }
            $messages[] = 'Assegnazione brand avvenuta con successo';
        }

        if (isset($group_options['chk_collection']) AND $group_options['chk_collection'] == 'on') {
            $collection_id = $group_options['collection_id'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['collection_id' => $collection_id]);
            }
            $messages[] = 'Assegnazione collezione avvenuta con successo';
        }

        if (isset($group_options['chk_flag_published']) AND $group_options['chk_flag_published'] == 'on') {
            $flag = (int)$group_options['published'];
            foreach ($ids as $id) {
                DB::table("products_lang")->where("product_id", $id)->update(['published' => $flag]);
            }
            $messages[] = 'Modifica status del flag ATTIVO avvenuta con successo';
        }

        if (isset($group_options['chk_flag_fp']) AND $group_options['chk_flag_fp'] == 'on') {
            $flag = (int)$group_options['is_out_of_production'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['is_out_of_production' => $flag]);
            }
            $messages[] = 'Modifica status del flag FUORI PRODUZIONE avvenuta con successo';
        }

        if (isset($group_options['chk_flag_new']) AND $group_options['chk_flag_new'] == 'on') {
            $flag = (int)$group_options['is_new'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['is_new' => $flag]);
            }
            $messages[] = 'Modifica status del flag FUORI PRODUZIONE avvenuta con successo';
        }

        if (isset($group_options['chk_flag_soldout']) AND $group_options['chk_flag_soldout'] == 'on') {
            $flag = (int)$group_options['is_soldout'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['is_soldout' => $flag]);
            }
            $messages[] = 'Modifica status del flag SOLD-OUT avvenuta con successo';
        }

        if (isset($group_options['chk_flag_outlet']) AND $group_options['chk_flag_outlet'] == 'on') {
            $flag = (int)$group_options['is_outlet'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['is_outlet' => $flag]);
            }
            $messages[] = 'Modifica status del flag OUTLET avvenuta con successo';
        }

        if (isset($group_options['chk_flag_featured']) AND $group_options['chk_flag_featured'] == 'on') {
            $flag = (int)$group_options['is_featured'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['is_featured' => $flag]);
            }
            $messages[] = 'Modifica status del flag VETRINA avvenuta con successo';
        }

        if (isset($group_options['chk_text_qty']) AND $group_options['chk_text_qty'] == 'on') {
            $v = (int)$group_options['qty'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['qty' => $v]);
            }
            $messages[] = 'Modifica valore del campo QUANTITA avvenuta con successo';
        }

        if (isset($group_options['chk_text_buy_price']) AND $group_options['chk_text_buy_price'] == 'on') {
            $v = (float)$group_options['buy_price'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['buy_price' => $v]);
            }
            $messages[] = 'Modifica valore del campo PREZZO BASE avvenuta con successo';
        }

        if (isset($group_options['chk_text_sell_price_wt']) AND $group_options['chk_text_sell_price_wt'] == 'on') {
            $v = (float)$group_options['sell_price_wt'];
            foreach ($ids as $id) {
                DB::table("products")->where("id", $id)->update(['sell_price_wt' => $v]);
            }
            $messages[] = 'Modifica valore del campo PREZZO DI LISTINO avvenuta con successo';
        }

        if (isset($group_options['chk_text_sell_price']) AND $group_options['chk_text_sell_price'] == 'on') {
            $v = (float)$group_options['sell_price'];
            foreach ($ids as $id) {
                $tax_rate = self::getTaxRateByProductId($id);
                $price = round($v + ($v / 100 * $tax_rate), 2);
                DB::table("products")->where("id", $id)->update(['sell_price' => $v, 'price' => $price]);
            }
            $messages[] = 'Modifica valore del campo PREZZO DI VENDITA (tasse escl.) avvenuta con successo';
        }

        if (isset($group_options['chk_text_price']) AND $group_options['chk_text_price'] == 'on') {
            $v = (float)$group_options['price'];
            foreach ($ids as $id) {
                $tax_rate = self::getTaxRateByProductId($id);
                $r = 1 + ($tax_rate / 100);
                $sell_price = round($v / $r, 2);
                DB::table("products")->where("id", $id)->update(['sell_price' => $sell_price, 'price' => $v]);
            }
            $messages[] = 'Modifica valore del campo PREZZO DI VENDITA (tasse incl.) avvenuta con successo';
        }

        if (isset($group_options['chk_pattern_name']) AND $group_options['chk_pattern_name'] == 'on') {
            self::groupOperationSetPattern($group_options, "name", $ids, "NOME PRODOTTO", $messages);
        }
        if (isset($group_options['chk_pattern_title']) AND $group_options['chk_pattern_title'] == 'on') {
            self::groupOperationSetPattern($group_options, "metatitle", $ids, "SEO - META TITLE", $messages);
        }
        if (isset($group_options['chk_pattern_h1']) AND $group_options['chk_pattern_h1'] == 'on') {
            self::groupOperationSetPattern($group_options, "h1", $ids, "HEADER H1", $messages);
        }
        if (isset($group_options['chk_pattern_keywords']) AND $group_options['chk_pattern_keywords'] == 'on') {
            self::groupOperationSetPattern($group_options, "metakeywords", $ids, "META KEYWORDS", $messages);
        }
        if (isset($group_options['chk_pattern_metadesc']) AND $group_options['chk_pattern_metadesc'] == 'on') {
            self::groupOperationSetPattern($group_options, "metadescription", $ids, "META DESCRIPTION", $messages);
        }

        if (isset($group_options['chk_recalc']) AND $group_options['chk_recalc'] == 'on') {
            foreach ($ids as $id) {
                $query = "SELECT P.id,COUNT(I.id) as cnt FROM products P LEFT JOIN images I ON P.id=I.product_id WHERE P.id=? GROUP BY P.id";
                $objImages = DB::selectOne($query, [$id]);
                $query = "SELECT P.id,COUNT(I.category_id) as cnt FROM products P LEFT JOIN categories_products I ON P.id=I.product_id WHERE P.id=? GROUP BY P.id";
                $objCategories = DB::selectOne($query, [$id]);
                DB::table("products")->where("id", $id)->update(['cnt_images' => $objImages->cnt, 'cnt_categories' => $objCategories->cnt]);
            }
            $messages[] = 'Aggiornamento counter avvenuto con successo';
        }


        if (isset($group_options['chk_trend']) AND $group_options['chk_trend'] == 'on') {
            $trend_id = $group_options['trend_id'];
            foreach ($ids as $id) {
                DB::table('trends_products')->where('product_id', $id)->where('trend_id', $trend_id)->delete();
                DB::table('trends_products')->insert(['product_id' => $id, 'trend_id' => $trend_id]);
            }
            $messages[] = 'Assegnazione gruppo personalizzato avvenuto con successo';
        }

        $rebuildMeta = false;
        if (isset($group_options['chk_attribute']) AND $group_options['chk_attribute'] == 'on') {

            $attribute_id = $group_options['attribute_id'];
            $attribute_option_id = isset($group_options['attribute_option_id']) ? $group_options['attribute_option_id'] : [];
            if (empty($attribute_option_id)) {
                $messages[] = 'Devi selezionare almeno un valore per opzioni attributo';
            } else {
                foreach ($ids as $id) {
                    $product_id = $id;

                    //check if the product has a record in products_attributes_position
                    $productAttributePosition = DB::table('products_attributes_position')->where('product_id', $id)->where('attribute_id', $attribute_id)->first();
                    if (!$productAttributePosition) {
                        $position = DB::table('products_attributes_position')->where('product_id', $id)->max('position') + 1;
                        DB::table('products_attributes_position')->insert(compact('product_id', 'attribute_id', 'position'));
                    }
                    //delete all previous options
                    DB::table('products_attributes')->where('product_id', $product_id)->where('attribute_id', $attribute_id)->delete();
                    foreach ($attribute_option_id as $attribute_val) {
                        DB::table('products_attributes')->insert(compact('product_id', 'attribute_id', 'attribute_val'));
                    }
                }
                $messages[] = 'Assegnazione attributo/valore avvenuto con successo';
                $rebuildMeta = true;
            }
        }


        if (count($messages) > 0) {
            foreach ($ids as $id) {
                try {
                    if ($rebuildMeta) {
                        self::buildProductCache($id, true);
                    } else {
                        \Product::find($id)->uncache();
                    }
                } catch (\Exception $e) {
                    \Utils::log($e->getMessage(), __METHOD__);
                }
            }
        }

        return $messages;
    }

    static function getTaxRateByProductId($product_id)
    {
        $group_id = DB::table("products")->where("id", $product_id)->pluck('tax_group_id');
        $obj = \TaxGroup::find($group_id);
        return $obj->getDefaultTaxRate();
    }

    static function getPreviewTableLang($obj)
    {

        $data_lang = [
            'Nome' => 'name',
            'Attivo' => 'published',
            'Descrizione breve' => 'sdesc',
            'Descrizione estesa' => 'ldesc',
            'Permalink' => 'slug',
            'Titolo pagina' => 'metatitle',
            'Tag H1' => 'h1',
            'Meta keywords' => 'metakeywords',
            'Meta description' => 'metadescription',
        ];

        $tbody = '<tbody>';

        $languages = \Mainframe::languagesCodes();

        foreach ($data_lang as $key => $value) {

            $tbody .= "<tr><td style='width:20%' class='align-right bold'>$key</td>";

            foreach ($languages as $lang) {
                $v = $obj->{$value . "_" . $lang};
                $tbody .= "<td class='center'>$v</td>";
            }

            $tbody .= "</tr>";
        }
        $tbody .= '</tbody>';

        $tdead = "<thead><tr><th style='width:20%' class='center'>Campo</th>";

        foreach ($languages as $lang) {
            $tdead .= "<th class='center'>Valore <img style='vertical-align:middle;' src='/themes/admin/assets/images/flags/$lang.jpg' /></th>";
        }

        $tdead .= "</tr></thead>";

        $table = $tdead . $tbody;

        return $table;
    }

    static function getPreviewTable($obj)
    {
        $data = [
            'SKU' => 'sku',
            'EAN13' => 'ean13',
            'UPC' => 'upc',
            'Novità' => 'is_new',
            'In Vetrina' => 'is_featured',
            'Fuori produzione' => 'is_out_of_production',
            'Outlet' => 'is_outlet',
        ];

        $tbody = '<tbody>';

        $query = "SELECT * FROM categories_products WHERE product_id=$obj->id";
        $rows = DB::select($query);
        $categories = [];
        foreach ($rows as $row) {
            $cat = \Category::withTrashed()->find($row->category_id);
            $cat->fillLanguages();
            $categories[] = $cat->name_it;
        }
        $v = (count($categories) > 0) ? implode(", ", $categories) : "-";
        $tbody .= "<tr><td class='width50 align-right bold'>Categorie</td><td class='center'>$v</td></tr>";

        $v = '-';
        if ($obj->default_category_id > 0) {
            $cat = \Category::withTrashed()->find($obj->default_category_id);
            $cat->fillLanguages();
            $v = $cat->name_it;
        }
        $tbody .= "<tr><td class='width50 align-right bold'>Categoria di default</td><td class='center'>$v</td></tr>";

        $v = '-';
        if ($obj->main_category_id > 0) {
            $cat = \Category::withTrashed()->find($obj->main_category_id);
            $cat->fillLanguages();
            $v = $cat->name_it;
        }
        $tbody .= "<tr><td class='width50 align-right bold'>Categoria principale</td><td class='center'>$v</td></tr>";

        $v = '-';
        if ($obj->brand_id > 0) {
            $cat = \Brand::withTrashed()->find($obj->brand_id);
            $cat->fillLanguages();
            $v = $cat->name_it;
        }
        $tbody .= "<tr><td class='width50 align-right bold'>Brand</td><td class='center'>$v</td></tr>";

        $v = '-';
        if ((int)$obj->collection_id > 0) {
            $cat = \Collection::withTrashed()->find($obj->collection_id);
            if ($cat) {
                $cat->fillLanguages();
                $v = $cat->name_it;
            } else {
                $v = $obj->collection_id;
            }
        }
        $tbody .= "<tr><td class='width50 align-right bold'>Collezione</td><td class='center'>$v</td></tr>";


        foreach ($data as $key => $value) {

            $tbody .= "<tr><td class='width50 align-right bold'>$key</td>";

            $v = $obj->{$value};
            $tbody .= "<td class='center'>$v</td>";

            $tbody .= "</tr>";
        }

        $v = Format::money($obj->buy_price);
        $tbody .= "<tr><td class='width50 align-right bold'>Prezzo di acquisto tasse escl.</td><td class='center'>&euro; $v</td></tr>";
        $v = Format::money($obj->sell_price);
        $tbody .= "<tr><td class='width50 align-right bold'>Prezzo di vendita tasse escl.</td><td class='center'>&euro; $v</td></tr>";
        $v = Format::money($obj->price);
        $tbody .= "<tr><td class='width50 align-right bold'>Prezzo di vendita tasse incl.</td><td class='center'>&euro; $v</td></tr>";
        $v = $obj->weight;
        $tbody .= "<tr><td class='width50 align-right bold'>Peso</td><td class='center'>$v kg</td></tr>";
        $v = $obj->qty;
        $tbody .= "<tr><td class='width50 align-right bold'>Quantità</td><td class='center'>$v</td></tr>";


        $tbody .= '</tbody>';

        $tdead = "<tdead><tr><th class='center'>Campo</th><th class='center'>Valore</th></tr>";

        $table = $tdead . $tbody;

        return $table;
    }

    static function getPreviewImages($obj)
    {
        $query = "SELECT * FROM images WHERE product_id=$obj->id";
        $rows = DB::select($query);
        $table = '';
        $thead = '<thead><tr><th width="150" class="center">Immagine</th><th class="center">Dettagli</th><th class="center" width="150">Cover</th><th width="150" class="center">Posizione</th></tr></thead>';
        $tbody = '<tbody>';
        if (count($rows) > 0) {
            $path = public_path() . "/assets/products/";
            $uri = Site::root();
            foreach ($rows as $row) {
                $realfile = $path . $row->filename;

                $dimensions = $type = $size = $size_str = 'N.D.';

                if (File::exists($realfile) AND is_readable($realfile)) {
                    try {
                        $image = \Image::make($realfile);
                        $dimensions = $image->getWidth() . " X " . $image->getHeight();
                        $type = Str::upper($image->extension);
                        $size = File::size($realfile);
                        $size_str = Format::bytes($size);
                    } catch (Exception $e) {

                    }
                }

                $cover = ($row->cover == 1) ? 'Sì' : 'No';
                $tbody .= <<<ROW
<tr><td class="center">        
            <img  style="max-width:80px" class="valign_middle tblImgPreview" src="$uri/images/small/assets/products/{$row->filename}" />
    </td>
    <td class="center">        
        <strong>Dimensioni:</strong> $dimensions <strong class="ml20">Tipo:</strong> $type <strong class="ml20">Taglia:</strong> $size_str
    </td>  
    <td class="center">        
        $cover
    </td>  
    <td class="center">        
        $row->position
    </td>  
</tr>
ROW;
            }
        } else {
            $tbody .= '<tr><td colspan=9>Nessun immagine presente per questo prodotto</td></tr>';
        }
        $tbody .= '</tbody>';
        $table = $thead . $tbody;

        return $table;
    }

    public static function getRelatedVideo($product_id)
    {
        $key = 'related_images_product_' . $product_id;
        $ids = [];
        if (Registry::has($key)) {
            $ids = Registry::get($key);
        }
        $html = '';
        if ($ids) {
            foreach ($ids as $id) {
                $obj = \ImageSet::withTrashed()->find($id);
                if ($obj->youtube != "") {
                    $html .= <<<HTML
<div class="control-group warning">
    <label class="control-label align-right">Video Youtube (ereditato da $obj->uname <a class="mini-button" href="javascript:Echo.remoteModal('/admin/images_sets/preview/$obj->id');">ANTEPRIMA</a>):</label><div class="controls"><textarea class="span6" disabled>$obj->youtube</textarea></div></div>                            
HTML;
                }
                if ($obj->video != "") {
                    $html .= <<<HTML
<div class="control-group warning">
    <label class="control-label align-right">Video Custom (ereditato da $obj->uname <a class="mini-button" href="javascript:Echo.remoteModal('/admin/images_sets/preview/$obj->id');">ANTEPRIMA</a>):</label><div class="controls"><textarea class="span6" disabled>$obj->video</textarea></div></div>                            
HTML;
                }
            }
        }
        return $html;
    }


    static public function getSeoBlocksForProducts($product_id)
    {
        $blocks = self::newSeoBlocksProducts($product_id);
        if (count($blocks)) {
            return $blocks;
        }
        return null;
    }


    static function newSeoBlocksProducts($product_id)
    {
        $obj = \Product::withTrashed()->find($product_id);
        $ids_add = [];
        $blocks = \SeoBlock::where("active", 1)->where("context", "product")->orderBy("xtype")->orderBy("position")->get();
        foreach ($blocks as $block) {
            \Utils::log($block->id, __METHOD__);
            if ($block->solveByProduct($obj)) {
                $block->solved = true;
                $ids_add[] = $block;
                \Utils::log("RESOLVED: $block->id", __METHOD__);
            } else {
                if ($block->conditions == null OR trim($block->conditions) == '') {
                    $block->solved = false;
                    $ids_add[] = $block;
                }
            }
        }
        /*if(count($ids_add) == 0){
            $blocks = \SeoBlock::where("active", 1)->where("conditions", "")->where("context", "product")->orderBy("xtype")->orderBy("position")->get();
            foreach($blocks as $block){
                    $ids_add[] = $block;
                    \Utils::log("RESOLVED: $block->id",__METHOD__);
            }
        }*/
        return $ids_add;

    }


    static function generateSeo($product_id, $return_array = false)
    {
        $blocks = self::getSeoBlocksForProducts($product_id);
        $xtypes = \Mainframe::seoXTypes();
        $data = [];
        $languages = \Mainframe::languagesCodes();

        $model = \Product::find($product_id);
        $token = new Token("");
        $token->set("sku", $model->sku);
        $token->set("product_id", $model->id);
        $token->set("brand_id", $model->brand_id);
        $token->set("collection_id", $model->collection_id);
        $token->set("category_id", $model->main_category_id);
        $token->set("default_category_id", $model->default_category_id);


        foreach ($languages as $lang) {
            $data[$lang] = [];
            foreach ($xtypes as $key => $value) {
                $data[$lang][$key] = "";
            }
        }

        foreach ($blocks as $block) {
            $key = $block->xtype;
            if ($block->solved == true) {
                foreach ($languages as $lang) {
                    $data[$lang][$key] .= " " . $block->{$lang}->content;
                    $data[$lang][$key] = ltrim($data[$lang][$key], " ");
                    $token->set("product", $model->{$lang}->name);
                    $token->set("attributes", $model->{$lang}->attributes);
                    $token->setLang($lang);
                    $token->rebind($data[$lang][$key]);
                    $data[$lang][$key] = $token->render();
                    $data[$lang][$key] = str_replace(' - ', ' ', $data[$lang][$key]);
                }
            }
        }

        foreach ($blocks as $block) {
            $key = $block->xtype;
            if ($block->solved == false) {
                foreach ($languages as $lang) {
                    $content = $block->{$lang}->content;
                    if ($data[$lang][$key] == "" AND $content) { //if is not setted yet
                        $data[$lang][$key] .= " " . $content;
                        $data[$lang][$key] = ltrim($data[$lang][$key], " ");
                        $token->set("product", $model->{$lang}->name);
                        $token->set("attributes", $model->{$lang}->attributes);
                        $token->setLang($lang);
                        $token->rebind($data[$lang][$key]);
                        $data[$lang][$key] = $token->render();
                        $data[$lang][$key] = str_replace(' - ', ' ', $data[$lang][$key]);
                    }
                }
            }
        }
        if ($return_array) {
            return $data;
        }
        $fields = [];
        foreach ($data as $lang => $arr) {
            foreach ($arr as $key => $value) {
                if ($key != 'h1') {
                    $key = "meta" . $key;
                }
                $fields[$key . "_" . $lang] = $value;
            }
        }
        return $fields;
    }

    static function groupOperationSetPattern($options, $name, $ids, $entity, &$messages)
    {
        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) {
            $field = $options[$name . "_" . $lang];
            $pattern = isset($options["mtt_" . $name]) ? (int)$options["mtt_" . $name] : 0;
            if ($pattern > 0) {
                foreach ($ids as $id) {
                    $model = \Product::find($id);
                    $mtt = \MagicTokenTemplate::find($pattern);
                    $token = new Token($mtt->{$lang}->content);
                    //\Utils::log($mtt->{$lang}->content,"Resolving");
                    $token->setLang($lang);
                    $token->set("sku", $model->sku);
                    $token->set("brand_id", $model->brand_id);
                    $token->set("collection_id", $model->collection_id);
                    $token->set("category_id", $model->main_category_id);
                    $token->set("default_category_id", $model->default_category_id);
                    $token->set("product_id", $model->id);
                    $token->set("attributes", $model->{$lang}->attributes);
                    $v = $token->render();
                    DB::table("products_lang")->where("product_id", $id)->where("lang_id", $lang)->update([$name => $v]);
                }
                $messages[] = "Modifica valore del campo $entity avvenuta con successo";
            } else {
                if ($field != '') {
                    foreach ($ids as $id) {
                        DB::table("products_lang")->where("product_id", $id)->where("lang_id", $lang)->update([$name => $field]);
                    }
                    $messages[] = "Modifica valore del campo $entity avvenuta con successo";
                }
            }
        }
    }


    static function getTagList()
    {
        return '';
        $languages = \Mainframe::languagesCodes();
        $code = '';
        foreach ($languages as $lang) {

            $tags = \Tag::where("lang_id", $lang)->lists("tag");
            \Utils::log($tags, "tags");
            $tags_js = count($tags) > 0 ? '"' . implode("\",\"", $tags) . '"' : "";
            $code .= <<<CODE
var _DEFAULT_TAGS_{$lang} = [$tags_js]; 
CODE;
        }
        return $code;
    }


    static function handleMetadata(Product $model)
    {
        $id = $model->id;

        $languages = \Mainframe::languagesCodes();

        $token = new Token("");
        $token->set("sku", $model->sku);
        $token->set("brand_id", $model->brand_id);
        $token->set("collection_id", $model->collection_id);
        $token->set("category_id", $model->main_category_id);
        $token->set("default_category_id", $model->default_category_id);

        $md = Metadata::getInstance();

        $query = "SELECT attribute_id FROM products_attributes_position WHERE product_id=$id ORDER BY position";
        $rows = DB::select($query);

        $query = "SELECT category_id FROM categories_products WHERE product_id=$id";
        $categories = DB::select($query);

        foreach ($languages as $lang) {
            $attributes = array();
            $token->setLang($lang);
            $category_name = $token->fetch("category");

            foreach ($categories as $cat) {
                $token->set("default_category_id", $cat->category_id);
                $md->index($category_name . " " . $token->fetch("category_default"));
            }
            $md->index($token->fetch("brand"));
            $md->index($token->fetch("collection"));
            $md->index($token->fetch("sku"));
            $md->index($model->$lang->name);

            foreach ($rows as $row) {
                //Utils::log($row->attribute_id,"PARSING ATTRIBUTE");
                try {
                    $ah = new AttributeHtml($row->attribute_id, $lang);
                    $ah->setProductValue($id);
                    $meta = $ah->getMeta();
                    $token->rebind($meta->value);
                    $meta->value = $token->render();
                    if (trim($meta->value) != '') {
                        $string = $meta->label . ": " . $meta->value;
                        $string_index = $meta->label . " " . $meta->value;
                        $md->index($string_index);
                        $md->attributes($string);
                        $attributes[] = $meta;
                    }
                    unset($ah);
                } catch (\Exception $e) {
                    \Utils::log("I cannot resolve AttributeHtml with ID $row->attribute_id for product [$id]", __METHOD__);
                }

            }
            $md->set("attributes", $attributes);
            $md->set("fields", $md->getLang($lang, "fields"));
            $md->update($model, $lang);
        }

        unset($md);
        unset($token);
        unset($categories);
        unset($rows);
        gc_collect_cycles();
    }


    static function getSuppliersTable($id)
    {
        if ($id == 0) {
            return null;
        }

        $table = <<<TABLE
<div class="body">
<div class="well">
    <div class="navbar">
        <div class="navbar-inner">
            <h5>{{ name }}</h5>
        </div>
    </div>
    <table id="imagesTable" class="table table-striped table-input">
    <thead>
        <tr>
            <th>Prodotto</th>
            <th>Fornitore</th>
            <th width="130" class="center">Prezzo tasse escluse</th>
            <th width="130" class="center">Valuta</th>
        </tr>
    </thead>
    <tbody>
    {{ rows }}
    </tbody>
    </table>
    </div>
</div>
TABLE;

        $html = '';
        $product = \Product::getObj($id);
        $suppliers = \DB::table("products_suppliers")->where("product_id", $id)->lists('supplier_id');
        foreach ($suppliers as $supplier_id) {
            $obj = \Supplier::getObj($supplier_id);

            $html_rows = '';
            $rows = \DB::table("products_suppliers")->where("product_id", $id)->where("supplier_id", $obj->id)->get();

            foreach ($rows as $row) {
                $price = \Form::input("text", "suppliers[$obj->id][supplier_price_nt]", \Format::currency($row->supplier_price_nt, false, $row->currency_id), ['class' => 'span12', 'autocomplete' => 'off']);
                $currency = \Form::select("suppliers[$obj->id][currency_id]", \Mainframe::selectCurrencies(), $obj->currency_id, ['class' => 'span12']);

                $html_rows .= <<<TPL
<tr>
<td>{$product->name}</td>
<td>{$obj->name}</td>
<td>{$price}</td>
<td>{$currency}</td>
</tr>
TPL;

            }

            $html .= \Theme::twigy($table, ['name' => $obj->name, 'rows' => $html_rows]);
        }
        return $html;
    }


    public static function bindCategoriesAncestors(&$categories, $findIn)
    {
        if(!is_array($findIn) or (is_array($findIn) and empty($findIn)))
            return;
        $rows = \Registry::remember(__METHOD__ . implode('-', $findIn), static function() use($findIn){
            return \DB::table('categories')->whereIn('id', $findIn)->select(['id', 'parent_id'])->get();
        });
        if (count($rows)) {
            foreach ($rows as $row) {
                if ($row->parent_id > 0) {
                    $categories[] = $row->parent_id;
                    self::bindCategoriesAncestors($categories, [$row->parent_id]);
                }
            }
        }
    }


    static function buildProductCache($product_id, $withMetadata = true)
    {
        if($product_id instanceof Product){
            $obj = $product_id;
            $product_id = $obj->id;
        }else{
            $obj =  \Product::find($product_id);
        }

        self::mapCombinationsAttributesToMaster($obj);
        $data = [];
        $categories = [];
        $categories[] = $obj->main_category_id;

        $ids = \DB::table('categories_products')->where('product_id', $product_id)->lists('category_id');
        foreach ($ids as $id) {
            $categories[] = $id;
        }
        self::bindCategoriesAncestors($categories, $categories);
        $categories = array_unique($categories);
        $data['cache_categories'] = '|' . implode('|', $categories) . '|';
        $data['cnt_categories'] = count($categories);

        $query = "select products_attributes.attribute_id,
products_attributes.attribute_val 
from products_attributes 
join attributes on products_attributes.attribute_id=attributes.id
join products_attributes_position on products_attributes.attribute_id=products_attributes_position.attribute_id
AND products_attributes.product_id=products_attributes_position.product_id  
where products_attributes.product_id=$product_id 
and isnull(attributes.deleted_at)
and attributes.frontend_input in ('select','multiselect')
order by products_attributes_position.position";
        $rows = \DB::select($query);
        $attributes = [];
        $attributes_val = [];
        foreach ($rows as $row) {
            $attributes[] = $row->attribute_id;
            $attributes_val[] = $row->attribute_val;
            if ($row->attribute_id == 214) {
                $data['cache_gender'] = (int)$row->attribute_val;
            }
        }

        $data['cache_attributes'] = '|' . implode('|', $attributes) . '|';
        $data['cache_attributes_val'] = '|' . implode('|', $attributes_val) . '|';
        $data['cnt_attributes'] = count($attributes);

        $cache_propagate = ['cache_gender', 'cache_categories', 'cache_attributes', 'cache_attributes_val'];
        $data_propagate = [];
        foreach ($cache_propagate as $field) {
            if (isset($data[$field]))
                $data_propagate[$field] = $data[$field] === '||' ? null : $data[$field];
        }
        try {
            DB::table('cache_products')->where('id', $product_id)->update($data_propagate);
        } catch (Exception $e) {

        }

        $default_img_id = \DB::table('images')->where('product_id', $product_id)->where('active', 1)->orderBy('cover', 'desc')->orderBy('position')->pluck('id');
        $data['default_img'] = ($default_img_id > 0) ? $default_img_id : 0;
        $data['cnt_images'] = \DB::table('images')->where('product_id', $product_id)->where('active', 1)->count();

        $now = time();
        if ($obj->new_from_date) {
            $new_from = strtotime($obj->new_from_date);
            if ($now >= $new_from) {
                $data['is_new'] = 1;
            } else {
                $data['is_new'] = 0;
            }
        }
        if ($obj->new_to_date) {
            $new_to = strtotime($obj->new_to_date);
            if ($now <= $new_to) {
                $data['is_new'] = 1;
            } else {
                $data['is_new'] = 0;
            }
        }

        //get price rules
        $rules_ids = [];
        //$rules = \DB::table("products_specific_prices")->where('product_id', $product_id)->get();
        $rules = ProductPrice::withProduct($product_id)->active()->available()->get();
        $data['cache_price_rules'] = null;
        $data['is_in_promotion'] = 0;
        foreach ($rules as $rule) {
            $from_passed = false;
            $to_passed = false;
            if ($rule->date_from) {
                $rule_from = strtotime($rule->date_from);
                if ($now >= $rule_from) {
                    $from_passed = true;
                }
            }
            if ($rule->date_to) {
                $rule_to = strtotime($rule->date_to);
                if ($now <= $rule_to) {
                    $to_passed = true;
                }
            }
            //both given, the time has to in both limits
            if ($rule->date_from and $rule->date_to) {
                if ($from_passed and $to_passed)
                    $rules_ids[] = $rule->id;
            } else {
                //only date from given
                if (is_null($rule->date_to)) {
                    if ($from_passed)
                        $rules_ids[] = $rule->id;
                }

                //only date to given
                if (is_null($rule->date_from)) {
                    if ($to_passed)
                        $rules_ids[] = $rule->id;
                }
            }
            if ($rule->date_from == null AND $rule->date_to == null) {
                $rules_ids[] = $rule->id;
            }
        }
        if (count($rules_ids) > 0) {
            $data['is_in_promotion'] = 1;
            $data['cache_price_rules'] = '|' . implode('|', $rules_ids) . '|';
        }
        $nullable_caches = ['cache_categories', 'cache_attributes', 'cache_attributes_val', 'cache_price_rules'];

        foreach ($nullable_caches as $nullable_cache){
            if(isset($data[$nullable_cache]) and $data[$nullable_cache] === '||'){
                $data[$nullable_cache] = null;
            }
        }

        if (count($data) > 0) \DB::table('products')->where('id', $product_id)->update($data);

        if ($withMetadata) {
            self::handleMetadata($obj);
        }

        //\ProductHelper::generateImagesSetsCacheForProduct($obj); //TODO: this must be optimized for mass update

        //$obj->uncache(); TODO: this has been disabled because is fired from Optimizer
        //\Cache::forget("produce-price-rule-$product_id");
        \Product::forgetPriceRulesById($product_id);

        $obj = null;
        $rules_ids = null;
        $data = null;
        $rows = null;
        $ids = null;
        $categories = null;
        $attributes = null;
        $attributes_val = null;
        //gc_collect_cycles();

    }


    static function getRuleExplainedPublic($price_original, $price_final, $tax_group_id, $type, $value, $currency, $reduction_tax)
    {
        if ($reduction_tax == 0) {
            $tax = \TaxGroup::getPublicObj($tax_group_id);
            $ratio = $tax->getDefaultTaxRatio();
            $value = $value * $ratio;
        }

        $rule = null;
        switch ($type) {
            case 'percent':
                $percent = \Format::percentage($value, TRUE);
                if ($percent > 0)
                    $rule = "-" . $percent;
                break;
            case 'amount':
                $amount = \Format::currencyMin($value, TRUE, $currency);
                if ($amount > 0)
                    $rule = "-" . $amount;
                break;
            case 'fixed_amount':
                $price = \Format::currencyMin($price_original - $value, TRUE, $currency);
                if ($price > 0)
                    $rule = "-" . $price;
                break;
            case 'fixed_percent':
                $price = \Format::percentage($value, TRUE);
                if ($price > 0)
                    $rule = $price;
                break;
        }
        return $rule;
    }


    static function setBrandsVisibility()
    {
        $query = "SELECT B.id,count(P.id) as cnt FROM brands B LEFT JOIN products P ON P.brand_id=B.id GROUP BY B.id HAVING cnt=0";
        $rows = \DB::select($query);
        $ids_disable = [];
        $ids_out = [];
        $ids_in = [];
        foreach ($rows as $row) {
            $ids_disable[] = $row->id;
        }

        $query = "SELECT B.id,count(P.id) as cnt FROM brands B LEFT JOIN products P ON P.brand_id=B.id WHERE is_out_of_production=1 GROUP BY B.id";
        $rows = \DB::select($query);
        foreach ($rows as $row) {
            $ids_out[$row->id] = $row->cnt;
        }

        $query = "SELECT B.id,count(P.id) as cnt FROM brands B LEFT JOIN products P ON P.brand_id=B.id WHERE is_out_of_production=0 GROUP BY B.id";
        $rows = \DB::select($query);
        foreach ($rows as $row) {
            $ids_in[$row->id] = $row->cnt;
        }

        foreach ($ids_out as $key => $out) {
            if ($out > 0) {
                if (isset($ids_in[$key])) {

                } else {
                    $ids_disable[] = $key;
                }
            }
        }
        \DB::table('brands')->whereIn('id', $ids_disable)->update(['is_visible' => 0]);
        return $ids_disable;
    }


    static function forgetPriceRulesCache($rule_id = null)
    {
        if ($rule_id == null) {
            $rows = \DB::table('products')->where('is_in_promotion', 1)->whereNull('deleted_at')->get();
            foreach ($rows as $row) {
                //\Cache::forget("produce-price-rule-" . $row->id);
                \Product::forgetPriceRulesById($row->id);
                \Utils::log("Forgetting cache key: produce-price-rule-" . $row->id);
            }
        } else {
            $rows = \DB::table('products_specific_prices')->where('price_rule_id', $rule_id)->get();
            foreach ($rows as $row) {
                //\Cache::forget("produce-price-rule-" . $row->product_id);
                \Product::forgetPriceRulesById($row->product_id);
                \Utils::log("Forgetting cache key: produce-price-rule-" . $row->product_id);
            }
        }
    }


    static function refreshPriceRule($rule_id, $activeStatus)
    {
        $rule = PriceRule::find($rule_id);
        if ($rule) {
            DB::transaction(function () use ($rule_id, $activeStatus) {
                DB::table('price_rules')->where('id', $rule_id)->update(['active' => $activeStatus]);
                DB::table('products_specific_prices')->where('price_rule_id', $rule_id)->update(['active' => $activeStatus]);
                $rows = \DB::table('products_specific_prices')->where('price_rule_id', $rule_id)->get();
                foreach ($rows as $row) {
                    //\Cache::forget("produce-price-rule-" . $row->product_id);
                    \Product::forgetPriceRulesById($row->product_id);
                    \Utils::log("Forgetting cache key: produce-price-rule-" . $row->product_id);
                }
            });
        }
    }


    static function updateProductsStates()
    {
        DB::transaction(function () {
            $query = "DELETE FROM cache_products WHERE id in (select id from products where is_out_of_production=1)";
            DB::statement($query);
            DB::table('cache_products')->update(['is_in_promotion' => 0, 'is_new' => 0]);
            $query = "UPDATE cache_products SET is_in_promotion=1 WHERE id IN (SELECT id FROM products WHERE is_in_promotion=1)";
            DB::statement($query);
            $query = "UPDATE cache_products SET is_new=1 WHERE id IN (SELECT id FROM products WHERE is_new=1)";
            DB::statement($query);
            $ids = DB::table('cache_products')->lists('id');
            foreach ($ids as $id) {
                Product::unpublic($id);
            }
        });
    }


    static function compareProducts($ids)
    {

        $products = [];
        foreach ($ids as $id) {
            $obj = Product::getPublicObj($id);
            $obj->setFullData();
            $products[$id] = $obj;
        }
        $labels = [
            'img' => 'Anteprima',
            'name' => 'Nome',
            'price' => 'Prezzo',
            'category' => 'Categoria',
            'subcategory' => 'Sottocategoria',
            'brand' => 'Marca',
            'collection' => 'Collezione',
        ];

        $grid = [];

        foreach ($products as $p) {
            $row = [
                'name' => $p->name,
                'category' => $p->main_category_name,
                'subcategory' => $p->category_name,
                'brand' => $p->brand_name,
                'collection' => $p->collection_name,
                'price' => Format::currency($p->price_final_raw, true),
                'img' => "<img src='$p->smallImg'>",
                'id' => $p->id,
            ];
            $attributes = $p->getFrontAttributes();

            //\Utils::log($attributes,"PRODUCT ATTRIBUTES");
            foreach ($attributes as $key => $attr) {
                if (!isset($labels[$key])) {
                    $labels[$key] = $attr->label;
                }
                $row[$key] = $attr->value;
            }

            $grid[] = $row;


        }

        return compact('grid', 'labels', 'products');
    }


    static function getCompareProducts()
    {
        return Session::get('blade_compare_products', []);
    }

    static function addCompareProduct($id)
    {
        $compare = self::getCompareProducts();

        $limit = Cfg::get('PRODUCTS_LIMIT_COMPARE');
        if (count($compare) >= $limit) {
            return -1;
        }

        if (in_array($id, $compare)) {
            return false;
        }
        $compare[] = $id;
        Session::set('blade_compare_products', $compare);
        return true;
    }

    static function removeCompareProduct($id)
    {
        $compare = self::getCompareProducts();

        foreach ($compare as $key => $value) {
            if ($value == $id) {
                unset($compare[$key]);
            }
        }
        Session::set('blade_compare_products', $compare);
    }

    static function emptyCompareProducts()
    {
        Session::set('blade_compare_products', []);
    }


    static function generateImagesSetsCacheForProduct(Product $obj)
    {
        $imageSetManager = ImageSetManager::getInstance();
        $imageSetManager->bindIds('CAT', $obj->default_category_id);
        $imageSetManager->bindIds('CAT', $obj->main_category_id);
        $imageSetManager->bindIds('BRA', $obj->brand_id);
        $imageSetManager->bindIds('COL', $obj->collection_id);
        $imageSetManager->bindProductAttributes($obj);

        /*$ids_add = array_merge($ids_add, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_all_plus')->where("target_mode", "*")->where("target_id", 0)->lists("image_set_id"));
        $ids_remove = array_merge($ids_remove, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_all_minus')->where("target_mode", "-")->where("target_id", 0)->lists("image_set_id"));

        if ($obj->default_category_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_cat_plus_' . $obj->default_category_id)->where("target_type", "CAT")->where("target_mode", "+")->where("target_id", $obj->default_category_id)->lists("image_set_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_cat_minus_' . $obj->default_category_id)->where("target_type", "CAT")->where("target_mode", "-")->where("target_id", $obj->default_category_id)->lists("image_set_id"));
        }
        if ($obj->main_category_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_cat_plus_' . $obj->main_category_id)->where("target_type", "CAT")->where("target_mode", "+")->where("target_id", $obj->main_category_id)->lists("image_set_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_cat_minus_' . $obj->main_category_id)->where("target_type", "CAT")->where("target_mode", "-")->where("target_id", $obj->main_category_id)->lists("image_set_id"));
        }
        if ($obj->collection_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_col_plus_' . $obj->collection_id)->where("target_type", "COL")->where("target_mode", "+")->where("target_id", $obj->collection_id)->lists("image_set_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_col_minus_' . $obj->collection_id)->where("target_type", "COL")->where("target_mode", "-")->where("target_id", $obj->collection_id)->lists("image_set_id"));
        }
        if ($obj->brand_id > 0) {
            $ids_add = array_merge($ids_add, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_brand_plus_' . $obj->brand_id)->where("target_type", "BRA")->where("target_mode", "+")->where("target_id", $obj->brand_id)->lists("image_set_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("images_sets_rules")->remember(60, 'images_sets_rules_brand_minus_' . $obj->brand_id)->where("target_type", "BRA")->where("target_mode", "-")->where("target_id", $obj->brand_id)->lists("image_set_id"));
        }
        $attributes_options = $obj->getProductAttributesOptionsIds(true);
        if (count($attributes_options) > 0) {
            $ids_add = array_merge($ids_add, \DB::table("images_sets_rules")->where("target_type", "ATT")->where("target_mode", "+")->whereIn("target_id", $attributes_options)->lists("image_set_id"));
            $ids_remove = array_merge($ids_remove, \DB::table("images_sets_rules")->where("target_type", "ATT")->where("target_mode", "-")->whereIn("target_id", $attributes_options)->lists("image_set_id"));
        }*/


        $final_ids = $imageSetManager->getImageSets($obj);
        $product_id = $obj->id;
        $stored_ids = DB::table('cache_products_sets')->where('product_id', $product_id)->lists('image_set_id');

        $left = array_diff($stored_ids, $final_ids);
        $right = array_diff($final_ids, $stored_ids);
        /*audit($stored_ids, 'STORED');
        audit($final_ids, 'FINAL');
        audit($left, 'LEFT');
        audit($right, 'RIGHT');*/

        //left is to delete
        if (!empty($left)) {
            DB::table('cache_products_sets')->where('product_id', $product_id)->whereIn('image_set_id', $left)->delete();
        }
        //right is to insert
        foreach ($right as $image_set_id) {
            DB::table('cache_products_sets')->insert(['product_id' => $product_id, 'image_set_id' => $image_set_id]);
        }

        unset($left, $right, $product_id, $stored_ids, $final_ids, $image_set_id, $imageSetManager);
    }


    static function generateImagesSetsCacheByGroup($obj)
    {

        $ids_add = [];
        $ids_remove = [];
        $final_ids = [];


        DB::table('cache_products_sets')->where('image_set_id', $obj->id)->delete();

        $rules = \DB::table("images_sets_rules")->where("image_set_id", $obj->id)->orderBy('position')->get();

        $query = "SELECT id FROM products WHERE deleted_at is null";

        foreach ($rules as $rule) {
            $target_id = $rule->target_id;
            switch ($rule->target_type) {
                case 'CAT':
                    switch ($rule->target_mode) {
                        case '+':
                            $query .= " AND (default_category_id=$target_id OR main_category_id=$target_id)";
                            break;
                        case '-':
                            $query .= " AND (default_category_id <> $target_id AND main_category_id <> $target_id)";
                            break;
                    }
                    break;
                case 'BRA':
                    switch ($rule->target_mode) {
                        case '+':
                            $query .= " AND brand_id=$target_id";
                            break;
                        case '-':
                            $query .= " AND brand_id <> $target_id";
                            break;
                    }
                    break;
                case 'COL':
                    switch ($rule->target_mode) {
                        case '+':
                            $query .= " AND collection_id=$target_id";
                            break;
                        case '-':
                            $query .= " AND collection_id <> $target_id";
                            break;
                    }
                    break;
                case 'ATT':
                    switch ($rule->target_mode) {
                        case '+':
                            $query .= " AND id IN (SELECT product_id FROM products_attributes WHERE attribute_val=$target_id AND attribute_id IN (SELECT id FROM attributes WHERE (frontend_input='select' OR frontend_input='multiselect')))";
                            break;
                        case '-':
                            $query .= " AND id NOT IN (SELECT product_id FROM products_attributes WHERE attribute_val=$target_id AND attribute_id IN (SELECT id FROM attributes WHERE (frontend_input='select' OR frontend_input='multiselect')))";
                            break;
                    }
                    break;
            }
        }

        \Utils::log($query, "generateImagesSetsCacheByGroup QUERY");
        $ids = DB::select($query);
        if (count($ids) > 0) {
            $image_set_id = $obj->id;
            DB::transaction(function () use ($image_set_id, $ids) {
                foreach ($ids as $i) {
                    $product_id = $i->id;
                    DB::table('cache_products_sets')->insert(['product_id' => $product_id, 'image_set_id' => $image_set_id]);
                }
            });
        }

    }


    static function renderProducts($params)
    {
        $defaults = [
            'template' => 'column',
            'start' => 0,
            'limit' => \Cfg::get('PRODUCTS_PER_PAGE'),
            'orderby' => 'position',
            'orderdir' => 'asc',
            'new' => 0,
            'outlet' => 0,
            'class' => '',
        ];

        $params = array_merge($defaults, $params);

        $catalog = new Catalog();

        if (isset($params['id'])) {
            $catalog->addCondition('id', $params['id']);
        } else {
            $valids = ['id', 'category', 'brand', 'collection', 'trend', 'minprice', 'maxprice', 'attribute', 'nav'];
            foreach ($valids as $model) {
                if (isset($params[$model]) AND $params[$model] != '') {
                    if (Str::contains($params[$model], ',')) {
                        $values = explode(',', $params[$model]);
                        foreach ($values as $value) {
                            //audit("$model: $value", 'ADDING CONDITION');
                            $catalog->addCondition($model, trim($value));
                        }
                    } else {
                        $value = $params[$model];
                        //audit("$model: $value", 'ADDING CONDITION');
                        $catalog->addCondition($model, $params[$model]);
                    }
                }
            }
        }

        $catalog->setWriteSession(false);
        $catalog->setPagesize($params['limit']);
        $catalog->setOrderParams($params['orderby'], $params['orderdir']);

        $products = $catalog->runAjax();


        switch ($params['template']) {
            case 'catalog':
                $partial = 'catalog';
                $default_class = 'col-lg-3 col-md-3 col-sm-6 col-xs-4 product-col cancel';
                break;
            case 'row':
                $partial = 'promo.products';
                $default_class = '';
                break;
            default:
            case 'column':
                $partial = 'productrel';
                $default_class = 'col-lg-3 col-md-3 col-sm-6 col-xs-4 product-col';
                break;
        }
        $class = ($params['class'] != '') ? $params['class'] : $default_class;

        $html = '';
        $rowClass = \Config::get('mobile', false) == false ? 'row' : 'row products-grid cancel flexgrid';
        if (\Config::get('mobile', false) == true) {
            $class .= ' flexitem';
        }
        if (count($products) > 0) {
            $html = FrontTpl::getTheme()->partial($partial, ['products' => $products, 'class' => $class]);
            $html = '<div class="' . $rowClass . '">' . $html . '</div>';
        }
        return $html;

    }


    static function cleanupCache()
    {
        $query = "DELETE FROM cache_products WHERE id IN (SELECT id FROM products WHERE is_out_of_production=1 OR deleted_at is not null)";
        DB::statement($query);
    }


    static function handleRevisions($before, $model)
    {
        $revisions = $model->revisions == null ? [] : unserialize($model->revisions);
        if (!is_array($revisions)) {
            $revisions = [];
        }
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            $beforeSlug = $before->{"slug_" . $lang};
            $modelSlug = $model->$lang->slug;
            \Utils::log("beforeSlug: $beforeSlug | modelSlug: $modelSlug", __METHOD__);
            if ($beforeSlug != $modelSlug) {
                $r = new stdClass();
                $r->slug = $beforeSlug;
                $r->lang = $lang;
                $r->date = \Format::now();
                $revisions[] = $r;
            }
        }
        if (count($revisions) > 0) {
            \Utils::log("Saving revisions", __METHOD__);
            \DB::table('products')->where('id', $model->id)->update(['revisions' => serialize($revisions)]);
        }
    }


    static function getItems($params)
    {

        $queries = [];
        $ids = [];
        $size = 0;
        $max = $params['max'];
        $order_by = $params['order_by'];
        $order_dir = $params['order_dir'];

        if ($params['products'] != '') {
            $ids = explode(",", $params['products']);
        }

        $queries[] = "SELECT id,price,sku,position FROM products WHERE is_out_of_production=0 AND deleted_at is null";

        if ($params['is_new']) {
            $queries[] = "AND is_new=1";
        }
        if ($params['is_featured']) {
            $queries[] = "AND is_featured=1";
        }
        if ($params['is_outlet']) {
            $queries[] = "AND is_outlet=1";
        }
        if ($params['is_in_promotion']) {
            $queries[] = "AND cache_price_rules is not null";
        }
        if ($params['categories'] != '') {
            $category_ids = explode(',', $params['categories']);
            foreach ($category_ids as $value) {
                $queries[] = "AND cache_categories LIKE '%|$value|%'";
            }
        }
        if ($params['brands'] != '') {
            $queries[] = "AND brand_id IN({$params['brands']})";
        }

        if (count($queries) == 1 AND $params['products'] != '') {
            $queries[] = "AND id IN({$params['products']})";
        }

        $queries[] = "ORDER BY $order_by $order_dir LIMIT $max";
        $query = implode(" ", $queries);

        //\Utils::log($query,__METHOD__);
        //\Utils::log($ids,__METHOD__);

        $rows = \DB::select($query);
        $products = [];

        if (count($ids) > 0) {

            if (count($ids) > 0) {
                foreach ($rows as $row) {
                    $ids[] = $row->id;
                }
            }
            $counter = 0;
            foreach ($ids as $id) {
                if ($counter < $max) {
                    $obj = \Product::getPublicObj($id);
                    if ($obj) {
                        $obj->setFullData();
                        $products[] = $obj;
                        $counter++;
                    }
                }
            }
        }
        return $products;
    }


    static function setMostWantedProducts()
    {
        $days = (int)config('catalog.products.most_wanted_days', 30 * 6);
        $start_date = Carbon::now()->addDays(-1 * $days)->format('Y-m-d');

        DB::transaction(function () use ($start_date) {
            DB::table('cache_products')->update(['wanted' => 0]);
            DB::table('products')->update(['wanted' => 0]);
            $query = "SELECT P.id, count(D.id) as wanted 
FROM products P 
left join order_details D ON P.id=D.product_id 
WHERE is_out_of_production=0
 AND isnull(P.deleted_at)
 AND isnull(D.deleted_at)
 AND D.created_at >= '$start_date'
group by P.id having wanted > 0 order by wanted desc";
            $rows = DB::select($query);
            audit($query, __METHOD__);
            foreach ($rows as $row) {
                //echo "Updating $row->id".PHP_EOL;
                DB::table('cache_products')->where('id', $row->id)->update(['wanted' => $row->wanted]);
                DB::table('products')->where('id', $row->id)->update(['wanted' => $row->wanted]);
            }
        });

    }


    static function getDeliveryDate($startDate, $currentHour)
    {
        $year = date('Y');

        $daysMaps = [
            'MO' => 1,
            'TU' => 2,
            'WE' => 3,
            'TH' => 4,
            'FR' => 5,
            'SA' => 6,
            'SU' => 7,
        ];

        $defaultBusinessDays = (int)Cfg::get('BUSINESS_DAYS', 2) - 1;
        //$defaultAvailabilityDays = Cfg::get('DEFAULT_AVAILABILITY_DAYS',16);
        $switchHour = Cfg::get('AVAILABILITY_SWITCH_HOUR', 13);
        $providedHolydays = explode(PHP_EOL, Cfg::get('GENERAL_HOLYDAYS', '01-01'));
        $providedWeekHolydays = explode(',', Cfg::get('WEEK_HOLIDAYS', 'SA,SU'));
        $holidays = [];
        for ($i = $year; $i < $year + 1; $i++) {
            foreach ($providedHolydays as $providedHolyday) {
                $holidays[] = new DateTime($i . '-' . $providedHolyday);
            }
        }

        $freeWeekDays = [];

        foreach ($providedWeekHolydays as $providedWeekHolyday) {
            $freeWeekDays[] = $daysMaps[$providedWeekHolyday];
        }

        $calculator = new BusinessDay();
        $calculator->setStartDate(new \DateTime($startDate));
        $calculator->setFreeWeekDays($freeWeekDays); // repeat every week
        $calculator->setHolidays($holidays);         // repeat every year
        $calculator->setFreeDays([]);         // don't repeat

        $dayOfWeek = date('N', strtotime($startDate));

        switch ($dayOfWeek) {
            default:
            case 1: //lun
            case 2: //mar
            case 3: //mer
            case 4: //gio
                $businessDay = $defaultBusinessDays;
                if ($currentHour >= $switchHour) {
                    $businessDay++;
                }
                break;

            case 5: //ven
                $businessDay = $defaultBusinessDays + 1;
                if ($currentHour >= $switchHour) {
                    $businessDay++;
                }
                break;

            case 6: //sab
            case 7: //sab
                $businessDay = $defaultBusinessDays + 1;
                break;
        }

        $calculator->addBusinessDays($businessDay);  // add X working days

        $result = $calculator->getDate();            // \DateTime
        $finalDay = $result->format('Y-m-d');        // 2001-01-03

        return $finalDay;
    }


    static function businessDays($start, $end)
    {

        $from = Carbon::parse($start);
        $to = Carbon::parse($end);
        return $to->diffInWeekdays($from);

        $year = date('Y');

        $daysMaps = [
            'MO' => 1,
            'TU' => 2,
            'WE' => 3,
            'TH' => 4,
            'FR' => 5,
            'SA' => 6,
            'SU' => 7,
        ];

        $providedHolydays = explode(PHP_EOL, Cfg::get('GENERAL_HOLYDAYS', '01-01'));
        $providedWeekHolydays = explode(',', Cfg::get('WEEK_HOLIDAYS', 'SA,SU'));

        $holidays = [];
        for ($i = $year; $i <= $year + 1; $i++) {
            foreach ($providedHolydays as $providedHolyday) {
                $holidays[] = new DateTime($i . '-' . $providedHolyday);
            }
        }

        $freeWeekDays = [];

        foreach ($providedWeekHolydays as $providedWeekHolyday) {
            $freeWeekDays[] = $daysMaps[$providedWeekHolyday];
        }
    }


    static function craftProductName($product, $lang_id)
    {
        if (is_integer($product)) {
            $product = Product::getObj($product);
        }

        $category_id = $product->default_category_id;
        $parent_category_id = $product->main_category_id;
        if ($parent_category_id == 1) {
            $product_name_rule = [
                'it' => ['parent_category', 'brand', 'collection', 'sku'],
                'es' => ['parent_category', 'brand', 'collection', 'sku'],
                'fr' => ['parent_category', 'brand', 'collection', 'sku'],
                'en' => ['brand', 'collection', 'parent_category', 'sku'],
                'de' => ['brand', 'collection', 'parent_category', 'sku'],
            ];
        } else {
            $product_name_rule = [
                'it' => ['category', 'brand', 'collection', 'sku'],
                'es' => ['category', 'brand', 'collection', 'sku'],
                'fr' => ['category', 'brand', 'collection', 'sku'],
                'en' => ['brand', 'collection', 'category', 'sku'],
                'de' => ['brand', 'collection', 'category', 'sku'],
            ];
        }


        $rules = $product_name_rule[$lang_id];
        $tokens = [];
        foreach ($rules as $rule) {
            switch ($rule) {
                case 'category':
                    $category = \Category::getObj($category_id, $lang_id);
                    if ($category) {
                        if ($category->singular != '') {
                            $tokens[] = $category->singular;
                        } else {
                            $tokens[] = $category->name;
                        }
                    }
                    break;

                case 'parent_category':
                    $category = \Category::getObj($parent_category_id, $lang_id);
                    if ($category) {
                        if ($category->singular != '') {
                            $tokens[] = $category->singular;
                        } else {
                            $tokens[] = $category->name;
                        }
                    } else {
                        $category = \Category::getObj($category_id, $lang_id);
                        if ($category) {
                            if ($category->singular != '') {
                                $tokens[] = $category->singular;
                            } else {
                                $tokens[] = $category->name;
                            }
                        }
                    }
                    break;

                case 'brand':
                    $brand = \Brand::getObj($product->brand_id, $lang_id);
                    if ($brand) {
                        $tokens[] = $brand->name;
                    }
                    break;

                case 'collection':
                    $collection = \Collection::getObj($product->collection_id, $lang_id);
                    if ($collection)
                        $tokens[] = $collection->name;
                    break;

                case 'sku':
                    $tokens[] = '- ' . $product->sku;
                    break;
            }
        }
        return Str::upper(implode(' ', $tokens));
    }

    /**
     * @param $shop_id
     * @param Product $product
     * @param $quantity
     * @param ProductCombination|null $combination
     * @param null $order_id
     * @return bool
     */
    static function checkShopQuantities($shop_id, Product $product, $quantity, ProductCombination $combination = null, $order_id = null)
    {
        if (feats()->switchEnabled(Core\Cfg::SERVICE_SHOP_AVAILABILITY)) {
            /** @var Connection $conn */
            $conn = \Core::getNegoziandoConnection();
            if($order_id > 0){
                $conn->setExcludeOrder($order_id);
            }
            $sku = ($combination) ? \Utils::getSapSku($combination->sku, $combination->sap_sku) : \Utils::getSapSku($product->sku, $product->sap_sku);
            $conn->addProduct($sku, $quantity, $product->id);
            $maximumQty = $conn->getMaximumOrderableQty($sku, $shop_id);
            audit($maximumQty, 'RECALCULATED QUANTITY FOR ' . "$sku, " . $shop_id);
            if ($maximumQty < $quantity) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * @param Product $product
     * @param $quantity
     * @param ProductCombination|null $combination
     * @return bool
     */
    static function checkOnlineQuantities(Product $product, $quantity, ProductCombination $combination = null)
    {
        $product->combination = $combination;
        $product->setFullData();
        $maximumQty = $product->getOnlineAvailability();
        if ($maximumQty < $quantity) {
            return false;
        }
        return true;
    }

    /**
     * @param Product $product
     * @param $quantity
     * @param ProductCombination|null $combination
     * @param null $order_id
     * @return bool
     */
    static function findShopQuantities(Product $product, $quantity, ProductCombination $combination = null, $order_id = null)
    {
        if (feats()->switchDisabled(Core\Cfg::SERVICE_SHOP_AVAILABILITY)) {
            return false;
        }
        /** @var Connection $conn */
        $conn = \Core::getNegoziandoConnection();
        if($order_id > 0){
            $conn->setExcludeOrder($order_id);
        }
        $sku = ($combination) ? \Utils::getSapSku($combination->sku, $combination->sap_sku) : \Utils::getSapSku($product->sku, $product->sap_sku);
        $shops = $conn->addProduct($sku, $quantity, $product->id);
        audit($shops, __METHOD__, 'SHOPS');
        return !empty($shops);
    }


    static function mapCombinationsAttributesToMaster($id)
    {
        if($id instanceof Product){
            /** @var Product $product */
            $product = $id;
            $id = $product->id;
        }else{
            /** @var Product $product */
            $product = Product::getObj($id);
        }

        if ($product === null)
            return;

        if(false === $product->hasCombinations())
            return;

        $combinations = $product->getCombinations();

        $options = [];
        foreach ($combinations as $combination) {
            $combinationOptions = $combination->getOptionsMap();
            foreach ($combinationOptions as $key => $value) {
                if (!isset($options[$key]))
                    $options[$key] = [];

                $options[$key][] = $value;
            }
        }
        if (empty($options))
            return;

        //reorder options with the real record position
        foreach ($options as $attribute_id => $options_ids) {
            $options[$attribute_id] = AttributeOption::whereIn('id', $options_ids)->orderBy('position')->lists('id');
        }


        //now we have an array with:
        // attribute_id => [option_id, option_id, ...]
        // attribute_id => [option_id, option_id, ...]
        //audit($options, __METHOD__);

        $product_id = $id;
        //we have to add the attribute to the master product, and add all options like if there are all for the same product
        foreach ($options as $attribute_id => $options_ids) {
            //check if the attribute is mapped for the product, if not insert a new one
            $existing = DB::table('products_attributes_position')->where(compact('product_id', 'attribute_id'))->first();
            if (is_null($existing)) {
                $position = DB::table('products_attributes_position')->where('product_id', $product_id)->max('position') + 1;
                DB::table('products_attributes_position')->insert(compact('product_id', 'attribute_id', 'position'));
            }
            //since the attribute nature must be 'select' or 'multiselect', first get all the 'attribute_val' for the products
            $mapped_options = DB::table('products_attributes')->where(compact('product_id', 'attribute_id'))->lists('attribute_val');

            foreach ($options_ids as $attribute_val) {
                if (!in_array($attribute_val, $mapped_options)) {
                    DB::table('products_attributes')->insert(compact('product_id', 'attribute_id', 'attribute_val'));
                }
            }

            //at the end, we must check if we have something to delete
            $deletable_options = array_diff($mapped_options, $options_ids);
            if (!empty($deletable_options)) {
                DB::table('products_attributes')->where(compact('product_id', 'attribute_id'))->whereIn('attribute_val', $deletable_options)->delete();
            }
        }
    }


    /**
     * @param $canvas
     * @return array
     */
    static function parseCanvas($canvas)
    {
        $data = ['new_from_date' => null, 'new_to_date' => null];
        if ($canvas != '') {
            $year = substr($canvas, 0, 4);
            $bimester = substr($canvas, 4, 2);
            $matrix = [
                '01' => '01',
                '02' => '03',
                '03' => '05',
                '04' => '07',
                '05' => '09',
                '06' => '11',
            ];
            $month = isset($matrix[$bimester]) ? $matrix[$bimester] : '02';
            $day = '01';
            $data['new_from_date'] = "$year-$month-$day";
            $data['new_to_date'] = date('Y-m-d', strtotime($data['new_from_date'] . ' +6 months'));
            $data['new_from_date'] = date('Y-m-d', strtotime($data['new_from_date'] . ' -1 months'));
            if ($data['new_from_date'] == '0000-00-00' or $data['new_from_date'] == '1970-01-01') {
                $data['new_from_date'] = null;
            }
            if ($data['new_to_date'] == '0000-00-00' or $data['new_to_date'] == '1970-01-01') {
                $data['new_to_date'] = null;
            }

            $now = time();
            $obj = (object)$data;
            if ($obj->new_from_date) {
                $new_from = strtotime($obj->new_from_date);
                if ($now >= $new_from) {
                    $data['is_new'] = 1;
                } else {
                    $data['is_new'] = 0;
                }
            }
            if ($obj->new_to_date) {
                $new_to = strtotime($obj->new_to_date);
                if ($now <= $new_to) {
                    $data['is_new'] = 1;
                } else {
                    $data['is_new'] = 0;
                }
            }
        }

        return $data;
    }

    /**
     * @param $obj
     * @return array
     */
    static function checkNewFlag($obj)
    {
        $now = time();
        $new_from = $new_to = 0;
        if ($obj->new_from_date) {
            $new_from = strtotime($obj->new_from_date);
        }
        if ($obj->new_to_date) {
            $new_to = strtotime($obj->new_to_date);
        }

        //both are setted, so now must be between the pivots
        $is_new = false;
        if ($new_from > 0 and $new_to > 0) {
            $is_new = $now >= $new_from and $now <= $new_to;
        } elseif ($new_from > 0) {
            //only new_from is setted
            $is_new = $now >= $new_from;
        } elseif ($new_to > 0) {
            //only new_to is setted
            $is_new = $now <= $new_to;
        }
        $is_new = (int)$is_new;
        return compact('is_new');
    }

    /**
     * @param $val
     * @return int|string
     */
    static function price($val)
    {
        if ($val <= 0) return 0;
        return number_format((float)$val, 3, '.', '');
    }

    /**
     * @param $price
     * @param null $tax_id
     * @return array
     */
    static function parsePrices($price, $tax_id = null)
    {
        $ratio = (is_null($tax_id)) ? \Core::getDefaultTaxRate() : \TaxGroup::getPublicObj($tax_id)->getDefaultRatio();
        $price_nt = self::price($price / $ratio);
        return [
            'price' => $price,
            'sell_price_wt' => $price,
            'sell_price' => $price_nt,
            'price_nt' => $price_nt,
        ];
    }

    /**
     * @param $id
     * @param $name
     * @param bool $empty
     * @return array
     */
    public static function prepareImageResources($id, $name, $empty = false){

        $manipulations = [
            'thumb',
            'small',
            'smallest',
            'default',
            'zoom',
            'huge',
        ];
        $relative = '/i/';
        $absolute = Site::img($relative, true);

        $empty_img = Site::emptyImg();

        $attributes = [];
        foreach($manipulations as $manipulation){
            $base_relative = "{$relative}{$manipulation}/{$id}/{$name}";
            $base = "{$absolute}{$manipulation}/{$id}/{$name}";
            $attributes[ $manipulation . 'Img'] = ($empty) ? $empty_img : $base . '.jpg';
            $attributes[ $manipulation . 'ImgRetina'] = ($empty) ? $empty_img : $base . '@2x.jpg';
            $attributes[ $manipulation . 'Webp'] = ($empty) ? $empty_img : $base . '.webp';
            $attributes[ $manipulation . 'WebpRetina'] = ($empty) ? $empty_img : $base . '@2x.webp';

            if('default' === $manipulation){
                $attributes[ $manipulation . 'ImgRelative'] = ($empty) ? $empty_img : $base_relative . '.jpg';
                $attributes[ $manipulation . 'ImgRelativeRetina'] = ($empty) ? $empty_img : $base_relative . '@2x.jpg';
                $attributes[ $manipulation . 'WebpRelative'] = ($empty) ? $empty_img : $base_relative . '.webp';
                $attributes[ $manipulation . 'WebpRelativeRetina'] = ($empty) ? $empty_img : $base_relative . '@2x.webp';
            }
        }
        return $attributes;
    }
}
