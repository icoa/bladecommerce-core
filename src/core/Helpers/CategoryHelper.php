<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-gen-2014 15.29.08
 */

use Carbon\Carbon;
use Underscore\Types\Arrays;

abstract class CategoryHelper {

    static public function getMainCategoryId($category_id) {
        $obj = \Category::find($category_id);
        if($obj->parent_id == 0){
            return $obj->id;
        }
        return self::getMainCategoryId($obj->parent_id);
    }
    

}
