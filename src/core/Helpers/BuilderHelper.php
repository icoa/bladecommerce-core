<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-gen-2014 15.29.08
 */

use Carbon\Carbon;
use HtmlObject\Element;
use HtmlObject\Image;
use HtmlObject\Traits\Tag;
use Underscore\Types\Arrays;
use Core\Condition\RuleResolver;

class BuilderHelper
{


    private $controller;
    private $fieldName;
    private $isDefault = FALSE;
    private $data;
    private $node;
    private $type;
    private $basetype;
    private $fulltype;
    private $attribute;

    function __construct($data, $controller, $fieldName = 'rule')
    {

        $this->controller = $controller;
        $this->fieldName = $fieldName;

        switch($this->controller){
            default:
                $this->basetype = 'site';
                break;
            case 'pricerules':
                $this->basetype = 'product';
                break;
            case 'general':
                $this->basetype = 'general';
                break;
            case 'cartrules':
                $this->basetype = 'cart';
                break;
        }
        $default_data = ["type" => "$this->basetype|rule_condition_combine", "aggregator" => "all", "value" => 1, "conditions" => []];
        if ($data == null) {
            $this->isDefault = TRUE;
            $this->data = [
                ["type" => "rule_condition_combine", "aggregator" => "all", "value" => 1, "conditions" => [
                    ["type" => "rule_condition_product", "attribute" => "category_ids", "operator" => "==", "value" => 9],
                    ["type" => "rule_condition_product", "attribute" => "brand_ids", "operator" => "==", "value" => 9],
                    ["type" => "rule_condition_product", "attribute" => "sku", "operator" => "==", "value" => 9],
                    ["type" => "rule_condition_product", "attribute" => "gender", "operator" => "==", "value" => 25],

                    ["type" => "rule_condition_combine", "aggregator" => "all", "value" => 0, "conditions" => [
                        ["type" => "rule_condition_product", "attribute" => "category_ids", "operator" => "==", "value" => 10]
                    ]]
                ]]];
            $this->data = $default_data;
        }else{
            if($data != ''){
                $rr = new RuleResolver();
                $this->data = $rr->toArray($data)[1];
            }else{
                $this->data = $default_data;
            }
        }
    }

    function getLevelsByString($string)
    {
        $tokens = explode("__", $string);
        $node = $tokens[1];
        $group = (int)$node;
        $subgroup = 0;
        if (\Str::contains($node, "--")) {
            $tokens = explode("--", $node);
            $group = (int)$tokens[0];
            $subgroup = (int)$tokens[1];
        }
        return compact("group", "subgroup");
    }

    function setNode($parentLevel, $childLevel)
    {
        $this->node = ($parentLevel == 1 AND $childLevel == 0) ? $parentLevel : $parentLevel . "--" . $childLevel;
    }

    function getNode()
    {
        return $this->node;
    }

    function getNodeByString($str)
    {
        $tokens = explode("__", $str);
        if (isset($tokens[1])) {
            return $tokens[1];
        }
        return "";
    }

    function renderType($type)
    {
        $tagName = $this->getTagName("type");
        $tagId = $this->getTagId("type");
        return \Form::hidden($tagName, $type, ["id" => $tagId]);
    }

    function renderTypeAttribute($attribute)
    {
        $tagName = $this->getTagName("attribute");
        $tagId = $this->getTagId("attribute");
        return \Form::hidden($tagName, $attribute, ["id" => $tagId]);
    }

    function wrapWithTrigger($html, $triggerLabel)
    {
        $tpl = <<<TPL
<span class="rule-param">
    <a href="javascript:void(0)" class="trigger">$triggerLabel</a>
    <span class="element">
        $html
    </span>
</span>
TPL;
        return $tpl;
    }

    function wrapWithTriggerToolbar($html, $triggerLabel, $row, $hasDelete = true, $forceHidden = false)
    {
        $toolbar = '';
        switch ($row['attribute']) {
            case 'category_ids':
            case 'collection_ids':
            case 'brand_ids':
            case 'sku':
            case 'supplier_ids':
            case "payment_ids":
            case "carrier_ids":
            case "state_ids":
            case "country_ids":
            case "trend_ids":
            case "nav_ids":
            case "section_ids":
            case "page_ids":
            case "pricerule_ids":
                $toolbar = '<a href="javascript:void(0)" class="rule-chooser-trigger"><img src="/themes/admin/images/icons/search.gif" alt="" class="v-middle rule-chooser-trigger" title="Apri selezione guidata"></a> <a href="javascript:void(0)" class="rule-param-apply"><img src="/themes/admin/images/icons/apply.gif" class="v-middle" alt="" title="Applica"></a>';
                break;

            default:
                $toolbar = '<a href="javascript:void(0)" class="rule-param-apply"><img src="/themes/admin/images/icons/apply.gif" class="v-middle" alt="" title="Applica"></a>';
                break;
        }

        $class = $forceHidden ? 'hidden' : '';

        $tpl = <<<TPL
<span class="rule-param $class">
    <a href="javascript:void(0)" class="trigger">$triggerLabel</a>
    <span class="element">
        $html
        $toolbar
    </span>
</span>
TPL;
        if($hasDelete)$tpl .= '<span class="rule-param"><a href="javascript:void(0)" class="rule-param-remove" title="Rimuovi"><img src="/themes/admin/images/icons/close.png" alt="" class="v-middle"></a></span>';
        return $tpl;
    }

    function wrapWithNewRule()
    {
        $options = $this->getContextOptions();
        $tagName = $this->getTagName("new_child");
        $tagId = $this->getTagId("new_child");
        $select = \Form::select($tagName, $options, "==", ["class" => "element-value-changer new_child", "id" => $tagId, "autocomplete" => "off"]);
        $tpl = <<<TPL
<li class="add-rule-param">
    <span class="rule-param">
        <a href="javascript:;" class="trigger"><img src="/themes/admin/images/icons/plus.png" /></a>
        <span class="element">
            $select
    </span>
</li>
TPL;
        return $tpl;
    }

    function renderAggregator($aggregator)
    {
        $options = $this->getAggregatorOptions();
        $label = $options[$aggregator];
        $tagName = $this->getTagName("aggregator");
        $tagId = $this->getTagId("aggregator");
        $select = \Form::select($tagName, $options, $aggregator, ["class" => "element-value-changer", "id" => $tagId, "autocomplete" => "off"]);
        return $this->wrapWithTrigger($select, $label);
    }

    function renderBoolean($boolean)
    {
        $options = $this->getBooleanOptions();
        $label = $options[$boolean];
        $tagName = $this->getTagName("value");
        $tagId = $this->getTagId("value");
        $select = \Form::select($tagName, $options, $boolean, ["class" => "element-value-changer", "id" => $tagId, "autocomplete" => "off"]);
        return $this->wrapWithTrigger($select, $label);
    }

    function renderFoundNotFound($boolean)
    {
        $options = $this->getFoundNotFoundOptions();
        $label = $options[$boolean];
        $tagName = $this->getTagName("value");
        $tagId = $this->getTagId("value");
        $select = \Form::select($tagName, $options, $boolean, ["class" => "element-value-changer", "id" => $tagId, "autocomplete" => "off"]);
        return $this->wrapWithTrigger($select, $label);
    }

    private function getTagName($target)
    {
        return "{$this->fieldName}[conditions][{$this->node}][$target]";
    }

    private function getTagId($target)
    {
        return "conditions__{$this->node}__$target";
    }

    private function getAggregatorOptions()
    {
        return ["all" => "TUTTE", "any" => "ALMENO UNA DI"];
    }

    private function getBooleanOptions()
    {
        return [1 => "VERE", 0 => "FALSE"];
    }

    private function getFoundNotFoundOptions()
    {
        return [1 => "VIENE TROVATO", 0 => "NON VIENE TROVATO"];
    }

    private function getOperators($attribute)
    {
        $operators = [];
        switch ($attribute) {
            case "category_ids":
            case "brand_ids":
            case "collection_ids":
            case "supplier_ids":
            case "attribute_ids":
            case "sku":
            case "payment_ids":
            case "carrier_ids":
            case "state_ids":
            case "country_ids":
            case "postcode":
            case "region":
            case "trend_ids":
            case "nav_ids":
            case "section_ids":
            case "page_ids":
            case "pricerule_ids":
                $operators = \Mainframe::selectRulesOperators(null, ['>', '>=', '<', '<=']);
                break;

            case "price":
            case "price_wt":
            case "qty":
            case "amount":
            case "base_subtotal":
            case "base_subtotal_wt":
            case "total_qty":
            case "total_row":
            case "weight":
            case "quote_item_price":
            case "quote_item_listprice":
            case "quote_item_qty":
            case "quote_item_row_total":
            case "order_cnt":
            case "order_amount":
                $operators = \Mainframe::selectRulesOperators(['==', '!=', '>', '>=', '<', '<=']);
                break;

            case "flag_new":
            case "flag_outlet":
            case "flag_featured":
            case "flag_fp":
            case "flag_promotion":
            case "site_scope":
                $operators = \Mainframe::selectRulesOperators(['==', '!=']);
                break;

            default:
                $operators = \Mainframe::selectRulesOperators(['==', '!=','+=','-=']);
                break;
        }
        //\Utils::log($operators, "getOperators $attribute");
        return $operators;
    }


    function getType(){
        return $this->type;
    }

    function getBasetype(){
        return $this->basetype;
    }

    function getFulltype(){
        return $this->fulltype;
    }

    function setAllTypes($str){
        $tokens = explode("|",$str);
        $this->type = $tokens[1];
        $this->basetype = $tokens[0];
        $this->fulltype = $str;
    }


    private function getProductOptions()
    {
        $lang = \Core::getLang();
        $options = [];
        $sub_options = [];
        $sub_options["product|category_ids"] = "Categoria";
        $sub_options["product|brand_ids"] = "Brand";
        $sub_options["product|collection_ids"] = "Collezione";
        $sub_options["product|sku"] = "Prodotto specifico";
        $sub_options["product|trend_ids"] = "Gruppo personalizzato";
        //$options["attribute_ids"]  = "Attributo";
        $sub_options["product|supplier_ids"] = "Fornitore";
        $sub_options["product|price"] = "Prezzo prodotto base";
        $sub_options["product|price_wt"] = "Prezzo prodotto finale";
        $options["Caratteristiche prodotti"] = $sub_options;
        $sub_options = [];
        $sub_options["product|flag_new"] = "Novità";
        $sub_options["product|flag_outlet"] = "Outlet";
        $sub_options["product|flag_featured"] = "Featured";
        $sub_options["product|flag_fp"] = "Fuori produzione";
        $sub_options["product|flag_promotion"] = "In promozione";
        $options["Flag/status prodotti"] = $sub_options;

        $sub_options = [];
        $rows = \Attribute::whereIn("frontend_input", ["select", "multiselect"])->rows($lang)->orderBy("name")->get();
        foreach ($rows as $row) {
            $sub_options["attribute|".$row->code] = $row->name;
        }
        $options["Attributo prodotti"] = $sub_options;
        return $options;
    }

    private function getSiteOptions()
    {
        $lang = \Core::getLang();
        $options = [];
        $sub_options = [];
        $sub_options["site|category_ids"] = "Categoria";
        $sub_options["site|brand_ids"] = "Brand";
        $sub_options["site|collection_ids"] = "Collezione";
        $sub_options["site|trend_ids"] = "Gruppo personalizzato";
        $sub_options["site|sku"] = "Prodotto specifico";
        $sub_options["site|supplier_ids"] = "Fornitore";
        $sub_options["site|flag_new"] = "Novità";
        $sub_options["site|flag_outlet"] = "Outlet";
        $sub_options["site|flag_featured"] = "Featured";
        $sub_options["site|flag_fp"] = "Fuori produzione";
        $sub_options["site|flag_promotion"] = "In promozione";
        $options["Caratteristiche prodotti"] = $sub_options;
        /*$sub_options = [];
        $sub_options["site|flag_new"] = "Novità";
        $sub_options["site|flag_outlet"] = "Outlet";
        $sub_options["site|flag_featured"] = "Featured";
        $sub_options["site|flag_fp"] = "Fuori produzione";
        $options["Flag/status prodotti"] = $sub_options;*/

        $sub_options = [];
        $sub_options["site|nav_ids"] = "Navigazione principale";
        $sub_options["site|section_ids"] = "Sezione";
        $sub_options["site|page_ids"] = "Pagina";
        $sub_options["site|pricerule_ids"] = "Promozione";
        $options["Struttura sito"] = $sub_options;

        $sub_options = [];
        $sub_options["site|site_scope"] = "Contesto frontend";
        $sub_options["site|customer_default"] = "Utente default (no guest|no registrato)";
        $sub_options["site|customer_guest"] = "Utente guest";
        $sub_options["site|customer_registered"] = "Utente registrato";
        $sub_options["site|customer_group"] = "Gruppo Utente";
        $options["Status"] = $sub_options;


        $sub_options = [];
        $rows = \Attribute::whereIn("frontend_input", ["select", "multiselect"])->rows($lang)->orderBy("name")->get();
        foreach ($rows as $row) {
            $sub_options["filter|".$row->code] = $row->name;
        }
        $options["Attributo prodotti"] = $sub_options;
        return $options;
    }



    private function getGeneralOptions()
    {
        $lang = \Core::getLang();
        $options = [];
        $sub_options = [];
        $sub_options["general|category_ids"] = "Categoria";
        $sub_options["general|brand_ids"] = "Brand";
        $sub_options["general|collection_ids"] = "Collezione";
        $sub_options["general|trend_ids"] = "Gruppo personalizzato";
        $sub_options["general|sku"] = "Prodotto specifico";
        $sub_options["general|supplier_ids"] = "Fornitore";
        $options["Caratteristiche prodotti"] = $sub_options;
        $sub_options = [];
        $sub_options["general|flag_new"] = "Novità";
        $sub_options["general|flag_outlet"] = "Outlet";
        $sub_options["general|flag_featured"] = "Featured";
        $sub_options["general|flag_fp"] = "Fuori produzione";
        $sub_options["general|flag_promotion"] = "Fuori produzione";
        $options["Flag/status prodotti"] = $sub_options;

        $sub_options = [];
        $rows = \Attribute::whereIn("frontend_input", ["select", "multiselect"])->rows($lang)->orderBy("name")->get();
        foreach ($rows as $row) {
            $sub_options["attr|".$row->code] = $row->name;
        }
        $options["Attributo prodotti"] = $sub_options;
        return $options;
    }



    private function getContextOptions()
    {
        $lang = \Core::getLang();
        $logical_type = "";

        switch($this->fulltype){
            case 'cart|rule_condition_product_found':
            case 'cart|rule_condition_product_subselect':
            case 'product|rule_condition_product':
            case 'attribute|rule_condition_product':
                $logical_type = 'product|rule_condition_combine';
                break;

            case 'cart|rule_condition_cart':
                $logical_type = 'cart|rule_condition_combine';
                if(in_array($this->attribute,["quote_item_price","quote_item_listprice","quote_item_qty","quote_item_row_total"])){
                    $logical_type = 'product|rule_condition_combine';
                    $this->fulltype = 'cart|rule_condition_product_subselect';
                }
                break;

            case 'customer|rule_condition_cart':
                $logical_type = 'cart|rule_condition_combine';
                break;

            default:
                $logical_type = $this->fulltype;
                break;
        }

        $options = ["" => "Prego scegliere la condizione da aggiungere...", $logical_type => "Gruppo/Blocco di condizioni"];
        switch ($this->fulltype) {

            case 'cart|rule_condition_product_found':
            case 'cart|rule_condition_product_subselect':
                $sub_options = [];
                $sub_options["cart|quote_item_price"] = "Prezzo finale articolo nel carrello";
                $sub_options["cart|quote_item_listprice"] = "Prezzo di listino articolo nel carrello";
                $sub_options["cart|quote_item_qty"] = "Quantità articolo nel carrello";
                $sub_options["cart|quote_item_row_total"] = "Numero articoli differenti(righe) nel carrello che soddisfano le condizioni precedenti";
                $options["Caratteristiche articolo carrello"] = $sub_options;

                $product_options = $this->getProductOptions();
                $options = array_merge($options, $product_options);

                break;

            case 'cart|rule_condition_combine':
            case 'cart|rule_condition_cart':
            case 'customer|rule_condition_cart':
                $options["cart|rule_condition_product_found"] = "Presenza di uno o più prodotti in base a condizioni";
                $options["cart|rule_condition_product_subselect"] = "Sotto-selezione di prodotti per quantità o ammontare";
                $sub_options = [];
                $sub_options["cart|base_subtotal"] = "Subtotale (tasse escluse)";
                $sub_options["cart|base_subtotal_wt"] = "Subtotale (tasse incluse)";
                $sub_options["cart|weight"] = "Peso totale";
                $sub_options["cart|total_qty"] = "Quantità totale articoli (somma)";
                $sub_options["cart|total_row"] = "Totale articoli differenti (righe)";
                $sub_options["cart|payment_ids"] = "Metodo di pagamento";
                $sub_options["cart|carrier_ids"] = "Spedizione - Corriere";
                $sub_options["cart|postcode"] = "Spedizione - CAP/ZIP/Postcode";
                $sub_options["cart|region"] = "Spedizione - Comuna/Città";
                $sub_options["cart|state_ids"] = "Spedizione - Provincia/Stato";
                $sub_options["cart|country_ids"] = "Spedizione - Nazione";
                $options["Caratteristiche ordine/carrello"] = $sub_options;

                $sub_options = [];
                //$sub_options["customer|user_ids"] = "Utente specifico";
                //$sub_options["customer|group_ids"] = "Gruppo utente";
                $sub_options["customer|order_cnt"] = "Cliente - Numero ordini confermati";
                $sub_options["customer|order_amount"] = "Cliente - Totale importo ordini confermati";
                //$sub_options["customer|country_ids"] = "Nazione di provenienza";
                $options["Dettagli cliente/utente (richiede il login)"] = $sub_options;
                break;

            case 'product|rule_condition_product':
            case 'product|rule_condition_combine':
            case 'attribute|rule_condition_product':
                $product_options = $this->getProductOptions();
                $options = array_merge($options, $product_options);
                break;


            case 'site|rule_condition_scope':
            case 'site|rule_condition_combine':
            case 'filter|rule_condition_scope':
                $product_options = $this->getSiteOptions();
                $options = array_merge($options, $product_options);
                break;

            case 'general|rule_condition_self':
            case 'general|rule_condition_combine':
            case 'attr|rule_condition_self':
                $product_options = $this->getGeneralOptions();
                $options = array_merge($options, $product_options);
                break;


        }
        return $options;
    }

    private function renderOperators($row)
    {
        $options = $this->getOperators($row['attribute']);
        $label = $options[$row['operator']];
        $value = $row['operator'];
        $tagName = $this->getTagName("operator");
        $tagId = $this->getTagId("operator");
        $select = \Form::select($tagName, $options, $value, ["class" => "element-value-changer", "id" => $tagId, "autocomplete" => "off"]);
        return $this->wrapWithTrigger($select, $label);
    }


    private function renderAttributes($row)
    {
        $options = ["qty" => \Str::upper("la quantità totale"),"amount" => \Str::upper("l'importo totale")];
        $label = $options[$row['attribute']];
        $value = $row['attribute'];
        $tagName = $this->getTagName("attribute");
        $tagId = $this->getTagId("attribute");
        $select = \Form::select($tagName, $options, $value, ["class" => "element-value-changer", "id" => $tagId, "autocomplete" => "off"]);
        return $this->wrapWithTrigger($select, $label);
    }

    function getLabel($row)
    {
        $lang = \Core::getLang();
        $value = $row['value'];
        $type = $row['attribute'];
        $labels = [];
        switch ($type) {
            case "category_ids":
                $ids = explode(",", $value);
                $rows = \Category::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;
            case "brand_ids":
                $ids = explode(",", $value);
                $rows = \Brand::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;
            case "collection_ids":
                $ids = explode(",", $value);
                $rows = \Collection::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;
            case "supplier_ids":
                $ids = explode(",", $value);
                $rows = \Supplier::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;
            case "sku":
                $ids = explode(",", $value);
                $rows = \Product::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = "({$row->sku}) - $row->name";
                }
                break;

            case "country_ids":
                $ids = explode(",", $value);
                $rows = \Country::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = "{$row->name} ($row->iso_code)";
                }
                break;

            case "state_ids":
                $ids = explode(",", $value);
                $rows = \State::whereIn("id", $ids)->get();
                foreach ($rows as $row) {
                    $labels[] = "{$row->name} ($row->iso_code)";
                }
                break;

            case "carrier_ids":
                $ids = explode(",", $value);
                $rows = \Carrier::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            case "payment_ids":
                $ids = explode(",", $value);
                $rows = \Payment::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            case "trend_ids":
                $ids = explode(",", $value);
                $rows = \Trend::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            case "nav_ids":
                $ids = explode(",", $value);
                $rows = \Nav::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            case "section_ids":
                $ids = explode(",", $value);
                $rows = \Section::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            case "page_ids":
                $ids = explode(",", $value);
                $rows = \Page::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            case "pricerule_ids":
                $ids = explode(",", $value);
                $rows = \PriceRule::whereIn("id", $ids)->rows($lang)->get();
                foreach ($rows as $row) {
                    $labels[] = $row->name;
                }
                break;

            default:
                //assuming it's a attribute
                if ($value != '') {
                    $obj = \AttributeOption::getObj($value);
                    $obj = $obj->getPresenter();
                    $labels[] = $obj->completename;
                }

                break;
        }
        $l = implode(", ", $labels);
        return ($l == "") ? "..." : $l;
    }

    private function renderInput($row , $hasDelete = true)
    {
        $tagName = $this->getTagName("value");
        $tagId = $this->getTagId("value");
        //$label = $row['label'];
        $label = $row['value'];
        $toolbar = '';
        $hasChooser = false;

        $style = '';
        $forceHidden = false;
        if(isset($row['operator']) AND ($row['operator'] == '+=' OR $row['operator'] == '-=')){
            $style = 'isHidden';
            $forceHidden = true;
        }

        switch ($row["attribute"]) {
            case "category_ids":
            case "brand_ids":
            case "collection_ids":
            case "supplier_ids":
            case "sku":
            case "payment_ids":
            case "carrier_ids":
            case "state_ids":
            case "country_ids":
            case "trend_ids":
            case "nav_ids":
            case "section_ids":
            case "page_ids":
            case "pricerule_ids":
                $label = $this->getLabel($row);
                $input = \Form::input("text", $tagName, $row['value'], ["class" => "element-value-changer input-text $style", "id" => $tagId, "autocomplete" => "off"]);
                $hasChooser = true;
                break;


            case "qty":
            case "amount":
            case "price":
            case "price_wt":
            case "base_subtotal":
            case "base_subtotal_wt":
            case "total_qty":
            case "total_row":
            case "weight":
            case "quote_item_price":
            case "quote_item_listprice":
            case "quote_item_qty":
            case "quote_item_row_total":
            case "order_cnt":
            case "order_amount":
                $input = \Form::input("number", $tagName, $row['value'], ["class" => "element-value-changer input-text align-right $style", "id" => $tagId, "autocomplete" => "off"]);
                //$hasDelete = false;
                break;

            case "postcode":
            case "region":
                $input = \Form::input("text", $tagName, $row['value'], ["class" => "element-value-changer input-text $style", "id" => $tagId, "autocomplete" => "off"]);
                break;

            case "flag_new":
            case "flag_outlet":
            case "flag_featured":
            case "flag_fp":
            case "flag_promotion":
            case "customer_guest":
            case "customer_registered":
            case "customer_default":
                $input = \Form::select($tagName, [1 => 'VERO', 0 => 'FALSO'], $row['value'], ["class" => "element-value-changer input-select $style", "id" => $tagId, "autocomplete" => "off"]);
                break;

            case "site_scope":
                $input = \Form::select($tagName, Mainframe::selectFrontendScopes(), $row['value'], ["class" => "element-value-changer input-select $style", "id" => $tagId, "autocomplete" => "off"]);
                break;

            case "customer_group":
                $input = \Form::select($tagName, Mainframe::selectCustomersGroups(), $row['value'], ["class" => "element-value-changer input-select $style", "id" => $tagId, "autocomplete" => "off"]);
                break;

            default:
                $label = $this->getLabel($row);
                $obj = \Attribute::where("code", $row["attribute"])->first();
                //$obj = \AttributeOption::getObj($row['value']);
                //$att_obj = \Attribute::getObj($obj->attribute_id);
                $input = \Form::select($tagName, \Mainframe::selectAttributeOptions($obj->id), $row['value'], ["class" => "element-value-changer input-select $style", "id" => $tagId, "autocomplete" => "off"]);

                break;
        }

        $label = ($label == '') ? '...' : $label;
        $html = $this->wrapWithTriggerToolbar($input, $label, $row, $hasDelete, $forceHidden);

        if ($hasChooser) $html .= "<div class=\"rule-chooser\" type=\"{$row["attribute"]}\"></div>";

        return $html;
    }

    function renderRow($row)
    {
        $hidden = $this->renderType($row['type']);

        switch ($row['type']) {
            case 'rule_condition_combine':
            case 'rule_condition_product_combine':
            case 'rule_condition_cart_combine':
            case 'cart|rule_condition_combine':
            case 'product|rule_condition_combine':
            case 'site|rule_condition_combine':
            case 'general|rule_condition_combine':
                $param1 = $this->renderAggregator($row['aggregator']);
                $param2 = $this->renderBoolean($row['value']);
                $tpl = "$hidden Se $param1 queste condizioni sono $param2:";
                break;

            case 'product|rule_condition_product':
            case 'attribute|rule_condition_product':
            case 'filter|rule_condition_scope':
            case 'site|rule_condition_scope':
            case 'general|rule_condition_self':
            case 'attr|rule_condition_self':
                $typeAttribute = $this->renderTypeAttribute($row['attribute']);
                $operators = $this->renderOperators($row);
                $options = $this->getContextOptions();
                $options_flattened = $this->searchItemsByKey($options, $this->basetype."|".$row['attribute']);
                $label = $options_flattened[0];
                $input = $this->renderInput($row);
                $tpl = "<li>$hidden $typeAttribute $label $operators $input</li>";
                break;

            case 'cart|rule_condition_cart':
            case 'customer|rule_condition_cart':
                $typeAttribute = $this->renderTypeAttribute($row['attribute']);
                $operators = $this->renderOperators($row);
                $options = $this->getContextOptions();
                $options_flattened = $this->searchItemsByKey($options, $this->basetype."|".$row['attribute']);
                $label = $options_flattened[0];
                $input = $this->renderInput($row);
                $tpl = "<li>$hidden $typeAttribute $label $operators $input</li>";
                break;

            case 'cart|rule_condition_product_found':
                $aggregator = $this->renderAggregator($row['aggregator']);
                $found = $this->renderFoundNotFound($row['value']);
                $tpl = "$hidden Se $found un articolo nel carrello per il quale $aggregator queste condizioni sono <b>vere</b>:";
                break;

            case 'cart|rule_condition_product_subselect':
                $aggregator = $this->renderAggregator($row['aggregator']);
                $operators = $this->renderOperators($row);
                $attributes = $this->renderAttributes($row);
                $input = $this->renderInput($row,false);
                $tpl = "$hidden Se $attributes $operators $input per una sotto-selezione di articoli nel carrello che soddisfano $aggregator queste condizioni:";
                break;
        }


        return $tpl;
    }

    function searchItemsByKey($array, $key)
    {
        $results = array();

        if (is_array($array)) {
            if (isset($array[$key])) {
                $results[] = $array[$key];
            }

            foreach ($array as $sub_array) {
                $a = $this->searchItemsByKey($sub_array, $key);
                if (!empty($a)) $results = array_merge($results, $a);

            }

        }

        return $results;
    }

    function renderRule($rule, $parentLevel = 0, $node = '')
    {
        $this->setAllTypes($rule["type"]);
        $this->attribute = (isset($rule["attribute"])) ? $rule["attribute"] : null;
        if (isset($rule["conditions"])) {
            $parentLevel++;

            $this->node = ($node == '') ? $parentLevel : $node;
            $current_node = $this->node;
            $temp_node = $this->node;

            $rows = $rule["conditions"];

            $html = "<li>" . $this->renderRow($rule);
            if ($parentLevel > 1) {
                $html .= '<span class="rule-param"><a href="javascript:void(0)" class="rule-param-remove" title="Rimuovi"><img src="/themes/admin/images/icons/close.png" alt="" class="v-middle"></a></span>';
            }
            $children_html = '';
            $ul = Element::ul()->addClass("rule-param-children")->setAttribute("conditions__{$this->node}__children");
            $childrenCount = 0;
            if (!empty($rows)) {
                foreach ($rows as $row) {
                    $childrenCount++;
                    $current_node = $temp_node . "--" . $childrenCount;
                    $children_html .= $this->renderRule($row, $parentLevel, $current_node);
                }
            }
            $childrenCount++;

            $this->node = $temp_node . "--" . $childrenCount;
            $this->setAllTypes($rule["type"]);
            $this->attribute = (isset($rule["attribute"])) ? $rule["attribute"] : null;
            $children_html .= $this->wrapWithNewRule();
            $ul->setValue($children_html);
            $html .= $ul . "</li>";

        } else {
            $this->node = $node;
            $html = $this->renderRow($rule);
        }
        //\Utils::log($html,"HTML rule with node $this->node");
        return $html;
    }


    function render()
    {
        $html = "<ul>" . $this->renderRule($this->data) . "</ul>";
        $container = Element::div($html)->addClass("builder")->setAttribute("controller", $this->controller)->setAttribute("fieldName", $this->fieldName);
        return $container;
    }

    function count(){
        if(isset($this->data['conditions'])){
            return count($this->data['conditions']);
        }
        return 0;
    }


}

