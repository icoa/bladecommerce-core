<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 30/09/14
 * Time: 17.20
 */
class ModuleHelper
{
    static function getModuleTypes()
    {
        $html = '<table class="table table-bordered table-gradient"><thead><tr><th colspan="2">Scegli la tipologia del modulo da creare:</th></tr></thead><tbody>';
        $rows = \DB::table("module_types")->where("active", 1)->orderBy("position")->get();
        foreach ($rows as $row) {
            $html .= "<tr>
            <td width='50' class='align-center'>
            <input type='radio' name='mod_type' value='$row->module' id='mod_{$row->id}' />
            </td>
            <td>
            <label for='mod_{$row->id}' class='pointer'><b>$row->name</b><br>
            <em>$row->sdesc</em></label>
            </td>
            </tr>";
        }
        $html .= '</table>';
        return $html;
    }

    static function typename($module_type){
        $obj = \DB::table("module_types")->where("module", $module_type)->orderBy("position")->first();
        return $obj->name;
    }


    static function setParams($params){
        if(!is_array($params)){
            return null;
        }
        return serialize((object)$params);
    }

    static function getParams($str){
        if($str == null OR $str == ''){
            return false;
        }
        return unserialize($str);
    }

    static function getFormParams($str){
        $params = self::getParams($str);
        if(!$params){
            return null;
        }
        $data = [];
        foreach($params as $key => $value){
            $data["params[{$key}]"] = $value;
        }
        return $data;
    }
}