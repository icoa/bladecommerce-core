<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 24/10/14
 * Time: 12.48
 */

namespace Core;

use Carbon\Carbon;
use HtmlObject\Element;
use HtmlObject\Image;
use HtmlObject\Traits\Tag;
use Underscore\Types\Arrays;

class Link
{

    protected $type;
    protected $id;
    protected $lang;
    protected $mode;
    protected $slugs;
    protected $links;
    protected $letters;
    protected $modifiers;
    protected $filters;
    protected $isSimple;
    protected $isCrafted;
    protected $scheme = false;
    protected $hasCatalog = false;
    protected $page = 1;
    protected $append = null;

    /**
     * @param $type
     * @param $id
     * @param string $lang
     * @param string $mode
     * @return $this
     */
    function create($type, $id, $lang = 'default', $mode = '_self')
    {
        $this->scheme = (!$this->scheme) ? \Cfg::get('LINK_URL_MODE') : $this->scheme;

        $this->type = $type;
        $this->id = $id;
        $this->lang = \Core::getLang($lang);
        $this->mode = $mode;
        $this->links = [];
        $this->slugs = [];
        $this->letters = [];
        $this->modifiers = [];
        $this->filters = [];
        $this->isSimple = false;
        $this->append = null;
        $cache = array_values($this->cache());
        //\Utils::log($cache,"CACHE");
        if ($type == 'nav' AND in_array($id, $cache)) {
            $type = 'base';
            $this->type = $type;
        }
        if ($type == 'home') {
            $type = 'base';
            $this->type = $type;
            $id = 1;
            $this->id = 1;
        }
        if ($type == 'search') {
            $this->addLink('list', 13);
            $this->addFilter('q', $id);
        } else {
            $this->addLink($type, $id);
        }

        $this->isCrafted = false;
        //\Utils::log($this,"LINK");
        //\Cache::forget('link_attribute');
        return $this;
    }


    function cache()
    {
        $data = \Cache::rememberForever('shortcut_nav', function () {
            $rows = \Nav::where('shortcut', '<>', '')->where('navtype_id', 1)->select(['id', 'shortcut'])->orderBy('id')->get();
            $a = [];
            foreach ($rows as $row) {
                $a[$row->shortcut] = $row->id;
            }
            return $a;
        });
        return $data;
    }

    function cacheNavSchema()
    {
        $data = \Cache::rememberForever('link_nav_schema', function () {
            $rows = \Nav::where('navtype_id', '>', 1)->select(['id', 'navtype_id'])->orderBy('id')->get();
            $a = [];
            foreach ($rows as $row) {
                $a[$row->id] = $row->navtype_id;
            }
            return $a;
        });
        return $data;
    }


    function fresh()
    {

        //$this->links = [];
        $this->slugs = [];
        $this->letters = [];
        $this->isSimple = false;
        $this->isCrafted = false;
        $this->append = null;
        return $this;
    }

    function setLang($lang)
    {
        $this->lang = $lang;
        return $this;
    }

    function setScheme($type)
    {
        $this->scheme = $type;
        return $this;
    }

    function setAppend($append)
    {
        $this->append = $append;
        return $this;
    }

    function setLink($type, $id)
    {
        $this->links[$type] = $id;
    }

    function setOpenMode($mode)
    {
        $this->mode = $mode;
        return $this;
    }

    function absolute()
    {
        $this->scheme = 'absolute';
        return $this;
    }

    function full($url = '')
    {
        if ($url[0] == '/') {
            return \Site::root() . $url;
        }
        return $url;
    }

    function relative()
    {
        $this->scheme = 'relative';
        return $this;
    }

    function to($type, $id, $lang = 'default', $mode = '_self')
    {
        $this->create($type, $id, $lang, $mode);
        return $this->getLink();
    }

    function a($type, $id, $lang = 'default', $mode = '_self')
    {
        $this->create($type, $id, $lang, $mode);
        return $this->getAnchor();
    }

    function shortcut($shortcut, $lang = 'default', $mode = '_self')
    {
        $data = $this->cache();
        if ($data AND isset($data[$shortcut])) {
            return $this->to("base", $data[$shortcut], $lang, $mode);
        }
        return null;
    }

    function shortcutNavObj($shortcut)
    {
        $data = $this->cache();
        if ($data AND isset($data[$shortcut])) {
            return \Nav::getPublicObj($data[$shortcut]);
        }
        return null;
    }

    function shortcutNavId($shortcut)
    {
        $data = $this->cache();
        if ($data AND isset($data[$shortcut])) {
            return ($data[$shortcut]);
        }
        return 0;
    }

    function size()
    {
        return count($this->links) + count($this->filters);
    }

    function add($what, $type, $id)
    {
        switch ($what) {
            case 'modifier':
                $this->addModifier($type, $id);
                break;
            case 'filter':
                $this->addFilter($type, $id);
                break;
            case 'search':
                $this->addFilter('q', $id);
                break;
        }
    }

    function addModifier($type, $id)
    {
        if (!is_array($this->slugs)) {
            return $this->create($type, $id);
        }
        $this->addLink($type, $id);
        return $this;
    }

    function setModifier($type, $id)
    {
        $this->addLink($type, $id, true);
        return $this;
    }

    function removeDefault()
    {
        //\Utils::log($this,"REMOVE DEFAULT");
        if (isset($this->links['nav']) AND $this->links['nav'] == 13) {
            unset($this->links['nav']);
        }
        if (isset($this->links['list']) AND $this->links['list'] == 13) {
            unset($this->links['list']);
        }
        return $this;
    }

    private function getRealType($type, $id)
    {
        if ($type != 'nav') {
            return $type;
        }
        try {
            $cacheNavSchema = $this->cacheNavSchema();
            //\Utils::log($cacheNavSchema,"cacheNavSchema for [$type,$id]");
            if (isset($cacheNavSchema[$id])) {
                switch ($cacheNavSchema[$id]) {
                    case 2:
                        $type = 'list';
                        break;
                    case 3:
                        $type = 'status';
                        break;
                    case 4:
                        $type = 'qty';
                        break;
                    case 5:
                        $type = 'pricerange';
                        break;
                }
            }
            return $type;
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), __METHOD__);
            return $type;
        }
    }

    private function addLink($type, $id, $override = false)
    {
        if (!$override AND $type != 'nav') {
            $type = $this->getRealType($type, $id);
        }
        $insert = !isset($this->links[$type]);
        if (!$insert AND $override) $insert = true;
        //\Utils::log("$type, $id, $override",__METHOD__);


        if ($id == null OR $id == false) {
            try {
                unset($this->links[$type]);
            } catch (\Exception $e) {

            }
        }

        if ($insert) {
            //\Utils::log("Setting type: $type | id: $id","addLink");

            switch ($type) {
                case 'section':
                case 'page':
                case 'base':
                case 'base_nav':
                case 'nav_base':
                case 'promo':
                    $this->isSimple = true;
                    $this->type = $type;
                    $this->id = $id;
                    break;

                case 'list':
                case 'status':
                case 'qty':
                case 'pricerange':
                case 'nav':
                    //$type = 'nav';
                    if ($id == 13) {
                        $this->hasCatalog = true;
                    }
                    if (isset($this->links['list'])) {
                        $this->removeDefault();
                    }
                    break;

                default:

                    if ($this->hasCatalog and $id != null) {
                        $this->removeDefault();
                    }

                    break;
            }
            $this->links[$type] = $id;
        }
    }

    function addFilter($type, $id)
    {
        if (!is_array($this->links)) {
            $this->addModifier('list', 13);
        }
        $this->filters[$type] = $id;
        return $this;
    }

    function setFilter($type, $id)
    {
        $this->filters[$type] = $id;
        return $this;
    }

    function removeFilter($type)
    {
        unset($this->filters[$type]);
        return $this;
    }

    private function getNavAliases()
    {
        return [
            'list',
            'status',
            'qty',
            'pricerange',
            'nav',
        ];
    }

    private function craft()
    {
        if ($this->isCrafted)
            return;

        //audit($this->type,"LINK TYPE");
        //standard link
        if ($this->isSimple) {
            switch ($this->type) {
                case 'section':
                    $slug = \Section::getSlug($this->id, $this->lang);
                    $this->addSlug($slug);
                    break;
                case 'page':
                    $slug = \Page::getSlug($this->id, $this->lang);
                    $this->addSlug($slug);
                    break;
                case 'base':
                    $slug = \Nav::getSlug($this->id, $this->lang);
                    $this->addSlug($slug);
                    break;
                case 'promo':
                    $slug = \PriceRule::getSlug($this->id, $this->lang);
                    $this->addSlug($slug);
                    break;
            }
        } else {
            $this->static_markers = $this->getStaticMarkers();
            $this->attribute_markers = $this->getAttributeMarkers();
            $this->nav_markers = $this->getNavMarkers();
            $nav_aliases = $this->getNavAliases();

            $keys = is_array($this->links) ? array_keys($this->links) : [];

            //\Utils::log($nav_aliases,"NAV ALIASES");
            //\Utils::log($this->links,"LINKS");

            foreach ($keys as $key) {
                if (in_array($key, $nav_aliases)) {
                    $id = $this->links[$key];
                    //\Utils::log($this->links[$key],"FETCHING");
                    foreach ($this->nav_markers as $nav_id => $letter) {
                        if ($id == $nav_id) {
                            $obj = \Nav::getPublicObj($id, $this->lang);
                            if ($obj) {
                                $slug = $obj->slug;
                                $id = '';
                                if ($slug) {
                                    $this->addPartial($slug, $letter, $id);
                                }
                            }

                        }
                    }
                }
            }

            foreach ($this->static_markers as $type => $letter) {
                if (isset($this->links[$type])) {
                    $id = $this->links[$type];
                    $model = ucfirst($type);
                    $slug = $model::getSlug($id, $this->lang);
                    if ($slug) {
                        $this->addPartial($slug, $letter, $id);
                    }
                }
            }

            foreach ($this->attribute_markers as $type => $schema) {
                if (isset($this->links[$type])) {
                    $id = $this->links[$type];
                    $slug = \AttributeOption::getUslug($id, $this->lang);

                    if ($slug) {
                        try {
                            $letter = $schema[$id];
                            $id = '';
                            $this->addPartial($slug, $letter, $id);
                        } catch (\Exception $e) {

                        }
                    } else {
                        $attribute_id = \Attribute::where('code', $type)->pluck('id');
                        $id = \AttributeOption::where('nav_id', $id)->where('attribute_id', $attribute_id)->pluck('id');
                        $slug = \AttributeOption::getUslug($id, $this->lang);
                        try {
                            $letter = $schema[$id];
                            $id = '';
                            $this->addPartial($slug, $letter, $id);
                        } catch (\Exception $e) {

                        }
                    }
                }
            }
        }

        $this->isCrafted = true;
        /*\Utils::log($this->attribute_markers,"ATTRIBUTE");
        \Utils::log($this->links,"LINKS");*/
    }

    function getLink($force = false)
    {
        if ($force) {
            $this->fresh();
        }
        $this->craft();
        $partials = $this->slugs;
        $ids = "";
        foreach ($this->letters as $letter => $number) {
            $ids .= $letter . $number;
        }
        if ($ids) {
            $partials[] = $ids;
        }
        //\Utils::log($partials, "PARTIALS");
        $link = implode("-", $partials);
        if ($this->page > 1) {
            $link .= '_' . $this->page;
        }
        if ($link != '') {
            if (!$this->isSimple) {
                $link .= '.htm';

            } else {
                if ($this->type == 'page') {
                    $link .= '.html';
                }
            }
            if (count($this->filters)) {
                $filters = http_build_query($this->filters);
                if ($filters != '') {
                    $link .= "?$filters";
                }
            }
            if (!is_null($this->append)) {
                $link .= $this->append;
            }
        }


        //apply language
        if (\Cfg::get("DEFAULT_LANGUAGE") != $this->lang) {
            $link = $this->lang . "/" . $link;
        }


        //apply prefix or domain
        switch ($this->scheme) {
            case 'relative':
                $link = "/" . $link;
                break;
            default:
            case 'absolute':
                $link = \Site::root() . "/" . $link;
                break;
        }

        $this->scheme = \Cfg::get('LINK_URL_MODE');

        return $link;
    }

    private function addPartial($slug, $letter, $number)
    {
        $this->addSlug($slug);
        $this->addLetter($letter, $number);
    }

    private function addSlug($slug)
    {
        $this->slugs[] = $slug;
    }

    private function addLetter($letter, $number)
    {
        $this->letters[$letter] = $number;
    }

    function getAnchor()
    {
        $link = $this->getLink();
        //\Utils::log($link, "getAnchor");
        $anchor = Element::a();
        $anchor->setAttribute("href", $link);
        if ($this->mode != '_self') {
            $anchor->setAttribute("target", $this->mode);
        }
        return $anchor;
    }

    function isStatic($type)
    {
        $array = array_keys($this->static_markers);
        return in_array($type, $array);
    }

    private function getStaticMarkers()
    {
        return [
            'category' => 'C',
            'brand' => 'B',
            'collection' => 'N',
            'trend' => 'T',
            'product' => 'P',
        ];
    }

    private function getNavMarkers()
    {
        $data = \Cache::rememberForever('link_nav', function () {
            $rows = \Nav::where("navtype_id", ">", 1)->select(['id', 'ucode'])->get();
            $a = [];
            foreach ($rows as $row) {
                $a[$row->id] = $row->ucode;
            }
            return $a;
        });
        return $data;
    }

    private function getAttributeMarkers()
    {
        //\Cache::forget('link_attribute');
        $data = \Cache::rememberForever('link_attribute', function () {
            $rows = \Attribute::where("is_nav", 1)->select(['nav_code', 'id', 'code'])->orderBy('position')->get();
            $data = [];
            $attribute_codes = [];
            foreach ($rows as $row) {
                $letter = $row->nav_code;
                $attribute_id = $row->id;
                $attribute_codes[$row->code] = [];
                $options = \AttributeOption::where("attribute_id", $attribute_id)->whereNotNull("nav_id")->select(['nav_id', 'id', 'attribute_id'])->orderBy("nav_id")->get();
                foreach ($options as $o) {
                    $attribute_codes[$row->code][$o->id] = $letter . $o->nav_id;
                }
            }

            return $attribute_codes;
        });
        return $data;
    }

    function setPage($page)
    {
        $this->page = $page;
        return $this;
    }

}