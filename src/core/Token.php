<?php

namespace Core;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 11.33.03
 */

use \Config,
    Log,
    DB;
use Underscore\Types\Arrays;
use Exception;

class Token
{

    public $lang;
    public $text;
    private $hash;
    private $isSandbox = false;

    public function __construct($text)
    {
        $this->lang = \Lang::getLocale();
        $this->text = $text;
        $this->hash = array();
        $this->setDefaults();
    }

    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    public function setSandboxMode($bool)
    {
        $this->isSandbox = $bool;
        if ($this->isSandbox) {
            $this->set('sku', 'TW0156');
            $this->set('brand', 'Breil');
            $this->set('collection', 'Manta');
            if ($this->lang === 'it') {
                $this->set('category', 'Orologio');
                $this->set('default_category', 'Cronografo');
                $this->set('categories', 'Orologi');
                $this->set('default_categories', 'Cronografi');
                $this->set('product', 'Orologio Breil Manta TW0156');
                $this->set('attributes', 'Genere Donna, Stile Classico, Colore Nero, Materiale Acciaio');
                $this->set('trend', 'Idee per regali');
            } else {
                $this->set('category', 'Watch');
                $this->set('default_category', 'Cronograph');
                $this->set('categories', 'Watches');
                $this->set('default_categories', 'Cronographs');
                $this->set('product', 'Breil Manta Watches TW0156');
                $this->set('attributes', 'Gender Woman, Style Classic, Color Black, Material Steel');
                $this->set('trend', 'Chirstmas gifts');
            }
        } else {
            $this->hash = array();
        }
    }

    public function rebind($text)
    {
        $this->text = $text;
        return $this;
    }

    public function set($key, $value)
    {
        $key = strtolower(trim($key));
        $this->hash[$key] = $value;
    }

    public function get($key)
    {
        $key = strtolower(trim($key));
        return (isset($this->hash[$key])) ? $this->hash[$key] : null;
    }

    public function fetch($key)
    {
        $key = strtolower(trim($key));
        $method = '_' . strtolower(trim($key));
        $value = '';
        if (method_exists($this, $method)) {
            $value = $this->{$method}();
        } else {
            if (isset($this->hash[$key])) {
                $value = $this->get($key);
            }
        }
        return $value;
    }

    public function setDefaults()
    {
        $this->set('YEAR', date('Y'));
        $this->set('YEAR_PREV', date('Y', strtotime('-1 year')));
        $this->set('YEAR_NEXT', date('Y', strtotime('+1 year')));
    }

    public function render()
    {
        $text = $this->text;
        preg_match_all('#\[(.+)\]#iUs', $text, $matches);
        //\Utils::log("TOKEN RENDERING: $text", __METHOD__);
        //print_r($matches);
        //\Utils::watch();
        if (count($matches) > 0) {
            $tokens = $matches[0];
            $words = $matches[1];
            $replacements = array();
            $values = array();
            $max = count($tokens);
            for ($i = 0; $i < $max; $i++) {
                $token = $tokens[$i];
                $word = $words[$i];

                $method = $word;
                $parameters = array();

                if (\Str::contains($word, ':')) {
                    $parameters = explode(':', $word);
                    $method = $parameters[0];
                    array_shift($parameters);
                } else {
                    $parameters[0] = $word;
                    $method = explode(' ', $word)[0];
                }
                $attributes = null;
                if (isset($parameters[0])) {
                    $attributes = \Utils::parseAttributes($parameters[0]);
                    //\Utils::log($attributes,__METHOD__.'Attributes');
                    if (count($attributes)) {
                        $parameters[0] = explode(' ', $parameters[0])[0];
                    }
                }

                $key = strtolower(trim($method));
                $method = '_' . strtolower(trim($method));
                //\Utils::log($method,__METHOD__);
                $value = '';
                if (method_exists($this, $method)) {
                    $value = $this->{$method}($parameters);
                } else {
                    if (isset($this->hash[$key])) {
                        $value = $this->get($key);
                    }
                }
                if ($value !== '') {
                    if (isset($attributes['before'])) {
                        $value = $attributes['before'] . ' ' . $value;
                    }
                    if (isset($attributes['after'])) {
                        $value .= ' ' . $attributes['after'];
                    }
                }
                $replacements[] = $token;
                $values[] = trim($value);
            }

            //remote potentially repeated values
            $matrix = [];
            foreach ($replacements as $index => $replacement) {
                $replacement = substr($replacement, 1, -1);
                $matrix[$replacement] = ['index' => $index, 'value' => $values[$index]];
            }
            if (isset($matrix['CATEGORY']) and isset($matrix['CATEGORY_DEFAULT'])) {
                if ($matrix['CATEGORY']['value'] == $matrix['CATEGORY_DEFAULT']['value']) {
                    $values[$matrix['CATEGORY_DEFAULT']['index']] = null;
                }
            }
            $text = str_replace($replacements, $values, $text);
            $text = mb_ereg_replace('\s+', ' ', $text);
        }
        return $text;
    }

    private function _sku()
    {
        return $this->get('sku');
    }

    private function _value()
    {
        return $this->get('value');
    }

    private function _product()
    {
        return $this->get('product');
    }

    private function _brand()
    {
        if ($brand = $this->get('brand'))
            return $brand;

        $brand_id = $this->get('brand_id');
        if ($this->isNull($brand_id)) {
            return '';
        }
        $obj = \Brand::getObj($brand_id, $this->lang);
        return ($obj) ? $obj->name : null;
    }

    private function _collection()
    {
        if ($collection = $this->get('collection'))
            return $collection;

        $collection_id = $this->get('collection_id');
        if ($this->isNull($collection_id)) {
            return '';
        }
        $obj = \Collection::getObj($collection_id, $this->lang);
        return ($obj) ? $obj->name : null;
    }

    private function _trend()
    {
        if ($trend = $this->get('trend'))
            return $trend;

        $trend_id = $this->get('trend_id');
        if ($this->isNull($trend_id)) {
            return '';
        }
        $obj = \Trend::getObj($trend_id, $this->lang);
        return ($obj) ? $obj->name : null;
    }

    private function _category()
    {
        if ($category = $this->get('category'))
            return $category;

        $category_id = $this->get('category_id');
        if ($this->isNull($category_id)) {
            return '';
        }
        $obj = \Category::getObj($category_id, $this->lang);
        if ($obj == null) return null;
        return (isset($obj->singular) AND $obj->singular != '') ? $obj->singular : $obj->name;
    }

    private function _categories()
    {
        if ($category = $this->get('categories'))
            return $category;

        $category_id = $this->get('category_id');
        if ($this->isNull($category_id)) {
            return '';
        }
        $obj = \Category::getObj($category_id, $this->lang);
        return ($obj) ? $obj->name : null;
    }

    private function _category_default()
    {
        if ($category = $this->get('default_category'))
            return $category;

        $category_id = $this->get('default_category_id');
        if ($this->isNull($category_id)) {
            return '';
        }
        $obj = \Category::getObj($category_id, $this->lang);
        if ($obj == null) return null;
        return (isset($obj->singular) AND $obj->singular != '') ? $obj->singular : $obj->name;
    }

    private function _categories_default()
    {
        if ($category = $this->get('default_categories'))
            return $category;

        $category_id = $this->get('default_category_id');
        if ($this->isNull($category_id)) {
            return '';
        }
        $obj = \Category::getObj($category_id, $this->lang);
        return ($obj) ? $obj->name : null;
    }

    private function _attr($params)
    {
        $code = $params[0];

        $hasValue = false;
        $value = null;
        $code = strtolower(trim($code));
        if (isset($params[1])) {
            $value = $params[1];
            $hasValue = true;
        }
        $attribute_option_id = $this->get('attr:' . $code);
        if ($attribute_option_id) {
            $value = $attribute_option_id;
            $hasValue = true;
        }
        $returnVal = '';
        try {
            if ($hasValue) {
                $ao = \AttributeOption::getObj($value, $this->lang);
                if ($ao) {
                    $returnVal = ($ao->name) ? $ao->name : $ao->uname;
                }
            } else {
                $product_id = $this->get('product_id');
                if ($product_id > 0) {
                    $attribute = \Attribute::where('code', $code)->first();
                    if ($attribute === null) return $returnVal;
                    $attribute_val = \DB::table('products_attributes')->where('product_id', $product_id)->where('attribute_id', $attribute->id)->lists('attribute_val');
                    if (!is_array($attribute_val)) $attribute_val = [];
                    $rows = \AttributeOption::with(array($this->lang))->whereIn("id", $attribute_val)->get();
                    /*if($ao){
                        $returnVal = ($ao->{$this->lang}->name) ? $ao->{$this->lang}->name : $ao->uname;
                    }       */
                    if (count($rows)) {
                        foreach ($rows as $ao) {
                            $returnVal .= (($ao->{$this->lang}->name) ? $ao->{$this->lang}->name : $ao->uname) . ' ';
                        }
                        $returnVal = rtrim($returnVal, ' ');
                    }
                } else {
                    if ($this->isSandbox) {
                        $attribute = \Attribute::where('code', $code)->first();
                        if ($attribute) {
                            $ao = \AttributeOption::with(array($this->lang))->where('attribute_id', $attribute->id)->orderBy(\DB::raw('RAND()'))->first();
                            if ($ao) {
                                $returnVal = ($ao->{$this->lang}->name) ? $ao->{$this->lang}->name : $ao->uname;
                            }
                        }
                    }
                    //return "";
                }
            }
        } catch (Exception $ex) {
            \Utils::log($ex->getMessage(), 'EXCEPTION');
            //return "";
        }
        //\Utils::unwatch();
        return $returnVal;
    }

    private function _lang($params)
    {
        $code = $params[0];
        $code = strtolower(trim($code));
        $obj = \Lexicon::where('code', $code)->first();
        return $obj->{$this->lang}->name;
    }

    private function _attributes($params)
    {
        return $this->get('attributes');
    }

    private function isNull($test)
    {
        return ($test == null OR $test == false OR $test == 0 OR $test == '');
    }

    public function resolve($function)
    {
        try {
            return $this->{'_' . $function}();
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), __METHOD__);
        }
    }
}