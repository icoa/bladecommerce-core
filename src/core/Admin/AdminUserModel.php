<?php

namespace Core\Admin;

use Sentry,
    AdminTpl,
    File;
use Robbo\Presenter\Presenter;
use services\Membership\Models\Affiliate;

class AdminUserModel extends Presenter
{

    public function presentName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function presentProfileImg()
    {
        $img = AdminTpl::root() . "/images/peoples/male.gif";
        if (isset($this->profile)) {
            $path = AdminTpl::path() . "/images/peoples/" . ($this->profile);
            if ($this->profile != "" AND File::exists($path)) {
                $img = AdminTpl::root() . "/images/peoples/" . $this->profile;
            }
        }
        return $img;
    }

    public function getShop()
    {
        if ($this->negoziando == 0) {
            return null;
        }
        return \MorellatoShop::find($this->shop_id);
    }

    public function presentShop()
    {
        $shop = $this->getShop();
        if (is_null($shop)) return null;
        return $shop->name;
    }

    public function getAffiliate()
    {
        if ($this->affiliate_id == 0) {
            return null;
        }
        return Affiliate::find($this->affiliate_id);
    }

    public function presentAffiliate()
    {
        $affiliate = $this->getAffiliate();
        if (is_null($affiliate)) return null;
        return $affiliate->name;
    }
}