<?php


/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */

class AdminAction {
    
    public $id;
    public $title;
    public $href='javascript:;';
    public $icon='default';
    public $class='';
    public $desc='';
    
    function __construct($title,$href=null,$icon=null,$class=null,$desc=null,$id=null) {
        $this->id = $id;
        $this->title = $title;
        if($href)$this->href = $href;
        if($icon)$this->icon = $icon;
        if($class)$this->class = $class;
        if($desc)$this->desc = $desc;
    }
}