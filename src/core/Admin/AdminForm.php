<?php

namespace Core\Admin;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */

use Theme,
    Request,
    Input,
    Form;
use HtmlObject\Element;
use HtmlObject\Image;
use HtmlObject\Traits\Tag;
use Underscore\Types\Arrays;

class Label extends Tag {

    /**
     * The tag element
     *
     * @var string
     */
    protected $element = 'label';

}

class AdminForm {

    protected $data = array();
    protected $attributes = array();
    protected $htmlInput = '';
    protected $htmlWrap = '';
    protected $htmlContainer = '';
    protected $htmlLabel = '';
    protected $htmlAfter = '';
    protected $groupClass = '';
    protected $groupAttributes;
    protected $labelText = '';
    protected $labelDefaultClass = 'control-label align-right';
    protected $labelAttributes = array();
    protected $tagName;
    protected $wrapClass;
    protected $hasWrap;
    protected $hasLang = false;
    protected $languages;
    protected $elements = array();
    protected $withControl = true;
    protected $appendElement = '';
    protected $prependElement = '';
    protected $defaultOptions = array("plain" => false);
    protected $options;

    function __construct() {
        $languages = \Mainframe::languages();
        $codes = \EchoArray::set($languages)->keyIndex('id', 'name')->get();
        $this->languages = $codes;
        $this->options = $this->defaultOptions;
        //\Input::flashExcept("_lang_fields[]");
    }
    
    function queue(){
        $args = func_get_args();
        foreach($args as $element){
            $this->elements[] = $element;
        }        
        return $this;
    }
    
    function append($text){
        $this->appendElement = $text;
        return $this;
    }
    
    function after($content){
        $this->htmlAfter .= $content;
    }
    
    function prepend($text){
        $this->prependElement = $text;
        return $this;
    }
    
    function control(){
        $this->reset();
        $this->withControl = !($this->withControl);
    }

    function group(Array $attributes = ['class' => 'control-group']) {        
        $this->groupAttributes = $attributes;
        return $this;
    }

    function label($text, $attributes = array()) {
        $this->labelText = $text;
        $this->labelAttributes = $attributes;
        return $this;
    }

    function populate($data,$flushHidden=true) {
        $this->data = $data;        
        if($flushHidden){
            if(isset($_REQUEST['popup'])){
                echo Form::hidden("popup", 'Y');
            }
        }
    }

    function expand($data){
        if(is_array($data)){
            foreach($data as $key => $value){
                $this->data[$key] = $value;
            }
        }
    }
    
    function plain(){
        $this->options(['plain' => TRUE]);
        return $this;
    }
    
    function options($options = array()){
        //Arrays::union($options,$this->options);
       
        $this->options = \EchoArray::set($this->options)->extend($options)->get();
        return $this;
        
    }
    
    function printData(){
        echo '<pre>';
        print_r($this->data);
        echo '</pre>';
    }

    function helpBlock($text) {
        $this->htmlAfter .= Element::span($text)->addClass('help-block');
        return $this;
    }
    
    function helpInline($text) {
        $this->htmlAfter .= Element::span($text)->addClass('help-inline');
        return $this;
    }

    function wrap($class) {
        $this->wrapClass = $class;
        return $this;
    }

    function lang() {
        $this->hasLang = true;
        return $this;
    }

    function reset() {
        $this->html = '';
        $this->htmlInput = '';
        $this->htmlAfter = '';
        $this->labelText = '';
        $this->appendElement = '';
        $this->prependElement = '';
        $this->labelAttributes = array();
        $this->attributes = array();
        $this->groupAttributes = array();
        $this->hasLang = false;
        $this->options = $this->defaultOptions;
    }

    private function _setup($name, $attributes) {
        if(\Str::contains($name, ".")){ //relation
            $tokens = explode(".", $name);            
            $name = $tokens[1];
        }
        $this->tagName = $name;        
        $default_attributes = array('id' => $name,'autocomplete' => 'off');
        $this->attributes = \EchoArray::set($default_attributes)->extend($attributes)->get();
    }

    function yesNo($name, $defaultValue = 1, $attributes = array('class' => 'yes_no')) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->htmlInput = Form::checkbox($this->tagName, 1, $this->getValue($name) == 1, $this->attributes);
        return $this;
    }
    
    function onOff($name, $defaultValue = 1, $attributes = array('class' => 'style')) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $value = $this->getValue($name);
        /*$element = Element::div("", ["class" => "radio inline"]);
        $this->attributes['id'] = $this->tagName."_on";
        $radio = \Form::radio($this->tagName, 1, $value == 1, $this->attributes);
        $label = Element::label("SI",["class" => "toggle-label", "for" => $this->attributes['id']]);
        $element->nest($radio)->nest($label);
        
        $element2 = Element::div("", ["class" => "radio inline"]);
        $this->attributes['id'] = $this->tagName."_off";
        $radio = \Form::radio($this->tagName, 0, $value == 0, $this->attributes);
        $label = Element::label("NO",["class" => "toggle-label", "for" => $this->attributes['id']]);
        $element2->nest($radio)->nest($label);*/
        
        $this->attributes['id'] = $this->tagName."_on";
        $radio = \Form::radio($this->tagName, 1, $value == 1, $this->attributes);
        $element = Element::label($radio."SI",["class" => "radio inline", "for" => $this->attributes['id']]);
        
        $this->attributes['id'] = $this->tagName."_off";
        $radio = \Form::radio($this->tagName, 0, $value == 0, $this->attributes);
        $element2 = Element::label($radio."NO",["class" => "radio inline", "for" => $this->attributes['id']]);

        
        $this->htmlInput = $element . $element2;
        return $this;
    }
    
    
    function lang_onOff($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {

            $lang_name = $name . "_" . $key;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            
            $value = $this->getValue($lang_name);
            $element = Element::div("", ["class" => "radio inline"]);
            $this->attributes['id'] = $lang_name."_on";
            $radio = \Form::radio($lang_name, 1, $value == 1, $this->attributes);
            $label = Element::label("SI",["class" => "toggle-label", "for" => $this->attributes['id']]);
            $element->nest($radio)->nest($label);

            $element2 = Element::div("", ["class" => "radio inline"]);
            $this->attributes['id'] = $lang_name."_off";
            $radio = \Form::radio($lang_name, 0, $value == 0, $this->attributes);
            $label = Element::label("NO",["class" => "toggle-label", "for" => $this->attributes['id']]);
            $element2->nest($radio)->nest($label);

            $this->htmlInput = $element . $element2;
        
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }

    function textarea($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);        
        $this->htmlInput = Form::textarea($this->tagName, $this->getValue($name), $this->attributes);
        return $this;
    }

    function ckeditor($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->htmlInput = Form::textarea($this->tagName, $this->getValue($name), $this->attributes);

        $this->rawTagName = $name;

        $custom_attributes = $attributes;
        $custom_attributes['class'] .= ' htmleditor';
        $this->_setup($name, $attributes);
        $attr = \HTML::attributes($this->attributes);
        $textarea = Form::textarea($this->tagName, $this->getValue($name),['class' => 'hidden', 'id' => "ta_".$this->tagName]);
        $value = $this->getValue($name);
        $div = "<div class='ckeditor_preview' id='preview_$this->tagName'><div class='toolbar'><button tabindex='-1' class='showeditor btn btn-small btn-warning' rel='$this->tagName'>MOSTRA EDITOR</button></div><div class='inner_content'>$value</div></div>";
        $this->htmlInput = "<div $attr >" . $div . $textarea . "</div>";


        return $this;
    }

    function text($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->htmlInput = Form::text($this->tagName, $this->getValue($name), $this->attributes);
        return $this;
    }

    function checkbox($name, $selected = false, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->htmlInput = Form::checkbox($this->tagName, $this->getValue($name), $selected, $this->attributes);
        return $this;
    }

    function checkbox_boolean($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $value = $this->getValue($name);
        $checked = true;
        if((int)$value == 0){
            $checked = false;
        }
        $this->htmlInput = Form::checkbox($this->tagName, 1, $checked, $this->attributes);
        return $this;
    }
    
    function money($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->attributes['class'] .= " align-right";
        $this->attributes['placeholder'] = "0.00";
        $value = $this->getValue($name);
        $value = \Format::money($value);
        $this->htmlInput = Form::text($this->tagName, $value, $this->attributes);
        $currency = \Core::getDefaultCurrency();
        $this->append($currency->sign);
        return $this;
    }
    
    function float($name, $attributes = array(), $appendSign = '') {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->attributes['class'] .= " align-right";
        $this->attributes['placeholder'] = "0.00";
        $value = $this->getValue($name);        
        $this->htmlInput = Form::text($this->tagName, $value, $this->attributes);
        $this->append($appendSign);
        return $this;
    }

    function number($name, $attributes = array(), $appendSign = '') {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->attributes['placeholder'] = "0";
        $value = $this->getValue($name);
        $this->htmlInput = Form::input("number",$this->tagName, $value, $this->attributes);
        $this->append($appendSign);
        return $this;
    }
    
    function hidden($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->htmlInput = Form::hidden($this->tagName, $this->getValue($name), $this->attributes);
        return $this;
    }

    function lang_text($name, $attributes = array(), $langAsArray = false) {
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {
            $lang_name = ($langAsArray) ? $name . "[" . $key . "]" : $name . "_" . $key;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $this->htmlInput .= Form::text($lang_name, $this->getValue($lang_name), $this->attributes);
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }


    function lang_param($name, $attributes = array(), $root = 'params') {
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {
            $lang_name = $root . "[" . $name . "_" . $key . "]" ;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $this->attributes['id'] = $name . "_" . $key;
            $this->htmlInput .= Form::text($lang_name, $this->getValue($lang_name), $this->attributes);
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }

    function lang_hidden($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        
        foreach ($languages as $key => $value) {
            $lang_name = $name . "_" . $key;
            $custom_attributes = $attributes;
            if(!isset($custom_attributes['class'])){
                $custom_attributes['class'] = '';
            }
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $this->htmlInput .= Form::hidden($lang_name, $this->getValue($lang_name), $this->attributes);
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }
    
    
    function tokens(){
        $this->htmlAfter .= Element::span("<b>Supporto:</b> puoi utilizzare una serie di Tokens; <a href='javascript:;' onclick='Echo.showTokensList();'>Clicca qui per conoscere la lista dei token utilizzabili</a>")->addClass('help-block');
        return $this;
    }
    
    function tokensGenerate(){
        $this->htmlAfter .= Element::span("<b>Supporto:</b> utilizza il tasto <b>GENERA SEO</b> per generare automaticamente un valore per questo campo, oppure <a href='javascript:;' onclick=\"Echo.showTokensListMenu('$this->rawTagName');\">clicca qui per selezionare un template di Magic Tokens dal menù di selezione</a>")->addClass('help-block');
        $this->htmlAfter .= "<div class='clearfix' id='mtt_$this->rawTagName'></div>";
        return $this;
    }
    
    function tokensTemplate(){
        $this->htmlAfter .= Element::span("<b>Nota:</b> puoi utilizzare un template di Magic Tokens; <a href='javascript:;' onclick=\"Echo.showTokensListMenu('$this->rawTagName');\">Clicca qui per selezionare un template dal menù di selezione</a>")->addClass('help-block');
        $this->htmlAfter .= "<div class='clearfix' id='mtt_$this->rawTagName'></div>";
        return $this;
    }
    
    
    function lang_select($name, $options, $attributes = array(), $toolbar = array()) {
        $this->rawTagName = $name;
        $attributes['autocomplete'] = 'off';
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {
            $lang_name = $name . "_" . $key;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $this->htmlInput .= Form::select($lang_name, $options, $this->getValue($lang_name), $this->attributes);
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }
    
    
    function lang_selectMulti($name, $options, $attributes = array(), $toolbar = array()) {
        $this->rawTagName = $name;
        $attributes['autocomplete'] = 'off';
        $attributes['multiple'] = 'multiple';
        
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {
            $lang_name = $name . "_" . $key;
            $formername = $lang_name."[]";
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $this->htmlInput .= Form::select($formername, $options, $this->getValue($lang_name), $this->attributes);
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }
    

    function lang_textarea($name, $attributes = array(), $langAsArray = false) {
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {

            //$lang_name = $name . "_" . $key;
            $lang_name = ($langAsArray) ? $name . "[" . $key . "]" : $name . "_" . $key;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $this->htmlInput .= Form::textarea($lang_name, $this->getValue($lang_name), $this->attributes);
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }
    
    function lang_ckeditor($name, $attributes = array(), $langAsArray = false){
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {

            //$lang_name = $name . "_" . $key;
            $lang_name = ($langAsArray) ? $name . "[" . $key . "]" : $name . "_" . $key;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' htmleditor hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $attr = \HTML::attributes($this->attributes);
            //$this->htmlInput .= "<div $attr >" . Form::textarea($lang_name, $this->getValue($lang_name),['class' => 'ckeditor']) . "</div>";
            $textarea = Form::textarea($lang_name, $this->getValue($lang_name),['class' => 'hidden', 'id' => "ta_".$lang_name]);
            $value = $this->getValue($lang_name);
            $div = "<div class='ckeditor_preview' id='preview_$lang_name'><div class='toolbar'><button tabindex='-1' class='showeditor btn btn-small btn-warning' rel='$lang_name'>MOSTRA EDITOR</button></div><div class='inner_content'>$value</div></div>";
            $this->htmlInput .= "<div $attr >" . $div . $textarea . "</div>";
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }
    
    function lang_bbeditor($name, $attributes = array()){
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        foreach ($languages as $key => $value) {

            $lang_name = $name . "_" . $key;
            $custom_attributes = $attributes;
            $custom_attributes['class'] .= ' bbeditor hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->_setup($lang_name, $custom_attributes);
            $attr = \HTML::attributes($this->attributes);
            //$this->htmlInput .= "<div $attr >" . Form::textarea($lang_name, $this->getValue($lang_name),['class' => 'ckeditor']) . "</div>";
            $textarea = Form::textarea($lang_name, $this->getValue($lang_name),['rows' => '4', 'id' => "ta_".$lang_name]);
            $value = $this->getValue($lang_name);
            $div = "<div class='ckeditor_preview' id='preview_$lang_name'><div class='toolbar'>";
            
            $tokens = \Mainframe::magicTokens();
            foreach($tokens as $token){
                $token->description = e($token->description);
                $div .= "<button title='$token->description' data-token='[$token->token]' tabindex='-1' class='btn btn-small btn-default btn-token hovertip' rel='$lang_name'>$token->token</button>";
            }
            
                    $div .= "</div>$textarea</div>";
            $this->htmlInput .= "<div $attr >" . $div . "</div>";
        }
        $this->htmlInput .= Form::hidden("_lang_fields[]", $name);
        return $this;
    }

    function password($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->_setup($name, $attributes);
        $this->htmlInput = Form::password($this->tagName, $this->attributes);
        return $this;
    }



    function select($name, $options, $attributes = array(), $toolbar = array()) {
        $this->rawTagName = $name;
        $attributes['autocomplete'] = 'off';
        $this->_setup($name, $attributes);        
        $this->htmlInput = Form::select($this->tagName, $options, $this->getValue($name), $this->attributes);
        if(count($toolbar) > 0){
            $this->htmlInput .= '<ul class="table-controls ml10 floatL">';
            if(isset($toolbar['new'])){
                $this->htmlInput .= <<<LI
<li><a data-placement="top" class="btn hovertip" onclick="Echo.popup('{$toolbar['new']}');" href="javascript:;" title="Aggiungi nuovo Record"><i class="icon-plus"></i></a></li>                     
LI;
            }
            if(isset($toolbar['reload'])){
                $this->htmlInput .= <<<LI
<li><a data-placement="top" class="btn hovertip" onclick="Echo.reloadSelect('{$toolbar['reload']}','$this->tagName');" href="javascript:;" title="Aggiorna elenco"><i class="icon-retweet"></i></a></li>                     
LI;
            }
            $this->htmlInput .= '</ul>';
        }
        return $this;
    }

    function selectMulti($name, $options, $attributes = array()) {
        if(!\Str::contains($name, "[]")){
            $name .= "[]";
        }
        $this->rawTagName = $name;
        $formername = str_replace('[]', '', $name);
        $attributes["multiple"] = "multiple";
        $attributes['autocomplete'] = 'off';
        $this->_setup($name, $attributes);
        $this->attributes["id"] = $formername;
        //$old = Input::old($formername, $this->data[$formername]);
        $old = $this->getValue($formername);
        if (!is_array($old)) {
            $old = explode(",", $old);
        }
        $this->htmlInput = Form::select($this->tagName, $options, $old, $this->attributes);
        return $this;
    }

    function bar($title) {
        $title = Element::h5($title);
        $inner = Element::div($title)->addClass("navbar-inner");
        $div = Element::div($inner)->addClass("navbar");
        return $div;
    }
    
    function alert($content,$class='default',$padded = false){
        if($padded)$class.=" padded";
        $html = <<<HTML
<div class="alert alert-$class semi-block">
    <button tabindex='-1' data-dismiss="alert" class="close" type="button">×</button>
    $content
</div>                
HTML;
        $group = $this->getGroup();
        if($group){
            $html = $group->nest($html);
        }
        $this->reset();
        return $html;        
    }

    private function camel($str){
        return str_replace(array('[',']'),'',\Str::camel(\Str::lower($str)));
    }

    function imagepicker($name, $attributes = array()) {
        $this->rawTagName = $name;
        $value = $this->getValue($name);
        $id = $this->camel($name);
        $this->text($name, ['class' => 'imagepicker', 'id' => $id]);
        $src = \Site::mediaUrl() . "/no.gif";
        $imgFile = \Site::path() . $value;

        if ($value != "" AND \File::exists($imgFile)) {
            $src = \Site::root() . $value;
        }

        if (isset($attributes['class']))
            $attributes['class'] .= ' imagepickerContainer';
        else
            $attributes['class'] = 'imagepickerContainer';

        $css = \HTML::attributes($attributes);
        
        //\Utils::log($css);

        $html = <<<HTML
<div $css id="{$id}_subjectImg">
    <a class="fancybox" href="$src" title="Anteprima immagine">
        <img class="subject" src="$src" />
    </a>
    $this->htmlInput
</div>
HTML;
        $this->htmlInput = $html;
        return $this;
    }

    function lang_imagepicker($name, $attributes = array()) {
        $this->rawTagName = $name;
        $this->lang();
        $languages = $this->languages;
        $cLang = \Core::getLang();
        $html = '';
        foreach ($languages as $key => $value) {
            $lang_name = $name . "_" . $key;
            $custom_attributes = $attributes;
            if (isset($custom_attributes['class']))
                $custom_attributes['class'] .= ' hasLang lang_' . $key;
            else
                $custom_attributes['class'] = 'hasLang lang_' . $key;
            if ($cLang != $key) {
                $custom_attributes['class'] .= ' hidden';
            }
            $this->tagName = $lang_name;                   
            $this->imagepicker($lang_name, $custom_attributes);
            $html .= $this->htmlInput;
        }
        $this->htmlInput = $html . Form::hidden("_lang_fields[]", $name);
        return $this;
    }

    function list_products($name, $attributes = array()) {
        $this->rawTagName = $name;
        $value = $this->getValue($name);

        $products = [];
        if ($value != "") {
            $products = \Product::rows(\Core::getLang())->whereIn('id',explode(",",$value))->select(['id','name'])->get();
        }

        $html = "<p>Nessun prodotto</p>";
        if(count($products) > 0){
            $html = '<ul>';
            foreach($products as $product){
                $html .= "<li><a target='_blank' href='/admin/products/edit/{$product->id}'>{$product->name}</a></li>";
            }
            $html .= '</ul>';
        }
        $this->htmlInput = $html;
        return $this;
    }
    
    function setValue($name,$value){
        /*\Utils::log($name,"FORM SET VALUE NAME");
        \Utils::log($value,"FORM SET VALUE");*/
        $this->data[$name] = $value;
    }
    

    function getValue($name) {



        
        $param = (isset($this->data[$name])) ? $this->data[$name] : null;
        
        if(\Str::contains($name, ".")){ //relation
            $tokens = explode(".", $name);
            if(!is_array($this->data)){
                if($this->data->has($tokens[0])){
                    $param = (isset($this->data->{$tokens[0]}->{$tokens[1]})) ? $this->data->{$tokens[0]}->{$tokens[1]} : null;                    
                }
            }
            $name = $tokens[1];
        }
        
        if (isset($param)) {
            //\Utils::log($this->data);
            $ret = Input::old($name, $param);
            //\Utils::log($ret,"RETURN VALUE");
        } else {
            $ret = Input::old($name);
        }
        return $ret;
    }

    private function getLabel() {
        if ($this->labelText != '') {
            $this->labelText .= ":";
            
            $labelFor = ($this->hasLang) ? $this->rawTagName."_".\Core::getLang() : $this->tagName;

            $default_attributes = array('for' => $labelFor, 'class' => $this->labelDefaultClass);
            $attributes = \EchoArray::set($default_attributes)->extend($this->labelAttributes)->get();
            $element = Element::label($this->labelText, $attributes);
            return $element;
        }
        return false;
    }
    
    public function langMenu($tagName){
        $cLang = \Core::getLang();

   
            $languages = $this->languages;
            $html = '';
            foreach ($languages as $key => $value) {
                $src = \AdminTpl::img("images/flags/{$key}.jpg");
                $html .= "<li><a title=\"$value\" href='javascript:;' onclick=\"Echo.swapLang('$key','$this->rawTagName');\"><i><img src='$src' /></i> $value</a></li>";
            }

            $src = \AdminTpl::img("images/flags/{$cLang}.jpg");

            $panel = <<<HTML
<div class="btn-group langChoice">
    <button tabindex='-1' data-toggle="dropdown" class="btn dropdown-toggle"><img class="currentLangFlag" src="$src" /> <span class="caret dd-caret"></span></button>
    <ul class="dropdown-menu pull-right">
        $html
    </ul>
</div>
HTML;
            return $panel;
        
    }

    private function getInput() {
        $content = $this->htmlInput;

        if ($this->hasLang) {          
            $content .= $this->langMenu($this->tagName);
        }
        
        if($this->appendElement != ''){           
            $content .= "<span class=\"add-on\">$this->appendElement</span>";
        }
        if($this->prependElement != ''){           
            $content = "<span class=\"add-on\">$this->prependElement</span>" . $content;
        }
        
        $content .= $this->htmlAfter;

        return $content;
    }

    private function getGroup() {
        /*if ($this->groupClass != '') {
            return Element::div('')->addClass($this->groupClass);
        }*/
        if(count($this->groupAttributes) > 0){
            if(!isset($this->groupAttributes['id'])){
               $this->groupAttributes['id'] = $this->rawTagName."_cgroup";
            }
            return Element::div('')->setAttributes($this->groupAttributes);
        }
        return false;
    }
    
    
    function renderQueue(){        
        $content = '';
       
            foreach($this->elements as $e){
                $content .= $e->render();
            }
        $this->reset();
        echo $content;
    }


    function __toString() {         
        $group = $this->getGroup();
        $label = $this->getLabel();
        $content = $this->getInput();

        //if($this->withControl){
        if($this->options['plain'] == false){
            $container = Element::div($content)->addClass("controls");
        }else{
            if($this->appendElement != '' OR $this->prependElement != '' OR isset($this->options['cntClass'])){
                $container = Element::div($content);
            }else{
                $container = $content;
            }            
        }
        
        if(is_object($container)){
            if($this->appendElement != ''){
                $container->addClass("input-append");           
            }
            if($this->prependElement != ''){
                $container->addClass("input-prepend");            
            }
            if(isset($this->options['cntClass'])){
                $container->addClass($this->options['cntClass']);
            }
        }
        
        
        

        $this->reset();

        if ($group) {
            if ($label) {
                $group->nest(array('label' => $label, 'content' => $container));
            } else {
                $group->nest(array('content' => $container));
            }
            return (string) $group;
        }

        if ($label) {
            return $label . $container;
        }

        return (string) $container;
    }
    
    
    function table_menu(){
        $cLang = \Core::getLang();

            $languages = $this->languages;
            $html = '';
            foreach ($languages as $key => $value) {
                $src = \AdminTpl::img("images/flags/{$key}.jpg");
                $html .= "<li><a title=\"$value\" href='javascript:;' onclick=\"EchoTable.swapLang('$key');\"><i><img src='$src' /></i> $value</a></li>";
            }

            $src = \AdminTpl::img("images/flags/{$cLang}.jpg");

            $panel = <<<HTML
<div class="nav pull-right">
    <a href="#" class="dropdown-toggle just-icon" data-toggle="dropdown"><img class="currentLangFlag" src="$src" /> <span class="caret dd-caret"></span></a>
    <ul class="dropdown-menu pull-right">
        $html
    </ul>
</div>
HTML;
            return $panel;
        
    }
    
    
    
    function address(){
        $instance = new static;
        
        $country_id = $instance::getValue('address.country_id');
        
        $html = '';
        $html .= $instance::text("address.address1", ['class' => 'span10'])->group()->label("Indirizzo");
        $html .= $instance::text("address.address2", ['class' => 'span4'])->group()->label("Indirizzo (2)");
        $html .= $instance::text("address.postcode", ['class' => 'span2'])->group()->label("C.A.P.");        
        $html .= $instance::text("address.city", ['class' => 'span8'])->group()->label("Città");
        $html .= $instance::select("address.country_id", \Mainframe::selectCountries(), ['class' => 'span8 select'])
                                                ->group()
                                                ->label("Nazione")
                                                ->helpBlock("<b>Nota:</b> al cambiare della Nazione le opzioni per Provincia/Stato si aggiornano automaticamente");
        $html .= $instance::select("address.state_id", \Mainframe::selectStates($country_id), ['class' => 'span8 select'])
                                                ->group()
                                                ->label("Provincia/Stato");
        $html .= $instance::text("address.phone", ['class' => 'span5'])->group()->label("Telefono");
        $html .= $instance::text("address.phone_mobile", ['class' => 'span5'])->group()->label("Cellulare");
        $html .= $instance::text("address.vat_number", ['class' => 'span8'])->group()->label("Partita IVA/C.F.");
        $html .= $instance::textarea("address.other", ['class' => 'span10'])->group()->label("Note");
        return $html;
    }

}

?>