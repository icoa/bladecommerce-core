<?php

class PaymentsController extends MultiLangController
{

    public $component_id = 66;
    public $title = 'Gestione Gateway di pagamento';
    public $page = 'payments.index';
    public $pageheader = 'Gestione Gateway di pagamento';
    public $iconClass = 'font-cog';
    public $model = 'Payment';
    protected $rules = array(
        'email_ok' => 'required',
        'payment_status' => 'required',
    );
    protected $lang_rules = array(
        'name' => 'required',
    );
    protected $friendly_names = array(
        'email_ok' => 'Messaggio di pagamento riuscito',
        'payment_status' => 'Status del pagamento (default)',
    );
    protected $lang_friendly_names = array(
        'name' => 'Denominazione Gateway',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Gateway di pagamento');
        $this->toFooter("js/echo/payments.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/payments.js");
        $this->page = 'payments.trash';
        $this->pageheader = 'Cestino Gateway di pagamento';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/payments.js");
        $this->page = 'payments.create';
        $this->pageheader = 'Nuovo Gateway di pagamento';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/payments.js");
        $this->page = 'payments.create';
        $this->pageheader = 'Modifica Gateway di pagamento';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        //$obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)
            ->groupBy("payments.id")
            ->select('payments.id', 'name', 'price', 'online', 'active', 'payments.updated_at', 'payments.position', 'payments.sdesc', 'payments_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('price', function ($data) {
                $v = \Format::currency($data['price'],true);
                return "<strong>$v</strong>";
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong><a href='$link'>{$data['name']}</a></strong>$add";
            })
            ->edit_column('online', function ($data) {
                return ($data['online'] == 1) ? '<span class="label label-success">Online</span>' : '<span class="label label-important">Offline</span>';
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->edit_column('position', function ($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("payments.id")
            ->select('payments.id', 'name', 'price', 'online', 'active', 'payments.updated_at', 'payments.deleted_at', 'payments.sdesc', 'payments_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('price', function ($data) {
                $v = \Format::currency($data['price'],true);
                return "<strong>$v</strong>";
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->edit_column('online', function ($data) {
                return ($data['online'] == 1) ? '<span class="label label-success">Online</span>' : '<span class="label label-important">Offline</span>';
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
        $this->handleRules($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
        $this->handleRules($model);
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
            }
        }

        $_POST['carrier_restriction'] = (int)\Input::get("carrier_restriction", 0);
        $_POST['group_restriction'] = (int)\Input::get("group_restriction", 0);
        $_POST['country_restriction'] = (int)\Input::get("country_restriction", 0);

        $_POST['options'] = $this->setOptions();

        $_POST['email_ok'] = (int)$_POST['email_ok'];
        $_POST['email_ko'] = (int)$_POST['email_ko'];
        $_POST['email_wait'] = (int)$_POST['email_wait'];

        $model = $this->model;
        if ((int)$_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }


    private function handleRules($model)
    {

        $payment_id = $model->id;

        \DB::table("payments_carrier")->where("payment_id", $payment_id)->delete();
        $carrier_restriction = (int)\Input::get("carrier_restriction", 0);
        if ($carrier_restriction == 1) {
            $unselected_carriers = \Input::get("unselected_carriers", []);
            if (!empty($unselected_carriers)) {
                foreach ($unselected_carriers as $carrier_id) {
                    \DB::table("payments_carrier")->insert(compact("payment_id", "carrier_id"));
                }
            }
        }

        \DB::table("payments_country")->where("payment_id", $payment_id)->delete();
        $country_restriction = (int)\Input::get("country_restriction", 0);
        if ($country_restriction == 1) {
            $unselected_countries = \Input::get("unselected_countries", []);
            if (!empty($unselected_countries)) {
                foreach ($unselected_countries as $country_id) {
                    \DB::table("payments_country")->insert(compact("payment_id", "country_id"));
                }
            }
        }

        \DB::table("payments_group")->where("payment_id", $payment_id)->delete();
        $group_restriction = (int)\Input::get("group_restriction", 0);
        if ($group_restriction == 1) {
            $unselected_groups = \Input::get("unselected_groups", []);
            if (!empty($unselected_groups)) {
                foreach ($unselected_groups as $group_id) {
                    \DB::table("payments_group")->insert(compact("payment_id", "group_id"));
                }
            }
        }


        return;

    }


    static function getCarriers($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);

        $xref_ids = \DB::table("payments_carrier")->where("payment_id", $id)->lists("carrier_id");
        \Utils::log($xref_ids);
        $ids = array_merge($ids, $xref_ids);
        $query = \Carrier::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getGroups($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("payments_group")->where("payment_id", $id)->lists("group_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \CustomerGroup::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getCountries($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("payments_country")->where("payment_id", $id)->lists("country_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \Country::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }


    private function setOptions()
    {
        $data = \Input::get("options", []);
        return count($data) > 0 ? serialize($data) : null;
    }


    static function getOptions($options)
    {
        $arr = [];
        if($options){
            $data = unserialize($options);

            if(is_array($data)){
                foreach($data as $key => $val){
                    if(is_array($val)){
                        foreach($val as $_key => $_val){
                            $arr["options[$key][$_key]"] = $_val;
                        }
                    }else{
                        $arr["options[$key]"] = $val;
                    }
                }
            }
        }
        return $arr;
    }


}
