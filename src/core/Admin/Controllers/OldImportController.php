<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-lug-2013 15.57.17
 */

use Carbon\Carbon;
use Core\Token;

class OldImportController extends BaseController {

    private $excel = true;
    private $_DATA = array();
    private $_ATTRIBUTES = array();
    private $_ATTRIBUTES_OPTIONS = array();
    private $_IMPORT_ATTRIBUTES = array();
    private $_IMPORT_ATTRIBUTES_OPTIONS = array();
    
    /*#DELETE FROM images WHERE product_id NOT IN (SELECT id FROM products)
#DELETE FROM images_lang WHERE product_image_id NOT IN (SELECT id FROM images)
#DELETE FROM products_attributes WHERE product_id NOT IN (SELECT id FROM products)
#DELETE FROM products_attributes_position WHERE product_id NOT IN (SELECT id FROM products)
#DELETE FROM products_attributes_position WHERE attribute_id NOT IN (SELECT id FROM attributes)
#SELECT * FROM attributes_sets_content WHERE attribute_id NOT IN (SELECT id FROM attributes)
#SELECT * FROM attributes_sets_content_cache WHERE attribute_id NOT IN (SELECT id FROM attributes)
#SELECT * FROM attributes_sets_rules WHERE attribute_set_id NOT IN (SELECT id FROM attributes_sets)
#SELECT * FROM attributes_sets_rules_cache WHERE attribute_set_id NOT IN (SELECT id FROM attributes_sets)
#SELECT * FROM attributes_sets_rules WHERE target_type='CAT' AND target_id > 0 AND target_id NOT IN (SELECT id FROM categories)
#SELECT * FROM attributes_sets_rules_cache WHERE target_type='CAT' AND target_id > 0 AND target_id NOT IN (SELECT id FROM categories)
#SELECT * FROM attributes_sets_rules WHERE target_type='BRA' AND target_id > 0 AND target_id NOT IN (SELECT id FROM brands)
#SELECT * FROM attributes_sets_rules_cache WHERE target_type='BRA' AND target_id > 0 AND target_id NOT IN (SELECT id FROM brands)
#SELECT * FROM attributes_sets_rules WHERE target_type='COL' AND target_id > 0 AND target_id NOT IN (SELECT id FROM collections)
#SELECT * FROM attributes_sets_rules_cache WHERE target_type='COL' AND target_id > 0 AND target_id NOT IN (SELECT id FROM collections)*/

    function getExec() {
        ini_set("max_execution_time", 0);
        $this->_DATA['categories'] = array();
        $this->_DATA['brands'] = array();
        $this->_style();
        //$this->_import_categories();
        //$this->_import_brands();
        //$this->_switch_attributes_ids();
        //$this->_import_attributes();
        $this->_rebind_attributes_ids();
        //$this->_import_attributes();
        //$this->_check_attributes_options();
        //$this->_import_attributes_options();
        //$this->_check_attributes_options_select();
        //$this->_import_attributes_options_select();
        //$this->_import_attributes_special();
        //$this->_import_products();
        //$this->_adjust_import_attributes_ids();
        //$this->_bind_attributes_to_products();
        //$this->explain_product_import(9236);
        //$this->_adjust_products();
        //$this->_save_images();
        //$this->_adjust_images();
        //$this->_adjust_brands();
        //$this->_import_collections_hiphop();
        //$this->_import_collections_breil();
        //$this->_import_collections_freestyle();
        //$this->_bind_collections_to_products();
        
        //$this->_import_attributes_notaligned();
        //$this->_cleanup_attributes();
        //$this->_bind_meta_to_products();
        //$this->_import_ean();
        $this->_import_seo();
    }

    function getTest($id) {
        $this->explain_product_import($id);
    }

    private function _style() {
        $style = <<<STYLE
<style>
    body {
        background-color:#000;
        font-family: Courier;
        font-size:12px;
        color:white;
    }

    .error{
        color:red;
    }

    .success{
        color:lime;
    }

    .message{
        color:cyan;
    }

    .warning{
        color:yellow;
    }

    .st1{
        color:magenta;
    }

    .st2{
        color:orange;
    }
</style>          
STYLE;
        echo $style;
    }

    private function _set_attributes_slug() {
        $query = "SELECT * FROM attributes_lang";
        $rows = DB::select($query);

        foreach ($rows as $row) {
            $slug = \Str::slug($row->name);
            //$query = "UPDATE attributes_lang SET slug=? WHERE attribute_id=? AND lang_id=?";
            DB::table("attributes_lang")->where("attribute_id", $row->attribute_id)->where("lang_id", $row->lang_id)->update(["slug" => $slug]);
        }
    }

    private function _switch_attributes_ids() {
        DB::statement("UPDATE attributes SET id=id+1000");
        DB::statement("UPDATE attributes_options SET attribute_id=attribute_id+1000");
        DB::statement("UPDATE attributes_lang SET attribute_id=attribute_id+1000");
        DB::statement("UPDATE attributes_sets_content_cache SET attribute_id=attribute_id+1000");
        DB::statement("UPDATE attributes_sets_content SET attribute_id=attribute_id+1000");
        DB::statement("UPDATE products_combinations_attributes SET attribute_id=attribute_id+1000");
        DB::statement("UPDATE products_attributes_position SET attribute_id=attribute_id+1000");
        DB::statement("UPDATE products_attributes SET attribute_id=attribute_id+1000");
    }

    private function _adjust_import_attributes_ids() {
        $ids = array(1030, 1031, 1046, 1059, 1062, 1068, 1088, 1160, 1161);
        $start = 214;
        foreach ($ids as $id) {
            DB::statement("UPDATE attributes SET id=? WHERE id=?", [$start, $id]);
            DB::statement("UPDATE attributes_options SET attribute_id=? WHERE attribute_id=?", [$start, $id]);
            DB::statement("UPDATE attributes_lang SET attribute_id=? WHERE attribute_id=?", [$start, $id]);
            DB::statement("UPDATE attributes_sets_content_cache SET attribute_id=? WHERE attribute_id=?", [$start, $id]);
            DB::statement("UPDATE attributes_sets_content SET attribute_id=? WHERE attribute_id=?", [$start, $id]);

            $start++;
        }
    }

    private function _rebind_attributes_ids() {
        \Utils::watch();
        $query = "SELECT * FROM import_attributes WHERE is_option=0";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            DB::statement("UPDATE attributes SET id=? WHERE id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE attributes_options SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE attributes_lang SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE attributes_sets_content_cache SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE attributes_sets_content SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE products_combinations_attributes SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE products_attributes_position SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
            DB::statement("UPDATE products_attributes SET attribute_id=? WHERE attribute_id=?", [$row->id, $row->rel]);
        }
    }

    private function __get_brand($brand) {
        $b = Str::slug($brand);
        if (isset($this->_DATA["brands"][$b])) {
            return $this->_DATA["brands"][$b];
        }
        $brand_id = DB::table("brands_lang")->where("slug", $b)->pluck("brand_id");
        if ((int) $brand_id == 0) {
            Writer::nl("Warning: Brand $brand NOT FOUND", "warning");
        }
        $this->_DATA["brands"][$b] = $brand_id;
        return (int) $brand_id;
    }

    private function __get_category($category) {
        $b = Str::slug($category);
        if (isset($this->_DATA["categories"][$b])) {
            return $this->_DATA["categories"][$b];
        }
        $category_id = DB::table("categories_lang")->where("slug", $b)->pluck("category_id");
        if ((int) $category_id == 0) {
            Writer::nl("Warning: Category $category NOT FOUND", "warning");
        }
        $this->_DATA["categories"][$b] = $category_id;
        return (int) $category_id;
    }
    
    
    private function _import_ean(){
        $query = "SELECT * FROM import_ean";
        $rows = DB::select($query);
        $content = '';
        foreach($rows as $row){
            $ean = trim($row->EAN);
            $sku = trim($row->SKU);
            $content .= "UPDATE products SET ean13='$ean' WHERE sku='$sku';".PHP_EOL;
        }
        File::put("C:/wamp/www/l4/sources/import_ean.sql", $content);
    }
    
    private function _import_seo(){
        $query = "SELECT * FROM import_seo WHERE lingua='it'";
        //$query = "SELECT * FROM import_seo WHERE lingua='it' LIMIT 0,10";
        $rows = DB::select($query);
        $content = '';
        foreach($rows as $row){
            $data = [
                "category_id" => $row->categoria_id,
                "brand_id" => $row->marca_id,
                "created_at" => date("Y-m-d H:i:s"),
                "updated_at" => date("Y-m-d H:i:s"),
                "created_by" => 2,
                "updated_by" => 2,
                ];
            
            $id = DB::table("seo_texts")->insertGetId($data);
            
            $data_it = [
                "seo_text_id" => $id,
                "lang_id" => "it",
                "published" => 1,
                "content" => $row->testo,
                "metatitle" => $row->pagetitle,
                "h1" => $row->pageh1,
                "metadescription" => $row->pagedes,
            ];
            
            DB::table("seo_texts_lang")->insert($data_it);
            
            $data_en = [
                "seo_text_id" => $id,
                "lang_id" => "en",
                "published" => 1,
                "content" => "",
                "metatitle" => "",
                "h1" => "",
                "metadescription" => "",
            ];
            
            DB::table("seo_texts_lang")->insert($data_en);
            
            if($row->genere_id > 0){
                
                switch ($row->genere_id) {
                    case 1:
                        $target_id = 24;
                        break;
                    case 2:
                        $target_id = 25;
                        break;
                    case 3:
                        $target_id = 27;
                        break;

                    default:
                        break;
                }
                
                $rule = [
                  "target_type" => "ATT",
                  "target_mode" => "+",
                  "target_id" => $target_id,
                  "seo_text_id" => $id,
                  "position" => 1,
                ];
                DB::table("seo_texts_rules")->insert($rule);
            }
            
        }
    }

    private function _import_attributes_notaligned() {

        $attribute_to_rebind = 7;
        $query = "SELECT * FROM import_attributes WHERE id=$attribute_to_rebind";
        $import_attribute = DB::selectOne($query);

        $query = "SELECT * FROM import_attributes_xref WHERE attributi_id LIKE '%|$attribute_to_rebind|%' ORDER BY prodotto_id";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $attributes = $this->arrayfy($row->attributi_id);
            $options = $this->arrayfy($row->valoriattributi_id);
            for ($i = 0; $i < count($attributes); $i++) {
                $a = $attributes[$i];
                $o = $options[$i];
                if ($a == $attribute_to_rebind) {
                    $query = "SELECT * FROM import_attributes_val WHERE id=$o";
                    $import_option = DB::selectOne($query);
                    echo '<pre>';
                    print_r($import_option);
                    echo '</pre>';
                    $slug = \Str::slug($import_option->nomeit);
                    $query = "SELECT * FROM attributes_options_lang WHERE lang_id='it' AND slug='$slug' AND attribute_option_id IN (SELECT id FROM attributes_options WHERE attribute_id=$import_attribute->rel)";
                    $option = DB::selectOne($query);
                    echo '<pre>';
                    print_r($option);
                    echo '</pre>';
                    
                    $query = "INSERT INTO products_attributes(product_id,attribute_id,attribute_val) VALUES($row->prodotto_id,$import_attribute->rel,$option->attribute_option_id)";
                    DB::statement($query);
                    Writer::nl($query,"success");
                    $y = $i + 3;
                    $query = "INSERT INTO products_attributes_position(product_id,attribute_id,position) VALUES($row->prodotto_id,$import_attribute->rel,$y)";
                    Writer::nl($query,"success");
                    DB::statement($query);
                }
            }
        }

        //option_val - adjusted_option_val
        //1048 - 388 (GPS)
        //1048 - 388 (GPS)
        //SELECT * FROM products_attributes WHERE attribute_val NOT IN (SELECT id FROM attributes_options) AND attribute_id IN (SELECT id FROM attributes WHERE frontend_input IN ('multiselect','select'))
        //UPDATE products_attributes SET attribute_val = 388 WHERE attribute_val = 1048;
    }
    
    private function _cleanup_attributes(){
        $query = "SELECT * FROM products_attributes WHERE attribute_val = 994"; // Pietre:sì
        $rows = DB::select($query);
        $general_attribute_id = 219;
        foreach($rows as $row){
            $query = "DELETE FROM products_attributes WHERE product_id=$row->product_id AND attribute_id=$general_attribute_id";
            Writer::nl($query);
            DB::statement($query);
            $query = "DELETE FROM products_attributes_position WHERE product_id=$row->product_id AND attribute_id=$general_attribute_id";
            Writer::nl($query);
            DB::statement($query);
        }
        
        $query = "SELECT * FROM products_attributes WHERE attribute_val NOT IN (SELECT id FROM attributes_options) AND attribute_id IN (SELECT id FROM attributes WHERE frontend_input IN ('multiselect','select')) ORDER BY attribute_val";
        $rows = DB::select($query);
        foreach($rows as $row){
            $query = "DELETE FROM products_attributes WHERE product_id=$row->product_id AND attribute_id=$row->attribute_id AND attribute_val=$row->attribute_val";
            Writer::nl($query);
            DB::statement($query);
            $query = "DELETE FROM products_attributes_position WHERE product_id=$row->product_id AND attribute_id=$row->attribute_id";
            Writer::nl($query);
            DB::statement($query);
        }
    }

    private function _adjust_and_check_products() {
        //falso positivo
        return;
        $debug = false;
        $query = "SELECT P.*,X.categorie_id FROM import_products P LEFT JOIN import_attributes_xref X ON P.id=X.prodotto_id WHERE id NOT IN (SELECT id FROM products)";
        $query = "SELECT P.*,X.categorie_id FROM import_products P LEFT JOIN import_attributes_xref X ON P.id=X.prodotto_id WHERE id = 9452";
        $rows = DB::select($query);

        foreach ($rows as $row) {
            $id = $row->id;
            $pattern = $row->categorie_id;
            $pattern = ltrim($pattern, "|");
            $pattern = rtrim($pattern, "|");
            $extra_categories = explode("|", $pattern);

            if (count($extra_categories) > 0) {
                Writer::nl("EXTRA CATEGORIES", "message");
                foreach ($extra_categories as $cat) {
                    $data_category = ["category_id" => $cat, "product_id" => $id];
                    echo '<pre>';
                    print_r($data_category);
                    echo '</pre>';

                    if (!$debug) {
                        try {
                            $bool = DB::table("categories_products")->insert($data_category);
                            ($bool) ? Writer::nl("categories_products succesfully inserted", "success") : Writer::nl("ERROR - categories_products failed!", "error");
                        } catch (Exception $ex) {
                            
                        }
                    }
                }
            }
        }
    }

    private function _import_products() {

        $categories_direct = false;
        $debug = false;

        $query = "SELECT * FROM import_products";
        $query = "SELECT * FROM import_products LIMIT 0,500";
        $query = "SELECT * FROM import_products LIMIT 500,500";
        $query = "SELECT * FROM import_products LIMIT 1000,500";
        $query = "SELECT * FROM import_products LIMIT 1500,500";
        $query = "SELECT * FROM import_products LIMIT 2000,500";
        $query = "SELECT * FROM import_products LIMIT 2500,5000";
        $query = "SELECT * FROM import_products LIMIT 7500,5000";
        $query = "SELECT * FROM import_products WHERE id NOT IN (SELECT id FROM products)";
        $query = "SELECT P.*,X.categorie_id FROM import_products P LEFT JOIN import_attributes_xref X ON P.id=X.prodotto_id WHERE id NOT IN (SELECT id FROM products)";
        $rows = DB::select($query);

        $counter = 0;




        $now = date("Y-m-d H:i:s");

        foreach ($rows as $row) {
            $counter++;

            $extra_categories = array();

            if ($categories_direct === false) {
                $pattern = $row->categorie_id;
                $pattern = ltrim($pattern, "|");
                $pattern = rtrim($pattern, "|");
                $tokens = explode("|", $pattern);

                try {
                    $categoria = $tokens[1];
                    $categoria_padre = $tokens[0];

                    if (count($tokens) > 2) {
                        $extra_categories = array_slice($tokens, 2);
                    }
                } catch (Exception $ex) {
                    Writer::nl("INTERNAL ERROR: [$row->id] " . $ex->getMessage(), "error");
                }
            } else {
                $categoria = $this->__get_category($row->categoria);
                $categoria_padre = $this->__get_category($row->categoria_padre);
            }

            $id = $row->id;
            $sku = trim($row->sku);
            $marca = $this->__get_brand($row->marca);

            $peso = round((float) $row->peso, 2);
            $novita = abs($row->novita);
            $prezzobase = round((float) $row->prezzobase, 3);
            $prezzovendita = round((float) $row->prezzovendita, 3);
            $prezzofinale = round((float) $row->prezzofinale, 3);
            $prezzofinale_tasse = round((float) $row->prezzofinale * 1.21, 3);
            $giacenza = $row->giacenza;
            $outlet = abs($row->outlet);
            $fuoriproduzione = abs($row->fuoriproduzione);
            $offerta = abs($row->offerta);
            $posizione = $row->posizione;

            $nomeit = trim($row->nomeit);
            $slugit = Str::slug($nomeit);
            $attivo = abs($row->attivo);
            $descrizionebreveit = trim($row->descrizionebreveit);
            $descrizioneit = trim($row->descrizioneit);
            $vecchio_seo_title = trim($row->vecchio_seo_title);
            $pageh1it = trim($row->pageh1it);
            $vecchio_seo_description = trim($row->vecchio_seo_description);
            $pagekeyit = trim($row->pagekeyit);

            $nomeen = trim($row->nomeen);
            $slugen = Str::slug($nomeen);
            $attivo = abs($row->attivo);
            $descrizionebreveen = trim($row->descrizionebreveen);
            $descrizioneen = trim($row->descrizioneen);
            $vecchio_seo_titleen = trim($row->vecchio_seo_titleen);
            $pageh1en = trim($row->pageh1en);
            $vecchio_seo_descriptionen = trim($row->vecchio_seo_descriptionen);
            $pagekeyen = trim($row->pagekeyen);


            if ($marca > 0 AND $categoria > 0 AND $categoria_padre > 0) {


                $data = [
                    "id" => $id,
                    "type" => "default",
                    "sku" => $sku,
                    "brand_id" => $marca,
                    "collection_id" => null,
                    "parent_id" => "0",
                    "default_category_id" => $categoria,
                    "main_category_id" => $categoria_padre,
                    "weight" => $peso,
                    "depth" => null,
                    "width" => null,
                    "height" => null,
                    "condition" => "N",
                    "is_new" => $novita,
                    "new_from_date" => null,
                    "new_to_date" => null,
                    "ean13" => null,
                    "upc" => null,
                    "buy_price" => $prezzobase,
                    "sell_price" => $prezzovendita,
                    "price_nt" => $prezzofinale,
                    "price" => $prezzofinale_tasse,
                    "unit_price" => null,
                    "unity" => null,
                    "show_price" => "1",
                    "tax_group_id" => "1",
                    "additional_shipping_cost" => "0.00",
                    "indexed" => "1",
                    "feed" => "1",
                    "gift_enabled" => "0",
                    "visibility" => "both",
                    "manage_stock" => "1",
                    "use_config_manage_stock" => "1",
                    "original_inventory_qty" => $giacenza,
                    "qty" => $giacenza,
                    "min_qty" => "1",
                    "use_config_min_qty" => "1",
                    "min_sale_qty" => "1",
                    "use_config_min_sale_qty" => "1",
                    "max_sale_qty" => "0",
                    "use_config_max_sale_qty" => "1",
                    "backorders" => "default",
                    "notify_stock" => "default",
                    "notify_stock_qty" => "2",
                    "qty_increments" => "default",
                    "is_available" => "1",
                    "availability_days" => "0",
                    "available_from_date" => null,
                    "available_to_date" => null,
                    "is_outlet" => $outlet,
                    "is_out_of_production" => $fuoriproduzione,
                    "is_in_promotion" => $offerta,
                    "redirect_type" => "",
                    "redirect_product_id" => null,
                    "robots" => "global",
                    "ogp" => "0",
                    "ogp_type" => "product",
                    "public_access" => "P",
                    "position" => $posizione,
                    "created_by" => 2,
                    "updated_by" => 2,
                    "created_at" => $now,
                    "updated_at" => $now,
                ];

                echo '<pre>';
                print_r($data);
                echo '</pre>';

                if (!$debug) {
                    $bool = DB::table("products")->insert($data);
                    ($bool) ? Writer::nl("Product [$id] succesfully inserted", "success") : Writer::nl("ERROR - Product id [$id] failed!", "error");
                }

                $data_it = [
                    "product_id" => $id,
                    "lang_id" => "it",
                    "name" => $nomeit,
                    "slug" => $slugit,
                    "published" => $attivo,
                    "sdesc" => $descrizionebreveit,
                    "ldesc" => $descrizioneit,
                    "metatitle" => $vecchio_seo_title,
                    "h1" => $pageh1it,
                    "metadescription" => $vecchio_seo_description,
                    "metakeywords" => $pagekeyit,
                ];

                echo '<pre>';
                print_r($data_it);
                echo '</pre>';

                if (!$debug) {
                    $bool = DB::table("products_lang")->insert($data_it);
                    ($bool) ? Writer::nl("Product Lang IT [$id] succesfully inserted", "success") : Writer::nl("ERROR - Product Lang IT id [$id] failed!", "error");
                }


                $data_en = [
                    "product_id" => $id,
                    "lang_id" => "en",
                    "name" => $nomeen,
                    "slug" => $slugen,
                    "published" => $attivo,
                    "sdesc" => $descrizionebreveen,
                    "ldesc" => $descrizioneen,
                    "metatitle" => $vecchio_seo_titleen,
                    "h1" => $pageh1en,
                    "metadescription" => $vecchio_seo_descriptionen,
                    "metakeywords" => $pagekeyen,
                ];

                echo '<pre>';
                print_r($data_en);
                echo '</pre>';

                if (!$debug) {
                    $bool = DB::table("products_lang")->insert($data_en);
                    ($bool) ? Writer::nl("Product Lang EN [$id] succesfully inserted", "success") : Writer::nl("ERROR - Product Lang EN id [$id] failed!", "error");
                }


                $data_category = ["category_id" => $categoria, "product_id" => $id];
                echo '<pre>';
                print_r($data_category);
                echo '</pre>';

                if (!$debug) {
                    try {
                        $bool = DB::table("categories_products")->insert($data_category);
                        ($bool) ? Writer::nl("categories_products succesfully inserted", "success") : Writer::nl("ERROR - categories_products failed!", "error");
                    } catch (Exception $ex) {
                        
                    }
                }

                if (count($extra_categories) > 0) {
                    Writer::nl("EXTRA CATEGORIES", "message");
                    foreach ($extra_categories as $cat) {
                        $data_category = ["category_id" => $cat, "product_id" => $id];
                        echo '<pre>';
                        print_r($data_category);
                        echo '</pre>';

                        if (!$debug) {
                            try {
                                $bool = DB::table("categories_products")->insert($data_category);
                                ($bool) ? Writer::nl("categories_products succesfully inserted", "success") : Writer::nl("ERROR - categories_products failed!", "error");
                            } catch (Exception $ex) {
                                
                            }
                        }
                    }
                }
            } else {
                Writer::nl("Product [$id] skipped => Marca: $marca | Categoria: $categoria | Categoria padre: $categoria_padre ", "warning");
            }
        }
    }

    private function arrayfy($str) {
        $str = ltrim($str, "|");
        $str = rtrim($str, "|");
        return explode("|", $str);
    }

    private function __get_platform_attribute($id) {
        if (isset($this->_ATTRIBUTES[$id])) {
            return $this->_ATTRIBUTES[$id];
        }
        $query = "SELECT * FROM attributes WHERE id=$id";
        //Writer::nl($query);
        $obj = DB::selectOne($query);
        if (is_object($obj))
            $this->_ATTRIBUTES[$id] = $obj;
        return $obj;
    }

    private function __get_platform_option($id) {
        if (isset($this->_ATTRIBUTES_OPTIONS[$id])) {
            return $this->_ATTRIBUTES_OPTIONS[$id];
        }
        $query = "SELECT * FROM attributes_options WHERE id=$id";
        $obj = DB::selectOne($query);
        if (is_object($obj))
            $this->_ATTRIBUTES_OPTIONS[$id] = $obj;
        return $obj;
    }

    private function __get_platform_option_by_import($id) {
        if (isset($this->_ATTRIBUTES_OPTIONS["rel-" . $id])) {
            return $this->_ATTRIBUTES_OPTIONS["rel-" . $id];
        }
        $query = "SELECT * FROM attributes_options WHERE id IN (SELECT attribute_option_id FROM import_attributes_options WHERE import_id=$id)";
        $obj = DB::select($query);
        if (count($obj))
            $this->_ATTRIBUTES_OPTIONS["rel-" . $id] = $obj;
        return $obj;
    }

    private function __get_import_attribute($id) {
        if (isset($this->_IMPORT_ATTRIBUTES[$id])) {
            return $this->_IMPORT_ATTRIBUTES[$id];
        }
        $query = "SELECT * FROM import_attributes WHERE id=$id";
        $obj = DB::selectOne($query);
        $this->_IMPORT_ATTRIBUTES[$id] = $obj;
        return $obj;
    }

    private function __get_import_option($id) {
        if (isset($this->_IMPORT_ATTRIBUTES_OPTIONS[$id])) {
            return $this->_IMPORT_ATTRIBUTES_OPTIONS[$id];
        }
        $query = "SELECT * FROM import_attributes_val WHERE id=$id";
        $obj = DB::selectOne($query);
        $this->_IMPORT_ATTRIBUTES_OPTIONS[$id] = $obj;
        return $obj;
    }

    private function _bind_attributes_to_products() {
        $page = (int) \Input::get("page", 1);
        $pagesize = 500;

        $start = ($page - 1) * $pagesize;

        $query = "SELECT * FROM import_attributes_xref WHERE attributi_id<>'' ORDER BY prodotto_id LIMIT $start,$pagesize";
        //$query = "SELECT * FROM import_attributes_xref WHERE attributi_id<>'' AND prodotto_id=9236 ORDER BY prodotto_id";
        //$query = "SELECT * FROM import_attributes_xref WHERE attributi_id<>'' ORDER BY prodotto_id LIMIT 0,10";
        $rows = DB::select($query);

        define("SPECIAL_PACKAGE_SINGLE_OPTION", 34);
        define("SPECIAL_GUARANTEE_LITETIME_OPTION", 993);
        define("SPECIAL_GUARANTEE_STANDARD_OPTION", 35);
        define("SPECIAL_GUARANTEE_2YEARS_OPTION", 36);
        $_ERRORS = array();

        foreach ($rows as $row) {
            $id = $row->prodotto_id;
            $attributes = $this->arrayfy($row->attributi_id);
            $attributes_options = $this->arrayfy($row->valoriattributi_id);
            Writer::nl("<====== PRODUCT ID: $id ======>", "success");
            Writer::nl("ATTRIBUTES", "st1");
            echo '<pre>';
            print_r($attributes);
            echo '</pre>';
            Writer::nl("ATTRIBUTES OPTIONS", "st2");
            echo '<pre>';
            print_r($attributes_options);
            echo '</pre>';

            for ($index = 0; $index < count($attributes); $index++) {

                $attribute_id = $attributes[$index];
                $value = $attributes_options[$index];

                Writer::nl("<====== HANDLING ATTRIBUTE ID: $attribute_id ======>", "message");

                $attribute_obj = $this->__get_platform_attribute($attribute_id);
                $import_attribute_obj = $this->__get_import_attribute($attribute_id);

                $option_obj = $this->__get_platform_option_by_import($value);
                $import_option_obj = $this->__get_import_option($value);



                $is_option = FALSE;
                $has_error = FALSE;
                $has_multi_option = FALSE;

                if ($import_attribute_obj->is_option == 1) {
                    $is_option = TRUE;
                    $attribute_obj = $this->__get_platform_attribute($import_attribute_obj->rel);
                }


                Writer::nl("ATTRIBUTE PLATFORM OBJ", "warning");
                echo '<pre>';
                print_r($attribute_obj);
                echo '</pre>';
                if (!is_object($attribute_obj)) {
                    Writer::nl("WARNING: ATTRIBUTE PLATFORM OBJ IS UNDEFINED", "error");
                    $has_error = TRUE;
                } else {
                    $attribute_id = $attribute_obj->id;
                }

                Writer::nl("ATTRIBUTE IMPORT OBJ", "warning");
                echo '<pre>';
                print_r($import_attribute_obj);
                echo '</pre>';


                Writer::nl("ATTRIBUTE OPTION PLATFORM OBJ", "warning");
                echo '<pre>';
                print_r($option_obj);
                echo '</pre>';
                if (!is_array($option_obj)) {
                    Writer::nl("WARNING: ATTRIBUTE OPTION PLATFORM ARRAY IS UNDEFINED", "error");
                    $has_error = TRUE;
                } else {
                    if (count($option_obj) == 1) {
                        $option_obj = $option_obj[0];
                        $value = $option_obj->id;
                    } else {
                        if (count($option_obj) > 1) {
                            $has_multi_option = TRUE;
                        }
                    }
                }

                Writer::nl("ATTRIBUTE OPTION IMPORT OBJ", "warning");
                echo '<pre>';
                print_r($import_option_obj);
                echo '</pre>';


                if ($attribute_obj->frontend_input == 'select' OR $attribute_obj == 'multiselect') {
                    switch ($attribute_obj->code) {
                        case 'package': //package are always binded to the same option
                            $value = SPECIAL_PACKAGE_SINGLE_OPTION;
                            Writer::nl("SPECIAL [package] - value: $value", "success");
                            $has_error = FALSE;
                            break;

                        case 'guarantee': //package are always binded to the same option
                            $val = \Str::lower($import_option_obj->nomeit);
                            $value = SPECIAL_GUARANTEE_STANDARD_OPTION;
                            if (\Str::contains($val, "a vita")) {
                                $value = SPECIAL_GUARANTEE_LITETIME_OPTION;
                            }
                            if (\Str::contains($val, "2 anni")) {
                                $value = SPECIAL_GUARANTEE_2YEARS_OPTION;
                            }
                            Writer::nl("SPECIAL [guarantee] - value: $value", "success");
                            $has_error = FALSE;
                            break;

                        default:
                            break;
                    }
                } else { //handle boolean or text input type
                    if ($attribute_obj->frontend_input == 'boolean') {
                        $value = ( \Str::lower($import_option_obj->nomeen) == 'yes') ? 1 : 0;
                        $has_error = FALSE;
                    }
                    if ($attribute_obj->frontend_input == 'text') {
                        $value = \Str::lower($import_option_obj->nomeit);
                        $value = rtrim($value, "atm");
                        $value = rtrim($value, "mm");
                        $value = rtrim($value, "cm");
                        $value = rtrim($value, "m");
                        $value = trim($value);
                        $has_error = FALSE;
                    }
                }


                if ($has_multi_option) {
                    Writer::nl("MULTI OPTION", "message");

                    foreach ($option_obj as $tmp) {
                        $data_attributes = array("product_id" => $id, "attribute_id" => $attribute_id, "attribute_val" => $tmp->id);

                        Writer::nl("ATTRIBUTES INSERT", "st1");
                        echo '<pre>';
                        print_r($data_attributes);
                        echo '</pre>';

                        DB::table("products_attributes")->insert($data_attributes);
                    }
                } else {
                    $data_attributes = array("product_id" => $id, "attribute_id" => $attribute_id, "attribute_val" => $value);

                    Writer::nl("ATTRIBUTES INSERT", "st1");
                    echo '<pre>';
                    print_r($data_attributes);
                    echo '</pre>';

                    DB::table("products_attributes")->insert($data_attributes);
                }




                $query = "SELECT * FROM products_attributes_position WHERE product_id=? AND attribute_id=?";
                $check = DB::selectOne($query, array($id, $attribute_id));
                if (!is_object($check)) {
                    $data_attributes_position = array("product_id" => $id, "attribute_id" => $attribute_id, "position" => $index + 1);
                    Writer::nl("ATTRIBUTES POSITION INSERT", "st2");
                    echo '<pre>';
                    print_r($data_attributes_position);
                    echo '</pre>';
                    DB::table("products_attributes_position")->insert($data_attributes_position);
                }

                if ($has_error) {
                    $e = new stdClass;
                    $e->attribute_obj = $attribute_obj;
                    $e->import_attribute_obj = $import_attribute_obj;
                    $e->option_obj = $option_obj;
                    $e->import_option_obj = $import_option_obj;
                    $_ERRORS[] = $e;
                }

                Writer::nl("<====== EOF HANDLING ATTRIBUTE ID: $attribute_id ======>", "message");
            }
            echo '<hr>';
        }

        if (count($_ERRORS) > 0) {
            Writer::nl("ERRORS FOUNDED");
            echo '<pre>';
            print_r($_ERRORS);
            echo '</pre>';
            \Utils::log($_ERRORS, "ERRORS FOUNDED");
        }
    }

    private function __get_special_option($option_it, $option_en, $attribute_id) {
        $it_val = \Str::slug($option_it);
        $en_val = \Str::slug($option_en);

        Writer::nl("Getting attribute value: $it_val | $en_val [attribute:$attribute_id]", "st1");

        $query = "SELECT L.*, O.attribute_id "
                . "FROM attributes_options_lang L "
                . "LEFT JOIN attributes_options O ON L.attribute_option_id=O.id "
                . "WHERE ((L.lang_id='it' AND L.slug=?) OR (L.lang_id='en' AND L.slug=?) OR (O.uslug=?)) AND O.attribute_id=?";
        $opt = DB::selectOne($query, [$it_val, $en_val, $en_val, $attribute_id]);
        echo '<pre>';
        print_r($opt);
        echo '</pre>';

        return $opt;
    }

    private function _adjust_brands() {
        $query = "SELECT * FROM brands_lang";
        $rows = DB::select($query);

        $root = public_path();
        foreach ($rows as $row) {
            $filename = $root . $row->image_default;
            if (\File::exists($filename)) {
                Writer::nl($filename, 'success');
            } else {
                Writer::nl($filename, 'error');
            }
        }
    }

    private function _bind_collections_to_products() {
        $query = "SELECT * FROM collections_lang WHERE lang_id='it' ORDER BY name";
        $query = "SELECT L.*,C.brand_id,C.category_id FROM collections_lang L LEFT JOIN collections C ON L.collection_id=C.id WHERE lang_id='it' ORDER BY name";
        $rows = DB::select($query);

        foreach ($rows as $row) {
            Writer::nl("Try to find [$row->brand_id,$row->name]");

            $filter = ($row->category_id == 0) ? "" : "AND L.product_id IN (SELECT product_id FROM categories_products WHERE category_id=$row->category_id)";
            if ($row->name == 'Milano') {
                $row->name = 'Collezione Milano';
            }
            if ($row->name == 'Link') {
                $row->name = 'Link Lady';
            }

            $query = "SELECT L.* FROM products_lang L LEFT JOIN products P ON L.product_id=P.id WHERE P.brand_id=$row->brand_id AND name LIKE '%$row->name%' AND lang_id='it' $filter";
            Writer::nl($query, "st1");
            $products = DB::select($query);
            if (count($products) > 0) {
                foreach ($products as $obj) {
                    Writer::nl("Product founded: $obj->name [$obj->product_id]", "success");
                    DB::table("products")->where("id", $obj->product_id)->update(['collection_id' => $row->collection_id]);
                }
            }
        }
    }

    private function _bind_meta_to_products() {
        $page = (int) \Input::get("page", 1);
        $pagesize = 500;

        $start = ($page - 1) * $pagesize;
        $query = "SELECT id FROM products ORDER BY id DESC LIMIT $start,$pagesize";
        $rows = DB::select($query);

        foreach ($rows as $row) {
            Writer::nl("Try to bind [$row->id]");
            $model = \Product::find($row->id);
            $this->handleMetadata($model);
        }
    }

    private function handleMetadata($model) {
        $id = $model->id;

        $lang = \Core::getLang();

        $languages = \Mainframe::languagesCodes();

        $token = new Token("");
        $token->set("sku", $model->sku);
        $token->set("brand_id", $model->brand_id);
        $token->set("collection_id", $model->collection_id);
        $token->set("category_id", $model->main_category_id);
        $token->set("default_category_id", $model->default_category_id);

        $md = Metadata::getInstance();

        $query = "SELECT attribute_id FROM products_attributes_position WHERE product_id=$id ORDER BY position";
        $rows = DB::select($query);
        Utils::log($query);
        foreach ($languages as $lang) {
            $attributes = array();
            foreach ($rows as $row) {
                $ah = new AttributeHtml($row->attribute_id, $lang);
                $ah->setProductValue($id);
                $meta = $ah->getMeta();
                $token->rebind($meta->value);
                $meta->value = $token->render();
                $string = $meta->label . " " . $meta->value;
                $md->index($string);
                $attributes[] = $meta;
            }
            $md->set("attributes", $attributes);
            $md->set("fields", $md->getLang($lang, "fields"));
            $md->update($model, $lang);
        }
    }

    private function _import_collections_hiphop() {
        $table = 'import_collections_hh';
        $query = "SELECT DISTINCT(id) FROM $table";
        $rows = DB::select($query);

        $brand_id = 26;
        $counter = 0;
        foreach ($rows as $row) {
            $query = "SELECT * FROM $table WHERE id=$row->id AND language_id='it'";
            $it_obj = DB::selectOne($query);
            $query = "SELECT * FROM $table WHERE id=$row->id AND language_id='en'";
            $en_obj = DB::selectOne($query);

            $counter++;
            $now = date("Y-m-d H:i:s");

            $data = [
                "brand_id" => $brand_id,
                "ogp" => 0,
                "created_by" => 2,
                "updated_by" => 2,
                "created_at" => $now,
                "updated_at" => $now,
                "position" => $counter,
            ];

            $insert_id = DB::table("collections")->insertGetId($data);

            Writer::nl("Inserted collection: $insert_id");

            $data_lang = [
                "collection_id" => $insert_id,
                "lang_id" => "it",
                "name" => ucwords($it_obj->name),
                "slug" => \Str::slug($it_obj->name),
                "metatitle" => ucwords($it_obj->name),
                "ldesc" => "<p>" . ucfirst($it_obj->description) . "</p>",
            ];

            DB::table("collections_lang")->insert($data_lang);

            Writer::nl("Inserted collections_lang: IT $insert_id");

            $data_lang = [
                "collection_id" => $insert_id,
                "lang_id" => "en",
                "name" => ucwords($en_obj->name),
                "slug" => \Str::slug($en_obj->name),
                "metatitle" => ucwords($en_obj->name),
                "ldesc" => "<p>" . ucfirst($en_obj->description) . "</p>",
            ];

            DB::table("collections_lang")->insert($data_lang);

            Writer::nl("Inserted collections_lang: EN $insert_id");
        }
    }

    private function _import_collections_freestyle() {
        $table = 'import_collections_freestyle';
        $query = "SELECT DISTINCT(id) FROM $table";
        $rows = DB::select($query);

        $brand_id = 67;
        $counter = 0;
        foreach ($rows as $row) {
            $query = "SELECT * FROM $table WHERE id=$row->id AND language_id='it'";
            $it_obj = DB::selectOne($query);

            $en_obj = $it_obj;

            $counter++;
            $now = date("Y-m-d H:i:s");

            $data = [
                "brand_id" => $brand_id,
                "ogp" => 0,
                "created_by" => 2,
                "updated_by" => 2,
                "created_at" => $now,
                "updated_at" => $now,
                "position" => $counter,
            ];

            $insert_id = DB::table("collections")->insertGetId($data);

            Writer::nl("Inserted collection: $insert_id");

            $data_lang = [
                "collection_id" => $insert_id,
                "lang_id" => "it",
                "name" => ucwords($it_obj->name),
                "slug" => \Str::slug($it_obj->name),
                "metatitle" => ucwords($it_obj->name)
            ];

            DB::table("collections_lang")->insert($data_lang);

            Writer::nl("Inserted collections_lang: IT $insert_id");

            $data_lang = [
                "collection_id" => $insert_id,
                "lang_id" => "en",
                "name" => ucwords($en_obj->name),
                "slug" => \Str::slug($en_obj->name),
                "metatitle" => ucwords($en_obj->name)
            ];

            DB::table("collections_lang")->insert($data_lang);

            Writer::nl("Inserted collections_lang: EN $insert_id");
        }
    }

    private function _import_collections_breil() {
        $table = 'import_collections_breil';
        $query = "SELECT DISTINCT(id) FROM $table WHERE published=1 AND language_id NOT IN ('es')";
        $rows = DB::select($query);


        $counter = 0;
        foreach ($rows as $row) {


            $query = "SELECT * FROM $table WHERE id=$row->id AND language_id='it'";
            Utils::log($query);
            $it_obj = DB::selectOne($query);
            $query = "SELECT * FROM $table WHERE id=$row->id AND language_id='en'";
            $en_obj = DB::selectOne($query);

            if (!isset($it_obj)) {
                $it_obj = $en_obj;
                $it_obj->description = '';
            }

            if (!isset($en_obj)) {
                $en_obj = $it_obj;
                $en_obj->description = '';
            }

            $counter++;
            $now = date("Y-m-d H:i:s");


            switch ($it_obj->brand_id) {
                case 'BR':
                    $brand_id = 3;
                    break;
                case 'BM':
                    $brand_id = 33;
                    break;
                case 'TR':
                    $brand_id = 4;
                    break;

                default:
                    break;
            }

            $data = [
                "brand_id" => $brand_id,
                "ogp" => 0,
                "created_by" => 2,
                "updated_by" => 2,
                "created_at" => $now,
                "updated_at" => $now,
                "position" => $counter,
            ];

            $insert_id = DB::table("collections")->insertGetId($data);

            Writer::nl("Inserted collection: $insert_id");
            $name = ucwords(\Str::lower($it_obj->name));
            $data_lang = [
                "collection_id" => $insert_id,
                "lang_id" => "it",
                "name" => $name,
                "slug" => \Str::slug($name),
                "metatitle" => $name,
                "ldesc" => "<p>" . ucfirst($it_obj->description) . "</p>",
            ];

            DB::table("collections_lang")->insert($data_lang);

            Writer::nl("Inserted collections_lang: IT $insert_id");
            $name = ucwords(\Str::lower($en_obj->name));
            $data_lang = [
                "collection_id" => $insert_id,
                "lang_id" => "en",
                "name" => $name,
                "slug" => \Str::slug($name),
                "metatitle" => $name,
                "ldesc" => "<p>" . ucfirst($en_obj->description) . "</p>",
            ];

            DB::table("collections_lang")->insert($data_lang);

            Writer::nl("Inserted collections_lang: EN $insert_id");
        }
    }

    private function _adjust_images() {
        $page = (int) \Input::get("page", 1);
        $pagesize = 1000;

        $start = ($page - 1) * $pagesize;
        $query = "SELECT * FROM images ORDER BY product_id,id LIMIT $start,$pagesize";
        //$query = "SELECT * FROM images LIMIT 0,50";
        $rows = DB::select($query);

        $pivot = 0;
        $cnt = 0;
        foreach ($rows as $row) {
            if ($row->product_id != $pivot) {
                $pivot = $row->product_id;
                $cnt = 0;
            }
            $cnt++;
            $cover = ($cnt == 1) ? 1 : 0;
            $query = "UPDATE images SET cover=$cover,position=$cnt WHERE id=$row->id";
            Writer::nl($query);
            DB::statement($query);
        }
    }

    private function __get_brand_obj($id) {
        if (isset($this->_DATA['brands_objs'][$id])) {
            return $this->_DATA['brands_objs'][$id];
        }
        $query = "SELECT * FROM brands WHERE id=$id";
        $obj = DB::selectOne($query);

        $query = "SELECT * FROM brands_lang WHERE lang_id='it' AND brand_id=$id";
        $obj_it = DB::selectOne($query);

        $query = "SELECT * FROM brands_lang WHERE lang_id='en' AND brand_id=$id";
        $obj_en = DB::selectOne($query);

        $obj->it = $obj_it;
        $obj->en = $obj_en;

        $this->_DATA['brands_objs'][$id] = $obj;
        return $obj;
    }

    private function __get_category_obj($id) {
        if (isset($this->_DATA['categories_objs'][$id])) {
            return $this->_DATA['categories_objs'][$id];
        }
        $query = "SELECT * FROM categories WHERE id=$id";
        $obj = DB::selectOne($query);

        $query = "SELECT * FROM categories_lang WHERE lang_id='it' AND category_id=$id";
        $obj_it = DB::selectOne($query);

        $query = "SELECT * FROM categories_lang WHERE lang_id='en' AND category_id=$id";
        $obj_en = DB::selectOne($query);

        $obj->it = $obj_it;
        $obj->en = $obj_en;

        $this->_DATA['categories_objs'][$id] = $obj;
        return $obj;
    }

    private function __get_product_obj($id) {
        $query = "SELECT * FROM products WHERE id=$id";
        $obj = DB::selectOne($query);

        $query = "SELECT * FROM products_lang WHERE lang_id='it' AND product_id=$id";
        $obj_it = DB::selectOne($query);

        $query = "SELECT * FROM products_lang WHERE lang_id='en' AND product_id=$id";
        $obj_en = DB::selectOne($query);

        $obj->it = $obj_it;
        $obj->en = $obj_en;

        $obj->main_category = $this->__get_category_obj($obj->main_category_id);
        $obj->default_category = $this->__get_category_obj($obj->default_category_id);

        $obj->categories = array();
        $query = "SELECT * FROM categories_products WHERE product_id=$obj->id";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $obj->categories[] = $this->__get_category_obj($row->category_id);
        }

        $obj->brand = $this->__get_brand_obj($obj->brand_id);
    }

    private function _check_import_integrity() {
        $page = (int) \Input::get("page", 1);
        $pagesize = 500;

        $start = ($page - 1) * $pagesize;
        $query = "SELECT P.* FROM import_products P LEFT JOIN import_attributes_xref X ON P.id=X.prodotto_id LIMIT $start,$pagesize";
        $query = "SELECT P.* FROM import_products P LEFT JOIN import_attributes_xref X ON P.id=X.prodotto_id ORDER BY id DESC LIMIT 0,50";
        $rows = DB::select($query);

        foreach ($rows as $row) {
            Writer::nl("<=== CHECKING PRODUCT [$row->id] ===>", "success");

            $product = $this->__get_product_obj($row->id);
        }
    }

    private function _save_images() {
        $page = (int) \Input::get("page", 1);
        $pagesize = 500;

        $start = ($page - 1) * $pagesize;
        $query = "SELECT * FROM import_images LIMIT $start,$pagesize";
        //$query = "SELECT * FROM import_images ORDER BY RAND() LIMIT 0,10";
        $rows = DB::select($query);
        define("_BASE_URL_", "http://www.kronoshop.com/data/imgprodotto/");

        $now = date("Y-m-d H:i:s");
        $user = 2;

        foreach ($rows as $row) {

            $source_file = _BASE_URL_ . $row->imgprodotto_id . "_zoom.jpg";
            $target_file = public_path() . "/assets/products/" . $row->imgprodotto_id . ".jpg";


            if (File::exists($target_file)) {
                Writer::nl("File $target_file SKIPPED!", "warning");
            } else {



                try {
                    $file_content = file_get_contents($source_file);
                    file_put_contents($target_file, $file_content);

                    Writer::nl("File $target_file SAVED!", "success");

                    $data = [
                        "id" => $row->imgprodotto_id,
                        "filename" => $row->imgprodotto_id . ".jpg",
                        "product_id" => $row->prodotto_id,
                        "updated_by" => $user,
                        "created_by" => $user,
                        "updated_at" => $now,
                        "created_at" => $now,
                    ];

                    $image_id = DB::table("images")->insertGetId($data);

                    $data_it = ["product_image_id" => $image_id, "lang_id" => "it"];
                    $data_en = ["product_image_id" => $image_id, "lang_id" => "en"];

                    DB::table("images_lang")->insert($data_it);
                    DB::table("images_lang")->insert($data_en);
                } catch (Exception $ex) {
                    
                }
            }
        }
    }

    private function _adjust_products() {
        $page = (int) \Input::get("page", 1);
        $pagesize = 500;

        $start = ($page - 1) * $pagesize;
        $query = "SELECT * FROM import_products LIMIT $start,$pagesize";
        //$query = "SELECT * FROM import_products ORDER BY RAND() LIMIT 0,10";

        $rows = DB::select($query);

        $_GENERE_ID = 214;
        $_STYLE_ID = 215;

        foreach ($rows as $row) {
            Writer::nl("<===== PRODUCT ID [$row->id] =====>", "success");
            $genere = $row->genere;
            $stile = $row->stile;

            $genere_option = $this->__get_special_option($genere, $genere, $_GENERE_ID);
            $style_option = $this->__get_special_option($stile, $stile, $_STYLE_ID);

            $genere_option_id = $genere_option->attribute_option_id;
            $style_option_id = $style_option->attribute_option_id;

            $query = "SELECT * FROM products_attributes WHERE product_id=$row->id AND attribute_id=$_GENERE_ID";
            $db_genere = DB::selectOne($query);

            $insert_genere = FALSE;

            if (is_object($db_genere)) {
                Writer::nl("Association [$row->id][$_GENERE_ID] skipped", "warning");
            } else {
                $data = ["product_id" => $row->id, "attribute_id" => $_GENERE_ID, "attribute_val" => $genere_option_id];
                $data_position = ["product_id" => $row->id, "attribute_id" => $_GENERE_ID, "position" => 1];
                $insert_genere = TRUE;

                DB::table("products_attributes")->insert($data);
                DB::table("products_attributes_position")->insert($data_position);
            }

            $query = "SELECT * FROM products_attributes WHERE product_id=$row->id AND attribute_id=$_STYLE_ID";
            $db_style = DB::selectOne($query);

            $insert_style = FALSE;

            if (is_object($db_genere)) {
                Writer::nl("Association [$row->id][$_STYLE_ID] skipped", "warning");
            } else {
                $data = ["product_id" => $row->id, "attribute_id" => $_STYLE_ID, "attribute_val" => $style_option_id];
                $data_position = ["product_id" => $row->id, "attribute_id" => $_STYLE_ID, "position" => 2];
                $insert_style = TRUE;

                DB::table("products_attributes")->insert($data);
                DB::table("products_attributes_position")->insert($data_position);
            }

            if ($insert_genere OR $insert_style) {
                $query = "UPDATE products_attributes_position SET position=position+2 WHERE product_id=$row->id AND attribute_id NOT IN($_GENERE_ID,$_STYLE_ID)";
                DB::statement($query);
            }


            $cnt_images = DB::table('images')->where("product_id", $row->id)->count();
            $cnt_categories = DB::table('categories_products')->where("product_id", $row->id)->count();

            $weight = str_replace(",", ".", $row->peso);
            $youtube = $row->youtube;
            $video = $row->video;
            $is_featured = abs($row->vetrina);



            $data_update = [
                "weight" => $weight,
                "youtube" => $youtube,
                "video" => $video,
                "is_featured" => $is_featured,
                "cnt_images" => $cnt_images,
                "cnt_categories" => $cnt_categories,
            ];



            Writer::nl("UPDATE DATA", "message");
            echo '<pre>';
            print_r($data_update);
            echo '</pre>';
            DB::table("products")->where("id", $row->id)->update($data_update);
        }
    }

    private function explain_product_import($id) {
        $query = "SELECT * FROM import_attributes_xref WHERE prodotto_id=$id";
        $obj = DB::selectOne($query);
        Writer::nl("PRODUCT: $id", "success");

        $attributes = $this->arrayfy($obj->attributi_id);
        $attributes_options = $this->arrayfy($obj->valoriattributi_id);

        for ($index = 0; $index < count($attributes); $index++) {
            $attribute_id = $attributes[$index];
            $option_id = $attributes_options[$index];

            $query = "SELECT * FROM import_attributes WHERE id=$attribute_id";
            $attribute = DB::selectOne($query);

            $query = "SELECT * FROM import_attributes_val WHERE id=$option_id";
            $option = DB::selectOne($query);

            Writer::nl("$attribute->nomeit = $option->nomeit");
        }
    }

    private function _import_attributes_options() {

        $query = "SELECT * FROM attributes WHERE frontend_input IN ('select','multiselect')";
        $query = "SELECT * FROM attributes WHERE frontend_input IN ('multiselect')";
        $rows = DB::select($query);

        $counter = 0;


        $now = date("Y-m-d H:i:s");

        foreach ($rows as $row) {
            $counter++;


            $query = "SELECT * FROM import_attributes_val WHERE attributo_id=$row->id";
            $attributes = DB::select($query);

            foreach ($attributes as $attr) {


                $attr->nomeit = $this->clean_token($attr->nomeit);
                $attr->nomeen = $this->clean_token($attr->nomeen);

                $option_it = $this->tokenize_attribute($attr->nomeit, $attr->attributo_id);
                $option_en = $this->tokenize_attribute($attr->nomeen, $attr->attributo_id);
                Writer::nl("Mapping attribute value: $attr->nomeit | $attr->nomeen [attribute:$attr->attributo_id][id:$attr->id]", "st2");

                if (is_array($option_en) AND is_array($option_it)) {
                    if (count($option_en) == count($option_it)) {
                        $match = true;
                    } else {
                        $match = false;
                        Writer::nl("Different number of elements between arrays", "error");
                    }
                } else {
                    if (is_array($option_it) AND !is_array($option_en)) {
                        Writer::nl("IT Option rebinded to string", "warning");
                        $option_it = $attr->nomeit;
                        $match = false;
                    }
                    if (!is_array($option_it) AND is_array($option_en)) {
                        Writer::nl("EN Option rebinded to string", "warning");
                        $option_en = $attr->nomeen;
                        $match = false;
                    }
                    if (!is_array($option_it) AND !is_array($option_en)) {
                        $match = true;
                    }
                }


                if (is_array($option_it) AND is_array($option_en)) {
                    $l = count($option_it);
                    for ($i = 0; $i < $l; $i++) {
                        $opt = $this->_get_attribute_option($option_it[$i], $option_en[$i], $attr->attributo_id, $attr->id);
                    }
                } else {
                    $opt = $this->_get_attribute_option($option_it, $option_en, $attr->attributo_id, $attr->id);
                }
            }
        }
    }

    private function _get_attribute_option($option_it, $option_en, $attribute_id, $id) {
        $it_val = \Str::slug($option_it);
        $en_val = \Str::slug($option_en);

        Writer::nl("Getting attribute value: $it_val | $en_val [attribute:$attribute_id] [attribute_option_id:$id]", "st1");

        $query = "SELECT L.*, O.attribute_id "
                . "FROM attributes_options_lang L "
                . "LEFT JOIN attributes_options O ON L.attribute_option_id=O.id "
                . "WHERE ((L.lang_id='it' AND L.slug=?) OR (L.lang_id='en' AND L.slug=?) OR (O.uslug=?)) AND O.attribute_id=?";
        $opt = DB::selectOne($query, [$it_val, $en_val, $en_val, $attribute_id]);
        echo '<pre>';
        print_r($opt);
        echo '</pre>';
        if (is_object($opt)) {
            Writer::nl("FOUNDED!", "success");
            try {
                DB::table("import_attributes_options")->insert(["attribute_option_id" => $opt->attribute_option_id, "import_id" => $id]);
                Writer::nl("Existant Option Record rebinded succesfully!", "success");
            } catch (Exception $ex) {
                Writer::nl("Existant Option Record rebinded with error!", "error");
                $queries = DB::getQueryLog();
                $last_query = end($queries);
                echo '<pre>';
                print_r($last_query);
                echo '</pre>';
            }
        } else {
            Writer::nl("NOT FOUNDED!", "error");

            $now = date("Y-m-d H:i:s");
            $user = 2;
            $uname = \Str::upper($en_val);
            $uslug = $en_val;

            $option_id = DB::table("attributes_options")->insertGetId([
                "uname" => $uname,
                "uslug" => $uslug,
                "attribute_id" => $attribute_id,
                "created_by" => $user,
                "updated_by" => $user,
                "created_at" => $now,
                "updated_at" => $now,
            ]);

            if ($option_id > 0):

                $bool = DB::table("attributes_options_lang")->insert([
                    "attribute_option_id" => $option_id,
                    "lang_id" => "it",
                    "name" => trim($option_it),
                    "slug" => $it_val,
                ]);

                ($bool) ? Writer::nl("IT Record inserted!", "success") : Writer::nl("IT Record failed!", "error");

                $bool = DB::table("attributes_options_lang")->insert([
                    "attribute_option_id" => $option_id,
                    "lang_id" => "en",
                    "name" => trim($option_en),
                    "slug" => $en_val,
                ]);

                ($bool) ? Writer::nl("EN Record inserted!", "success") : Writer::nl("EN Record failed!", "error");


                $bool = DB::table("import_attributes_options")->insert(["attribute_option_id" => $option_id, "import_id" => $id]);

            endif;
        }
        return $opt;
    }

    private function _check_attributes_options() {

        $query = "SELECT * FROM attributes WHERE frontend_input IN ('select','multiselect')";
        $query = "SELECT * FROM attributes WHERE frontend_input IN ('multiselect')";
        $rows = DB::select($query);

        $counter = 0;


        $now = date("Y-m-d H:i:s");

        foreach ($rows as $row) {
            $counter++;


            $query = "SELECT * FROM import_attributes_val WHERE attributo_id=$row->id";
            $attributes = DB::select($query);

            foreach ($attributes as $attr) {
                Writer::nl("String IT: $attr->nomeit | String EN: $attr->nomeen", "message");

                $attr->nomeit = $this->clean_token($attr->nomeit);
                $attr->nomeen = $this->clean_token($attr->nomeen);

                $option_it = $this->tokenize_attribute($attr->nomeit, $attr->attributo_id);
                $option_en = $this->tokenize_attribute($attr->nomeen, $attr->attributo_id);
                $match = false;
                if (is_array($option_en) AND is_array($option_it)) {
                    if (count($option_en) == count($option_it)) {
                        $match = true;
                    } else {
                        $match = false;
                        Writer::nl("Different number of elements between arrays", "error");
                    }
                } else {
                    if (is_array($option_it) AND !is_array($option_en)) {
                        Writer::nl("IT Option rebinded to string", "warning");
                        $option_it = $attr->nomeit;
                        $match = false;
                    }
                    if (!is_array($option_it) AND is_array($option_en)) {
                        Writer::nl("EN Option rebinded to string", "warning");
                        $option_en = $attr->nomeen;
                        $match = false;
                    }
                    if (!is_array($option_it) AND !is_array($option_en)) {
                        $match = true;
                    }
                }

                /* if(is_array($option_en)){
                  array_walk($option_en, array($this, 'clean_token'));
                  } */


                echo '<pre>';
                print_r($option_it);
                echo PHP_EOL;
                print_r($option_en);
                echo '</pre>';
                if ($match) {
                    Writer::nl("[$row->id][$attr->id] Perfect match", "success");
                } else {
                    Writer::nl("[$row->id][$attr->id] Not match", "error");
                }
            }
        }
    }

    private function _check_attributes_options_select() {

        $query = "SELECT * FROM attributes WHERE frontend_input IN ('select') AND code NOT IN ('package','guarantee')";
        $rows = DB::select($query);

        $counter = 0;


        $now = date("Y-m-d H:i:s");

        foreach ($rows as $row) {
            $counter++;


            $query = "SELECT * FROM import_attributes_val WHERE attributo_id=$row->id";
            $attributes = DB::select($query);

            foreach ($attributes as $attr) {
                Writer::nl("String IT: $attr->nomeit | String EN: $attr->nomeen", "message");

                $option_it = $attr->nomeit;
                $option_en = $attr->nomeen;

                $it_val = \Str::slug($option_it);
                $en_val = \Str::slug($option_en);
                $attribute_id = $attr->attributo_id;

                Writer::nl("Getting attribute value: $it_val | $en_val [attribute:$attribute_id]", "st1");

                $query = "SELECT L.*, O.attribute_id "
                        . "FROM attributes_options_lang L "
                        . "LEFT JOIN attributes_options O ON L.attribute_option_id=O.id "
                        . "WHERE ((L.lang_id='it' AND L.slug=?) OR (L.lang_id='en' AND L.slug=?) OR (O.uslug=?)) AND O.attribute_id=?";
                $opt = DB::selectOne($query, [$it_val, $en_val, $en_val, $attribute_id]);
                echo '<pre>';
                print_r($opt);
                echo '</pre>';
                if (is_object($opt)) {
                    Writer::nl("FOUNDED!", "success");
                } else {
                    Writer::nl("NOT FOUNDED!", "error");
                }
            }
        }
    }

    private function _import_attributes_options_select() {

        $query = "SELECT * FROM attributes WHERE frontend_input IN ('select') AND code NOT IN ('package','guarantee')";
        $rows = DB::select($query);

        $counter = 0;


        $now = date("Y-m-d H:i:s");

        foreach ($rows as $row) {
            $counter++;


            $query = "SELECT * FROM import_attributes_val WHERE attributo_id=$row->id";
            $attributes = DB::select($query);

            foreach ($attributes as $attr) {
                Writer::nl("String IT: $attr->nomeit | String EN: $attr->nomeen", "message");

                $option_it = $attr->nomeit;
                $option_en = $attr->nomeen;
                $this->_get_attribute_option($option_it, $option_en, $attr->attributo_id, $attr->id);
            }
        }
    }

    private function _import_attributes_special() {

        $query = "SELECT * FROM import_attributes WHERE is_option=1 ORDER BY rel,id";
        $rows = DB::select($query);

        $counter = 0;


        $now = date("Y-m-d H:i:s");

        foreach ($rows as $row) {
            $counter++;


            $query = "SELECT * FROM import_attributes_val WHERE attributo_id=$row->id AND nomeit='Si'";
            $attributes = DB::select($query);

            foreach ($attributes as $attr) {
                Writer::nl("String IT: $row->nomeit | String EN: $row->nomeen", "message");

                $option_it = $row->nomeit;
                $option_en = $row->nomeen;
                $this->_get_attribute_option($option_it, $option_en, $row->rel, $attr->id);
            }
        }
    }

    private function tokenize_attribute($str, $attribute_id) {
        $tokens = array();
        if (Str::contains($str, ",")) {
            $tokens = explode(",", $str);
            return $tokens;
        }
        if (Str::contains($str, " e ")) {
            $tokens = explode(" e ", $str);
            return $tokens;
        }
        if (Str::contains($str, " and ")) {
            $tokens = explode(" and ", $str);
            return $tokens;
        }
        if (Str::contains($str, " & ")) {
            $tokens = explode(" & ", $str);
            return $tokens;
        }
        if (Str::contains($str, " with ")) {
            $tokens = explode(" with ", $str);
            return $tokens;
        }
        if (Str::contains($str, "/")) {
            $tokens = explode("/", $str);
            return $tokens;
        }

        if (Str::contains($str, ";")) {
            $tokens = explode(";", $str);
            return $tokens;
        }
        if (Str::contains($str, " - ")) {
            $tokens = explode(" - ", $str);
            return $tokens;
        }

        return $str;
    }

    private function clean_token($str) {
        $search = ["Si - Bande ", "Yes - Bands ", "formati supportati: ", "supported formats: ", "Si - formati supportati: ", "Yes - supported formats: ", "Si - formato supportato:", "Yes - supported format: ", "Si - formati ", "Si  - ", "Si - ", "Yes - "];
        return str_replace($search, "", $str);
    }

    private function _import_attributes() {

        DB::table("import_attributes")->update(["rel" => 0, "is_option" => 0]);

        $query = "SELECT * FROM import_attributes";
        $rows = DB::select($query);

        $counter = 0;

        $founded = 0;
        $not_founded = 0;

        $not_founded_array = [];

        foreach ($rows as $row) {
            $counter++;
            $now = date("Y-m-d H:i:s");

            $slug_it = \Str::slug($row->nomeit);
            $slug_en = \Str::slug($row->nomeen);

            $query = "SELECT * FROM attributes_lang WHERE (slug=? AND lang_id='it') OR (slug=? AND lang_id='en')";
            $obj = DB::selectOne($query, [$slug_it, $slug_en]);
            Writer::nl("Finding element for [$row->id][$row->nomeit]");
            if (is_object($obj)) {
                echo '<pre>';
                print_r($obj);
                echo '</pre>';
                $founded++;

                DB::table("import_attributes")->where("id", $row->id)->update(["rel" => $obj->attribute_id]);
            } else {
                Writer::nl("Direct Match element not founded!");

                //try to search if the attribute is a given option

                $query = "SELECT L.*,O.attribute_id FROM attributes_options_lang L LEFT JOIN attributes_options O ON L.attribute_option_id=O.id WHERE (L.slug=? AND L.lang_id='it') OR (L.slug=? AND L.lang_id='en')";
                $obj = DB::selectOne($query, [$slug_it, $slug_en]);
                if (is_object($obj)) {
                    Writer::nl("Element found as option [$row->id][$row->nomeit]");
                    echo '<pre>';
                    print_r($obj);
                    echo '</pre>';
                    $founded++;

                    DB::table("import_attributes")->where("id", $row->id)->update(["rel" => $obj->attribute_id, "is_option" => 1]);
                } else {
                    $not_founded++;
                    $not_founded_array[] = "[$row->id][$row->nomeit]";
                }
            }
        }

        Writer::nl("Elements founded $founded / $counter");
        Writer::nl("Elements NOT founded $not_founded / $counter");
        echo '<pre>';
        print_r($not_founded_array);
        echo '</pre>';
    }

    private function _import_brands() {
        $query = "SELECT * FROM import_brands";
        $rows = DB::select($query);

        $counter = 0;

        foreach ($rows as $row) {
            $counter++;
            $now = date("Y-m-d H:i:s");

            $data = [
                "id" => $row->id,
                "ogp" => 0,
                "created_by" => 2,
                "updated_by" => 2,
                "created_at" => $now,
                "updated_at" => $now,
                "position" => $counter,
            ];

            DB::table("brands")->insert($data);

            Writer::nl("Inserted brand: $row->id");

            $data_lang = [
                "brand_id" => $row->id,
                "lang_id" => "it",
                "name" => ucwords($row->nome),
                "slug" => \Str::slug($row->nome),
                "metatitle" => ucfirst($row->pagetitleit),
                "h1" => ucfirst($row->pageh1it),
                "ldesc" => ucfirst($row->pagedesit),
            ];

            DB::table("brands_lang")->insert($data_lang);

            Writer::nl("Inserted brands_lang: IT $row->id");

            $data_lang = [
                "brand_id" => $row->id,
                "lang_id" => "en",
                "name" => ucwords($row->nome),
                "slug" => \Str::slug($row->nome),
                "metatitle" => ucfirst($row->pagetitleen),
                "h1" => ucfirst($row->pageh1en),
                "ldesc" => ucfirst($row->pagedesen),
            ];

            DB::table("brands_lang")->insert($data_lang);

            Writer::nl("Inserted brands_lang: EN $row->id");
        }
    }

    private function _import_categories() {
        $query = "SELECT * FROM import_categories";
        $categories = DB::select($query);

        $counter = 0;

        foreach ($categories as $cat) {
            $counter++;
            $now = date("Y-m-d H:i:s");

            $data = [
                "id" => $cat->id,
                "parent_id" => (int) $cat->padre_id,
                "created_by" => 2,
                "updated_by" => 2,
                "created_at" => $now,
                "updated_at" => $now,
                "position" => $counter,
            ];

            DB::table("categories")->insert($data);

            Writer::nl("Inserted category: $cat->id");

            $data_lang = [
                "category_id" => $cat->id,
                "lang_id" => "it",
                "name" => ucfirst($cat->nomeit),
                "slug" => \Str::slug($cat->nomeit),
                "metatitle" => ucfirst($cat->pagetitleit),
                "h1" => ucfirst($cat->pageh1it),
                "ldesc" => ucfirst($cat->pagedesit),
            ];

            DB::table("categories_lang")->insert($data_lang);

            Writer::nl("Inserted category_lang: IT $cat->id");

            $data_lang = [
                "category_id" => $cat->id,
                "lang_id" => "en",
                "name" => ucfirst($cat->nomeen),
                "slug" => \Str::slug($cat->nomeen),
                "metatitle" => ucfirst($cat->pageh1en),
                "h1" => ucfirst($cat->pageh1en),
                "ldesc" => ucfirst($cat->pagedesen),
            ];

            DB::table("categories_lang")->insert($data_lang);

            Writer::nl("Inserted category_lang: EN $cat->id");

            $data = [
                "category_id" => $cat->id,
                "locale" => "it",
                "sdesc" => $cat->googleproductsit,
            ];

            DB::table("google_categories")->insert($data);

            $data = [
                "category_id" => $cat->id,
                "locale" => "uk",
                "sdesc" => $cat->googleproductsuk,
            ];

            DB::table("google_categories")->insert($data);

            $data = [
                "category_id" => $cat->id,
                "locale" => "us",
                "sdesc" => $cat->googleproductsus,
            ];

            DB::table("google_categories")->insert($data);

            Writer::nl("Inserted google_categories: $cat->id");
        }
    }

}

?>
