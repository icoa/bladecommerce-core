<?php

use services\Amazon\Handler;
use services\Amazon\AmazonConfig;
use services\Amazon\AmazonHelper;
use services\Amazon\AmazonRepository;

class AmazonController extends BackendController
{

    public $component_id = 79;
    public $title = 'Gestione Amazon Market Place';
    public $page = 'amazon.index';
    public $pageheader = 'Gestione Amazon Market Place';
    public $iconClass = 'font-columns';
    private $amazonRepository;

    function __construct(AmazonRepository $amazonRepository)
    {
        $this->amazonRepository = $amazonRepository;
        parent::__construct();
        $this->className = __CLASS__;
    }

    function getTest(){
        $this->_style();
        /*$handler = new Handler();

        Writer::nl("Test Amazon");
        $handler->parse();*/

        /*Writer::nl(AmazonConfig::enabledShops());*/

        //AmazonHelper::loadYaml();
        //AmazonHelper::importYaml();
        //AmazonHelper::fetchTemplate();
        AmazonHelper::fetchAttribute();

    }

    public function getConfig()
    {
        $this->page = 'amazon.config';
        $title = 'Configurazione Amazon Market Place';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        //$this->toFooter("js/echo/amazon.js");
        $view = array();
        $this->toolbar('create');
        return $this->render($view);
    }


    public function getMapping()
    {
        $this->page = 'amazon.mapping';
        $title = 'Mapping attributi';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/amazon.mapping.js");
        $attributes = $this->amazonRepository->getAttributesMapping();
        $view = compact('attributes');
        $this->toolbar('create');
        return $this->render($view);
    }


    public function getIndex(){
        $this->addBreadcrumb('Pagina principale');
        $this->toFooter("js/echo/amazon.js");
        $view = array(

        );
        $this->toolbar();
        return $this->render($view);
    }


    public function getCategories(){
        $this->page = 'amazon.categories';
        $this->addBreadcrumb('Categorie');
        $this->toFooter("js/echo/amazon.js");
        $categories = \Category::with('translations')->orderBy('id')->get();
        $mapping = AmazonHelper::categoriesTemplatesMapping();
        $templates = AmazonHelper::templatesOptions();
        $view = array(
            'controllerAction' => "AmazonController@postCategories",
            'categories' => $categories,
            'mapping' => $mapping,
            'templates' => $templates,
        );
        $this->toolbar('create');
        return $this->render($view);
    }



    private function _style()
    {
        echo <<<STYLE
<style>
    body {
        background-color:#000;
        font-family: Courier;
        font-size:12px;
        color:white;
    }

    .error{
        color:red;
    }

    .success{
        color:lime;
    }

    .message{
        color:cyan;
    }

    .warning{
        color:yellow;
    }

    .st1{
        color:magenta;
    }

    .st2{
        color:orange;
    }
</style>
STYLE;
    }


    protected function actions_default($params = [])
    {

        $config = ($this->action("getConfig", TRUE));
        $categories = ($this->action("getCategories", TRUE));
        $mapping = ($this->action("getMapping", TRUE));
        $actions = array(
            new AdminAction('Impostazioni', $config, 'font-edit', '', 'Imposta i parametri per la gestione di Amazon Market Place', 'index'),
            new AdminAction('Categorie', $categories, 'font-edit', '', 'Imposta la correlazione tra Categorie del sistema e le Categorie di Amazon', 'categories'),
            new AdminAction('Mapping', $mapping, 'font-edit', '', 'Imposta la correlazione tra Attributi del sistema e Attributi di Amazon', 'mapping'),
        );
        return $actions;
    }




    function postSaveconfig(){
        //Flash current values to session
        \Input::flashExcept("_lang_fields[]","_lang_fields");

        $data = \Input::all();
        unset($data['_token']);
        unset($data['task']);

        \Utils::log($data,__METHOD__);

        AmazonConfig::setMultiple($data);

        \Input::flush();

        Notification::success("Configurazione aggiornata con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getConfig"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }



    function postSaveMapping(){
        //Flash current values to session
        \Input::flashExcept("_lang_fields[]","_lang_fields");

        $data = \Input::all();
        unset($data['_token']);
        unset($data['task']);

        \Utils::log($data,__METHOD__);

        \Input::flush();

        Notification::success("Mapping attributi aggiornato con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getMapping"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }



    function postCategories(){
        $data = \Input::all();
        unset($data['_token']);
        unset($data['task']);

        \Utils::log($data,__METHOD__);

        \Input::flush();

        $template = $data['template'];

        AmazonHelper::setCategoriesTemplates($template);

        Notification::success("Configurazione aggiornata con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getCategories"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }




};