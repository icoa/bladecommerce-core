<?php

class TaxesController extends MultiLangController {

    public $component_id = 49;
    public $title = 'Gestione Tasse';
    public $page = 'taxes.index';
    public $pageheader = 'Gestione Tasse';
    public $iconClass = 'font-columns';
    public $model = 'Tax';
    protected $rules = array(
        'rate' => 'required'
    );
    protected $lang_rules = array(
        'name' => 'required'
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        
    }

    

    public function getIndex() {
        $this->addBreadcrumb('Elenco Tasse');
        $this->toFooter("js/echo/taxes.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/taxes.js");
        $this->page = 'taxes.trash';
        $this->pageheader = 'Cestino Tasse';      
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {        
        $this->page = 'taxes.create';
        $this->pageheader = 'Nuovo Tasse';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {        
        $this->page = 'taxes.create';
        $this->pageheader = 'Modifica Tasse';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }
    

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';        

        $model = $this->model;

        $pages = $model::leftJoin('taxes_lang', 'id', '=', 'taxes_lang.tax_id')
                ->where('lang_id', $lang_id)                
                ->select('taxes.id', 'taxes_lang.name as name', 'rate', 'active', 'created_at', 'lang_id');
        
        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::date($data['created_at']);
                                })
                        ->edit_column('name', function($data) {
                                    $link = \URL::action($this->action("getEdit"), $data['id']);
                                    return "<strong><a href='$link'>{$data['name']}</a></strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_actions($data);
                                })
                        ->edit_column('active', function($data) {
                                    return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                                })                        
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                                })
                        ->make();
    }
    
    
    

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('taxes_lang', 'id', '=', 'taxes_lang.tax_id')
                ->where('lang_id', $lang_id)                
                ->select('taxes.id', 'taxes_lang.name as name', 'rate', 'active', 'deleted_at', 'lang_id');

        return \Datatables::of($pages)                        
                        ->edit_column('deleted_at', function($data) {
                                    return \Format::date($data['deleted_at']);
                                })
                        ->edit_column('name', function($data) {
                                    return "<strong>{$data['name']}</strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_trash_actions($data);
                                })                        
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                                })
                        ->make();
    }
    
    
    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {        
        $this->_prepare();
    }

    function _prepare() {
        if(count($_POST) == 0){
            return;
        }        
        
        $langDef = Cfg::get('DEFAULT_LANGUAGE');
        
        $defaultName = $_POST["name_".$langDef];      

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if($lang != $langDef){
                if($_POST["name_".$lang] == "")$_POST["name_".$lang] = $defaultName;                
            }            
        }

        $model = $this->model;
        
        \Input::replace($_POST);

    }
    
    
    function postList(){
        $rows = \Mainframe::brands();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

}