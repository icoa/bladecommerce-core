<?php

class CarriersController extends MultiLangController
{

    public $component_id = 42;
    public $title = 'Gestione Corrieri';
    public $page = 'carriers.index';
    public $pageheader = 'Gestione Corrieri';
    public $iconClass = 'font-columns';
    public $model = 'Carrier';
    protected $rules = array(
        'grade' => 'required',
    );
    protected $lang_rules = array(
        'name' => 'required',
        'delay' => 'required'
    );
    protected $friendly_names = array(

        'grade' => 'Grado di velocità',
    );
    protected $lang_friendly_names = array(
        'name' => 'Denominazione corriere',
        'delay' => 'Etichetta tempo di consegna'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Corrieri');
        $this->toFooter("js/echo/carriers.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/carriers.js");
        $this->page = 'carriers.trash';
        $this->pageheader = 'Cestino Corrieri';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }


    public function getConfig()
    {
        $this->page = 'carriers.config';
        $this->pageheader = 'Impostazioni Corrieri';
        $this->iconClass = 'font-cog';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('create');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/carriers.js");
        $this->page = 'carriers.create';
        $this->pageheader = 'Nuovo Corriere';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/carriers.js");
        $this->page = 'carriers.create';
        $this->pageheader = 'Modifica Corriere';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getPreview($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->fillLanguages();
        $view = array('obj' => $obj);
        return Theme::scope("carriers.preview", $view)->content();
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('carriers_lang', 'id', '=', 'carriers_lang.carrier_id')
            ->where('lang_id', $lang_id)
            ->groupBy("carriers.id")
            ->select('carriers.id', 'name', 'delay', 'shipping_method', 'active', 'is_free', 'carriers.created_at', 'carriers.position', 'carriers_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('shipping_method', function ($data) {
                $name = '';
                switch ($data['shipping_method']) {
                    case 1:
                        $name = 'In base al peso';
                        break;
                    case 2:
                        $name = 'In base al prezzo';
                        break;

                    default:
                        break;
                }
                return "<strong>$name</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->edit_column('is_free', function ($data) {
                return ($data['is_free'] == 1) ? '<span class="label label-success">Sì</span>' : '<span class="label label-important">No</span>';
            })
            ->edit_column('position', function ($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('carriers_lang', 'id', '=', 'carriers_lang.carrier_id')
            ->where('lang_id', $lang_id)
            ->groupBy("carriers.id")
            ->select('carriers.id', 'name', 'delay', 'shipping_method', 'active', 'is_free', 'carriers.deleted_at', 'carriers.created_at', 'carriers_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('shipping_method', function ($data) {
                $name = '';
                switch ($data['shipping_method']) {
                    case 1:
                        $name = 'In base al peso';
                        break;
                    case 2:
                        $name = 'In base al prezzo';
                        break;

                    default:
                        break;
                }
                return "<strong>$name</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->edit_column('is_free', function ($data) {
                return ($data['is_free'] == 1) ? '<span class="label label-success">Sì</span>' : '<span class="label label-important">No</span>';
            })
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function postRules($id, $mode)
    {
        $response = array("success" => true, "html" => \Carrier::generateZonesTable($id, $mode));
        return Json::encode($response);
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
        $this->handleFees($model);
        $this->handleRules($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
        $this->handleFees($model);
        $this->handleRules($model);
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }
        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
            }
        }

        $_POST['max_width'] = (int)\Input::get("max_width", 0);
        $_POST['max_height'] = (int)\Input::get("max_height", 0);
        $_POST['max_depth'] = (int)\Input::get("max_depth", 0);
        $_POST['max_weight'] = (float)\Input::get("max_weight", 0);

        $_POST['group_restriction'] = (int)\Input::get("group_restriction", 0);

        $model = $this->model;
        if ((int)$_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }

    /* private function handleFees($model) {
      if ((int) \Input::get("shipping_method") == 1) {
      $this->handleWeightFees($model);
      } else {
      $this->handlePriceFees($model);
      }
      } */

    private function handleFees($model)
    {
        $shipping_method = (int)\Input::get("shipping_method");
        $is_free = (int)\Input::get("is_free");
        switch ($shipping_method) {
            case 1:
                $table = 'range_weight';
                break;
            case 2:
                $table = 'range_price';
                break;

            default:
                break;
        }
        \Utils::watch();
        $carrier_id = $model->id;
        $range_inf = \Input::get("range_inf");
        $range_sup = \Input::get("range_sup");
        $fees = \Input::get("fees");
        $fasce = [];

        \DB::table("range_weight")->where("carrier_id", $carrier_id)->delete();
        \DB::table("range_price")->where("carrier_id", $carrier_id)->delete();
        \DB::table("delivery")->where("carrier_id", $carrier_id)->delete();

        if ($is_free == 1) {
            return;
        }

        foreach ($range_inf as $key => $value) {
            $ri = (float)$range_inf[$key];
            $rs = (float)$range_sup[$key];
            $data = [
                "carrier_id" => $carrier_id,
                "delimiter1" => $ri,
                "delimiter2" => $rs,
            ];
            $fascia_id = \DB::table($table)->insertGetId($data);
            $fasce[$key] = $fascia_id;
        }
        foreach ($fees as $zone_id => $price_array) {
            if (isset($_POST["zone_" . $zone_id]) AND $_POST["zone_" . $zone_id] == 1) {
                foreach ($price_array as $key => $value) {
                    $fascia_id = $fasce[$key];
                    $price = (float)$value;
                    $data = [
                        "carrier_id" => $carrier_id,
                        $table . "_id" => $fascia_id,
                        "zone_id" => $zone_id,
                        "price" => $price,
                    ];
                    DB::table("delivery")->insert($data);
                }
            }
        }
    }

    private function handlePriceFees($model)
    {

    }

    function postSaveconfig()
    {
        //Flash current values to session
        \Input::flashExcept("_lang_fields[]", "_lang_fields");

        $keys = \Cfg::getGroupKeys('shipping');

        $data = array();

        foreach ($keys as $key) {
            if (isset($_POST[$key])) {
                //$data[$key] = Input::get($key);
                $data[$key] = $_POST[$key];
            }
        }

        \Cfg::storeGroup('shipping', $data);

        \Input::flush();

        Notification::success("Configurazione aggiornata con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getConfig"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }


    static function getGroups($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("carriers_group")->where("carrier_id", $id)->lists("group_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \CustomerGroup::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }


    private function handleRules($model)
    {
        $carrier_id = $model->id;

        \DB::table("carriers_group")->where("carrier_id", $carrier_id)->delete();
        $group_restriction = (int)\Input::get("group_restriction", 0);

        if ($group_restriction == 1) {
            $unselected_groups = \Input::get("unselected_groups", []);

            if (!empty($unselected_groups)) {
                foreach ($unselected_groups as $group_id) {
                    \DB::table("carriers_group")->insert(compact("carrier_id", "group_id"));
                }
            }
        }


        return;

    }

}
