<?php

class RmasController extends BackendController
{

    public $component_id = 31;
    public $title = 'Gestione Resi';
    public $page = 'rmas.index';
    public $pageheader = 'Gestione Resi';
    public $iconClass = 'font-columns';
    public $model = 'Rma';
    protected $rules = array(

    );

    protected $friendly_names = array(

    );

    protected $asset_versioning = '1.01';

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }



    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }


        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Resi');
        $this->toFooter("js/echo/rmas.js?v={$this->asset_versioning}");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash(){

    }

    public function getCreate(){

    }

    public function getPreview($id){
        $this->page = 'rmas.preview';
        $this->toFooter("js/echo/rmas.js?v={$this->asset_versioning}");
        $this->pageheader = 'Dettagli RMA';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->expand();
        $view['obj'] = $obj;
        \AdminTpl::setData('rma',$obj);
        return $this->render($view);
    }


    public function getSlip($id){
        App::setLocale( Input::get('locale','it') );
        $order = Rma::getObj($id);
        $data = $order->getSlipData();
        $pdf = PDF::loadView('pdf.rma', $data);
        return $pdf->setPaper('a4')->setOrientation('landscape')->stream();
    }


    public function getTable()
    {
        $model = $this->model;

        $pages = $model::where('id','>',0)
            ->select(
                'id',
                'reference',
                'reason',
                'option_id',
                'status',
                'order_id',
                'customer_id',
                'rma_date',
                'updated_at'
            );

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('rma_date', function ($data) {
                return \Format::date($data['rma_date']);
            })
            ->edit_column('reference', function ($data) {
                $link = \URL::action($this->action("getPreview"), $data['id']);
                return "<strong><a href='$link'>{$data['reference']}</a></strong>";
            })
            ->edit_column('reason', function ($data) {
                $rma = \Rma::getObj($data['id']);
                $rma->expand();
                return $rma->details;
            })
            ->edit_column('customer_id', function ($data) {
                $order = \Rma::getObj($data['id']);
                $customer = $order->getCustomer();
                $s = ($customer) ? $customer->getName() : "ND";
                $url = URL::action("CustomersController@getEdit",$data['customer_id']);
                return "<strong><a href='$url' target='_blank'>{$s}</a></strong>";
            })
            ->edit_column('order_id', function ($data) {
                $order = \Rma::getObj($data['id']);
                $customer = $order->getOrder();
                $s = ($customer) ? $customer->reference : "ND";
                $url = URL::action("OrdersController@getPreview",$data['order_id']);
                return "<strong><a href='$url' target='_blank'>{$s}</a><br>#{$data['order_id']}</strong>";
            })
            ->edit_column('option_id', function ($data) {
                $order = \Rma::getObj($data['id']);
                $customer = $order->getOption();
                $s = ($customer) ? $customer->name : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('status', function ($data) {
                $order = \Rma::getObj($data['id']);
                return $order->statusName(true);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }



    function postApply($id){
        $data = Input::all();
        /* @var $rma \Rma */
        $rma = Rma::getObj($id);
        $success = false;
        $msg = "Impossibile trovare un RMA con i dati forniti";
        if($rma){
            $rma->setStatus($data['status'],$data['response']);
            $rma->updateOrderPaymentStatus();
            if(isset($data['update_order']) AND $data['update_order'] == 1){
                $rma->updateOrderStatus();
            }
            \Session::set('rmaLastStatus',$data['status']);
            $success = true;
        }
        return Json::encode(compact('success','msg'));
    }



    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        /*$export = ($this->action("getExport", TRUE));
        $actions[] = new AdminAction('Esporta CSV', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');*/
        return $actions;
    }

    protected function actions_create( $params = [] ){
        $actions = parent::actions_create(['only' => ['back']]);
        return $actions;
    }

}