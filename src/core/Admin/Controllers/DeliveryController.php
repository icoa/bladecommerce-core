<?php

use services\Models\OrderSlipDetail;

class DeliveryController extends BackendController
{

    public $component_id = 32;
    public $title = 'Gestione Documenti di trasporto';
    public $page = 'delivery.index';
    public $pageheader = 'Gestione Documenti di trasporto';
    public $iconClass = 'font-columns';
    public $model = 'Order';
    protected $rules = array();

    protected $friendly_names = array();

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }


        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Documenti di trasporto');
        //$this->toFooter("js/echo/invoices.js");
        $this->toFooter("js/echo/delivery.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'invoices.create';
        $this->toFooter("js/echo/invoices.js");
        $this->pageheader = 'Nuovo Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'invoices.create';
        $this->pageheader = 'Nuovo Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }


    public function getEdit($id)
    {
        return $this->getPreview($id);
        $this->page = 'invoices.create';
        $this->pageheader = 'Modifica Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getPreview($id)
    {
        $this->page = 'invoices.preview';
        $this->toFooter("js/echo/invoices.js");
        $this->pageheader = 'Dettagli Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTableLegacy()
    {
        $model = $this->model;

        $pages = $model::where('delivery_number', '>', 0)
            ->select(
                'orders.id',
                'delivery_number',
                'reference',
                'customer_id',
                'delivery_date',
                'created_at',
                'total_order'
            );

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('delivery_number', function ($data) {
                $order = \Order::getObj($data['id']);
                $invoice = $order->getDelivery();
                $str = "<span class='label label-important'>{$invoice['number']}</span>";
                if ($invoice['hasFile']) {
                    $str .= "<a href='{$invoice['url']}' target='_blank' class='btn btn-mini btn-small ml15'>Apri documento</a>";
                }
                return $str;
            })
            ->edit_column('delivery_date', function ($data) {
                return \Format::date($data['delivery_date']);
            })
            ->edit_column('reference', function ($data) {
                $url = URL::action("OrdersController@getPreview", $data['id']);
                return "<strong><a href='$url' target='_blank'>{$data['reference']}</a></strong>";
            })
            ->edit_column('customer_id', function ($data) {
                $order = \Order::getObj($data['id']);
                $customer = $order->getCustomer();
                $s = ($customer) ? $customer->getName() : "ND";
                $url = URL::action("CustomersController@getEdit", $data['customer_id']);
                return "<strong><a href='$url' target='_blank'>{$s}</a></strong>";
            })
            ->edit_column('total_order', function ($data) {
                return "<strong>" . \Format::currency($data['total_order'], true) . "</strong>";
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $queryBuilder = OrderSlipDetail::leftJoin('orders', 'mi_orders_slips_details.order_id', '=', 'orders.id')
            ->leftJoin('mi_orders_slips', 'mi_orders_slips_details.order_id', '=', 'mi_orders_slips.order_id')
            ->select([
                'mi_orders_slips_details.id as id',
                'orders.reference',
                'mi_orders_slips.sales_id',
                'mi_orders_slips.delivery_id',
                'mi_orders_slips.invoice_id',
                'mi_orders_slips.withdrawal_id',
                'mi_orders_slips_details.type',
                'mi_orders_slips_details.matnr',
                'mi_orders_slips_details.posnn',
                'mi_orders_slips_details.posnv',
                'mi_orders_slips_details.vbelv',
                'mi_orders_slips_details.lgnum',

                'mi_orders_slips_details.response_at as response_at',
                'mi_orders_slips_details.created_at as created_at',
                'mi_orders_slips_details.updated_at as updated_at',

                'mi_orders_slips_details.order_id',
                'orders.created_at as order_created_at',
                'mi_orders_slips_details.vbeln',

            ]);


        $pages = $queryBuilder;


        $callback = function ($data) {
            $key = 'order_details_slip_' . $data['id'];
            if (\Registry::has($key))
                return \Registry::get($key);

            $model = new OrderSlipDetail($data);
            \Registry::set($key, $model);
            return $model;
        };

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('response_at', function ($data) {
                return \Format::datetime($data['response_at']);
            })
            ->edit_column('order_created_at', function ($data) {
                return \Format::datetime($data['order_created_at']);
            })
            ->edit_column('reference', function ($data) {
                $link = \URL::action("OrdersController@getPreview", $data['order_id']);
                return "<strong><a href='$link'>{$data['reference']}</a></strong>";
            })
            ->edit_column('type', function ($data) use ($callback) {
                $model = $callback($data);
                return $model->getTypeHtmlAttribute();
            })
            ->edit_column('matnr', function ($data) use ($callback) {
                $model = $callback($data);
                return $model->getSkuHtmlAttribute();
            })
            ->edit_column('vbelv', function ($data) use ($callback) {
                $model = $callback($data);
                return $model->getSummaryHtmlAttribute();
            })
            ->edit_column('lgnum', function ($data) use ($callback) {
                $model = $callback($data);
                return $model->getReasonHtmlAttribute();
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }


    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        /*$export = ($this->action("getExport", TRUE));
        $actions[] = new AdminAction('Esporta CSV', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');*/
        return $actions;
    }


    function getExport()
    {

        $rows = \Newsletter::orderBy('email')->get();

        $lines = ['email,lingua,data'];
        foreach ($rows as $row) {
            $lines[] = $row->email . "," . $row->lang_id . "," . date("d/m/Y", strtotime($row->created_at));
        }

        $file = storage_path() . '/temp.csv';

        \File::put($file, implode(PHP_EOL, $lines));

        $filename = "destinatari_newsletter_" . time() . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }


}