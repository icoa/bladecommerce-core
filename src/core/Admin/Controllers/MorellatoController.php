<?php

class MorellatoController extends BackendController
{

    public $component_id = 82;
    public $title = 'Importazione dati Morellato';
    public $page = 'morellato.index';
    public $pageheader = 'Importazione dati Morellato';
    public $iconClass = 'font-columns';
    const EMPLOYER_ID = 10;


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    public function getIndex()
    {
        $this->pageheader = 'Importazione dati Morellato';
        $this->addBreadcrumb('Importazione dati Morellato');
        $this->toFooter("js/echo/morellato.js");
        $view =  $this->stats() ;
        $this->toolbar();
        return $this->render($view);
    }


    private function stats(){
        Utils::watch();
        $lastDay = DB::table('mi_logs')->max('created_at');
        $lastDate = date('Y-m-d',strtotime($lastDay));
        $stats = [];
        $builder = DB::table('mi_logs')->where(DB::raw('date(created_at)'),$lastDate);
        $stats['insert'] = $builder->where('message','like','%successfully inserted%')->count();
        $builder = DB::table('mi_logs')->where(DB::raw('date(created_at)'),$lastDate);
        $stats['update'] = $builder->where('message','like','%successfully updated%')->count();
        $builder = DB::table('mi_logs')->where(DB::raw('date(created_at)'),$lastDate);
        $stats['skip'] = $builder->where('message','like','%(skip)%')->count();
        $builder = DB::table('mi_logs')->where(DB::raw('date(created_at)'),$lastDate);
        $stats['errors'] = $builder->where('verbose','error')->count();
        return compact('lastDay','lastDate','stats');
    }


    public function getTable()
    {
        \Utils::watch();

        $pages = DB::table('mi_logs')
            ->select(
                "id",
                "message",
                "type",
                "verbose",
                "created_at"
            );

        return \Datatables::of($pages)
            ->edit_column('verbose', function ($data) {
                $class = $data['verbose'] == 'error' ? 'label-important' : 'label-success';
                return "<span class='label $class'>{$data['verbose']}</span>";
            })
            ->make(true);
    }


    public function getTableorders()
    {
        \Utils::watch();

        $pages = DB::table('mi_orders_logs')
            ->select(
                "id",
                "message",
                "type",
                "verbose",
                "created_at"
            );

        return \Datatables::of($pages)
            ->edit_column('verbose', function ($data) {
                $class = $data['verbose'] == 'error' ? 'label-important' : 'label-success';
                return "<span class='label $class'>{$data['verbose']}</span>";
            })
            ->make(true);
    }


    public function getTablefidelity()
    {
        \Utils::watch();

        $pages = DB::table('mi_fidelity_logs')
            ->select(
                "id",
                "message",
                "type",
                "verbose",
                "created_at"
            );

        return \Datatables::of($pages)
            ->edit_column('verbose', function ($data) {
                $class = $data['verbose'] == 'error' ? 'label-important' : 'label-success';
                return "<span class='label $class'>{$data['verbose']}</span>";
            })
            ->make(true);
    }


    public function getOrders()
    {
        $this->pageheader = 'Morellato - dettagli ordini';
        $this->addBreadcrumb('Morellato - dettagli ordini');
        $this->toFooter("js/echo/morellato.js");
        $this->page = 'morellato.orders';
        $view =  [];
        $this->toolbar();
        return $this->render($view);
    }

    public function getFidelity()
    {
        $this->pageheader = 'Dettagli operazioni Fidelity';
        $this->addBreadcrumb('Dettagli operazioni Fidelity');
        $this->toFooter("js/echo/morellato.js");
        $this->page = 'morellato.fidelity';
        $view =  [];
        $this->toolbar();
        return $this->render($view);
    }


    private function _style()
    {
        echo <<<STYLE
<style>
    body {
        background-color:#000;
        font-family: Courier;
        font-size:12px;
        color:white;
    }

    .error{
        color:red;
    }

    .success{
        color:lime;
    }

    .message{
        color:cyan;
    }

    .warning{
        color:yellow;
    }

    .st1{
        color:magenta;
    }

    .st2{
        color:orange;
    }
</style>
STYLE;
    }


    protected function actions_default($params = [])
    {
        $orders = ($this->action("getOrders", TRUE));
        $index = ($this->action("getIndex", TRUE));
        $fidelity = ($this->action("getFidelity", TRUE));
        $actions = array(
            new AdminAction('Prodotti', $index, 'font-plus', '', 'Mostra i dettagli circa i prodotti','index'),
            new AdminAction('Ordini', $orders, 'font-plus', '', 'Mostra i dettagli circa gli ordini','orders'),
            new AdminAction('Fidelity', $fidelity, 'font-plus', '', 'Mostra i dettagli circa le Fidelity','fidelity'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente','reload'),
        );
        return $actions;
    }


    protected function actions_create($params = [])
    {
        $actions = parent::actions_create(['only' => ['back']]);
        return $actions;
    }



    static function prt($val, $class = '')
    {
        $now = Format::now();
        echo "<span class='$class'>[$now] - $val</span><br>";
    }


}