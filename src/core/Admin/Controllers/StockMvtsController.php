<?php

class StockMvtsController extends BackendController
{

    public $component_id = 77;
    public $title = 'Movimentazioni magazzino';
    public $page = 'stock_mvts.index';
    public $pageheader = 'Movimentazioni magazzino';
    public $iconClass = 'font-columns';
    public $model = 'StockMvt';


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_create($model)
    {

    }

    function _prepare()
    {

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Movimentazioni magazzino');
        $this->toFooter("js/echo/stock_mvts.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }


    public function getEdit($id)
    {

    }





    public function getTable()
    {
        $model = $this->model;

        \Utils::watch();

        $pages = $model::groupBy('stock_mvts.id')
            ->leftJoin('orders','stock_mvts.order_id','=','orders.id')
            ->select(
                'stock_mvts.id',
                'reference',
                'stock_mvts.stock_reason_id',
                'stock_mvts.product_reference',
                'stock_mvts.product_name',
                'stock_mvts.sign',
                'stock_mvts.qty',
                'stock_mvts.user_firstname',
                'stock_mvts.user_lastname',
                'stock_mvts.created_at',
                'stock_mvts.product_id',
                DB::raw('orders.id as order_id')
            );

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('reference', function ($data) {
                if($data['order_id'] > 0){
                    $link = \URL::action("OrdersController@getPreview", $data['order_id']);
                    return "<a href='$link'><span class='label label-success'>{$data['reference']}</span></a>";
                }else{
                    return "<strong>N.D.</strong>";
                }
            })
            ->edit_column('product_name', function ($data) {
                if($data['product_id'] > 0){
                    $link = \URL::action("ProductsController@getEdit", $data['product_id']);
                    return "<a href='$link'>{$data['product_name']}</a>";
                }else{
                    return "<strong>N.D.</strong>";
                }
            })
            ->edit_column('stock_reason_id', function ($data) {
                $stock = \StockMvt::getObj($data['id']);
                $reason = $stock->getReason();
                if($reason){
                    return $reason->getName();
                }else{
                    return "<strong>N.D.</strong>";
                }
            })
            ->add_column('qty', function ($data) {
                $c = $data['qty'];
                return "<strong style='font-size:13px'>$c</strong>";
            })
            ->add_column('sign', function ($data) {
                $c = $data['sign'];
                return $c > 0 ? "<span class='label label-success'><i class='font-plus'></i></span>" : "<span class='label label-important'><i class='font-minus'></i></span>";
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }





    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        return $actions;
    }

    protected function actions_create( $params = [] ){
        $actions = parent::actions_create(['only' => ['back']]);
        return $actions;
    }



}