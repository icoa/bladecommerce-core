<?php

use Core\Condition\RuleResolver;

class ModulesController extends MultiLangController
{

    public $component_id = 26;
    public $title = 'Gestione Moduli';
    public $page = 'modules.index';
    public $pageheader = 'Gestione Moduli';
    public $iconClass = 'font-columns';
    public $model = 'Module';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model){
        parent::_after_update($model);
        $languages = \Mainframe::languagesCodes();
        foreach($languages as $lang){
            $cache_key = "module-$lang-".$model->id;
            \Cache::forget($cache_key);
            $cache_key = "mobile-module-$lang-".$model->id;
            \Cache::forget($cache_key);
        }
        try{
            $cache_key = "position-".$model->mod_position;
            \Cache::forget($cache_key);
        }catch(\Exception $e){
            \Utils::log($e->getMessage());
        }
    }

    function _after_create($model){
        parent::_after_create($model);
        try{
            $cache_key = "position-".$model->mod_position;
            \Cache::forget($cache_key);
        }catch(\Exception $e){
            \Utils::log($e->getMessage());
        }
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'PRE POST');

        $_POST['starting_at'] = Format::sqlDatetime($_POST['starting']);
        $_POST['finishing_at'] = Format::sqlDatetime($_POST['finishing']);
        $_POST['params'] = \ModuleHelper::setParams($_POST['params']);
        $_POST['acl_groups'] = implode(",",$_POST['acl_groups']);

        $default_lang = \Core::getLang();

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            $source = $_POST["name_" . $default_lang];
            $target = $_POST["name_" . $lang];
            if ($target == "") {
                $_POST["name_" . $lang] = $source;
            }
        }
        $_POST['position'] = ((int)$_POST['position'] > 0) ? $_POST['position'] : \Module::getPosition($_POST['mod_position']);
        if(isset($_POST['content'])){
            $json = \Input::get("json",0);
            if($json == 1){
                $_POST['content'] = \Json::decode($_POST['content']);
            }
            $_POST['content'] = is_array($_POST['content']) ? serialize($_POST['content']) : $_POST['content'];

        }
        if(isset($_POST['content_alt'])){
            $json = \Input::get("json_alt",0);
            if($json == 1){
                $_POST['content_alt'] = \Json::decode($_POST['content_alt']);
            }
            $_POST['content_alt'] = is_array($_POST['content_alt']) ? serialize($_POST['content_alt']) : $_POST['content_alt'];

        }

        $conditions = \Input::get("rule")["conditions"];
        $rr = new RuleResolver();
        $_POST['conditions'] = $rr->getSerializable($conditions);

        \Input::replace($_POST);

    }



    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Moduli');
        $this->toFooter("js/echo/modules.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'modules.trash';
        $this->pageheader = 'Cestino Moduli';
        $this->toFooter("js/echo/modules.js");
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getChoose()
    {
        $this->page = 'modules.choose';
        $this->pageheader = 'Tipologia del modulo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('choose');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->_add_assets();
        $this->toFooter("js/echo/builder.js");
        $this->page = 'modules.create';
        $this->pageheader = 'Modifica Modulo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)->leftJoin("module_types","modules.mod_type","=","module_types.module")
            ->select('modules.id', 'modules_lang.name', 'modules.mod_type', 'mod_position', 'modules_lang.published', 'modules.created_at', 'modules.position', 'modules_lang.input_textarea', 'modules_lang.lang_id', 'module_types.name as typename', 'cache', 'cache_ttl');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $flag = $data['cache'] == 1 ? "<span class='label label-success' style='float:right'>CACHED: {$data['cache_ttl']}</span>" : "<span class='label label-warning' style='float:right'>NOT CACHED</span>";
                return "<strong><a href='$link'>{$data['name']}</a>$flag</strong>";
            })
            ->edit_column('mod_type', function ($data) {
                return "<em>{$data['typename']}</em>";
            })
            ->edit_column('mod_position', function ($data) {
                return "<strong>{$data['mod_position']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicato</span>' : '<span class="label label-important">Sospeso</span>';
            })
            ->edit_column('position', function($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('input_textarea')
            ->remove_column('typename')
            ->remove_column('lang_id')
            ->remove_column('cache')
            ->remove_column('cache_ttl')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)->leftJoin("module_types","modules.mod_type","=","module_types.module")
            ->select('modules.id', 'modules_lang.name', 'module_types.name as typename', 'mod_position', 'modules_lang.published', 'modules.deleted_at', 'modules.position', 'modules_lang.input_textarea', 'modules_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->edit_column('typename', function ($data) {
                return "<em>{$data['typename']}</em>";
            })
            ->edit_column('mod_position', function ($data) {
                return "<strong>{$data['mod_position']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicato</span>' : '<span class="label label-important">Sospeso</span>';
            })
            ->remove_column('input_textarea')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getCreate(){
        $this->_add_assets();
        $this->toFooter("js/echo/builder.js");
        $mod_type = (\Input::old("mod_type") != '') ? \Input::old("mod_type") : \Input::get("mod_type");
        $this->page = 'modules.create';
        $this->pageheader = 'Nuovo Modulo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array("mod_type" => $mod_type);
        return $this->render($view);
    }


    private function _add_assets(){
        //$this->toFooter("js/plugins/gridster/jquery.gridster.js");
        $this->toFooter("js/echo/mosaic.js");
        $this->toFooter("js/echo/modules.js");
        //$this->toHead("js/plugins/gridster/jquery.gridster.min.css");
    }



    protected function toolbar($what = 'default') {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'choose':
                $index = \URL::action($this->action("getIndex"));
                $actions = array(
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                    new AdminAction('Avanti', "javascript:Echo.submitForm('create');", 'font-arrow-right', 'btn-success', 'Continua con la creazione del Modulo'),
                );
                break;


            case 'create':
                $index = \URL::action($this->action("getIndex"));

                $actions = array(
                    new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva ed esci', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Torna indietro', $index, 'font-arrow-left'),
                );

                if ($this->isPopup) {
                    $actions = array(
                        new AdminAction('Salva', "javascript:Echo.submitForm('reopen');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e riapre questa schermata'),
                        new AdminAction('Chiudi', "javascript:window.close();", 'font-remove'),
                    );
                }

                break;

            case 'trash':
                $index = $this->action("getIndex", TRUE);
                $restore = $this->action("postRestoreMulti", TRUE, 0);
                $destroy = $this->action("postDestroyMulti", TRUE);
                $actions = array(
                    new AdminAction('Ripristina', $restore, 'font-undo', 'btn-success confirm-action-multi', 'Recupera i record selezionati'),
                    new AdminAction('Elimina', $destroy, 'font-minus-sign', 'btn-danger confirm-action-multi', 'Elimina definitivamente i record selezionati'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );

                break;

            default:
                $trash = ($this->action("getTrash", TRUE));
                $create = ($this->action("getChoose", TRUE));
                $flagMultiON = ($this->action("postFlagMulti", TRUE, 1));
                $flagMultiOff = ($this->action("postFlagMulti", TRUE, 0));
                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $cloneMulti = ($this->action("postCloneMulti", TRUE));
                $actions = array(
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Abilita', $flagMultiON, 'font-ok', 'btn-warning action-multi', 'Abilita uno o più record selezionati'),
                    new AdminAction('Disabilita', $flagMultiOff, 'font-off', 'btn-warning action-multi', 'Disabilita uno o più record selezionati'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),
                    new AdminAction('Clona', $cloneMulti, 'font-copy', 'btn-info confirm-action-multi', 'Clona uno o più record selezionati'),
                    new AdminAction('Cestino', $trash, 'font-trash', '', 'Mostra tutti gli elementi eliminati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }



}