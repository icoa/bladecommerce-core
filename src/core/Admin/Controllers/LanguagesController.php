<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class LanguagesController extends BackendController {

    public $component_id = 44;
    public $title = 'Gestione Lingue';
    public $page = 'languages.index';
    public $pageheader = 'Gestione Lingue';
    public $iconClass = 'font-columns';
    public $model = 'Language';
    protected $rules = array(
        'language_code' => 'required|unique:languages,language_code',
        'name' => 'required',
        'language_code' => 'required',
        'date_format_lite' => 'required',
        'date_format_full' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    protected function toolbar($what = 'default') {

        global $actions;
        $actions = [];


        if ($what == 'default') {

            $flagMultiON = ($this->action("postFlagMulti", TRUE, 1));
            $flagMultiOff = ($this->action("postFlagMulti", TRUE, 0));

            $actions = array(
                new AdminAction('Abilita', $flagMultiON, 'font-ok', 'btn-warning action-multi', 'Abilita uno o più record selezionati'),
                new AdminAction('Disabilita', $flagMultiOff, 'font-off', 'btn-warning action-multi', 'Disabilita uno o più record selezionati'),
                new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
            );
        } else {

            parent::toolbar($what);
        }
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Lingue');
        $this->toFooter("js/echo/languages.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/languages.js");
        $this->page = 'languages.trash';
        $this->pageheader = 'Cestino Lingue';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/languages.js");
        $this->page = 'languages.create';
        $this->pageheader = 'Nuovo Lingua';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/languages.js");
        $this->page = 'languages.create';
        $this->pageheader = 'Modifica Lingua';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::select('id', 'name', 'language_code', 'date_format_lite', 'date_format_full', 'active', 'position', 'created_at');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->select('id', 'name', 'language_code', 'date_format_lite', 'date_format_full', 'active', 'position', 'deleted_at');

        return \Datatables::of($pages)
                        ->edit_column('deleted_at', function($data) {
                            return \Format::date($data['deleted_at']);
                        })
                        ->edit_column('name', function($data) {
                            return "<strong>{$data['name']}</strong>";
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['language_code'] = 'required|unique:languages,language_code,' . $model->id;
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;

        \Input::replace($_POST);

    }

    function postList() {
        $rows = \Mainframe::brands();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

}
