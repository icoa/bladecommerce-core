<?php

use services\Controllers\OrderControllerService;
use services\Membership\Models\Affiliate;
use services\Models\OrderTracking;
use services\Morellato\Database\Sql\Connection;
use services\Repositories\OrderFeedRepository;

class OrdersController extends BackendController
{

    private $asset_version = 8;
    public $component_id = 29;
    public $title = 'Gestione Ordini';
    public $page = 'orders.index';
    public $pageheader = 'Gestione Ordini';
    public $iconClass = 'font-columns';
    public $model = 'Order';
    protected $rules = array(
        'carrier_id' => 'required',
        'payment_id' => 'required',
        'customer_id' => 'required',
        'status' => 'required',
        'payment_status' => 'required',
        'shipping_address_id' => 'required',
        'order_date' => 'required',
    );

    protected $friendly_names = array(
        'carrier_id' => 'Metodo di spedizione',
        'payment_id' => 'Metodo di pagamento',
        'customer_id' => 'Cliente',
        'status' => 'Status ordine',
        'payment_status' => 'Status pagamento',
        'shipping_address_id' => 'Indirizzo di spedizione',
        'order_date' => 'Data ordine',
    );

    /**
     * @var OrderControllerService
     */
    protected $service;

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->service = new OrderControllerService();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        unset($this->rules['order_date']);
        $this->_prepare_update($model);
    }

    /**
     * @param Order $model
     */
    function _after_create($model)
    {
        $created_at = Format::sqlDatetime($_POST['order_date']);


        $parent = null;
        $cart_id = $model->cart_id;
        $reference = $model->reference;


        //if the created order is a "child" then the cart already exists
        if ($model->parent_id > 0) {
            $parentOrder = Order::getObj($model->parent_id);
            $cart_id = $parentOrder->cart_id;
            $parent = $model->parent_id;
            $reference = $parentOrder->reference;
            $warehouse = \OrderHelper::getWarehouseByOrder($model);
            if ($warehouse) {
                $reference .= '-' . $warehouse;
            }
            if ($model->availability_shop_id) {
                $reference .= $model->availability_shop_id;
            }
        } else {
            //create the fake cart
            $data = [
                'shop_group_id' => $model->shop_group_id,
                'shop_id' => $model->shop_id,
                'carrier_id' => $model->carrier_id,
                'payment_id' => $model->payment_id,
                'lang_id' => $model->lang_id,
                'shipping_address_id' => $model->shipping_address_id,
                'billing_address_id' => $model->billing_address_id,
                'currency_id' => $model->currency_id,
                'customer_id' => $model->customer_id,
                'secure_key' => $model->secure_key,
                'recyclable' => $model->recyclable,
                'gift' => $model->gift,
                'gift_message' => $model->gift_message,
                'notes' => $model->notes,
                'availability_mode' => $model->availability_mode,
                'availability_shop_id' => $model->availability_shop_id,
            ];

            $cart = new Cart();
            $cart->setAttributes($data);
            $cart->save();
            $reference = \OrderManager::generateReference($cart->id);
            $cart_id = $cart->id;
        }

        $orderData = [
            'created_at' => $created_at,
            'cart_id' => $cart_id,
            'reference' => $reference,
            'parent_id' => $parent,
        ];

        \DB::table('orders')->where('id', $model->id)->update($orderData);

        $model->setStatus(
            $model->status,
            true,
            'Creazione ordine manuale',
            OrderTracking::byUser()
        );
        $model->setPaymentStatus($model->payment_status, true, 'Creazione ordine manuale');
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $ratio = \Core::getDefaultTaxRate();

        $_POST['shop_group_id'] = 1;
        $_POST['shop_id'] = 1;
        $_POST['cart_id'] = 0;
        $_POST['reference'] = '';

        $_POST['billing_address_id'] = (int)$_POST['billing_address_id'];
        $_POST['secure_key'] = \Format::secure_key();
        $payment = \Payment::getObj($_POST['payment_id']);
        $_POST['payment'] = $payment->name;
        $_POST['module'] = $payment->module;
        $_POST['total_discounts'] = Format::float($_POST['total_discounts']);
        $_POST['total_discounts_tax_incl'] = $_POST['total_discounts'];
        $_POST['total_discounts_tax_excl'] = \Core::untax($_POST['total_discounts'], $ratio);
        $_POST['total_shipping'] = Format::float($_POST['total_shipping']);
        $_POST['total_shipping_tax_incl'] = $_POST['total_shipping'];
        $_POST['total_shipping_tax_excl'] = \Core::untax($_POST['total_shipping'], $ratio);
        $carrier = \Carrier::getObj($_POST['carrier_id']);
        $_POST['carrier_tax_rate'] = $carrier->getTaxRate();
        $_POST['total_payment'] = Format::float($_POST['total_payment']);
        $_POST['total_payment_tax_incl'] = $_POST['total_payment'];
        $_POST['total_payment_tax_excl'] = \Core::untax($_POST['total_payment'], $ratio);
        $_POST['total_wrapping'] = Format::float($_POST['total_wrapping']);
        $_POST['total_wrapping_tax_incl'] = $_POST['total_wrapping'];
        $_POST['total_wrapping_tax_excl'] = \Core::untax($_POST['total_wrapping'], $ratio);
        $_POST['active'] = 1;
        $_POST['valid'] = 1;
        $currency = \Currency::getObj($_POST['currency_id']);
        $_POST['conversion_rate'] = $currency->conversion_rate;


        \Input::replace($_POST);

    }


    function _prepare_update($model)
    {
        if (count($_POST) == 0) {
            return;
        }

        $obj = Order::getObj($model->id);

        \Utils::watch();
        $ratio = \Core::getDefaultTaxRate();


        $_POST['shipping_address_id'] = (int)$_POST['shipping_address_id'];
        $_POST['billing_address_id'] = (int)$_POST['billing_address_id'];

        $payment = \Payment::getObj($_POST['payment_id']);
        $_POST['payment'] = $payment->name;
        $_POST['module'] = $payment->module;
        $carrier = \Carrier::getObj($_POST['carrier_id']);

        $currency = \Currency::getObj($_POST['currency_id']);
        $_POST['conversion_rate'] = $currency->conversion_rate;

        $status = (int)$_POST['status'];
        $payment_status = (int)$_POST['payment_status'];

        if ($status != $model->status) {
            $success = $obj->updateStatus($status);
            $msg = $success ? "Aggiornamento status effettuato con successo" : "Aggiornamento status non eseguito";
            if ($success) {
                \Session::set('orderLastStatus', $status);
            }
        }

        if ($payment_status != $model->payment_status) {
            $success = $obj->updatePaymentStatus($payment_status);
            $msg = $success ? "Aggiornamento status effettuato con successo" : "Aggiornamento status non eseguito";
            if ($success) {
                \Session::set('orderLastPaymentStatus', $payment_status);
            }
        }

        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Ordini');
        $this->toFooter("js/echo/orders.{$this->asset_version}.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'orders.create';
        $this->toFooter("js/echo/orders.{$this->asset_version}.js");
        $this->pageheader = 'Nuovo Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'orders.create';
        $this->pageheader = 'Nuovo Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }


    public function getEdit($id)
    {
        $this->page = 'orders.modify';
        $this->toFooter("js/echo/orders.{$this->asset_version}.js");

        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        $this->pageheader = 'Modifica Ordine - ' . $obj->reference;
        return $this->render($view);
    }


    public function getPreview($id)
    {
        $this->page = 'orders.preview';
        $this->toFooter("js/echo/orders.{$this->asset_version}.js");
        $this->pageheader = 'Dettagli Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->expand();
        $view['obj'] = $obj;
        \AdminTpl::setData('order', $obj);
        return $this->render($view);
    }


    public function getTable()
    {
        $model = $this->model;
        $include_status_cancelled = Input::get('include_status_cancelled', 'true');

        $builder = $model::leftJoin('customers', 'customer_id', '=', 'customers.id');

        if ($include_status_cancelled == 'false') {
            $builder->where('status', '!=', OrderState::STATUS_CANCELED);
        }

        $builder->groupBy('orders.id');

        $pages = $builder
            ->select(
                'orders.id',
                'orders.reference',
                DB::raw("CONCAT_WS(' ',firstname,lastname,company) as customer_name"),
                'orders.status',
                'orders.total_quantity',
                'orders.payment',
                'orders.payment_status',
                'orders.carrier_id',
                'orders.shipping_number',
                'orders.verified_address',
                'orders.delivery_store_id',
                'orders.availability_shop_id',
                'orders.total_order',
                'orders.campaign_id',
                'orders.created_at',
                'orders.gift_box',
                'orders.affiliate_id',
                'orders.availability_mode',
                'orders.flag_warnable'
            );

        $order_states = Mainframe::selectOrderStates(false);
        $payment_states = Mainframe::selectPaymentStates(false);

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('verified_address', function ($data) {
                return $this->boolean($data, 'verified_address');
            })
            ->edit_column('reference', function ($data) {
                $link = \URL::action($this->action("getPreview"), $data['id']);
                $mode = $data['availability_mode'];
                $class = '';
                if ($mode == 'master') {
                    $class = 'label label-success';
                }
                return "<strong><a class='$class' href='$link'>{$data['reference']}</a></strong><br>{ $mode }";
            })
            ->edit_column('shipping_number', function ($data) {
                $order = \Order::getObj($data['id']);
                $url = $order->getTrackingUrl();
                $link = ($url) ? $url : 'javascript:;';
                $str = $order->shipping_number;
                if ($order->tracking_url != '') {
                    $str = 'Custom URL';
                }
                $s = "<a href='javascript:;' title='Modifica Cod. spedizione' data-id='{$data['id']}' class='trigger-popover'><i style='margin-left:10px; font-size:14px' class='font-edit'></i></a><div class='hidden'>" . Form::text("tracking[]", $data['shipping_number'], ['placeholder' => 'Scrivi qui il codice']) . "<button class='btn btn-primary order-action' data-action='setShippingNumber' data-id='{$data['id']}'>MODIFICA</button></div>";
                $has_warning = (strlen($order->tracking_url) > 0);
                $warning = ($has_warning) ? "<i class=\"font-warning-sign\" style=\"font-size:20px; margin-left:4px; vertical-align:middle; color:gold;\" title=\"Attenzione: questo codice tracking viene sovrascritto da altri valori; controllare il dettaglio ordine\"></i>" : null;
                return "<a href='$link' target='_blank'>{$str}</a>{$s}{$warning}";
            })
            ->edit_column('customer_name', function ($data) {
                //$order = \Order::getObj($data['id']);
                $s = $data['customer_name'];
                $label = $data['flag_warnable'] == 0 ? '<i class="font-check"></i> Disturbabile' : '<i class="font-warning-sign"></i> Non disturbare';
                $html = "<strong>$s</strong><br><a title='Premi per cambiare il valore di questo flag' href='javascript:;' class='order-action mini-button' data-action='toggle-flag-warnable' data-id='{$data['id']}'>$label</a>";
                return $html;
            })
            ->edit_column('gift_box', function ($data) {
                $order = \Order::getObj($data['id']);
                return $order->getFlags();
            })
            ->edit_column('carrier_id', function ($data) {
                /*$order = \Order::getObj($data['id']);
                $customer = $order->getCarrier();
                $s = ($customer) ? $customer->name : "ND";*/
                $carrier = Carrier::getPublicObj($data['carrier_id']);
                $s = ($carrier) ? $carrier->name : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('campaign_id', function ($data) {
                //$order = \Order::getObj($data['id']);
                //$customer = $order->getCampaign();
                $campaign = Campaign::getPublicObj($data['campaign_id']);
                $s = '';
                if ($campaign) {
                    $src = \AdminTpl::img("images/flags/{$campaign->lang_id}.jpg");
                    $s = "<img src='$src' /> $campaign->name";
                }
                return $s;
            })
            ->edit_column('status', function ($data) use ($order_states) {
                /*$order = \Order::getObj($data['id']);
                $status = $order->getStatus();*/
                $status = OrderState::getPublicObj($data['status']);
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title=\"{$status->name}\"><i class='{$status->icon}'></i></span>" : "ND";

                $s .= "<a href='javascript:;' title='Modifica Status ordine' data-id='{$data['id']}' class='trigger-popover'><i style='margin-left:10px; font-size:14px' class='font-edit'></i></a><div class='hidden'>" . Form::select("status[]", $order_states, $data['status'], ['class' => 'order-option', 'data-action' => 'setStatusButton', 'data-original' => $data['status']]) . "<button disabled class='btn btn-primary order-action' data-action='setStatus' data-id='{$data['id']}'>MODIFICA</button></div>";

                return $s;
            })
            ->edit_column('payment_status', function ($data) use ($payment_states) {
                /*$order = \Order::getObj($data['id']);
                $status = $order->getPaymentStatus();*/
                $status = PaymentState::getPublicObj($data['payment_status']);
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title=\"{$status->name}\"><i class='{$status->icon}'></i></span>" : "ND";

                $s .= "<a href='javascript:;' title='Modifica Status Pagam.' data-id='{$data['id']}' class='trigger-popover'><i style='margin-left:10px; font-size:14px' class='font-edit'></i></a><div class='hidden'>" . Form::select("payment_status[]", $payment_states, $data['payment_status'], ['class' => 'order-option', 'data-action' => 'setStatusButton', 'data-original' => $data['status']]) . "<button disabled class='btn btn-primary order-action' data-action='setPaymentStatus' data-id='{$data['id']}'>MODIFICA</button></div>";

                return $s;
            })
            ->edit_column('total_quantity', function ($data) {
                $s = "<strong>" . $data['total_quantity'] . "</strong>";
                return $s;
            })
            ->edit_column('total_order', function ($data) {
                $order = \Order::getObj($data['id']);
                return "<strong>" . $order->getGrandTotalAttribute() . "</strong>";
            })
            ->edit_column('delivery_store_id', function ($data) {
                if ($data['delivery_store_id'] > 0) {
                    //$order = \Order::getObj($data['id']);
                    //$store = $order->getDeliveryStore();
                    $store = MorellatoShop::getPublicObj($data['delivery_store_id']);
                    if ($store) {
                        $id = $store->id;
                        $name = $store->name;
                        return "<a href='/admin/morellato_shops/edit/$id' target='_blank'>$name</a>";
                    }
                }
                return null;
            })
            ->edit_column('availability_shop_id', function ($data) {
                if ($data['availability_shop_id'] != '') {
                    $order = \Order::getObj($data['id']);
                    $store = $order->getAvailabilityShop();
                    if ($store) {
                        $id = $store->id;
                        $name = $store->name;
                        return "<a href='/admin/morellato_shops/edit/$id' target='_blank'>$name</a>";
                    }
                }
                return null;
            })
            ->edit_column('affiliate_id', function ($data) {
                if ($data['affiliate_id'] > 0) {
                    $order = \Order::getObj($data['id']);
                    $store = $order->getAffiliate();
                    if ($store) {
                        $id = $store->id;
                        $name = $store->name;
                        return "<a href='/admin/affiliates/edit/$id' target='_blank'>$name</a>";
                    }
                }
                return null;
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function postSetTracking($order_id)
    {
        $number = \Input::get('number', null);
        $obj = Order::getObj($order_id);
        $obj->shipping_number = (trim($number));
        $obj->save();
        $success = true;
        return Json::encode(compact('success', 'number'));
    }

    function postSetGiftBox($order_id)
    {
        $input = \Input::get('input', null);
        $obj = Order::getObj($order_id);
        $input = Str::upper(trim($input));
        $gift = 1;
        if ($input == '') {
            $input = null;
            $gift = 0;
        }
        $obj->gift_box = $input;
        $obj->gift = $gift;
        $obj->save();
        $success = true;
        return Json::encode(compact('success', 'input'));
    }

    function postSetDirectName($order_id)
    {
        $input = \Input::get('input', null);
        $obj = Order::getObj($order_id);
        $input = Str::upper(trim($input));
        $gift = 1;
        if ($input == '') {
            $input = null;
            $gift = 0;
        }
        $obj->direct_name = $input;
        $obj->recyclable = $gift;
        $obj->save();
        $success = true;
        return Json::encode(compact('success', 'input'));
    }


    function postSetAddress($order_id)
    {
        $address_id = \Input::get('address_id', null);
        $billing = \Input::get('billing', 0);
        $obj = Order::getObj($order_id);
        if ($billing == 1) {
            if ($address_id == $obj->shipping_address_id) {
                $obj->billing_address_id = 0;
            } else {
                $obj->billing_address_id = $address_id;
            }
        } else {
            $obj->shipping_address_id = $address_id;
        }
        $obj->save();
        $success = true;
        return Json::encode(compact('success'));
    }


    function postSetStatus($order_id)
    {
        $status_id = \Input::get('status_id', null);
        $order = \Input::get('order', 0);
        $payload = \Input::get('payload', null);
        /** @var Order $obj */
        $obj = Order::getObj($order_id);
        $success = true;
        if ($order == 1) {
            $success = $obj->updateStatus($status_id, $payload);
            $msg = $success ? 'Aggiornamento status effettuato con successo' : 'Aggiornamento status non eseguito';
            if ($success) {
                \Session::set('orderLastStatus', $status_id);
            }
        } else {
            $success = $obj->updatePaymentStatus($status_id);
            $msg = $success ? "Aggiornamento status pagamento effettuato con successo" : "Aggiornamento status pagamento non eseguito";
            if ($success) {
                \Session::set('orderLastPaymentStatus', $status_id);
                $obj->handlePaymentStatusUpdateEmail();
            }
        }
        //audit(compact('status_id', 'payload'), __METHOD__);
        return Json::encode(compact('success', 'msg'));
    }


    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['new', 'reload']]);
        $export = ($this->action("getExport", TRUE));
        $user = Sentry::getUser();
        if ($user->hasPermission('export')) {
            $actions[] = new AdminAction('Esporta', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');
        }
        return $actions;
    }


    function getExport()
    {
        $this->page = 'orders.export';
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
        }
        $this->toFooter("js/echo/orders.{$this->asset_version}.js");
        $this->pageheader = 'Esportazione Ordini';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('export');
        $view = array();
        return $this->render($view);
    }


    function getDownload()
    {
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
            $view = array();
            return $this->render($view);
        }
        \Utils::watch();
        ini_set('max_execution_time', 600);

        $from = Input::get('from', '');
        $to = Input::get('to', '');
        $lang_id = Input::get('lang_id', '');
        $status = Input::get('status', []);
        $payment_status = Input::get('payment_status', []);
        $carriers = Input::get('carriers', []);
        $export_mode = Input::get('export_mode', 'default');

        $query = \Order::whereNull('parent_id')->orderBy('id', 'desc');
        if ($from != '') {
            $from = Format::sqlDate($from);
            $query->where('created_at', '>=', $from);
        }
        if ($to != '') {
            $to = Format::sqlDate($to);
            $query->where('created_at', '<=', $to);
        }
        if ($lang_id != '') {
            $query->where('lang_id', $lang_id);
        }
        if (count($status) > 0) {
            $query->whereIn('status', $status);
        }
        if (count($payment_status) > 0) {
            $query->whereIn('payment_status', $payment_status);
        }
        if (count($carriers) > 0) {
            $query->whereIn('carrier_id', $carriers);
        }

        $method = 'export_' . $export_mode;
        return $this->$method($query);
    }


    private function export_default($query)
    {
        $rows = $query->get();
        $lines = ['N. ordine;Data;Nome;Cognome;Email;ID Cliente;Citta;Provincia;CAP;Nazione;Codice Fiscale;Campagna;Coupon;Stato ordine;Pagamento;Stato pagamento;Spedizione;Tot Ordine;Sku prodotto;Nome Prodotto;Prezzo listino;Prezzo base;Prezzo finale;Prodotto disponibile;Numero prodotti;Brand;Genere'];
        foreach ($rows as $row) {

            $customer = $row->getCustomer();
            $address = $row->getShippingAddress();
            $campaignObj = $row->getCampaign();
            $statusObj = $row->getStatus();
            $paymentStatusObj = $row->getPaymentStatus();
            $carrierObj = $row->getCarrier();
            $paymentObj = $row->getPayment();
            $products = \OrderDetail::where('order_id', $row->id)->get();

            $firstname = ($customer) ? $customer->firstname : '';
            $lastname = ($customer) ? $customer->lastname : '';
            $email = ($customer) ? $customer->email : '';
            $customer_id = $row->customer_id;
            if ($customer AND $customer->people_id == 2) {
                $firstname = $customer->name;
                $lastname = '';
            }
            $status = $statusObj ? $statusObj->name : '';
            $paymentStatus = $paymentStatusObj ? $paymentStatusObj->name : '';
            $carrier = $carrierObj ? $carrierObj->name : '';
            $payment = $paymentObj ? $paymentObj->name : '';
            $campaign = $campaignObj ? $campaignObj->name : '';


            foreach ($products as $p) {

                $product = Product::getObj($p->product_id);
                if ($product) {

                    $product->setFullData();

                    $data = [];
                    $data[] = $row->reference;
                    $data[] = \Format::lang_date($row->created_at);
                    $data[] = $firstname;
                    $data[] = $lastname;
                    $data[] = $email;
                    $data[] = $customer_id;
                    $data[] = $address->city;
                    $data[] = $address->stateName;
                    $data[] = $address->postcode;
                    $data[] = $address->countryName;
                    $data[] = $address->cf;
                    $data[] = $campaign;
                    $data[] = $row->coupon_code;
                    $data[] = $status;
                    $data[] = $payment;
                    $data[] = $paymentStatus;
                    $data[] = $carrier;
                    $data[] = Format::currency($row->total_order);
                    //Sku prodotto|Nome Prodotto|Marca Prodotto|Categoria I livello Prodotto|Categoria II livello Prodotto|Prezzo listino|Prezzo base|Prezzo finale|Prodotto disponibile|Numero prodotti
                    $data[] = $p->product_reference;
                    $data[] = $p->product_name;
                    $data[] = Format::currency($product->sell_price_wt);
                    $data[] = Format::currency($p->product_sell_price_wt);
                    $data[] = Format::currency($p->cart_price_tax_incl);
                    $data[] = $p->product_quantity_in_stock;
                    $data[] = $p->product_quantity;
                    $data[] = $product->brand_name;
                    $data[] = $product->gender;

                    $lines[] = implode(";", $data);

                }

            }

        }

        $file = storage_path() . '/temp.csv';

        \File::put($file, implode(PHP_EOL, $lines));

        $filename = "ordini_default_" . time() . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }


    private function export_feedaty($query)
    {
        $rows = $query->get();
        $lines = ['"Order ID","UserID","E-mail","Date","Product ID","Name","Price","Qty","Brand","Url","Image"'];
        foreach ($rows as $row) {
            $customer = $row->getCustomer();
            $products = \OrderDetail::where('order_id', $row->id)->get();
            $email = ($customer) ? $customer->email : '';
            $customer_id = $row->customer_id;

            foreach ($products as $p) {

                $product = \Product::getObj($p->product_id);
                if ($product) {
                    $product->setFullData();
                    $data = [];
                    $data[] = $row->reference;
                    $data[] = $customer_id;
                    $data[] = $email;
                    $data[] = \Format::lang_date($row->created_at, 'full');

                    $data[] = $p->product_id;
                    $data[] = $p->product_name;
                    $data[] = $p->cart_price_tax_incl;
                    $data[] = $p->product_quantity;
                    $data[] = $product->brand_name;
                    $data[] = $product->link_absolute;
                    $data[] = $product->defaultImg;
                    $lines[] = '"' . implode('","', $data) . '"';
                }

            }

        }

        $file = storage_path() . '/feedaty.csv';

        \File::put($file, implode(PHP_EOL, $lines));

        $filename = "feedaty_" . time() . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }


    private function export_gls($query)
    {
        $rows = $query->get();
        $headers = [
            "Consignor n°",
            null,
            "Destination Name",
            null,
            null,
            "Destination Street",
            "Destination House n°",
            null,
            "Destination Zip code",
            "Destination City",
            "Destination Country",
            null,
            null,
            null,
            null,
            null,
            null,
            "N° of shipment units",
            "Weight",
            "Package",
        ];
        $lines = ['"' . implode('","', $headers) . '"'];

        foreach ($rows as $row) {
            $customer = $row->getCustomer();
            $products = \OrderDetail::where('order_id', $row->id)->get();
            $address = $row->getShippingAddress();


            $data = [];
            $data[] = $row->reference;
            $data[] = null;
            $data[] = $customer->getName();
            $data[] = null;
            $data[] = null;
            $data[] = trim(rtrim($address->address, $address->address2));
            $data[] = $address->address2;
            $data[] = null;
            $data[] = $address->postcode;
            $data[] = $address->city;
            $data[] = $address->countryCode();
            $data[] = null;
            $data[] = null;
            $data[] = null;
            $data[] = null;
            $data[] = null;
            $data[] = null;
            $data[] = count($products);
            $data[] = $row->total_weight;
            $data[] = 'PCO';

            $lines[] = '"' . implode('","', $data) . '"';

        }

        $file = storage_path() . '/gls.csv';
        $content = implode(PHP_EOL, $lines);
        $content = utf8_decode($content);
        \File::put($file, $content);

        $filename = "gls_" . time() . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }


    function postAddProduct($order_id)
    {
        $product_id = \Input::get('product_id', null);
        $product_price = \Input::get('product_price', null);
        $product_quantity = \Input::get('product_quantity', 1);
        $product_combination_id = \Input::get('product_combination_id', 0);

        \Utils::log(\Input::all(), __METHOD__);

        $order = Order::getObj($order_id);
        $product = Product::getObj($product_id);
        $success = true;
        if ($order == null OR $product == null) {
            $success = false;
            $msg = "Could not execute update";
        } else {
            try {
                $order->addProduct($product_id, \Format::float($product_price), (int)$product_quantity, (int)$product_combination_id);
            } catch (Exception $e) {
                $success = false;
                $msg = $e->getMessage();
            }

        }
        return Json::encode(compact('success', 'msg'));
    }


    function postRemoveProduct($detail_id)
    {
        $obj = OrderDetail::find($detail_id);
        if ($obj) {
            $order = Order::find($obj->order_id);
            $order->removeDetail($obj);
            $success = true;
        } else {
            $success = false;
            $msg = "Could not execute update";
        }
        return Json::encode(compact('success', 'msg'));
    }


    function getInvoice($order_id)
    {
        $order = Order::getObj($order_id);
        $invoice = $order->getInvoice();
        if ($invoice) {
            $invoice = (object)$invoice;
            $data = [
                'document_type' => 'invoice',
                'document_number' => $invoice->id,
                'document_date' => Format::date($invoice->date),
                'document_notes' => $invoice->notes
            ];
        } else {
            $data = [
                'document_type' => 'invoice',
                'document_number' => \Core::getInvoiceNumber(),
                'document_date' => date("d-m-Y"),
                'document_notes' => ''
            ];
        }
        $success = true;
        $html = Theme::partial('order.document', ['obj' => $data]);
        return Json::encode(compact('success', 'html'));
    }


    function getDelivery($order_id)
    {
        $order = Order::getObj($order_id);
        $invoice = $order->getDelivery();
        if ($invoice) {
            $invoice = (object)$invoice;
            $data = [
                'document_type' => 'delivery',
                'document_number' => $invoice->id,
                'document_date' => Format::date($invoice->date),
                'document_notes' => $invoice->notes
            ];
        } else {
            $data = [
                'document_type' => 'delivery',
                'document_number' => \Core::getDeliveryNumber(),
                'document_date' => date("d-m-Y"),
                'document_notes' => ''
            ];
        }
        $success = true;
        $html = Theme::partial('order.document', ['obj' => $data]);
        return Json::encode(compact('success', 'html'));
    }


    function postDocument($order_id)
    {
        $params = Input::all();
        $success = true;
        $msg = '';


        $order_check = Order::where($params['type'] . '_number', $params['number'])->first();
        if ($order_check AND $order_check->id != $order_id) {
            $success = false;
            $msg = "Esiste già un documento con questo numero in un altro ordine";
        }

        if ($success) {
            try {
                OrderHelper::createDocument($order_id, $params);
            } catch (Exception $e) {
                audit($e->getMessage(), __METHOD__);
                audit($e->getTraceAsString(), __METHOD__);
                $msg = $e->getMessage();
                $success = false;
            }
        }


        return Json::encode(compact('success', 'msg'));
    }


    function postUpdateOrder($order_id)
    {
        \Utils::watch();
        $data = Input::all();
        $success = false;
        $msg = '';

        $order = Order::find($order_id);
        $tax_rate = \Core::getDefaultTaxRate();

        if ($order) {
            $discount = (float)$data['fixed_discounts'];
            $wrapping = (float)$data['total_wrapping'];
            $shipping = (float)$data['total_shipping'];
            $payment = (float)$data['total_payment'];

            $order->delivery_date = Format::date($data['delivery_date'], false);
            $order->availability_mode = $data['availability_mode'];
            $order->availability_shop_id = $data['availability_shop_id'];
            $order->delivery_store_id = $data['delivery_store_id'];
            if (feats()->receipts()) {
                $order->invoice_required = $data['invoice_required'];
            }

            $paymentObj = \Payment::getObj($data['payment_id']);
            if ($paymentObj) {
                $order->payment_id = $paymentObj->id;
                $order->payment = $paymentObj->name;
                $order->module = $paymentObj->module;
            }

            if ($discount != 0) {
                $order->fixed_discounts = $discount;
            }
            if ($wrapping != 0) {
                $order->total_wrapping = $wrapping;
                $order->total_wrapping_tax_incl = $wrapping;
                $order->total_wrapping_tax_excl = \Core::untax($wrapping, $tax_rate);
            }
            if ($shipping != 0) {
                $order->total_shipping = $shipping;
                $order->total_shipping_tax_incl = $shipping;
                $order->total_shipping_tax_excl = \Core::untax($shipping, $tax_rate);
            }
            if ($payment != 0) {
                $order->total_payment = $payment;
                $order->total_payment_tax_incl = $payment;
                $order->total_payment_tax_excl = \Core::untax($payment, $tax_rate);
            }
            $order->updateTotals();
            $success = true;
        } else {
            $msg = 'Ordine non trovato';
        }
        return Json::encode(compact('success', 'msg'));
    }


    function postNotes($order_id)
    {
        \Utils::watch();
        $data = Input::all();
        $success = false;
        $msg = '';

        $order = Order::find($order_id);
        $notes_internal = $data['notes_internal'];

        if ($order) {
            Order::where('id', $order_id)->update(['notes_internal' => $notes_internal]);
            $success = true;
        } else {
            $msg = 'Ordine non trovato';
        }
        return Json::encode(compact('success', 'msg'));
    }


    function postSaveInlineProduct()
    {
        $detail_id = \Input::get('order_details_id');
        $obj = OrderDetail::find($detail_id);
        if ($obj) {
            $data = \Input::all();
            unset($data['order_details_id']);
            $obj->fill($data);
            $obj->save();
            $success = true;
        } else {
            $success = false;
            $msg = "Could not execute update";
        }
        return Json::encode(compact('success', 'msg'));
    }


    function getDownloadXml($order_id)
    {
        $order = Order::getObj($order_id);
        $path = $order->getSapXmlFile();
        if ($path) {
            return Response::download($path);
        }
    }


    function postIncreaseReference($order_id)
    {
        $order = Order::find($order_id);
        $success = false;
        $msg = 'Ordine non trovato';
        $payload = \Input::get('payload', null);
        if ($order) {
            $reference = OrderManager::increaseReference($order->reference);
            $order->reference = $reference;
            $order->invoice_code = null;
            $order->shipping_number = null;
            $order->tracking_url = null;
            $order->save(['timestamps' => false]);
            $order->updateStatus(OrderState::STATUS_NEW, $payload);

            \DB::table('mi_orders_slips')->where('order_id', $order_id)->delete();
            $success = true;
        }
        return Json::encode(compact('success', 'msg'));
    }


    private function export_affiliate($query)
    {
        $rows = $query->get();
        $headers = "N. ordine
Data
Nome
Cognome
Email
ID Cliente
Citta
Provincia
C.A.P.
Nazione
Codice Fiscale
Campagna
Coupon
Stato ordine
Pagamento
Stato pagamento
Spedizione
Ritiro in negozio
Tot Ordine
Sku prodotto
Nome Prodotto
Prezzo base
Prezzo finale
IVA
Prodotto disponibile
Numero prodotti
Brand
Genere
Paternita (affiliato)
Costo commissione Pagamento
Contributo di spedizione";

        $header = implode(';', explode(PHP_EOL, $headers));
        $lines = [$header];
        foreach ($rows as $row) {

            $customer = $row->getCustomer();
            $address = $row->getShippingAddress();
            $campaignObj = $row->getCampaign();
            $statusObj = $row->getStatus();
            $paymentStatusObj = $row->getPaymentStatus();
            $carrierObj = $row->getCarrier();
            $paymentObj = $row->getPayment();
            $products = \OrderDetail::where('order_id', $row->id)->get();

            $firstname = ($customer) ? $customer->firstname : '';
            $lastname = ($customer) ? $customer->lastname : '';
            $email = ($customer) ? $customer->email : '';
            $customer_id = $row->customer_id;
            if ($customer AND $customer->people_id == 2) {
                $firstname = $customer->name;
                $lastname = '';
            }
            $status = $statusObj ? $statusObj->name : '';
            $paymentStatus = $paymentStatusObj ? $paymentStatusObj->name : '';
            $carrier = $carrierObj ? $carrierObj->name : '';
            $payment = $paymentObj ? $paymentObj->name : '';
            $campaign = $campaignObj ? $campaignObj->name : '';


            foreach ($products as $p) {

                $product = Product::getObj($p->product_id);
                if ($product) {

                    $product->setFullData();

                    $data = [];

                    $data[] = $row->reference;  //N. ordine
                    $data[] = \Format::lang_date($row->created_at); //Data
                    $data[] = $firstname;   //Nome
                    $data[] = $lastname;    //Cognome
                    $data[] = $email;       //Email
                    $data[] = $customer_id; //Customer ID
                    $data[] = $address->city;   //Citta
                    $data[] = $address->stateName;  //Provincia
                    $data[] = $address->postcode;   //CAP
                    $data[] = $address->countryName;    //Nazione
                    $data[] = $address->cf; //CF
                    $data[] = $campaign;    //Campagna
                    $data[] = $row->coupon_code;    //Codice coupon
                    $data[] = $status;  //Stato ordine
                    $data[] = $payment; //Pagamento
                    $data[] = $paymentStatus;   //Stato pagamento
                    $data[] = $carrier; //Spedizione

                    //customization here
                    $shop_withdraw = '';
                    if ($row->hasDeliveryStore()) {
                        $store = $row->getDeliveryStore();
                        if ($store) {
                            $shop_withdraw = $store->cd_neg;
                        }
                    }
                    $data[] = $shop_withdraw; //Ritiro in negozio
                    $data[] = Format::currency($row->total_order); //Tot. Ordine

                    $data[] = $p->product_reference; //Sku prodotto
                    $data[] = $p->product_name; //Nome prodotto
                    $data[] = Format::currency($product->sell_price_wt); //Prezzo base
                    //$data[] = Format::currency($p->product_sell_price_wt);
                    $data[] = Format::currency($p->cart_price_tax_incl); //Prezzo finale
                    //IVA
                    $data[] = $p->tax_name; //Prodotto disponibile
                    $data[] = $p->product_quantity_in_stock; //Prodotto disponibile
                    $data[] = $p->product_quantity; //Numero prodotti
                    $data[] = $product->brand_name; //Brand
                    $data[] = $product->gender; //Genere

                    $affiliate = Affiliate::getObj($p->affiliate_id);
                    $data[] = ($affiliate) ? $affiliate->name : null;   //Paternitá (affiliato)

                    $product_lines = count($products);

                    //Contributo di spedizione = valore totale delle spedizioni addebitato al cliente in fase di ordine (costo della spedizione) / numero di righe dell'ordine
                    $shipment_contribute = $row->total_shipping / $product_lines;

                    //Costo commissione Pagamento = (Prezzo finale + Contributo di spedizione) * 0,015
                    $payment_cost = ($p->cart_price_tax_incl + $shipment_contribute) * 0.015;

                    $data[] = $payment_cost;
                    $data[] = $shipment_contribute;

                    $lines[] = implode(";", $data);

                }

            }

        }

        $file = storage_path() . '/temp.csv';

        \File::put($file, implode(PHP_EOL, $lines));

        $filename = "ordini_affiliazione_" . time() . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }


    function postMakeStarling($id)
    {
        $data = Input::all();
        audit($data, __METHOD__);
        /* @var $order Order */
        $order = Order::find($id);

        $url = null;
        $msg = null;
        $success = false;

        if ($order) {
            if ($order->canCreateStarling()) {
                audit($order->id, 'Proceed to create a starling for order');

                try {
                    //create the starling order
                    $starlingOrder = $order->createStarling();

                    if (is_null($starlingOrder)) {
                        throw new Exception('Could not create starling order');
                    }
                    audit($order->id, 'Starling order/document created');
                    //redirect the back-office user to the new starling order
                    $url = $starlingOrder->backend_link;
                    $msg = 'Storno creato con successo';
                    $success = true;

                    //if wanted, create another order with product details
                    if (Input::get('only_starling', 1) == 0) {
                        //create the info matrix for order details + quantities
                        $details = [];
                        $starling_products = Input::get('starling_products', []);
                        $starling_products_quantities = Input::get('starling_products_quantities', []);
                        foreach ($starling_products as $starling_product) {
                            $details[$starling_product] = $starling_products_quantities[$starling_product];
                        }
                        audit($details, __METHOD__ . '::product matrix details');

                        $newOrder = OrderManager::createOrderWithDetails($order, $details);

                        if (is_null($newOrder)) {
                            throw new Exception('Could not create the new order');
                        }

                        $url = $newOrder->backend_link;
                        $msg = 'Ordine creato con successo';
                        $success = true;
                    }

                } catch (Exception $e) {
                    audit_exception($e, __METHOD__);
                    audit_error($data, __METHOD__);
                    $msg = "Impossibile completare l'operazione: errore interno";
                    $success = false;
                }

            } else {
                $msg = 'Impossibile creare storno - requisiti interni non soddisfatti';
            }
        }

        return Json::encode(compact('success', 'msg', 'url'));
    }


    /**
     * @param $order_id
     * @return string
     */
    function postSetReceipt($order_id)
    {
        $receipt = strtoupper(trim(\Input::get('code', null)));
        $valid = false;
        $code = null;
        $error = null;
        $success = false;
        $obj = Order::getObj($order_id);

        //validation
        $valid = OrderHelper::isReceiptValid($obj, $receipt);

        if ($valid === true) {
            try {

                $obj->receipt = (trim($receipt));

                if ($obj->receipt == '')
                    $obj->receipt = null;

                $obj->save();
                $code = $obj->getLocalReceiptFeeHtmlAttribute();
                $success = true;
            } catch (Exception $e) {
                $error = $e->getMessage();
            }
        } else {
            $error = "Codice non conforme alle specifiche per caratteri e/o lunghezza";
        }

        return Json::encode(compact('success', 'code', 'error'));
    }


    /**
     * @param $attribute
     * @param $order_id
     * @return string
     */
    function postSetInputCode($attribute, $order_id)
    {
        $msg = null;
        $success = true;
        $code = trim(\Input::get('code', null));
        $obj = Order::getObj($order_id);
        $valid_attributes = ['tracking_url', 'carrier_shipping'];
        try {
            if (!in_array($attribute, $valid_attributes)) {
                throw new Exception("Could not change attribute [$attribute]; it is not in the white list");
            }
            $obj->setAttribute($attribute, $code);
            $obj->save();
        } catch (Exception $e) {
            $success = false;
            $msg = $e->getMessage();
        }

        return Json::encode(compact('success', 'code', 'msg'));
    }


    function getOrderFlowFileContent($order_id)
    {
        $obj = Order::getObj($order_id);
        return $this->service->parseOrderFlowResponseData($obj);
    }


    /**
     * @param $order_id
     * @return string
     */
    function postToggleFlagWarnable($order_id)
    {
        $msg = null;
        $success = true;

        $obj = Order::getObj($order_id);
        $attribute = $obj->flag_warnable == 0 ? 1 : 0;
        try {
            $obj->setAttribute('flag_warnable', $attribute);
            $obj->save();
        } catch (Exception $e) {
            $success = false;
            $msg = $e->getMessage();
        }

        return Json::encode(compact('success', 'msg'));
    }


    function getOptions()
    {
        //\Utils::watch();
        $q = \Input::get("q");
        $page_limit = \Input::get("page_limit");
        $page = \Input::get("page");
        $start = ($page - 1) * $page_limit;
        $master = \Input::get('master') == 1;

        $builder = Order::with('customer')->select(['id', 'reference', 'customer_id', 'availability_mode'])
            ->where(function ($query) use ($q) {
                $query->where('reference', 'like', "%$q%");

                if (is_numeric($q) and $q > 0) {
                    $query->orWhere('id', $q);
                }
            });

        $start_id = \Config::get('soap.order.startId');

        if ($master)
            $builder->whereNull('parent_id')->where('id', '>', $start_id)->where('availability_mode', 'master');

        $count_builder = clone $builder;

        $total = $count_builder->count('id');

        $rows = $builder
            ->orderBy('reference')
            ->take($page_limit)
            ->skip($start)
            ->get();

        $count = count($rows);
        for ($i = 0; $i < $count; $i++) {
            $customer_name = $rows[$i]->getCustomer()->name;
            $rows[$i]->name = "#{$rows[$i]->id} - {$rows[$i]->reference} ($customer_name)";
            unset($rows[$i]->customer);
        }
        $success = true;
        $records = $rows;
        return Json::encode(compact("total", "success", "records"));
    }

    function getSingleoption()
    {
        //\Utils::watch();
        $id = \Input::get("id");

        $row = Order::find($id);
        if ($row) {
            $customer_name = $row->getCustomer()->name;
            $row->name = "#{$row->id} - {$row->reference} ($customer_name)";
        }

        $record = $row;
        return Json::encode(compact("record"));
    }


    public function getBestshop()
    {
        $id = Input::get('id');
        /** @var Order|null $order */
        $order = ($id > 0) ? Order::getObj($id) : null;
        if ($order) {
            //build params for order
            $params = [
                'delivery_store_id' => $order->delivery_store_id,
                'avoid_stores' => [],
            ];

            $products = $order->getProducts();
            $index = 0;
            foreach ($products as $product) {
                if ($product->isModeShop()) {
                    $params['products_' . $index . '_sku'] = $product->getSapSku();
                    $params['products_' . $index . '_qty'] = $product->product_quantity;
                    if ($product->availability_shop_id != '') {
                        $store = MorellatoShop::getByCode($product->availability_shop_id);
                        if ($store) {
                            $params['avoid_stores'][] = $store->id;
                        }
                    }
                    $index++;
                }
            }
            $params['avoid_stores'] = implode(',', $params['avoid_stores']);
            $http = http_build_query($params);
            return Redirect::to('/admin/orders/bestshop?' . $http);
        }
        $this->page = 'orders.best_shop';
        $this->toFooter("js/echo/orders.{$this->asset_version}.js");
        $this->pageheader = 'Simula Best Shop';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array('search_done' => 0);

        //calculation
        $max_items = 10;
        $products = [];
        for ($i = 0; $i < $max_items; $i++) {
            $sku = trim(Input::get('products_' . $i . '_sku'));
            $qty = (int)Input::get('products_' . $i . '_qty', 1);
            if ($sku !== '') {
                $products[$sku] = $qty;
            }
        }
        if (!empty($products)) {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();

            $preferred = trim(Input::get('delivery_store_id'));
            if ($preferred !== '') {
                $shop = MorellatoShop::getObj($preferred);
                if ($shop)
                    $conn->addPreferredShop($shop->code);
            }

            $unused = Input::get('avoid_stores');
            if ($unused !== '') {
                if (!is_array($unused)) {
                    $unused = explode(',', $unused);
                }
                foreach ($unused as $u) {
                    $shop = MorellatoShop::getObj($u);
                    if ($shop)
                        $conn->addUnusedShop($shop->code);
                }
            }

            $product_id = 1000;
            foreach ($products as $sku => $qty) {
                $conn->addProduct($sku, $qty, $product_id);
                $product_id++;
            }

            $view['search_done'] = 1;
            $view['best_shop'] = $conn->getBestShop();
        }

        return $this->render($view);
    }

    /**
     * @param $filepath
     * @return \Illuminate\Http\Response
     */
    private function download_csv($filepath){
        $filename = last(explode('/', $filepath));
        return Response::make(File::get($filepath), 200, ['content-type' => 'text/csv', 'Content-Disposition' => 'attachment; filename=' . $filename]);
    }

    private function export_best_shop($query)
    {
        $feeder = new OrderFeedRepository();
        $filepath = $feeder->bestShops($query);
        return $this->download_csv($filepath);
    }

    private function export_gls_new($query)
    {
        $feeder = new OrderFeedRepository();
        $filepath = $feeder->gls($query);
        return $this->download_csv($filepath);
    }

    private function export_working($query)
    {
        $feeder = new OrderFeedRepository();
        $filepath = $feeder->working($query);
        return $this->download_csv($filepath);
    }

    private function export_working_slips($query)
    {
        $feeder = new OrderFeedRepository();
        $filepath = $feeder->workingWithoutSlips($query);
        return $this->download_csv($filepath);
    }

    private function export_master_auto($query)
    {
        $feeder = new OrderFeedRepository();
        $filepath = $feeder->masterOrdersAutoStatus($query);
        return $this->download_csv($filepath);
    }

}