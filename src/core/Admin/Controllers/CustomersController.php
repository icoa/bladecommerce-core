<?php

class CustomersController extends MultiLangController
{

    public $component_id = 68;
    public $title = 'Gestione Clienti';
    public $page = 'customers.index';
    public $pageheader = 'Gestione Clienti';
    public $iconClass = 'font-cog';
    public $model = 'Customer';
    protected $rules = array(
        'people_id' => 'required',
        'email' => 'required|email|unique:customers,email,NULL,id,active,1',
    );
    protected $lang_rules = array();
    protected $friendly_names = array(
        'people_id' => 'Tipologia',
        'firstname' => 'Nome',
        'lastname' => 'Cognome',
        'company' => 'Rag. sociale',
        'birthdate' => 'Data di nascita',
        'gender_id' => 'Sesso',
    );
    protected $lang_friendly_names = array();

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Clienti');
        $this->toFooter("js/echo/customers.js?v=2");

        $this->pageheader = 'Gestione Clienti<a style="margin:0 0 0 20px" class="btn btn-mini" href="/admin/customers/groups">Visualizza Gruppi Cliente</a>';
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/customers.js");
        $this->page = 'customers.trash';
        $this->pageheader = 'Cestino Messaggi email';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/customers.js");
        $this->page = 'customers.create';
        $this->pageheader = 'Nuovo Cliente';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/customers.js");
        $this->page = 'customers.create';
        $this->pageheader = 'Modifica Cliente';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin("peoples_lang", "customers.people_id", "=", "peoples_lang.people_id")
            ->leftJoin("customers_groups_lang", "customers.default_group_id", "=", "customers_groups_lang.customer_group_id")
            ->leftJoin("customers_groups_lang as cgl", "customers.alt_group_id", "=", "cgl.customer_group_id")
            ->where("customers_groups_lang.lang_id", $lang_id)
            ->where("peoples_lang.lang_id", $lang_id)
            ->groupBy("customers.id")
            ->select(
                'customers.id',
                'customers.people_id',
                DB::raw("CONCAT_WS(' ',customers.firstname,customers.lastname,customers.company) as customer_name"),
                'customers.email',
                'default_group_id',
                'guest',
                'active',
                'newsletter',
                'profile',
                'marketing',
                'risk_id',
                'siret',
                'customers.created_at',
                'alt_group_id',
                'lastname',
                'firstname',
                'company',
                'customers_groups_lang.name as groupname',
                'cgl.name as altgroupname',
                'peoples_lang.name as peoplename'
            );

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('default_group_id', function ($data) {
                $v = $data['groupname'];
                return "<strong>$v</strong>";
            })
            ->edit_column('alt_group_id', function ($data) {
                $v = $data['altgroupname'];
                return "<strong>$v</strong>";
            })
            ->edit_column('customer_name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $name = ($data["people_id"] == 2) ? $data['company'] : $data['firstname'] . " " . $data['lastname'];
                return "<strong><a href='$link'>{$name}</a></strong>";
            })
            ->edit_column('newsletter', function ($data) {
                return $this->boolean($data, 'newsletter');
            })
            ->edit_column('guest', function ($data) {
                return $this->boolean($data, 'guest');
            })
            ->edit_column('profile', function ($data) {
                return $this->boolean($data, 'profile');
            })
            ->edit_column('marketing', function ($data) {
                return $this->boolean($data, 'marketing');
            })
            ->edit_column('active', function ($data) {
                return $this->boolean($data, 'active');
            })
            ->edit_column('risk_id', function ($data) {
                $obj = \Customer::getObj($data['id']);
                $s = $obj->getTotalOrdersCount();
                return "<strong>$s</strong>";
            })
            ->edit_column('siret', function ($data) {
                $obj = \Customer::getObj($data['id']);
                $s = $obj->getTotalAddressCount();
                return "<strong>$s</strong>";
            })
            ->edit_column('people_id', function ($data) {
                $v = $data['peoplename'];
                return "<strong>$v</strong>";
            })
            ->remove_column('lastname')
            ->remove_column('firstname')
            ->remove_column('company')
            ->remove_column('groupname')
            ->remove_column('peoplename')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("customers.id")
            ->select('customers.id', 'code', 'name', 'active', 'customers.updated_at', 'customers.deleted_at', 'customers.sdesc', 'emails_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('code', function ($data) {
                $v = $data['code'];
                return "<strong>$v</strong>";
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->rules['email'] = 'required|email|unique:customers,email,' . $model->id . ',id,active,1';
        $this->_prepare($model);
    }

    function _after_update($model)
    {
        parent::_after_update($model);
        $this->handle_newsletter($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
        $this->handle_newsletter($model);
    }

    function handle_newsletter($model)
    {
        $model->handleNewsletter();
    }

    function _prepare($model = null)
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        if ($_POST['password'] != '') {
            $this->rules['password'] = 'required|min:6|confirmed';
            $_POST['passwd'] = \Hash::make($_POST['password']);
            $_POST['last_passwd_gen'] = \Format::now();
        } else {
            if ($_POST['id'] == 0) {
                $this->rules['password'] = 'required|min:6|confirmed';
            }
        }

        if ($_POST['people_id'] == 1) {
            $this->rules['firstname'] = 'required';
            $this->rules['lastname'] = 'required';
            $this->rules['gender_id'] = 'required';
            $this->rules['birthdate'] = 'required';
            if(config('auth.customer.avoid_minors', false) === true){
                $this->rules['birthdate'] .= '|olderThan';
            }
        } else {
            $this->rules['company'] = 'required';
        }

        $_POST['birthday'] = Format::sqlDatetime($_POST['birthdate']);
        if ($model) {
            if ($model->newsletter == 0 AND $_POST['newsletter'] == 1) {
                $_POST['newsletter_date_add'] = \Format::now();
            }
        } else {
            if ($_POST['newsletter'] == 1) {
                $_POST['newsletter_date_add'] = \Format::now();
            } else {
                $_POST['newsletter_date_add'] = null;
            }
        }
        $_POST['firstname'] = ucfirst($_POST['firstname']);
        $_POST['lastname'] = ucfirst($_POST['lastname']);
        $_POST['company'] = ucfirst($_POST['company']);
        $_POST['name'] = $_POST['people_id'] == 1 ? $_POST['firstname'] . " " . $_POST['lastname'] : $_POST['company'];

        \Input::replace($_POST);

    }

    static function getAddressTable($customer_id)
    {
        $rows = \Address::where('customer_id', $customer_id)->get(['id']);
        $table = <<<TABLE
<table class="table">
<thead>
<tr>
    <th>Denominazione</th>
    <th>Indirizzo</th>
    <th>Indirizzo (2)</th>
    <th>Cap</th>
    <th>Città</th>
    <th>Provincia</th>
    <th>Nazione</th>
    <th>Tel.</th>
    <th>Cell.</th>
    <th>CF</th>
    <th>P.IVA</th>
    <th>Azioni</th>
</tr>
</thead>
<tbody>
TABLE;

        foreach ($rows as $a) {
            $row = \Address::find($a->id);
            $fullname = $row->fullname();
            $country = $row->countryName();
            $state = $row->stateName();
            $table .= <<<HTML
<tr>
    <td>$fullname</td>
    <td>$row->address1</td>
    <td>$row->address2</td>
    <td>$row->postcode</td>
    <td>$row->city</td>
    <td>$state</td>
    <td>$country</td>
    <td>$row->phone</td>
    <td>$row->phone_mobile</td>
    <td>$row->cf</td>
    <td>$row->vat_number</td>
    <td>
        <a href="javascript:;" class="btn bo-action" data-action="editAddress" data-id="$row->id">Modifica</a>
        <a href="javascript:;" class="btn bo-action" data-action="removeAddress" data-id="$row->id">Elimina</a>
    </td>
</tr>
HTML;
        }

        $table .= <<<TABLE
    </tbody>
</table>
TABLE;

        return $table;

    }


    static function getProfilesTable($customer_id)
    {
        $rows = \Profile::where('customer_id', $customer_id)->get();

        if (count($rows) == 0) {
            return "<div class='alert alert-warning'>Non ci sono profili per questo Cliente</div>";
        }

        $table = <<<TABLE
<table class="table">
<thead>
<tr>
    <th>Provider</th>
    <th>Profile URL</th>
    <th>Img</th>
    <th>Display name</th>
    <th>Sesso</th>
    <th>Lingua</th>
    <th>Età</th>
    <th>Data di nascita</th>
    <th>Email</th>
    <th>Tel.</th>
    <th>Indirizzo</th>
    <th>Nazione</th>
    <th>Regione</th>
    <th>Città</th>
    <th>CAP</th>
    <th>Creato il</th>
    <th>Ult. modifica</th>
</tr>
</thead>
<tbody>
TABLE;

        foreach ($rows as $row) {
            $birthdate = $row->birthDay == 0 ? "-" : "$row->birthDay/$row->birthMonth/$row->birthYear";
            $cdate = \Format::datetime($row->created_at);
            $mdate = \Format::datetime($row->updated_at);
            $table .= <<<HTML
<tr>
    <td>$row->provider</td>
    <td><a href="$row->profileURL" target="_blank">APRI</a></td>
    <td><img src="$row->photoURL" width=50 /></td>
    <td>$row->displayName</td>
    <td>$row->gender</td>
    <td>$row->language</td>
    <td>$row->age</td>
    <td>$birthdate</td>
    <td>$row->email</td>
    <td>$row->phone</td>
    <td>$row->address</td>
    <td>$row->country</td>
    <td>$row->region</td>
    <td>$row->city</td>
    <td>$row->zip</td>
    <td>$cdate</td>
    <td>$mdate</td>
</tr>
HTML;
        }

        $table .= <<<TABLE
    </tbody>
</table>
TABLE;

        return $table;

    }


    static function getOrdersTable($customer_id)
    {


        $rows = \Order::where('customer_id', $customer_id)->get();

        if (count($rows) == 0) {
            return "<div class='alert alert-warning'>Non ci sono ordini per questo Cliente</div>";
        }

        $table = <<<TABLE
<table class="table">
<thead>
<tr>
    <th>Numero</th>
    <th>Data</th>
    <th>Articoli</th>
    <th>Status</th>
    <th>Metodo di pagamento</th>
    <th>Status pagamento</th>
    <th>Mezzo di spedizione</th>
    <th>Subtotale prodotti</th>
    <th>Totale ordine</th>
</tr>
</thead>
<tbody>
TABLE;

        foreach ($rows as $row) {
            $cdate = \Format::datetime($row->created_at);
            $status = $row->statusName();
            $payment = $row->paymentStatusName();
            $carrier = $row->carrierName();
            $tot_products = \Format::currency($row->total_products, true);
            $tot_orders = \Format::currency($row->total_order, true);
            $table .= <<<HTML
<tr>
    <td>$row->reference</td>
    <td>$cdate</td>
    <td>$row->total_quantity</td>
    <td>$status</td>
    <td>$row->payment</td>
    <td>$payment</td>
    <td>$carrier</td>
    <td>$tot_products</td>
    <td>$tot_orders</td>
</tr>
HTML;
        }

        $table .= <<<TABLE
    </tbody>
</table>
TABLE;

        return $table;
    }


    function getAddressForm($customer_id)
    {
        $id = \Input::get('id', 0);
        $obj = \Address::find($id);
        $view = array('obj' => $obj, 'customer_id' => $customer_id);
        $html = Theme::scope("customers.form", $view)->content();
        $success = true;
        return Json::encode(compact('success', 'html'));
    }


    function getAddressList($customer_id)
    {
        $html = $this->getAddressTable($customer_id);
        $success = true;
        return Json::encode(compact('success', 'html'));
    }


    function postAddressDelete($id)
    {
        $obj = Address::find($id);
        $success = false;
        $msg = '';
        if ($obj) {
            if ($obj->isDeletable()) {
                $success = true;
                $obj->delete();
            } else {
                $msg = "Questo indirizzo non può essere eliminato";
            }
        }
        return Json::encode(compact('success', 'msg'));
    }


    function postAddressSave()
    {
        $data = \Input::all();
        $required = [
            'address1' => "Indirizzo",
            'postcode' => "CAP",
            'country_id' => "Nazione",
            'city' => "Città"
        ];
        $errors = [];
        foreach ($required as $key => $value) {
            if (isset($data[$key])) {
                if ($data[$key] == '') {
                    $errors[] = "Il campo $value è obbligatorio";
                }
            } else {
                $errors[] = "Il campo $value è obbligatorio";
            }
        }

        if (count($errors) == 0) {
            if ($data['id'] > 0) {
                $obj = Address::find($data['id']);
            } else {
                $obj = new Address();
            }

            $data = [
                'customer_id' => $data['customer_id'],
                'firstname' => ucfirst($data['firstname']),
                'lastname' => ucfirst($data['lastname']),
                'company' => ucfirst($data['company']),
                'people_id' => $data['people_id'],
                'address1' => ucfirst($data['address1']),
                'address2' => $data['address2'],
                'postcode' => $data['postcode'],
                'city' => ucfirst($data['city']),
                'country_id' => $data['country_id'],
                'state_id' => isset($data['state_id']) ? $data['state_id'] : null,
                'phone' => $data['phone'],
                'phone_mobile' => $data['phone_mobile'],
                'cf' => \Str::upper($data['cf']),
                'vat_number' => \Str::upper($data['vat_number']),
                'extrainfo' => $data['extrainfo'],
                'billing' => $data['billing'],
            ];

            $obj->setAttributes($data);
            $obj->save();
        }

        $success = (count($errors) == 0);
        $msg = $success ? "" : implode("<br>", $errors);
        return Json::encode(compact("success", "msg"));
    }


    function getSelectAddress($id)
    {
        $customer = \Customer::find($id);
        $success = false;
        $shipping = '<option value="">Seleziona...</option>';
        $billing = '<option value="">Seleziona...</option>';
        $msg = 'Cliente non esistente';
        if ($customer) {
            $success = true;
            $as_rows = $customer->getAddresses(0);
            if (count($as_rows) > 0) {
                $shipping = '';
                foreach ($as_rows as $row) {
                    $name = $row->extendedName();
                    $shipping .= "<option value='$row->id'>$name</option>";
                }
            }

            $as_rows = $customer->getAddresses(1);
            if (count($as_rows) > 0) {
                $billing = '';
                foreach ($as_rows as $row) {
                    $name = $row->extendedName();
                    $billing .= "<option value='$row->id'>$name</option>";
                }
            }
        }
        return Json::encode(compact("success", "msg", "shipping", "billing"));
    }


    function getOrders($id)
    {
        $success = true;
        $customer = \Customer::getObj($id);
        $options = $customer->selectOrders('Nessun ordine');
        $html = '';
        $counter = 0;
        foreach ($options as $key => $name) {
            $sel = ($counter == 1) ? 'selected' : '';
            $html .= "<option $sel value='$key'>$name</option>";
            $counter++;
        }
        return Json::encode(compact("success", "html"));
    }


    function getOptions()
    {
        //\Utils::watch();
        $q = \Input::get("q");
        $page_limit = \Input::get("page_limit");
        $page = \Input::get("page");
        $start = ($page - 1) * $page_limit;

        $builder = Customer::select(['id', 'name', 'email', 'people_id', 'firstname', 'lastname', 'company'])
            ->where('active', 1)
            ->where(function ($query) use ($q) {
                $query->where('name', 'like', "%$q%")
                    ->orWhere('firstname', 'like', "%$q%")
                    ->orWhere('lastname', 'like', "%$q%")
                    ->orWhere('company', 'like', "%$q%")
                    ->orWhere('email', 'like', "%$q%");

                if(is_numeric($q) and $q > 0){
                    $query->orWhere('id', $q);
                }
            });

        $count_builder = clone $builder;

        $total = $count_builder->count('id');

        $rows = $builder
            ->orderBy('firstname')
            ->orderBy('lastname')
            ->orderBy('name')
            ->take($page_limit)
            ->skip($start)
            ->get();

        $count = count($rows);
        for ($i = 0; $i < $count; $i++) {
            $rows[$i]->name = $rows[$i]->getName() . " ({$rows[$i]->email}) #{$rows[$i]->id}";
        }
        $success = true;
        $records = $rows;
        return Json::encode(compact("total", "success", "records"));
    }

    function getSingleoption()
    {
        //\Utils::watch();
        $id = \Input::get("id");

        $row = Customer::where('active', 1)->find($id);
        if ($row) {
            $row->name = $row->getName() . " ({$row->email}) #{$row->id}";
        }

        $record = $row;
        return Json::encode(compact("record"));
    }

    public function getGroups()
    {
        $this->addBreadcrumb('Elenco Gruppi Clienti');
        $this->page = 'customers.groups';
        $view = array('rows' => CustomerGroup::rows()->orderBy('id', 'desc')->get());
        return $this->render($view);
    }

}