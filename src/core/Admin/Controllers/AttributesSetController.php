<?php

class AttributesSetController extends BackendController {

    public $component_id = 21;
    public $title = 'Gestione Set Attributi';
    public $page = 'attributes_sets.index';
    public $pageheader = 'Gestione Set Attributi';
    public $iconClass = 'font-columns';
    public $model = 'AttributeSet';
    protected $rules = array(
        'uname' => 'required|unique:attributes_sets,uname',
    );
    protected $friendly_names = array(
        'uname' => 'Codice Set Attributi'
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/attributes_sets.js");
    }

    public function getIndex() {
        $this->addBreadcrumb("Elenco Set Attributi");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->page = 'attributes_sets.trash';
        $this->pageheader = 'Cestino Set Attributi';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->page = 'attributes_sets.create';
        $this->pageheader = 'Nuovo Set Attributo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->page = 'attributes_sets.create';
        $this->pageheader = 'Modifica Set Attributo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }
    
    public function getPreview($id) {        
        $model = $this->model;
        $obj = $model::find($id);        
        $view = array('obj' => $obj);
        return Theme::scope("attributes_sets.preview",$view)->content();
    }

    public function getTable() {

        $model = $this->model;

        $pages = DB::table(DB::raw('attributes_sets S1'))
                ->leftJoin(DB::raw('attributes_sets S2'), 'S1.parent_id', '=', 'S2.id')
                ->leftJoin(DB::raw('attributes_sets_content_cache C'), 'S1.id', '=', 'C.attribute_set_id')
                ->where('S1.deleted_at', null)
                ->groupBy("S1.id")
                ->select(['S1.id', 'S1.uname', 'S2.uname AS parent', DB::raw('count(C.id) as attributes'), 'S1.created_at', 'S1.published', 'S1.position']);

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('uname', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['uname']}</a>";
                        })
                        ->edit_column('attributes', function($data) {
                            $link = \URL::action($this->action("getPreview"), $data['id']);
                            return "<span class=\"label label-info mr5\">{$data['attributes']}</span> <a href=\"javascript:Echo.remoteModal('$link');\">Anteprima</a>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->edit_column('published', function($data) {
                            return ($data['published'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Sospeso</span>';
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {

        $model = $this->model;

        $pages = DB::table(DB::raw('attributes_sets S1'))
                ->leftJoin(DB::raw('attributes_sets S2'), 'S1.parent_id', '=', 'S2.id')
                ->leftJoin(DB::raw('attributes_sets_content_cache C'), 'S1.id', '=', 'C.attribute_set_id')
                ->whereNotNull('S1.deleted_at')
                ->groupBy("S1.id")
                ->select(['S1.id', 'S1.uname', 'S2.uname AS parent', DB::raw('count(C.id) as attributes') , 'S1.created_at', 'S1.deleted_at', 'S1.published']);

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('attributes', function($data) {                            
                            return "<span class=\"label label-info\">{$data['attributes']}</span>";
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::datetime($data['deleted_at']);
                        })
                        ->edit_column('uname', function($data) {
                            return "<strong>{$data['uname']}</strong>";
                        })
                        ->edit_column('published', function($data) {
                            return ($data['published'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Sospeso</span>';
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['uname'] = 'required|unique:attributes_sets,uname,' . $model->id;
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }

    function _after_create($model) {
        $this->handleData($model);
    }

    function _after_update($model) {
        $this->handleData($model);
    }

    function handleData($model) {
        $input = Input::all();
        $attributes_add = Input::get('attributes_add', []);
        $attributes_remove = Input::get('attributes_remove', []);
        $attributes_position = explode(",", Input::get('attributes_position', ''));

        \Utils::log($attributes_position, "HANDLE DATA");

        DB::table('attributes_sets_content')->where('attribute_set_id', $model->id)->delete();
        DB::table('attributes_sets_content_cache')->where('attribute_set_id', $model->id)->delete();

        if (count($attributes_add) > 0) {
            $counter = 0;
            foreach ($attributes_add as $attribute_id) {
                $counter++;
                $default_value = \Helper::getAttributeValues($attribute_id);
                DB::table('attributes_sets_content')->insert([
                    'attribute_set_id' => $model->id,
                    'attribute_id' => $attribute_id,
                    'rule' => '+',
                    'position' => $counter,
                    'default_value' => $default_value,
                ]);
            }
        }

        if (count($attributes_remove) > 0) {
            $counter = 0;
            foreach ($attributes_remove as $attribute_id) {
                DB::table('attributes_sets_content')->insert([
                    'attribute_set_id' => $model->id,
                    'attribute_id' => $attribute_id,
                    'rule' => '-',
                    'position' => 0,
                ]);
            }
        }

        if (count($attributes_position) > 0) {
            $counter = 0;
            foreach ($attributes_position as $attribute_id) {
                $counter++;
                $default_value = \Helper::getAttributeValues($attribute_id);
                DB::table('attributes_sets_content_cache')->insert([
                    'attribute_set_id' => $model->id,
                    'attribute_id' => $attribute_id,
                    'position' => $counter,
                    'default_value' => $default_value,
                ]);
            }
        }
    }

    function postAttributes($set_id) {
        $rows = Mainframe::attributes_by_set($set_id);

        $data = array('success' => true, 'data' => $rows);

        return Json::encode($data);
    }

    function postAttributesadd($parent_id) {
        $id = $_POST['id'];

        $data = array('success' => true, 'data' => Mainframe::optionsAttributesAdded($id, $parent_id));

        return Json::encode($data);
    }

    function postAttributesremove($parent_id) {
        $id = $_POST['id'];

        $data = array('success' => true, 'data' => Mainframe::optionsAttributesRemoved($id, $parent_id));

        return Json::encode($data);
    }

    function postAttributestable($parent_id) {
        $id = $_POST['id'];

        $data = array('success' => true, 'data' => \Helper::getAttributesSetTable($parent_id));

        return Json::encode($data);
    }

    function postAddRule() {
        $data = \Input::all();

        $success = false;

        \Utils::watch();

        if ($data['target_mode'] == '*') {
            DB::table('attributes_sets_rules')->insert([
                'attribute_set_id' => $data['id'],
                'target_type' => $data['target_type'],
                'target_mode' => $data['target_mode'],
                'position' => 9999,
            ]);
            $success = true;
            $message = 'Regola salvata con successo';
        } else {
            $ids = \Input::get('condition_' . $data['target_type'], []);
            $many = count($ids);
            if ($many > 0) {
                DB::table('attributes_sets_rules')
                        ->where('attribute_set_id', $data['id'])
                        ->update(['position' => DB::raw('position+' . $many)]);
                $counter = 0;
                foreach ($ids as $id) {
                    DB::table('attributes_sets_rules')->insert([
                        'attribute_set_id' => $data['id'],
                        'target_type' => $data['target_type'],
                        'target_mode' => $data['target_mode'],
                        'target_id' => $id,
                        'position' => $counter,
                    ]);
                    $counter++;
                }
                $success = true;
                $message = 'Regola/e salvate con successo';
            } else {
                $message = 'Devi selezionare almeno un elemento per il campo Condizione';
            }
        }

        $data = array('success' => $success, 'message' => $message);

        \Utils::unwatch();

        return Json::encode($data);
    }

    function postRulesTable() {
        $id = \Input::get('id');

        $data = array('success' => true, 'html' => \Helper::getAttributesSetRulesTable($id));

        return Json::encode($data);
    }

    function postRemoveRule($id) {
        DB::table('attributes_sets_rules')->where('id', $id)->delete();
        $data = array('success' => true, 'msg' => 'Record eliminato con successo');
        return Json::encode($data);
    }

    function postAddAttribute() {
        $id = \Input::get("id");

        $success = true;
        $html = '';

        $ah = new AttributeHtml($id);
        $render = $ah->getRenders();
        $row = <<<ROW
<tr id="{$render['id']}" rel="{$render['id']}">
    <td class="dragHandle"></td>
    <td class="align-right">
        {$render['labelHtml']}
    </td>
    <td class="center">       
        {$render['html']}
    </td>
</tr>                
ROW;
        $html = $row;

        $data = compact("success", "html", "insert_attributes");
        return Json::encode($data);
    }

}
