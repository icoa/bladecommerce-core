<?php

class CampaignsController extends BackendController
{

    public $component_id = 57;
    public $title = 'Gestione Campagne';
    public $page = 'campaigns.index';
    public $pageheader = 'Gestione Campagne';
    public $iconClass = 'font-columns';
    public $model = 'Campaign';
    protected $rules = array(
        'name' => 'required',
        'lang_id' => 'required',
        'campaign_type' => 'required',
    );
    protected $friendly_names = array(
        'name' => 'Denominazione',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Campagne');
        $this->toFooter("js/echo/campaigns.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/campaigns.js");
        $this->page = 'campaigns.trash';
        $this->pageheader = 'Cestino Campagne';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'campaigns.create';
        $this->pageheader = 'Nuova Campagna';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->page = 'campaigns.create';
        $this->pageheader = 'Modifica Campagna';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('campaign_types', 'campaign_types.id', '=', 'campaigns.campaign_type')
            ->select('campaigns.id', 'lang_id', 'campaigns.name as name', 'campaigns.campaign_type', 'active', 'created_at', 'campaign_types.name as type');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('lang_id', function ($data) {
                $src = \AdminTpl::img("images/flags/{$data['lang_id']}.jpg");
                return "<img src='$src' />";
            })
            ->edit_column('campaign_type', function ($data) {
                $s = $data['type'];
                return "<strong>$s</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
            })
            ->remove_column('type')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('campaign_types', 'campaign_types.id', '=', 'campaigns.campaign_type')
            ->select('campaigns.id', 'lang_id', 'campaigns.name as name', 'campaign_types.name as type', 'active', 'deleted_at');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
            })
            ->edit_column('lang_id', function ($data) {
                $src = \AdminTpl::img("images/flags/{$data['lang_id']}.jpg");
                return "<img src='$src' />";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }
    }


}