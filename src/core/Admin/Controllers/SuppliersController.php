<?php

class SuppliersController extends MultiLangController {

    public $component_id = 22;
    public $title = 'Gestione Fornitori';
    public $page = 'suppliers.index';
    public $pageheader = 'Gestione Fornitori';
    public $iconClass = 'font-columns';
    public $model = 'Supplier';
    protected $rules = array(
        'address1' => 'required',
        'postcode' => 'required',
        'city' => 'required',
        'country_id' => 'required',
    );
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:suppliers_lang,slug,supplier_id'
    );
    protected $friendly_names = array(
        'address1' => 'Indirizzo',
        'postcode' => 'CAP',
        'city' => 'Città',
        'country_id' => 'Nazione',
    );    

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    

    public function getIndex() {
        $this->addBreadcrumb('Elenco Fornitori');
        $this->toFooter("js/echo/suppliers.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/suppliers.js");
        $this->page = 'suppliers.trash';
        $this->pageheader = 'Cestino Fornitori';      
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {        
        $this->page = 'suppliers.create';
        $this->pageheader = 'Nuovo Fornitore';
        $this->addBreadcrumb($this->pageheader);
        $this->toFooter("js/echo/address.js");
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {        
        $this->page = 'suppliers.create';
        $this->pageheader = 'Modifica Fornitore';
        $this->addBreadcrumb($this->pageheader);
        $this->toFooter("js/echo/address.js");
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }
    

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';        

        $model = $this->model;

        $pages = $model::leftJoin('suppliers_lang', 'id', '=', 'suppliers_lang.supplier_id')
                ->where('lang_id', $lang_id)                
                ->select('suppliers.id', 'suppliers_lang.name as name', 'suppliers_lang.slug', 'suppliers_lang.published', 'suppliers.created_at', 'suppliers.position', 'suppliers_lang.ldesc', 'suppliers_lang.lang_id');
        
        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::date($data['created_at']);
                                })
                        ->edit_column('name', function($data) {
                                    $link = \URL::action($this->action("getEdit"), $data['id']);
                                    return "<strong><a href='$link'>{$data['name']}</a></strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_actions($data);
                                })
                        ->edit_column('published', function($data) {
                                    return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
                                })
                        ->edit_column('position', function($data,$index,$obj) {
                                    return $this->column_position($data, $index, $obj);
                                })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                                })
                        ->make();
    }
    
    
    

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('suppliers_lang', 'id', '=', 'suppliers_lang.supplier_id')
                ->where('lang_id', $lang_id)
                ->select('id', 'name', 'slug', 'position','deleted_at', 'created_at', 'ldesc', 'lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::date($data['created_at']);
                                })
                        ->edit_column('deleted_at', function($data) {
                                    return \Format::date($data['deleted_at']);
                                })
                        ->edit_column('name', function($data) {
                                    return "<strong>{$data['name']}</strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_trash_actions($data);
                                })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                                })
                        ->make();
    }
    
    
    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] .','.$model->id;
        $this->_prepare();
    }

    function _prepare() {
        if(count($_POST) == 0){
            return;
        }        
        
        $langDef = Cfg::get('DEFAULT_LANGUAGE');
        
        $defaultName = $_POST["name_".$langDef];
        $defaultImage = $_POST["image_default_".$langDef];
        

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if($lang != $langDef){
                if($_POST["name_".$lang] == "")$_POST["name_".$lang] = $defaultName;
                if($_POST["image_default_".$lang] == "")$_POST["image_default_".$lang] = $defaultImage;                
            }
            $source = $_POST["name_" . $lang];
            $target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }
        }

        $model = $this->model;
        if( (int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();
        
        \Input::replace($_POST);

    }

}