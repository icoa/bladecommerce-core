<?php

class MessagesController extends BackendController
{

    public $component_id = 73;
    public $title = 'Gestione Conversazioni';
    public $page = 'messages.index';
    public $pageheader = 'Gestione Conversazioni';
    public $iconClass = 'font-cog';
    public $model = 'Message';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'message' => 'required',
    );
    protected $friendly_names = array();
    protected $lang_friendly_names = array(
        'name' => 'Nome del template',
        'message' => 'Messaggio predefinito',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Conversazioni - Messaggi ricevuti');
        $this->pageheader = 'Elenco Conversazioni - Messaggi ricevuti';
        $this->toFooter("js/echo/messages.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }


    public function getTrash()
    {
        $this->addBreadcrumb('Elenco Conversazioni - Messaggi inviati');
        $this->pageheader = 'Elenco Conversazioni - Messaggi inviati';
        $this->toFooter("js/echo/messages.js");
        $view = array();
        $this->toolbar();
        $this->page = 'messages.trash';
        return $this->render($view);
    }



    public function getCreate()
    {
        $this->toFooter("js/echo/messages.js");
        $this->toFooter("js/echo/orders.js");
        $this->page = 'messages.create';
        $this->pageheader = 'Nuovo Messaggio';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }



    protected function actions_default( $params = [] ){
        $trash = ($this->action("getTrash", TRUE));
        $create = ($this->action("getCreate", TRUE));
        $index = ($this->action("getIndex", TRUE));

        $actions = array(
            new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record','new'),
            new AdminAction('Ricevuti', $index, 'font-arrow-left', '', 'Elenco i messaggi inviati dai Clienti','index'),
            new AdminAction('Inviati', $trash, 'font-arrow-right', '', 'Elenca i messaggi inviati dagli Operatori','trash'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente','reload'),
        );
        return $actions;
    }

    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('customers','messages.customer_id','=','customers.id')
            ->leftJoin('orders','messages.order_id','=','orders.id')
            ->leftJoin('users','messages.created_by','=','users.id')
            ->where('messages.type','C')
            ->where("messages.id","=", DB::raw("(SELECT MAX(id) from messages M2 WHERE messages.thread_id=M2.thread_id AND M2.type='C')"))
            ->groupBy("messages.thread_id")
            ->select( 'thread_id', 'message', 'messages.created_at','messages.id','customers.name','orders.reference','users.email');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('reference', function ($data) {
                $str = '';
                if($data['reference'] != ''){
                    $str = "<span class='label label-inverse'>{$data['reference']}</span>";
                }
                return $str;
            })
            ->add_column('total',function($data){
                $obj = Message::getObj($data['id']);
                $str = "<span class='label'>{$obj->count}</span>";
                return $str;
            })
            ->edit_column('message', function ($data) {
                $obj = Message::getObj($data['id']);
                $sender = $obj->sender;
                $message = substr($obj->message,0,150)."...";
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<img src='$obj->image' width='32' class='pull-left mt5 mr10 ml5' /><strong><a href='$link'>Inviato da $sender</a></strong><br><em>$message</em>";
            })
            ->remove_column('id')
            ->make(true);
    }




    public function getTabletrash()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        /*$pages = $model::where('type','U')
            ->where("id","=", DB::raw("(SELECT MAX(id) from messages M2 WHERE messages.thread_id=M2.thread_id AND M2.type='U')"))
            ->groupBy("messages.thread_id")
            ->select( 'thread_id', 'message', 'created_at','id');*/
        $pages = $model::leftJoin('customers','messages.customer_id','=','customers.id')
            ->leftJoin('orders','messages.order_id','=','orders.id')
            ->leftJoin('users','messages.created_by','=','users.id')
            ->where('messages.type','U')
            ->where("messages.id","=", DB::raw("(SELECT MAX(id) from messages M2 WHERE messages.thread_id=M2.thread_id AND M2.type='U')"))
            ->groupBy("messages.thread_id")
            ->select( 'thread_id', 'message', 'messages.created_at','messages.id','customers.name','orders.reference','users.email');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('reference', function ($data) {
                $str = '';
                if($data['reference'] != ''){
                    $str = "<span class='label label-inverse'>{$data['reference']}</span>";
                }
                return $str;
            })
            ->add_column('total',function($data){
                $obj = Message::getObj($data['id']);
                $str = "<span class='label'>{$obj->count}</span>";
                return $str;
            })
            ->edit_column('message', function ($data) {
                $obj = Message::getObj($data['id']);
                $sender = $obj->sender;
                $customer = $obj->customer;
                if($customer)$sender .= " - Destinatario: ".$customer->getName();

                $message = substr($obj->message,0,150)."...";
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<img src='$obj->image' width='32' class='pull-left mt5 mr10 ml5' /><strong><a href='$link'>Inviato da $sender</a></strong><br><em>$message</em>";
            })
            ->remove_column('id')
            ->make(true);
    }



    public function getEdit($id){
        $this->toFooter("js/echo/messages.js");
        $this->toFooter("js/echo/orders.js");
        $this->page = 'messages.edit';
        $this->pageheader = 'Visualizza conversazione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->expand();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    protected function actions_create( $params = [] ){
        $trash = ($this->action("getTrash", TRUE));
        $create = ($this->action("getCreate", TRUE));
        $index = ($this->action("getIndex", TRUE));

        $actions = array(
            new AdminAction('Ricevuti', $index, 'font-arrow-left', '', 'Elenco i messaggi inviati dai Clienti','index'),
            new AdminAction('Inviati', $trash, 'font-arrow-right', '', 'Elenca i messaggi inviati dagli Operatori','trash'),
        );
        return $actions;
    }


    protected function actions_trash(){
        $index = $this->action("getIndex", TRUE);
        $restore = $this->action("postRestoreMulti", TRUE, 0);
        $destroy = $this->action("postDestroyMulti", TRUE);
        $actions = array(
            new AdminAction('Ripristina', $restore, 'font-undo', 'btn-success confirm-action-multi', 'Recupera i record selezionati'),
            new AdminAction('Elimina', $destroy, 'font-minus-sign', 'btn-danger confirm-action-multi', 'Elimina definitivamente i record selezionati'),
            new AdminAction('Indietro', $index, 'font-arrow-left'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
        );
        return $actions;
    }




    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
            }
        }

        \Input::replace($_POST);

    }




}