<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 22-gen-2014 13.49.22
 */

class ProductsPricesController extends BackendController {
    
    
    public $model = 'ProductPrice';
    
    function postLoadForm($id) {
        //Utils::watch();
        $obj = ProductPrice::find($id);
        $success = true;
        $html = ProductHelper::getSpecificPriceForm($obj->product_id, $id);
        return Json::encode(compact("success","html"));
    }
    
    function getForm($product_id) {        
        $success = true;
        $html = ProductHelper::getSpecificPriceForm($product_id, 0);
        return Json::encode(compact("success","html"));
    }
    
    function getTable($id){
        $success = true;
        $html = ProductHelper::getSpecificPricesTable($id);
        return Json::encode(compact("success","html"));
    }
    
    
    function postOrder(){
        $ids = Input::get("ids");
        $counter = 0;
        foreach($ids as $id){
            $counter++;
            ProductPrice::where("id",$id)->update(["position" => $counter]);
        }
        $success = true;
        return Json::encode(compact("success"));
    }
    

    function postSubmit($product_id) {
        $data = \Input::all();

        Utils::log($data);
        Utils::watch();

        if (isset($data['specific_price_id']) AND $data['specific_price_id'] > 0) {
            $obj = ProductPrice::find($data['specific_price_id']);
        } else {
            $obj = new ProductPrice();
        }
        
        $data["product_id"] = $product_id;
        //$data["price"] = $data['specific_price'];
        $data["price_rule_id"] = 0;
        $data["date_from"] = ($data["date_from"] == "") ? null : Format::sqlDatetime($data["date_from"]);
        $data["date_to"] = ($data["date_to"] == "") ? null : Format::sqlDatetime($data["date_to"]);
        $data["position"] = isset($data["position"]) ? (int)$data["position"] : $obj->position();
        $data["reduction_value"] = (float)$data["reduction_value"];
        
        $obj->setDataSource($data);
       /* $obj->product_id = $product_id;
        $obj->price = $data['specific_price'];
        $obj->from = ($data["from"] == "") ? null : Format::datetime($data["from"]);
        $obj->to = ($data["to"] == "") ? null : Format::datetime($data["to"]);*/
        $obj->save();
        
        Utils::unwatch();
        $data = array('success' => true, 'msg' => 'Regola prezzo specifico aggiornata correttamente');
        return Json::encode($data);
    }

}
