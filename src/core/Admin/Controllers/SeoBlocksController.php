<?php

use Core\Token;
use Core\Condition\RuleResolver;

class SeoBlocksController extends MultiLangController {

    public $component_id = 54;
    public $title = 'Gestione Blocchi SEO';
    public $page = 'seoblocks.index';
    public $pageheader = 'Gestione Blocchi SEO';
    public $iconClass = 'font-columns';
    public $model = 'SeoBlock';
    protected $rules = array();
    protected $lang_rules = array(
        'content' => 'required'
    );
    protected $lang_friendly_names = array(
        'content' => "Contenuto del blocco"
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Blocchi SEO');
        $this->toFooter("js/echo/seoblocks.js?v=2");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/seoblocks.js?v=2");
        $this->page = 'seoblocks.trash';
        $this->pageheader = 'Cestino Blocchi SEO';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/builder.js");
        $this->toFooter("js/echo/seoblocks.js?v=2");
        $this->page = 'seoblocks.create';
        $this->pageheader = 'Nuovo Bloccho SEO';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/builder.js");
        $this->toFooter("js/echo/seoblocks.js?v=2");
        $this->page = 'seoblocks.create';
        $this->pageheader = 'Modifica Blocchi SEO';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }
    
    
    public function getPreview($id) {        
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->fillLanguages();        
        $view = array('obj' => $obj);
        return Theme::scope("seoblocks.preview",$view)->content();
    }

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('seo_blocks_lang', 'id', '=', 'seo_blocks_lang.seo_block_id')
                ->leftJoin(DB::raw('seo_blocks_rules C'), 'seo_blocks.id', '=', 'C.seo_block_id')
                ->where('lang_id', $lang_id)
                ->groupBy("seo_blocks.id")
                ->select('seo_blocks.id', 'xtype','context', 'content', DB::raw('count(C.id) as rules'), 'active', 'seo_blocks.created_at', 'seo_blocks.position', 'seo_blocks_lang.lang_id', 'conditions','isDefault');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('xtype', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['xtype']}</a></strong>";
                        })
                        ->edit_column('context', function($data) {
                            $add = $data['isDefault'] == 1 ? ' <span class="label">Default</span>' : '';
                            return "<strong>{$data['context']}</strong>$add";
                        })
                        ->edit_column('content', function($data) {                
                            $token = new Token($data["content"]);
                            $token->setLang($data["lang_id"]);
                            $token->setSandboxMode(TRUE);
                            $example = $token->render();
                            return "<span class='courier'>{$data['content']}</span><br><em class='disabled'>$example</em>";
                        })
                        ->edit_column('rules', function($data) {
                            $bh = new \BuilderHelper($data['conditions'], "general");
                            $data['rules'] = $bh->count();
                            $link = \URL::action($this->action("getPreview"), $data['id']);
                            return "<span class=\"label label-info mr5\">{$data['rules']}</span> <a href=\"javascript:Echo.remoteModalLarge('$link');\">Anteprima</a>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->remove_column('lang_id')
                        ->remove_column('conditions')
                        ->remove_column('isDefault')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('seo_blocks_lang', 'id', '=', 'seo_blocks_lang.seo_block_id')
                ->leftJoin(DB::raw('seo_blocks_rules C'), 'seo_blocks.id', '=', 'C.seo_block_id')
                ->where('lang_id', $lang_id)
                ->groupBy("seo_blocks.id")
                ->select('seo_blocks.id', 'xtype','context', 'content', DB::raw('count(C.id) as rules'), 'seo_blocks.position', 'seo_blocks.deleted_at', 'seo_blocks.created_at', 'seo_blocks_lang.lang_id','conditions','isDefault');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('content', function($data) {                
                            $token = new Token($data["content"]);
                            $token->setLang($data["lang_id"]);
                            $token->setSandboxMode(TRUE);
                            $example = $token->render();
                            return "<span class='courier'>{$data['content']}</span><br><em class='disabled'>$example</em>";
                        })
                        ->edit_column('rules', function($data) {
                            $bh = new \BuilderHelper($data['conditions'], "general");
                            $data['rules'] = $bh->count();
                            $link = \URL::action($this->action("getPreview"), $data['id']);
                            return "<span class=\"label label-info mr5\">{$data['rules']}</span> <a href=\"javascript:Echo.remoteModalLarge('$link');\">Anteprima</a>";
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::datetime($data['deleted_at']);
                        })                        
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })                        
                        ->remove_column('lang_id')
                        ->remove_column('conditions')
                        ->remove_column('isDefault')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();


        $conditions = \Input::get("rule")["conditions"];
        $rr = new RuleResolver();
        $_POST['conditions'] = $rr->getSerializable($conditions);

        \Input::replace($_POST);

    }
    
    
    function postAddRule() {
        $data = \Input::all();

        $success = false;

        \Utils::watch();

        if ($data['target_mode'] == '*') {
            DB::table('seo_blocks_rules')->insert([
                'seo_block_id' => $data['id'],
                'target_type' => $data['target_type'],
                'target_mode' => $data['target_mode'],
                'position' => 9999,
            ]);
            $success = true;
            $message = 'Regola salvata con successo';
        } else {
            $ids = \Input::get('condition_' . $data['target_type'], []);
            $many = count($ids);
            if ($many > 0) {
                DB::table('seo_blocks_rules')
                        ->where('seo_block_id', $data['id'])
                        ->update(['position' => DB::raw('position+' . $many)]);
                $counter = 0;
                foreach ($ids as $id) {
                    DB::table('seo_blocks_rules')->insert([
                        'seo_block_id' => $data['id'],
                        'target_type' => $data['target_type'],
                        'target_mode' => $data['target_mode'],
                        'target_id' => $id,
                        'position' => $counter,
                    ]);
                    $counter++;
                }
                $success = true;
                $message = 'Regola/e salvate con successo';
            } else {
                $message = 'Devi selezionare almeno un elemento per il campo Condizione';
            }
        }

        $data = array('success' => $success, 'message' => $message);

        \Utils::unwatch();

        return Json::encode($data);
    }

    function postRulesTable() {
        $id = \Input::get('id');

        $data = array('success' => true, 'html' => \Helper::getSeoBlocksRulesTable($id));

        return Json::encode($data);
    }

    function postRemoveRule($id) {
        DB::table('seo_blocks_rules')->where('id', $id)->delete();
        $data = array('success' => true, 'msg' => 'Record eliminato con successo');
        return Json::encode($data);
    }

}
