<?php

class EmailsController extends MultiLangController
{

    public $component_id = 67;
    public $title = 'Gestione Messaggi email';
    public $page = 'emails.index';
    public $pageheader = 'Gestione Messaggi email';
    public $iconClass = 'font-cog';
    public $model = 'Email';
    protected $rules = array(
        'name' => 'required',
        'code' => 'required',
        'sender' => 'required',
    );
    protected $lang_rules = array(
        'subject' => 'required',
        'body' => 'required',
    );
    protected $friendly_names = array(
        'name' => 'Nome del template',
        'code' => 'Codice del template',
        'sender' => 'Indirizzo email mittente',
    );
    protected $lang_friendly_names = array(
        'subject' => 'Oggetto del messaggio',
        'body' => 'Corpo del messaggio',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Messaggi email');
        $this->toFooter("js/echo/emails.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/emails.js");
        $this->page = 'emails.trash';
        $this->pageheader = 'Cestino Messaggi email';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/emails.js");
        $this->page = 'emails.create';
        $this->pageheader = 'Nuovo Messaggio email';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/emails.js");
        $this->page = 'emails.create';
        $this->pageheader = 'Modifica Messaggio email';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        //$obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)
            ->groupBy("emails.id")
            ->select('emails.id', 'code', 'name', 'active', 'emails.updated_at', 'emails.sdesc', 'emails_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('code', function ($data) {
                $v = $data['code'];
                return "<strong>$v</strong>";
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong><a href='$link'>{$data['name']}</a></strong>$add";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("emails.id")
            ->select('emails.id', 'code', 'name', 'active', 'emails.updated_at', 'emails.deleted_at', 'emails.sdesc', 'emails_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('code', function ($data) {
                $v = $data['code'];
                return "<strong>$v</strong>";
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["sender_name_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["sender_name_" . $lang] == "")
                    $_POST["sender_name_" . $lang] = $defaultName;
            }
        }

        \Input::replace($_POST);

    }




}