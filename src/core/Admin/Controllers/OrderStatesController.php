<?php

class OrderStatesController extends MultiLangController
{

    public $component_id = 34;
    public $title = 'Gestione Status Ordini';
    public $page = 'order_states.index';
    public $pageheader = 'Gestione Status Ordini';
    public $iconClass = 'font-cog';
    public $model = 'OrderState';
    protected $rules = array(
        'color' => 'required',
    );
    protected $lang_rules = array(
        'name' => 'required',
    );
    protected $friendly_names = array(
        'color' => 'Colore',
    );
    protected $lang_friendly_names = array();

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Status Ordini');
        $this->toFooter("js/echo/order_states.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/order_states.js");
        $this->page = 'order_states.trash';
        $this->pageheader = 'Cestino Messaggi email';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/order_states.js");
        $this->page = 'order_states.create';
        $this->pageheader = 'Nuovo Status Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/order_states.js");
        $this->page = 'order_states.create';
        $this->pageheader = 'Modifica Status Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        //$obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)
            ->groupBy("order_states.id")
            ->select(
                'order_states.id',
                'name',
                'invoice',
                'send_email',
                'unremovable',
                'delivery',
                'paid',
                'shipped',
                'icon',
                'color'
            );


        return \Datatables::of($pages)
            ->edit_column('name', function ($data) {
                return "<span class='label' style='background-color: {$data['color']}'><i class='fa {$data['icon']}'></i> {$data['name']}</span>";
            })
            ->edit_column('send_email', function ($data) {
                return $this->boolean($data, 'send_email');
            })
            ->edit_column('paid', function ($data) {
                return $this->boolean($data, 'paid');
            })
            ->edit_column('delivery', function ($data) {
                return $this->boolean($data, 'delivery');
            })
            ->edit_column('shipped', function ($data) {
                return $this->boolean($data, 'shipped');
            })
            ->edit_column('invoice', function ($data) {
                return $this->boolean($data, 'invoice');
            })
            ->edit_column('unremovable', function ($data) {
                return $this->boolean($data, 'unremovable');
            })
            ->remove_column('lang_id')
            ->remove_column('icon')
            ->remove_column('color')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("order_states.id")
            ->select('order_states.id', 'code', 'name', 'active', 'order_states.updated_at', 'order_states.deleted_at', 'order_states.sdesc', 'order_states_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('code', function ($data) {
                $v = $data['code'];
                return "<strong>$v</strong>";
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["sender_name_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["sender_name_" . $lang] == "")
                    $_POST["sender_name_" . $lang] = $defaultName;
            }
        }

        \Input::replace($_POST);

    }

    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        /*$export = ($this->action("getExport", TRUE));
        $actions[] = new AdminAction('Esporta CSV', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');*/
        return $actions;
    }


}