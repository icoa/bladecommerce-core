<?php

use Core\Condition\RuleResolver;

class OverrideRulesController extends BackendController
{

    public $component_id = 72;
    public $title = 'Gestione regole override Template';
    public $page = 'override_rules.index';
    public $pageheader = 'Gestione regole override Template';
    public $iconClass = 'font-cog';
    public $model = 'OverrideTemplate';
    protected $rules = array(
        'name' => 'required',
        'view' => 'required',
    );

    protected $friendly_names = array(
        'name' => 'Denominazione regola',
        'view' => 'Nome file vista',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco regole override Template');
        $this->toFooter("js/echo/override_rules.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/override_rules.js");
        $this->page = 'override_rules.trash';
        $this->pageheader = 'Cestino regole override Template';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/override.js");
        $this->toFooter("js/echo/builder.js");
        $this->page = 'override_rules.create';
        $this->pageheader = 'Nuova regola override Template';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {

        $this->toFooter("js/echo/override.js");
        $this->toFooter("js/echo/builder.js");
        $this->page = 'override_rules.create';
        $this->pageheader = 'Nuova regola override Template';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);

        //$obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::groupBy("override_rules.id")
            ->select('override_rules.id', 'override_rules.name', 'override_rules.view', 'override_rules.active', 'override_rules.created_at', 'position', 'sdesc');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong><br><em>" . $data['sdesc'] . '</em>';
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->add_column('active', function ($data) {
                return $this->boolean($data,'active');
            })
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()
            ->groupBy("override_rules.id")
            ->leftJoin("customers as C", "override_rules.customer_id", "=", "C.id")
            ->select('override_rules.id', 'override_rules.email', 'override_rules.lang_id', 'override_rules.user_ip', 'override_rules.marketing', 'override_rules.deleted_at', 'C.name as customer', 'customer_id');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('lang_id', function ($data) {
                $src = \AdminTpl::img("images/flags/{$data['lang_id']}.jpg");
                return "<img src='$src' />";
            })
            ->edit_column('marketing', function ($data) {
                return $this->boolean($data, 'marketing');
            })
            ->edit_column('customer', function ($data) {
                $link = \URL::action("CustomersController@getEdit", $data['customer_id']);
                return "<strong><a href='$link'>{$data['customer']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('customer_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
        \Cache::forget('override_templates');
    }

    function _after_create($model)
    {
        parent::_after_create($model);
        \Cache::forget('override_templates');
    }


    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);


        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }



    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $rules = Input::get('rules', []);
        $_POST['actions'] = null;
        if (count($rules) > 0) {
            $rules = serialize($rules);
            $_POST['actions'] = $rules;
        }

        if ($_POST['position'] == '') {
            $_POST['position'] = $this->position();
        }

        $conditions = \Input::get("rule")["conditions"];
        $rr = new RuleResolver();
        $_POST['conditions'] = $rr->getSerializable($conditions);

        \Input::replace($_POST);

    }


    function postAddBlock(){

        $type = Input::get('type');

        $ids = Input::get('ids',[]);
        $index = Input::get('index',0);

        $html = '';
        foreach($ids as $id){
            $d = ['type' => $type, 'id' => $id];
            $html .= self::renderAction($index,$d);
            $index++;
        }
        $success = true;
        return Json::encode(compact('success','html'));
    }



    static function renderActions($str){
        $html = '';
        $data = unserialize($str);

        if(count($data) > 0){
            foreach($data as $index => $d){
                $html .= self::renderAction($index,$d);
            }
        }
        return $html;
    }


    static function renderAction($index, $d){
        $obj = (object)$d;

        $data = Mainframe::selectNavsSingleTarget($obj->type,$obj->id);

        $label = $data['label'];
        $target = $data['name'];

        $html = <<<HTML
<li>
    <span class="label label-inverse">$label</span><span class="label label-success">$target</span>
    <span class="pull-right"><a href="#" class="action" data-action="remove">elimina</a></span>
    <input type="hidden" name="rules[$index][type]" value="$obj->type">
    <input type="hidden" name="rules[$index][id]" value="$obj->id">
</li>
HTML;

        return $html;
    }


}