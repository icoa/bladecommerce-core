<?php

use services\GoogleMerchantConfig;

class GoogleMerchantController extends BackendController
{

    public $component_id = 79;
    public $title = 'Gestione Google Merchant';
    public $page = 'google_merchant.index';
    public $pageheader = 'Gestione Google Merchant';
    public $iconClass = 'font-columns';
    const EMPLOYER_ID = 10;


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_create($model)
    {

    }

    function _prepare()
    {

    }

    public function getConfig()
    {
        $this->page = 'google_merchant.config';
        $title = 'Configurazione Google Merchant';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/google_merchant.js");
        $view = array();
        $this->toolbar('create');
        return $this->render($view);
    }

    public function getReport($report)
    {
        /*$this->page = 'danea.table';
        $title = $report == 'importable' ? 'Prodotti aggiornabili' : 'Prodotti non importabili';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/danea.js");
        $view = array('report' => $report);
        $this->toolbar();
        return $this->render($view);*/
    }

    public function getDeletable()
    {
        /*$this->page = 'danea.deletable';
        $this->pageheader = 'Prodotti rimossi in Danea';
        $this->addBreadcrumb('Prodotti rimossi in Danea');
        $this->toFooter("js/echo/danea.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);*/
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Report Google Merchant');
        $this->toFooter("js/echo/google_merchant.js");

        $enabled = explode(",", GoogleMerchantConfig::get('enabled_countries','IT,GB,US'));

        $countries = \Country::rows(\Core::getLang())->where('gm', 1)
            ->leftJoin('currencies', 'countries.currency_id', '=', 'currencies.id')
            ->select([
                'countries.id',
                'countries_lang.name',
                'countries.iso_code',
                'countries.accepted_lang',
                'countries.default_lang',
                DB::raw('currencies.name as currency'),
                DB::raw('currencies.iso_code as currency_iso'),
            ])
            ->orderBy('countries_lang.name')
            ->get();

        /*$locales = DB::table('locales')
            ->leftJoin('languages', 'locales.language_id', '=', 'languages.id')
            ->where('locales.active', 1)
            ->select([
                'locales.id',
                'locales.name',
                'locales.iso_code',
                'locales.locale',
                DB::raw('languages.name as language'),
            ])
            ->orderBy('locales.name')
            ->get();*/

        /*foreach ($locales as $row) {
            $names = \Country::rows(\Core::getLang())
                ->where('accepted_lang', 'like', "%$row->locale%")
                ->lists('name');
            $row->countries = implode(", ", $names);
        }*/

        foreach($countries as $country){
            $country->enabled = in_array($country->iso_code,$enabled);
            $country->support = DB::table('google_categories_schema')->where('locale',$country->iso_code)->count() > 0;
        }

        $view = array(
            'countries' => $countries
        );
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }


    public function getEdit($id)
    {

    }


    public function getTable()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        \Utils::watch();

        $pages = Product::leftJoin('products_lang', 'products.id', '=', 'products_lang.product_id')
            ->where('products_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', function($join) use ($lang_id){
                $join->on('brands_lang.brand_id', '=', 'products.brand_id')->on('brands_lang.lang_id', "=", 'products_lang.lang_id');
            })->leftJoin('collections_lang', function($join) use ($lang_id){
                $join->on('collections_lang.collection_id', '=', 'products.collection_id')->on('collections_lang.lang_id', "=", 'products_lang.lang_id');
            })->leftJoin('google_custom_labels', function($join) use ($lang_id){
                $join->on('google_custom_labels.node_product_id', '=', 'products.id');
            })->select(
                'products.id', 'sku', 'ean13', 'export_ean', 'products_lang.name', 'brands_lang.name as brand_name', 'collections_lang.name as collection_name', 'buy_price', 'sell_price_wt', 'price', 'is_out_of_production', 'qty', 'products_lang.published',
            'label_0', 'label_1', 'label_2', 'label_3', 'label_4'
            );

        return \Datatables::of($pages)

            ->edit_column('price', function ($data) {
                return \Format::currency($data['price'], true);
            })
            ->edit_column('sell_price_wt', function ($data) {
                return \Format::currency($data['sell_price_wt'], true);
            })
            ->edit_column('buy_price', function ($data) {
                return \Format::currency($data['buy_price'], true);
            })
            ->edit_column('is_out_of_production', function ($data) {
                return $this->boolean($data, 'is_out_of_production');
            })
            ->edit_column('published', function ($data) {
                return $this->boolean($data, 'published');
            })
            ->edit_column('export_ean', function ($data) {
                return $this->boolean($data, 'export_ean');
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }


    public function getTabletrash()
    {
        \Utils::watch();

        $lang_id = 'it';

        $pages = Product::rows($lang_id)
            ->whereIn('id', function ($query) {
                $query->select(['product_id'])
                    ->from('danea_import_deleted_products');
            })
            ->select(
                "id",
                "sku",
                "ean13",
                "name",
                "published",
                "buy_price",
                "sell_price_wt",
                "price",
                "qty",
                "is_out_of_production"
            );

        return \Datatables::of($pages)
            ->edit_column('buy_price', function ($data) {
                return \Format::currency($data['buy_price'], true);
            })
            ->edit_column('sell_price_wt', function ($data) {
                return \Format::currency($data['sell_price_wt'], true);
            })
            ->edit_column('price', function ($data) {
                return \Format::currency($data['price'], true);
            })
            ->edit_column('is_out_of_production', function ($data) {
                return $this->boolean($data, 'is_out_of_production');
            })
            ->edit_column('published', function ($data) {
                return $this->boolean($data, 'published');
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }


    private function _style()
    {
        echo <<<STYLE
<style>
    body {
        background-color:#000;
        font-family: Courier;
        font-size:12px;
        color:white;
    }

    .error{
        color:red;
    }

    .success{
        color:lime;
    }

    .message{
        color:cyan;
    }

    .warning{
        color:yellow;
    }

    .st1{
        color:magenta;
    }

    .st2{
        color:orange;
    }
</style>
STYLE;
    }


    protected function actions_default($params = [])
    {

        $config = ($this->action("getConfig", TRUE));
        $index = ($this->action("getIndex", TRUE));
        $actions = array(
            new AdminAction('Impostazioni', $config, 'font-edit', '', 'Imposta i parametri per la generazione dei Feed', 'index'),
            /*new AdminAction('Aggiornabili', $importable, 'font-ok', 'btn-success', 'Gestione dei prodotti aggiornabili', 'report'),
            new AdminAction('Inesistenti', $fixable, 'font-off', 'btn-danger', 'Gestione dei prodotti NON aggiornabili', 'report'),
            new AdminAction('Rimossi', $deletable, 'font-remove', 'btn-warning', 'Gestione dei prodotti rimossi da Danea', 'report'),*/

            //new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente', 'reload'),
        );
        return $actions;
    }




    function postSaveconfig(){
        //Flash current values to session
        \Input::flashExcept("_lang_fields[]","_lang_fields");

        $data = \Input::all();
        unset($data['_token']);
        unset($data['task']);

        $taxonomies = \Input::get('taxonomies',[]);
        if(count($taxonomies) > 0){
            unset($data['taxonomies']);
            $this->saveTaxonomies($taxonomies);
        }

        \Utils::log($data,__METHOD__);

        GoogleMerchantConfig::setMultiple($data);

        \Input::flush();

        Notification::success("Configurazione aggiornata con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getConfig"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }


    private function saveTaxonomies($taxonomies){
        \Utils::log($taxonomies,__METHOD__);
        foreach($taxonomies as $category_id => $locales){
            foreach($locales as $locale => $sdesc){
                $exist = DB::table('google_categories')->where('category_id',$category_id)->where('locale',$locale)->count();
                if($exist){
                    DB::table('google_categories')->where('category_id',$category_id)->where('locale',$locale)->update(['sdesc' => $sdesc]);
                }else{
                    DB::table('google_categories')->insert(
                        [
                            'sdesc' => $sdesc,
                            'category_id' => $category_id,
                            'locale' => $locale,
                        ]
                    );
                }
            }
        }
    }


    function postChooser(){
        $category = \Input::get('category',0);
        $locale = \Input::get('locale',false);
        $selected = \Input::get('selected','');
        $html = 'Google Taxonomy not available!';
        \Utils::log($selected,__METHOD__);
        if($locale){
            $rows = \DB::table('google_categories_schema')->where('locale',$locale)->orderBy('name')->get();
            $data = \EchoArray::set($rows)->keyIndex('name', 'name')->get();
            $html = \Form::select("taxonomies[$category][$locale]", $data, $selected, ['class' => 'select', 'style' => 'width:100%']);
        }
        return Response::json(compact('html'));
    }


    function postEan(){
        $ids = \Input::get('ids',[]);
        $flag = (int) \Input::get('flag',1);
        if(count($ids))DB::table('products')->whereIn('id',$ids)->update(['export_ean' => $flag]);
        $success = true;
        return Response::json(compact('success'));
    }


    function postLabels(){
        \Utils::log(\Input::all());
        \Utils::watch();
        $ids = \Input::get('ids',[]);
        $selected = \Input::get('selected',[]);
        $data = [];
        foreach($selected as $i => $bool){
            if($bool == 1){
                $data['label_'.$i] = \Input::get('label_'.$i,'');
            }
        }
        if(count($data)){
            foreach($ids as $id){
                $record = DB::table('google_custom_labels')->where('node_product_id',$id)->first();
                if($record){
                    DB::table('google_custom_labels')->where('node_product_id',$id)->update($data);
                }else{
                    $data['node_product_id'] = $id;
                    DB::table('google_custom_labels')->insert($data);
                }
            }
        }
        $success = true;
        return Response::json(compact('success'));
    }


}