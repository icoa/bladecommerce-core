<?php

use services\Fees\Repositories\FeesBackendRepository;

class InvoicesController extends BackendController
{

    public $component_id = 30;
    public $title = 'Gestione Fatture';
    public $page = 'invoices.index';
    public $pageheader = 'Gestione Fatture';
    public $iconClass = 'font-columns';
    public $model = 'Order';
    protected $rules = array();

    protected $friendly_names = array();

    /**
     * @var FeesBackendRepository
     */
    protected $feeRepository;

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->feeRepository = app(FeesBackendRepository::class);
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }


        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Fatture');
        $this->toFooter("js/echo/invoices.js?v=3");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'invoices.create';
        $this->toFooter("js/echo/invoices.js");
        $this->pageheader = 'Nuovo Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'invoices.create';
        $this->pageheader = 'Nuovo Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }


    public function getEdit($id)
    {
        return $this->getPreview($id);
        $this->page = 'invoices.create';
        $this->pageheader = 'Modifica Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getPreview($id)
    {
        $this->page = 'invoices.preview';
        $this->toFooter("js/echo/invoices.js");
        $this->pageheader = 'Dettagli Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        $model = $this->model;

        $pages = $model::where('invoice_number', '>', 0)
            ->leftJoin('customers', 'customer_id', '=', 'customers.id')
            ->select(
                'orders.id',
                'orders.invoice_number',
                'orders.invoice_type',
                'orders.reference',
                //'customer_id',
                DB::raw("CONCAT_WS(' ',firstname,lastname,company) as customer_name"),
                'orders.invoice_date',
                'orders.created_at',
                'orders.invoice_sent',
                'orders.total_order'
            );

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('invoice_number', function ($data) {
                $order = \Order::getObj($data['id']);
                $invoice = $order->getInvoice();
                $str = "<span class='label label-inverse'>{$invoice['number']}</span>";
                if ($invoice['hasFile']) {
                    $str .= "<a href='{$invoice['url']}' target='_blank' class='btn btn-mini btn-small ml15'>Apri documento</a>";
                }else{
                    OrderHelper::createDocumentFile('invoice', $order->id);
                    $str .= "<a href='{$invoice['url']}' target='_blank' class='btn btn-mini btn-small ml15'>Apri documento</a>";
                }
                return $str;
            })
            ->edit_column('invoice_date', function ($data) {
                return \Format::date($data['invoice_date']);
            })
            ->edit_column('invoice_type', function ($data) {
                $cssClass = $data['invoice_type'] == 'starling' ? 'label-important' : 'label-success';
                $name = $data['invoice_type'] == 'starling' ? 'Storno' : 'Ricevuta';
                return "<span class='label $cssClass'>$name</span>";
            })
            ->edit_column('reference', function ($data) {
                $url = URL::action("OrdersController@getPreview", $data['id']);
                return "<strong><a href='$url' target='_blank'>{$data['reference']}</a></strong>";
            })
            ->edit_column('customer_name', function ($data) {
                $s = $data['customer_name'];
                return "<strong>$s</strong>";
            })
            ->edit_column('total_order', function ($data) {
                $order = \Order::getObj($data['id']);
                return "<strong>" . $order->getGrandTotalAttribute() . "</strong>";
            })
            ->edit_column('invoice_sent', function ($data) {
                $bool = $data['invoice_sent'] == 1;
                return $bool ? '<span class="label label-success">Y</span>' : '<span class="label label-important">N</span>';
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        $export = ($this->action("getExport", TRUE));
        $user = Sentry::getUser();
        if($user->hasPermission('export')) {
            $actions[] = new AdminAction('Esporta CSV', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');
        }
        return $actions;
    }


    function getExport()
    {
        $this->page = 'invoices.export';
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
        }
        $this->pageheader = 'Esporta documenti fiscali';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('export');
        $view = array();
        return $this->render($view);
    }


    function getDownload()
    {
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
            $view = array();
            return $this->render($view);
        }
        \Utils::watch();
        ini_set('max_execution_time', 600);

        $from = Input::get('from');
        $to = Input::get('to');

        if ($from == '')
            $from = null;

        if ($to == '')
            $to = null;

        $params = compact('from', 'to');

        $filePath = $this->feeRepository->createCustomCsvFile($params);

        $filename = "receipts_default_" . time() . ".csv";

        return Response::download($filePath, $filename, ['content-type' => 'text/cvs']);
    }


}