<?php

use Carbon\Carbon;
use Core\Condition\RuleResolver;
use services\Repositories\CartRuleRepository;

class CartRulesController extends MultiLangController
{

    public $component_id = 39;
    public $title = 'Gestione Regole Carrello';
    public $page = 'cartrules.index';
    public $pageheader = 'Gestione Regole Carrello';
    public $iconClass = 'font-columns';
    public $model = 'CartRule';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required'
    );
    protected $friendly_names = array();
    protected $lang_friendly_names = array(
        'name' => 'Denominazione regola',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Regole Carrello');
        $this->toFooter("js/echo/cartrules.js?v=2");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/cartrules.js?v=2");
        $this->page = 'cartrules.trash';
        $this->pageheader = 'Cestino Regole Carrello';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/cartrules.js?v=2");
        $this->toFooter("js/echo/builder.js");
        $this->toFooter("js/echo/repeatable_prices.js");
        $this->page = 'cartrules.create';
        $this->pageheader = 'Nuova Regola Carrello';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/cartrules.js?v=2");
        $this->toFooter("js/echo/builder.js");
        $this->toFooter("js/echo/repeatable_prices.js");
        $this->page = 'cartrules.create';
        $this->pageheader = 'Modifica Regola Carrello';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getPreview($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->fillLanguages();
        $view = array('obj' => $obj);
        return Theme::scope("cartrules.preview", $view)->content();
    }

    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)
            ->groupBy("cart_rules.id")
            ->select('cart_rules.id', 'name', 'priority', 'code', 'active', 'date_from', 'date_to', 'cart_rules.created_at', 'cart_rules.position', 'cart_rules.sdesc', 'cart_rules_lang.lang_id');

        $now = Carbon::now();

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('date_from', function ($data) use ($now) {
                $style = null;
                if ($data['date_from'] != '') {
                    $date = Carbon::parse($data['date_from']);
                    $style = $now->lt($date) ? 'label label-important' : 'label label-success';
                }
                return "<span class='$style'>" . \Format::datetime($data['date_from']) . '</span>';
            })
            ->edit_column('date_to', function ($data) use ($now) {
                $style = null;
                if ($data['date_to'] != '') {
                    $date = Carbon::parse($data['date_to']);
                    $style = $now->gt($date) ? 'label label-important' : 'label label-success';
                }
                return "<span class='$style'>" . \Format::datetime($data['date_to']) . '</span>';
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong><a href='$link'>{$data['name']}</a></strong>$add";
            })
            ->edit_column('code', function ($data) {
                $name = $data['code'];
                return "<strong>$name</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->edit_column('position', function ($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("cart_rules.id")
            ->select('cart_rules.id', 'name', 'priority', 'code', 'active', 'date_from', 'date_to', 'cart_rules.created_at', 'cart_rules.deleted_at', 'cart_rules.sdesc', 'cart_rules_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('date_from', function ($data) {
                return \Format::datetime($data['date_from']);
            })
            ->edit_column('date_to', function ($data) {
                return \Format::datetime($data['date_to']);
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->edit_column('code', function ($data) {
                $name = $data['code'];
                return "<strong>$name</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
        $this->handleRules($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
        $this->handleRules($model);
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];
        $defaultImageList = $_POST["image_list_" . $langDef];
        $defaultImageContent = $_POST["image_content_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
                if ($_POST["image_list_" . $lang] == "")
                    $_POST["image_list_" . $lang] = $defaultImageList;
                if ($_POST["image_content_" . $lang] == "")
                    $_POST["image_content_" . $lang] = $defaultImageContent;
            }
        }

        $_POST['priority'] = (int)\Input::get("priority", 1);
        $_POST['quantity'] = (int)\Input::get("quantity", 0);
        $_POST['quantity_per_user'] = (int)\Input::get("quantity_per_user", 0);
        $_POST['carrier_restriction'] = (int)\Input::get("carrier_restriction", 0);
        $_POST['group_restriction'] = (int)\Input::get("group_restriction", 0);
        $_POST['country_restriction'] = (int)\Input::get("country_restriction", 0);
        $_POST['date_from'] = Format::sqlDatetime($_POST['date_from']);
        $_POST['date_to'] = Format::sqlDatetime($_POST['date_to']);

        $conditions = \Input::get("rule")["conditions"];
        $rr = new RuleResolver();
        $_POST['conditions'] = $rr->getSerializable($conditions);

        $conditions = \Input::get("products_apply_conditions")["conditions"];
        $rr = new RuleResolver();
        $_POST['products_apply_conditions'] = $rr->getSerializable($conditions);

        $conditions = \Input::get("so_conditions_x")["conditions"];
        $rr = new RuleResolver();
        $_POST['so_conditions_x'] = $rr->getSerializable($conditions);

        $conditions = \Input::get("so_conditions_y")["conditions"];
        $rr = new RuleResolver();
        $_POST['so_conditions_y'] = $rr->getSerializable($conditions);

        $_POST['actions'] = $this->setActions();


        $model = $this->model;
        if ((int)$_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }


    private function handleRules($model)
    {
        \Utils::watch();
        $cart_rule_id = $model->id;

        \DB::table("cart_rules_carrier")->where("cart_rule_id", $cart_rule_id)->delete();
        $carrier_restriction = (int)\Input::get("carrier_restriction", 0);
        if ($carrier_restriction == 1) {
            $unselected_carriers = \Input::get("unselected_carriers", []);
            if (!empty($unselected_carriers)) {
                foreach ($unselected_carriers as $carrier_id) {
                    \DB::table("cart_rules_carrier")->insert(compact("cart_rule_id", "carrier_id"));
                }
            }
        }

        \DB::table("cart_rules_country")->where("cart_rule_id", $cart_rule_id)->delete();
        $country_restriction = (int)\Input::get("country_restriction", 0);
        if ($country_restriction == 1) {
            $unselected_countries = \Input::get("unselected_countries", []);
            if (!empty($unselected_countries)) {
                foreach ($unselected_countries as $country_id) {
                    \DB::table("cart_rules_country")->insert(compact("cart_rule_id", "country_id"));
                }
            }
        }

        \DB::table("cart_rules_group")->where("cart_rule_id", $cart_rule_id)->delete();
        $group_restriction = (int)\Input::get("group_restriction", 0);
        if ($group_restriction == 1) {
            $unselected_groups = \Input::get("unselected_groups", []);
            if (!empty($unselected_groups)) {
                foreach ($unselected_groups as $group_id) {
                    \DB::table("cart_rules_group")->insert(compact("cart_rule_id", "group_id"));
                }
            }
        }


        return;

    }


    function postCode()
    {
        $html = \Core::randomString();
        $success = true;
        $response = compact("html", "success");
        return \Json::encode($response);
    }

    function postGenerate()
    {
        $error = '';
        $success = false;
        $cp_len = \Input::get("cp_len");
        $cp_total = \Input::get("cp_total");
        $cp_prefix = \Input::get("cp_prefix");
        $cp_suffix = \Input::get("cp_suffix");
        $cart_rule_id = \Input::get("cart_rule_id");

        $params = [
            'len' => $cp_len,
            'total' => $cp_total,
            'prefix' => $cp_prefix,
            'suffix' => $cp_suffix,
            'cart_rule_id' => $cart_rule_id,
        ];

        $repository = new CartRuleRepository();
        try {
            $repository->generateCoupons($params);
            $success = true;
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return Json::encode(compact('success', 'error'));
    }


    function postCoupons()
    {
        $cart_rule_id = \Input::get("cart_rule_id");
        $success = true;
        $html = CartRulesController::getCouponsTable($cart_rule_id);
        return Json::encode(compact('success', 'html'));
    }


    static function getCarriers($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        \Utils::watch();
        $xref_ids = \DB::table("cart_rules_carrier")->where("cart_rule_id", $id)->lists("carrier_id");
        \Utils::log($xref_ids);
        $ids = array_merge($ids, $xref_ids);
        $query = \Carrier::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getGroups($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("cart_rules_group")->where("cart_rule_id", $id)->lists("group_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \CustomerGroup::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getCountries($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("cart_rules_country")->where("cart_rule_id", $id)->lists("country_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \Country::rows($lang)->where('active', 1);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }


    private function setActions()
    {
        $input = \Input::all();
        $fields = [
            "free_shipping",
            "rule_type",
            "reduction_type",
            "reduction_target",
            "reduction_shipping",
            "reduction_value",
            "reduction_currency",
            "reduction_tax",
            "reduction_price_target",
            "products_apply_conditions",
            "so_amount_x",
            "so_target_y",
            "so_conditions_x",
            "so_amount_y",
            "so_conditions_y",
            "so_reduction_type",
            "so_reduction_value",
            "so_reduction_currency",
            "so_limit_y",
            "so_reduction_price_target",
            "so_target_add_cart",
            "so_target_cheapest",
            "so_repeatable",];
        $data = [];
        foreach ($fields as $f) {
            $data[$f] = isset($_POST[$f]) ? $_POST[$f] : "";
        }
        return serialize($data);
    }


    static function getActions($actions)
    {
        return unserialize($actions);
    }


    public function postRemcoupon($id)
    {
        $obj = \Coupon::find($id);
        $obj->delete();
        $success = true;
        return Json::encode(compact('success'));
    }

    public function postRemcoupons()
    {
        \Utils::log($_POST, "POST");
        $rows = \Coupon::whereIn("id", \Input::get('coupons', []))->get();
        foreach ($rows as $row) {
            $obj = \Coupon::find($row->id);
            $obj->delete();
        }
        $success = true;
        return Json::encode(compact('success'));
    }


    static function getCouponsTable($cart_rule_id)
    {

        $body = '<tr><td colspan="99"><em>Non ci sono Coupon aggiuntivi per questa regola</em></td></tr>';

        $rows = \Coupon::where('cart_rule_id', $cart_rule_id)->take(100)->orderBy('created_at', 'desc')->get();
        if (count($rows) > 0) {
            $body = '';
            foreach ($rows as $row) {

                $used = ($row->used_by > 0) ? 'SI' : 'NO';

                $body .= <<<BODY
<tr id="coupon_$row->id" rel="$row->id">
    <th class="center" width="50"><input type="checkbox" name="coupons[]" value="$row->id"></th>
    <td class="center"><b>$row->code</b></td>
    <td class="center">$row->created_at</td>
    <td class="center">$used</td>
    <td class="center">$row->used_at</td>
    <td class="center">
        <div class="btn-group">
            <button type="button" title="" onclick="Echo.xhrAction(this,true,CartRules.reloadCouponTable);" data-action="/admin/cartrules/remcoupon/$row->id" class="btn hovertip" data-placement="top"
                    data-original-title="Elimina"><i class="icon-remove"></i></button>
        </div>
    </td>
</tr>
BODY;

            }
        }

        $table = <<<TABLE
<table class="table table-striped table-input" id="couponsTable">
    <thead>
    <tr>
        <th class="center" width="50"></th>
        <th class="center">Codice</th>
        <th class="center">Creato il</th>
        <th class="center">Utilizzato?</th>
        <th class="center">Utilizzato il</th>
        <th class="center">Azioni</th>
    </tr>
    </thead>
    <tbody>
        $body
    </tbody>
</table>
TABLE;

        return $table;

    }


    function getExport($id)
    {
        $repository = new CartRuleRepository();
        $repository->exportCsvForCoupons($id);
        try {
            $filePath = $repository->exportCsvForCoupons($id);
            return Response::download($filePath);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        return $error;
    }
}
