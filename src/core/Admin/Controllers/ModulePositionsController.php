<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class ModulePositionsController extends BackendController
{

    public $component_id = 27;
    public $title = 'Posizioni moduli';
    public $page = 'module_positions.index';
    public $pageheader = 'Posizioni moduli';
    public $iconClass = 'font-columns';
    public $model = 'ModulePosition';
    protected $rules = array(
        'name' => 'required|unique:module_positions,name',
    );
    protected $friendly_names = array(
        'name' => 'Denominazione',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco posizioni Moduli');
        $this->toFooter("js/echo/module_positions.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/module_positions.js");
        $this->page = 'module_positions.create';
        $this->pageheader = 'Nuova posizione Moduli';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/module_positions.js");
        $this->page = 'module_positions.create';
        $this->pageheader = 'Modifica posizione Moduli';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getDesign()
    {
        $this->component_id = 60;
        $this->toFooter("js/echo/design.js");
        $this->page = 'module_positions.design';
        $this->pageheader = 'Gestione template';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getTable()
    {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::leftJoin("modules as M", "module_positions.name", "=", "M.mod_position")->groupBy("module_positions.id")->select('module_positions.id', 'module_positions.name', DB::raw("COUNT(mod_position) as total"));

        return \Datatables::of($pages)
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['name'] = 'required|unique:module_positions,name,' . $model->id;
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $_POST['name'] = \Str::slug($_POST['name'],'_');

        \Input::replace($_POST);

    }

    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">    
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = \URL::action($this->action("getIndex"));

                $actions = array(
                    new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva e chiudi', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );

                break;

            default:
                $create = ($this->action("getCreate", TRUE));
                $design = ($this->action("getDesign", TRUE));
                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $actions = array(
                    new AdminAction('Template', $design, 'font-cog', 'btn-warning', 'Passa alla gestione Template'),
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }


    public function postSavedesign(){
        $data = \Input::get("data");
        \Utils::log($data,"postSavedesign");
        \Utils::watch();
        if(is_array($data) AND count($data)){
            foreach($data as $position){
                $name = $position['name'];
                \Module::where("mod_position",$name)->update(['mod_position' => '']);
                if(isset($position['modules'])){
                    foreach($position['modules'] as $module){
                        $mod_id = (int)$module['id'];
                        $mod_position = (int)$module['position'];
                        \Module::where("id",$mod_id)->update(['mod_position' => $name, 'position' => $mod_position]);
                    }
                }else{

                }
            }
        }
        $success = true;
        return Json::encode(compact("success"));
    }


}
