<?php

use Core\Token;
use Core\Metadata;
use SebastianBergmann\PHPLOC\Log\Csv;
use services\Elastic\Repositories\ElasticProductSearchRepository;
use services\Morellato\Database\Sql\Connection;
use services\Repositories\CsvProductsRepository;

class ProductsController extends MultiLangController
{

    public $component_id = 23;
    public $title = 'Gestione Prodotti';
    public $page = 'products.index';
    public $pageheader = 'Gestione Prodotti';
    public $iconClass = 'font-columns';
    public $model = 'Product';
    protected $rules = array(
        'sku' => 'required',
        'sell_price' => 'required',
        'sell_price_wt' => 'required',
        'qty' => 'required',
        'price' => 'required',
        'weight' => 'required',
        'default_category_id' => 'required',
    );
    protected $lang_rules = array(
        'name' => 'required',
        //'slug' => 'unique_lang:products_lang,slug,product_id'
    );
    protected $friendly_names = array(
        'sell_price_wt' => "Prezzo di listino tasse incl.",
        'default_category_id' => "Categoria di default",
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/products.js?v=107");
        $this->metadata = array();
    }

    function _before_create()
    {
        $this->_prepare();
        $_POST['original_inventory_qty'] = $_POST['qty'];
    }

    function _before_update($model)
    {
        //$this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
        $this->prevQty = $model->qty;
        global $beforeModel;
        $beforeModel = clone $model;
        $beforeModel->fillLanguages();
    }

    function _prepare()
    {
        //\Utils::log($_POST, 'PREPARE POST');
        if (isset($_POST['mtt_name']) AND $_POST['mtt_name'] > 0) {
            unset($this->lang_rules['name']);
        }
        $_POST['old_id'] = (int)$_POST['old_id'];
        if ($_POST['old_id'] == 0) {
            $_POST['old_id'] = null;
        }
        $_POST['weight'] = (float)$_POST['weight'];
        $_POST['depth'] = (float)$_POST['depth'];
        $_POST['width'] = (float)$_POST['width'];
        $_POST['height'] = (float)$_POST['height'];
        $_POST['package_weight'] = (float)$_POST['package_weight'];
        $_POST['package_depth'] = (float)$_POST['package_depth'];
        $_POST['package_width'] = (float)$_POST['package_width'];
        $_POST['package_height'] = (float)$_POST['package_height'];
        $_POST['additional_shipping_cost'] = (float)$_POST['additional_shipping_cost'];
        $_POST['buy_price'] = ((float)$_POST['buy_price'] == 0) ? null : (float)$_POST['buy_price'];
        $_POST['sell_price_wt'] = ((float)$_POST['sell_price_wt'] == 0) ? null : (float)$_POST['sell_price_wt'];
        $_POST['availability_days'] = ((int)$_POST['availability_days'] == 0) ? null : (int)$_POST['availability_days'];
        $_POST['new_from_date'] = Format::sqlDatetime($_POST['new_from_date']);
        $_POST['new_to_date'] = Format::sqlDatetime($_POST['new_to_date']);
        $_POST['available_from_date'] = Format::sqlDate($_POST['available_from_date']);
        $_POST['available_to_date'] = Format::sqlDate($_POST['available_to_date']);
        if ((float)$_POST['unit_price'] == 0) {
            $_POST['unit_price'] = null;
        }
        $_POST['parent_id'] = (int)$_POST['parent_id'];
        $_POST['price_nt'] = (float)$_POST['sell_price'];
        $_POST['price'] = (float)$_POST['price'];
        if (isset($_POST['default_category_id'])) {
            $default_category_id = $_POST['default_category_id'];
            if ((int)$default_category_id > 0) {
                $_POST['main_category_id'] = \CategoryHelper::getMainCategoryId($default_category_id);
            }

        }

    }

    function _prepare_data()
    {

        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $md = Metadata::getInstance();

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            /*$source = $_POST["name_" . $lang];
            $target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }
            $target = $_POST["h1_" . $lang];
            if ($target == "") {
                $_POST["h1_" . $lang] = $source;
            }*/

            //now save the real data
            $token = new Token("");
            $token->setLang($lang);
            $token->set("sku", \Input::get("sku"));
            $token->set("brand_id", \Input::get("brand_id"));
            $token->set("collection_id", \Input::get("collection_id"));
            $token->set("category_id", $_POST['main_category_id']);
            $token->set("default_category_id", \Input::get("default_category_id"));
            if (isset($_POST['id']) AND $_POST['id'] > 0) {
                $token->set("product_id", $_POST['id']);
            }

            $tokenizable = array("name", "metatitle", "h1", "metakeywords", "metadescription", "metafacebook", "metatwitter", "metagoogle");
            $md_fields = array();
            foreach ($tokenizable as $field) {
                /*//save the original (may contain tokens)
                $value = $_POST[$field . "_" . $lang];
                //now save the real data
                $token->rebind($value);*/

                if (isset($_POST['mtt_' . $field])) {
                    $mtt_name = (int)$_POST['mtt_' . $field];
                    if ($mtt_name > 0) {
                        $mtt = \MagicTokenTemplate::find($mtt_name);
                        $token->rebind($mtt->{$lang}->content);
                        $_POST[$field . "_" . $lang] = trim($token->render());
                    }
                }
                $md_fields[$field] = $_POST[$field . "_" . $lang];
                /* $md_fields[$field] = $value;
                 $_POST[$field . "_" . $lang] = trim($token->render());*/
            }

            //trim always the name
            $_POST["name_" . $lang] = trim($_POST["name_" . $lang]);

            //setting slug always as the name
            $_POST["slug_" . $lang] = \Str::slug($_POST["name_" . $lang]);

            $md->setLang($lang, "fields", $md_fields);
        }

        if ((int)$_POST['position'] == 0) {
            $_POST['position'] = $this->position();
        } else {
            $_POST['position'] = (int)$_POST['position'];
        }

        \Input::replace($_POST);
        \Utils::log($_POST, __METHOD__);
    }

    protected function position($parent_id = false)
    {
        $category_id = \Input::get('default_category_id', 0);
        $max = DB::table("products")->where('default_category_id', $category_id)->max("position");
        return $max + 1;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Prodotti');
        $this->toFooter('js/echo/products.groups.actions.js');
        $this->pageheader = 'Gestione Prodotti<a style="margin:0 0 0 20px" class="btn btn-mini" href="/admin/products/search-shops">Ricerca su Negoziando</a>';

        if(feats()->enabled('elasticsearch')){
            $this->pageheader .= '<a style="margin:0 0 0 20px" class="btn btn-mini" href="/admin/products/elastic">Indice ElasticSearch</a>';
        }

        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'products.trash';
        $this->pageheader = 'Cestino Prodotti';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getSelect()
    {
        $this->toFooter("js/echo/products.select.js");
        $this->page = 'products.select';
        $this->pageheader = 'Seleziona Prodotti';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        global $actions;
        $actions = array(
            new AdminAction('Associa i prodotti selezionati', "javascript:ProductsSelect.submit();", 'font-save', 'btn-success', 'Associa i prodotti selezionati nella relazione precedente'),
            new AdminAction('Chiudi questa finestra', "javascript:window.close();", 'font-remove'),
        );
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/products.combinations.js");
        $this->toFooter("js/echo/products.prices.js");
        $this->page = 'products.create';
        $this->pageheader = 'Nuovo Prodotto';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/products.combinations.js");
        $this->toFooter("js/echo/products.prices.js");
        $this->page = 'products.create';

        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $obj->rebindAttributes();
        $this->pageheader = 'Modifica Prodotto: ' . $obj->name_it;
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $parent_id = isset($_REQUEST['parent_id']) ? intval($_REQUEST['parent_id']) : 0;

        $categories = \Input::get("categories");
        $brands = \Input::get("brands");
        $collections = \Input::get("collections");
        $price_rules = \Input::get("price_rules");
        $min_price = (float)\Input::get("min_price");
        $max_price = \Input::get("max_price");

        //$categories = $brands = $collections = "";

        $model = $this->model;

        $queryBuilder = $model::leftJoin('products_lang', 'products.id', '=', 'products_lang.product_id')
            ->where('products_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', function ($join) use ($lang_id) {
                $join->on('brands_lang.brand_id', '=', 'products.brand_id')->on('brands_lang.lang_id', "=", 'products_lang.lang_id');
            })->leftJoin('collections_lang', function ($join) use ($lang_id) {
                $join->on('collections_lang.collection_id', '=', 'products.collection_id')->on('collections_lang.lang_id', "=", 'products_lang.lang_id');
            });
        //->leftJoin('brands_lang', 'products.brand_id', '=', 'brands_lang.brand_id')
        //->where('brands_lang.lang_id', $lang_id);

        if ($categories != '') {
            $categories = explode(",", $categories);
            \Session::set("filter_categories", $categories);
            $queryBuilder->where(function ($query) use ($categories) {
                $query->whereIn('products.id', function ($query) use ($categories) {
                    $query->select(['product_id'])
                        ->from('categories_products')
                        ->whereIn('category_id', $categories);
                });
                $query->orWhereIn("products.main_category_id", $categories);
            });

        } else {
            \Session::forget("filter_categories");
        }

        if ($brands != '') {
            $brands = explode(",", $brands);
            \Session::set("filter_brands", $brands);
            $queryBuilder->whereIn('products.brand_id', $brands);
        } else {
            \Session::forget("filter_brands");
        }
        if ($collections != '') {
            $collections = explode(",", $collections);
            \Session::set("filter_collections", $collections);
            $queryBuilder->whereIn('products.collection_id', $collections);
        } else {
            \Session::forget("filter_collections");
        }
        if ($price_rules != '') {
            $price_rules = explode(",", $price_rules);
            \Session::set("filter_price_rules", $price_rules);
            $queryBuilder->whereIn('products.id', function ($query) use ($price_rules) {
                $query->select(['product_id'])
                    ->from('products_specific_prices')
                    ->whereIn('price_rule_id', $price_rules);
            });
        } else {
            \Session::forget("filter_price_rules");
        }
        if ($min_price > 0) {
            \Session::set("filter_min_price", $min_price);
            $queryBuilder->where("products.price", ">=", $min_price);
        } else {
            \Session::forget("filter_min_price");
        }
        if ($max_price > 0) {
            \Session::set("filter_max_price", $max_price);
            $queryBuilder->where("products.price", "<=", $max_price);
        } else {
            \Session::forget("filter_max_price");
        }

        $queryBuilder->groupBy("products.id");

        //\Utils::log($queryBuilder->getBindings(),"ORIGINAL BINDINGS");

        //\Utils::watch();
        $pages = $queryBuilder->select(
            'products.id',
            'products_lang.name',
            'sku',
            'sap_sku',
            'products.ean13',
            'brands_lang.name as brand_name',
            'collections_lang.name as collection_name',
            'products_lang.published',
            'is_out_of_production',
            'is_soldout',
            'is_outlet',
            'is_new',
            'qty',
            'cnt_images',
            'price',
            'products.updated_at',
            'products.position',
            'indexable',
            'products_lang.ldesc',
            'products_lang.lang_id',
            'spin'
        );
        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('price', function ($data) {
                return '<b>' . \Format::currency($data['price'], true) . '</b>';
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                //return ($data['published'] == 1) ? '<span class="label label-success">Att.</span>' : '<span class="label label-important">Dis.</span>';
                return $this->checkboxLang($data, 'published', $data['lang_id']);
            })
            ->edit_column('qty', function ($data) {
                $css = ($data['qty'] >= 1) ? 'label-success' : 'label-important';
                $id = $data['id'];
                return '<span onclick="Echo.remoteModal(\'/admin/products/stocks/' . $id . '\');" class="label ' . $css . '">' . $data['qty'] . '</span>';
            })
            ->edit_column('cnt_images', function ($data) {
                $css = ($data['cnt_images'] >= 1) ? 'label-success' : 'label-important';
                $hasSpin = '';
                if ($data['spin'] != '')
                    $hasSpin = '<a href="' . $data['spin'] . '" target="_blank"><span class="label">360</span></a>';
                return '<span class="label ' . $css . '">' . $data['cnt_images'] . '</span>' . $hasSpin;
            })
            ->edit_column('is_out_of_production', function ($data) {
                return $this->checkbox($data, 'is_out_of_production');
            })
            ->edit_column('is_new', function ($data) {
                return $this->checkbox($data, 'is_new');
            })
            ->edit_column('is_soldout', function ($data) {
                return $this->checkbox($data, 'is_soldout');
            })
            ->edit_column('is_outlet', function ($data) {
                return $this->checkbox($data, 'is_outlet');
            })
            ->edit_column('position', function ($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('indexable')
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->remove_column('spin')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('products_lang', 'products.id', '=', 'products_lang.product_id')
            ->where('products_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', 'products.brand_id', '=', 'brands_lang.brand_id')
            ->where('brands_lang.lang_id', $lang_id)
            ->select('products.id', 'sku', 'products_lang.name', 'brands_lang.name as brand_name', 'products.deleted_at', 'products.created_at', 'indexable', 'products_lang.ldesc', 'products_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('indexable')
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    protected function column_actions($data)
    {

        $destroy = \URL::action($this->action("postDelete"), $data['id']);
        $clone = \URL::action($this->action("postClone"), $data['id']);

        $link = \URL::action($this->action("getPreview"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">    
    <li><a href="javascript:;" onclick="Echo.remoteModalLarge('$link');" class="btn" title="Anteprima"><i class="icon-eye-open"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$clone');" class="btn" title="Clona"><i class="icon-share"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;

        return $actions;
    }


    public function postSelected()
    {

        $ids = \Input::get("ids");
        $lang_id = \Input::get("lang_id");

        $rows = \Product::rows($lang_id)->whereIn("id", $ids)->orderBy("name")->get();
        $html = '';
        foreach ($rows as $row) {
            $html .= "<option value=\"$row->id\">{$row->id} - {$row->name}</option>";
        }

        $response = ['success' => true, 'msg' => 'Prodotti selezionati con successo', 'html' => $html];
        return \Json::encode($response);
    }


    public function getListall()
    {
        //\Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $parent_id = isset($_REQUEST['parent_id']) ? intval($_REQUEST['parent_id']) : 0;

        $categories = \Input::get("categories");
        $brands = \Input::get("brands");
        $collections = \Input::get("collections");
        $min_price = (float)\Input::get("min_price");
        $max_price = \Input::get("max_price");

        //$categories = $brands = $collections = "";

        $model = $this->model;

        $queryBuilder = $model::leftJoin('products_lang', 'products.id', '=', 'products_lang.product_id')
            ->where('products_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', function ($join) use ($lang_id) {
                $join->on('brands_lang.brand_id', '=', 'products.brand_id')->on('brands_lang.lang_id', "=", 'products_lang.lang_id');
            })->leftJoin('collections_lang', function ($join) use ($lang_id) {
                $join->on('collections_lang.collection_id', '=', 'products.collection_id')->on('collections_lang.lang_id', "=", 'products_lang.lang_id');
            });
        //->leftJoin('brands_lang', 'products.brand_id', '=', 'brands_lang.brand_id')
        //->where('brands_lang.lang_id', $lang_id);

        if ($categories != '') {

            $categories = explode(",", $categories);
            \Session::set("filter_categories", $categories);
            $queryBuilder->whereIn('products.id', function ($query) use ($categories) {
                $query->select(['product_id'])
                    ->from('categories_products')
                    ->whereIn('category_id', $categories);
            });
        } else {
            \Session::forget("filter_categories");
        }

        if ($brands != '') {

            $brands = explode(",", $brands);
            \Session::set("filter_brands", $brands);
            $queryBuilder->whereIn('products.brand_id', $brands);
        } else {
            \Session::forget("filter_brands");
        }
        if ($collections != '') {

            $collections = explode(",", $collections);
            \Session::set("filter_collections", $collections);
            $queryBuilder->whereIn('products.collection_id', $collections);
        } else {
            \Session::forget("filter_collections");
        }
        if ($min_price > 0) {
            \Session::set("filter_min_price", $min_price);
            $queryBuilder->where("products.price", ">=", $min_price);
        } else {
            \Session::forget("filter_min_price");
        }
        if ($max_price > 0) {
            \Session::set("filter_max_price", $max_price);
            $queryBuilder->where("products.price", "<=", $max_price);
        } else {
            \Session::forget("filter_max_price");
        }

        //\Utils::watch();
        $pages = $queryBuilder->select(
            'products.id', 'sku', 'products_lang.name', 'brands_lang.name as brand_name', 'collections_lang.name as collection_name', 'products_lang.published', 'qty', 'buy_price', 'sell_price_wt', 'price', 'is_new', 'products.created_at', 'indexable', 'products_lang.ldesc', 'products_lang.lang_id'
        );
        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('buy_price', function ($data) {
                return \Format::currency($data['buy_price'], true);
            })
            ->edit_column('sell_price_wt', function ($data) {
                return \Format::currency($data['sell_price_wt'], true);
            })
            ->edit_column('price', function ($data) {
                return \Format::currency($data['price'], true);
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->edit_column('published', function ($data) {
                return $this->renderBoolean($data, 'published', $data['lang_id']);
            })
            ->edit_column('is_new', function ($data) {
                return $this->renderBoolean($data, 'is_new', $data['lang_id']);
            })
            ->edit_column('qty', function ($data) {
                $css = ($data['qty'] >= 1) ? 'label-success' : 'label-important';
                return '<span class="label ' . $css . '">' . $data['qty'] . '</span>';
            })
            ->remove_column('indexable')
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function postRemoveAttribute($id)
    {

    }

    function postAddAttribute()
    {
        $insert_attributes = \Input::get("insert_attributes");

        if (count($insert_attributes) == 0) {
            $success = false;
            $error = "Devi specificare almeno un elemento da aggiungere";
            $data = compact("success", "error");
            return Json::encode($data);
        }

        $success = true;
        $html = '';
        foreach ($insert_attributes as $id) {
            $ah = new AttributeHtml($id);
            $html .= $ah->getRow();
        }
        $data = compact("success", "html", "insert_attributes");
        return Json::encode($data);
    }

    function postAttributesTable()
    {
        $product_id = \Input::get('product_id', 0);
        $brand_id = \Input::get('brand_id', 0);
        $collection_id = \Input::get('collection_id', 0);
        $categories_ids = \Input::get('categories_ids', 0);

        $html = \Helper::getAttributesTableForProduct($product_id, $categories_ids, $brand_id, $collection_id);

        $data = array('success' => true, 'msg' => 'Set attributi caricato correttamente', 'html' => $html);
        return Json::encode($data);
    }

    function postGroupActions()
    {

        $html = \ProductHelper::getGroupActionsPanel();

        $data = array('success' => true, 'html' => $html);
        return Json::encode($data);
    }

    function postAddImage($product_id)
    {

        $upload_filelist = \Input::get("files");
        $savePath = public_path() . "/assets/products/";
        $sourcePath = storage_path() . "/uploads/";
        if (!File::isDirectory($savePath)) {
            File::makeDirectory($savePath, 0775);
        }

        $languages = \Mainframe::languagesCodes();


        $rows = ProductImage::where("product_id", $product_id)->get();
        $totalImages = count($rows);
        //$candidate_id = ProductImage::max('id');

        $files = explode(",", $upload_filelist);
        $position = $totalImages;
        foreach ($files as $file) {

            $uploadedFile = $sourcePath . $file;
            if (File::exists($uploadedFile)):
                $ext = File::extension($uploadedFile);
                $position++;
                //$candidate_id++;
                //$newFile = \Str::lower($candidate_id.".".$ext);
                //$newFile = $file;

                $ds = array(
                    "product_id" => $product_id,
                    "filename" => $file,
                    "position" => $position,
                    "cover" => ($position == 1) ? 1 : 0,
                );

                foreach ($languages as $lang) {
                    $ds["legend_" . $lang] = null;
                }

                $image = new ProductImage();
                $image->setDataSource($ds);
                $image->save();

                if ($image AND $image->id > 0) { //update table with the real name and move file
                    try {
                        $newFile = \Str::lower($image->id . "." . $ext);
                        File::move($uploadedFile, $savePath . $newFile);
                        DB::table('images')->where('id', $image->id)->update(['filename' => $newFile]);
                        chmod($savePath . $newFile, 0775);
                    } catch (\Exception $e) {
                        audit_exception($e, __METHOD__);
                        \Utils::log($e->getMessage(), __METHOD__);
                    }
                }
            endif;
        }

        $result = array('success' => true);
        return Json::encode($result);
    }

    function _after_create($model)
    {
        $id = $model->id;
        $this->handleCategories($model);
        $this->handleTrends($model);
        $this->handleCarriers($model);
        $this->handleSuppliers($model);
        $this->handleAttributes($model);
        $this->handleRelations($model);
        $this->handleTags($model);
        $this->handleMetadata($model);
        $this->handlePriceRules($model);
        $this->handleQty($model);
        usleep(10);
        \ProductHelper::buildProductCache($model->id);
    }

    function _after_update($model)
    {
        DB::beginTransaction();
        parent::_after_update($model);
        $id = $model->id;
        $this->handleCategories($model);
        $this->handleTrends($model);
        $this->handleCarriers($model);
        $this->handleSuppliers($model);
        $this->handleAttributes($model);
        $this->handleRelations($model);
        $this->handleTags($model);
        //$this->handleMetadata($model); this is called by buildProductCache
        $this->handlePriceRules($model);
        $this->handleQty($model);
        usleep(10);
        global $beforeModel;
        //\ProductHelper::handleRevisions($beforeModel,$model);
        \ProductHelper::buildProductCache($model->id);
        DB::commit();
    }

    private function handleQty($model)
    {
        if ($model->hasCombinations()) {
            $model->updateQuantity(1);
            return;
        }
        $prevQty = isset($this->prevQty) ? $this->prevQty : 0;
        $qty = $model->qty;
        if ($prevQty != $qty) {
            if ($prevQty > $qty) {
                $action = 'decrease';
            } else {
                $action = 'increase';
            }
            $qty = abs($prevQty - $qty);
            $params = [
                'product_id' => $model->id,
                'product_reference' => $model->sku,
                'product_name' => $model->it->name,
                'qty' => $qty,
            ];
            StockMvt::register($action, $params);
        }
    }

    private function handleCategories($model)
    {
        $id = $model->id;
        $category_ids = \Input::get("category_ids");

        if ($category_ids == "")
            return;

        $ids = explode(",", $category_ids);
        DB::table("categories_products")->where("product_id", $id)->delete();
        $cnt = 0;
        foreach ($ids as $cat_id) {
            DB::table("categories_products")->insert(array("category_id" => $cat_id, "product_id" => $id));
            $cnt++;
        }
        $model->update(["cnt_categories" => $cnt]);
    }

    private function handleTrends($model)
    {
        $id = $model->id;
        $trends = \Input::get("trends", []);

        DB::table("trends_products")->where("product_id", $id)->delete();
        if (empty($trends))
            return;

        $cnt = 0;
        foreach ($trends as $trend_id) {
            DB::table("trends_products")->insert(array("trend_id" => $trend_id, "product_id" => $id));
            $cnt++;
        }
        $model->update(["cnt_trends" => $cnt]);
    }


    private function handlePriceRules($model)
    {

        $id = $model->id;
        // delete all specific prices
        \ProductPrice::where("product_id", $id)->where("price_rule_id", ">", 0)->delete();

        $obj = \Product::find($id);
        $obj->setAllIds();
        $rules = $obj->getResolvablePriceRules();

        \Utils::log($rules, "-------------- RESOLVABLE RULES --------------");

        if (count($rules) > 0) {
            foreach ($rules as $rule_id) {
                $PriceRule = \PriceRule::find($rule_id);
                $PriceRule->applyRuleForProduct($id);
            }
        }

    }


    private function handleCarriers($model)
    {
        $id = $model->id;
        $carriers = \Input::get("carrier_ids", []);

        DB::table("products_carriers")->where("product_id", $id)->delete();
        if (empty($carriers))
            return;

        $cnt = 0;
        foreach ($carriers as $carrier_id) {
            DB::table("products_carriers")->insert(array("carrier_id" => $carrier_id, "product_id" => $id));
            $cnt++;
        }
    }

    private function handleSuppliers($model)
    {
        return;
        $id = $model->id;
        $suppliers = \Input::get("supplier_ids", []);
        $suppliers_data = \Input::get("suppliers", []);

        DB::table("products_suppliers")->where("product_id", $id)->delete();
        if (empty($suppliers))
            return;

        $cnt = 0;
        foreach ($suppliers as $supplier_id) {
            $data = array("supplier_id" => $supplier_id, "product_id" => $id, "currency_id" => \Core::getDefaultCurrency()->id);
            if (count($suppliers_data)) {
                $data["supplier_price_nt"] = (float)$suppliers_data[$supplier_id]["supplier_price_nt"];
                $data["currency_id"] = (int)$suppliers_data[$supplier_id]["currency_id"];
            }
            DB::table("products_suppliers")->insert($data);
            $cnt++;
        }
    }

    private function handleAttributes($model)
    {
        $id = $model->id;
        $attribute_ids = \Input::get("attribute_ids");
        $attribute_ids = trim($attribute_ids, ',');
        $attribute_ids = trim($attribute_ids);

        DB::table("products_attributes")->where("product_id", $id)->delete();
        DB::table("products_attributes_position")->where("product_id", $id)->delete();
        if ($attribute_ids == "")
            return;
        $rows = DB::select("SELECT * FROM attributes WHERE id IN($attribute_ids) ORDER BY FIELD(id, $attribute_ids)");
        $cnt = 0;
        $languages = \Mainframe::languagesCodes();
        foreach ($rows as $row) {
            $cnt++;
            $attribute_id = $row->id;

            if ($row->is_unique == 0) { //multi-lang
                foreach ($languages as $lang_id) {
                    $value = \Input::get("attr_" . $row->code . "_" . $lang_id);
                    if (is_array($value)) {
                        foreach ($value as $v) {
                            DB::table("products_attributes")->insert(array("lang_id" => $lang_id, "product_id" => $id, "attribute_id" => $attribute_id, "attribute_val" => $v));
                        }
                    } else {
                        DB::table("products_attributes")->insert(array("lang_id" => $lang_id, "product_id" => $id, "attribute_id" => $attribute_id, "attribute_val" => $value));
                    }
                }
            } else { //single-lang
                $value = \Input::get("attr_" . $row->code);
                if (is_array($value)) {
                    foreach ($value as $v) {
                        DB::table("products_attributes")->insert(array("product_id" => $id, "attribute_id" => $attribute_id, "attribute_val" => $v));
                    }
                } else {
                    DB::table("products_attributes")->insert(array("product_id" => $id, "attribute_id" => $attribute_id, "attribute_val" => $value));
                }
            }

            DB::table("products_attributes_position")->insert(array("product_id" => $id, "attribute_id" => $attribute_id, "position" => $cnt));
        }
        $model->update(["cnt_attributes" => $cnt]);
    }

    private function handleRelations($model)
    {
        $id = $model->id;

        $relation = 'accessory';
        $accessories = \Input::get("accessories");
        $accessories = explode(",", $accessories);
        DB::table("products_relations")->where("relation", $relation)->where("master_id", $id)->delete();
        $cnt = 0;
        if (count($accessories) > 0 AND $accessories[0] != '') {

            foreach ($accessories as $slave_id) {
                $cnt++;
                DB::table("products_relations")->insert(array("relation" => $relation, "master_id" => $id, "slave_id" => $slave_id, "position" => $cnt));
            }
        }
        $model->update(["cnt_relation_accessories" => $cnt]);

        $relation = 'related';
        $accessories = \Input::get("related");
        $accessories = explode(",", $accessories);
        DB::table("products_relations")->where("relation", $relation)->where("master_id", $id)->delete();
        $cnt = 0;
        if (count($accessories) > 0 AND $accessories[0] != '') {

            foreach ($accessories as $slave_id) {
                $cnt++;
                DB::table("products_relations")->insert(array("relation" => $relation, "master_id" => $id, "slave_id" => $slave_id, "position" => $cnt));
            }
        }
        $model->update(["cnt_relation_products" => $cnt]);
    }

    private function handleTags($model)
    {
        return;
        $id = $model->id;
        $languages = \Mainframe::languagesCodes();
        DB::table("products_tags")->where("product_id", $id)->delete();
        foreach ($languages as $lang) {
            $source = trim($_POST["tags_" . $lang]);
            $words = explode(",", $source);
            if ($source != '' AND count($words) > 0) {
                foreach ($words as $tag) {
                    $tag = strtolower(trim($tag));
                    $existingObj = \Tag::whereTag($tag)->where("lang_id", $lang)->first();
                    if ($existingObj) {
                        $tag_id = $existingObj->id;
                    } else {
                        $obj = new Tag();
                        $obj->lang_id = $lang;
                        $obj->tag = $tag;
                        $obj->save();
                        $tag_id = $obj->id;
                    }
                    DB::table("products_tags")->insert(array("product_id" => $id, "tag_id" => $tag_id));
                }
            }
        }
    }

    private function handleMetadata($model)
    {
        return \ProductHelper::handleMetadata($model);
        $id = $model->id;

        $lang = \Core::getLang();

        $languages = \Mainframe::languagesCodes();

        $token = new Token("");
        $token->set("sku", $model->sku);
        $token->set("brand_id", $model->brand_id);
        $token->set("collection_id", $model->collection_id);
        $token->set("category_id", $model->main_category_id);
        $token->set("default_category_id", $model->default_category_id);

        $md = Metadata::getInstance();

        $query = "SELECT attribute_id FROM products_attributes_position WHERE product_id=$id ORDER BY position";
        $rows = DB::select($query);

        foreach ($languages as $lang) {
            $attributes = array();
            $token->setLang($lang);
            foreach ($rows as $row) {
                $ah = new AttributeHtml($row->attribute_id, $lang);
                $ah->setProductValue($id);
                $meta = $ah->getMeta();
                $token->rebind($meta->value);
                $meta->value = $token->render();
                $string = $meta->label . ": " . $meta->value;
                $md->index($string);
                $attributes[] = $meta;
            }

            $md->index($token->resolve('brand'));
            $md->index($token->resolve('collection'));
            $md->index($token->resolve('category'));
            $md->index($token->resolve('category_default'));

            $md->set("attributes", $attributes);
            $md->set("fields", $md->getLang($lang, "fields"));
            $md->update($model, $lang);
        }
    }

    function getOptions()
    {

        $q = \Input::get("q");
        $page_limit = \Input::get("page_limit");
        $page = \Input::get("page");
        $ids = \Input::get("ids");

        $lang = \Core::getLang();
        $start = ($page - 1) * $page_limit;

        $query = "SELECT P.id,P.sku,L.name FROM products P LEFT JOIN products_lang L ON P.id=L.product_id LEFT JOIN brands_lang B ON P.brand_id=B.brand_id LEFT JOIN collections_lang C ON P.collection_id=L.id WHERE L.lang_id='$lang' AND (P.sku LIKE '%$q%' OR L.name LIKE '%$q%') ORDER BY L.name LIMIT $start,$page_limit";
        $query_count = "SELECT COUNT(P.id) AS cnt FROM products P LEFT JOIN products_lang L ON P.id=L.product_id WHERE L.lang_id='$lang' AND (P.sku LIKE '%$q%' OR L.name LIKE '%$q%')";

        $query = "SELECT P.id,P.sku,L.name,B.name as brand,C.name as collection FROM products P
LEFT JOIN products_lang L ON P.id=L.product_id AND L.lang_id='$lang'
LEFT JOIN brands_lang B ON P.brand_id=B.brand_id AND B.lang_id='$lang'
LEFT JOIN collections_lang C ON P.collection_id=C.collection_id AND C.lang_id='$lang'
WHERE (P.sku LIKE '%$q%' OR L.name LIKE '%$q%' OR B.name LIKE '%$q%' OR C.name LIKE '%$q')
ORDER BY L.name
LIMIT $start,$page_limit";

        $query_count = "SELECT COUNT(P.id) AS cnt FROM products P
LEFT JOIN products_lang L ON P.id=L.product_id AND L.lang_id='$lang'
LEFT JOIN brands_lang B ON P.brand_id=B.brand_id AND B.lang_id='$lang'
LEFT JOIN collections_lang C ON P.collection_id=C.collection_id AND C.lang_id='$lang'
WHERE (P.sku LIKE '%$q%' OR L.name LIKE '%$q%' OR B.name LIKE '%$q%' OR C.name LIKE '%$q')";

        $total_obj = \DB::selectOne($query_count);
        $total = $total_obj->cnt;

        $rows = DB::select($query);

        $count = count($rows);
        for ($i = 0; $i < $count; $i++) {
            $rows[$i]->name = $rows[$i]->name . " ({$rows[$i]->sku})";
            if ($rows[$i]->brand != '') {
                $rows[$i]->name .= ", " . $rows[$i]->brand;
            }
            if ($rows[$i]->collection != '') {
                $rows[$i]->name .= "/" . $rows[$i]->collection;
            }
        }

        $success = true;
        $records = $rows;
        return Json::encode(compact("total", "success", "records"));
    }

    function getSingleoption()
    {

        $id = \Input::get("id");

        $lang = \Core::getLang();

        $query = "SELECT P.id,P.sku,L.name FROM products P LEFT JOIN products_lang L ON P.id=L.product_id WHERE L.lang_id='$lang' AND P.id=$id";

        $row = DB::selectOne($query);
        if ($row) {
            $row->name = $row->name . " ({$row->sku})";
        }

        $record = $row;
        return Json::encode(compact("record"));
    }


    function postGroupOperation()
    {
        $messages = \ProductHelper::performGroupActions();
        $success = true;
        $msg = "<ul><li>" . implode("</li><li>", $messages) . "</li></ul>";
        $error = '';
        return Json::encode(compact("msg", "success", "error"));
    }


    function postAddTemplate()
    {
        $html = \Helper::getAttributesTableByTemplate(\Input::get("template_id"));

        $data = array('success' => true, 'msg' => 'Set attributi caricato correttamente', 'html' => $html);
        return Json::encode($data);
    }


    public function getPreview($id)
    {
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $obj->rebindAttributes();
        $view = array('obj' => $obj);
        return Theme::scope("products.preview", $view)->content();
    }

    public function postSeo($id)
    {
        $data = \ProductHelper::generateSeo($id);

        $data = array('success' => true, 'data' => $data);
        return Json::encode($data);
    }

    public function getRecord($id)
    {
        $success = true;
        $model = $this->model;
        $lang = Core::getLang();
        $obj = $model::getObj($id, $lang);
        $combinations = ProductCombination::where('product_id', $id)->get();
        $combinations_array = [];
        foreach ($combinations as $c) {
            $combinations_array[] = $c->toArray();
        }
        $msg = '';
        if (!$obj) {
            $success = false;
            $msg = "Could not find record with id [$id]";
        } else {
            $obj->combinations = $combinations_array;
        }
        return Json::encode(compact('success', 'obj', 'msg'));
    }


    protected function checkRecords(&$messages, $obj)
    {
        parent::checkRecords($messages, $obj);
        if ($obj) {
            if ($obj->cnt_images == 0) {
                $messages[] = "questo prodotto non contiene immagini";
            }
            if ($obj->main_category_id == 0) {
                $messages[] = "questo prodotto non ha associata una <strong class='label label-important'>CATEGORIA PRINCIPALE</strong>";
            }
            if ($obj->default_category_id == 0) {
                $messages[] = "questo prodotto non ha associata una <strong class='label label-important'>CATEGORIA DI DEFAULT</strong>";
            }
            if ($obj->brand_id == 0) {
                $messages[] = "questo prodotto non ha associata una <strong class='label label-important'>MARCA</strong>";
            }
            if ($obj->collection_id == 0) {
                $messages[] = "questo prodotto non ha associata una <strong class='label'>COLLEZIONE</strong>";
            }
            if ($obj->qty <= 0) {
                $messages[] = "questo prodotto ha una giacenza attuale (quantità) di <strong class='label label-important'>$obj->qty</strong>";
            }
            if ($obj->outlet == 1) {
                $messages[] = "questo prodotto è flaggato come <strong class='label'>OUTLET</strong>";
            }
            if ($obj->is_out_of_production == 1) {
                $messages[] = "questo prodotto è flaggato come <strong class='label'>FUORI PRODUZIONE</strong>";
            }
        }
    }


    public function getStocks($id)
    {

        $model = $this->model;
        $obj = $model::find($id);
        $stocks = $obj->getSpecificStocksSet();
        $view = compact('obj', 'stocks');
        return Theme::scope("products.preview_stocks", $view)->content();
    }


    public function getSearchShops()
    {
        $this->page = 'products.search_shops';
        $this->pageheader = 'Ricerca Negoziando';
        $this->iconClass = 'font-search';
        $this->addBreadcrumb('Ricerca Negoziando');

        $sku = Input::get('sku');
        $sku = strtoupper(trim($sku));
        /** @var Connection $conn */
        $conn = Core::getNegoziandoConnection();

        $rows = ($sku != '') ? $conn->getAvailabilityBySku($sku, false) : [];
        foreach ($rows as $row) {
            $row->shop = MorellatoShop::getByCode($row->code);
        }
        $view = compact('sku', 'rows');
        return $this->render($view);
    }


    public function getElastic()
    {
        $this->page = 'products.elastic';
        $this->pageheader = 'Indice Elasticsearch';
        $this->iconClass = 'font-search';
        $this->addBreadcrumb('Indice Elasticsearch');

        $search = Input::get('search');
        $type = Input::get('type');
        $simulate_lang_id = Input::get('simulate_lang_id');
        $current_lang = Core::getLang();

        Core::setLang($simulate_lang_id);
        /** @var ElasticProductSearchRepository $repository */
        $repository = App::make(ElasticProductSearchRepository::class);
        try{
            if (strlen($search) > 0) {
                if($type == 'live')
                    $results = json_encode($repository->liveSearch($search)->getHits(), JSON_PRETTY_PRINT);

                if($type == 'catalog')
                    $results = json_encode($repository->liveCatalogSearch($search)->getHits(), JSON_PRETTY_PRINT);

                if($type == 'custom'){
                    $fields = explode(',', Input::get('fields'));
                    if($fields[0] == '')
                        throw new \Exception("La textarea con la definizione dei fields non deve essere vuota!");

                    $sort = explode(',', Input::get('sort'));
                    if($sort[0] == '')
                        throw new \Exception("La textarea con la definizione del sorting non deve essere vuota!");

                    $results = json_encode($repository->customSearch($search, $fields, $sort)->getHits(), JSON_PRETTY_PRINT);
                }

            } else {
                $results = json_encode([]);
            }
        }catch (Exception $e){
            $results = json_encode(['error' => $e->getMessage()]);
        }

        Core::setLang($current_lang);
        $view = compact('results');
        return $this->render($view);
    }


    protected function actions_default($params = [])
    {
        $actions = parent::actions_default($params);
        $export = ($this->action("getExport", TRUE));
        $actions[] = new AdminAction('Esporta', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');
        return $actions;
    }


    function getExport()
    {
        $this->page = 'products.export';
        $this->pageheader = 'Esportazione Catalogo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('export');
        $view = array();
        return $this->render($view);
    }


    function getDownload()
    {
        ini_set('max_execution_time', 6000);
        $export_mode = Input::get('export_mode', 'default');

        /*$from = Input::get('from', '');
        $to = Input::get('to', '');
        $brand_ids = Input::get('brand_ids', []);
        $is_outlet = Input::get('is_outlet', 0);
        $is_new = Input::get('is_new', 0);


        $query = \Product::orderBy('id', 'desc');
        if ($from != '') {
            $from = Format::sqlDate($from);
            $query->where('created_at', '>=', $from);
        }
        if ($to != '') {
            $to = Format::sqlDate($to);
            $query->where('created_at', '<=', $to);
        }
        if (count($brand_ids) > 0) {
            $query->whereIn('brand_id', $brand_ids);
        }
        if ($is_outlet == 1) {
            $query->whereIn('is_outlet', 1);
        }
        if ($is_new == 1) {
            $query->whereIn('is_new', 1);
        }

        $rows = $query->get();*/
        /** @var CsvProductsRepository $repository */
        $repository = app(CsvProductsRepository::class);
        $builder = $repository->build(Input::all());
        $file = $repository->export($builder);
        $filename = 'catalogo_default_' . time() . '.csv';

        return Response::download($file, $filename, ['content-type' => 'text/csv']);
    }


    private function export_default($rows)
    {
        $lines = ['id;sku;sapsku;fuoriproduzione;outlet;nuovo;brand;quantita;creato;modificato'];
        foreach ($rows as $row) {

            $brand = Brand::getObj($row->brand_id);

            $data = [];
            $data[] = $row->id;
            $data[] = $row->sku;
            $data[] = $row->sap_sku;
            $data[] = $row->is_out_of_production;
            $data[] = $row->is_outlet;
            $data[] = $row->is_new;
            $data[] = $brand ? $brand->name : null;
            $data[] = $row->qty;
            $data[] = $row->created_at;
            $data[] = $row->updated_at;

            $lines[] = implode(";", $data);

        }

        $file = storage_path() . '/temp.csv';
        $content = implode(PHP_EOL, $lines);
        $content = utf8_encode($content);

        \File::put($file, $content);

        $filename = "catalogo_default_" . time() . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/x-csv']);
    }


    function getSyncRep($id)
    {
        ini_set('max_execution_time', 600);
        $product = Product::where('id', $id)->select(['sku', 'sap_sku'])->first();
        $sku = \Utils::getSapSku($product->sku, $product->sap_sku);
        $env = \App::environment();
        $email = \AdminUser::get()->email;
        $artisan = base_path('artisan');
        $cmd =  "php $artisan csv:grabimages --mode=single --sku=$sku --email=$email --env=$env";
        audit($cmd, __METHOD__);
        execInBackground($cmd);
        /*$command = new services\Morellato\Commands\GrabImage();
        $grabber = \App::make('services\Morellato\Grabbers\ImageGrabber', [$command]);
        $output = $grabber->single($sku);*/
        //audit($output);
        $output = "Job avviato - non è possibile mostrarne l'avanzamento via Web;\nAl termine del processo una mail di report verrà inviata all'indirizzo $email";
        return '<pre style="background-color: #000; color:#fff; font-family: Courier; font-size: 12px;">' . $output . '</pre>';
    }
}
