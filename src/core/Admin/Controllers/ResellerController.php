<?php

use Input;

class ResellerController extends BackendController
{
    public $component_id = 85;
    public $title = 'Gestione Negozi Reseller';
    public $page = 'reseller.index';
    public $pageheader = 'Gestione Negozi Reseller';
    public $iconClass = 'font-columns';
    public $model = 'Reseller';
    
    protected $rules = array(
        'code' => 'required',
        'company_name' => 'required',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }
    
    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Negozi Reseller');
        $this->toFooter("js/echo/reseller.js?v=4");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/reseller.js");
        $this->page = 'reseller.trash';
        $this->pageheader = 'Cestino Negozi Reseller';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/reseller.js");
        $this->page = 'reseller.create';
        $this->pageheader = 'Nuovo Negozio Reseller';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/reseller.js");
        $this->page = 'reseller.create';
        $this->pageheader = 'Modifica Negozio Reseller';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }
    
    public function getTable()
    {
        \Utils::watch();
        $model = $this->model;

        $pages = $model::select('id', 'code', 'company_name', 'country_text', 'province', 'localization', 'region', 'address', 'mail', 'lat', 'lng', 'telephone');

        return \Datatables::of($pages)
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->make();
    }


    public function getTabletrash()
    {
        $model = $this->model;
        $pages = $model::onlyTrashed()->select('id', 'code', 'company_name', 'country_text', 'province', 'localization', 'region', 'address', 'mail', 'lat', 'lng', 'telephone');
        
        return \Datatables::of($pages)
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

}