<?php

use Core\Token;

class SeoTextsController extends MultiLangController
{

    public $component_id = 56;
    public $title = 'Gestione Testi SEO';
    public $page = 'seotexts.index';
    public $pageheader = 'Gestione Testi SEO';
    public $iconClass = 'font-columns';
    public $model = 'SeoText';
    protected $rules = array();
    protected $lang_rules = array();

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Testi SEO');
        $this->toFooter("js/echo/seotexts.js?v=2");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getPreview($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->fillLanguages();
        $view = array('obj' => $obj);
        return Theme::scope("seotexts.preview", $view)->content();
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/seotexts.js?v=2");
        $this->page = 'seotexts.trash';
        $this->pageheader = 'Cestino Testi SEO';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/seotexts.js?v=2");
        $this->page = 'seotexts.create';
        $this->pageheader = 'Nuovo Testo SEO';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/seotexts.js?v=2");
        $this->page = 'seotexts.create';
        $this->pageheader = 'Modifica Testo SEO';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('seo_texts_lang', 'seo_texts.id', '=', 'seo_texts_lang.seo_text_id')
            ->where('seo_texts_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', function ($join) use ($lang_id) {
                $join->on('brands_lang.brand_id', '=', 'seo_texts.brand_id')->on('brands_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('collections_lang', function ($join) use ($lang_id) {
                $join->on('collections_lang.collection_id', '=', 'seo_texts.collection_id')->on('collections_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('categories_lang', function ($join) use ($lang_id) {
                $join->on('categories_lang.category_id', '=', 'seo_texts.category_id')->on('categories_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('trends_lang', function ($join) use ($lang_id) {
                $join->on('trends_lang.trend_id', '=', 'seo_texts.trend_id')->on('trends_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('seo_texts_rules as R', 'seo_texts.id', '=', 'R.seo_text_id')
            ->groupBy("seo_texts.id")
            ->select('seo_texts.id', 'categories_lang.name as category', 'brands_lang.name as brand', 'collections_lang.name as collection', 'trends_lang.name as trend', DB::raw("count(R.id) as rules"), 'seo_texts_lang.content', 'seo_texts_lang.published', 'seo_texts.updated_at', 'seo_texts_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('rules', function ($data) {
                $cnt = \DB::table("seo_texts_rules")->where("seo_text_id", $data["id"])->count("id");
                $link = \URL::action($this->action("getPreview"), $data['id']);
                return "<span class=\"label label-info mr5\">{$cnt}</span> <a href=\"javascript:Echo.remoteModalLarge('$link');\">Anteprima</a>";
            })
            ->edit_column('content', function ($data) {
                return "<div style='max-height:100px; overflow:hidden;'>" . $data["content"] . "</div>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('image_default')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('seo_texts_lang', 'seo_texts.id', '=', 'seo_texts_lang.seo_text_id')
            ->where('seo_texts_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', function ($join) use ($lang_id) {
                $join->on('brands_lang.brand_id', '=', 'seo_texts.brand_id')->on('brands_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('collections_lang', function ($join) use ($lang_id) {
                $join->on('collections_lang.collection_id', '=', 'seo_texts.collection_id')->on('collections_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('categories_lang', function ($join) use ($lang_id) {
                $join->on('categories_lang.category_id', '=', 'seo_texts.category_id')->on('categories_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('trends_lang', function ($join) use ($lang_id) {
                $join->on('trends_lang.trend_id', '=', 'seo_texts.trend_id')->on('trends_lang.lang_id', "=", 'seo_texts_lang.lang_id');
            })
            ->leftJoin('seo_texts_rules as R', 'seo_texts.id', '=', 'R.seo_text_id')
            ->groupBy("seo_texts.id")
            ->select('seo_texts.id', 'categories_lang.name as category', 'brands_lang.name as brand', 'collections_lang.name as collection', 'trends_lang.name as trend', DB::raw("count(R.id) as rules"), 'seo_texts_lang.content', 'seo_texts.deleted_at', 'seo_texts.created_at', 'seo_texts_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('rules', function ($data) {
                $cnt = \DB::table("seo_texts_rules")->where("seo_text_id", $data["id"])->count("id");
                $link = \URL::action($this->action("getPreview"), $data['id']);
                return "<span class=\"label label-info mr5\">{$cnt}</span> <a href=\"javascript:Echo.remoteModalLarge('$link');\">Anteprima</a>";
            })
            ->edit_column('content', function ($data) {
                return "<div style='max-height:100px; overflow:hidden;'>" . $data["content"] . "</div>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('lang_id')
            ->remove_column('image_default')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $category_id = (int)\Input::get("category_id", 0);
        $brand_id = (int)\Input::get("brand_id", 0);
        $collection_id = (int)\Input::get("collection_id", 0);
        $trend_id = (int)\Input::get("trend_id", 0);
        $id = (int)\Input::get("id", 0);

        $rules = \DB::table('seo_texts_rules')->where('seo_text_id', $id)->count();

        if ($category_id == 0 AND $brand_id == 0 AND $collection_id == 0 AND $rules == 0 AND $trend_id == 0) {
            $this->errors[] = 'Devi specificare almeno un elemento tra Categoria, Brand, Collezione o Gruppo o regole';
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');


        \Input::replace($_POST);

    }

    public function postSeo($id)
    {
        $data = \Helper::generateSeo($id);

        $data = array('success' => true, 'data' => $data);
        return Json::encode($data);
    }

    function _prepare_data()
    {
        //\Utils::watch();
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');


        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            //now save the real data
            $token = new Token("");
            $token->setLang($lang);
            $token->set("brand_id", \Input::get("brand_id"));
            $token->set("collection_id", \Input::get("collection_id"));
            $token->set("trend_id", \Input::get("trend_id"));
            $token->set("category_id", $_POST['category_id']);
            $token->set("default_category_id", \Input::get("category_id"));

            $tokenizable = array("metatitle", "h1", "metakeywords", "metadescription", "metafacebook", "metatwitter", "metagoogle");

            foreach ($tokenizable as $field) {

                if (isset($_POST['mtt_' . $field])) {
                    $mtt_name = (int)$_POST['mtt_' . $field];
                    if ($mtt_name > 0) {
                        $mtt = \MagicTokenTemplate::find($mtt_name);
                        $token->rebind($mtt->{$lang}->content);
                        $_POST[$field . "_" . $lang] = trim($token->render());
                    }
                }
            }
        }

        \Input::replace($_POST);

    }

    function postAddRule()
    {
        $data = \Input::all();

        $success = false;

        \Utils::watch();

        if ($data['target_mode'] == '*') {
            DB::table('seo_texts_rules')->insert([
                'seo_text_id' => $data['id'],
                'target_type' => $data['target_type'],
                'target_mode' => $data['target_mode'],
                'position' => 9999,
            ]);
            $success = true;
            $message = 'Regola salvata con successo';
        } else {
            $ids = \Input::get('condition_' . $data['target_type'], []);
            $many = count($ids);
            if ($many > 0) {
                DB::table('seo_texts_rules')
                    ->where('seo_text_id', $data['id'])
                    ->update(['position' => DB::raw('position+' . $many)]);
                $counter = 0;
                foreach ($ids as $id) {
                    DB::table('seo_texts_rules')->insert([
                        'seo_text_id' => $data['id'],
                        'target_type' => $data['target_type'],
                        'target_mode' => $data['target_mode'],
                        'target_id' => $id,
                        'position' => $counter,
                    ]);
                    $counter++;
                }
                $success = true;
                $message = 'Regola/e salvate con successo';
            } else {
                $message = 'Devi selezionare almeno un elemento per il campo Condizione';
            }
        }

        $data = array('success' => $success, 'message' => $message);

        \Utils::unwatch();

        return Json::encode($data);
    }

    function postRulesTable()
    {
        $id = \Input::get('id');

        $data = array('success' => true, 'html' => \Helper::getSeoTextsRulesTable($id));

        return Json::encode($data);
    }

    function postRemoveRule($id)
    {
        DB::table('seo_texts_rules')->where('id', $id)->delete();
        $data = array('success' => true, 'msg' => 'Record eliminato con successo');
        return Json::encode($data);
    }

}
