<?php

namespace Echo1\Admin;

use AdminUser,
    AdminTpl,
    Input,
    Notification,
    Redirect,
    Sentry,
    Str,
    Utils,
    Validator;

class SettingsController extends \BackendController
{

    public $component_id = 5;
    public $title = 'Impostazioni';
    public $page = 'settings.default';


    private function fillCheckboxes()
    {
        $checkboxes = \Cfg::getBooleanKeys();
        foreach ($checkboxes as $c) {
            $v = 0;
            if(isset($_POST[$c])){
                if((int)$_POST[$c] === 1){
                    $v = 1;
                }else{
                    $v = 0;
                }
            }
            $_POST[$c] = $v;
        }
    }

    public function showGeneral()
    {
        $group = 'general';
        $pageheader = 'Generale';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showMail()
    {
        $group = 'mail';
        $pageheader = 'Configurazione Email';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showGeo()
    {
        $group = 'geo';
        $pageheader = 'Configurazione Nazioni e Geolocalizzazione';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showSeo()
    {
        $group = 'seo';
        $pageheader = 'Configurazione SEO e Indicizzazione';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showSocial()
    {
        $group = 'social';
        $pageheader = 'Configurazione Social Network';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showRss()
    {
        $group = 'rss';
        $pageheader = 'Configurazione Sitemap e Feed RSS';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showUnits()
    {
        $group = 'units';
        $pageheader = 'Configurazione Valute, Tasse e Unità di misura';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showImage()
    {
        $group = 'image';
        $pageheader = 'Configurazione Immagini';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showCatalog()
    {
        $group = 'catalog';
        $pageheader = 'Configurazione Catalogo prodotti';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showOrder()
    {
        $group = 'order';
        $pageheader = 'Configurazione Ordini, Fatture e Documenti di trasporto';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showMobile()
    {
        $group = 'mobile';
        $pageheader = 'Configurazione sito Mobile';
        return $this->showPanelByGroup($group, $pageheader);
    }

    public function showFeatures()
    {
        $group = 'features';
        $pageheader = 'Configurazione vendors & 3rd parties';
        return $this->showPanelByGroup($group, $pageheader);
    }

    protected function toolbar($what = 'default')
    {
        global $actions;

        $actions = array(
            new \AdminAction('Salva', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutte le Impostazioni correnti'),
        );

        if($what === 'features'){
            $user = Sentry::getUser();
            if (!$user->hasPermission('feats')) {
                $actions = [];
            }
        }
    }

    private function showPanelByGroup($group, $pageheader = false)
    {
        $this->group = $group;
        if ($pageheader) {
            $this->addBreadcrumb($pageheader);
            $this->toolbar($group);
            $this->pageheader = $this->title . ' - ' . $pageheader;
            $this->title = $this->title . ' - ' . $pageheader;
        }

        $data['group'] = $group;

        $this->theme->partialComposer('settings.' . $group, function ($view) {
            $view->obj = \Cfg::getGroup($this->group);
        });

        $this->toFooter('js/echo/settings.js');

        return $this->render($data);
    }


    public function save()
    {

        \Utils::watch();

        //Flash current values to session
        \Input::flashExcept("_lang_fields[]", "_lang_fields");

        $this->fillCheckboxes();

        $group = Input::get("group");

        $keys = \Cfg::getGroupKeys($group);

        $data = \Input::except(['task', 'group', 'token', '_token']);

        foreach ($keys as $key) {
            if (isset($_POST[$key])) {
                //$data[$key] = Input::get($key);
                $data[$key] = $_POST[$key];
            }
        }

        \Cfg::storeGroup($group, $data);

        \Input::flush();

        Notification::success("Configurazione aggiornata con successo");

        return Redirect::route('settings.' . $group);

    }

    public function getSettings()
    {
        $user = Sentry::getUser();
        return $this->render($user);
    }

    public function saveSettings()
    {
        Redirect::to("profile.settings");
    }

    public function testEmail(){
        $success = true;
        $email = \Input::get('email',null);
        $msg = "Messaggio inviato correttamente a $email";
        try{
            if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
                throw new \Exception('Indirizzo email non valido');
            }
            \Mail::send('emails.ping',['date' => \Format::now()], function ($message) use ($email) {
                $message->from( \Cfg::get('MAIL_GENERAL_ADDRESS'), 'Blade Framework');
                $message->to($email)->subject('Blade Framework Email test');
            });
        }catch(\Exception $e){
            $msg = $e->getMessage();
            $success = false;
        }
        return \Json::encode(compact('success','msg'));
    }

}