<?php

class ImagesSetController extends BackendController {

    public $component_id = 52;
    public $title = 'Gestione Set Immagini e Video';
    public $page = 'images_sets.index';
    public $pageheader = 'Gestione Set Immagini e Video';
    public $iconClass = 'font-columns';
    public $model = 'ImageSet';
    protected $rules = array(
        'uname' => 'required|unique:images_sets,uname',
    );
    protected $friendly_names = array(
        'uname' => 'Nome del Set',
    );   

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/images_sets.js");
    }

    public function getIndex() {
        $this->addBreadcrumb("Elenco Set Immagini e Video");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->page = 'images_sets.trash';
        $this->pageheader = 'Cestino Set Immagini e Video';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->page = 'images_sets.create';
        $this->pageheader = 'Nuovo Set Immagini e Video';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->page = 'images_sets.create';
        $this->pageheader = 'Modifica Set Immagini e Video';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }
    
    public function getPreview($id) {        
        $model = $this->model;
        $obj = $model::find($id);        
        $view = array('obj' => $obj);
        return Theme::scope("images_sets.preview",$view)->content();
    }

    public function getTable() {

        $model = $this->model;

        $pages = DB::table(DB::raw('images_sets S1'))                                
                ->leftJoin(DB::raw('images_groups G'), 'S1.id', '=', 'G.set_id')                
                ->where('S1.deleted_at', null)
                ->groupBy("S1.id")
                ->select(['S1.id', 'S1.uname', DB::raw('count(G.id) as images'), 'S1.created_at', 'S1.published', 'S1.position']);

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('uname', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['uname']}</a>";
                        })                        
                        ->edit_column('images', function($data) {                            
                            $link = \URL::action($this->action("getPreview"), $data['id']);
                            return "<span class=\"label label-info mr5\">{$data['images']}</span> <a href=\"javascript:Echo.remoteModal('$link');\">Anteprima</a>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->edit_column('published', function($data) {
                            return ($data['published'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Sospeso</span>';
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {

        $model = $this->model;

        $pages = DB::table(DB::raw('images_sets S1'))
                ->leftJoin(DB::raw('images_groups G'), 'S1.id', '=', 'G.set_id')               
                ->whereNotNull('S1.deleted_at')
                ->groupBy("S1.id")
                ->select(['S1.id', 'S1.uname', DB::raw('count(G.id) as images') , 'S1.created_at', 'S1.published', 'S1.deleted_at']);

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('images', function($data) {                                                        
                            return "<span class=\"label label-info\">{$data['images']}</span>";
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::datetime($data['deleted_at']);
                        })
                        ->edit_column('uname', function($data) {
                            return "<strong>{$data['uname']}</strong>";
                        })
                        ->edit_column('published', function($data) {
                            return ($data['published'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Sospeso</span>';
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['uname'] = 'required|unique:images_sets,uname,' . $model->id;
        $this->_prepare();
    }

    function _after_create($model)
    {
        $this->handleCache($model);
    }

    function _after_update($model)
    {
        $this->handleCache($model);
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }


    function postAddRule() {
        $data = \Input::all();

        $success = false;

        \Utils::watch();

        if ($data['target_mode'] == '*') {
            DB::table('images_sets_rules')->insert([
                'image_set_id' => $data['id'],
                'target_type' => $data['target_type'],
                'target_mode' => $data['target_mode'],
                'position' => 9999,
            ]);
            $success = true;
            $message = 'Regola salvata con successo';
        } else {
            $ids = \Input::get('condition_' . $data['target_type'], []);
            $many = count($ids);
            if ($many > 0) {
                DB::table('images_sets_rules')
                        ->where('image_set_id', $data['id'])
                        ->update(['position' => DB::raw('position+' . $many)]);
                $counter = 0;
                foreach ($ids as $id) {
                    DB::table('images_sets_rules')->insert([
                        'image_set_id' => $data['id'],
                        'target_type' => $data['target_type'],
                        'target_mode' => $data['target_mode'],
                        'target_id' => $id,
                        'position' => $counter,
                    ]);
                    $counter++;
                }
                $success = true;
                $message = 'Regola/e salvate con successo';
            } else {
                $message = 'Devi selezionare almeno un elemento per il campo Condizione';
            }
        }

        $data = array('success' => $success, 'message' => $message);

        \Utils::unwatch();

        return Json::encode($data);
    }

    function postRulesTable() {
        $id = \Input::get('id');

        $data = array('success' => true, 'html' => \Helper::getImagesSetRulesTable($id));

        return Json::encode($data);
    }

    function postRemoveRule($id) {
        DB::table('images_sets_rules')->where('id', $id)->delete();
        $data = array('success' => true, 'msg' => 'Record eliminato con successo');
        return Json::encode($data);
    }


    function postAddImage($set_id){
        $upload_filelist = \Input::get("files");
        $savePath = public_path() . "/assets/groups/";
        $sourcePath = storage_path() . "/uploads/";
        if (!File::exists($savePath)) {
            File::makeDirectory($savePath, 0775);
        }

        $languages = \Mainframe::languagesCodes();


        $rows = GroupImage::where("set_id", $set_id)->get();
        $totalImages = count($rows);

        $files = explode(",", $upload_filelist);
        $position = $totalImages;
        \Utils::watch();
        foreach ($files as $file) {

            if (File::exists($sourcePath . $file)):

                $position++;
                $extension = File::extension($sourcePath . $file);


                $ds = array(
                    "set_id" => $set_id,
                    "filename" => $file,
                    "position" => $position
                );

                foreach ($languages as $lang) {
                    $ds["legend_" . $lang] = '';
                }

                $image = new GroupImage();
                $image->setDataSource($ds);
                $image->save();

                if($image->id > 0){
                    $newFilename = $image->id.".".$extension;
                    File::move($sourcePath . $file, $savePath . $newFilename);

                    chmod($savePath . $newFilename, 0775);
                    DB::table('images_groups')->where('id',$image->id)->update(['filename' => $newFilename]);
                }

            endif;
        }
        /* \Utils::watch();
          DB::table("products")->where("id",$product_id)->update(["cnt_images" => $position]); */

        $result = array('success' => true);
        return Json::encode($result);
    }


    private function handleCache($model){
        ProductHelper::generateImagesSetsCacheByGroup($model);
    }

}
