<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class TagsController extends BackendController {

    public $component_id = 24;
    public $title = 'Gestione Tag Prodotti';
    public $page = 'tags.index';
    public $pageheader = 'Gestione Tag Prodotti';
    public $iconClass = 'font-columns';
    public $model = 'Tag';
    protected $rules = array(
        'tag' => 'required',
        'lang_id' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Tag Prodotti');
        $this->toFooter("js/echo/tags.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/tags.js");
        $this->page = 'tags.create';
        $this->pageheader = 'Nuovo Tag';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/tags.js");
        $this->page = 'tags.create';
        $this->pageheader = 'Modifica Tag';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::leftJoin("products_tags as P", "tags.id", "=", "P.tag_id")->groupBy("tags.id")->select('id', 'lang_id', 'tag', DB::raw("COUNT(product_id) as products"));

        return \Datatables::of($pages)
                        ->edit_column('tag', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['tag']}</a></strong>";
                        })
                        ->edit_column('lang_id', function($data) {
                            $obj = \Language::find($data["lang_id"]);
                            return "<strong>{$obj->name}</strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->_prepare();
    }
    
    function _after_create($model) {
        parent::_after_create($model);
        $this->handleProducts($model);
    }
    
    function _after_update($model) {
        parent::_after_update($model);
        $this->handleProducts($model);
    }
    
    
    
    
    private function handleProducts($model){
        $tag_id = $model->id;
        
        $products = \Input::get("products",[]);
        \DB::table("products_tags")->where("tag_id",$tag_id)->delete();
        
        foreach($products as $product_id){
            DB::table("products_tags")->insert(["tag_id" => $tag_id, "product_id" => $product_id]);
        }
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }


    }
    
    protected function column_actions($data) {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">    
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }
    
    protected function toolbar($what = 'default') {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = \URL::action($this->action("getIndex"));
                
                $actions = array(
                    new AdminAction('Aggiorna', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );                
                
                break;

            default:                
                $create = ($this->action("getCreate", TRUE));                
                $deleteMulti = ($this->action("postDeleteMulti", TRUE));                
                $actions = array(
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),                    
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),                    
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }

}
