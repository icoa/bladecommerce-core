<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-lug-2013 15.57.17
 */

use Carbon\Carbon;
use UglifyPHP\JS;
use UglifyPHP\CSS;


class Writer
{

    static function nl($text, $class = '')
    {
        if (is_array($text) OR is_object($text)) {
            $text = "<pre>" . print_r($text, 1) . "</pre>";
        }
        echo "<span class='$class'>$text</span><br />";
    }

}

class UtilsController extends BaseController
{

    private $excel = true;

    function getTable()
    {
        $table = Input::get("table");
        $query = 'SHOW COLUMNS FROM ' . $table;
        $column_name = 'Field';
        foreach (DB::select($query) as $column) {
            $columns[] = $column->$column_name;
        }


        Writer::nl("db_fields = array(");
        foreach ($columns as $column) {
            Writer::nl('"' . $column . '",');
        }
        Writer::nl(");");
    }

    function getBuild()
    {
        $this->_buildAttributesCache();
    }

    function getCompress()
    {
        $files = array();
        $files[] = 'js/plugins/imagepicker/mootools-core-1.3.1.js';
        $files[] = 'js/plugins/imagepicker/mootools-more-1.3.1.1.js';
        $files[] = 'js/plugins/imagepicker/Source/FileManager.js';
        $files[] = 'js/plugins/imagepicker/Assets/js/milkbox/milkbox.js';
        $files[] = 'js/plugins/imagepicker/Source/Uploader/Fx.ProgressBar.js';
        $files[] = 'js/plugins/imagepicker/Source/Uploader/Swiff.Uploader.js';
        $files[] = 'js/plugins/imagepicker/Source/Uploader.js';
        $files[] = 'js/plugins/imagepicker/Language/Language.it.js';

        $files[] = 'js/jquery.1.10.min.js';
        $files[] = 'js/jquery-migrate-1.2.1.min.js';
        //$files[] = 'js/jquery_ui_custom.js';
        $files[] = 'js/jquery-ui-1.10.3.custom.min.js';

        $files[] = 'js/echo/jquery.noConflict.js';
        $files[] = 'js/echo/jquery.json.js';
        $files[] = 'js/echo/jquery.rest.js';
        $files[] = 'js/jquery.tmpl.min.js';
        $files[] = 'js/plugins/charts/excanvas.min.js';

        $files[] = 'js/plugins/cookie/jquery.cookie.js';
        $files[] = 'js/plugins/charts/jquery.flot.js';
        $files[] = 'js/plugins/charts/jquery.flot.resize.js';
        $files[] = 'js/plugins/charts/jquery.sparkline.min.js';


        $files[] = 'js/plugins/forms/jquery.tagsinput.min.js';
        //$files[] = 'js/plugins/forms/jquery.inputlimiter.min.js');
        //$files[] = 'js/plugins/forms/jquery.maskedinput.min.js');
        //$files[] = 'js/plugins/forms/jquery.autosize.js');
        $files[] = 'js/plugins/forms/jquery.ibutton.js';
        //$files[] = 'js/plugins/forms/jquery.dualListBox.js');
        //$files[] = 'js/plugins/forms/jquery.validate.js');
        $files[] = 'js/plugins/forms/jquery.uniform.min.js';
        $files[] = 'js/plugins/forms/jquery.select2.min.js';
        //$files[] = 'js/plugins/forms/select2_locale_it.js');
        //$files[] = 'js/plugins/forms/jquery.cleditor.js');

        $files[] = 'js/plugins/uploader/jquery.upload.js';
        $files[] = 'js/plugins/uploader/plupload.js';
        $files[] = 'js/plugins/uploader/plupload.html4.js';
        $files[] = 'js/plugins/uploader/plupload.html5.js';
        $files[] = 'js/plugins/uploader/jquery.plupload.queue.js';

        //$files[] = 'js/plugins/wizard/jquery.form.wizard.js');
        //$files[] = 'js/plugins/wizard/jquery.form.js');

        $files[] = 'js/plugins/ui/jquery.collapsible.min.js';
        $files[] = 'js/plugins/ui/jquery.timepicker.min.js';
        $files[] = 'js/plugins/ui/jquery.ui.datepicker-it.js';
        $files[] = 'js/plugins/ui/jquery.jgrowl.min.js';
        //$files[] = 'js/plugins/ui/jquery.pie.chart.js');
        //$files[] = 'js/plugins/ui/jquery.fullcalendar.min.js');
        $files[] = 'js/plugins/ui/jquery.elfinder.js';
        $files[] = 'js/plugins/ui/jquery.fancybox.js';

        $files[] = 'js/plugins/datetimepicker/jquery-ui-timepicker-addon.js';
        $files[] = 'js/plugins/datetimepicker/jquery-ui-sliderAccess.js';
        $files[] = 'js/plugins/datetimepicker/i18n/jquery-ui-timepicker-it.js';

        $files[] = 'js/plugins/validation-engine/jquery.validationEngine-it.js';
        $files[] = 'js/plugins/validation-engine/jquery.validationEngine.min.js';


        $files[] = 'js/plugins/tables/jquery.dataTables.min.js';

        $files[] = 'js/plugins/bootstrap/bootstrap.min.js';
        $files[] = 'js/plugins/bootstrap/bootstrap-bootbox.min.js';
        $files[] = 'js/plugins/bootstrap/bootstrap-progressbar.js';
        $files[] = 'js/plugins/bootstrap/bootstrap-colorpicker.js';


        //$files[] = 'js/plugins/jquery.sticky-kit.min.js');
        $files[] = 'js/plugins/jquery.sticky.js';
        $files[] = 'js/plugins/jquery.tablednd.js';
        $files[] = 'js/plugins/underscore-min.js';
        $files[] = 'js/plugins/jquery-scrollto.js';


        // FANCY TREE

        $files[] = "js/plugins/fancytree/jquery.fancytree.js";
        //$this->toQueue("js/plugins/fancytree/jquery.fancytree.persist.js");
        $files[] = "js/plugins/fancytree/jquery.fancytree.filter.js";


        // DATATABLES FILTERS
        //$this->toHead("js/plugins/tables/jquery.dataTables.yadcf.css");
        //$this->toQueue("js/plugins/tables/jquery.dataTables.yadcf.js");
        //$this->toFooter("js/plugins/tables/jquery.dataTables.yadcf.js");
        $files[] = "js/plugins/tables/jquery.dataTables.columnFilter.js";

        //$files[] = 'ckeditor/ckeditor.js';


        $root = "C:/wamp/www/l4/public/themes/admin/assets/";
        $minFile = $root . "js/min.1.1.js";
        $cmd = "uglifyjs";
        foreach ($files as $f) {
            $cmd .= " " . $root . $f;
        }
        $cmd .= " -o $minFile";
        echo '<pre>';
        echo $cmd;
        echo '</pre>';

        $files = array();
        $files[] = 'js/functions/custom.js';
        $files[] = 'js/echo/mainframe.js';
        $root = "C:/wamp/www/l4/public/themes/admin/assets/";
        $minFile = $root . "js/blade.1.2.js";
        $cmd = "uglifyjs";
        foreach ($files as $f) {
            $cmd .= " " . $root . $f;
        }
        $cmd .= " -o $minFile";
        echo '<pre>';
        echo $cmd;
        echo '</pre>';


        $css = array();
        $rewrite = array();
        $rewrite[] = "js/plugins/fancytree/skin-lion/ui.fancytree.css";
        $rewrite[] = "js/plugins/tables/jquery.dataTables.yadcf.css";

        $css[] = "css/font.css";
        $css[] = "css/plugins.css";
        $css[] = "css/bootstrap.css";
        $css[] = "css/elfinder.css";
        $css[] = "css/fancybox.css";
        $css[] = "css/ui_custom.css";
        $css[] = "css/main.css";

        $rewrite[] = "js/plugins/imagepicker/Assets/js/milkbox/css/milkbox.css";
        $rewrite[] = "js/plugins/imagepicker/Assets/Css/FileManager.css";
        $rewrite[] = "js/plugins/imagepicker/Assets/Css/Additions.css";
        $rewrite[] = "js/plugins/datetimepicker/jquery-ui-timepicker-addon.css";
        $rewrite[] = "js/plugins/validation-engine/css/validationEngine.jquery.css";
        $css[] = "css/echo.css";

        $docRoot = $root;
        $tempDir = "C:/Temp/";
        foreach ($rewrite as $f) {
            $cssFile = $root . $f;
            $info = pathinfo($cssFile);
            $currentDir = $info["dirname"];
            $style = Casset_Cssurirewriter::rewrite(file_get_contents($cssFile), $currentDir, $docRoot);
            $style = str_replace("/js/", "../js/", $style);
            //echo $style;
            file_put_contents($tempDir . $info["basename"], $style);
            $css[] = $tempDir . $info["basename"];
            echo '<pre>';
            //echo Casset_Cssurirewriter::$debugText;
            echo '</pre>';
        }

        $minFile = $root . "css/min.1.0.css";
        $cmd = "uglifycss";
        foreach ($css as $f) {
            if (substr($f, 0, 2) == "C:") {
                $cmd .= " " . $f;
            } else {
                $cmd .= " " . $root . $f;
            }

        }
        $cmd .= " > $minFile";
        echo '<pre>';
        echo $cmd;
        echo '</pre>';
    }


    public function getMinify()
    {
        $cmd = "uglifyjs " . public_path('themes/frontend/assets/js/polyfill/src.menu.js') . " -o " . public_path('themes/frontend/assets/js/polyfill/menu.js');
        echo '<pre>';
        echo $cmd;
        echo '</pre>';

        $cmd = "uglifyjs " . public_path('themes/admin/assets/js/echo/GoogleAnalyticActionLib.js') . " -o " . public_path('themes/admin/assets/js/echo/GoogleAnalyticActionLib.min.js');
        echo '<pre>';
        echo $cmd;
        echo '</pre>';
    }

    protected function less($asset)
    {
        $path = \Theme::path();
        $assetTarget = str_replace(['css', '.less'], ['cache', '.css'], $asset);
        $file = $path . "/assets/" . $asset;
        if (file_exists($file)) {
            $less = new lessc;
            $target = $path . "/assets/" . $assetTarget;
            try {
                $less->checkedCompile($file, $target);
                return $assetTarget;
            } catch (\Exception $e) {
                \Utils::error($e->getMessage());
                \Utils::error($e->getTraceAsString());
            }
        }
    }


    public function getTemplate($folder = 'frontend')
    {

        Theme::theme($folder)->layout('default');

        $useCleanCSS = false;

        $version = Theme::getConfig('assetVersioning');
        $assets = Theme::getConfig('assets');

        $files = array();
        $rows = $assets['footer']();
        foreach ($rows as $asset) {
            $files[] = $asset;
        }

        $root = public_path(\Theme::path() . '/assets/');
        $minFile = $root . "js/blade_$version.js";

        $cmd = "uglifyjs2";
        foreach ($files as $f) {
            $cmd .= " " . $root . $f;
        }
        //GA FILE
        $cmd .= " " . public_path("themes/admin/assets/js/echo/GoogleAnalyticActionLib.min.js");
        $cmd .= " -o $minFile";
        echo '<pre>';
        echo $cmd . PHP_EOL;
        echo '</pre>';

        $css = array();
        $rows = $assets['head']();
        $rewrite = $assets['rewrite']();
        foreach ($rows as $asset) {
            if (substr($asset, -4) == 'less') {
                $asset = $this->less($asset);
            }
            if (in_array($asset, $rewrite)) {
                $css[] = $this->_rewrite($asset, $root);
            } else {
                $css[] = $asset;
            }
        }


        $minFile = $root . "css/blade_$version.css";

        if ($useCleanCSS) {
            $cmd = "type";

            foreach ($css as $f) {
                if (substr($f, 0, 2) == "C:") {
                    $cmd .= " " . $f;
                } else {
                    $cmd .= " " . $root . $f;
                }

            }
            $cmd .= " | cleancss -o $minFile";
            $cmd = str_replace("/", "\\", $cmd);
            echo '<pre>';
            echo $cmd . PHP_EOL;
            echo '</pre>';
        } else {
            $cmd = "uglifycss";
            foreach ($css as $f) {
                if (substr($f, 0, 2) == "C:") {
                    $cmd .= " " . $f;
                } else {
                    $cmd .= " " . $root . $f;
                }

            }
            $cmd .= " > $minFile";
            echo '<pre>';
            echo $cmd . PHP_EOL;
            echo '</pre>';
        }


        $minFile = $root . "css/ie1_blade_$version.css";

        if ($useCleanCSS) {
            $cmd = "type";
            foreach ($css as $f) {
                if ($f == 'js/plugins/revslider/css/captions-original.css') {
                    $cmd .= " | cleancss -o $minFile";
                    $cmd = str_replace("/", "\\", $cmd);
                    echo '<pre>';
                    echo $cmd;
                    echo '</pre>';
                    $cmd = "type";
                    $minFile = $root . "css/ie2_blade_$version.css";
                }
                if ($f == 'css/topnav2.css') {
                    $cmd .= " | cleancss -o $minFile";
                    $cmd = str_replace("/", "\\", $cmd);
                    echo '<pre>';
                    echo $cmd;
                    echo '</pre>';
                    $cmd = "type";
                    $minFile = $root . "css/ie3_blade_$version.css";
                }
                if (substr($f, 0, 2) == "C:") {
                    $cmd .= " " . $f;
                } else {
                    $cmd .= " " . $root . $f;
                }
            }
            $cmd .= " | cleancss -o $minFile";
            $cmd = str_replace("/", "\\", $cmd);
            echo '<pre>';
            echo $cmd;
            echo '</pre>';
        } else {
            $cmd = "uglifycss";
            foreach ($css as $f) {
                if ($f == 'js/plugins/revslider/css/captions-original.css') {
                    $cmd .= " > $minFile";
                    echo '<pre>';
                    echo $cmd;
                    echo '</pre>';
                    $cmd = "uglifycss";
                    $minFile = $root . "css/ie2_blade_$version.css";
                }
                if ($f == 'css/topnav2.css') {
                    $cmd .= " > $minFile";
                    echo '<pre>';
                    echo $cmd;
                    echo '</pre>';
                    $cmd = "uglifycss";
                    $minFile = $root . "css/ie3_blade_$version.css";
                }
                if (substr($f, 0, 2) == "C:") {
                    $cmd .= " " . $f;
                } else {
                    $cmd .= " " . $root . $f;
                }
            }
            $cmd .= " > $minFile";
            echo '<pre>';
            echo $cmd;
            echo '</pre>';
        }


    }


    private function compress($file)
    {
        $buffer = file_get_contents($file);
        /* remove this type of comments */
        $buffer = preg_replace("/(\/\*[\w\'\s\r\n\*\+\,\"\-\.]*\*\/)/", "", $buffer);
        //remove tabs, spaces, newlines, etc.
        $buffer = str_replace(array("\rn", "\r", "\n", "\t",
            '  ', '    ', '    '), '', $buffer);
        file_put_contents($file, $buffer);
    }

    function getOptimize($folder = 'frontend')
    {
        Theme::theme($folder)->layout('default');
        $version = Theme::getConfig('assetVersioning');
        $root = public_path(\Theme::path() . '/assets/');

        $minFile = $root . "css/blade_$version.css";
        $this->compress($minFile);
        $minFile_opt = $root . "css/blade_$version.min.css";
        $css = array(
            $minFile => $minFile_opt
        );
        minifyCSS($css);
        $minFile = $root . "js/blade_$version.js";
        //$this->compress($minFile);
        $minFile_opt = $root . "js/blade_$version.min.js";
        $js = array(
            $minFile => $minFile_opt
        );
        minifyJS($js);
    }


    function getPlugins()
    {
        $root = "C:/wamp/www/l4/public/themes/frontend/assets/";
        $version = FrontendController::ASSET_VERSIONING;
        $css = array();


        $css[] = "js/plugins/sod/selectordie.css";

        $css[] = $this->_rewrite("js/plugins/revslider/css/settings.css", $root);
        $css[] = $this->_rewrite("js/plugins/revslider/css/captions-original.css", $root);
        $css[] = $this->_rewrite("js/plugins/revslider/css/ks-settings.css", $root);

        $css[] = "js/plugins/owlcarousel/owl.carousel.css";
        $css[] = "js/plugins/owlcarousel/owl.theme.css";
        $css[] = "js/plugins/cs/jquery.mCustomScrollbar.css";
        $css[] = "js/plugins/tooltipster/css/tooltipster.css";
        $css[] = "js/plugins/tooltipster/css/themes/tooltipster-noir.css";
        $css[] = "js/plugins/mpopup/magnific-popup.css";

        $css[] = $this->_rewrite("js/plugins/ion/css/ion.rangeSlider.css", $root);
        $css[] = $this->_rewrite("js/plugins/ion/css/ion.rangeSlider.skinModern.css", $root);

        $css[] = "js/plugins/toastr/toastr.min.css";
        $css[] = $this->_rewrite("js/plugins/uniform/default/css/uniform.default.min.css", $root);
        $css[] = "js/plugins/dialog/css/bootstrap-dialog.min.css";


        $minFile = $root . "css/plugins_$version.css";
        $cmd = "uglifycss";
        foreach ($css as $f) {
            if (substr($f, 0, 2) == "C:") {
                $cmd .= " " . $f;
            } else {
                $cmd .= " " . $root . $f;
            }

        }
        $cmd .= " > $minFile";
        echo '<pre>';
        echo $cmd;
        echo '</pre>';
    }

    private function _rewrite($f, $root)
    {
        $docRoot = $root;
        $tempDir = "C:/Temp/";

        $cssFile = $root . $f;
        $info = pathinfo($cssFile);
        $currentDir = $info["dirname"];
        $style = Casset_Cssurirewriter::rewrite(file_get_contents($cssFile), $currentDir, $docRoot);
        $style = str_replace(["/js/", "url('/", 'url("/'], ["../js/", "url('../", 'url("../'], $style);
        //echo $style;
        file_put_contents($tempDir . $info["basename"], $style);
        return $tempDir . $info["basename"];

    }

    private function _buildAttributesCache()
    {
        $rows = DB::table('attributes_sets')->get();

        foreach ($rows as $row) {
            Writer::nl("Building cache for $row->uname");
            $obj = $this->_getAttributeSet($row->id);
            echo '<pre>';
            print_r($obj);
            echo '</pre>';

            DB::table('attributes_sets_content_cache')->where('attribute_set_id', $obj->id)->delete();
            $this->_insertAttributesCache($obj, $obj->id);
        }
    }

    private function _insertAttributesCache($obj, $target_id, &$counter = 0)
    {
        Writer::nl("_insertAttributesCache for $obj->uname");


        $parent = $obj->parent;
        if ($parent != false) {
            $this->_insertAttributesCache($parent, $target_id, $counter);
        }

        $rules = DB::table('attributes_sets_content')->where('attribute_set_id', $obj->id)->orderBy('position')->get();

        foreach ($rules as $rule) {
            Writer::nl("Rule: attribute_set_id $rule->attribute_set_id | attribute_id $rule->attribute_id | $rule->rule");
            if ($rule->rule == '+') {
                $counter++;
                DB::table('attributes_sets_content_cache')->insert([
                    'attribute_set_id' => $target_id,
                    'attribute_id' => $rule->attribute_id,
                    'position' => $counter,
                ]);
            }

            if ($rule->rule == '-') {
                DB::table('attributes_sets_content_cache')
                    ->where('attribute_set_id', $target_id)
                    ->where('attribute_id', $rule->attribute_id)
                    ->delete();
            }
        }
    }

    private function _getAttributeSet($id)
    {
        $obj = DB::table('attributes_sets')->find($id);
        $obj->parent = false;
        if ($obj->parent_id > 0) {
            $parent = $this->_getAttributeSet($obj->parent_id);
            $obj->parent = $parent;
        }
        return $obj;
    }

    function getIndex()
    {
        $rows = DB::table(DB::raw('attributes_sets_content_cache C'))
            ->leftJoin(DB::raw('attributes_sets S'), "C.attribute_set_id", '=', "S.id")
            ->leftJoin(DB::raw('attributes_lang L'), "C.attribute_id", '=', "L.attribute_id")
            ->where('lang_id', 'it')
            ->orderBy('attribute_set_id')
            ->orderBy('C.position')
            ->select(['C.*', 'S.uname', 'L.*'])
            ->get();

        $table_row = '';

        foreach ($rows as $row) {
            $table_row .= <<<ROW
<tr>
    <td>$row->id</td>
    <td>$row->uname</td>
    <td>$row->name</td>
    <td>$row->position</td>
</tr>
ROW;
        }


        $html = <<<TABLE
<table>
<tr>
<th>ID</th>
<th>NAME</th>
<th>ATTRIBUTE</th>
<th>POSITION</th>
</tr>
$table_row
</table>
TABLE;
        echo $html;
    }

    function getExportWatches()
    {
        $this->_export_products(1, true);
    }

    function getExportJewels()
    {
        $this->_export_products(4, true);
    }

    function getExportHome()
    {
        $this->_export_products(26, true);
    }

    function _export_products($main_cat_id = 1, $translated = true)
    {

        $this->_excel();

        $root = "C:/wamp/www/l4/sources/export/";

        //$query = "SELECT DISTINCT(attribute_id),C.position,A.frontend_input,A.`code` FROM attributes_sets_content_cache C LEFT JOIN attributes A ON C.attribute_id=A.id WHERE attribute_set_id IN (SELECT attribute_set_id FROM attributes_sets_rules WHERE target_type='CAT' AND (target_id=? OR target_id IN (SELECT id FROM categories WHERE parent_id=? ) ) ) AND A.id IN (SELECT DISTINCT(attribute_id) FROM attributes_lang WHERE published=1 AND lang_id='it') GROUP BY attribute_id ORDER BY C.position";


        $query = "SELECT P.*, L.* FROM products P
        LEFT JOIN products_lang L ON P.id=L.product_id
        WHERE lang_id='it' AND
        P.id IN (SELECT product_id FROM categories_products WHERE category_id=? OR category_id IN (SELECT id FROM categories WHERE parent_id=?))
        AND old_id IS null AND created_at>='2014-09-30'
        AND deleted_at IS null
        ORDER BY id ASC";
        $products = DB::select($query, [$main_cat_id, $main_cat_id]);

        $products_ids = [];
        foreach ($products as $p) {
            $products_ids[] = $p->id;
        }

        $query = "SELECT A.*,L.* FROM attributes A LEFT JOIN attributes_lang L ON A.id=L.attribute_id INNER JOIN products_attributes_position P ON P.attribute_id=A.id WHERE lang_id='it' AND published=1 AND product_id IN (" . implode(",", $products_ids) . ") GROUP BY A.id ORDER BY P.position";
        $attributes = DB::select($query);

        /*$query = "SELECT A.*,L.* FROM attributes_options A LEFT JOIN attributes_options_lang L ON A.id=L.attribute_option_idid WHERE lang_id='it' AND published=1 AND A.attribute_id IN (SELECT attribute_id FROM products_attributes_position WHERE product_id IN (".implode(",",$products_ids)."))";
        $options_ids = DB::select($query);*/

        $query = "SELECT B.*,L.* FROM brands B LEFT JOIN brands_lang L ON B.id=L.brand_id WHERE lang_id='it'";
        $brands = DB::select($query);

        $query = "SELECT C.*,L.* FROM collections C LEFT JOIN collections_lang L ON C.id=L.collection_id WHERE lang_id='it'";
        $collections = DB::select($query);

        $_ATTRIBUTES_CACHE = \EchoArray::set($attributes)->keyIndex('id')->get();
        $_BRANDS_CACHE = \EchoArray::set($brands)->keyIndex('id')->get();
        $_COLLECTIONS_CACHE = \EchoArray::set($collections)->keyIndex('id')->get();


        $headers = [];
        $table_rows = '';

        $headers[] = ($translated) ? 'Categoria 1 (*)' : 'cat_id1 (*)';
        $headers[] = ($translated) ? 'Categoria 2 (*)' : 'cat_id2 (*)';
        $headers[] = ($translated) ? 'Categoria 3 (*)' : 'cat_id3 (*)';
        $headers[] = ($translated) ? 'Categoria 4 (*)' : 'cat_id4 (*)';
        $headers[] = ($translated) ? 'Brand (*)' : 'brand_id (*)';
        $headers[] = ($translated) ? 'Collezione (*)' : 'collection_id (*)';
        $headers[] = ($translated) ? 'Codice prodotto' : 'sku';
        $headers[] = ($translated) ? 'Codice prodotto genitore' : 'parent_id';
        $headers[] = ($translated) ? 'EAN13' : 'ean13';
        $headers[] = ($translated) ? 'UPC' : 'upc';
        $headers[] = ($translated) ? 'Nome (IT)' : 'name_it';
        $headers[] = ($translated) ? 'Nome (EN)' : 'name_en';
        $headers[] = ($translated) ? 'Descrizione (IT)' : 'desc_it';
        $headers[] = ($translated) ? 'Descrizione (EN)' : 'desc_en';
        $headers[] = ($translated) ? 'Pubblicato (IT)' : 'published_it';
        $headers[] = ($translated) ? 'Pubblicato (EN)' : 'published_en';
        $headers[] = ($translated) ? 'Peso' : 'weight';
        $headers[] = ($translated) ? 'Prezzo di acquisto tasse escl' : 'buy_price';
        $headers[] = ($translated) ? 'Prezzo di listino tasse incl' : 'sell_price_wt';
        $headers[] = ($translated) ? 'Prezzo di vendita tasse escl' : 'sell_price';
        $headers[] = ($translated) ? 'Prezzo finale tasse incl' : 'price';
        $headers[] = ($translated) ? 'IVA %' : 'tax_group_id (*)';
        $headers[] = ($translated) ? 'Quantità' : 'qty';
        $headers[] = ($translated) ? 'Novità [B]' : 'is_new [B]';
        $headers[] = ($translated) ? 'Novità (da)' : 'new_from_date';
        $headers[] = ($translated) ? 'Novità (a)' : 'new_to_date';
        $headers[] = ($translated) ? 'Disponibile [B]' : 'is_available [B]';
        $headers[] = ($translated) ? 'Disponibilità - giorni' : 'availability_days';
        $headers[] = ($translated) ? 'Outlet [B]' : 'is_outlet [B]';
        $headers[] = ($translated) ? 'Fuori produzione [B]' : 'is_out_of_production [B]';
        $headers[] = ($translated) ? 'Promozione [B]' : 'is_in_promotion [B]';
        $headers[] = ($translated) ? 'Condizione (*)' : 'condition (*)';
        $headers[] = ($translated) ? 'Posizione' : 'position';
        $headers[] = ($translated) ? 'SEO - Titolo pagina (IT)' : 'metatitle';
        $headers[] = ($translated) ? 'SEO - Titolo pagina (EN)' : 'metatitle';
        $headers[] = ($translated) ? 'SEO - H1 (IT)' : 'h1';
        $headers[] = ($translated) ? 'SEO - H1 (EN)' : 'h1';
        $headers[] = ($translated) ? 'SEO - Keywords (IT)' : 'metakeywords';
        $headers[] = ($translated) ? 'SEO - Keywords (EN)' : 'metakeywords';
        $headers[] = ($translated) ? 'SEO - Description (IT)' : 'metadescription';
        $headers[] = ($translated) ? 'SEO - Description (EN)' : 'metadescription';
        $headers[] = ($translated) ? 'VIDEO - Youtube' : 'youtube';
        $headers[] = ($translated) ? 'VIDEO - Custom' : 'video';
        $headers[] = ($translated) ? 'Data creazione' : 'created_at';
        $headers[] = ($translated) ? 'Ult. modifica' : 'updated_at';
        $headers[] = ($translated) ? 'Backend' : 'link';

        foreach ($attributes as $row) {
            $obj = $_ATTRIBUTES_CACHE[$row->id];
            //Writer::nl("$obj->name ($obj->code)");
            $label = ($translated) ? $obj->name : $obj->code;

            if ($obj->frontend_input == 'select') {
                $label .= " (*)";
            }

            if ($obj->frontend_input == 'multiselect') {
                $label .= " (*)[M]";
            }

            if ($obj->frontend_input == 'boolean') {
                $label .= " [B]";
            }

            $headers[] = $label;
        }

        \Utils::watch();
        $i = 0;
        foreach ($products as $p) {

            $folder = $root . trim(strtoupper($p->sku));
            if ($p->sku == '') {
                $folder = $root . trim(strtoupper($p->id));
            }

            if (!is_dir($folder)) {
                mkdir($folder);
            }


            $tuple = [];

            $query = "SELECT category_id FROM categories_products WHERE product_id=?";
            $categories = DB::select($query, [$p->id]);

            $cat1 = '';
            $cat2 = '';
            $cat3 = '';
            $cat4 = '';
            $brand = '';
            $collection = '';

            if (count($categories) > 0) {
                $category_obj = $this->_getCategoryObj($categories[0]->category_id);
                $cat1 = ($translated) ? $category_obj->name : $category_obj->id;
                if ($category_obj->parent) {
                    $cat2 = ($translated) ? $category_obj->parent->name : $category_obj->parent->id;
                }
            }

            if (count($categories) == 2) {
                $category_obj = $this->_getCategoryObj($categories[1]->category_id);
                $cat3 = ($translated) ? $category_obj->name : $category_obj->id;
                if ($category_obj->parent) {
                    $cat4 = ($translated) ? $category_obj->parent->name : $category_obj->parent->id;
                }
            }

            if ($p->brand_id > 0) {
                $brand = ($translated) ? $_BRANDS_CACHE[$p->brand_id]->name : $p->brand_id;
            }

            if ($p->collection_id > 0) {
                $collection = ($translated) ? $_COLLECTIONS_CACHE[$p->collection_id]->name : $p->collection_id;
            }


            $query = "SELECT * FROM products_lang WHERE lang_id='en' AND product_id = ?";
            $translation = DB::selectOne($query, [$p->id]);

            $name_it = $p->name;
            $desc_it = $this->_strip($p->ldesc);
            $published_it = $p->published;
            $metatitle_it = $p->metatitle;
            $h1_it = ($p->h1 == '') ? $p->metatitle : $p->h1;
            $metakeywords_it = $p->metakeywords;
            $metadescription_it = $p->metadescription;
            $attributes_it = $p->attributes;


            $published_en = 0;
            $name_en = '';
            $desc_en = '';
            $metatitle_en = '';
            $h1_en = '';
            $metakeywords_en = '';
            $metadescription_en = '';

            if ($translation) {
                $name_en = $translation->name;
                $desc_en = $this->_strip($translation->ldesc);
                $published_en = $translation->published;
                $metatitle_en = $translation->metatitle;
                $h1_en = ($translation->h1 == '') ? $translation->metatitle : $translation->h1;
                $metakeywords_en = $translation->metakeywords;
                $metadescription_en = $translation->metadescription;
                $attributes_en = $translation->attributes;
            }

            $condition = 'default';
            switch ($p->condition) {
                case 'N':
                    $condition = 'new';
                    break;
                case 'U':
                    $condition = 'used';
                    break;
                case 'R':
                    $condition = 'refurbished';
                    break;
            }

            $parent = '';
            if ($p->parent_id > 0) {
                $temp = \Product::find($p->parent_id);
                $parent = $temp->sku;
            }


            if (!$translated)
                $condition = $p->condition;

            $tuple[] = $cat1;
            $tuple[] = $cat2;
            $tuple[] = $cat3;
            $tuple[] = $cat4;
            $tuple[] = $brand;
            $tuple[] = $collection;
            $tuple[] = $p->sku;
            $tuple[] = $parent;
            $tuple[] = $p->ean13;
            $tuple[] = $p->upc;
            $tuple[] = $name_it;
            $tuple[] = $name_en;
            $tuple[] = $desc_it;
            $tuple[] = $desc_en;
            $tuple[] = $published_it;
            $tuple[] = $published_en;
            $tuple[] = $p->weight;
            $tuple[] = $p->buy_price;
            $tuple[] = $p->sell_price_wt;
            $tuple[] = $p->sell_price;
            $tuple[] = $p->price;
            $tuple[] = ($translated) ? '22' : $p->tax_group_id;
            $tuple[] = $p->qty;
            $tuple[] = $p->is_new;
            $tuple[] = $p->new_from_date;
            $tuple[] = $p->new_to_date;
            $tuple[] = $p->is_available;
            $tuple[] = $p->availability_days;
            $tuple[] = $p->is_outlet;
            $tuple[] = $p->is_out_of_production;
            $tuple[] = $p->is_in_promotion;
            $tuple[] = $condition;
            $tuple[] = $p->position;
            $tuple[] = $metatitle_it;
            $tuple[] = $metatitle_en;
            $tuple[] = $h1_it;
            $tuple[] = $h1_en;
            $tuple[] = $metakeywords_it;
            $tuple[] = $metakeywords_en;
            $tuple[] = $metadescription_it;
            $tuple[] = $metadescription_en;


            $tuple[] = $p->youtube;
            $tuple[] = $p->video;
            $tuple[] = $p->created_at;
            $tuple[] = $p->updated_at;
            $tuple[] = "https://bo.kronoshop.collaudo.biz/admin/products/edit/" . $p->id;

            $file_attribute_it = $folder . "/attributes_it.txt";
            $file_attribute_en = $folder . "/attributes_en.txt";

            //attributes files
            \File::put($file_attribute_it, $attributes_it);
            \File::put($file_attribute_en, $attributes_en);


            //images
            $images = \ProductImage::where("product_id", $p->id)->orderBy('position')->get();
            foreach ($images as $img) {
                $filename = $img->filename;
                $tokens = explode(".", $filename);
                $ext = $tokens[1];
                $url = "c:/wamp/www/l4/public/assets/products/" . $img->filename;
                $new_file = $folder . "/" . $p->sku . "_" . $img->position . "." . $ext;
                try {
                    //\File::put($new_file, file_get_contents($url));
                    copy($url, $new_file);
                    \Utils::log($new_file, "IMAGE SAVED");
                } catch (\Exception $e) {
                    \Utils::log($url, "ERROR SAVING IMAGE");
                    \Utils::log($new_file, "DESTINATION WAS");
                }
            }

            $matrix = $this->_getProductAttributesMatrix($p->id, $_ATTRIBUTES_CACHE);

            foreach ($attributes as $row) {
                if (array_key_exists($row->id, $matrix)) {
                    $obj = $matrix[$row->id];
                    $obj->value = str_replace('[BRAND]', $brand, $obj->value);
                    $tuple[] = $obj->value;
                } else {
                    $tuple[] = ($row->frontend_input == 'boolean') ? 0 : '';
                }
            }

            $odd = ($i % 2 == 0) ? "style='background-color:#FFFFCC;'" : "style='background-color:#DDFFC6;'";

            $table_rows .= "<tr $odd><td align='center'>" . implode("</td><td align='center'>", $tuple) . "</td></tr>";

            $i++;
        }

        $table_headers = "<tr style='background-color:#DEDEDE;'><th>" . implode("</th><th>", $headers) . "</th></tr>";

        $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
        $table_headers
        $table_rows
</table>
TABLE;

        echo $this->_toCharset($table);
    }

    function ___getProductAttributesMatrix($id, $translated)
    {
        $query = "SELECT DISTINCT(PA.attribute_id),PA.attribute_val,O.uname,A.frontend_input,O.id AS option_id FROM products_attributes PA LEFT JOIN attributes_options O ON PA.attribute_id=O.attribute_id AND PA.attribute_val=O.uname LEFT JOIN attributes A ON PA.attribute_id=A.id WHERE PA.product_id=?";
        $rows = DB::select($query, [$id]);
        $data = [];
        foreach ($rows as $row) {
            $row->value = $row->attribute_val;
            if (array_key_exists($row->attribute_id, $data)) {
                $temp = $data[$row->attribute_id];
                // \Utils::log($temp,"TEMP IS");
                $row->value = ($translated) ? ($temp->value . "," . $row->value) : ($temp->value . "," . $row->value);
            }
            if ($translated):
                if ($row->option_id > 0) {
                    $query = "SELECT name FROM attributes_options_lang WHERE lang_id='it' AND attribute_option_id=?";
                    $obj = DB::selectOne($query, [$row->option_id]);
                    $row->value = ($obj->name == '') ? $row->attribute_val : $obj->name;
                }
            endif;

            // \Utils::log($row,"ROW IS");
            $data[$row->attribute_id] = $row;
        }
        return $data;
    }

    function _getProductAttributesMatrix($id, $_ATTRIBUTES_CACHE)
    {
        $query = "SELECT * FROM products_attributes WHERE product_id=?";
        $rows = DB::select($query, [$id]);
        $data = [];
        foreach ($rows as $row) {
            if ($row->attribute_val) {
                $attribute = $_ATTRIBUTES_CACHE[$row->attribute_id];
                switch ($attribute->frontend_input) {
                    case 'select':
                    case 'multiselect':
                        $query = "SELECT O.*,L.* FROM attributes_options O LEFT JOIN attributes_options_lang L ON O.id=L.attribute_option_id WHERE lang_id='it' AND id=$row->attribute_val";
                        $option = \DB::selectOne($query);
                        $row->value = ($option->name != '') ? $option->name : $option->uname;
                        if ($attribute->is_boolean) {
                            $row->value .= ": Sì";
                        }
                        if (isset($data[$row->attribute_id])) {
                            $temp = $data[$row->attribute_id];
                            $temp->value .= "<br>" . $row->value;
                            $row = $temp;
                        }
                        break;

                    case 'boolean':
                        $row->value = ($row->attribute_val == 1) ? 'SI' : 'NO';
                        break;

                    default:
                        $row->value = $row->attribute_val;
                        break;
                }

                //Utils::log($attribute);
                if ($attribute->txt_prefix != '') {
                    $row->value = $attribute->txt_prefix . " " . $row->value;
                }
                if ($attribute->txt_suffix != '') {
                    $row->value = $row->value . " " . $attribute->txt_suffix;
                }

                $data[$row->attribute_id] = $row;
            }


        }
        return $data;
    }

    function _getCategoryObj($id)
    {
        $query = "SELECT C.*,L.* FROM categories C LEFT JOIN categories_lang L ON C.id=L.category_id WHERE lang_id='it' AND C.id = ?";
        $obj = DB::selectOne($query, [$id]);
        $obj->parent = FALSE;
        if ($obj->parent_id > 0) {
            $obj->parent = $this->_getCategoryObj($obj->parent_id);
        }
        return $obj;
    }


    function getExportAll()
    {
        /*$query = "SELECT * FROM brands_lang";
        $rows = DB::select($query);
        $queries = [];
        foreach($rows as $row){
            $data = [];
            if($row->image_default != ''){
                $data['image_default'] = $row->image_default;
            }
            if($row->image_thumb != ''){
                $data['image_thumb'] = $row->image_thumb;
            }
            $query = "UPDATE brands_lang SET ";
            if(count($data) > 0){
                foreach($data as $key => $value){
                    $query .= "$key='$value', ";
                }
                $query = rtrim($query,", ");
                $query .= " WHERE brand_id=$row->brand_id AND lang_id='$row->lang_id'";
                $queries[] = $query;
            }

        }
        \File::put("C:/wamp/www/l4/sources/reimport/brands.sql", implode(";\n",$queries));*/

        $queries = [];
        $query = "SELECT * FROM images WHERE id>15354";
        $rows = DB::select($query);
        $root = "C:/wamp/www/l4/public/assets/products/";
        foreach ($rows as $row) {
            $file = $row->filename;
            $tokens = explode(".", $file);
            $extension = $tokens[1];
            $new_file = \Str::lower($row->id . '.' . $extension);
            $queries[] = "UPDATE images SET filename='$new_file' WHERE id=$row->id";
            \File::move($root . $file, $root . $new_file);
        }
        \File::put("C:/wamp/www/l4/sources/reimport/images.sql", implode(";\n", $queries));
    }


    function getExportCategories($translated = true)
    {

        $this->_excel();

        $query = "SELECT * FROM categories ORDER BY parent_id,position";
        $categories = DB::select($query);

        $headers = [];
        $table_rows = '';

        $headers[] = ($translated) ? 'ID' : 'id';
        $headers[] = ($translated) ? 'Categoria padre' : 'parent_id';
        $headers[] = ($translated) ? 'Nome (IT)' : 'name_it';
        $headers[] = ($translated) ? 'Nome (EN)' : 'name_en';
        $headers[] = ($translated) ? 'Descrizione breve (IT)' : 'sdesc_it';
        $headers[] = ($translated) ? 'Descrizione breve (EN)' : 'sdesc_en';
        $headers[] = ($translated) ? 'Descrizione estesa (IT)' : 'ldesc_it';
        $headers[] = ($translated) ? 'Descrizione estesa (EN)' : 'ldesc_en';
        $headers[] = ($translated) ? 'Pubblicato (IT)' : 'published_it';
        $headers[] = ($translated) ? 'Pubblicato (EN)' : 'published_en';
        $headers[] = ($translated) ? 'Posizione' : 'position';

        $i = 0;
        foreach ($categories as $cat) {

            $tuple = [];

            $query = "SELECT * FROM categories_lang WHERE lang_id=? AND category_id=?";
            $obj_it = DB::selectOne($query, ['it', $cat->id]);

            $query = "SELECT * FROM categories_lang WHERE lang_id=? AND category_id=?";
            $obj_en = DB::selectOne($query, ['en', $cat->id]);

            $parent = 'Principale';
            if ($cat->parent_id > 0) {
                $t = $this->_getCategoryObj($cat->parent_id);
                $parent = $t->name;
            }

            $tuple[] = $cat->id;
            $tuple[] = ($translated) ? $parent : $cat->parent_id;
            $tuple[] = $obj_it->name;
            $tuple[] = $obj_en->name;
            $tuple[] = $this->_strip($obj_it->sdesc);
            $tuple[] = $this->_strip($obj_en->sdesc);
            $tuple[] = $this->_strip($obj_it->ldesc);
            $tuple[] = $this->_strip($obj_en->ldesc);
            $tuple[] = $obj_it->published;
            $tuple[] = $obj_en->published;
            $tuple[] = $cat->position;

            $odd = ($i % 2 == 0) ? "style='background-color:#FFFFCC;'" : "style='background-color:#DDFFC6;'";

            $table_rows .= "<tr $odd><td align='center'>" . implode("</td><td align='center'>", $tuple) . "</td></tr>";

            $i++;
        }

        $table_headers = "<tr style='background-color:#DEDEDE;'><th>" . implode("</th><th>", $headers) . "</th></tr>";

        $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
        $table_headers
        $table_rows
</table>
TABLE;

        echo $this->_toCharset($table);
    }

    function getExportBrands($translated = false)
    {

        $this->_excel();

        $query = "SELECT * FROM brands WHERE ISNULL(deleted_at) ORDER BY position";
        $categories = DB::select($query);

        $headers = [];
        $table_rows = '';

        $headers[] = ($translated) ? 'ID' : 'id';
        $headers[] = ($translated) ? 'Nome (IT)' : 'name_it';
        $headers[] = ($translated) ? 'Nome (EN)' : 'name_en';
        $headers[] = ($translated) ? 'Descrizione breve (IT)' : 'sdesc_it';
        $headers[] = ($translated) ? 'Descrizione breve (EN)' : 'sdesc_en';
        $headers[] = ($translated) ? 'Descrizione estesa (IT)' : 'ldesc_it';
        $headers[] = ($translated) ? 'Descrizione estesa (EN)' : 'ldesc_en';
        $headers[] = ($translated) ? 'Pubblicato (IT)' : 'published_it';
        $headers[] = ($translated) ? 'Pubblicato (EN)' : 'published_en';
        $headers[] = ($translated) ? 'Posizione' : 'position';

        $i = 0;
        foreach ($categories as $cat) {

            $tuple = [];

            $query = "SELECT * FROM brands_lang WHERE lang_id=? AND brand_id=?";
            $obj_it = DB::selectOne($query, ['it', $cat->id]);

            $query = "SELECT * FROM brands_lang WHERE lang_id=? AND brand_id=?";
            $obj_en = DB::selectOne($query, ['en', $cat->id]);


            $tuple[] = $cat->id;

            $tuple[] = $obj_it->name;
            $tuple[] = $obj_en->name;
            $tuple[] = $this->_strip($obj_it->sdesc);
            $tuple[] = $this->_strip($obj_en->sdesc);
            $tuple[] = $this->_strip($obj_it->ldesc);
            $tuple[] = $this->_strip($obj_en->ldesc);
            $tuple[] = $obj_it->published;
            $tuple[] = $obj_en->published;
            $tuple[] = $cat->position;

            $odd = ($i % 2 == 0) ? "style='background-color:#FFFFCC;'" : "style='background-color:#DDFFC6;'";

            $table_rows .= "<tr $odd><td align='center'>" . implode("</td><td align='center'>", $tuple) . "</td></tr>";

            $i++;
        }

        $table_headers = "<tr style='background-color:#DEDEDE;'><th>" . implode("</th><th>", $headers) . "</th></tr>";

        $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
        $table_headers
        $table_rows
</table>
TABLE;

        echo $this->_toCharset($table);
    }

    function getExportCollections($translated = false)
    {

        $this->_excel();

        $query = "SELECT * FROM collections WHERE ISNULL(deleted_at) ORDER BY position";
        $collections = DB::select($query);

        $query = "SELECT B.*,L.* FROM brands B LEFT JOIN brands_lang L ON B.id=L.brand_id WHERE lang_id='it'";
        $brands = DB::select($query);

        $query = "SELECT C.*,L.* FROM categories C LEFT JOIN categories_lang L ON C.id=L.category_id WHERE lang_id='it'";
        $categories = DB::select($query);

        $_BRANDS_CACHE = \EchoArray::set($brands)->keyIndex('id')->get();
        $_CATEGORIES_CACHE = \EchoArray::set($categories)->keyIndex('id')->get();

        $headers = [];
        $table_rows = '';

        $headers[] = ($translated) ? 'ID' : 'id';
        $headers[] = ($translated) ? 'Brand' : 'brand_id';
        $headers[] = ($translated) ? 'Categoria' : 'category_id';
        $headers[] = ($translated) ? 'Nome (IT)' : 'name_it';
        $headers[] = ($translated) ? 'Nome (EN)' : 'name_en';
        $headers[] = ($translated) ? 'Descrizione breve (IT)' : 'sdesc_it';
        $headers[] = ($translated) ? 'Descrizione breve (EN)' : 'sdesc_en';
        $headers[] = ($translated) ? 'Descrizione estesa (IT)' : 'ldesc_it';
        $headers[] = ($translated) ? 'Descrizione estesa (EN)' : 'ldesc_en';
        $headers[] = ($translated) ? 'Pubblicato (IT)' : 'published_it';
        $headers[] = ($translated) ? 'Pubblicato (EN)' : 'published_en';
        $headers[] = ($translated) ? 'Posizione' : 'position';

        $i = 0;
        foreach ($collections as $cat) {

            $tuple = [];

            $query = "SELECT * FROM collections_lang WHERE lang_id=? AND collection_id=?";
            $obj_it = DB::selectOne($query, ['it', $cat->id]);

            $query = "SELECT * FROM collections_lang WHERE lang_id=? AND collection_id=?";
            $obj_en = DB::selectOne($query, ['en', $cat->id]);

            if ($cat->category_id > 0) {
                $related_cat = ($translated) ? $_CATEGORIES_CACHE[$cat->category_id]->name : $cat->category_id;
            } else {
                $related_cat = '-';
            }


            $tuple[] = $cat->id;
            $tuple[] = ($translated) ? $_BRANDS_CACHE[$cat->brand_id]->name : $cat->brand_id;
            $tuple[] = $related_cat;
            $tuple[] = $obj_it->name;
            $tuple[] = $obj_en->name;
            $tuple[] = $this->_strip($obj_it->sdesc);
            $tuple[] = $this->_strip($obj_en->sdesc);
            $tuple[] = $this->_strip($obj_it->ldesc);
            $tuple[] = $this->_strip($obj_en->ldesc);
            $tuple[] = $obj_it->published;
            $tuple[] = $obj_en->published;
            $tuple[] = $cat->position;

            $odd = ($i % 2 == 0) ? "style='background-color:#FFFFCC;'" : "style='background-color:#DDFFC6;'";

            $table_rows .= "<tr $odd><td align='center'>" . implode("</td><td align='center'>", $tuple) . "</td></tr>";

            $i++;
        }

        $table_headers = "<tr style='background-color:#DEDEDE;'><th>" . implode("</th><th>", $headers) . "</th></tr>";

        $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
        $table_headers
        $table_rows
</table>
TABLE;

        echo $this->_toCharset($table);
    }

    function getExportAttributes($translated = true)
    {

        $this->_excel();

        $query = "SELECT A.*,L.* FROM attributes A LEFT JOIN attributes_lang L ON A.id=L.attribute_id WHERE lang_id='it' AND frontend_input IN ('select','multiselect') AND published=1 ORDER BY position";
        $attributes = DB::select($query);

        $headers = [];
        $table_rows = '';

        $headers[] = ($translated) ? 'Codice' : 'code';
        $headers[] = ($translated) ? 'Nome (IT)' : 'name_it';
        $headers[] = ($translated) ? 'Nome (EN)' : 'name_en';
        $headers[] = ($translated) ? 'Posizione' : 'position';


        $i = 0;
        foreach ($attributes as $obj) {

            $table_spec = <<<TEXT
   <th colspan=4>$obj->name ($obj->code) $obj->ldesc</th>
TEXT;


            $query = "SELECT A.*,L.* FROM attributes_options A LEFT JOIN attributes_options_lang L ON A.id=L.attribute_option_id WHERE lang_id='it' AND attribute_id=? ORDER BY attribute_id,position";
            $options = DB::select($query, [$obj->id]);

            $i = 0;
            $table_rows = "";
            foreach ($options as $opt) {
                $query = "SELECT * FROM attributes_options_lang WHERE lang_id=? AND attribute_option_id=?";
                $opt_en = DB::selectOne($query, ['en', $opt->id]);

                $tuple = [];

                $tuple[] = $opt->uname;
                $tuple[] = $opt->name;
                $tuple[] = $opt_en->name;
                $tuple[] = $opt->position;

                $odd = ($i % 2 == 0) ? "style='background-color:#FFFFCC;'" : "style='background-color:#DDFFC6;'";

                $table_rows .= "<tr $odd><td align='center'>" . implode("</td><td align='center'>", $tuple) . "</td></tr>";

                $i++;
            }

            $table_headers = "<tr style='background-color:#DEDEDE;'><th>" . implode("</th><th>", $headers) . "</th></tr>";

            $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
        $table_spec
        $table_headers
        $table_rows
</table><br /><br />
TABLE;

            echo $this->_toCharset($table);
        }
    }

    function _toCharset($text)
    {
        if ($this->excel == false)
            return $text;
        //$text = utf8_encode($text);
        return mb_convert_encoding($text, 'windows-1252', 'utf-8');
        return iconv('utf-8', 'windows-1252//TRANSLIT', $text);
    }

    function _excel()
    {

        if ($this->excel == false) {
            header("Content-Type: text/html; charset=utf-8");
            return;
        }


        header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
        //header("Content-type:   application/x-msexcel; charset=utf-8");
        header("Content-Disposition: attachment; filename=export.xls");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
    }

    function _strip($str)
    {
        //return $str;
        $str = strip_tags($str, '<strong><b><em><i><u>');
        return $str;
        //return str_replace(['<br>','<br />','<p>','</p>'], "", $str);
    }


    public function getRename()
    {
        /**
         * Get IMAGE files recursively from Root and all sub-folders
         * Skip folders in our list of results
         * LEAVES_ONLY mode makes sure ONLY FILES/Leafs endpoint is returned
         * Make sure file extension is in our Images extensions array
         */
        $path = 'C:\wamp\www\l4\sources\optimized';

        $directory = new RecursiveDirectoryIterator($path, RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator($directory, RecursiveIteratorIterator::LEAVES_ONLY);

        $extensions = array("png", "jpg", "jpeg", "gif");
        echo '<pre>';
        foreach ($iterator as $fileinfo) {
            if (in_array($fileinfo->getExtension(), $extensions)) {
                $pathname = $fileinfo->getPathname();
                $new_pathname = str_replace('-min', '', $pathname);
                echo 'File path = ' . $pathname . PHP_EOL;
                echo 'File new path = ' . $new_pathname . PHP_EOL;
                //echo 'File extension = ' .$fileinfo->getExtension(). PHP_EOL;
                if ($pathname != $new_pathname) {
                    rename($pathname, $new_pathname);
                    echo '==>> RENAMED' . PHP_EOL;
                }
                $files[] = $fileinfo->getPathname();
            }
        }

        print_r($files);
        echo '</pre>';
    }
}


function minifyJS($arr)
{
    minify($arr, 'http://javascript-minifier.com/raw');
}

function minifyCSS($arr)
{
    minify($arr, 'http://cssminifier.com/raw');
}

function minify($arr, $url)
{
    foreach ($arr as $key => $value) {
        $handler = fopen($value, 'w') or die("File <a href='" . $value . "'>" . $value . "</a> error!<br />");
        fwrite($handler, getMinified($url, file_get_contents($key)));
        fclose($handler);
        echo "File <a href='" . $value . "'>" . $value . "</a> done!<br />";
    }
}

function getMinified($url, $content)
{
    $data = array(
        'input' => $content,
    );

    // init the request, set some info, send it and finally close it
    $ch = curl_init($url);

    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $minified = curl_exec($ch);

    curl_close($ch);
    return $minified;
}


class Casset_Cssurirewriter
{

    /**
     * rewrite() and rewriteRelative() append debugging information here
     * @var string
     */
    public static $debugText = '';

    /**
     * In CSS content, rewrite file relative URIs as root relative
     *
     * @param string $css
     *
     * @param string $currentDir The directory of the current CSS file.
     *
     * @param string $docRoot The document root of the web site in which
     * the CSS file resides (default = $_SERVER['DOCUMENT_ROOT']).
     *
     * @param array $symlinks (default = array()) If the CSS file is stored in
     * a symlink-ed directory, provide an array of link paths to
     * target paths, where the link paths are within the document root. Because
     * paths need to be normalized for this to work, use "//" to substitute
     * the doc root in the link paths (the array keys). E.g.:
     * <code>
     * array('//symlink' => '/real/target/path') // unix
     * array('//static' => 'D:\\staticStorage')  // Windows
     * </code>
     *
     * @return string
     */
    public static function rewrite($css, $currentDir, $docRoot = null, $symlinks = array())
    {
        self::$_docRoot = self::_realpath(
            $docRoot ? $docRoot : $_SERVER['DOCUMENT_ROOT']
        );
        self::$_currentDir = self::_realpath($currentDir);
        self::$_symlinks = array();

        // normalize symlinks
        foreach ($symlinks as $link => $target) {
            $link = ($link === '//') ? self::$_docRoot : str_replace('//', self::$_docRoot . '/', $link);
            $link = strtr($link, '/', DIRECTORY_SEPARATOR);
            self::$_symlinks[$link] = self::_realpath($target);
        }

        self::$debugText .= "docRoot    : " . self::$_docRoot . "\n"
            . "currentDir : " . self::$_currentDir . "\n";
        if (self::$_symlinks) {
            self::$debugText .= "symlinks : " . var_export(self::$_symlinks, 1) . "\n";
        }
        self::$debugText .= "\n";

        $css = self::_trimUrls($css);

        // rewrite
        $css = preg_replace_callback('/@import\\s+([\'"])(.*?)[\'"]/'
            , array(self::$className, '_processUriCB'), $css);
        $css = preg_replace_callback('/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
            , array(self::$className, '_processUriCB'), $css);

        return $css;
    }

    /**
     * In CSS content, prepend a path to relative URIs
     *
     * @param string $css
     *
     * @param string $path The path to prepend.
     *
     * @return string
     */
    public static function prepend($css, $path)
    {
        self::$_prependPath = $path;

        $css = self::_trimUrls($css);

        // append
        $css = preg_replace_callback('/@import\\s+([\'"])(.*?)[\'"]/'
            , array(self::$className, '_processUriCB'), $css);
        $css = preg_replace_callback('/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
            , array(self::$className, '_processUriCB'), $css);

        self::$_prependPath = null;
        return $css;
    }

    /**
     * Get a root relative URI from a file relative URI
     *
     * <code>
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       '../img/hello.gif'
     *     , '/home/user/www/css'  // path of CSS file
     *     , '/home/user/www'      // doc root
     * );
     * // returns '/img/hello.gif'
     *
     * // example where static files are stored in a symlinked directory
     * Minify_CSS_UriRewriter::rewriteRelative(
     *       'hello.gif'
     *     , '/var/staticFiles/theme'
     *     , '/home/user/www'
     *     , array('/home/user/www/static' => '/var/staticFiles')
     * );
     * // returns '/static/theme/hello.gif'
     * </code>
     *
     * @param string $uri file relative URI
     *
     * @param string $realCurrentDir realpath of the current file's directory.
     *
     * @param string $realDocRoot realpath of the site document root.
     *
     * @param array $symlinks (default = array()) If the file is stored in
     * a symlink-ed directory, provide an array of link paths to
     * real target paths, where the link paths "appear" to be within the document
     * root. E.g.:
     * <code>
     * array('/home/foo/www/not/real/path' => '/real/target/path') // unix
     * array('C:\\htdocs\\not\\real' => 'D:\\real\\target\\path')  // Windows
     * </code>
     *
     * @return string
     */
    public static function rewriteRelative($uri, $realCurrentDir, $realDocRoot, $symlinks = array())
    {
        // prepend path with current dir separator (OS-independent)
        $path = strtr($realCurrentDir, '/', DIRECTORY_SEPARATOR)
            . DIRECTORY_SEPARATOR . strtr($uri, '/', DIRECTORY_SEPARATOR);

        self::$debugText .= "file-relative URI  : {$uri}\n"
            . "path prepended     : {$path}\n";

        // "unresolve" a symlink back to doc root
        foreach ($symlinks as $link => $target) {
            if (0 === strpos($path, $target)) {
                // replace $target with $link
                $path = $link . substr($path, strlen($target));

                self::$debugText .= "symlink unresolved : {$path}\n";

                break;
            }
        }
        // strip doc root
        $path = substr($path, strlen($realDocRoot));

        self::$debugText .= "docroot stripped   : {$path}\n";

        // fix to root-relative URI

        $uri = strtr($path, '/\\', '//');

        $uri = self::removeDots($uri);

        self::$debugText .= "traversals removed : {$uri}\n\n";

        return $uri;
    }

    /**
     * Remove instances of "./" and "../" where possible from a root-relative URI
     * @param string $uri
     * @return string
     */
    public static function removeDots($uri)
    {
        $uri = str_replace('/./', '/', $uri);
        // inspired by patch from Oleg Cherniy
        do {
            $uri = preg_replace('@/[^/]+/\\.\\./@', '/', $uri, 1, $changed);
        } while ($changed);
        return $uri;
    }

    /**
     * Defines which class to call as part of callbacks, change this
     * if you extend Minify_CSS_UriRewriter
     * @var string
     */
    protected static $className = 'Casset_Cssurirewriter';

    /**
     * Get realpath with any trailing slash removed. If realpath() fails,
     * just remove the trailing slash.
     *
     * @param string $path
     *
     * @return mixed path with no trailing slash
     */
    protected static function _realpath($path)
    {
        $realPath = realpath($path);
        if ($realPath !== false) {
            $path = $realPath;
        }
        return rtrim($path, '/\\');
    }

    /**
     * @var string directory of this stylesheet
     */
    private static $_currentDir = '';

    /**
     * @var string DOC_ROOT
     */
    private static $_docRoot = '';

    /**
     * @var array directory replacements to map symlink targets back to their
     * source (within the document root) E.g. '/var/www/symlink' => '/var/realpath'
     */
    private static $_symlinks = array();

    /**
     * @var string path to prepend
     */
    private static $_prependPath = null;

    private static function _trimUrls($css)
    {
        return preg_replace('/
            url\\(      # url(
            \\s*
            ([^\\)]+?)  # 1 = URI (assuming does not contain ")")
            \\s*
            \\)         # )
        /x', 'url($1)', $css);
    }

    private static function _processUriCB($m)
    {
        // $m matched either '/@import\\s+([\'"])(.*?)[\'"]/' or '/url\\(\\s*([^\\)\\s]+)\\s*\\)/'
        $isImport = ($m[0][0] === '@');
        // determine URI and the quote character (if any)
        if ($isImport) {
            $quoteChar = $m[1];
            $uri = $m[2];
        } else {
            // $m[1] is either quoted or not
            $quoteChar = ($m[1][0] === "'" || $m[1][0] === '"') ? $m[1][0] : '';
            $uri = ($quoteChar === '') ? $m[1] : substr($m[1], 1, strlen($m[1]) - 2);
        }
        // analyze URI
        if ('/' !== $uri[0]                  // root-relative
            && false === strpos($uri, '//')  // protocol (non-data)
            && 0 !== strpos($uri, 'data:')   // data protocol
        ) {
            // URI is file-relative: rewrite depending on options
            if (self::$_prependPath === null) {
                $uri = self::rewriteRelative($uri, self::$_currentDir, self::$_docRoot, self::$_symlinks);
            } else {
                $uri = self::$_prependPath . $uri;
                if ($uri[0] === '/') {
                    $root = '';
                    $rootRelative = $uri;
                    $uri = $root . self::removeDots($rootRelative);
                } elseif (preg_match('@^((https?\:)?//([^/]+))/@', $uri, $m) && (false !== strpos($m[3], '.'))) {
                    $root = $m[1];
                    $rootRelative = substr($uri, strlen($root));
                    $uri = $root . self::removeDots($rootRelative);
                }
            }
        }
        return $isImport ? "@import {$quoteChar}{$uri}{$quoteChar}" : "url({$quoteChar}{$uri}{$quoteChar})";
    }


}

?>
