<?php

class MorellatoShopsController extends BackendController
{

    public $component_id = 83;
    public $title = 'Gestione Negozi Negoziando';
    public $page = 'morellato_shops.index';
    public $pageheader = 'Gestione Negozi Negoziando';
    public $iconClass = 'font-columns';
    public $model = 'MorellatoShop';
    protected $rules = array(
        'name' => 'required',
        'cd_neg' => 'required',
        //'cd_sped' => 'required',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Negozi Negoziando');
        $this->toFooter("js/echo/morellato_shops.js?v=5");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/morellato_shops.js");
        $this->page = 'morellato_shops.trash';
        $this->pageheader = 'Cestino Stati / Province';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/morellato_shops.js");
        $this->page = 'morellato_shops.create';
        $this->pageheader = 'Nuovo Negozio Negoziando';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/morellato_shops.js");
        $this->page = 'morellato_shops.create';
        $this->pageheader = 'Modifica Negozio Negoziando';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {
        \Utils::watch();
        $model = $this->model;

        $lang_id = \Core::getLang();


        $pages = $model::select('id', 'cd_neg', 'cd_internal', 'name', 'description', 'address', 'city', 'featured', 'engraving', 'active', 'created_at');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::date($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('active', function ($data) {
                $status = \URL::action($this->action("postSwitchFlag"), array($data['id'], 'active'));
                return $data['active'] == 1 ? '<span title="Clicca per eseguire lo switch di questo valore" onclick="EchoTable.action(\''.$status.'\');" class="label label-success">Sì</span>' : '<span title="Clicca per eseguire lo switch di questo valore" onclick="EchoTable.action(\''.$status.'\');" class="label label-danger">No</span>';
            })
            ->edit_column('featured', function ($data) {
                $status = \URL::action($this->action("postSwitchFlag"), array($data['id'], 'featured'));
                return $data['featured'] == 1 ? '<span title="Clicca per eseguire lo switch di questo valore" onclick="EchoTable.action(\''.$status.'\');" class="label label-success">Sì</span>' : '<span title="Clicca per eseguire lo switch di questo valore" onclick="EchoTable.action(\''.$status.'\');" class="label label-danger">No</span>';
            })
            ->edit_column('engraving', function ($data) {
                $status = \URL::action($this->action("postSwitchFlag"), array($data['id'], 'engraving'));
                return $data['engraving'] == 1 ? '<span title="Clicca per eseguire lo switch di questo valore" onclick="EchoTable.action(\''.$status.'\');" class="label label-success">Sì</span>' : '<span title="Clicca per eseguire lo switch di questo valore" onclick="EchoTable.action(\''.$status.'\');" class="label label-danger">No</span>';
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getTabletrash()
    {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('countries_lang', 'morellato_shops.country_id', '=', 'countries_lang.country_id')
            ->where('lang_id', $lang_id)
            ->select('morellato_shops.id', 'morellato_shops.name as state', 'countries_lang.name as country', 'morellato_shops.active', 'morellato_shops.deleted_at', 'morellato_shops.created_at', 'countries_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::date($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::date($data['deleted_at']);
            })
            ->edit_column('state', function ($data) {
                return "<strong>{$data['country']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;
        $latitude = \Input::get('latitude');
        $longitude = \Input::get('longitude');

        if ($latitude == '' or $latitude == 0)
            $latitude = null;

        if ($longitude == '' or $longitude == 0)
            $longitude = null;

        $_POST['latitude'] = $latitude;
        $_POST['longitude'] = $longitude;

        \Input::replace($_POST);

    }


    function _after_update($model)
    {
        \DB::table('mi_shops')->where('latitude', 0)->update(['latitude' => null]);
        \DB::table('mi_shops')->where('longitude', 0)->update(['longitude' => null]);
    }

}
