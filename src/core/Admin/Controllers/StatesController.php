<?php

class StatesController extends BackendController {

    public $component_id = 47;
    public $title = 'Gestione Stati / Province';
    public $page = 'states.index';
    public $pageheader = 'Gestione Stati / Province';
    public $iconClass = 'font-columns';
    public $model = 'State';
    protected $rules = array(
        'iso_code' => 'required',
        'name' => 'required',
        'country_id' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Stati / Province');
        $this->toFooter("js/echo/states.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/states.js");
        $this->page = 'states.trash';
        $this->pageheader = 'Cestino Stati / Province';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/states.js");
        $this->page = 'states.create';
        $this->pageheader = 'Nuovo Stato / Provincia';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/states.js");
        $this->page = 'states.create';
        $this->pageheader = 'Modifica Stato / Provincia';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::leftJoin("countries_lang", "states.country_id", "=", "countries_lang.country_id")
                ->where('lang_id', $lang_id)
                ->select('states.id', 'states.name as state', 'countries_lang.name as country', 'states.active', 'states.created_at', 'countries_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('state', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['state']}</a></strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })
                        ->remove_column('lang_id')
                        ->rebind_column("countries_lang.name", "states.country_id")
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('countries_lang', 'states.country_id', '=', 'countries_lang.country_id')
                ->where('lang_id', $lang_id)
                ->select('states.id', 'states.name as state', 'countries_lang.name as country', 'states.active', 'states.deleted_at', 'states.created_at', 'countries_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::date($data['deleted_at']);
                        })
                        ->edit_column('state', function($data) {
                            return "<strong>{$data['country']}</strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;

        \Input::replace($_POST);

    }

    function postList($country_id) {        
        $rows = State::where("country_id",$country_id)->orderBy("name")->select("id","name")->get();
        $data = array('success' => true, 'data' => $rows->toArray());
        return Json::encode($data);
    }

}
