<?php

namespace Echo1\Admin;

use Input,
    BackendController,
    Utils,
    Str;

class MediaController extends BackendController
{
    /* public $title = 'Dashboard';
      public $page = 'dashboard.index';
      public $pageheader = 'Dashboard'; */

    public $component_id = 5;
    public $title = 'Gestione Media';
    public $page = 'media.index';
    public $pageheader = 'Gestione Media';
    public $iconClass = 'font-image';


    protected function toolbar($what = 'default')
    {


    }


    public function index()
    {


        $this->pageheader = 'Gestione Media';
        $this->addBreadcrumb($this->pageheader);

        $this->toFooter('js/echo/media.js');
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function cache()
    {

        $this->page = 'media.cache';
        $this->pageheader = 'Gestione Cache';
        $this->addBreadcrumb($this->pageheader);

        $this->toFooter('js/echo/cache.js');
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function purge()
    {
        $this->rememberStaticKey = \Config::get('cache.rememberStaticKey');
        ini_set('max_execution_time',3600);
        $action = \Input::get("action");
        $success = true;
        $msg = 'Operazione eseguita con successo';
        switch ($action) {
            case 'admin':
                \Cache::forget('nav_items_0');
                $rows = \DB::table("widgets")
                    ->get();
                foreach ($rows as $row) {
                    \Cache::forget('nav_items_' . $row->id);
                }
                break;

            case 'config':
                \Cfg::purge();
                break;

            case 'lang':
                $languages = \Core::getLanguages();
                foreach($languages as $lang){
                    \Cache::forget("site_cache_".$lang);
                    \Cache::forget("langobj_".$lang);
                    \Cache::forget($lang."_javascript_lexicon");
                }
                \Currency::initCache();
                \Cache::forget($this->rememberStaticKey . "_languages_1");
                \Cache::forget($this->rememberStaticKey . "_languages_0");
                break;

            case 'route':
                \Cache::forget("route_markers");
                \Cache::forget("route_nav");
                \Cache::forget("route_attribute");
                \Cache::forget("route_attribute_filters");
                \Cache::forget("shortcut_nav");
                \Cache::forget("link_nav");
                \Cache::forget("link_attribute");
                break;

            case 'position':
                $rows = \ModulePosition::get();
                foreach ($rows as $row) {
                    \Cache::forget("position-" . $row->name);
                    \Cache::forget("mobile-position-" . $row->name);
                }
                \Cache::forget("override_templates");
                break;

            case 'module':
                $rows = \Module::get();
                $languages = \Core::getLanguages();
                foreach($rows as $row){
                    foreach($languages as $lang){
                        \Cache::forget("module-$lang-".$row->id);
                        \Cache::forget("mobile-module-$lang-".$row->id);
                    }
                    $row->uncache();
                }
                break;

            case 'attribute':
                $languages = \Core::getLanguages();
                foreach ($languages as $lang) {
                    \Cache::forget("db-attributes-$lang");
                    \Cache::forget("catalog_attribute_nav_$lang");
                    \Cache::forget("catalog_special_$lang");
                }
                \Cache::forget("link_attribute");
                break;

            case 'pricerule':
                /*$rows = \Product::where("is_out_of_production",0)->lists('id');
                $cache = \Product::cache('price-rules-products');
                foreach ($rows as $id) {
                    $cache->forget("product-price-rule-$id");
                }*/
                \Cache::driver(config('cache.product_driver', 'apc'))->tags(['price-rules-products'])->flush();
                break;

            case 'carrier':
                $rows = \Carrier::all();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                break;

            case 'redis':
                \Category::initCache();
                \Brand::initCache();
                \Collection::initCache();
                \Trend::initCache();
                \Nav::initCache();
                \Attribute::initCache();
                \AttributeOption::initCache();
                \Section::initCache();
                \Page::initCache();
                \PriceRule::initCache();
                \Lexicon::initCache();

                $rows = \Category::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Brand::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Collection::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Trend::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Nav::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Attribute::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \AttributeOption::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Section::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \Page::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                $rows = \PriceRule::get();
                foreach($rows as $row){
                    $row->uncache();
                }
                break;

            case 'products':
                $rows = \Product::get();
                foreach($rows as $row){
                    $row->uncacheSimple();
                }

                break;

            case 'catalog':
                \Helper::uncache();
                break;

            case 'intelligent':
                \Helper::purgeAllTaggedCached(true);
                break;
        }

        return \Json::encode(compact("success", "msg"));
    }


}
