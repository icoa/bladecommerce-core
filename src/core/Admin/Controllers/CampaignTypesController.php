<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class CampaignTypesController extends BackendController
{

    public $component_id = 40;
    public $title = 'Tipi campagne';
    public $page = 'campaign_types.index';
    public $pageheader = 'Tipi campagne';
    public $iconClass = 'font-columns';
    public $model = 'CampaignType';
    protected $rules = array(
        'name' => 'required',
    );
    protected $friendly_names = array(
        'name' => 'Denominazione',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco tipo campagne');
        $this->toFooter("js/echo/campaign_types.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/campaign_types.js");
        $this->page = 'campaign_types.create';
        $this->pageheader = 'Nuovo tipo campagna';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/campaign_types.js");
        $this->page = 'campaign_types.create';
        $this->pageheader = 'Modifica tipo campagna';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::leftJoin("campaigns as C", "campaign_types.id", "=", "C.campaign_type")->groupBy("campaign_types.id")->select('campaign_types.id', 'campaign_types.name', DB::raw("COUNT(campaign_type) as total"));

        return \Datatables::of($pages)
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }


    }

    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">    
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = \URL::action($this->action("getIndex"));

                $actions = array(
                    new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva e chiudi', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );

                break;

            default:
                $create = ($this->action("getCreate", TRUE));
                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $actions = array(
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }


/*
    public function postDelete($id) {
        \Utils::log("DELETED ID: $id");
        $model = $this->model;
        \Utils::log("MODEL: $model");
        $obj = $model::find($id);

        if(count($obj->campaigns) > 0){
            $data = array('success' => false, 'msg' => 'Questo elemento è collegato ad altre Campagne e non è possibile eliminarlo!');
        }else{
            $obj->delete();
            $data = array('success' => true, 'msg' => 'Elemento spostato nel cestino con successo');
        }

        return Json::encode($data);
    }

    public function postDeleteMulti() {
        $ids = $_POST['ids'];
        $model = $this->model;
        $obj = $model::whereIn('id', $ids);
        $obj->delete();

        $data = array('success' => true, 'msg' => 'Elementi spostati nel cestino con successo');
        return Json::encode($data);
    }
*/
}
