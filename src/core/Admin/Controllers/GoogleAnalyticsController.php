<?php

class GoogleAnalyticsController extends BackendController
{

    public $component_id = 75;
    public $title = 'Gestione Google Analytics';
    public $page = 'google_analytics.index';
    public $pageheader = 'Gestione Google Analytics';
    public $iconClass = 'font-cog';
    public $model = 'GoogleAnalytic';
    protected $rules = array(
        'name' => 'required|unique:google_analytics,name',
        'account' => 'required',
    );

    protected $friendly_names = array(
        'name' => 'Nome istanza',
        'account' => 'Account Google Analytics',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Google Analytics');
        $this->toFooter("js/echo/google_analytics.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/google_analytics.js");
        $this->page = 'google_analytics.trash';
        $this->pageheader = 'Cestino Google Analytics';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/google_analytics.js");
        $this->page = 'google_analytics.create';
        $this->pageheader = 'Nuovo destinatario Newsletter';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/google_analytics.js");
        $this->page = 'google_analytics.create';
        $this->pageheader = 'Modifica destinatario Newsletter';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::groupBy("google_analytics.id")->select('*');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('account', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['account']}</a></strong>";
            })
            ->edit_column('debug', function($data) {
                return $this->boolean($data, 'debug');
            })
            ->edit_column('overall', function($data) {
                return $this->boolean($data, 'overall');
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()
            ->groupBy("google_analytics.id")
            ->leftJoin("customers as C", "google_analytics.customer_id", "=", "C.id")
            ->select('google_analytics.id', 'google_analytics.email', 'google_analytics.lang_id', 'google_analytics.user_ip', 'google_analytics.marketing', 'google_analytics.deleted_at', 'C.name as customer', 'customer_id');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('lang_id', function ($data) {
                $src = \AdminTpl::img("images/flags/{$data['lang_id']}.jpg");
                return "<img src='$src' />";
            })
            ->edit_column('marketing', function($data) {
                return $this->boolean($data, 'marketing');
            })
            ->edit_column('customer', function ($data) {
                $link = \URL::action("CustomersController@getEdit", $data['customer_id']);
                return "<strong><a href='$link'>{$data['customer']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('customer_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->rules['name'] .= ',' . $model->id;
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
    }

    function getExport(){

        $rows = \Newsletter::orderBy('email')->get();

        $lines = ['email,lingua,data'];
        foreach($rows as $row){
            $lines[] = $row->email.",".$row->lang_id.",".date("d/m/Y",strtotime($row->created_at));
        }

        $file = storage_path(). '/temp.csv';

        \File::put( $file, implode(PHP_EOL,$lines));

        $filename = "destinatari_newsletter_".time().".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }

    protected function actions_default( $params = [] ){
        $actions = parent::actions_default( ['except' => ['trash']]);
        return $actions;
    }


    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);


        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');


        $_POST['name'] = strtolower(\Str::camel($_POST['name']));
        $_POST['params'] = serialize($_POST['params']);
        if(!isset($_POST['debug']))$_POST['debug'] = 0;
        if(!isset($_POST['overall']))$_POST['overall'] = 0;


        \Input::replace($_POST);

    }







}