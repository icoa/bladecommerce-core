<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class CurrenciesController extends BackendController {

    public $component_id = 48;
    public $title = 'Gestione Valute';
    public $page = 'currencies.index';
    public $pageheader = 'Gestione Valute';
    public $iconClass = 'font-columns';
    public $model = 'Currency';
    protected $rules = array(
        'iso_code' => 'required|unique:currencies,iso_code',
        'iso_code_num' => 'required|unique:currencies,iso_code_num',
        'name' => 'required',
        'sign' => 'required',
        'conversion_rate' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Valute');
        $this->toFooter("js/echo/currencies.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/currencies.js");
        $this->page = 'currencies.trash';
        $this->pageheader = 'Cestino Valute';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/currencies.js");
        $this->page = 'currencies.create';
        $this->pageheader = 'Nuovo Valuta';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/currencies.js");
        $this->page = 'currencies.create';
        $this->pageheader = 'Modifica Valuta';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->hasDecimals = ($obj->decimals > 0);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::select('id', 'name', 'iso_code', 'iso_code_num', 'sign', 'conversion_rate', 'active', 'created_at');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })                        
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->select('id', 'name', 'iso_code', 'iso_code_num', 'sign', 'conversion_rate', 'active', 'deleted_at');

        return \Datatables::of($pages)                        
                        ->edit_column('deleted_at', function($data) {
                            return \Format::date($data['deleted_at']);
                        })
                        ->edit_column('name', function($data) {
                            return "<strong>{$data['name']}</strong>";
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })   
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })                        
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['iso_code'] = 'required|unique:currencies,iso_code,' . $model->id;
        $this->rules['iso_code_num'] = 'required|unique:currencies,iso_code_num,' . $model->id;
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;
        $vars = $this->_getDataByFormat($_POST['format']);
        $_POST['decimals'] = ((int)$_POST['hasDecimals'] == 0) ? 0 : $vars["decimals"];
        $_POST['dec_point'] = $vars["dec_point"];
        $_POST['thousan_sep'] = $vars["thousan_sep"];

        \Input::replace($_POST);

    }
    
    function _getDataByFormat($format){
        
        switch ($format) {
            case "1":
            case "4":
                $decimals = 2;
                $dec_point = '.';
                $thousan_sep = ',';
                break;
            
            case "2":
                $decimals = 2;
                $dec_point = ',';
                $thousan_sep = ' ';
                break;
            
            case "3":
                $decimals = 2;
                $dec_point = ',';
                $thousan_sep = '.';
                break;
            
            case "5":
                $decimals = 2;
                $dec_point = '.';
                $thousan_sep = '\'';
                break;
        }
        
        return compact("decimals","dec_point","thousan_sep");
    }

    function postList() {
        $rows = \Mainframe::brands();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

    function _after_update($model)
    {
        parent::_after_update($model);
        $model->uncache();
    }

}
