<?php

class MenusController extends MultiLangController
{

    public $component_id = 60;
    public $title = 'Gestione Voci di menù';
    public $page = 'menus.index';
    public $pageheader = 'Gestione Voci di menù';
    public $iconClass = 'font-columns';
    public $model = 'Menu';
    protected $rules = array('position' => 'required');
    protected $lang_rules = array(
        'name' => 'required',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'PRE POST');

        $_POST['starting_at'] = Format::sqlDatetime($_POST['starting']);
        $_POST['finishing_at'] = Format::sqlDatetime($_POST['finishing']);
        $_POST['params'] = \ModuleHelper::setParams($_POST['params']);
        $_POST['acl_groups'] = implode(",",$_POST['acl_groups']);

        $default_lang = \Core::getLang();

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            $source = $_POST["name_" . $default_lang];
            $target = $_POST["name_" . $lang];
            if ($target == "") {
                $_POST["name_" . $lang] = $source;
            }
        }

        $link_params = \Input::get("link_params");
        if(is_array($link_params)){
            $_POST['link_type'] = \LinkHelper::getTypeByLinkParams($link_params);
            $_POST['link_params'] = serialize($link_params);
        }

        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Voci di menù');
        $this->toFooter("js/echo/menus.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'menus.trash';
        $this->pageheader = 'Cestino Voci di menù';
        $this->toFooter("js/echo/menus.js");
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }


    public function getEdit($id)
    {
        $this->page = 'menus.create';
        $this->pageheader = 'Modifica Voce di menù';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getDescribe($id,$lang_id)
    {
        $model = $this->model;
        $obj = $model::getObj($id,$lang_id);
        $obj->typename = \LinkHelper::getTypename($obj->link_type);
        return Json::encode($obj);
    }



    public function postEditinline($id)
    {
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        $success = true;
        $html = Theme::scope("menus.edit", $view)->content();
        return Json::encode(compact("success","html"));
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)->leftJoin("module_types","menus.mod_type","=","module_types.module")
            ->select('menus.id', 'modules_lang.name', 'menus.mod_type', 'mod_position', 'modules_lang.published', 'menus.created_at', 'menus.position', 'modules_lang.input_textarea', 'modules_lang.lang_id', 'module_types.name as typename');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('mod_type', function ($data) {
                return "<em>{$data['typename']}</em>";
            })
            ->edit_column('mod_position', function ($data) {
                return "<strong>{$data['mod_position']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicato</span>' : '<span class="label label-important">Sospeso</span>';
            })
            ->edit_column('position', function($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('input_textarea')
            ->remove_column('typename')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)->leftJoin("menu_types","menus.menutype_id","=","menu_types.id")
            ->select('menus.id', 'menus_lang.name', 'link_type', 'menu_types.name as menutype','menus.deleted_at', 'menus_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getCreate(){
        $menutype_id = (\Input::old("menutype_id") != '') ? \Input::old("menutype_id") : \Input::get("menutype_id",0);
        $this->page = 'menus.create';
        $this->pageheader = 'Nuova Voce di menù';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array("menutype_id" => $menutype_id);
        return $this->render($view);
    }


    public function postDeletemenu($id){

        try{
            $this->deleteTree($id);
            $data = array('success' => true, 'msg' => 'Elemento rimosso con successo');
        }catch(Exception $e){
            $data = array('success' => false, 'msg' => 'Si è verificato un errore durante la rimozione del nodo '.$id);
            \Utils::log($e->getMessage());
        }

        return Json::encode($data);

    }

    private function deleteTree($id){
        $children = \Menu::where("parent_id",$id)->get();
        foreach($children as $child){
            $this->deleteTree($child->id);
        }
        $obj = \Menu::find($id);
        $obj->delete();
    }


    public function postGenerate(){
        $type = \Input::get("type");
        $name = \Input::get("name");
        $menutype_id = \Input::get("menutype_id");
        $ids = \Input::get("ids");
        $parent_id = \Input::get("parent_id");

        $lang = \Core::getLang();
        $languages = \Mainframe::languagesCodes();

        $html = '';

        \Utils::watch();

        $params = [
            'rendering_type' => 'link',
            "open_mode" => "_self",
        ];

        $obj = null;
        $showname = 1;

        foreach($ids as $id){
            switch($type){
                case "base":
                case "list":
                case "qty":
                case "pricerange":
                case "status":
                    $obj = \Nav::find($id);
                    $itemname = $obj->getPublicName($lang);
                    $params['default'] = ['target_type' => $type, 'target_id' => $id];
                    break;

                case "category":
                case "brand":
                case "collection":
                case "trend":
                case "section":
                case "page":
                    $model = ucfirst($type);
                    $obj = $model::find($id);
                    $itemname = $obj->$lang->name;
                    $params['default'] = ['target_type' => $type, 'target_id' => $id];
                    break;

                case "modulePosition":
                    $model = ucfirst($type);
                    $obj = $model::find($id);
                    $itemname = $obj->name;
                    $showname = 0;
                    $params['default'] = ['target_type' => $type, 'target_id' => $id];
                    break;

                case "other":
                    switch($id){
                        case 'url':
                        case 'separator':
                        case 'container':
                        case 'html':
                        case 'auto':
                            $type = $id;
                            $params['rendering_type'] = $type;
                            $name = \LinkHelper::getTypename($type);
                            $itemname = $name;
                            $showname = 0;
                            break;

                        case 'product':
                            $type = $id;
                            $params['default'] = ['target_type' => 'product', 'target_id' => 0];
                            $name = \LinkHelper::getTypename($type);
                            $itemname = $name;
                            break;

                        default: //assuming is a module position
                            $type = 'modulePosition';
                            $params['rendering_type'] = $type;
                            $params['modulePosition'] = $id;
                            $name = \LinkHelper::getTypename($type);
                            $itemname = $id;
                            $showname = 0;
                            break;
                            break;
                    }

                    break;

                default: //assuming attribute option
                    $obj = \AttributeOption::find($id);
                    $itemname = $obj->getPublicName($lang);
                    $params['default'] = ['target_type' => $type, 'target_id' => $id];
                    break;
            }

            $menu = new \Menu();
            $menu->parent_id = $parent_id;
            $menu->menutype_id = $menutype_id;
            $menu->link_type = $type;
            $menu->showname = $showname;
            //$menu->setPosition();
            $menu->position = $menu->position($parent_id);
            \Utils::log($menu->position,"AUTO POSITION");
            $lb = new \LinkHelper("default");
            $lb->setValues($params);
            $menu->link_params = $lb->getDbValues();
            $menu->save();
            $record_id = $menu->id;

            $tr = $menu->translations();
            foreach($languages as $lang_id){
                $langMenu = new \Menu_Lang();
                $langMenu->lang_id = $lang_id;
                if($obj){
                    try{
                        $langMenu->name = $obj->getPublicName($lang_id);
                    }catch(Exception $ex){
                        $langMenu->name = $obj->$lang_id->name;
                    }
                }else{
                    $langMenu->name = $itemname;
                }

                $tr->save($langMenu);
            }

            $html .= \LinkHelper::getHtmlTemplate($record_id,$itemname,$name);
        }


        $success = true;
        return Json::encode(compact("success","html"));

        //$obj = new \Menu();
    }



}