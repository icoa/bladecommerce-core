<?php

class CategoriesController extends MultiLangController {

    public $component_id = 17;
    public $title = 'Gestione Categorie';
    public $page = 'categories.index';
    public $pageheader = 'Gestione Categorie';
    public $iconClass = 'font-columns';
    public $model = 'Category';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:categories_lang,slug,category_id'
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/categories.js");
    }

   
    
    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] .','.$model->id;
        $this->_prepare();
    }

    function _prepare() {
        if(count($_POST) == 0){
            return;
        }        
        
        $langDef = Cfg::get('DEFAULT_LANGUAGE');
        
        $defaultImage = $_POST["image_default_".$langDef];
        $defaultImageThumb = $_POST["image_thumb_".$langDef];
        $defaultImageZoom = $_POST["image_zoom_".$langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            /*$source = $_POST["name_" . $lang];
            $target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }
            $target = $_POST["h1_" . $lang];
            if ($target == "") {
                $_POST["h1_" . $lang] = $source;
            }*/
            if($lang != $langDef){
                if($_POST["image_default_".$lang] == "")$_POST["image_default_".$lang] = $defaultImage;
                if($_POST["image_thumb_".$lang] == "")$_POST["image_thumb_".$lang] = $defaultImageThumb;
                if($_POST["image_zoom_".$lang] == "")$_POST["image_zoom_".$lang] = $defaultImageZoom;
            }
        }
        $parent_id = $_POST['parent_id'];
        if( (int) $_POST['position'] == 0)
            $_POST['position'] = $this->position($parent_id);
        \Input::replace($_POST);

    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Categorie');
        $this->bodyClass = 'extended';
        $this->prependWidget('CategoryTree');
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->page = 'categories.trash';
        $this->pageheader = 'Cestino Categorie';        
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {        
        $this->page = 'categories.create';
        $this->pageheader = 'Nuova Categoria';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {        
        $this->page = 'categories.create';
        $this->pageheader = 'Modifica Categoria';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }
    
    
    
        
    

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $parent_id = isset($_REQUEST['parent_id']) ? intval($_REQUEST['parent_id']) : 0;

        $model = $this->model;
        Utils::watch();
        $pages = $model::leftJoin('categories_lang', 'id', '=', 'categories_lang.category_id')
                ->where('lang_id', $lang_id)
                ->where('categories.parent_id', $parent_id)
                ->leftJoin('categories_products', 'categories_products.category_id', '=', 'categories.id')                
                ->leftJoin('products', 'categories_products.product_id', '=', 'products.id')
                ->whereNull('products.deleted_at')
                ->select('categories.id', 'name', 'slug', 'published', 'categories.created_at', 'categories.position', DB::raw("count(product_id) as products"), 'ldesc', 'lang_id')->groupBy("categories.id");
        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::datetime($data['created_at']);
                                })
                        ->edit_column('products', function($data) {
                                    return "<b><a href='/admin/products/?category_id=".$data['id']."'>".$data['products']."</a></b>";
                                })
                        ->edit_column('name', function($data) {
                                    $link = \URL::action($this->action("getEdit"), $data['id']);
                                    return "<strong><a href='$link'>{$data['name']}</a></strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_actions($data);
                                })
                        ->edit_column('published', function($data) {
                                    return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
                                })
                        ->edit_column('position', function($data,$index,$obj) {
                                   return $this->column_position($data, $index, $obj);
                                })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                                })
                        ->make();
    }
    
    
    

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        
        $pages = $model::onlyTrashed()->leftJoin('categories_lang', 'id', '=', 'categories_lang.category_id')
                ->where('categories_lang.lang_id', $lang_id)
                ->leftJoin('categories_products', 'categories_products.category_id', '=', 'categories.id')
                ->leftJoin( DB::raw('categories_lang as c2'), 'c2.category_id', '=', 'categories.parent_id')
                ->where('c2.lang_id', $lang_id)
                ->groupBy("categories.id")
                ->select('categories.id', 'categories_lang.name', 'c2.name as parent',  'categories_lang.slug', 'categories.deleted_at', 'categories.created_at', DB::raw("count(product_id) as products"), 'categories_lang.ldesc', 'categories_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::datetime($data['created_at']);
                                })
                        ->edit_column('products', function($data) {
                                    return "<b><a href='/admin/products/?category_id=".$data['id']."'>".$data['products']."</a></b>";
                                })        
                        ->edit_column('deleted_at', function($data) {
                                    return \Format::datetime($data['deleted_at']);
                                })
                        ->edit_column('name', function($data) {
                                    return "<strong>{$data['name']}</strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_trash_actions($data);
                                })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                                })
                        ->make();
    }

}