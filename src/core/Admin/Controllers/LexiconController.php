<?php

class LexiconController extends MultiLangController {

    public $component_id = 43;
    public $title = 'Gestione chiavi Lexicon';
    public $page = 'lexicon.index';
    public $pageheader = 'Gestione chiavi Lexicon';
    public $iconClass = 'font-columns';
    public $model = 'Lexicon';
    protected $rules = array(
        'code' => 'required|unique:lexicons,code',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/lexicon.js");
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco chiavi Lexicon');
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->page = 'lexicon.trash';
        $this->pageheader = 'Cestino chiavi Lexicon';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->page = 'lexicon.create';
        $this->pageheader = 'Nuova chiave Lexicon';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->page = 'lexicon.create';
        $this->pageheader = 'Modifica chiave Lexicon';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : \Core::getLang();

        $model = $this->model;

        $pages = $model::leftJoin('lexicons_lang', 'id', '=', 'lexicons_lang.lexicon_id')
            ->where('lang_id', $lang_id)
            ->select('id', 'code', 'name', 'name_plural', 'created_at', 'ldesc', 'lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('code', function($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['code']}</a></strong><br><em class='disabled'>{$data['ldesc']}</em>";
            })
            ->add_column('actions', function($data) {
                return $this->column_actions($data);
            })
            ->add_column('status', function($data) {
                return $this->flagStatus($data);
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->edit_column('id', function($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    private function flagStatus($data) {
        $languages = \Mainframe::languagesCodes();
        $obj = \Lexicon::find($data["id"]);
        $output = "";
        foreach ($languages as $lang_id) {
            if(isset($obj->{$lang_id}) and isset($obj->{$lang_id}->name)){
                if(trim($obj->{$lang_id}->name) != ""){
                    $output .= "<span title='Questa lingua è tradotta' class='label label-success mr5 mb5'>$lang_id</span>";
                }else{
                    $output .= "<span title='Questa lingua NON è tradotta' class='label label-important mr5 mb5'>$lang_id</span>";
                }
            }
        }
        return $output;
    }

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;

        $pages = $model::onlyTrashed()->leftJoin('lexicons_lang', 'id', '=', 'lexicons_lang.lexicon_id')
            ->where('lang_id', $lang_id)
            ->select('id', 'code', 'name', 'name_plural', 'deleted_at', 'created_at', 'ldesc', 'lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('code', function($data) {
                return "<strong>{$data['code']}</strong><br><em class='disabled'>{$data['ldesc']}</em>";
            })
            ->add_column('actions', function($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->edit_column('id', function($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create() {
        $this->_prepare();
        \Lex::forget();
    }

    function _before_update($model) {
        $this->rules['code'] = 'required|unique:lexicons,code,' . $model->id;
        $this->_prepare();
        \Lex::forget();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        //replace spaces from code and make it lower
        $_POST['code'] = str_replace(' ', '_', Str::lower($_POST['code']));


        \Input::replace($_POST);

    }

    function getSummary($id) {
        $html = Helper::getAttributesTableSummary($id);
        echo $html;
    }

}
