<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 20-gen-2014 17.06.59
 */

class GroupsImagesController extends BackendController{
    
    function postChangeFlag($image_id,$flag,$status){            
        
        $image = GroupImage::find($image_id);        
        $image->{$flag} = $status;
        $image->update();
        
        $response = array('success' => true);
        return Json::encode($response);
    }
    
    function postRemove($image_id){
        //\Utils::watch();
        $image = GroupImage::find($image_id);
        $filename = public_path()."/assets/groups/".$image->filename;
        try{
            $set_id = $image->set_id;
            File::delete($filename);
            $image->delete();
            $response = array('success' => true);
            /*\Utils::watch();
            DB::table("products")->where("id",$set_id)->update(["cnt_images" => DB::raw("cnt_images-1")]);*/
        } catch (Exception $ex) {

        }
        
        return Json::encode($response);
    }
    
    
    function postReload($set_id){
        $response = array('success' => true, 'html' => Helper::getImagesTableForSet($set_id));
        return Json::encode($response);
    }
    
    function postSaveTable($set_id){
        $languages = \Mainframe::languagesCodes();
        $image_ids = Input::get("image_ids");
        $ids = explode(",", $image_ids);
        $counter = 0;
        
        foreach ($ids as $id) {
            $counter++;
            $ds = array();
            
            foreach($languages as $lang){
                $ds["legend_".$lang] = Input::get($id."_legend_".$lang);
            }
            $ds['position'] = $counter;
            
            $image = GroupImage::find($id);            
            if($image){
                $image->setDataSource($ds);            
                $image->save();
            }
            
        }       
        
        $response = array('success' => true);
        return Json::encode($response);
    }
    
    
    
}