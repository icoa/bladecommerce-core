<?php

class NegoziandoOrdersController extends BackendController
{

    public $component_id = 29;
    public $title = 'Gestione Ordini Negoziando';
    public $page = 'negoziando_orders.index';
    public $pageheader = 'Gestione Ordini Negoziando';
    public $iconClass = 'font-columns';
    public $model = 'Order';
    public $themeLayout = 'negoziando';
    protected $user;
    protected $shop;
    protected $js = 'orders_negoziando_final_v2.js';

    protected $rules = array(
        'carrier_id' => 'required',
        'payment_id' => 'required',
        'customer_id' => 'required',
        'status' => 'required',
        'payment_status' => 'required',
        'shipping_address_id' => 'required',
        'order_date' => 'required',
    );

    protected $friendly_names = array(
        'carrier_id' => 'Metodo di spedizione',
        'payment_id' => 'Metodo di pagamento',
        'customer_id' => 'Cliente',
        'status' => 'Status ordine',
        'payment_status' => 'Status pagamento',
        'shipping_address_id' => 'Indirizzo di spedizione',
        'order_date' => 'Data ordine',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->user = AdminUser::get();
        $this->shop = AdminUser::get()->getShop();
        if(is_null($this->shop)){
            audit_error("Shop object should not be null", __METHOD__);
            audit_error($this->shop, __METHOD__);
        }
    }


    public function dashboard()
    {
        return $this->getIndex();
    }


    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Ordini Negoziando');
        $this->toFooter("js/echo/$this->js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'negoziando_orders.create';
        $this->toFooter("js/echo/$this->js");
        $this->pageheader = 'Nuovo Ordine Negoziando';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }


    public function getPreview($id)
    {
        $this->page = 'negoziando_orders.preview';
        $this->toFooter("js/echo/$this->js");
        $this->pageheader = 'Dettagli Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->expand();
        $view['obj'] = $obj;
        \AdminTpl::setData('order', $obj);
        return $this->render($view);
    }

    public function getTrash()
    {

    }

    public function getExport()
    {

    }

    public function getTable()
    {
        $model = $this->model;

        $this->user = AdminUser::get();
        $this->shop = AdminUser::get()->getShop();
        if(is_null($this->shop)){
            audit_error("Shop object should not be null", __METHOD__);
            audit_error($this->user, __METHOD__, 'User');
        }


        $shop_code = $this->shop->cd_neg;
        $shop_id = $this->shop->id;

        $pages = $model::leftJoin('customers', 'customer_id', '=', 'customers.id')
            ->where(function ($query) use ($shop_code, $shop_id) {
                $query->whereIn('orders.customer_id', function ($query) use ($shop_id) {
                    $query->select('id')->from('customers')->where('default_group_id', \Config::get('negoziando.b2b_group'))->where('shop_id', $shop_id);
                })->orwhereIn('orders.id', function ($query) use ($shop_code) {
                    $query->select('order_id')->from('order_details')->where('availability_shop_id', $shop_code);
                })->orWhere('delivery_store_id', $shop_id);
            })
            //->whereNotIn('availability_mode',['local'])
            ->whereNull('parent_id')
            ->groupBy('orders.id')
            ->select(
                'orders.id',
                'orders.reference',
                DB::raw("CONCAT_WS(' ',firstname,lastname,company) as customer_name"),
                'orders.status',
                'orders.payment',
                'orders.payment_status',
                'orders.carrier_id',
                'orders.total_order',
                'orders.created_at',
                'orders.gift_box'
            );

        $order_states = Mainframe::selectOrderStates(false);
        $payment_states = Mainframe::selectPaymentStates(false);

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('reference', function ($data) {
                $link = \URL::action($this->action("getPreview"), $data['id']);
                return "<strong><a href='$link'>{$data['reference']}</a></strong>";
            })
            ->edit_column('customer_id', function ($data) {
                $order = \Order::getObj($data['id']);
                $customer = $order->getCustomer();
                $s = ($customer) ? $customer->getName() : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('customer_name', function ($data) {
                //$order = \Order::getObj($data['id']);
                $s = $data['customer_name'];
                return $s;
            })
            ->edit_column('gift_box', function ($data) {
                $order = \Order::getObj($data['id']);
                return $order->getActionsForClerk();
            })
            ->edit_column('carrier_id', function ($data) {
                $order = \Order::getObj($data['id']);
                $customer = $order->getCarrier();
                $s = ($customer) ? $customer->name : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('status', function ($data) use ($order_states) {
                $order = \Order::getObj($data['id']);
                $status = $order->getStatus();
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title='{$status->name}'><i class='{$status->icon}'></i></span>" : "ND";

                return $s;
            })
            ->edit_column('payment_status', function ($data) use ($payment_states) {
                $order = \Order::getObj($data['id']);
                $status = $order->getPaymentStatus();
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title='{$status->name}'><i class='{$status->icon}'></i></span>" : "ND";

                return $s;
            })
            ->edit_column('total_order', function ($data) {
                return "<strong>" . \Format::currency($data['total_order'], true) . "</strong>";
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    protected function actions_default($params = [])
    {
        if(\Core::b2b()){
            $link = \Link::to('nav', 13) . '?b2b=1&splash=0';
            $actions = [
                new AdminAction('INIZIA L\'ORDINE: SCEGLI I PRODOTTI', $link . '&empty_cart=1', 'font-shopping-cart', 'btn-info', 'Apre il catalogo del sito senza carrello', 'catalog'),
                new AdminAction('Gestione clienti', '/admin/negoziando/customers', 'font-user', '', 'Visualizza la gestione clienti', 'customers'),
            ];
        }else{
            $actions = parent::actions_default(['only' => ['reload']]);
        }
        return $actions;
    }


    protected function actions_create($params = [])
    {
        $index = \URL::action($this->action("getIndex"));

        $actions = array(
            new AdminAction('Torna indietro', $index, 'font-arrow-left', 'btn-default', 'Torna indietro alla pagina principale del Componente', 'back'),
        );

        return $actions;
    }


    function postSetStatus($order_id)
    {
        $status_id = \Input::get('status_id', null);
        $order = \Input::get('order', 0);
        /** @var Order $obj */
        $obj = Order::getObj($order_id);
        $success = true;
        if ($order == 1) {
            $success = $obj->updateStatus($status_id);
            $msg = $success ? "Aggiornamento status effettuato con successo" : "Aggiornamento status non eseguito";
            if ($success) {
                \Session::set('orderLastStatus', $status_id);
                $obj->handleStatusUpdateEmail();
            }
        } else {
            $success = $obj->updatePaymentStatus($status_id);
            $msg = $success ? "Aggiornamento status pagamento effettuato con successo" : "Aggiornamento status pagamento non eseguito";
            if ($success) {
                \Session::set('orderLastPaymentStatus', $status_id);
                $obj->handlePaymentStatusUpdateEmail();
            }
        }


        return Json::encode(compact('success', 'msg'));
    }

    function postRma($id)
    {
        $response = ['success' => false, 'msg' => "Si è verificato un problema interno; contattare il servizio di assistenza"];
        $order = Order::getObj($id);
        if ($order) {
            try {
                $order->payment_status = \Config::get('negoziando.payment_status_refunded');
                $order->status = \Config::get('negoziando.order_status_refunded');
                $order->save();

                $order->handlePaymentStatusUpdateEmail();
                $response['success'] = true;
                $response['msg'] = "Il rimborso dell'ordine <b>$order->reference</b> è stato richiesto con successo";
            } catch (Exception $e) {
                $response['msg'] = $e->getMessage();
            }
        }
        return Response::json($response);
    }
}