<?php

/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 16/09/14
 * Time: 12.56
 */
class BuilderController extends BaseController
{

    function postRender($type)
    {
        $lang = \Core::getLang();
        $value = \Input::get("value","");
        switch($type){
            case 'category_ids':
                $value = explode(",",$value);
                $html = \Form::select("category_ids", \Mainframe::selectCategoriesTree(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'brand_ids':
                $value = explode(",",$value);
                $html = \Form::select("brand_ids", \Mainframe::selectBrands(false), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'collection_ids':
                $value = explode(",",$value);
                $html = \Form::select("collection_ids", \Mainframe::selectCollectionsTree(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'supplier_ids':
                $value = explode(",",$value);
                $html = \Form::select("supplier_ids", \Mainframe::selectSuppliers(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'country_ids':
                $value = explode(",",$value);
                $html = \Form::select("country_ids", \Mainframe::selectCountries(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'state_ids':
                $value = explode(",",$value);
                $html = \Form::select("state_ids", \Mainframe::selectStatesTree(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'carrier_ids':
                $value = explode(",",$value);
                $html = \Form::select("carrier_ids", \Mainframe::selectCarriers(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'payment_ids':
                $value = explode(",",$value);
                $html = \Form::select("payment_ids", \Mainframe::selectPayments(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'sku':
                $value = explode(",",$value);
                $select = '<select name="sku" multiple class="multiple span12 product-chooser">';
                $rows = \Product::whereIn("id",$value)->rows($lang)->get();
                if(!empty($rows)){
                    foreach($rows as $row){
                        $select .= "<option value=\"$row->id\">({$row->sku}) - {$row->name}</option>";
                    }
                }
                $select .= "</select>";
                $url_select = URL::action("ProductsController@getSelect");
                $html = <<<TPL
<div class="clearfix">
$select
<button type="button" class="btn btn-default pull-right clearfix" onclick="Echo.popup('$url_select');"><i class="icon-plus"></i> AGGIUNGI PRODOTTI</button>
<button type="button" class="btn btn-default pull-right clearfix mr5" id="removeProducts"><i class="icon-minus"></i> RIMUOVI SELEZIONATI</button>
</div>
TPL;

                break;
            case 'trend_ids':
                $value = explode(",",$value);
                $html = \Form::select("trend_ids", \Mainframe::selectTrends(false), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'nav_ids':
                $value = explode(",",$value);
                $html = \Form::select("nav_ids", \Mainframe::selectNavsTree(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'page_ids':
                $value = explode(",",$value);
                $html = \Form::select("page_ids", \Mainframe::selectPagesTree(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'section_ids':
                $value = explode(",",$value);
                $html = \Form::select("section_ids", \Mainframe::selectSections(false), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            case 'pricerule_ids':
                $value = explode(",",$value);
                $html = \Form::select("pricerule_ids", \Mainframe::selectPriceRules(), $value, ['class' => 'selectMultiple', 'multiple' => 'multiple', 'data-placeholder' => 'Scegli uno o più elementi']);
                break;
            default: //assuming it's a product attribute
                $obj = \Attribute::where("code",$type)->get();
                break;
        }
        $success = true;
        return Json::encode(compact("success","html"));
    }

    function postFetch($type)
    {
        $lang = \Core::getLang();
        $value = \Input::get("value","");
        $controller = \Input::get("controller","");
        $row = ["attribute" => $type, "value" => $value];
        $bh = new BuilderHelper(0,$controller);
        $success = true;
        $label = $bh->getLabel($row);
        /*$labels = [];
        switch($type){
            case 'category_ids':
                $value = explode(",",$value);
                $rows = \Category::whereIn("id",$value)->rows($lang)->get();
                if(!empty($rows)){
                    foreach($rows as $row){
                        $labels[] = $row->name;
                    }
                }
                break;

            case 'sku':
                $value = explode(",",$value);
                $rows = \Product::whereIn("id",$value)->rows($lang)->get();
                if(!empty($rows)){
                    foreach($rows as $row){
                        $labels[] = "#{$row->id} - {$row->name}";
                    }
                }
                break;
        }
        $success = true;
        $label = implode(", ",$labels);*/
        return Json::encode(compact("success","label"));
    }


    function postHtml($type){
        $id = \Input::get("id");
        $controller = \Input::get("controller");
        $fieldName = \Input::get("fieldName");
        $bh = new BuilderHelper(0,$controller,$fieldName);
        $node = $bh->getNodeByString($id);

        $bh->setAllTypes($type);

        \Utils::log($node,"NODE");

        $_type = $bh->getType();
        $_basetype = $bh->getBasetype();
        $_fulltype = $bh->getFulltype();


        switch($type){
            case 'cart|rule_condition_combine':
            case 'product|rule_condition_combine':
            case 'site|rule_condition_combine':
            case 'general|rule_condition_combine':
            case 'rule_condition_product_combine':
            case 'rule_condition_cart_combine':
            case 'cart|rule_condition_product_found':
                $row = ["type" => $type, "aggregator" => "all", "value" => 1, "conditions" => []];
                break;
            case 'cart|rule_condition_product_subselect':
                $row = ["type" => $type, "attribute" => "qty", "operator" => "==", "aggregator" => "all", "value" => "", "conditions" => []];
                break;
            default: // product attribute
                if($_basetype == 'product' OR $_basetype=='attribute'){
                    $row = ["type" => "$_basetype|rule_condition_product", "attribute" => $_type, "operator" => "==", "value" => ""];
                }
                if($_basetype == 'cart'){
                    $row = ["type" => "$_basetype|rule_condition_cart", "attribute" => $_type, "operator" => "==", "value" => ""];
                }
                if($_basetype == 'customer'){
                    $row = ["type" => "customer|rule_condition_cart", "attribute" => $_type, "operator" => "==", "value" => ""];
                }
                if($_basetype == 'site'){
                    $row = ["type" => "site|rule_condition_scope", "attribute" => $_type, "operator" => "==", "value" => ""];
                }
                if($_basetype == 'general'){
                    $row = ["type" => "general|rule_condition_self", "attribute" => $_type, "operator" => "==", "value" => ""];
                }
                if($_basetype=='filter'){
                    $row = ["type" => "$_basetype|rule_condition_scope", "attribute" => $_type, "operator" => "==", "value" => ""];
                }
                if($_basetype=='attr'){
                    $row = ["type" => "$_basetype|rule_condition_self", "attribute" => $_type, "operator" => "==", "value" => ""];
                }

                break;
        }

        //\Utils::log($row,"FETCHED ROW");
        $tokens = explode("--",$node);
        $parent_id = $tokens[count($tokens) - 2];
        $last_id = last($tokens);
        $html = $bh->renderRule($row,$parent_id,$node);
        $tokens[count($tokens) - 1] = $last_id + 1;
        $key = implode("--",$tokens);

        $success = true;
        return Json::encode(compact("success","html","key"));
    }


    function postNavtarget($type){
        $html = \LinkHelper::getHtmlInputByType($type, \Input::get("field"), \Input::get("rel"), \Input::get("mode"));
        $success = true;
        return Json::encode(compact("success","html"));
    }


    function postNavgenerate($type){
        $field = \Input::get("field","link");
        $html = ($type == 'modifier') ? \LinkHelper::getModifier($field) : \LinkHelper::getFilter($field);
        $success = true;
        return Json::encode(compact("success","html"));
    }


    function getSelect($type){
        $data = \Mainframe::selectNavsTarget($type,false);
        $result = [];
        foreach($data as $id => $text){
            $result[] = ['id' => $id, 'text' => $text];
        }
        return Json::encode($result);
    }

    function getObtain($type){
        $data = \Mainframe::selectNavsTarget($type,false);
        $ids = \Input::get("ids");
        $result = [];
        foreach($ids as $id){
            if(isset($data[$id])){
                $result[] = ['id' => $id, 'text' => $data[$id]];
            }
        }
        return Json::encode($result);
    }




}