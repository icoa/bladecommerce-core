<?php

use services\Repositories\CartRepository;

class CartsController extends BackendController
{

    public $component_id = 74;
    public $title = 'Gestione Carrelli';
    public $page = 'carts.index';
    public $pageheader = 'Gestione Carrelli';
    public $iconClass = 'font-columns';
    public $model = 'Cart';

    /**
     * @var CartRepository
     */
    protected $cartRepository;

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->cartRepository = app(CartRepository::class);
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_create($model)
    {

    }

    function _prepare()
    {

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Carrelli');
        $this->toFooter("js/echo/carts.js?v=2");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }


    public function getEdit($id)
    {
        return $this->getPreview($id);
    }


    public function getPreview($id)
    {
        $this->page = 'carts.preview';
        $this->toFooter("js/echo/carts.js");
        $this->pageheader = 'Dettagli Carrello';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();

        $obj = Cart::getObj($id);
        $obj->expand();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        $model = $this->model;

        \Utils::watch();

        $pages = $model::groupBy('cart.id')
            ->leftJoin('orders', 'cart.id', '=', 'orders.cart_id')
            ->leftJoin('customers', 'cart.customer_id', '=', 'customers.id')
            ->select(
                'cart.id',
                'reference',
                DB::raw('customers.name as customer'),
                DB::raw('orders.id as order_id'),
                'cart.payment_id',
                'cart.carrier_id',
                'cart.customer_id',
                'cart.origin',
                'cart.ip',
                'cart.availability_mode',
                'cart.campaign_id',
                'cart.created_at',
                'cart.updated_at'
            );

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('reference', function ($data) {
                if ($data['order_id'] > 0) {
                    $link = \URL::action("OrdersController@getPreview", $data['order_id']);
                    return "<a href='$link'><span class='label label-success'>{$data['reference']}</span></a>";
                } else {
                    return "<strong>N.D.</strong>";
                }

            })
            ->edit_column('customer', function ($data) {
                $cart = \Cart::getObj($data['id']);
                $customer = $cart->getCustomer();
                if ($customer) {
                    $link = \URL::action("CustomersController@getEdit", $customer->id);
                    return "<strong><a href='$link'>{$customer->name}</a></strong>";
                } else {
                    return "<strong>N.D.</strong>";
                }
            })
            ->edit_column('carrier_id', function ($data) {
                $order = \Cart::getObj($data['id']);
                $obj = $order->getCarrier();
                $s = ($obj) ? $obj->name : "N.D.";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('payment_id', function ($data) {
                $order = \Cart::getObj($data['id']);
                $obj = $order->getPayment();
                $s = ($obj) ? $obj->name : "N.D.";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('campaign_id', function ($data) {
                $order = \Cart::getObj($data['id']);
                $obj = $order->getCampaign();
                $s = '';
                if ($obj) {
                    $src = \AdminTpl::img("images/flags/{$obj->lang_id}.jpg");
                    $s = "<img src='$src' /> $obj->name";
                }
                return $s;
            })
            ->add_column('quantity', function ($data) {
                $cart = \Cart::getObj($data['id']);
                $c = $cart->getCount();
                return "<span class='label label-info'>$c</span>";
            })
            ->add_column('total', function ($data) {
                $cart = \Cart::getObj($data['id']);
                $c = \Format::currency($cart->getTotal(), true);
                return "<strong>$c</strong>";
            })
            ->add_column('actions', function ($data) {
                $link = \URL::action($this->action("getPreview"), $data['id']);
                return "<a class='btn btn-default' href='$link'>DETTAGLI</a>";
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }


    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        $export = ($this->action("getExport", TRUE));
        $user = Sentry::getUser();
        if ($user->hasPermission('export')) {
            $actions[] = new AdminAction('Esporta CSV', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');
        }
        return $actions;
    }

    protected function actions_create($params = [])
    {
        $actions = parent::actions_create(['only' => ['back']]);
        return $actions;
    }


    function getExport()
    {
        $this->page = 'carts.export';
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
        }
        $this->pageheader = 'Esporta carrelli abbandonati';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('export');
        $view = array();
        return $this->render($view);
    }


    function getDownload()
    {
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
            $view = array();
            return $this->render($view);
        }
        \Utils::watch();
        ini_set('max_execution_time', 600);

        $from = Input::get('from');
        $to = Input::get('to');

        if ($from == '')
            $from = null;

        if ($to == '')
            $to = null;

        $filePath = $this->cartRepository->getAbandonedCarts($from);

        $filename = "abandoned_carts_" . time() . ".csv";

        return Response::download($filePath, $filename, ['content-type' => 'text/cvs']);
    }


}