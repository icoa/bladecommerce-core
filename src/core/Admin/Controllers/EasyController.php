<?php

class EasyController extends BackendController
{


    public $title = 'Interfaccia di carico/scarico';
    public $page = 'easy.index';
    public $pageheader = 'Interfaccia di carico/scarico';
    public $iconClass = 'font-columns';

    function __construct(){
        $this->themeLayout = 'easy';
        parent::__construct();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $view = array();
        return $this->render($view);
    }

    public function getScarico()
    {
        $this->page = 'easy.scarico';
        $view = array();
        return $this->render($view);
    }

    public function getCarico()
    {
        $this->page = 'easy.carico';
        $view = array();
        return $this->render($view);
    }

    public function getStorico()
    {
        $this->page = 'easy.storico';
        $view = array();
        return $this->render($view);
    }

    protected function _assets()
    {

        $this->toFooter('easy/bower_components/jquery/dist/jquery.min.js');
        $this->toFooter('easy/bower_components/bootstrap/dist/js/bootstrap.min.js');
        $this->toFooter('easy/bower_components/metisMenu/dist/metisMenu.min.js');
        $this->toFooter('easy/js/sb-admin-2.js');

        $this->toHead('easy/bower_components/bootstrap/dist/css/bootstrap.min.css');
        $this->toHead('easy/bower_components/metisMenu/dist/metisMenu.min.css');
        $this->toHead('easy/bower_components/font-awesome/css/font-awesome.min.css');
        $this->toHead('easy/css/sb-admin-2.css');

    }


    protected function _widgets_default()
    {

    }

    protected function beforeRender()
    {

    }


}