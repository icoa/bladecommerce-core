<?php

class NavsController extends MultiLangController
{

    public $component_id = 61;
    public $title = 'Gestione Componenti di navigazione';
    public $page = 'navs.index';
    public $pageheader = 'Gestione Componenti di navigazione';
    public $iconClass = 'font-columns';
    public $model = 'Nav';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:navs_lang,slug,nav_id'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }
        \Utils::log(count($_POST), 'COUNT POST');
        \Utils::log($_POST, 'PRE POST');

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            $source = $_POST["name_" . $lang];
            $target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }
        }

        if($_POST["navtype_id"] == 5){
            $_POST["minprice"] = (float)$_POST["minprice"];
            $_POST["maxprice"] = (float)$_POST["maxprice"];
        }else{
            $_POST["minprice"] = $_POST["maxprice"] = $_POST["currency_id"] = null;
        }
        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Componenti di navigazione');
        $this->toFooter("js/echo/navs.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'navs.create';
        $this->pageheader = 'Nuovo Componente di navigazione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'navs.create';
        $this->pageheader = 'Nuovo Componente di navigazione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }


    public function getEdit($id)
    {
        $this->page = 'navs.create';
        $this->pageheader = 'Modifica Componente di navigazione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('navs_lang', 'navs.id', '=', 'navs_lang.nav_id')
            ->where('navs_lang.lang_id', $lang_id)
            ->leftJoin('nav_types as S', function ($join) use ($lang_id) {
                $join->on('navs.navtype_id', '=', 'S.id');
            })
            ->groupBy('navs.id')
            ->select('navs.id', 'navs_lang.name', 'navs_lang.slug', 'navs.navtype_id', 'navs_lang.published', 'ucode', 'shortcut', 'navs_lang.ldesc', 'navs_lang.lang_id', 'S.name as type');

        return \Datatables::of($pages)
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('navtype_id', function ($data) {
                $s = $data['type'];
                return "<strong>{$s}</strong>";
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicato</span>' : '<span class="label label-important">Sospeso</span>';
            })
            ->remove_column('ldesc')
            ->remove_column('section')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }



    public function getOverride()
    {
        $this->toFooter('js/plugins/jquery.nestable.js');
        $this->toFooter('js/echo/override.js');
        $this->page = 'navs.override';
        $this->toolbar('create');
        $this->pageheader = 'Gestione Override template';
        $this->addBreadcrumb($this->pageheader);
        $view = [];
        return $this->render($view);
    }



}