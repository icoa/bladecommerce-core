<?php

use Core\Condition\RuleResolver;

class AmazonProfilesController extends BackendController
{

    public $component_id = 138;
    public $title = 'Gestione Profili Amazon';
    public $page = 'amazonprofiles.index';
    public $pageheader = 'Gestione Profili Amazon';
    public $iconClass = 'font-columns';
    public $model = 'services\Amazon\Models\AmazonProfile';
    protected $rules = array('name' => 'required',);
    protected $friendly_names = array('name' => 'Denominazione regola',);


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Profili Amazon');
        $this->toFooter("js/echo/amazonprofiles.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/amazonprofiles.js");
        $this->page = 'amazonprofiles.trash';
        $this->pageheader = 'Cestino Profili Amazon';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/amazonprofiles.js");
        $this->toFooter("js/echo/builder.js");
        $this->page = 'amazonprofiles.create';
        $this->pageheader = 'Nuovo Profilo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/amazonprofiles.js");
        $this->toFooter("js/echo/builder.js");
        $this->page = 'amazonprofiles.create';
        $this->pageheader = 'Modifica Profilo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getPreview($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $view = array('obj' => $obj);
        return Theme::scope("amazonprofiles.preview", $view)->content();
    }

    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::groupBy("amazon_profiles.id")
            ->leftJoin('amazon_profiles_products', 'amazon_profiles.id', '=', 'amazon_profiles_products.amazon_profile_id')
            ->select('amazon_profiles.id', 'name', 'priority', 'price_rules.active', DB::raw("count(distinct amazon_profiles_products.product_id) as products"), 'price_rules.created_at', 'price_rules.sdesc');


        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('products', function ($data) {
                return "<b>" . $data['products'] . "</b>";
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong><a href='$link'>{$data['name']}</a></strong>$add";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("price_rules.id")
            ->leftJoin('products_specific_prices', 'price_rules.id', '=', 'products_specific_prices.price_rule_id')
            ->select('price_rules.id', 'name', 'priority', 'price_rules.reduction_type', 'price_rules.active', DB::raw("count(products_specific_prices.product_id) as products"), 'price_rules.date_from', 'price_rules.date_to', 'price_rules.created_at', 'price_rules.deleted_at', 'price_rules.sdesc', 'price_rules_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('date_from', function ($data) {
                return \Format::datetime($data['date_from']);
            })
            ->edit_column('date_to', function ($data) {
                return \Format::datetime($data['date_to']);
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->edit_column('reduction_type', function ($data) {
                return PriceRulesController::getRuleExplained($data['id']);
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);

        $this->handleRules($model);
        $this->applyRule($model);

    }

    function _after_create($model)
    {
        parent::_after_create($model);


        $this->handleRules($model);
        $this->applyRule($model);

    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        $conditions = \Input::get("rule")["conditions"];
        $rr = new RuleResolver();
        $_POST['conditions'] = $rr->getSerializable($conditions);
        \Utils::log($_POST['conditions'], "serialize conditions");
        $model = $this->model;

        \Input::replace($_POST);

    }


    private function handleRules($model)
    {
        $bindrule = (int)\Input::get("bindrule", 1);
        if ($bindrule == 0) return;
        \Utils::watch();
        $price_rule_id = $model->id;




        return;

    }


    private function applyRule($model)
    {

        $bindrule = (int)\Input::get("bindrule", 1);
        $mode = ($bindrule == 0) ? 'update' : 'replace';
        \Utils::watch();


        $price_rule_id = $model->id;

        if ($mode == 'replace') {
            // delete all specific prices
            //\ProductPrice::where("price_rule_id", $price_rule_id)->delete();

            $rules = $model->getRuleExceptions();

            $rr = new RuleResolver();
            $rr->setRules($model->conditions);
            $product_ids = $rr->bindProducts();

            $real_ids = \Product::where('is_out_of_production',0)->whereIn('id',$product_ids)->lists('id');

            $position = 0;
            if (count($real_ids) > 0) {
                foreach ($real_ids as $product_id) {

                    $position++;

                    //DB::beginTransaction();

                    DB::transaction(function() use ($rules,$product_id,$price_rule_id,$model,$position){
                        foreach ($rules as $rule) {
                            $data = [
                              'product_id' => $product_id,
                              'price_rule_id' => $price_rule_id,
                              'customer_group_id' => $rule->customer_group_id,
                              'currency_id' => $rule->currency_id,
                              'country_id' => $rule->country_id,

                              'date_from' => $model->date_from,
                              'date_to' => $model->date_to,
                              'from_quantity' => $model->from_quantity,
                              'position' => ($model->priority * 10) + ($position),
                              'reduction_type' => $model->reduction_type,
                              'reduction_value' => $model->reduction_value,
                              'reduction_currency' => $model->reduction_currency,
                              'reduction_tax' => $model->reduction_tax,
                              'reduction_target' => $model->reduction_target,
                              'stop_other_rules' => $model->stop_other_rules,
                              'free_shipping' => $model->free_shipping,
                              'active' => $model->active,
                            ];
                            DB::table('products_specific_prices')->insert($data);

                            try{
                                $product = \Product::find($product_id);
                                $product->uncache();
                            }catch(\Exception $e){

                            }
                        }

                    });

                }
            }
        }

        if ($mode == 'update') {
            $data = [
                'date_from' => $model->date_from,
                'date_to' => $model->date_to,
                'from_quantity' => $model->from_quantity,
                'reduction_type' => $model->reduction_type,
                'reduction_value' => $model->reduction_value,
                'reduction_currency' => $model->reduction_currency,
                'reduction_tax' => $model->reduction_tax,
                'reduction_target' => $model->reduction_target,
                'stop_other_rules' => $model->stop_other_rules,
                'free_shipping' => $model->free_shipping,
                'active' => $model->active,
            ];
            \ProductPrice::where("price_rule_id", $price_rule_id)->update($data);

        }

        \ProductHelper::forgetPriceRulesCache($price_rule_id);


    }



    /*public function postFlagSingle($id, $lang, $status) {
        \ProductHelper::forgetPriceRulesCache($id);
        \ProductPrice::where("price_rule_id", $id)->update(['active' => $status]);
        return parent::postFlagSingle($id, $lang, $status);
    }


    public function postFlagMulti($status) {
        $ids = $_POST['ids'];
        if(is_array($ids) AND count($ids) > 0){
            foreach($ids as $id){
                \ProductPrice::where("price_rule_id", $id)->update(['active' => $status]);
                \ProductHelper::forgetPriceRulesCache($id);
            }
        }
        return parent::postFlagMulti($status);
    }*/










}
