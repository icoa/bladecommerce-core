<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

use HtmlObject\Element;

class MenuTypesController extends BackendController
{

    public $component_id = 59;
    public $title = 'Tipi Menù';
    public $page = 'menu_types.index';
    public $pageheader = 'Tipi Menù';
    public $iconClass = 'font-columns';
    public $model = 'MenuType';
    protected $rules = array(
        'name' => 'required',
    );
    protected $friendly_names = array(
        'name' => 'Denominazione',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco tipi menù');
        $this->toFooter("js/echo/menu_types.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/menu_types.js");
        $this->page = 'menu_types.create';
        $this->pageheader = 'Nuovo tipo menù';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/menu_types.js");
        $this->page = 'menu_types.create';
        $this->pageheader = 'Modifica tipo menù';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getManage($id)
    {
        $this->toHead("css/menu-builder.css");
        $this->toFooter('js/plugins/jquery.nestable.js');
        $this->toFooter('js/echo/menu-builder.js');
        $this->page = 'menu_types.manage';
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $this->pageheader = 'Gestione menù: '.$obj->name;
        $this->addBreadcrumb($this->pageheader);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTrash()
    {
        $this->addBreadcrumb('Cestino voci di menù');
        $this->toFooter("js/echo/menus.js");
        $view = array();
        $this->page = 'menu_types.trash';
        $this->toolbar('trash');
        return $this->render($view);
    }




    public function postSavemenu($id)
    {
        \Utils::watch();
        $data = \Input::get("data");
        \Utils::log($data);

        if(!empty($data)){
            $this->updateMenuSchema($data);
        }

        $success = true;
        $msg = "Posizioni menù salvate con successo";
        return Json::encode(compact("success","msg"));
    }


    private function updateMenuSchema($nodes, $parent_id = 0){

        foreach($nodes as $key => $node){
            $position = $key + 1;
            $id = $node['id'];
            \Menu::where("id",$id)->update(['position' => $position, 'is_temp' => 0, 'parent_id' => $parent_id]);
            if(isset($node['children'])){
                $this->updateMenuSchema($node['children'], $id);
            }
        }
    }


    static function renderMenuSchema($menutype_id, $parent_id = 0){
        //\Utils::watch();
        $lang = \Core::getLang();
        $rows = \Menu::rows($lang)->where("menutype_id",$menutype_id)->where("parent_id",$parent_id)->orderBy("position")->get();
        $ol = "";
        if(count($rows) > 0){
            $ol =  Element::ol("",['class' => 'dd-list']);
            foreach($rows as $row){
                $typename = \LinkHelper::getTypename($row->link_type);
                $tpl = <<<TPL
<div class="dd-handle dd3-handle">Drag</div><div class="dd3-content"><span class="itemname">$row->name</span><span class="dd-toolbar"><em>$typename</em><a class="dd-action" data-action="edit" href="javascript:;"><i class="icon-edit"></i></a><a class="dd-action" data-action="remove" href="javascript:;"><i class="icon-remove"></i></a></span></div>
TPL;

                $li = Element::li($tpl,['class' => 'dd-item dd3-item', 'data-id' => $row->id, 'id' => "node-{$row->id}"]);
                $li->nest( self::renderMenuSchema($menutype_id, $row->id) );
                $ol->nest($li);
                //$li = \LinkHelper::getHtmlTemplate($row->id,$row->name,$typename);
            }
        }else{
            if($parent_id == 0){
                $ol =  Element::ol("",['class' => 'dd-list']);
            }
        }
        return (string)$ol;
    }


    public function getTable()
    {

        $model = $this->model;

        $lang_id = \Core::getLang();


        $pages = $model::leftJoin("menus as C", "menu_types.id", "=", "C.menutype_id")->whereNull('C.deleted_at')->groupBy("menu_types.id")->select('menu_types.id', 'menu_types.name', DB::raw("COUNT(menutype_id) as total"), "menu_types.description");

        return \Datatables::of($pages)
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $link_manage = \URL::action($this->action("getManage"), $data['id']);
                $btn = "<a href='$link_manage' class='pull-right btn btn-mini btn-primary'>Gestione voce di menù</a>";
                $desc = ($data['description'] == '') ? '' : "<br><em>{$data['description']}</em>";
                return "$btn<strong><a href='$link'>{$data['name']}</a></strong>$desc";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->remove_column('description')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $_POST['params'] = \ModuleHelper::setParams($_POST['params']);


    }

    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">    
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = \URL::action($this->action("getIndex"));

                $actions = array(
                    new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva e chiudi', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );

                break;

            case 'trash':
                $index = $this->action("getIndex", TRUE);
                $restore = \URL::action("MenusController@postRestoreMulti", 0);
                $destroy = \URL::action("MenusController@postDestroyMulti");
                $actions = array(
                    new AdminAction('Ripristina', $restore, 'font-undo', 'btn-success confirm-action-multi', 'Recupera i record selezionati'),
                    new AdminAction('Elimina', $destroy, 'font-minus-sign', 'btn-danger confirm-action-multi', 'Elimina definitivamente i record selezionati'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );

                break;

            default:
                $create = ($this->action("getCreate", TRUE));
                $trash = ($this->action("getTrash", TRUE));
                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $actions = array(
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella uno o più record selezionati'),
                    new AdminAction('Cestino', $trash, 'font-trash', '', 'Mostra tutti gli elementi eliminati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }


}
