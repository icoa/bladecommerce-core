<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 23-lug-2013 15.57.17
 */

use Carbon\Carbon;

class ImportController extends BaseController
{

    private function _style()
    {
        $style = <<<STYLE
<style>
    body, table, th, td {
        background-color:#000;
        font-family: Courier;
        font-size:12px;
        color:white;
        /*background-color:#fff;
        font-family: Courier;
        font-size:12px;
        color:#000;*/
    }

    .error{
        color:red;
    }

    .success{
        color:lime;
    }

    .message{
        color:cyan;
    }

    .warning{
        color:yellow;
    }

    .st1{
        color:magenta;
    }

    .st2{
        color:orange;
    }
</style>          
STYLE;
        echo $style;
    }


    private function io_float($v)
    {
        $v = str_replace(',', '.', $v);
        return round($v, 3);
    }




    private function _301_parse($segment)
    {
        $tokens = explode("-", $segment);
        $tokens_count = count($tokens);
        $models = [];
        if ($tokens_count > 1) {
            $last_token = $tokens[$tokens_count - 1];
            if (preg_match_all('/([A-Z]+)([0-9]+)/i', $last_token, $matches, PREG_SET_ORDER)) {


                foreach ($matches as $match) {
                    $obj = new stdClass();
                    $obj->id = $match[2];
                    $obj->letter = $match[1];
                    $obj->key = $match[0];

                    switch ($obj->letter) {
                        case 'B':
                            $obj->model = 'brand';
                            break;
                        case 'C':
                            $obj->model = 'category';
                            break;
                        case 'S':
                            $obj->model = 'style';
                            $obj->id = isset($this->_DATA['style'][$obj->id]) ? $this->_DATA['style'][$obj->id] : 0;
                            break;
                        case 'G':
                            $obj->model = 'gender';
                            $obj->id = isset($this->_DATA['gender'][$obj->id]) ? $this->_DATA['gender'][$obj->id] : 0;
                            break;
                        case 'P':
                            $obj->model = 'product';
                            if ($obj->id > 10800) {
                                $obj->id = isset($this->_DATA['old'][$obj->id]) ? $this->_DATA['old'][$obj->id] : 0;
                            }
                            break;
                    }
                    $models[] = $obj;
                }

            }
        }
        return $models;
    }



    private function brand_cache()
    {
        $ids = Brand::get();
        foreach ($ids as $id) {
            $id->uncache();
        }
        Brand::initCache();
        AttributeOption::initCache();
        return;
        $ids = Category::get();
        foreach ($ids as $id) {
            $id->uncache();
        }
        Category::initCache();
        Product::initCache();
    }


    private function import_newsletter()
    {
        //adjust email
        $query = "UPDATE import_newsletter SET email=LOWER(TRIM(email))";
        DB::statement($query);

        //find duplicate records
        $query = "SELECT *, COUNT(*) as count
FROM import_newsletter
GROUP BY email
HAVING count > 1
ORDER BY data DESC";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $query = "DELETE FROM import_newsletter WHERE id='$row->id'";
            DB::statement($query);
        }

        $current = DB::table('newsletters')->lists('email');
        $customers = [];
        $data = Customer::get();
        foreach ($data as $c) {
            $customers[$c->email] = $c->id;
        }

        $rows = DB::table('import_newsletter')->get();

        foreach ($rows as $row) {
            if (!in_array($row->email, $current)) {

                $customer_id = (isset($customers[$row->email])) ? $customers[$row->email] : 0;


                $data = [
                    'customer_id' => $customer_id,
                    'email' => $row->email,
                    'lang_id' => $row->lingua,
                    'created_at' => $row->data,
                    'updated_at' => $row->data,
                    'user_ip' => '127.0.0.1',
                    'created_by' => 6,
                    'updated_by' => 6,
                ];

                DB::table('newsletters')->insert($data);

                Writer::nl($data);

                if ($customer_id > 0) {
                    $update_data = [
                        'newsletter_date_add' => $row->data,
                        'ip_registration_newsletter' => '127.0.0.1',
                        'newsletter' => 1,
                    ];

                    Writer::nl($update_data, 'warning');
                    DB::table('customers')->where('id', $customer_id)->update($update_data);
                }
            }
        }
    }


    private function _301_fix_by_code()
    {

        $data = [];
        $products = DB::select("select `id`, `sku`, old_id from `products` where `products`.`deleted_at` is null and `id` > '10800' and (old_id = 0 OR old_id is null)");
        foreach ($products as $p) {
            $data[\Str::upper(trim($p->sku))] = $p->id;
        }
        $queries = [];

        $rows = DB::table('import_301')->where('target', '/')->where('source', 'like', '%-P%')->get();
        foreach ($rows as $row) {
            $tokens = explode("-", $row->source);
            $sku = \Str::upper($tokens[count($tokens) - 2]);
            Writer::nl($row->source);
            Writer::nl($sku, 'warning');
            if (isset($data[$sku])) {
                Writer::nl("FOUND: " . $data[$sku], 'success');
                $old_id = str_replace(['P', '.htm'], '', last($tokens));
                $queries[] = "UPDATE products SET old_id={$old_id} WHERE id={$data[$sku]};";
            }
        }
        Writer::nl(implode('<br>', $queries));
    }


    private function _301_fix_lang_flag()
    {
        $products = DB::select("select `product_id` from `products_lang` where lang_id='it' AND published=1 AND product_id in (select product_id from products_lang where lang_id='en' and published=0)");
        $data = [];
        foreach ($products as $p) {
            $data[] = $p->product_id;
        }
        $list = implode(",", $data);
        echo "UPDATE products_lang SET published=1 WHERE lang_id='en' AND product_id IN ($list);";
        echo '<br>';
        echo "UPDATE cache_products SET published_en=1 WHERE id IN ($list);";
    }


    private function _301_adjust_similar()
    {
        $rows = DB::table('redirect_links')
            ->where('source', 'like', '%-P%')
            ->where('target', 'like', '%-P%')
            //->take(100)
            ->get();

        $queries = [];

        foreach ($rows as $row) {


            preg_match("#-P([0-9]*)\.htm#", $row->source, $sources);
            preg_match("#-P([0-9]*)\.htm#", $row->target, $targets);
            $source_id = count($sources) == 2 ? $sources[1] : 0;
            $target_id = count($targets) == 2 ? $targets[1] : 0;
            if ($target_id > 0 AND $source_id == $target_id) {
                $query = "DELETE FROM redirect_links WHERE id=$row->id;";
                $queries[] = $query;

                Writer::nl("$row->source => $row->target");
            }

            //Writer::nl($sources);
            //Writer::nl($targets);
        }

        foreach ($queries as $q) {
            Writer::nl($q, 'warning');
        }
    }


    private function _301_adjust_product()
    {

        \Utils::watch();

        $categories_it = [];
        $categories_en = [];
        $brands_it = [];
        $brands_en = [];

        $rows = DB::table('categories_lang')->get();

        foreach ($rows as $row) {
            if ($row->lang_id == 'it') {
                $categories_it[$row->slug] = $row->category_id;
                if ($row->singular != '') {
                    $categories_it[\Str::slug($row->singular)] = $row->category_id;
                }
            }
            if ($row->lang_id == 'en') {
                $categories_en[$row->slug] = $row->category_id;
                if ($row->singular != '') {
                    $categories_en[\Str::slug($row->singular)] = $row->category_id;
                }
            }
        }

        $rows = DB::table('brands_lang')->get();

        foreach ($rows as $row) {
            if ($row->lang_id == 'it') {
                $brands_it[$row->slug] = $row->brand_id;
            }
            if ($row->lang_id == 'en') {
                $brands_en[$row->slug] = $row->brand_id;
            }
        }


        $rows = DB::table('redirect_links')
            ->where('source', 'like', '%-P%')
            ->where('target', 'like', '%-A1.htm%')
            /*->where('source', 'like', '%/en/%')
            ->take(50)*/
            ->get();

        $queries = [];

        $mdate = \Format::now();

        foreach ($rows as $row) {
            $lang = \Str::contains($row->target, '/en') ? 'en' : 'it';
            $slugs = explode("-P", $row->source);
            $slug = $slugs[0];
            $words = explode(' ', str_replace(['---', '--', '-'], ' ', $slug));

            $newTarget = '';

            $category_id = 0;
            $brand_id = 0;

            \Utils::log($row->source, "PARSING URL");

            if ($lang == 'it') {
                $categories = $categories_it;
                $brands = $brands_it;
            } else {
                $categories = $categories_en;
                $brands = $brands_en;
            }

            foreach ($categories as $slug => $id) {
                if (\Str::contains($row->source, $slug)) {
                    $category_id = $id;
                }
            }

            foreach ($brands as $slug => $id) {
                if (\Str::contains($row->source, $slug)) {
                    $brand_id = $id;
                }
            }

            if ($brand_id OR $category_id) {
                $link = null;
                if ($category_id) {
                    $link = \Link::create('category', $category_id, $lang);
                }
                if ($brand_id) {
                    if ($link) {
                        $link->addModifier('brand', $brand_id);
                    } else {
                        $link = \Link::create('brand', $brand_id, $lang);
                    }
                }
                $newTarget = $link->getLink();
            }

            if ($newTarget != '') {
                $query = "UPDATE redirect_links SET target='$newTarget' WHERE id=$row->id;";
                $queries[] = $query;
                $query = "UPDATE crawl_errors SET redirect='$newTarget',mdate='$mdate' WHERE url='$row->source';";
                $queries[] = $query;
            }


            Writer::nl("$row->source => $newTarget");

        }

        foreach ($queries as $q) {
            Writer::nl($q, 'warning');
        }
    }


    private function _301_test_status()
    {
        ini_set("max_execution_time", 0);
        $mode = 'live';
        // Create a client and provide a base URL

        if ($mode == 'debug') {
            $client = new GuzzleHttp\Client(['base_url' => 'http://kronoshop.l4']);
            $rows = DB::table('crawl_errors')
                ->where('response', null)
                ->whereNull('status')
                ->take(50)
                ->orderBy('url')
                ->get();
        } else {
            $page = 2;
            $skip = ($page - 1) * 50;
            $client = new GuzzleHttp\Client(['base_url' => 'https://www.kronoshop.com']);
            $rows = DB::table('crawl_errors')
                ->where('response', null)
                ->take(50)
                ->skip($skip)
                ->orderBy('url')
                ->get();
        }

        echo <<<TABLE
<table cellpadding=2 cellspacing=2>
<thead>
<tr>
<th>Source</th>
<th>Status</th>
<th>Target</th>
</tr>
</thead>
</tbody>
TABLE;


        foreach ($rows as $row) {
            // Send the request and get the response
            $status = 404;
            $url = null;
            try {
                $response = $client->head($row->url, ['allow_redirects' => false]);
                $status = $response->getStatusCode();
                if ($status == 301) {
                    $response = $client->head($row->url);
                    $url = $response->getEffectiveUrl();
                }
            } catch (\Exception $e) {
                //Writer::nl($e->getMessage());
            }
            //Writer::nl("$row->url [$status] ==>> $url");

            $source = $row->url;
            $target = $url;

            $class = 'error';
            switch ($status) {
                case 200:
                    $class = 'success';
                    break;
                case 301:
                    $class = 'warning';
                    break;
            }

            echo <<<ROW
<tr>
<td>$source</td>
<td class="$class">$status</td>
<td>$target</td>
</tr>
ROW;
            if ($mode != 'live') DB::table('crawl_errors')->where('id', $row->id)->update(['status' => $status, 'redirect' => $target]);

        }

        echo '</tbody></table>';
    }


    private function _301_redirect_report()
    {
        $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
<thead>
<tr>
<th>url</th>
<th>response</th>
<th>rilevato</th>
<th>categoria</th>
<th>piattaforma</th>
<th>last_scan</th>
<th>mdate</th>
<th>status</th>
<th>redirect</th>
</tr>
</thead>
<tbody>
TABLE;
        $rows = DB::table('crawl_errors')
            ->orderBy('url')
            ->get();

        foreach ($rows as $row) {

            $url = "<a href='https://www.kronoshop.com$row->url'>$row->url</a>";
            $redirect = $row->redirect != '' ? "<a href='https://www.kronoshop.com$row->redirect'>$row->redirect</a>" : '';
            $status_color = 'yellow';
            switch ($row->status) {
                case 200:
                    $status_color = 'green';
                    break;
                case 404:
                    $status_color = 'red';
                    break;
            }


            $table .= <<<TABLE
<tr>
<td>$url</td>
<td align="center">$row->response</td>
<td align="center">$row->rilevato</td>
<td align="center">$row->categoria</td>
<td align="center">$row->piattaforma</td>
<td align="center">$row->last_scan</td>
<td align="center">$row->mdate</td>
<td align="center" style="background-color:$status_color">$row->status</td>
<td>$redirect</td>
</tr>
TABLE;
        }

        $table .= '</tbody></table>';

        echo $table;

    }


    private function _gm_import()
    {
        $rows = DB::table('google_categories_import')->get();

        foreach ($rows as $row) {
            $names = [];
            for ($i = 1, $max = 7; $i <= $max; $i++) {
                if (trim($row->{"field" . $i}) != '') {
                    $names[] = $row->{"field" . $i};
                }
            }
            $data = [
                'name' => implode(' > ', $names),
                'locale' => $row->locale
            ];
            DB::table('google_categories_schema')->insert($data);
        }
    }


    private function _seo_check()
    {
        ini_set('max_execution_time', 0);
        $max = 1500;
        $counter = 0;
        $file = storage_path("xml/sitemap/it_categories.xml");
        $xml = simplexml_load_file($file);

        $cache = DB::table('export_seo')->lists('url');


        foreach ($xml->url as $val) {
            if ($counter < $max) {
                echo $val->loc . '<br>';
                if (!in_array($val->loc, $cache)) {
                    try {
                        $url = str_replace('https://www.kronoshop.com', 'http://kronoshop.l4', $val->loc);
                        $meta = get_meta_tags($url);
                        Writer::nl($meta);

                        $data = [
                            'url' => $val->loc,
                            'tr_title' => $meta['twitter:title'],
                            'tr_description' => $meta['twitter:description'],
                            'title' => $meta['title'],
                            'description' => $meta['description'],
                            'ln_title' => \Str::length($meta['twitter:title']),
                            'ln_description' => \Str::length($meta['twitter:description']),
                            'canonical' => $meta['canonical'],
                            'keywords' => $meta['keywords'],
                            'h1' => $meta['h1'],
                            'language' => $meta['language'],
                        ];
                        DB::table('export_seo')->insert($data);
                    } catch (\Exception $e) {
                        Writer::nl($e->getMessage(), 'error');
                    }
                } else {
                    Writer::nl("-> SKIPPED", 'warning');
                }


            }
            $counter++;
        }
    }


    private function _seo_table()
    {
        $table = <<<TABLE
<table cellpadding=2 cellspacing=2 border=1>
<thead>
<tr>
<th>url</th>
<th>title</th>
<th>lunghezza title</th>
<th>title originale</th>
<th>description</th>
<th>lunghezza description</th>
<th>description originale</th>
<th>h1</th>
<th>keywords</th>
<th>canonical</th>
<th>lang</th>
</tr>
</thead>
<tbody>
TABLE;
        $rows = DB::table('export_seo')->get();

        foreach ($rows as $row) {

            $canonical = str_replace('http://kronoshop.l4', 'https://www.kronoshop.com', $row->canonical);

            $row->ln_title = \Str::length($row->tr_title);
            $row->ln_description = \Str::length($row->tr_description);

            $c1 = $row->ln_title > 70 ? 'background-color:yellow; color:#000' : 'background-color:green';
            $c2 = $row->ln_description > 160 ? 'background-color:yellow; color:#000' : 'background-color:green';
            $c3 = $row->url != $canonical ? 'background-color:yellow;  color:#000' : '';

            $table .= <<<TABLE
<tr>
<td>$row->url</td>
<td>$row->title</td>
<td style="$c1">$row->ln_title</td>
<td>$row->tr_title</td>
<td>$row->description</td>
<td style="$c2">$row->ln_description</td>
<td>$row->tr_description</td>
<td>$row->h1</td>
<td>$row->keywords</td>
<td style="$c3">$canonical</td>
<td>$row->language</td>
</tr>
TABLE;
        }

        $table .= '</tbody></table>';

        /*$response = Response::make($table);
        $response->header('Content-Type', 'application/vnd.ms-excel');
        return $response;*/
        echo $table;

    }


    private function _seo_report($delete = false)
    {
        /*#select *,count(title) as cnt from export_seo  group by title having cnt > 1  order by title
select *,count(description) as cnt from export_seo  group by description having cnt > 1  order by description*/
        $ids = [];
        $rows = DB::select("select title,count(title) as cnt from export_seo  group by title having cnt > 1 order by title");
        foreach ($rows as $row) {
            $temp_ids = DB::table("export_seo")->where('title', $row->title)->lists('id');
            $ids = array_merge($ids, $temp_ids);
        }
        $rows = DB::select("select description,count(description) as cnt from export_seo  group by description having cnt > 1 order by description");
        foreach ($rows as $row) {
            $temp_ids = DB::table("export_seo")->where('description', $row->description)->lists('id');
            $ids = array_merge($ids, $temp_ids);
        }
        Writer::nl($ids);
        if ($delete) DB::table('export_seo')->whereIn('id', $ids)->delete();
    }


    private function _sql_insert($table, $data)
    {
        $keys = [];
        $values = [];
        foreach ($data as $key => $value) {
            $keys[] = $key;
            $values[] = $value;
        }
        $query = "INSERT INTO $table()";

    }





    private function _orders_options()
    {
        $str = '';

        $queries = [];

        $lines = explode(PHP_EOL, $str);
        foreach ($lines as $line) {
            if (\Str::contains($line, 'KRS')) {
                $reference = mb_substr($line, strpos($line, 'KRS'));
                $queries[] = "UPDATE orders SET send_purchase=1 WHERE reference='$reference';";
            }
        }

        Writer::nl(implode('<br>', $queries));

    }


    private function _add_module_positions()
    {
        return;
        $data = [
            'mobile_catalog_top',
            'mobile_catalog_bottom',
            'mobile_footer_menu',
            'mobile_footer_6',
            'mobile_footer_5',
            'mobile_footer_4',
            'mobile_footer_3',
            'mobile_footer_2',
            'mobile_footer_1',
            'mobile_homepage',
            'mobile_slideshow',
            'mobile_menu_3',
            'mobile_menu_2',
            'mobile_menu_1',
        ];

        foreach ($data as $name) {
            $obj = ['name' => $name, 'active' => 1];
            DB::table('module_positions')->insert($obj);
        }
    }


    private function _export_mobile()
    {
        $this->_export_mobile_menu();
        $this->_export_mobile_modules();
        $this->_export_lexicon();
    }


    private function _export_mobile_menu()
    {
        $start = 231;
        $rows = DB::table('menus')->whereIn('menutype_id', [17, 18, 19])->orderBy('id')->get();
        $queries = [];
        foreach ($rows as $row) {
            $start++;
            $a = (array)$row;
            //$a['id'] = $start;
            $query = $this->_insert_query('menus', $a);
            Writer::nl($query);
            $queries[] = $query;
            $lang_rows = DB::table('menus_lang')->where('menu_id', $row->id)->orderBy('lang_id')->get();
            foreach ($lang_rows as $lang_row) {
                $a = (array)$lang_row;
                //$a['menu_id'] = $start;
                $query = $this->_insert_query('menus_lang', $a);
                Writer::nl($query, 'warning');
                $queries[] = $query;
            }
        }

        File::put(app_path('patch_mobile_menu.sql'), implode(PHP_EOL, $queries));
    }


    private function _export_mobile_modules()
    {
        $start = 75;
        $rows = DB::table('modules')->where('id', '>', 75)->orderBy('id')->get();
        $queries = [];
        foreach ($rows as $row) {
            $start++;
            $a = (array)$row;
            $a['id'] = $start;
            $query = $this->_insert_query('modules', $a);
            Writer::nl($query);
            $queries[] = $query;
            $lang_rows = DB::table('modules_lang')->where('module_id', $row->id)->orderBy('lang_id')->get();
            foreach ($lang_rows as $lang_row) {
                $a = (array)$lang_row;
                $a['module_id'] = $start;
                $query = $this->_insert_query('modules_lang', $a);
                Writer::nl($query, 'warning');
                $queries[] = $query;
            }
        }

        File::put(app_path('patch_mobile_modules.sql'), implode(PHP_EOL, $queries));
    }

    private function _insert_query($table, $data)
    {
        foreach ($data as $key => $value) {
            if ($value == null) {
                unset($data[$key]);
            }
        }


        $fields = array_keys($data);
        $values = array_values($data);
        $fieldStr = implode(',', $fields);
        $valueStr = "'" . implode("','", $values) . "'";

        $query = "INSERT INTO $table($fieldStr) VALUES($valueStr);";
        return $query;
    }


    private function _export_lexicon()
    {
        $start = 348;
        $rows = DB::table('lexicons')->where('id', '>=', $start + 1)->orderBy('id')->get();
        $queries = [];
        foreach ($rows as $row) {
            $start++;
            $a = (array)$row;
            $a['id'] = $start;
            $query = $this->_insert_query('lexicons', $a);
            Writer::nl($query);
            $queries[] = $query;
            $lang_rows = DB::table('lexicons_lang')->where('lexicon_id', $row->id)->orderBy('lang_id')->get();
            foreach ($lang_rows as $lang_row) {
                $a = (array)$lang_row;
                $a['lexicon_id'] = $start;
                $query = $this->_insert_query('lexicons_lang', $a);
                Writer::nl($query, 'warning');
                $queries[] = $query;
            }
        }

        File::put(app_path('patch_mobile_lexicon.sql'), implode(PHP_EOL, $queries));
    }


    private function removeMinFilename()
    {
        $path = public_path("media/images/brands_default");
        $files = File::allFiles($path);
        //Writer::nl($files);
        foreach ($files as $file) {
            $newFile = str_replace('-min', '', $file->getPathname());
            rename($file->getPathname(), $newFile);
            Writer::nl($newFile, 'success');
        }
    }


    private function _files_to_lexicon()
    {
        $rows_it = include(app_path("lang/it/template.php"));
        $rows_en = include(app_path("lang/en/template.php"));

        $cache = array();
        $cache_rows = DB::table('import_lexicon')->get();
        foreach ($cache_rows as $row) {
            $cache[$row->code] = true;
        }


        foreach ($rows_it as $key => $value) {
            if (isset($cache[$key])) {
                DB::table('import_lexicon')->where('code', $key)->update(['str_it' => trim($value)]);
                \Writer::nl("$key (IT) -> UPDATED");
            } else {
                DB::table('import_lexicon')->insert(['str_it' => trim($value), 'code' => $key]);
                $cache[$key] = true;
                \Writer::nl("$key (IT) -> INSERTED");
            }
        }

        foreach ($rows_en as $key => $value) {
            if (isset($cache[$key])) {
                DB::table('import_lexicon')->where('code', $key)->update(['str_en' => trim($value)]);
                \Writer::nl("$key (EN) -> UPDATED");
            } else {
                DB::table('import_lexicon')->insert(['str_en' => trim($value), 'code' => $key]);
                $cache[$key] = true;
                \Writer::nl("$key (EN) -> INSERTED");
            }
        }
    }


    private function fix_multiple_sku()
    {
        $rows = DB::select("select sku,count(*) as cnt from products where deleted_at is null GROUP BY sku having cnt > 1 order by cnt desc");
        foreach ($rows as $row) {
            $row->sku = trim($row->sku);
            $products = DB::select("SELECT id FROM products where sku='$row->sku'");
            $i = 0;
            foreach ($products as $p) {
                $i++;
                $sku = $row->sku . '-' . $i;
                DB::table('products')->where('id', $p->id)->update(['supplier_sku' => $sku]);
                \Writer::nl("Updated [$p->id] $row->sku ==>> $sku", 'success');
            }
        }
    }


    private function _export_meta()
    {
        //export products for easybags
        //export by brands
        //104 - trussardi jeans;
        //114 - patrizia pepe;
        //105 - pandorine;
        //106 - numeroventidue;
        //98 - gabs;
        //93 - you-bag;


        //export by categories
        //41 - borse;
        //63 - guscio;
        //64 - body;
        //65 - top;
        //69 - scarpe;
        //70 - sneakers:
        //71 - sandali;
        //72 - stivali;
        //73 - mocassini;
        //74 - decolletès

        $brands_ids = [
            104,
            114,
            105,
            106,
            98,
            93,
        ];

        $categories_ids = [
            41,
            63,
            64,
            65,
            69,
            70,
            71,
            72,
            73,
            74,
        ];

        $images_sets_id = [45, 46, 47, 48, 49, 50, 51, 52, 53, 54];

        echo '<pre>';
        //check names
        $default_lang = 'it';
        $main_export_dir = storage_path('export/' . date('Y-m-d') . "/");
        if (!is_dir($main_export_dir)) {
            \File::makeDirectory($main_export_dir);
        }
        $languages = \Mainframe::languagesCodes();

        $brands = \Brand::whereIn('id', $brands_ids)->get();
        $brands_schema = [];
        \Writer::nl('Prepare to exporting the following brands', 'warning');
        foreach ($brands as $row) {
            $data = $row->toArray();
            foreach ($row->translations as $tr) {
                \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                $data[$tr->lang_id] = $tr->toArray();
            }
            $brands_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'brands.exp';
        \File::put($file, serialize($brands_schema));
        \Writer::nl("Meta exported to $file", 'success');

        $categories = \Category::whereIn('id', $categories_ids)->get();
        $categories_schema = [];
        \Writer::nl('Prepare to exporting the following categories', 'warning');
        foreach ($categories as $row) {
            $data = $row->toArray();
            foreach ($row->translations as $tr) {
                \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                $data[$tr->lang_id] = $tr->toArray();
            }
            $categories_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'categories.exp';
        \File::put($file, serialize($categories_schema));
        \Writer::nl("Meta exported to $file", 'success');

        $collections = \Collection::whereIn('brand_id', array_keys($brands_schema))->get();
        $collections_schema = [];
        \Writer::nl('Prepare to exporting the following collections', 'warning');
        foreach ($collections as $row) {
            $data = $row->toArray();
            foreach ($row->translations as $tr) {
                \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                $data[$tr->lang_id] = $tr->toArray();
            }
            $collections_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'collections.exp';
        \File::put($file, serialize($collections_schema));
        \Writer::nl("Meta exported to $file", 'success');

        $products = \Product::whereIn('brand_id', array_keys($brands_schema))
            ->whereIn('id', function ($query) use ($categories_schema) {
                $query->select(['product_id'])
                    ->from('categories_products')
                    ->whereIn('category_id', array_keys($categories_schema));
            })
            ->get();
        $products_schema = [];
        \Writer::nl('Prepare to exporting the following products', 'warning');
        foreach ($products as $row) {
            $data = $row->toArray();
            foreach ($row->translations as $tr) {
                \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                $data[$tr->lang_id] = $tr->toArray();
            }
            $products_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'products.exp';
        \File::put($file, serialize($products_schema));
        \Writer::nl("Meta exported to $file", 'success');


        $products_attributes = \DB::table('products_attributes')->whereIn('product_id', array_keys($products_schema))->get();
        $products_attributes_schema = [];
        \Writer::nl('Prepare to exporting the following products_attributes', 'warning');
        foreach ($products_attributes as $row) {
            $data = (array)$row;
            $products_attributes_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'products_attributes.exp';
        \File::put($file, serialize($products_attributes_schema));
        \Writer::nl("Meta exported to $file", 'success');


        $products_attributes_position = \DB::table('products_attributes_position')->whereIn('product_id', array_keys($products_schema))->get();
        $products_attributes_position_schema = [];
        \Writer::nl('Prepare to exporting the following products_attributes_position', 'warning');
        foreach ($products_attributes_position as $row) {
            $data = (array)$row;
            $products_attributes_position_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'products_attributes_position.exp';
        \File::put($file, serialize($products_attributes_position_schema));
        \Writer::nl("Meta exported to $file", 'success');


        $images = \ProductImage::whereIn('product_id', array_keys($products_schema))->get();
        $images_schema = [];
        \Writer::nl('Prepare to exporting the following images', 'warning');
        foreach ($images as $row) {
            $data = $row->toArray();
            foreach ($row->translations as $tr) {
                \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                $data[$tr->lang_id] = $tr->toArray();
            }
            $images_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'images.exp';
        \File::put($file, serialize($images_schema));
        \Writer::nl("Meta exported to $file", 'success');

        //categories products
        $categories_products = \DB::table('categories_products')->whereIn('product_id', array_keys($products_schema))->whereIn('category_id', array_keys($categories_schema))->get();
        $categories_products_schema = [];
        \Writer::nl('Prepare to exporting the following categories_products', 'warning');
        foreach ($categories_products as $row) {
            $data = (array)$row;
            $categories_products_schema[] = $data;
        }
        $file = $main_export_dir . 'categories_products.exp';
        \File::put($file, serialize($categories_products_schema));
        \Writer::nl("Meta exported to $file", 'success');

        //images set
        //$images_sets_id = \DB::table('cache_products_sets')->whereIn('product_id',array_keys($products_schema))->select([DB::raw('distinct(image_set_id)')])->lists('image_set_id');

        $images_sets = \DB::table('images_sets')->whereIn('id', $images_sets_id)->get();
        $images_sets_schema = [];
        foreach ($images_sets as $set) {
            $main = (array)$set;
            $images_groups = \GroupImage::where('set_id', $set->id)->get();
            if (count($images_groups)) {
                $main['images_groups'] = [];
            }
            foreach ($images_groups as $row) {
                $data = $row->toArray();
                foreach ($row->translations as $tr) {
                    \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                    $data[$tr->lang_id] = $tr->toArray();
                }
                $main['images_groups'][] = $data;
            }
            $image_sets_rules = \DB::table('images_sets_rules')->where('image_set_id', $set->id)->get();
            $main['rules'] = $image_sets_rules;
            $images_sets_schema[$set->id] = $main;
        }
        $file = $main_export_dir . 'images_sets.exp';
        \File::put($file, serialize($images_sets_schema));
        \Writer::nl("Meta exported to $file", 'success');

    }


    private function _import_meta()
    {
        $dir = '2015-11-09';
        $lookups = ['brands', 'categories', 'collections'];
        $lookups = ['products'];
        $lookups = ['products_attributes'];
        $lookups = ['products_attributes_position'];
        $lookups = ['categories_products'];
        $lookups = ['images'];
        /*$lookups = ['images_sets'];
        $lookups = ['lexicon'];*/
        $main_export_dir = storage_path('export/' . $dir . "/");
        $main_import_dir = storage_path('import/' . $dir . "/");
        if (!is_dir($main_export_dir)) {
            $main_export_dir = "C:/wamp/www/l4/app/storage/export/$dir/";
        }
        if (!is_dir($main_import_dir)) {
            mkdir($main_import_dir, 0777, true);
        }

        $queries = [];
        \Event::listen('illuminate.query', function ($sql, $bindings) use (&$queries) {
            $i = 0;
            $searches = [];
            $replaces = [];
            foreach ($bindings as $binding) {
                $binding = \DB::connection()->getPdo()->quote($binding);
                $sql = preg_replace('/\?/', '{' . $i . '}', $sql, 1);
                $searches[] = '{' . $i . '}';
                $replaces[] = $binding;
                $i++;
            }
            $sql = str_replace($searches, $replaces, $sql);
            $queries[] = $sql;
        });

        $serialize = [];
        $sync_file = $main_export_dir . 'sync.meta';
        if (file_exists($sync_file)) {
            $sync = unserialize(\File::get($sync_file));
        } else {
            $sync = [];
        }

        foreach ($lookups as $l) {
            $file = $main_export_dir . $l . '.exp';
            if (file_exists($file)) {
                \Writer::nl("Founded $file", 'success');
                $data = unserialize(\File::get($file));
                if (is_array($data)) {
                    $serialize[$l] = $data;
                    \Writer::nl("Data unserialized for [$l]", 'success');
                } else {
                    \Writer::nl("Cannot unserialize data for [$l]", 'error');
                }
            } else {
                \Writer::nl("File $file not founded", 'error');
            }
        }

        if (isset($serialize['brands'])) {
            $this->handle_import_meta(['rows' => $serialize['brands'], 'scope' => 'brand'], $sync);
        }

        if (isset($serialize['categories'])) {
            $this->handle_import_meta(['rows' => $serialize['categories'], 'scope' => 'category', 'table' => 'categories', 'table_lang' => 'categories_lang'], $sync);
        }

        if (isset($serialize['collections'])) {
            $this->handle_import_meta(['rows' => $serialize['collections'], 'scope' => 'collection'], $sync);
        }

        if (isset($serialize['products'])) {
            $this->handle_import_meta_products($serialize['products'], $sync);
        }

        if (isset($serialize['products_attributes'])) {
            $this->handle_import_meta_products_attributes('products_attributes', $serialize['products_attributes'], $sync);
        }

        if (isset($serialize['products_attributes_position'])) {
            $this->handle_import_meta_products_attributes('products_attributes_position', $serialize['products_attributes_position'], $sync);
        }

        if (isset($serialize['categories_products'])) {
            $this->handle_import_meta_categories_products($serialize['categories_products'], $sync);
        }

        if (isset($serialize['images'])) {
            $this->handle_import_meta_images($serialize['images'], $sync);
        }

        if (isset($serialize['images_sets'])) {
            $this->handle_import_meta_images_sets($serialize['images_sets'], $sync);
        }

        if (isset($serialize['lexicon'])) {
            $this->handle_import_lexicon($serialize['lexicon'], $sync);
        }

        @\File::put($sync_file, serialize($sync));
        \Writer::nl("Content of sync file", 'warning');
        Writer::nl($sync);
        foreach ($queries as $k => $query) {
            if (substr($query, 0, 6) == 'select') unset($queries[$k]);
        }
        \Writer::nl("All queries to perform", 'warning');
        Writer::nl($queries);
        $content = implode(';' . PHP_EOL, $queries);
        $content = str_replace(" = ''", " = null", $content);
        $content = str_replace(", ''", ", null", $content);
        $sql = implode('_', $lookups) . '.sql';
        \File::put($main_import_dir . $sql, $content);
    }


    private function handle_import_meta($params, &$sync)
    {

        $languages = \Mainframe::languagesCodes();
        $rows = $params['rows'];
        $scope = $params['scope'];
        $table = isset($params['table']) ? $params['table'] : $scope . 's';
        $table_lang = isset($params['table_lang']) ? $params['table_lang'] : $scope . 's_lang';

        $many = count($rows);
        \Writer::nl("Importing [$many] $table", 'message');
        $brand_max_id = DB::table($table)->max('id');
        foreach ($rows as $brand) {
            //Writer::nl($brand);
            $brand_id = null;

            if ($table == 'collections') {
                if (isset($sync['brand_' . $brand['brand_id']])) {
                    \Writer::nl("REMAPPING ===>>> brand_" . $brand['brand_id'], 'warning');
                    $brand['brand_id'] = $sync['brand_' . $brand['brand_id']];
                }
            }

            if ($table == 'categories') {
                if (isset($sync['category_' . $brand['parent_id']])) {
                    \Writer::nl("REMAPPING ===>>> category_" . $brand['parent_id'], 'warning');
                    $brand['parent_id'] = $sync['category_' . $brand['parent_id']];
                }
            }

            $main_brand = $brand;
            foreach ($languages as $c) {
                unset($main_brand[$c]);
            }

            unset($main_brand['id']);
            foreach ($languages as $lang) {
                if (isset($brand[$lang])) {
                    $importTranslation = $brand[$lang];

                    \Writer::nl("Starting to import [{$importTranslation['name']}] for lang '$lang'", 'st2');
                    //where collection_id not in (select id from collections where deleted_at is not null)
                    $translation = DB::table($table_lang)
                        ->where('lang_id', $lang)
                        ->whereSlug($importTranslation['slug'])
                        ->whereNotIn($scope . "_id", function ($query) use ($table) {
                            $query->select(['id'])
                                ->from($table)
                                ->whereNotNull('deleted_at');
                        })
                        ->first();
                    if ($translation) {
                        unset($importTranslation[$scope . '_id']);
                        unset($importTranslation['lang_id']);
                        unset($importTranslation['published']);
                        \Writer::nl("Translation [$lang] founded", 'warning');
                        if ($translation->{$scope . '_id'} == $brand['id']) {
                            \Writer::nl("Translation has same id => proceed to standard update", 'st1');
                            DB::connection()->pretend(function () use ($importTranslation, $translation, $scope, $table_lang) {
                                DB::table($table_lang)
                                    ->where($scope . '_id', $translation->{$scope . '_id'})
                                    ->where('lang_id', $translation->lang_id)
                                    ->update($importTranslation);
                            });
                        } else {
                            \Writer::nl("Translation has different id => re-sync current node and injecting translation into sync file", 'st1');
                            $sync[$scope . '_' . $brand['id']] = $translation->{$scope . '_id'};
                            DB::connection()->pretend(function () use ($importTranslation, $translation, $scope, $table_lang) {
                                DB::table($table_lang)
                                    ->where($scope . '_id', $translation->{$scope . '_id'})
                                    ->where('lang_id', $translation->lang_id)
                                    ->update($importTranslation);
                            });
                        }
                    } else {
                        \Writer::nl("Translation [$lang] NOT founded", 'error');
                        if ($brand_id === null) {
                            //\Writer::nl($main_brand);
                            $brand_max_id++;
                            $main_brand['id'] = $brand_max_id;
                            $brand_id = $brand_max_id;
                            $sync[$scope . '_' . $brand['id']] = $brand_max_id;
                            DB::connection()->pretend(function () use ($main_brand, $table, $scope) {
                                //create the brand
                                DB::table($table)
                                    ->insert($main_brand);
                                \Writer::nl("New $scope created with id [{$main_brand['id']}]", 'success');
                            });
                        }
                        $importTranslation[$scope . '_id'] = $brand_id;
                        DB::connection()->pretend(function () use ($importTranslation, $table_lang) {
                            DB::table($table_lang)
                                ->insert($importTranslation);
                        });

                    }
                }
            }
        }
    }


    private function handle_import_meta_products($rows, &$sync)
    {
        $languages = \Mainframe::languagesCodes();
        $main_exclude = [
            'cache_categories',
            'cache_attributes',
            'cache_attributes_val',
            'cache_price_rules',
            'cnt_categories',
            'cnt_images',
            'cnt_relation_accessories',
            'cnt_relation_products',
            'cnt_attributes',
            'cnt_combinations',
            'cnt_trends',
        ];
        $main_exclude_update = [
            'cnt_categories',
            'cnt_images',
            'cnt_relation_accessories',
            'cnt_relation_products',
            'cnt_attributes',
            'cnt_combinations',
            'cnt_trends',
            'id',
            'new_from_date',
            'new_to_date',
            'qty',
            'default_img',
        ];
        $lang_exclude = [
            //'published',
            'metadata',
            'indexable',
            'attributes',
        ];

        $product_max_id = DB::table('products')->max('id');

        foreach ($rows as $row) {
            foreach ($main_exclude as $e) {
                unset($row[$e]);
            }
            $row['collection_id'] = (int)$row['collection_id'];
            $row['parent_id'] = (int)$row['parent_id'];
            $row['default_category_id'] = (int)$row['default_category_id'];
            $row['main_category_id'] = (int)$row['main_category_id'];
            if (isset($sync['category_' . $row['default_category_id']])) {
                \Writer::nl("REMAPPING ===>>> category_" . $row['default_category_id'], 'warning');
                $row['default_category_id'] = $sync['category_' . $row['default_category_id']];
            }
            if (isset($sync['category_' . $row['main_category_id']])) {
                \Writer::nl("REMAPPING ===>>> category_" . $row['main_category_id'], 'warning');
                $row['main_category_id'] = $sync['category_' . $row['main_category_id']];
            }
            $product_id = null;
            $importProduct = (object)$row;
            /*if($importProduct->id != 7533)
                continue;

            \Writer::nl($importProduct);*/

            \Writer::nl("Starting to import SKU => $importProduct->sku | EAN13 => $importProduct->ean13 | BRAND: $importProduct->brand_id | COLLECTION: $importProduct->collection_id", 'st2');


            if ($importProduct->supplier_sku != '') {
                $product = \Product::where('supplier_sku', $importProduct->supplier_sku)->first();
            } else {
                $product = \Product::where(function ($query) use ($importProduct) {
                    $query->where('sku', trim($importProduct->sku))->where('sku', '<>', '');
                })->first();
            }


            if ($product) {
                \Writer::nl("Founded product with ID $product->id", 'success');

                if ($importProduct->id == $product->id) {
                    \Writer::nl("Founded ID is equal to import ID, do a standard update", 'st1');

                    DB::connection()->pretend(function () use ($importProduct, $product, $languages, $main_exclude_update, $sync) {
                        $p = (array)$importProduct;
                        foreach ($languages as $l) {
                            unset($p[$l]);
                        }
                        foreach ($main_exclude_update as $e) {
                            unset($p[$e]);
                        }
                        //remapping collection and brand
                        if (isset($sync['brand_' . $p['brand_id']])) {
                            \Writer::nl("REMAPPING ===>>> brand_" . $p['brand_id'], 'warning');
                            $p['brand_id'] = $sync['brand_' . $p['brand_id']];
                        }
                        if (isset($sync['collection_' . $p['collection_id']])) {
                            \Writer::nl("REMAPPING ===>>> collection_" . $p['collection_id'], 'warning');
                            $p['collection_id'] = $sync['collection_' . $p['collection_id']];
                        }

                        DB::table('products')
                            ->where('id', $product->id)
                            ->update($p);
                    });

                    foreach ($languages as $lang) {
                        if (isset($importProduct->$lang)) {
                            $translation = $importProduct->$lang;
                            foreach ($lang_exclude as $e) {
                                unset($translation[$e]);
                            }
                            DB::connection()->pretend(function () use ($translation) {
                                DB::table('products_lang')
                                    ->where('product_id', $translation['product_id'])
                                    ->where('lang_id', $translation['lang_id'])
                                    ->update($translation);
                            });
                        }
                    }

                } else {
                    \Writer::nl("Product exists but has different ID", 'error');
                    \Writer::nl("Perform sync to product $importProduct->id => [$product->id]");
                    $sync['product_' . $importProduct->id] = $product->id;
                    DB::connection()->pretend(function () use ($importProduct, $product, $languages, $main_exclude_update, $sync) {
                        $p = (array)$importProduct;
                        $p['old_id'] = $p['id'];
                        foreach ($languages as $l) {
                            unset($p[$l]);
                        }
                        foreach ($main_exclude_update as $e) {
                            unset($p[$e]);
                        }
                        //remapping collection and brand
                        if (isset($sync['brand_' . $p['brand_id']])) {
                            \Writer::nl("REMAPPING ===>>> brand_" . $p['brand_id'], 'warning');
                            $p['brand_id'] = $sync['brand_' . $p['brand_id']];
                        }
                        if (isset($sync['collection_' . $p['collection_id']])) {
                            \Writer::nl("REMAPPING ===>>> collection_" . $p['collection_id'], 'warning');
                            $p['collection_id'] = $sync['collection_' . $p['collection_id']];
                        }

                        DB::table('products')
                            ->where('id', $product->id)
                            ->update($p);
                    });

                    foreach ($languages as $lang) {
                        if (isset($importProduct->$lang)) {
                            $translation = $importProduct->$lang;
                            foreach ($lang_exclude as $e) {
                                unset($translation[$e]);
                            }
                            $translation['product_id'] = $product->id;
                            DB::connection()->pretend(function () use ($translation, $product) {
                                DB::table('products_lang')
                                    ->where('product_id', $product->id)
                                    ->where('lang_id', $translation['lang_id'])
                                    ->update($translation);
                            });
                        }
                    }
                }


            } else {
                \Writer::nl("Product NOT found", 'error');

                if ($product_id === null) {
                    //\Writer::nl($main_brand);
                    $product_max_id++;
                    $new_product = (array)$importProduct;
                    foreach ($languages as $l) {
                        unset($new_product[$l]);
                    }
                    //remapping collection and brand
                    if (isset($sync['brand_' . $new_product['brand_id']])) {
                        \Writer::nl("REMAPPING ===>>> brand_" . $new_product['brand_id'], 'warning');
                        $new_product['brand_id'] = $sync['brand_' . $new_product['brand_id']];
                    }
                    if (isset($sync['collection_' . $new_product['collection_id']])) {
                        \Writer::nl("REMAPPING ===>>> collection_" . $new_product['collection_id'], 'warning');
                        $new_product['collection_id'] = $sync['collection_' . $new_product['collection_id']];
                    }
                    $new_product['id'] = $product_max_id;
                    $product_id = $product_max_id;
                    $sync['product_' . $row['id']] = $product_max_id;
                    DB::connection()->pretend(function () use ($new_product) {
                        //create the brand
                        DB::table('products')
                            ->insert($new_product);
                        \Writer::nl("New product created with id [{$new_product['id']}]", 'success');
                    });
                }

                foreach ($languages as $lang) {
                    if (isset($importProduct->$lang)) {
                        $translation = $importProduct->$lang;
                        foreach ($lang_exclude as $e) {
                            unset($translation[$e]);
                        }
                        $translation['product_id'] = $product_id;
                        DB::connection()->pretend(function () use ($translation) {
                            DB::table('products_lang')
                                ->insert($translation);
                        });
                    }
                }

            }
        }
    }


    private function handle_import_meta_products_attributes($table, $rows, &$sync)
    {
        foreach ($rows as $row) {
            $obj = (object)$row;
            \Writer::nl("Starting to import $table [$obj->id]", 'st2');
            if (isset($sync['product_' . $row['product_id']])) {
                $newId = $sync['product_' . $row['product_id']];
                \Writer::nl("REMAPPING ===>>> product_" . $row['product_id'] . " || $newId", 'warning');
                $row['product_id'] = $newId;
            }

            if ($table == 'products_attributes') {
                $tupla = \DB::table($table)
                    ->where('product_id', $row['product_id'])
                    ->where('attribute_id', $row['attribute_id'])
                    ->where('attribute_val', $row['attribute_val'])->first();
            } else {
                $tupla = \DB::table($table)
                    ->where('product_id', $row['product_id'])
                    ->where('attribute_id', $row['attribute_id'])
                    ->first();
            }


            if ($tupla) {
                \Writer::nl("Tupla exists for product_id => {$row['product_id']} attribute_id => {$row['attribute_id']}", 'success');
            } else {
                unset($row['id']);
                \Writer::nl("Insert new record for product {$row['product_id']}", 'success');
                DB::connection()->pretend(function () use ($row, $table) {
                    DB::table($table)
                        ->insert($row);
                });
            }
        }
    }


    private function _export_lexicon_by_date($mindate)
    {
        $main_export_dir = storage_path('export/' . date('Y-m-d') . "/");
        if (!is_dir($main_export_dir)) {
            mkdir($main_export_dir);
        }
        $rows = \Lexicon::where('created_at', '>=', $mindate)->orWhere('updated_at', '>=', $mindate)->get();
        $lexicon_schema = [];
        \Writer::nl('Prepare to exporting the latest Lexicon', 'warning');
        foreach ($rows as $row) {
            $data = $row->toArray();
            foreach ($row->translations as $tr) {
                \Writer::nl("[$row->id][$tr->lang_id] ==>> $tr->name");
                $data[$tr->lang_id] = $tr->toArray();
            }
            $lexicon_schema[$row->id] = $data;
        }
        $file = $main_export_dir . 'lexicon.exp';
        \File::put($file, serialize($lexicon_schema));
        \Writer::nl("Meta exported to $file", 'success');
    }

    private function handle_import_meta_categories_products($rows, &$sync)
    {
        foreach ($rows as $row) {
            $obj = (object)$row;
            \Writer::nl("Starting to import [category:$obj->category_id] [product:$obj->product_id]", 'st2');
            if (isset($sync['product_' . $row['product_id']])) {
                \Writer::nl("REMAPPING ===>>> product_" . $row['product_id'], 'warning');
                $row['product_id'] = $sync['product_' . $row['product_id']];
            }
            if (isset($sync['category_' . $row['category_id']])) {
                \Writer::nl("REMAPPING ===>>> category_" . $row['category_id'], 'warning');
                $row['category_id'] = $sync['category_' . $row['category_id']];
            }


            $tupla = \DB::table('categories_products')
                ->where('product_id', $row['product_id'])
                ->where('category_id', $row['category_id'])
                ->first();


            if ($tupla) {
                \Writer::nl("Tupla exists... do nothing", 'success');
            } else {
                DB::connection()->pretend(function () use ($row) {
                    DB::table('categories_products')
                        ->insert($row);
                });
            }
        }
    }


    private function handle_import_meta_images($rows, &$sync)
    {
        $cmd = [];
        $languages = \Mainframe::languagesCodes();
        $image_id = DB::table('images')->max('id');
        $images = [];
        foreach ($rows as $row) {
            if (isset($sync['product_' . $row['product_id']])) {
                \Writer::nl("REMAPPING ===>>> product_" . $row['product_id'], 'warning');
                $row['product_id'] = $sync['product_' . $row['product_id']];
            }
            if (!isset($images[$row['product_id']])) {
                $images[$row['product_id']] = [];
            }
            $images[$row['product_id']][] = $row;
        }
        \Writer::nl($images);
        foreach ($images as $product_id => $imgList) {

            \Writer::nl("Starting to import [product:$product_id]", 'st2');

            $imagesCnt = \DB::table('images')
                ->where('product_id', $product_id)
                ->count();

            if (count($imgList) == $imagesCnt) {
                \Writer::nl("Same number of images... do nothing", 'success');
            } else {
                \Writer::nl("Different number of images... insert all", 'warning');
                foreach ($imgList as $row) {
                    $image_id++;
                    $row['id'] = $image_id;
                    $image_file_tokens = explode('.', $row['filename']);
                    $new_file = $image_id . '.' . $image_file_tokens[1];
                    $cmd[] = "cp /workarea/www/production/l4/public/assets/products/{$row['filename']} /workarea/www/easybags/l4/public/assets/products/$new_file";
                    $row['filename'] = $new_file;
                    foreach ($languages as $lang) {
                        unset($row[$lang]);
                    }
                    \Writer::nl("Insert image with ID [$image_id]", 'success');
                    DB::connection()->pretend(function () use ($row) {
                        DB::table('images')
                            ->insert($row);
                    });

                    foreach ($languages as $lang) {
                        $data = ['product_image_id' => $image_id, 'published' => 1, 'lang_id' => $lang];
                        DB::connection()->pretend(function () use ($data) {
                            DB::table('images_lang')
                                ->insert($data);
                        });
                    }
                }
            }
        }
        \Writer::nl("Command for server", 'st1');
        echo '<pre>';
        \Writer::nl(implode(PHP_EOL, $cmd), 'message');
        echo '</pre>';
    }


    private function handle_import_meta_images_sets($rows, &$sync)
    {
        Writer::nl($rows);
        $languages = \Mainframe::languagesCodes();
        foreach ($rows as $row) {
            $rules = isset($row['rules']) ? $row['rules'] : null;
            $images_groups = isset($row['images_groups']) ? $row['images_groups'] : null;
            unset($row['rules']);
            unset($row['images_groups']);
            DB::connection()->pretend(function () use ($row) {
                DB::table('images_sets')->where('id', $row['id'])
                    ->delete();
            });
            DB::connection()->pretend(function () use ($row) {
                DB::table('images_sets')
                    ->insert($row);
            });

            if ($rules) {
                foreach ($rules as $rule) {
                    $rule = (array)$rule;
                    switch ($rule['target_type']) {
                        case 'BRA':
                            if (isset($sync['brand_' . $rule['target_id']])) {
                                $rule['target_id'] = $sync['brand_' . $rule['target_id']];
                            }
                            break;
                        case 'COL':
                            if (isset($sync['collection_' . $rule['target_id']])) {
                                $rule['target_id'] = $sync['collection_' . $rule['target_id']];
                            }
                            break;
                        case 'CAT':
                            if (isset($sync['category_' . $rule['target_id']])) {
                                $rule['target_id'] = $sync['category_' . $rule['target_id']];
                            }
                            break;
                    }
                    DB::connection()->pretend(function () use ($rule) {
                        DB::table('images_sets_rules')->where('id', $rule['id'])
                            ->delete();
                    });
                    DB::connection()->pretend(function () use ($rule) {
                        DB::table('images_sets_rules')
                            ->insert($rule);
                    });
                }
            }

            if ($images_groups) {
                foreach ($images_groups as $group) {
                    $group = (array)$group;
                    foreach ($languages as $lang) {
                        $translation = $group[$lang];
                        DB::connection()->pretend(function () use ($translation) {
                            DB::table('images_groups_lang')->where('lang_id', $translation['lang_id'])->where('group_image_id', $translation['group_image_id'])
                                ->delete();
                        });
                        DB::connection()->pretend(function () use ($translation) {
                            DB::table('images_groups_lang')
                                ->insert($translation);
                        });
                        unset($group[$lang]);
                    }
                    DB::connection()->pretend(function () use ($group) {
                        DB::table('images_groups')->where('id', $group['id'])
                            ->delete();
                    });
                    DB::connection()->pretend(function () use ($group) {
                        DB::table('images_groups')
                            ->insert($group);
                    });
                }
            }
        }
    }


    private function replace_shop_scope()
    {
        $searches = ['Kronoshop'];//Kronoshop, kronoshop, Kronohop
        $replaces = ['Easybags'];//Easybags, easybags, Easybags
        $entities = ['brands_lang' => 'brand_id', 'categories_lang' => 'category_id', 'collections_lang' => 'collection_id', 'products_lang' => 'product_id'];
        $fields = ['h1', 'metatitle', 'metakeywords', 'metadescription', 'sdesc', 'ldesc'];
        $update_queries = [];


        foreach ($searches as $index => $search) {
            $replace = $replaces[$index];

            foreach ($entities as $entity => $record_rel) {
                \Writer::nl("Starting scanning [$entity] to find occurrence of [$search]", 'st1');

                $query = "select * from $entity where";

                foreach ($fields as $f) {
                    $query .= " $f LIKE '%$search%' OR";
                }
                $query = rtrim($query, ' OR');

                $founded = DB::select($query);
                foreach ($founded as $found) {
                    $id = $found->{$record_rel};
                    \Writer::nl("Found occurrence for record [$id][$found->lang_id]", 'success');
                }
                if (count($founded)) {
                    foreach ($fields as $f) {
                        $replace_query = "update $entity set $f = replace($f,'$search','$replace');";
                        $update_queries[] = $replace_query;
                    }
                }
            }
        }

        echo '<pre>';
        \Writer::nl(implode(PHP_EOL, $update_queries), 'message');

    }


    private function handle_import_lexicon($rows, &$sync)
    {
        Writer::nl($rows);

        $languages = \Mainframe::languagesCodes();
        $lex_id = null;
        $max_lex_id = \Lexicon::max('id');
        foreach ($rows as $row) {
            $code = $row['code'];
            $lexicon = \Lexicon::where('code', $code)->first();
            if ($lexicon) {
                \Writer::nl("Lexicon [$code] exist... perform an update", 'success');
                $data = $row;
                $data['id'] = $lexicon->id;
                foreach ($languages as $lang) {
                    $translation = $row[$lang];
                    $translation['lexicon_id'] = $lexicon->id;
                    DB::connection()->pretend(function () use ($translation) {
                        DB::table('lexicons_lang')
                            ->where('lexicon_id', $translation['lexicon_id'])
                            ->where('lang_id', $translation['lang_id'])
                            ->update($translation);
                    });
                    unset($data[$lang]);
                }

                DB::connection()->pretend(function () use ($data) {
                    $id = $data['id'];
                    unset($data['id']);
                    DB::table('lexicons')
                        ->where('id', $id)
                        ->update($data);
                });
            } else {
                \Writer::nl("Lexicon [$code] DOES NOT exist... perform an insert", 'warning');

                $data = $row;
                $max_lex_id++;
                $lex_id = $max_lex_id;
                $data['id'] = $max_lex_id;
                foreach ($languages as $lang) {
                    $translation = $row[$lang];
                    $translation['lexicon_id'] = $max_lex_id;
                    DB::connection()->pretend(function () use ($translation) {
                        DB::table('lexicons_lang')
                            ->where('lexicon_id', $translation['lexicon_id'])
                            ->where('lang_id', $translation['lang_id'])
                            ->insert($translation);
                    });
                    unset($data[$lang]);

                }

                DB::connection()->pretend(function () use ($data) {
                    DB::table('lexicons')
                        ->insert($data);
                });
            }
        }

    }


    private function _adjust_dates()
    {
        $replaces = [
            '2013 / 2014' => '2015 / 2016',
            'novità 2014' => 'novità 2016',
            'collection 2015' => 'collection 2016',
            'collezione 2015' => 'collezione 2016',
            '2012 / 2013' => '2016 / 2017',
            '2014/2016' => '2016 / 2017',
            '2015/2016' => '2016 / 2017',
            '2013/2015' => '2016 / 2017'
        ];

        $replaces = [
            '2015 / 2016' => '2017 / 2018',
            'novità 2016' => 'novità 2017',
            'collection 2016' => 'collection 2017',
            'collezione 2016' => 'collezione 2017',
            '2016 / 2017' => '2017 / 2018',
        ];

        $query = "UPDATE :table SET :field = REPLACE(:field, ':search', ':replace') WHERE :field LIKE '%:search%';";

        $tables = [
            'products_lang' => [
                'sdesc',
                'ldesc',
                'h1',
                'metatitle',
                'metakeywords',
                'metadescription',
                'indexable',
            ],
            'brands_lang' => [
                'sdesc',
                'ldesc',
                'h1',
                'metatitle',
                'metakeywords',
                'metadescription',
            ],
            'categories_lang' => [
                'sdesc',
                'ldesc',
                'h1',
                'metatitle',
                'metakeywords',
                'metadescription',
            ],
            'collections_lang' => [
                'sdesc',
                'ldesc',
                'h1',
                'metatitle',
                'metakeywords',
                'metadescription',
            ],
            'seo_texts_lang' => [
                'content',
                'h1',
                'metatitle',
                'metakeywords',
                'metadescription',
            ],
        ];

        foreach($tables as $table => $fields){
            Writer::nl("# Query for $table",'warning');

            foreach($fields as $field){
                foreach($replaces as $search => $replace){
                    $querySearch = [':table',':field',':search',':replace'];
                    $queryReplace = [$table,$field,$search,$replace];
                    $updateQuery = str_replace($querySearch,$queryReplace,$query);

                    Writer::nl($updateQuery);
                }
            }
        }
    }

}

?>