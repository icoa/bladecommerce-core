<?php

class DaneaEasyFattController extends BackendController
{

    public $component_id = 78;
    public $title = 'Importazione/Esportazione Danea';
    public $page = 'danea.index';
    public $pageheader = 'Importazione/Esportazione Danea';
    public $iconClass = 'font-columns';
    const EMPLOYER_ID = 10;


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_create($model)
    {

    }

    function _prepare()
    {

    }

    public function getReport($report)
    {
        $this->page = 'danea.table';
        $title = $report == 'importable' ? 'Prodotti aggiornabili' : 'Prodotti non importabili';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/danea.js");
        $view = array('report' => $report);
        $this->toolbar();
        return $this->render($view);
    }

    public function getDeletable()
    {
        $this->page = 'danea.deletable';
        $this->pageheader = 'Prodotti rimossi in Danea';
        $this->addBreadcrumb('Prodotti rimossi in Danea');
        $this->toFooter("js/echo/danea.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getIndex()
    {
        $this->pageheader = 'Report Danea';
        $this->addBreadcrumb('Report Danea');
        $this->toFooter("js/echo/danea.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }


    public function getEdit($id)
    {

    }


    public function getTable($report)
    {


        \Utils::watch();

        $flag = ($report == 'importable') ? 1 : 0;

        $pages = DB::table('danea_import_updated_products')
            ->where('report', $flag)
            ->select(
                "id",
                "internal_id",
                "code",
                "barcode",
                "description",
                "category",
                "subcategory",
                "producer_name",
                "net_price1",
                "gross_price1",
                "supplier_name",
                "supplier_net_price",
                "supplier_gross_price",
                "available_qty",
                "out_of_production",
                "availability",
                "net_price2",
                "gross_price2"
            );

        return \Datatables::of($pages)
            ->edit_column('net_price1', function ($data) {
                $v = \Format::currency($data['net_price1'], true);
                if((float)$data['net_price2'] > 0){
                    $v .= "<br>Vendita: ".\Format::currency($data['net_price2'], true);;
                }
                return $v;
            })
            ->edit_column('gross_price1', function ($data) {
                $v = 'Listino: '.\Format::currency($data['gross_price1'], true);
                if((float)$data['gross_price2'] > 0){
                    $v .= "<br>Vendita: ".\Format::currency($data['gross_price2'], true);;
                }
                return $v;
            })
            ->edit_column('supplier_net_price', function ($data) {
                return \Format::currency($data['supplier_net_price'], true);
            })
            ->edit_column('supplier_gross_price', function ($data) {
                return \Format::currency($data['supplier_gross_price'], true);
            })
            ->edit_column('out_of_production', function ($data) {
                return $this->boolean($data, 'out_of_production');
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }


    public function getTabletrash()
    {
        \Utils::watch();

        $lang_id = 'it';

        $pages = Product::rows($lang_id)
            ->whereIn( 'id' , function ($query) {
                $query->select(['product_id'])
                    ->from('danea_import_deleted_products');
            })
            ->select(
                "id",
                "sku",
                "ean13",
                "name",
                "published",
                "buy_price",
                "sell_price_wt",
                "price",
                "qty",
                "is_out_of_production"
            );

        return \Datatables::of($pages)
            ->edit_column('buy_price', function ($data) {
                return \Format::currency($data['buy_price'], true);
            })
            ->edit_column('sell_price_wt', function ($data) {
                return \Format::currency($data['sell_price_wt'], true);
            })
            ->edit_column('price', function ($data) {
                return \Format::currency($data['price'], true);
            })
            ->edit_column('is_out_of_production', function ($data) {
                return $this->boolean($data, 'is_out_of_production');
            })
            ->edit_column('published', function ($data) {
                return $this->boolean($data, 'published');
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make(true);
    }


    private function _style()
    {
        echo <<<STYLE
<style>
    body {
        background-color:#000;
        font-family: Courier;
        font-size:12px;
        color:white;
    }

    .error{
        color:red;
    }

    .success{
        color:lime;
    }

    .message{
        color:cyan;
    }

    .warning{
        color:yellow;
    }

    .st1{
        color:magenta;
    }

    .st2{
        color:orange;
    }
</style>
STYLE;
    }


    function getThrottle()
    {
        $this->_style();
        if (ob_get_level() == 0) ob_start();
        $mode = Input::get('mode', 'import');
        if ($mode == 'import') {
            self::processImport();
        } else {
            self::processImportDelete();
        }
        ob_end_flush();
    }


    protected function actions_default($params = [])
    {
        $importable = ($this->action("getReport", TRUE, 'importable'));
        $fixable = ($this->action("getReport", TRUE, 'fixable'));
        $deletable = ($this->action("getDeletable", TRUE));
        $index = ($this->action("getIndex", TRUE));
        $actions = array(
            new AdminAction('Report', $index, 'font-plus', '', 'Crea il report per il processo di importazione Danea','index'),
            new AdminAction('Aggiornabili', $importable, 'font-ok', 'btn-success', 'Gestione dei prodotti aggiornabili','report'),
            new AdminAction('Inesistenti', $fixable, 'font-off', 'btn-danger', 'Gestione dei prodotti NON aggiornabili','report'),
            new AdminAction('Rimossi', $deletable, 'font-remove', 'btn-warning', 'Gestione dei prodotti rimossi da Danea','report'),

            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente','reload'),
        );
        return $actions;
    }

    protected function actions_create($params = [])
    {
        $actions = parent::actions_create(['only' => ['back']]);
        return $actions;
    }


    static function setFlags(){
        $flag = 1;
        $query = "select id,code,barcode from danea_import_updated_products where code in (select sku from products WHERE deleted_at is null) union select id,code,barcode from danea_import_updated_products where barcode is not null and barcode in (select ean13 from products WHERE deleted_at is null)";
        $rows = DB::select($query);
        $ids = [];

        foreach ($rows as $row) {
            $ids[] = $row->id;
        }

        DB::table('danea_import_updated_products')->whereIn('id', $ids)->update(['report' => $flag]);

        $flag = 0;
        $query = "select id,code,barcode from danea_import_updated_products where code not in (select sku from products WHERE deleted_at is null) union select id,code,barcode from danea_import_updated_products where barcode is not null and barcode not in (select ean13 from products WHERE deleted_at is null)";
        $rows = DB::select($query);
        $ids = [];

        foreach ($rows as $row) {
            $ids[] = $row->id;
        }

        DB::table('danea_import_updated_products')->whereIn('id', $ids)->update(['report' => $flag]);

        $query = "UPDATE danea_import_deleted_products SET code=TRIM(code)";
        DB::statement($query);

        $query = "select P.id,D.code from products P right join danea_import_deleted_products D ON P.sku=D.code WHERE P.deleted_at is null having P.id>0";
        $rows = DB::select($query);
        DB::beginTransaction();
        foreach($rows as $row){
            $query = "UPDATE danea_import_deleted_products SET product_id='$row->id' WHERE code='$row->code'";
            DB::statement($query);
        }
        DB::commit();
    }


    static function _getSummary($mode = true)
    {
        /*if ($mode === true) {
            $flag = 1;
            $query = "select id,code,barcode from danea_import_updated_products where code in (select sku from products WHERE deleted_at is null) union select id,code,barcode from danea_import_updated_products where barcode is not null and barcode in (select ean13 from products WHERE deleted_at is null)";
        } else {
            $flag = 0;
            $query = "select id,code,barcode from danea_import_updated_products where code not in (select sku from products WHERE deleted_at is null) union select id,code,barcode from danea_import_updated_products where barcode is not null and barcode not in (select ean13 from products WHERE deleted_at is null)";
        }
        $rows = DB::select($query);
        $ids = [];
        DB::beginTransaction();
        foreach ($rows as $row) {
            $ids[] = $row->id;
            DB::table('danea_import_updated_products')->where('id', $row->id)->update(['report' => $flag]);
        }
        DB::commit();
        return count($rows);*/
        $flag = ($mode === true) ? 1 : 0;
        return DB::table('danea_import_updated_products')->where('report',$flag)->count('id');
    }

    static function _getDeleted()
    {
        return DB::table('danea_import_deleted_products')->where('product_id','>',0)->count();
        $query = "select count(id) as cnt from products WHERE deleted_at is null and trim(sku) IN (select trim(code) from danea_import_deleted_products)";
        $row = DB::selectOne($query);
        return $row->cnt;
    }


    static function _getProducts($code, $barcode)
    {
        $product_ids = [];
        if ($code != '') {
            $product_ids = Product::where('sku', trim($code))->lists('id');
        }
        if (count($product_ids) == 0 AND $barcode != '') {
            $product_ids = Product::where('ean13', trim($barcode))->lists('id');
        }
        $data = [];
        foreach ($product_ids as $id) {
            $data[] = Product::getObj($id);
        }
        return $data;
    }


    static function processImport()
    {
        ini_set('max_execution_time', 1200);
        Utils::watch();
        $query = "select * from danea_import_updated_products where report=1 order by id desc";
        $rows = DB::select($query);
        $counter = 0;
        $updated = 0;
        $total = count($rows);
        foreach ($rows as $row) {
            $counter++;
            $products = self::_getProducts($row->code, $row->barcode);

            if (count($products) > 0) {
                foreach ($products as $product) {
                    self::prt("Processing Product [$product->id] - SKU: $row->code | BARCODE: $row->barcode [$counter/$total]");
                    $bool = self::updateProductByFlag($product, $row);
                    if ($bool === true) {
                        self::prt('=> UPDATE OK', 'success');
                        $updated++;
                    } else {
                        self::prt("=> UPDATE ERROR ($bool)", 'error');
                    }
                }
            } else {
                self::prt("Product not found! SKU: $row->code | BARCODE: $row->barcode [$counter/$total]", 'error');
            }
            echo "<script>parent.Danea.updateStep($counter,$total); window.scrollBy(0,document.body.scrollHeight);</script>";
            ob_flush();
            flush();
        }
        echo "<script>parent.Danea.finishRun($updated,$total)</script>";
        \ProductHelper::cleanupCache();
    }




    static function processImportDelete()
    {
        ini_set('max_execution_time', 1200);
        Utils::watch();
        $lang_id = 'it';
        $rows = Product::rows($lang_id)
            ->whereIn( 'id' , function ($query) {
                $query->select(['product_id'])
                    ->from('danea_import_deleted_products');
            })
            ->select(
                "id",
                "sku",
                "ean13",
                "name",
                "published",
                "buy_price",
                "sell_price_wt",
                "price",
                "qty",
                "is_out_of_production"
            )
            ->get();


        $counter = 0;
        $updated = 0;
        $total = count($rows);
        foreach ($rows as $product) {
            $counter++;

            self::prt("Processing Product [$product->id] - SKU: $product->sku [$counter/$total]");
            $bool = self::updateProductDeleteByFlag($product);
            if ($bool === true) {
                self::prt('=> UPDATE OK', 'success');
                $updated++;
            } else {
                self::prt("=> UPDATE ERROR ($bool)", 'error');
            }
            echo "<script>parent.Danea.updateStep($counter,$total); window.scrollBy(0,document.body.scrollHeight);</script>";
            ob_flush();
            flush();
        }
        echo "<script>parent.Danea.finishRun($updated,$total)</script>";
    }

    static function updateProductDeleteByFlag($product)
    {
        $qty = \Input::get('chk_qty', false);
        $published = \Input::get('chk_published', false);
        $flag = \Input::get('chk_flag', false);

        try {
            $data = [];
            $data_lang = [];

            if ($qty) {
                $data['qty'] = 0;
            }
            if ($flag) {
                $data['is_out_of_production'] = 1;
            }
            if ($published) {
                $data_lang['published'] = 0;
                DB::table('products_lang')->where('product_id', $product->id)->update($data_lang);
            }
            if (count($data) > 0) {
                $data['updated_at'] = Format::now();
                $data['updated_by'] = self::EMPLOYER_ID;
                Utils::log($data, __METHOD__);
                DB::table('products')->where('id', $product->id)->update($data);
                $product->uncache();
                Utils::log("UPDATED PRODUCT $product->id", __METHOD__);
                if ($qty) {
                    $prevQty = $product->qty;
                    $qty = 0;
                    if ($prevQty != $qty) {
                        if ($prevQty > $qty) {
                            $action = 'decrease';
                        } else {
                            $action = 'increase';
                        }
                        $qty = abs($prevQty - $qty);
                        $params = [
                            'product_id' => $product->id,
                            'product_reference' => $product->sku,
                            'product_name' => $product->name,
                            'qty' => $qty,
                        ];
                        StockMvt::register($action, $params, self::EMPLOYER_ID);
                    }
                }
                DB::table('danea_import_deleted_products')->where('code', $product->sku)->delete();
                Utils::log("DELETED DANEA RECORD $product->sku", __METHOD__);
                return true;
            }
            return 'Nothing to update';
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return 'Internal error';
    }

    static function updateProductByFlag($product, $danea)
    {
        //http://kronoshop.l4/admin/danea/throttle?chk_qty=on&chk_price=on&chk_flag=on&chk_sku=on&rand=1427816856
        $qty = \Input::get('chk_qty', false);
        $price = \Input::get('chk_price', false);
        $flag = \Input::get('chk_flag', false);
        $sku = \Input::get('chk_sku', false);

        try {
            $data = [];
            $vat = floatval($danea->vat);

            if ($qty) {
                $data['qty'] = $danea->available_qty;
            }
            if ($flag) {
                if ($danea->out_of_production == 1 OR $danea->out_of_production == 0) {
                    $data['is_out_of_production'] = $danea->out_of_production;
                }
                if ($danea->availability > 0) {
                    $data['availability_days'] = $danea->availability;
                }
            }
            if ($sku) {
                if (trim($danea->code) != '') $data['sku'] = \Str::upper(trim($danea->code));
                if (trim($danea->barcode) != '') $data['ean13'] = \Str::upper(trim($danea->barcode));
                if ((int)$danea->availability > 0) $data['availability_days'] = (int)$danea->availability;
            }
            if ($price) {
                /*
               * Prezzo di acquisto
                  <SupplierNetPrice>44.5</SupplierNetPrice>
                  <SupplierGrossPrice>54.29</SupplierGrossPrice>

                Prezzo di vendita
                  <NetPrice2>63.11</NetPrice2>
                  <GrossPrice2>77</GrossPrice2>

                Prezzo di listino
                  <NetPrice1>72.95</NetPrice1>
                  <GrossPrice1>89</GrossPrice1>

                Prezzo di acquisto tasse escl.: buy_price
                Prezzo di listino tasse incl.: sell_price_wt
                Prezzo di vendita tasse escl.: sell_price
                Prezzo di vendita tasse incl.: price
                 *
                 * */
                /*if ((float)$danea->gross_price1 > 0) {
                    $data['price'] = \Format::float($danea->gross_price1, 3);
                }
                if ((float)$danea->net_price1 > 0) {
                    $data['sell_price'] = \Format::float($danea->net_price1, 3);
                }
                if ((float)$danea->gross_price2 > 0) {
                    $data['sell_price_wt'] = \Format::float($danea->gross_price2, 3);
                }*/
                if ((float)$danea->gross_price1 > 0) {
                    $data['sell_price_wt'] = \Format::float($danea->gross_price1, 3);
                }
                if ((float)$danea->net_price2 > 0) {
                    $data['sell_price'] = \Format::float($danea->net_price2, 3);
                }
                if ((float)$danea->gross_price2 > 0) {
                    $data['price'] = \Format::float($danea->gross_price2, 3);
                }
                if ((float)$danea->supplier_net_price > 0) {
                    $data['buy_price'] = \Format::float($danea->supplier_net_price, 3);
                }
                /*
                if ((float)$danea->net_price1 > 0) {
                    $data['sell_price'] = \Format::float($danea->net_price1, 3);
                    if($vat > 0){
                        $data['price'] = $data['sell_price'] + ($data['sell_price'] / 100 * $vat);
                        $data['price'] = \Format::float($data['sell_price_wt'], 3);
                    }
                }*/
            }

            if (count($data) > 0) {
                $data['updated_at'] = Format::now();
                $data['updated_by'] = self::EMPLOYER_ID;
                Utils::log($data, __METHOD__);
                DB::table('products')->where('id', $product->id)->update($data);
                $product->uncache();
                Utils::log("UPDATED PRODUCT $product->id", __METHOD__);
                if ($qty) {
                    $prevQty = $product->qty;
                    $qty = $danea->available_qty;
                    if ($prevQty != $qty) {
                        if ($prevQty > $qty) {
                            $action = 'decrease';
                        } else {
                            $action = 'increase';
                        }
                        $qty = abs($prevQty - $qty);
                        $params = [
                            'product_id' => $product->id,
                            'product_reference' => $product->sku,
                            'product_name' => $product->name,
                            'qty' => $qty,
                        ];
                        StockMvt::register($action, $params, self::EMPLOYER_ID);
                    }
                }
                DB::table('danea_import_updated_products')->where('id', $danea->id)->delete();
                Utils::log("DELETED DANEA RECORD $danea->id", __METHOD__);
                return true;
            }
            return 'Nothing to update';
        } catch (Exception $e) {
            return $e->getMessage();
        }
        return 'Internal error';
    }

    static function prt($val, $class = '')
    {
        $now = Format::now();
        echo "<span class='$class'>[$now] - $val</span><br>";
    }


}