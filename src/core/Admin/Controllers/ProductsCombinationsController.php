<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 22-gen-2014 13.49.22
 */

class ProductsCombinationsController extends BackendController
{


    public $model = 'ProductCombination';

    function postDefault($id)
    {
        $obj = ProductCombination::find($id);
        ProductCombination::where("product_id", $obj->product_id)->update(["isDefault" => 0]);
        $obj->isDefault = 1;
        $obj->update();
    }

    function postLoadForm($id)
    {
        //Utils::watch();
        $obj = ProductCombination::find($id);
        $success = true;
        $html = ProductHelper::getCombinationForm($obj->product_id, $id);
        return Json::encode(compact("success", "html"));
    }

    function getForm($product_id)
    {
        $success = true;
        $html = ProductHelper::getCombinationForm($product_id, 0);
        return Json::encode(compact("success", "html"));
    }

    function getQuick($product_id)
    {
        $success = true;
        $html = ProductHelper::getCombinationQuickForm($product_id, 0);
        return Json::encode(compact("success", "html"));
    }

    function postDestroy($id)
    {
        DB::table("products_combinations_attributes")->where("combination_id", $id)->delete();
        DB::table("products_combinations_images")->where("combination_id", $id)->delete();
        return parent::postDestroy($id);
    }

    function getTable($id)
    {
        $success = true;
        $html = ProductHelper::getCombinationsTable($id);
        return Json::encode(compact("success", "html"));
    }

    private function insertQuick($product_id)
    {
        Utils::log(Input::all(), __METHOD__);
        $combination_quick_attributes = Input::get('combination_quick_attributes', []);
        $combination_quick_options = Input::get('combination_quick_options', []);
        $combination_qty = Input::get('combination_qty', 1);
        if (count($combination_qty) > 0) {
            $records = [];
            foreach ($combination_quick_attributes as $index => $attribute_id) {
                if(isset($combination_quick_options[$index])){
                    $match = [];
                    $option_ids = $combination_quick_options[$index];
                    foreach ($option_ids as $option_id) {
                        $match[$attribute_id] = $option_id;
                        if(isset($combination_quick_options[$index+1])){
                            $temp_attribute_id = $combination_quick_attributes[$index+1];
                            $temp_options_ids = $combination_quick_options[$index+1];
                            if(count($temp_options_ids) > 0){
                                foreach($temp_options_ids as $temp_options_id){
                                    $match[$temp_attribute_id] = $temp_options_id;
                                    $records[] = $match;
                                }
                            }
                        }elseif($index <= 1){
                            $records[] = $match;
                        }
                    }
                }
            }
            foreach($records as $record){
                $query = "select id from products_combinations where product_id=$product_id";
                foreach($record as $attribute_id => $option_id){
                    $query .= " and id in (select combination_id from products_combinations_attributes where attribute_id=$attribute_id and option_id=$option_id)";
                }
                $existing_combination = DB::selectOne($query);
                if($existing_combination and $existing_combination->id > 0){
                    ProductCombination::where('id', $existing_combination->id)->update(['quantity' => $combination_qty]);
                }else{
                    $combination = new ProductCombination();
                    $combination->quantity = $combination_qty;
                    $combination->product_id = $product_id;
                    $combination->save();
                    $combination_id = $combination->id;
                    $names = [];
                    foreach($record as $attribute_id => $option_id){
                        $insert = compact('attribute_id','option_id','combination_id');
                        DB::table('products_combinations_attributes')->insert($insert);
                        $attribute = Attribute::getObj($attribute_id);
                        $option = AttributeOption::getObj($option_id);
                        $names[] = $attribute->name.": ".$option->title;
                    }
                    $combination->update(["name" => implode(" - ", $names)]);
                }
            }

            $data = array('success' => true, 'msg' => 'Combinazioni varianti create correttamente');
        } else {
            $data = array('success' => false, 'msg' => 'Nessun valore scelto per gli attributi');
        }

        return Json::encode($data);
    }


    function postSubmit($product_id)
    {
        $data = \Input::all();
        if (Input::get('quick') == 1) {
            return $this->insertQuick($product_id);
        }

        Utils::log($data);
        Utils::watch();
        $exists = false;

        if (isset($data['combination_id']) AND $data['combination_id'] > 0) {
            $obj = ProductCombination::find($data['combination_id']);
            $exists = true;
        } else {
            $obj = new ProductCombination();
        }

        $priceType = $data["combination_impact_price_type"];

        $price = (float)$data["combination_impact_price_vat"];
        switch ($priceType) {
            case "0":
                $price = 0;
                break;
            case "+":
                $price = abs($price);
                break;
            case "-":
                $price = abs($price) * -1;
                break;
        }

        $weightType = $data["combination_impact_price_weight"];
        $weight = (float)$data["combination_weight"];
        switch ($weightType) {
            case "0":
                $weight = 0;
                break;
            case "+":
                $weight = abs($weight);
                break;
            case "-":
                $weight = abs($weight) * -1;
                break;
        }

        $unitPriceType = $data["combination_impact_unitprice_type"];
        $unitPrice = (float)$data["combination_unitprice"];
        switch ($unitPriceType) {
            case "0":
                $unitPrice = 0;
                break;
            case "+":
                $unitPrice = abs($unitPrice);
                break;
            case "-":
                $unitPrice = abs($unitPrice) * -1;
                break;
        }

        $obj->product_id = $product_id;
        $obj->sku = $data["combination_sku"];
        $obj->ean13 = $data["combination_ean13"];
        $obj->upc = $data["combination_upc"];
        $obj->buy_price = (float)$data["combination_buy_price"];
        $obj->price = (float)$price;
        $obj->weight = (float)$weight;
        $obj->unit_price_impact = (float)$unitPrice;
        $obj->isDefault = (int)$data["combination_isDefault"];
        $obj->minimal_quantity = (int)$data["combination_min_qty"];
        $obj->quantity = (int)$data["combination_qty"];
        $obj->available_date_from = ($data["combination_from_date"] == "") ? NULL : Format::sqlDatetime($data["combination_from_date"]);
        $obj->available_date_to = ($data["combination_to_date"] == "") ? NULL : Format::sqlDatetime($data["combination_to_date"]);

        if ($obj->isDefault == 1) {
            ProductCombination::where("product_id", $obj->product_id)->update(["isDefault" => 0]);
        }

        if($exists == false){
            $record = [];

            $combinations = $data["combinations"];
            foreach ($combinations as $c) {
                $tokens = explode("-", $c);
                $attribute_id = $tokens[0];
                $option_id = $tokens[1];
                $record[$attribute_id] = $option_id;
            }

            $query = "select id from products_combinations where product_id=$product_id";
            foreach($record as $attribute_id => $option_id){
                $query .= " and id in (select combination_id from products_combinations_attributes where attribute_id=$attribute_id and option_id=$option_id)";
            }
            $existing_combination = DB::selectOne($query);
            if($existing_combination and $existing_combination->id > 0){
                $data = array('success' => false, 'msg' => 'Esiste già una variante con questa combinazione di attributi');
                return Json::encode($data);
            }
        }

        $combinations = $data["combinations"];
        $default_lang = Cfg::get("DEFAULT_LANGUAGE");
        $names = array();
        foreach ($combinations as $c) {
            $tokens = explode("-", $c);
            $attribute_id = $tokens[0];
            $option_id = $tokens[1];
            $attribute = Attribute::find($attribute_id);
            $option = AttributeOption::find($option_id);

            $names[] = $attribute->{$default_lang}->name . ": " . (($option->{$default_lang}->name == "") ? $option->uname : $option->{$default_lang}->name);
        }
        $obj->name = implode(" - ", $names);
        $obj->save();

        DB::table("products_combinations_attributes")->where("combination_id", $obj->id)->delete();
        foreach ($combinations as $c) {
            $tokens = explode("-", $c);
            $attribute_id = $tokens[0];
            $option_id = $tokens[1];
            DB::table("products_combinations_attributes")->insert([
                "attribute_id" => $attribute_id,
                "option_id" => $option_id,
                "combination_id" => $obj->id,
            ]);
        }



        //images
        $images = isset($data["combination_images"]) ? $data["combination_images"] : false;
        if ($images) {
            DB::table("products_combinations_images")->where("combination_id", $obj->id)->delete();
            foreach ($images as $img) {
                DB::table("products_combinations_images")->insert([
                    "image_id" => $img,
                    "combination_id" => $obj->id,
                ]);
            }
        }

        Utils::unwatch();
        $data = array('success' => true, 'msg' => 'Variante prodotto aggiornata correttamente');
        return Json::encode($data);
    }

}
