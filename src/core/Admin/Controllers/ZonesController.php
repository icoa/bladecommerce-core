<?php

class ZonesController extends BackendController {

    public $component_id = 45;
    public $title = 'Gestione Zone';
    public $page = 'zones.index';
    public $pageheader = 'Gestione Zone';
    public $iconClass = 'font-columns';
    public $model = 'Zone';
    protected $rules = array(
        'name' => 'required'
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Zone');
        $this->toFooter("js/echo/zones.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/zones.js");
        $this->page = 'zones.trash';
        $this->pageheader = 'Cestino Zone';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/zones.js");
        $this->page = 'zones.create';
        $this->pageheader = 'Nuova Zona';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/zones.js");
        $this->page = 'zones.create';
        $this->pageheader = 'Modifica Zona';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::leftJoin("countries", "zones.id", "=", "countries.zone_id")                
                ->groupBy("zones.id")
                ->select('zones.id', 'zones.name', DB::raw("COUNT(countries.id) as cnt"), 'zones.active', 'zones.created_at');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>";
                        })
                        ->edit_column('cnt', function($data) {
                            $link = \URL::action($this->action("getPreview"), $data['id']);
                            return "<span class=\"label label-info mr5\">{$data['cnt']}</span> <a href=\"javascript:Echo.remoteModal('$link');\">Anteprima</a>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })                        
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->having_column("cnt")
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('countries_lang', 'zones.country_id', '=', 'countries_lang.country_id')
                ->where('lang_id', $lang_id)
                ->select('zones.id', 'zones.name as state', 'countries_lang.name as country', 'zones.active', 'zones.deleted_at', 'zones.created_at', 'countries_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::date($data['deleted_at']);
                        })
                        ->edit_column('state', function($data) {
                            return "<strong>{$data['country']}</strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;

        \Input::replace($_POST);

    }
    
    function getPreview($id){
        $model = $this->model;
        $obj = $model::find($id);        
        $view = array('obj' => $obj);
        return Theme::scope("zones.preview",$view)->content();
    }

    function postList() {
        $rows = \Mainframe::brands();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }
    
    function _after_create($model) {
        parent::_after_create($model);
        $this->handleCountries($model);
    }
    
    function _after_update($model) {
        parent::_after_update($model);
        $this->handleCountries($model);
    }
    
    function handleCountries($model){
        $zone_id = $model->id;
        \DB::table("countries")->where("zone_id",$zone_id)->update(["zone_id"=>0]);
        $countries = \Input::get("countries",[]);
        \DB::table("countries")->whereIn("id",$countries)->update(["zone_id"=>$zone_id]);
    }

}
