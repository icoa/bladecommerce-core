<?php

use App\Models\User as ProfileUser;

class UsersController extends BackendController
{

    public $component_id = 51;
    public $title = 'Gestione Utenti';
    public $page = 'users.index';
    public $pageheader = 'Gestione Utenti';
    public $iconClass = 'font-columns';
    public $model = 'Employer';
    protected $rules = array();
    protected $lang_rules = array(
        'first_name' => 'required',
        'email' => 'required'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Utenti');
        $this->toFooter("js/echo/users.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = \URL::action($this->action("getIndex"));

                $actions = array(
                    new AdminAction('Aggiorna', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );

                if ($this->isPopup) {
                    $actions = array(
                        new AdminAction('Salva', "javascript:Echo.submitForm('reopen');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e riapre questa schermata'),
                        new AdminAction('Chiudi', "javascript:window.close();", 'font-remove'),
                    );
                }

                break;

            case 'trash':
                $index = $this->action("getIndex", TRUE);
                $restore = $this->action("postRestoreMulti", TRUE, 0);
                $destroy = $this->action("postDestroyMulti", TRUE);
                $actions = array(
                    new AdminAction('Ripristina', $restore, 'font-undo', 'btn-success confirm-action-multi', 'Recupera i record selezionati'),
                    new AdminAction('Elimina', $destroy, 'font-minus-sign', 'btn-danger confirm-action-multi', 'Elimina definitivamente i record selezionati'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );

                break;

            default:
                $create = ($this->action("getCreate", TRUE));
                $flagMultiON = ($this->action("postFlagMulti", TRUE, 1));
                $flagMultiOff = ($this->action("postFlagMulti", TRUE, 0));
                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $actions = array(
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Abilita', $flagMultiON, 'font-ok', 'btn-warning action-multi', 'Abilita uno o più record selezionati'),
                    new AdminAction('Disabilita', $flagMultiOff, 'font-off', 'btn-warning action-multi', 'Disabilita uno o più record selezionati'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }

    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);

        $flag_label = 'Attiva';
        $flag_icon = 'icon-ok';
        $flag_status = 1;

        if (isset($data['activated'])) {
            if ($data['activated'] == '1') {
                $flag_label = 'Disattiva';
                $flag_icon = 'icon-off';
                $flag_status = 0;
            }
        }


        $status = \URL::action($this->action("postFlag"), array($data['id'], $flag_status));

        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="javascript:;" onclick="EchoTable.action('$status');" class="btn" title="$flag_label"><i class="$flag_icon"></i></a> </li>
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/users.js");
        $this->toFooter("js/echo/profile.js");
        $this->page = 'users.create';
        $this->pageheader = 'Nuovo Utente';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/users.js");
        $this->toFooter("js/echo/profile.js");
        $this->page = 'users.create';
        $this->pageheader = 'Modifica Utente';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $model = $this->model;

        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = DB::table('users')->select('id', 'first_name', 'email', 'activated', 'created_at', 'last_login', 'last_name', 'has_2fa', 'negoziando');

        $gauth_enabled = config('auth.google_authenticator.enabled', false);

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('last_login', function ($data) {
                return \Format::datetime($data['last_login']);
            })
            ->edit_column('first_name', function ($data) use($gauth_enabled) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $post = '';
                if($gauth_enabled and $data['negoziando'] == 0){
                    $post = ($data['has_2fa'] == 1) ? '<span class="label label-success right pull-right">2FA Secure</span>' : '<span class="label label-important right pull-right">2FA Missing</span>';
                }
                return "<strong><a href='$link'>{$data['first_name']} {$data['last_name']}</a></strong>$post";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('activated', function ($data) {
                return ($data['activated'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->remove_column("last_name")
            ->remove_column("has_2fa")
            ->remove_column("negoziando")
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;

        \Input::replace($_POST);

    }

    public function postSave()
    {
        //Flash current values to session
        Input::flash();

        $task = Input::get('task');

        //Add rules here
        $rules = array(
            'first_name' => 'required',
            //'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'min:4',
            'password2' => 'same:password',
        );

        //Get all inputs fields
        $input = Input::all();
        \Utils::log($input, "INPUT ALL");

        //Apply validaiton rules
        $validation = \Validator::make($input, $rules);
        //Validate rules
        if ($validation->fails()) {
            $url = URL::action($this->action("getCreate"));
            return Redirect::to($url)->withErrors($validation);
        }


        try {
            // Create the user
            $user = Sentry::createUser(array(
                'email' => $input['email'],
                'password' => $input['password'],
                'first_name' => ucfirst($input['first_name']),
                'last_name' => ucfirst($input['last_name']),
                'profile' => $input['profile'],
                'activated' => true,
                'negoziando' => $input['negoziando'],
                'clerk' => $input['clerk'],
                'shop_id' => $input['shop_id'] > 0 ? $input['shop_id'] : null,
            ));

            // Find the group using the group id
            $adminGroup = Sentry::findGroupById(2);

            // Assign the group to the user
            $user->addGroup($adminGroup);
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            echo 'Login field is required.';
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            echo 'Password field is required.';
        } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
            echo 'User with this login already exists.';
        } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            echo 'Group was not found.';
        }


        $profile = $input['profile'];
        if ($profile != "") {
            $root = AdminTpl::path();

            $source = $root . "/php/uploads/$profile";
            $dest = $root . "/images/peoples/$profile";
            rename($source, $dest);
        }


        $id = $user->id;

        Input::flush();

        Notification::success("Utente creato con successo");

        if ($task == 'reopen') {
            $url = URL::action($this->action("getEdit"), array($id));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }

    public function postUpdate($id)
    {
        //Flash current values to session
        Input::flash();

        $task = Input::get('task');

        //Add rules here
        $rules = array(
            'first_name' => 'required',
            //'last_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id,
        );
        $password = \Input::get("password", "");
        if ($password != '') {
            $rules['password'] = 'min:4';
            $rules['password2'] = 'same:password';
        }

        //Get all inputs fields
        $input = Input::all();
        \Utils::log($input, "INPUT ALL");
        $profile = \Input::get("profile", "");
        //Apply validaiton rules
        $validation = \Validator::make($input, $rules);
        if ($id == 1 and \App::environment() != 'local') {
            $validation->errors()->add('id', 'This User cannot be updated');
            return Redirect::action($this->action("getEdit"), array($id))->withErrors($validation);
        }
        //Validate rules
        if ($validation->fails()) {
            return Redirect::action($this->action("getEdit"), array($id))->withErrors($validation);
        }


        try {
            // Update the user
            $user = Sentry::findUserById($id);
            $user->email = $input['email'];
            if ($password != '') {
                $user->password = $input['password'];
            }
            $user->first_name = ucfirst($input['first_name']);
            $user->last_name = ucfirst($input['last_name']);
            $user->negoziando = $input['negoziando'];
            $user->clerk = $input['clerk'];
            $user->shop_id = $input['shop_id'] > 0 ? $input['shop_id'] : null;
            if ($profile != '') {
                $user->profile = $input['profile'];
            }
        } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
            echo 'User with this login already exists.';
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            echo 'User was not found.';
        } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            echo 'Group was not found.';
        }

        if ($user->save()) {

            if ($profile != "") {
                $root = AdminTpl::path();

                $source = $root . "/php/uploads/$profile";
                $dest = $root . "/images/peoples/$profile";
                rename($source, $dest);
            }


            $id = $user->id;

            Input::flush();

            Notification::success("Utente aggiornato con successo");

            if ($task == 'reopen') {
                $url = URL::action($this->action("getEdit"), array($id));
                return Redirect::to($url);
            } else {
                return Redirect::action($this->action("getIndex"));
            }
        }
    }

    public function postFlagMulti($status)
    {
        $ids = $_POST['ids'];
        $model = $this->model;
        $rows = $model::whereIn('id', $ids)->get();
        foreach ($rows as $obj) {
            $obj->update(array("activated" => $status));
        }
        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

    public function postFlag($id, $status)
    {
        $model = $this->model;
        $obj = $model::find($id);


        $obj->update(array("activated" => $status));


        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

    public function postDelete($id)
    {
        $model = $this->model;

        if ($id == 2 || $id == 1) {
            $data = array('success' => false, 'error' => 'L\'utente [ID:' . $id . '] non può essere eliminato perchè è un Utente protetto');
        } else {
            $user = Sentry::findUserById($id);
            // Delete the user
            $user->delete();
            $data = array('success' => true, 'msg' => 'Utente eliminato con successo');
        }

        return Json::encode($data);
    }

    public function postDeleteMulti()
    {
        $ids = $_POST['ids'];

        foreach ($ids as $id) {
            if ($id == 2 || $id == 1) {
                $data = array('success' => false, 'error' => 'L\'utente [ID:' . $id . '] non può essere eliminato perchè è un Utente protetto');
                break;
            } else {
                $user = Sentry::findUserById($id);
                // Delete the user
                $user->delete();
                $data = array('success' => true, 'msg' => 'Utente eliminato con successo');
            }
        }

        return Json::encode($data);
    }

}
