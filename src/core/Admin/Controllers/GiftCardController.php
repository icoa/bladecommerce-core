<?php

class GiftCardController extends BackendController
{

    public $component_id = 87;
    public $title = 'Gestione Gift Card';
    public $page = 'giftcard.index';
    public $pageheader = 'Gestione Gift Card';
    public $iconClass = 'font-columns';
    public $model = 'GiftCard';
    protected $rules = array();

    protected $friendly_names = array();

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb($this->title);
        //$this->toFooter("js/echo/invoices.js");
        $this->toFooter("js/echo/giftcard.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }

    public function getEdit($id)
    {

    }

    public function getPreview($id)
    {
        $this->page = 'giftcard.preview';
        $this->pageheader = 'Dettagli Gift Card';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();

        $obj = GiftCard::getObj($id);
        $view['obj'] = $obj;
        $view['movements'] = GiftCardMovement::with(['order', 'customer'])->where('gift_card_id', $obj->id)->get();
        return $this->render($view);
    }

    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $queryBuilder = GiftCard::withTrashed()->leftJoin('orders', 'gift_cards.order_id', '=', 'orders.id')
            ->select([
                'gift_cards.id',
                'orders.reference',
                'gift_cards.code',
                'gift_cards.name',
                'gift_cards.customer_name',
                'gift_cards.amount',
                'gift_cards.original_amount',
                'gift_cards.expires_at',
                'gift_cards.created_at',
                'gift_cards.updated_at',
                'gift_cards.deleted_at',
                'gift_cards.order_id',
            ]);

        $pages = $queryBuilder;


        $callback = function ($data) {
            $key = 'gift_card_record_' . $data['id'];
            if (\Registry::has($key))
                return \Registry::get($key);

            $model = GiftCard::withTrashed()->find($data['id']);
            \Registry::set($key, $model);
            return $model;
        };

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('expires_at', function ($data) {
                return \Format::datetime($data['expires_at']);
            })
            ->edit_column('code', function ($data) {
                $link = \URL::action("GiftCardController@getPreview", $data['id']);
                return "<strong><a href='$link'>{$data['code']}</a></strong>";
            })
            ->edit_column('deleted_at', function ($data) use ($callback) {
                $model = $callback($data);
                return ($model) ? $model->getHtmlStatusAttribute() . $model->getSwitchStatusAttribute() : 'N/D';
            })
            ->edit_column('amount', function ($data) {
                return \Format::currency($data['amount'], true);
            })
            ->edit_column('original_amount', function ($data) {
                return \Format::currency($data['original_amount'], true);
            })
            ->edit_column('reference', function ($data) {
                $link = \URL::action("OrdersController@getPreview", $data['order_id']);
                return "<strong><a href='$link'>{$data['reference']}</a></strong>";
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->remove_column('order_id')
            ->make(true);
    }


    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['reload']]);
        return $actions;
    }


    function getExport()
    {

    }


    public function postFlag($id, $status)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);

        if ((int)$status === 1) {
            $obj->restore();
        } else {
            $obj->delete();
        }

        $data = array('success' => true, 'msg' => 'Status elemento modificato con successo');
        //return json_encode($data);
        return Json::encode($data);
    }

}