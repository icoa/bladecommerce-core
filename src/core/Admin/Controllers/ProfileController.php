<?php

namespace Echo1\Admin;

use AdminUser, AdminTpl, Input, Notification,
    Redirect,
    Sentry,
    Str,
    Utils,
    Validator;
use Illuminate\Support\MessageBag;

class ProfileController extends \BackendController
{

    public $title = 'Modifica Profilo';
    public $pageheader = 'Modifica Profilo personale';
    public $page = 'profile.index';

    public function index()
    {

        Notification::warningInstant('Non compilare il campo <strong>Password</strong> se non vuoi modificare la Password');
        $this->toFooter("js/echo/profile.js");
        $data['obj'] = AdminUser::get();

        return $this->render($data);
    }

    public function save()
    {
        //Flash current values to session
        Input::flash();

        //Same action is used for editing and adding a new category        
        $password = Input::get("password");
        $first_name = Input::get('first_name');
        $last_name = Input::get('last_name');
        $email = Input::get('email');
        $id = Input::get('id');
        $profile = Input::get('profile');

        //Add rules here
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email,' . $id
        );

        if ($password != '') {
            $rules['password'] = 'min:6';
        }

        //Get all inputs fields
        $input = Input::all();

        //Apply validaiton rules
        $validation = Validator::make($input, $rules);
        if ($id == 1 and \App::environment() != 'local') {
            $validation->errors()->add('id', 'This User cannot be updated');
            return Redirect::route('profile')->withErrors($validation);
        }
        //Validate rules
        if ($validation->fails()) {
            return Redirect::route('profile')->withErrors($validation);
        }

        $user = Sentry::getUser();
        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->email = $email;
        if ($password != '') {
            $user->password = \Hash::make($password);
        }
        if ($profile != "") {
            $user->profile = $profile;
            $root = AdminTpl::path();

            $source = $root . "/php/uploads/$profile";
            $dest = $root . "/images/peoples/$profile";
            rename($source, $dest);
        }
        $user->save();

        Input::flush();

        Notification::success("Profilo aggiornato con successo");

        return Redirect::route("profile");
    }

    public function getSettings()
    {
        $user = Sentry::getUser();
        return $this->render($user);
    }

    public function saveSettings()
    {
        Redirect::to("profile.settings");
    }

    public function get2Fa()
    {
        Notification::success("Meccanismo di autenticazione a 2 fattori (2FA) attivato con successo! Ti invitiamo ad eseguire il logout e a testare il login con il nuovo meccanismo di protezione.");

        /** @var \Cartalyst\Sentry\Users\Eloquent\User $user */
        $user = Sentry::getUser();
        $user->bootGoogle2FaChallenge();
        $this->page = $user->has2faEnabled() ? 'profile.2fa_enabled' : 'profile.2fa';
        return $this->render(['obj' => $user]);
    }

    public function save2Fa()
    {
        $this->page = 'profile.2fa';
        //Flash current values to session
        Input::flash();

        /** @var \Cartalyst\Sentry\Users\Eloquent\User $user */
        $user = Sentry::getUser();
        $code = Input::get("code");

        $rules = array(
            'code' => 'required|min:6|max:6',
        );

        //Get all inputs fields
        $input = Input::all();

        //Apply validation rules
        $validation = Validator::make($input, $rules);

        //Validate rules
        if ($validation->fails()) {
            return Redirect::route('profile.2fa')->withErrors($validation);
        }

        $errorMessages = new MessageBag();
        $codeIsValid = $user->check2FaGoogleCodeChallenge($code);
        if (false === $codeIsValid) {
            $errorMessages->add('code', 'Attenzione: il codice non è esatto, controlla di aver eseguito correttamente la procedura');
            return Redirect::route('profile.2fa')->withErrors($errorMessages);
        }

        //now it's all ok
        try {
            $user->save2FaEncryptedToken();
        } catch (\Exception $e) {
            $errorMessages->add('code', 'Attenzione: si è verificato il seguente errore, ' . $e->getMessage());
            return Redirect::route('profile.2fa')->withErrors($errorMessages);
        }

        Input::flush();

        Notification::success("Meccanismo di autenticazione a 2 fattori (2FA) attivato con successo! Ti invitiamo ad eseguire il logout e a testare il login con il nuovo meccanismo di protezione.");

        return Redirect::route("profile.2fa");
    }

    public function disable2Fa()
    {
        $this->page = 'profile.2fa';
        //Flash current values to session
        Input::flash();

        /** @var \Cartalyst\Sentry\Users\Eloquent\User $user */
        $user = Sentry::getUser();
        $code = Input::get("code");

        $rules = array(
            'code' => 'required|min:6|max:6',
        );

        //Get all inputs fields
        $input = Input::all();

        //Apply validation rules
        $validation = Validator::make($input, $rules);

        //Validate rules
        if ($validation->fails()) {
            return Redirect::route('profile.2fa')->withErrors($validation);
        }

        $errorMessages = new MessageBag();
        $codeIsValid = $user->check2FaCode($code);
        if (false === $codeIsValid) {
            $errorMessages->add('code', 'Attenzione: il codice non è esatto, controlla di aver eseguito correttamente la procedura');
            return Redirect::route('profile.2fa')->withErrors($errorMessages);
        }

        //now it's all ok
        try {
            $user->disable2Fa();
        } catch (\Exception $e) {
            $errorMessages->add('code', 'Attenzione: si è verificato il seguente errore, ' . $e->getMessage());
            return Redirect::route('profile.2fa')->withErrors($errorMessages);
        }

        Input::flush();

        Notification::success("Meccanismo di autenticazione a 2 fattori (2FA) disattivato con successo!");

        return Redirect::route("profile.2fa");
    }


}