<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class RedirectLinksController extends BackendController
{

    public $component_id = 76;
    public $title = 'Schema Redirect 301';
    public $page = 'redirect_links.index';
    public $pageheader = 'Schema Redirect 301';
    public $iconClass = 'font-columns';
    public $model = 'RedirectLink';
    protected $rules = array(
        'source' => 'required|unique:redirect_links,source',
        'target' => 'required|different:source',
    );
    protected $friendly_names = array(
        'source' => 'URL sorgente',
        'target' => 'URL destinazione',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Redirect 301');
        $this->toFooter("js/echo/redirect_links.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/redirect_links.js");
        $this->page = 'redirect_links.create';
        $this->pageheader = 'Nuovo Redirect 301';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/redirect_links.js");
        $this->page = 'redirect_links.create';
        $this->pageheader = 'Modifica Redirect 301';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


   
    public function getTable()
    {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::select('id', 'source', 'target');

        return \Datatables::of($pages)
            ->edit_column('source', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "{$data['source']}<a class='pull-right' href='$link'>MODIFICA</a>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['source'] = 'required|unique:redirect_links,source,' . $model->id;
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $_POST['source'] = (trim($_POST['source']));
        $_POST['target'] = (trim($_POST['target']));

        \Input::replace($_POST);

    }

    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);

        $actions = <<<HTML
<ul class="table-controls">    
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = \URL::action($this->action("getIndex"));

                $actions = array(
                    new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva e chiudi', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );

                break;

            default:
                $create = ($this->action("getCreate", TRUE));

                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $actions = array(
                    new AdminAction('Rebuild', 'javascript:RedirectLinks.rebuild();', 'font-cog', 'btn-warning', 'Esegui il rebuild della cache'),
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }



    function postRebuild(){
        $success = true;
        try{
            RedirectLink::rebuildSchema();
        }catch(Exception $e){
            $success = false;
        }
        return Response::json(compact('success'));
    }

    function _after_update($model)
    {
        $model::removeEntry($model->source);
        if($model->isRegExp == 0){
            \Utils::log("addEntry($model->source,$model->target)",__METHOD__);
            $model::addEntry($model->source,$model->target);
        }
    }

    function _after_create($model)
    {
        $model::removeEntry($model->source);
        if($model->isRegExp == 0){
            \Utils::log("addEntry($model->source,$model->target)",__METHOD__);
            $model::addEntry($model->source,$model->target);
        }
    }
    

}
