<?php

class BannersController extends MultiLangController
{

    public $component_id = 26;
    public $title = 'Gestione Banner';
    public $page = 'banners.index';
    public $pageheader = 'Gestione Banner';
    public $iconClass = 'font-columns';
    public $model = 'Banner';
    protected $rules = array();
    protected $lang_rules = array(

    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'PRE POST');

        $_POST['params'] = \ModuleHelper::setParams($_POST['params']);

        $default_lang = \Core::getLang();

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            $source = $_POST["image_" . $default_lang];
            $target = $_POST["image_" . $lang];
            if ($target == "") {
                $_POST["image_" . $lang] = $source;
            }
        }
        $_POST['content'] = is_array($_POST['content']) ? serialize($_POST['content']) : $_POST['content'];
        \Input::replace($_POST);

    }



    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Moduli');
        $this->toFooter("js/echo/modules.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'modules.trash';
        $this->pageheader = 'Cestino Moduli';
        $this->toFooter("js/echo/modules.js");
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getChoose()
    {
        $this->page = 'modules.choose';
        $this->pageheader = 'Tipologia del modulo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('choose');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->page = 'banners.create';
        $this->pageheader = 'Modifica Modulo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getEditinline($id)
    {
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        $success = true;
        $html = Theme::scope("banners.edit", $view)->content();
        return Json::encode(compact("success","html"));
    }


    public function postCreate(){
        $lang = \Core::getLang();
        $languages = \Mainframe::languagesCodes();
        $row_index = \Input::get("row_index");

        $banner = new Banner();
        $banner->row_index = $row_index;
        $banner->save();
        $record_id = $banner->id;

        $tr = $banner->translations();
        foreach($languages as $lang_id){
            $langMenu = new \Banner_Lang();
            $langMenu->lang_id = $lang_id;
            $langMenu->published = 1;
            //$langMenu->image = \Site::emptyImg();
            $tr->save($langMenu);
        }

        $success = true;
        $html = \ResponsiveHelper::box($record_id);
        return Json::encode(compact("success","html"));
    }


    function postRwdAddRow(){
        $structure = \Input::get("structure");
        $mode = \Input::get("mode");
        $blocks = explode("-",$structure);
        $row = ''; $html = '';
        foreach($blocks as $block){
            $row .= \ResponsiveHelper::block($block,$mode);
        }
        $row_index = \Banner::getRowIndex();
        $html = \ResponsiveHelper::row($row_index,$row);
        $success = true;
        return Json::encode(compact("success","html"));
    }


    function postRwdBlock(){
        $span = abs( \Input::get("span") );
        $mode = \Input::get("mode") ;
        if($span > 12){
            $span = 12;
        }

        $html = \ResponsiveHelper::block($span,$mode);

        $success = true;
        return Json::encode(compact("success","html"));
    }


    function postRemove($id){
        $success = true;
        $obj = Banner::find($id);
        $obj->delete();
        return Json::encode(compact("success"));
    }

    public function getDescribe($id,$lang_id)
    {
        $model = $this->model;
        $obj = $model::getObj($id,$lang_id);
        $obj->image = ($obj->image == '') ? '/media/no.gif' : $obj->image;
        return Json::encode($obj);
    }




}