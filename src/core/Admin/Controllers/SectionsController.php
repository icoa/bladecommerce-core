<?php

class SectionsController extends MultiLangController
{

    public $component_id = 15;
    public $title = 'Gestione Sezioni';
    public $page = 'sections.index';
    public $pageheader = 'Gestione Sezioni';
    public $iconClass = 'font-columns';
    public $model = 'Section';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:sections_lang,slug,section_id'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $default_lang = \Core::getLang();
        $languages = \Mainframe::languagesCodes();
        $copyCat = ['image_default','image_thumb','image_zoom'];
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            foreach($copyCat as $field){
                $source = $_POST[$field . "_" . $default_lang];
                $target = $_POST[$field . "_" . $lang];
                if ($target == "") {
                    $_POST[$field . "_" . $lang] = $source;
                }
            }
        }
        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position( \Input::get("parent_id",0) );
        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Sezioni');
        $this->toFooter("js/echo/sections.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'sections.trash';
        $this->pageheader = 'Cestino Sezioni';
        $this->toFooter("js/echo/sections.js");
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'sections.create';
        $this->pageheader = 'Nuova Sezione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->page = 'sections.create';
        $this->pageheader = 'Modifica Sezione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('sections_lang', 'id', '=', 'sections_lang.section_id')
            ->where('sections_lang.lang_id', $lang_id)
            ->leftJoin('sections_lang as P', function($join) use ($lang_id) {
                $join->on('sections.parent_id', '=', 'P.section_id')->on('P.lang_id', '=', 'sections_lang.lang_id');
            })
            ->leftJoin('pages as G', "sections.id", "=", "G.section_id")
            ->groupBy("sections.id")
            ->select('sections.id', 'sections_lang.name', 'sections_lang.slug', 'sections.parent_id', DB::raw('COUNT(G.id) as pages'), 'sections_lang.published', 'sections.created_at', 'sections.position', 'sections_lang.ldesc', 'sections_lang.lang_id', 'P.name as parent');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('parent_id', function ($data) {
                $s = $data['parent'];
                return "<strong>$s</strong>";
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->edit_column('position', function($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('ldesc')
            ->remove_column('parent')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('sections_lang', 'id', '=', 'sections_lang.section_id')
            ->where('sections_lang.lang_id', $lang_id)
            ->leftJoin('sections_lang as P', function($join) use ($lang_id) {
                $join->on('sections.parent_id', '=', 'P.section_id')->on('P.lang_id', '=', 'sections_lang.lang_id');
            })
            ->leftJoin('pages as G', "sections.id", "=", "G.section_id")
            ->groupBy("sections.id")
            ->select('sections.id', 'sections_lang.name', 'sections_lang.slug', 'sections.parent_id', DB::raw('COUNT(G.id) as pages'), 'sections_lang.published', 'sections.deleted_at', 'sections.position', 'sections_lang.ldesc', 'sections_lang.lang_id', 'P.name as parent');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->edit_column('parent_id', function ($data) {
                return "<strong>{$data['parent']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('ldesc')
            ->remove_column('parent')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

}