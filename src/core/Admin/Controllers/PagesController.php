<?php

class PagesController extends MultiLangController
{

    public $component_id = 15;
    public $title = 'Gestione Pagine';
    public $page = 'pages.index';
    public $pageheader = 'Gestione Pagine';
    public $iconClass = 'font-columns';
    public $model = 'Page';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:pages_lang,slug,page_id'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }
        \Utils::log(count($_POST), 'COUNT POST');
        \Utils::log($_POST, 'PRE POST');

        $_POST['starting_at'] = Format::sqlDatetime($_POST['starting'], false);
        $_POST['finishing_at'] = Format::sqlDatetime($_POST['finishing'], false);
        $_POST['created_at'] = Format::sqlDatetime($_POST['created'], false);

        $default_lang = \Core::getLang();
        $languages = \Mainframe::languagesCodes();
        $copyCat = ['image_default','image_thumb','image_zoom'];
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            foreach($copyCat as $field){
                $source = $_POST[$field . "_" . $default_lang];
                $target = $_POST[$field . "_" . $lang];
                if ($target == "") {
                    $_POST[$field . "_" . $lang] = $source;
                }
            }
        }
        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position( \Input::get("section_id",0) );
        \Input::replace($_POST);

    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Pagine');
        $this->toFooter("js/echo/pages.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'pages.trash';
        $this->pageheader = 'Cestino Pagine';
        $this->toFooter("js/echo/pages.js");
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'pages.create';
        $this->pageheader = 'Nuova Pagina';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->page = 'pages.create';
        $this->pageheader = 'Modifica Pagina';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('pages_lang', 'pages.id', '=', 'pages_lang.page_id')
            ->where('pages_lang.lang_id', $lang_id)
            ->leftJoin('sections_lang as S', function ($join) use ($lang_id) {
                $join->on('pages.section_id', '=', 'S.section_id')->on('S.lang_id', '=', 'pages_lang.lang_id');
            })
            ->groupBy('pages.id')
            ->select('pages.id', 'pages_lang.name', 'pages_lang.slug', 'pages.section_id', 'pages_lang.published', 'pages.created_at', 'pages.position', 'pages_lang.ldesc', 'pages_lang.lang_id', 'S.name as section');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('section_id', function ($data) {
                $s = $data['section'];
                return "<strong>{$s}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->edit_column('position', function($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('ldesc')
            ->remove_column('section')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('pages_lang', 'pages.id', '=', 'pages_lang.page_id')
            ->where('pages_lang.lang_id', $lang_id)
            ->leftJoin('sections_lang as S', function ($join) use ($lang_id) {
                $join->on('pages.section_id', '=', 'S.section_id')->on('S.lang_id', '=', 'pages_lang.lang_id');
            })
            ->groupBy('pages.id')
            ->select('pages.id', 'pages_lang.name', 'pages_lang.slug', 'pages.section_id', 'pages_lang.published', 'pages.deleted_at', 'pages.position', 'pages_lang.ldesc', 'pages_lang.lang_id', 'S.name as section');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->edit_column('section_id', function ($data) {
                $s = $data['section'];
                return "<strong>{$s}</strong>";
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('ldesc')
            ->remove_column('section')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

}