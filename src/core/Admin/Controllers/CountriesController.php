<?php

class CountriesController extends MultiLangController {

    public $component_id = 46;
    public $title = 'Gestione Nazioni';
    public $page = 'countries.index';
    public $pageheader = 'Gestione Nazioni';
    public $iconClass = 'font-columns';
    public $model = 'Country';
    protected $rules = array(
        'iso_code' => 'required|unique:countries,iso_code',        
        'zone_id' => 'required',
        'address_format' => 'required',
    );
    protected $lang_rules = array(
        'name' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        
    }

    

    public function getIndex() {
        $this->addBreadcrumb('Elenco Nazioni');
        $this->toFooter("js/echo/countries.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/countries.js");
        $this->page = 'countries.trash';
        $this->pageheader = 'Cestino Nazioni';      
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {        
        $this->toFooter("js/echo/countries.js");
        $this->page = 'countries.create';
        $this->pageheader = 'Nuova Nazione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {     
        $this->toFooter("js/echo/countries.js");
        $this->page = 'countries.create';
        $this->pageheader = 'Modifica Nazione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }
    

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';        

        $model = $this->model;

        $pages = $model::leftJoin('countries_lang', 'id', '=', 'countries_lang.country_id')
                ->leftJoin("zones","zone_id","=","zones.id")
                ->where('lang_id', $lang_id)
                ->select('countries.iso_code', 'countries_lang.name as country', 'zones.name as zone', 'countries.active', 'countries.eu', 'countries.created_at', 'countries_lang.lang_id', 'countries.zone_id','countries.id');
        
        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::date($data['created_at']);
                                })
                        ->edit_column('country', function($data) {
                                    $link = \URL::action($this->action("getEdit"), $data['id']);
                                    return "<strong><a href='$link'>{$data['country']}</a></strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_actions($data);
                                })
                        ->edit_column('active', function($data) {
                                    return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                                })                        
                        ->edit_column('eu', function($data) {
                                    return ($data['eu'] == 1) ? '<span class="label label-success">Sì</span>' : '<span class="label label-important">No</span>';
                                })                        
                        ->remove_column('lang_id')
                        ->remove_column('zone_id')
                        ->remove_column('id')
                        ->rebind_column('zones.name','countries.zone_id')
                        ->edit_column('iso_code', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['iso_code']."</label>";
                                })
                        ->make(true);
    }
    
    
    

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('countries_lang', 'id', '=', 'countries_lang.country_id')
                ->leftJoin("zones","zone_id","=","zones.id")
                ->where('lang_id', $lang_id)
                ->select('countries.iso_code', 'countries_lang.name as country', 'zones.name as zone', 'countries.active', 'countries.deleted_at', 'countries.created_at', 'countries_lang.lang_id','countries.id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                                    return \Format::date($data['created_at']);
                                })
                        ->edit_column('deleted_at', function($data) {
                                    return \Format::date($data['deleted_at']);
                                })
                        ->edit_column('country', function($data) {
                                    return "<strong>{$data['country']}</strong>";
                                })
                        ->add_column('actions', function($data) {
                                    return $this->column_trash_actions($data);
                                })                        
                        ->remove_column('lang_id')
                        ->remove_column('id')
                        ->edit_column('iso_code', function($data) {
                                    return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['iso_code']."</label>";
                                })
                        ->make();
    }
    
    
    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {      
        $this->rules['iso_code'] = 'required|unique:countries,iso_code,' . $model->id;
        $this->_prepare();
    }

    function _prepare() {
        if(count($_POST) == 0){
            return;
        }        
        
        $langDef = Cfg::get('DEFAULT_LANGUAGE');
        
        $defaultName = $_POST["name_".$langDef];        

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if($lang != $langDef){
                if($_POST["name_".$lang] == "")$_POST["name_".$lang] = $defaultName;                
            }            
        }
        
        $_POST["need_zip_code"] = (int)Input::get("need_zip_code",0);
        $_POST["contains_states"] = (int)Input::get("contains_states",0);
        $_POST["need_identification_number"] = (int)Input::get("need_identification_number",0);
        $_POST["eu"] = (int)Input::get("eu",0);
        

        $model = $this->model;        
        
        \Input::replace($_POST);

    }
    
    
    function postList(){
        $rows = \Mainframe::brands();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

}