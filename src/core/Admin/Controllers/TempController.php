<?php

class TempController extends BackendController {

    public $component_id = 25;
    public $title = 'Gestione Sezioni';
    public $page = 'temp.index';
    public $pageheader = 'Gestione Sezioni';
    public $iconClass = 'font-columns';
    public $model = 'Section';
    protected $rules = array();
    protected $lang_rules = array();

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb($this->title);        
        $view = array();        
        return $this->render($view);
    }
}