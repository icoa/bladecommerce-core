<?php

class NewslettersController extends BackendController
{

    public $component_id = 67;
    public $title = 'Gestione destinatari Newsletter';
    public $page = 'newsletters.index';
    public $pageheader = 'Gestione destinatari Newsletter';
    public $iconClass = 'font-cog';
    public $model = 'Newsletter';
    protected $rules = array(
        'email' => 'required|unique:newsletters,email,0',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco destinatari Newsletter');
        $this->toFooter("js/echo/newsletters.js?v=2");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/newsletters.js");
        $this->page = 'newsletters.trash';
        $this->pageheader = 'Cestino destinatari Newsletter';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/newsletters.js");
        $this->page = 'newsletters.create';
        $this->pageheader = 'Nuovo destinatario Newsletter';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/newsletters.js");
        $this->page = 'newsletters.create';
        $this->pageheader = 'Modifica destinatario Newsletter';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);

        //$obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::groupBy("newsletters.id")
            ->leftJoin("customers as C", "newsletters.customer_id", "=", "C.id")
            ->select(
                'newsletters.id',
                'newsletters.email',
                'newsletters.lang_id',
                'newsletters.user_ip',
                'newsletters.profile',
                'newsletters.marketing',
                'newsletters.created_at',
                'C.name as customer',
                'customer_id'
            );

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('lang_id', function ($data) {
                $src = \AdminTpl::img("images/flags/{$data['lang_id']}.jpg");
                return "<img src='$src' />";
            })
            ->edit_column('email', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['email']}</a></strong>";
            })
            ->edit_column('customer', function ($data) {
                $link = \URL::action("CustomersController@getEdit", $data['customer_id']);
                return "<strong><a href='$link'>{$data['customer']}</a></strong>";
            })
            ->edit_column('marketing', function ($data) {
                return $this->boolean($data, 'marketing');
            })
            ->edit_column('profile', function ($data) {
                return $this->boolean($data, 'profile');
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->remove_column('customer_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()
            ->groupBy("newsletters.id")
            ->leftJoin("customers as C", "newsletters.customer_id", "=", "C.id")
            ->select('newsletters.id', 'newsletters.email', 'newsletters.lang_id', 'newsletters.user_ip', 'newsletters.marketing', 'newsletters.deleted_at', 'C.name as customer', 'customer_id');

        return \Datatables::of($pages)
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('lang_id', function ($data) {
                $src = \AdminTpl::img("images/flags/{$data['lang_id']}.jpg");
                return "<img src='$src' />";
            })
            ->edit_column('marketing', function ($data) {
                return $this->boolean($data, 'marketing');
            })
            ->edit_column('customer', function ($data) {
                $link = \URL::action("CustomersController@getEdit", $data['customer_id']);
                return "<strong><a href='$link'>{$data['customer']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('customer_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->rules['email'] = 'required|email|unique:newsletters,email,' . $model->id . ',id';
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);
    }

    function _after_create($model)
    {
        parent::_after_create($model);
    }


    function getExport()
    {
        $this->page = 'newsletters.export';
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
        }
        $this->toFooter("js/echo/newsletters.js");
        $this->pageheader = 'Esportazione Newsletter';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('export');
        $view = array();
        return $this->render($view);
    }

    function getDownload()
    {
        $user = Sentry::getUser();
        if (!$user->hasPermission('export')) {
            $this->page = 'errors.403';
            $view = array();
            return $this->render($view);
        }
        ini_set('max_execution_time', 600);

        $from = Input::get('from', '');
        $to = Input::get('to', '');
        $lang_id = Input::get('lang_id', '');

        $query = \Newsletter::orderBy('email', 'asc');
        if ($from != '') {
            $from = Format::sqlDate($from);
            $query->where('created_at', '>=', $from);
        }
        if ($to != '') {
            $to = Format::sqlDate($to);
            $query->where('created_at', '<=', $to);
        }
        if ($lang_id != '') {
            $query->where('lang_id', $lang_id);
        }

        $rows = $query->get();

        $lines = ['email;lingua;data;segmentazione;comunicazione'];
        foreach ($rows as $row) {
            $lines[] = $row->email . ";" . $row->lang_id . ";" . date("d/m/Y", strtotime($row->created_at)) . ";$row->profile;$row->marketing";
        }

        $file = storage_path() . '/temp.csv';

        \File::put($file, implode(PHP_EOL, $lines));
        $time = isset($rows[0]) ? date("dmY", strtotime($rows[0]->created_at)) : '';
        $time .= '_';
        $time .= isset($rows[count($rows) - 1]) ? date("dmY", strtotime($rows[count($rows) - 1]->created_at)) : '';

        $filename = "destinatari_newsletter_" . $time . ".csv";

        return Response::download($file, $filename, ['content-type' => 'text/cvs']);
    }

    protected function actions_default($params = [])
    {
        $actions = parent::actions_default(['only' => ['new', 'remove', 'trash', 'reload']]);
        $export = ($this->action("getExport", TRUE));
        $user = Sentry::getUser();
        if($user->hasPermission('export')){
            $actions[] = new AdminAction('Esporta CSV', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export');
        }
        return $actions;
    }


    protected function column_actions($data)
    {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);


        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Cestina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        \Utils::log($_POST, 'BEFORE POST');

        if ($_POST['user_ip'] == '') {
            $_POST['user_ip'] = $_SERVER['REMOTE_ADDR'];
        }

        \Input::replace($_POST);

    }


}