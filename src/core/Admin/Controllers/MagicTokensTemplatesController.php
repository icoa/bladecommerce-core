<?php

use Core\Token;

class MagicTokensTemplatesController extends MultiLangController {

    public $component_id = 55;
    public $title = 'Gestione Template Magic Tokens';
    public $page = 'magictokens.index';
    public $pageheader = 'Gestione Template Magic Tokens';
    public $iconClass = 'font-columns';
    public $model = 'MagicTokenTemplate';
    protected $rules = array();
    protected $lang_rules = array(
        'content' => 'required'
    );
    protected $lang_friendly_names = array(
        'content' => 'Struttura del Template'
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Template Magic Tokens');
        $this->toFooter("js/echo/magictokens.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/magictokens.js");
        $this->page = 'magictokens.trash';
        $this->pageheader = 'Cestino Template Magic Tokens';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/magictokens.js");
        $this->page = 'magictokens.create';
        $this->pageheader = 'Nuovo Template Magic Tokens';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/magictokens.js");
        $this->page = 'magictokens.create';
        $this->pageheader = 'Modifica Template Magic Tokens';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }
    
    
    public function getPreview($id) {        
        $model = $this->model;
        $obj = $model::find($id);        
        $obj->fillLanguages();        
        $view = array('obj' => $obj);
        return Theme::scope("magictokens.preview",$view)->content();
    }

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('magic_tokens_templates_lang', 'id', '=', 'magic_tokens_templates_lang.magic_token_template_id')                
                ->where('lang_id', $lang_id)
                ->groupBy("magic_tokens_templates.id")
                ->select('magic_tokens_templates.id', 'name', 'content', 'active', 'magic_tokens_templates.created_at', 'magic_tokens_templates.position', 'magic_tokens_templates_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>";
                        })
                        ->edit_column('content', function($data) {                
                            $token = new Token($data["content"]);
                            $token->setLang($data["lang_id"]);
                            $token->setSandboxMode(TRUE);
                            $example = $token->render();
                            return "<span class='courier'>{$data['content']}</span><br><em class='disabled'>$example</em>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('magic_tokens_templates_lang', 'id', '=', 'magic_tokens_templates_lang.magic_token_template_id')                
                ->where('lang_id', $lang_id)
                ->groupBy("magic_tokens_templates.id")
                ->select('magic_tokens_templates.id', 'name', 'content', 'magic_tokens_templates.position', 'magic_tokens_templates.deleted_at', 'magic_tokens_templates.created_at', 'magic_tokens_templates_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>";
                        })
                        ->edit_column('content', function($data) {                
                            $token = new Token($data["content"]);
                            $token->setLang($data["lang_id"]);
                            $token->setSandboxMode(TRUE);
                            $example = $token->render();
                            return "<span class='courier'>{$data['content']}</span><br><em class='disabled'>$example</em>";
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::datetime($data['deleted_at']);
                        })
                        ->edit_column('name', function($data) {
                            return "<strong>{$data['name']}</strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })                        
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }
    
    
    function postAddRule() {
        $data = \Input::all();

        $success = false;

        \Utils::watch();

        if ($data['target_mode'] == '*') {
            DB::table('magic_tokens_templates_rules')->insert([
                'magic_token_template_id' => $data['id'],
                'target_type' => $data['target_type'],
                'target_mode' => $data['target_mode'],
                'position' => 9999,
            ]);
            $success = true;
            $message = 'Regola salvata con successo';
        } else {
            $ids = \Input::get('condition_' . $data['target_type'], []);
            $many = count($ids);
            if ($many > 0) {
                DB::table('magic_tokens_templates_rules')
                        ->where('magic_token_template_id', $data['id'])
                        ->update(['position' => DB::raw('position+' . $many)]);
                $counter = 0;
                foreach ($ids as $id) {
                    DB::table('magic_tokens_templates_rules')->insert([
                        'magic_token_template_id' => $data['id'],
                        'target_type' => $data['target_type'],
                        'target_mode' => $data['target_mode'],
                        'target_id' => $id,
                        'position' => $counter,
                    ]);
                    $counter++;
                }
                $success = true;
                $message = 'Regola/e salvate con successo';
            } else {
                $message = 'Devi selezionare almeno un elemento per il campo Condizione';
            }
        }

        $data = array('success' => $success, 'message' => $message);

        \Utils::unwatch();

        return Json::encode($data);
    }

    function postRulesTable() {
        $id = \Input::get('id');

        $data = array('success' => true, 'html' => \Helper::getSeoBlocksRulesTable($id));

        return Json::encode($data);
    }

    function postRemoveRule($id) {
        DB::table('magic_tokens_templates_rules')->where('id', $id)->delete();
        $data = array('success' => true, 'msg' => 'Record eliminato con successo');
        return Json::encode($data);
    }

}
