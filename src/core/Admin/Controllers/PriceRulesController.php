<?php

use Core\Condition\RuleResolver;
use services\Membership\Models\Affiliate;

class PriceRulesController extends MultiLangController
{

    public $component_id = 38;
    public $title = 'Gestione Regole Prezzi';
    public $page = 'pricerules.index';
    public $pageheader = 'Gestione Regole Prezzi';
    public $iconClass = 'font-columns';
    public $model = 'PriceRule';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:price_rules_lang,slug,price_rule_id'
    );
    protected $friendly_names = array();
    protected $lang_friendly_names = array(
        'name' => 'Denominazione regola',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Regole Prezzi');
        $this->toFooter("js/echo/pricerules.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/pricerules.js");
        $this->page = 'pricerules.trash';
        $this->pageheader = 'Cestino Regole Prezzi';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/pricerules.js");
        $this->toFooter("js/echo/builder.js");
        $this->toFooter("js/echo/repeatable_prices.js");
        $this->page = 'pricerules.create';
        $this->pageheader = 'Nuova Regola Prezzi';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/pricerules.js");
        $this->toFooter("js/echo/builder.js");
        $this->toFooter("js/echo/repeatable_prices.js");
        $this->page = 'pricerules.create';
        $this->pageheader = 'Modifica Regola Prezzi';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $obj->rebindAttributes();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getPreview($id)
    {
        $model = $this->model;
        $obj = $model::withTrashed()->find($id);
        $obj->fillLanguages();
        $view = array('obj' => $obj);
        return Theme::scope("pricerules.preview", $view)->content();
    }

    public function getTable()
    {
        \Utils::watch();
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::rows($lang_id)
            ->groupBy("price_rules.id")
            ->leftJoin('products_specific_prices', 'price_rules.id', '=', 'products_specific_prices.price_rule_id')
            ->select('price_rules.id', 'name', 'priority', 'price_rules.from_quantity', 'price_rules.reduction_type', 'price_rules.active', DB::raw("count(distinct products_specific_prices.product_id) as products"), 'price_rules.date_from', 'price_rules.date_to', 'price_rules.created_at', 'price_rules.position', 'price_rules.sdesc', 'price_rules_lang.lang_id');


        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('position', function($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->edit_column('products', function ($data) {
                return "<b><a href='/admin/products/?price_rule_id=" . $data['id'] . "'>" . $data['products'] . "</a></b>";
            })
            ->edit_column('date_from', function ($data) {
                return \Format::datetime($data['date_from']);
            })
            ->edit_column('date_to', function ($data) {
                return \Format::datetime($data['date_to']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong><a href='$link'>{$data['name']}</a></strong>$add";
            })
            ->edit_column('reduction_type', function ($data) {
                return PriceRulesController::getRuleExplained($data['id']);
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->rows($lang_id)
            ->groupBy("price_rules.id")
            ->leftJoin('products_specific_prices', 'price_rules.id', '=', 'products_specific_prices.price_rule_id')
            ->select('price_rules.id', 'name', 'priority', 'price_rules.reduction_type', 'price_rules.active', DB::raw("count(products_specific_prices.product_id) as products"), 'price_rules.date_from', 'price_rules.date_to', 'price_rules.created_at', 'price_rules.deleted_at', 'price_rules.sdesc', 'price_rules_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('date_from', function ($data) {
                return \Format::datetime($data['date_from']);
            })
            ->edit_column('date_to', function ($data) {
                return \Format::datetime($data['date_to']);
            })
            ->edit_column('name', function ($data) {
                $add = ($data["sdesc"] == '') ? '' : "<br><em>{$data["sdesc"]}</em>";
                return "<strong>{$data['name']}</strong>$add";
            })
            ->edit_column('reduction_type', function ($data) {
                return PriceRulesController::getRuleExplained($data['id']);
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->edit_column('active', function ($data) {
                return ($data['active'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
            })
            ->remove_column('lang_id')
            ->remove_column('sdesc')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _after_update($model)
    {
        parent::_after_update($model);

        $this->handleRules($model);
        $this->applyRule($model);

    }

    function _after_create($model)
    {
        parent::_after_create($model);


        $this->handleRules($model);
        $this->applyRule($model);

    }

    function _prepare()
    {

        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];
        $defaultImageList = $_POST["image_list_" . $langDef];
        $defaultImageContent = $_POST["image_content_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
                if ($_POST["image_list_" . $lang] == "")
                    $_POST["image_list_" . $lang] = $defaultImageList;
                if ($_POST["image_content_" . $lang] == "")
                    $_POST["image_content_" . $lang] = $defaultImageContent;
            }
        }


        $_POST['from_quantity'] = (int)\Input::get("from_quantity", 1);

        $_POST['currency_restriction'] = (int)\Input::get("currency_restriction", 0);
        $_POST['group_restriction'] = (int)\Input::get("group_restriction", 0);
        $_POST['country_restriction'] = (int)\Input::get("country_restriction", 0);
        $_POST['affiliate_restriction'] = (int)\Input::get("affiliate_restriction", 0);
        if ($_POST['affiliate_restriction'] == 0) {
            $_POST['affiliate_id'] = null;
        }

        $_POST['reduction_value'] = (float)\Input::get("reduction_value", 0);

        $_POST['date_from'] = Format::sqlDatetime($_POST['date_from']);
        $_POST['date_to'] = Format::sqlDatetime($_POST['date_to']);

        $conditions = \Input::get("rule")["conditions"];
        $rr = new RuleResolver();
        $_POST['conditions'] = $rr->getSerializable($conditions);
        \Utils::log($_POST['conditions'], "serialize conditions");
        $model = $this->model;

        $position = \Input::get('position',0);
        if ($position == 0){
            $_POST['position'] = $this->position();
        }else{
            $_POST['position'] = (int)$_POST['position'];
        }


        \Input::replace($_POST);

    }


    private function handleRules($model)
    {
        $bindrule = (int)\Input::get("bindrule", 1);
        if ($bindrule == 0) return;
        \Utils::watch();
        $price_rule_id = $model->id;

        \DB::table("price_rules_currency")->where("price_rule_id", $price_rule_id)->delete();
        $currency_restriction = (int)\Input::get("currency_restriction", 0);
        if ($currency_restriction == 1) {
            $unselected_currencies = \Input::get("unselected_currencies", []);
            if (!empty($unselected_currencies)) {
                foreach ($unselected_currencies as $currency_id) {
                    \DB::table("price_rules_currency")->insert(compact("price_rule_id", "currency_id"));
                }
            }
        }

        \DB::table("price_rules_country")->where("price_rule_id", $price_rule_id)->delete();
        $country_restriction = (int)\Input::get("country_restriction", 0);
        if ($country_restriction == 1) {
            $unselected_countries = \Input::get("unselected_countries", []);
            if (!empty($unselected_countries)) {
                foreach ($unselected_countries as $country_id) {
                    \DB::table("price_rules_country")->insert(compact("price_rule_id", "country_id"));
                }
            }
        }

        \DB::table("price_rules_group")->where("price_rule_id", $price_rule_id)->delete();
        $group_restriction = (int)\Input::get("group_restriction", 0);
        if ($group_restriction == 1) {
            $unselected_groups = \Input::get("unselected_groups", []);
            if (!empty($unselected_groups)) {
                foreach ($unselected_groups as $group_id) {
                    \DB::table("price_rules_group")->insert(compact("price_rule_id", "group_id"));
                }
            }
        }


        return;

    }


    private function applyRule($model)
    {

        $bindrule = (int)\Input::get("bindrule", 1);
        $mode = ($bindrule == 0) ? 'update' : 'replace';

        $price_rule_id = $model->id;

        if ($mode == 'replace') {
            DB::beginTransaction();
            // delete all specific prices
            \ProductPrice::where("price_rule_id", $price_rule_id)->delete();

            $rules = $model->getRuleExceptions();

            $rr = new RuleResolver();
            $rr->setRules($model->conditions);
            $product_ids = $rr->bindProducts();

            $real_ids = \Product::whereIn('id', $product_ids)->lists('id');

            //\Utils::log($real_ids,"REAL IDS");
            //return;
            $position = 0;

            $errors = [];
            if (count($real_ids) > 0) {
                foreach ($real_ids as $product_id) {

                    $position++;

                    foreach ($rules as $rule) {

                        $data = [
                            'product_id' => $product_id,
                            'price_rule_id' => $price_rule_id,
                            'customer_group_id' => $rule->customer_group_id,
                            'currency_id' => $rule->currency_id,
                            'country_id' => $rule->country_id,

                            'date_from' => $model->date_from,
                            'date_to' => $model->date_to,
                            'from_quantity' => $model->from_quantity,
                            'position' => ($model->priority * 10) + ($position),
                            'reduction_type' => $model->reduction_type,
                            'reduction_value' => $model->reduction_value,
                            'reduction_currency' => $model->reduction_currency,
                            'reduction_tax' => $model->reduction_tax,
                            'reduction_target' => $model->reduction_target,
                            'stop_other_rules' => $model->stop_other_rules,
                            'free_shipping' => $model->free_shipping,
                            'active' => $model->active,
                            'global' => $model->global,
                            'repeatable' => $model->repeatable,
                            'affiliate_id' => $model->affiliate_id,
                        ];
                        DB::table('products_specific_prices')->insert($data);

                        try{
                            /** @var Product $product */
                            $product = \Product::find($product_id);
                            if ($product and (int)$product->is_out_of_production === 0){
                                $product->uncache();
                            }

                        }catch(\Exception $e){
                            $errors[] = $e->getMessage();
                            audit_exception($e, __METHOD__);
                        }
                    }
                }
            }

            if(count($errors) === 0){
                DB::commit();
            }else{
                audit_error($errors, 'errors', __METHOD__);
                audit_error($model->toArray(), 'model', __METHOD__);
                DB::rollBack();
            }
        }


        if ($mode == 'update') {
            DB::beginTransaction();
            $data = [
                'date_from' => $model->date_from,
                'date_to' => $model->date_to,
                'from_quantity' => $model->from_quantity,
                'reduction_type' => $model->reduction_type,
                'reduction_value' => $model->reduction_value,
                'reduction_currency' => $model->reduction_currency,
                'reduction_tax' => $model->reduction_tax,
                'reduction_target' => $model->reduction_target,
                'stop_other_rules' => $model->stop_other_rules,
                'free_shipping' => $model->free_shipping,
                'active' => $model->active,
                'global' => $model->global,
                'repeatable' => $model->repeatable,
                'affiliate_id' => $model->affiliate_id,
            ];
            \ProductPrice::where("price_rule_id", $price_rule_id)->update($data);
            DB::commit();
        }

        \ProductHelper::forgetPriceRulesCache($price_rule_id);

    }



    public function postFlagSingle($id, $lang, $status) {
        \ProductHelper::forgetPriceRulesCache($id);
        \ProductPrice::where("price_rule_id", $id)->update(['active' => $status]);
        return parent::postFlagSingle($id, $lang, $status);
    }


    public function postFlagMulti($status) {
        $ids = $_POST['ids'];
        if(is_array($ids) AND count($ids) > 0){
            foreach($ids as $id){
                \ProductPrice::where("price_rule_id", $id)->update(['active' => $status]);
                \ProductHelper::forgetPriceRulesCache($id);
            }
        }
        return parent::postFlagMulti($status);
    }




    static function getCurrencies($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        \Utils::watch();
        $xref_ids = \DB::table("price_rules_currency")->where("price_rule_id", $id)->lists("currency_id");
        \Utils::log($xref_ids);
        $ids = array_merge($ids, $xref_ids);
        $query = \Currency::where("active", 1);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getGroups($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("price_rules_group")->where("price_rule_id", $id)->lists("group_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \CustomerGroup::rows($lang);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getCountries($unselected = true, $id = 0)
    {
        $lang = \Core::getLang();
        $ids = array(0);
        $xref_ids = \DB::table("price_rules_country")->where("price_rule_id", $id)->lists("country_id");
        $ids = array_merge($ids, $xref_ids);
        $query = \Country::rows($lang)->where('active',1);

        if ($unselected) {
            $query->whereIn('id', $ids);
        } else {
            $query->whereNotIn('id', $ids);
        }

        $rows = $query->orderBy('name')->get();

        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return $data;
    }

    static function getAffiliates()
    {
        return Affiliate::options();
    }


    static function getRuleExplained($id)
    {

        $obj = \PriceRule::withTrashed()->find($id);
        $type = $obj->reduction_type;
        $value = $obj->reduction_value;
        $currency = $obj->reduction_currency;
        $tax = $obj->reduction_tax;
        $target = $obj->reduction_target;
        $free_shipping = $obj->free_shipping;

        if((int)$obj->repeatable === 1){
            return '<b>Condizioni in base a soglie/valori multipli</b>';
        }

        return self::getRuleExplainedData($type, $value, $currency, $tax, $target, $free_shipping);
    }


    static function getRuleExplainedData($type, $value, $currency, $tax, $target, $free_shipping)
    {

        $tax_str = '';
        switch ($tax) {
            case 0:
                $tax_str = '(tax escl.)';
                break;
            case 1:
                $tax_str = '(tax incl.)';
                break;
        }
        switch ($target) {
            case 1:
                $target = 'Prz di vendita finale ' . $tax_str;
                break;
            case 2:
                $target = 'Prz di listino (tax incl.)';
                break;
            case 3:
                $target = 'Prz di acquisto (tax escl.)';
                break;
        }

        $rule = '';
        switch ($type) {
            case 'percent':
                $percent = \Format::percentage($value, TRUE);
                $rule .= "<b>Riduzione</b> del <b>$percent</b> su <b>$target</b>";
                break;
            case 'amount':
                $amount = \Format::currency(\Format::convert($value, 1, $currency), true);
                $rule .= "<b>Riduzione</b> di <b>$amount</b> su <b>$target</b>";
                break;
            case 'fixed_amount':
                $price = \Format::currency(\Format::convert($value, 1, $currency), true);
                $rule .= "<b>Importo fisso</b> a <b>$price</b> del <b>$target</b>";
                break;
            case 'fixed_percent':
                $price = \Format::percentage($value, TRUE);
                $rule .= "<b>Importo percentuale</b> al <b>$price</b> del <b>$target</b>";
                break;
        }
        if ($free_shipping == 1) {
            $rule .= "<br><em style='color:#c00'>Spedizione gratuita</em>";
        }
        return $rule;
    }


}
