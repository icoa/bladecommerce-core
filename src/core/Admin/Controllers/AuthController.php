<?php

namespace Echo1\Admin;

use BackendController,
    Input,
    Lang,
    Redirect,
    Sentry,
    Validator;
use Illuminate\Support\MessageBag;

class AuthController extends BackendController
{

    public $title = 'Login';
    public $page = 'login';
    public $bodyClass = 'no-background';
    public $themeLayout = 'login';

    public function getLogin()
    {

        if (!Sentry::check()) {

            //$this->toFooter('js/echo/login.js');
            $this->toFooter('js/echo/password.js');

            return $this->render(array());
        }

        return Redirect::to('admin');
    }

    public function postLogin()
    {
        $credentials = array(
            'email' => Input::get('username'),
            'password' => Input::get('password')
        );

        try {
            /** @var \Cartalyst\Sentry\Users\Eloquent\User|null $user */
            $user = Sentry::authenticate($credentials, false);

            if ($user) {
                if (true === config('auth.google_authenticator.enabled', false)) {
                    if ($user->requires2FaCode()) {
                        return Redirect::action('admin.login.2fa');
                    }
                }
                return Redirect::to('/admin');
            }
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return $this->redirectWithError("login_required");
        } catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return $this->redirectWithError("password_required");
        } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return $this->redirectWithError("password_wrong");
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return $this->redirectWithError("user_not_found");
        } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return $this->redirectWithError("user_not_active");
        } // The following is only required if throttle is enabled
        catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            return $this->redirectWithError("user_suspended");
        } catch (\Cartalyst\Sentry\Throttling\UserBannedException $e) {
            return $this->redirectWithError("user_banned");
        } catch (\Exception $e) {

        }
    }

    public function getLogout()
    {
        /** @var \Cartalyst\Sentry\Users\Eloquent\User $user */
        $user = Sentry::getUser();

        if($user){
            $user->forget2FaSessionToken();
        }

        Sentry::logout();

        return Redirect::route('admin.login');
    }

    private function redirectWithError($errorMsg)
    {
        $err = (Lang::get('login.' . $errorMsg) != '') ? Lang::get('login.' . $errorMsg) : $errorMsg;
        return Redirect::route('admin.login')->withErrors(array('login' => $err));
    }

    public function getLogin2fa()
    {
        $this->page = 'login_2fa';
        if (Sentry::check()) {
            return $this->render(array());
        }
        return Redirect::to('admin');
    }

    public function postLogin2fa()
    {
        if (Sentry::check()) {
            /** @var \Cartalyst\Sentry\Users\Eloquent\User $user */
            $user = Sentry::getUser();
            $code = Input::get("code");

            $rules = array(
                'code' => 'required|min:6|max:6',
            );

            //Get all inputs fields
            $input = Input::all();

            //Apply validation rules
            $validation = Validator::make($input, $rules);

            //Validate rules
            if ($validation->fails()) {
                return Redirect::route('admin.login.2fa')->withErrors($validation);
            }

            $errorMessages = new MessageBag();
            $codeIsValid = $user->check2FaCode($code);
            if (false === $codeIsValid) {
                $errorMessages->add('code', 'Attenzione: il codice non è esatto, controlla di aver eseguito correttamente la procedura');
                return Redirect::route('admin.login.2fa')->withErrors($errorMessages);
            }

            $user->set2FaSessionToken();

            return Redirect::to('admin');
        }
        return Redirect::to('login');
    }

}