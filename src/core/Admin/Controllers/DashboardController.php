<?php

namespace Echo1\Admin;

use Input,
    BackendController,
    Utils,
    Str;

class DashboardController extends BackendController
{
    /* public $title = 'Dashboard';
      public $page = 'dashboard.index';
      public $pageheader = 'Dashboard'; */

    public $component_id = 1;
    public $title = 'Dashboard';
    public $page = 'dashboard.index';
    public $pageheader = 'Dashboard';
    public $iconClass = 'font-home';

    protected function beforeRender()
    {
        parent::beforeRender();
        //$this->toFooter("js/charts/chart.js");
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {

            default:

                $actions = array(
                    new \AdminAction('Prodotti', "/admin/products", 'font-shopping-cart', 'btn-success', 'Gestione Prodotti'),
                    new \AdminAction('Agg. prodotto', "/admin/products/create", 'font-plus-sign', 'btn-success', 'Aggiungi nuovo Prodotto'),
                    new \AdminAction('Categorie', "/admin/categories", 'font-sitemap', 'btn-warning', 'Gestione Categorie'),
                    new \AdminAction('Brand', "/admin/brands", 'font-tags', 'btn-warning', 'Gestione Brand'),
                    new \AdminAction('Collezioni', "/admin/collections", 'font-star', 'btn-warning', 'Gestione Collezioni'),
                    new \AdminAction('Attributi', "/admin/attributes", 'font-table', 'btn-warning', 'Gestione Attributi'),
                    new \AdminAction('Pagine', "/admin/pages", 'font-list', 'btn-info', 'Gestione Pagine'),
                    new \AdminAction('Agg. pagina', "/admin/pages/create", 'font-edit', 'btn-info', 'Aggiungi nuova Pagina'),
                    new \AdminAction('Utenti', "/admin/users", 'font-user', '', 'Gestione Utenti del sistema'),
                    new \AdminAction('Opzioni', "/admin/settings/general", 'font-cog', '', 'Gestisci le Impostazioni del sistema'),
                );
                break;
        }
    }

    public function index()
    {
        $stats = \AdminTpl::getFrontStats();
        $warnings = $this->getWarnings();

        $sin = '[';
        foreach ($stats['series1'] as $value) {
            $sin .= '[' . $value . '],';
        }
        $sin = rtrim($sin, ',') . ']';

        $cos = '[';
        foreach ($stats['series2'] as $value) {
            $cos .= '[' . $value . '],';
        }
        $cos = rtrim($cos, ',') . ']';

        $js = <<<JS
    \$j(function () {
        var sin = $sin, cos = $cos;

        var plot = \$j.plot(\$j("#chart"),
            [ {
                data: sin,
                label: "Carrelli"
            }, {
                data: cos,
                label: "Ordini"
            } ], {
                series: {
                    lines: {
                        show: true
                    },
                    points: {
                        show: true
                    }
                },
                grid: {
                    hoverable: true,
                    clickable: true
                },
                yaxis: {
                    min: 0
                },
                xaxis: {
                    mode: "time",
                    timeformat: "%d/%m/%y"
                }
            });

        function showTooltip(x, y, contents) {
        	//console.log({x, y, contents}, 'showTooltip');
            \$j('<div id="tooltip" class="chart-tooltip">' + contents + '</div>').css( {
                position: 'absolute',
                display: 'none',
                top: y - 40,
                left: x - 180 - 5,
                'z-index': '9999',
                'color': '#fff',
                'font-size': '13px',
                opacity: 0.9
            }).appendTo("body").fadeIn(200);
        }

        var previousPoint = null;
        \$j("#chart").bind("plothover", function (event, pos, item) {
            \$j("#x").text(pos.x.toFixed(2));
            \$j("#y").text(pos.y.toFixed(2));

            if (\$j("#chart").length > 0) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                       \$j("#tooltip").remove();
                        var x = item.datapoint[0],
                            y = item.datapoint[1];

                        x = new Date(x);
                        x = \$j.datepicker.formatDate( "d M yy",x);
                        showTooltip(item.pageX, item.pageY,
                            item.series.label + " per " + x + " = " + y);
                    }
                }
                else {
                    \$j("#tooltip").remove();
                    previousPoint = null;
                }
            }
        });

        \$j("#chart").bind("plotclick", function (event, pos, item) {
            if (item) {
            	//console.log({event, pos, item}, 'plotclick');
            	var x = item.datapoint[0],
                            y = item.datapoint[1];
                        x = new Date(x);
                        x = \$j.datepicker.formatDate( "d M yy",x);
                        var msg = item.series.label + " per " + x + " = " + y;                        
                plot.highlight(item.series, item.datapoint);
                var cssClass = item.series.label === 'Carrelli' ? 'alert-error' : 'alert-info';
                \$j("#chart").parent().append('<div class="alert ' + cssClass + '">Serie evidenziata: <strong>' + msg + '</strong></div>');
            }
        });
    });
JS;


        \Theme::asset()->writeScript('inline-script', $js, []);

        $view = array(
            'warnings' => $warnings,
        );
        $this->toolbar();
        return $this->render($view);
    }


    private function getStats()
    {

    }


    private function getWarnings()
    {
        $warnings = [];

        //if (feats()->bladeReceipts()): this check is temporary disabled
        if (false):
            //check if there are with 'hasLocalReceiptFeeWarning' warnings
            $rows = \Order::withLocalReceiptFeeWarning()->orderBy('id', 'desc')->get();
            foreach ($rows as $row) {
                if (is_null($row->getFeeTransactionCodeAttribute())) {
                    $msg = "L'ordine <strong>$row->reference</strong> necessita di un <strong>codice transazione interno</strong>";
                    $class = 'important blink';
                    $url = "/admin/orders/preview/" . $row->id;
                    $warnings[] = new AdminWarning($msg, $url, $class);
                }
            }
        endif;

        //check if there are order with payment_status = 4
        $payment_status = \PaymentState::STATUS_REFUND_REQUESTED;
        $rows = \Order::where('payment_status', $payment_status)->orderBy('id', 'desc')->get();
        foreach ($rows as $row) {
            $msg = "L'ordine <strong>$row->reference</strong> contiene lo status pagamento <strong>Richiesto rimborso</strong>";
            $class = 'warning';
            $url = "/admin/orders/preview/" . $row->id;
            $warnings[] = new AdminWarning($msg, $url, $class);
        }

        //check if there are shops with no lat/long
        $rows = \MorellatoShop::whereNull('latitude')->orWhereNull('longitude')->get();
        foreach ($rows as $row) {
            $msg = "Il punto vendita <strong>$row->name</strong> non contiene dati geografici di latitudine / longitudine";
            $class = 'important';
            $url = "/admin/morellato_shops/edit/" . $row->id;
            $warnings[] = new AdminWarning($msg, $url, $class);
        }

        //check if there are shops without users
        //get all shop_id from users
        $ids = \DB::table('users')->where('negoziando', 1)->lists('shop_id');
        $rows = \MorellatoShop::whereNotIn('id', $ids)->get();
        foreach ($rows as $row) {
            $msg = "Il punto vendita <strong>$row->name</strong> non ha alcun utente collegato (indirizzo email non fornito)";
            $class = 'warning';
            $url = "/admin/morellato_shops/edit/" . $row->id;
            $warnings[] = new AdminWarning($msg, $url, $class);
        }


        return $warnings;
    }

}


class AdminWarning
{

    public $msg;
    public $class;
    public $url;

    function __construct($msg, $url, $class = 'default')
    {
        $this->msg = $msg;
        $this->url = $url;
        $this->class = $class;
    }
}