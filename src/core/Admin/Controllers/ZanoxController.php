<?php

use services\ZanoxConfig;

class ZanoxController extends BackendController
{

    public $component_id = 80;
    public $title = 'Gestione Zanox';
    public $page = 'zanox.index';
    public $pageheader = 'Gestione Zanox';
    public $iconClass = 'font-columns';
    const EMPLOYER_ID = 10;


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_create($model)
    {

    }

    function _prepare()
    {

    }

    public function getConfig()
    {
        $this->page = 'zanox.config';
        $title = 'Configurazione Zanox';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/zanox.js");
        $view = array();
        $this->toolbar('create');
        return $this->render($view);
    }

    public function getReport($report)
    {
        /*$this->page = 'danea.table';
        $title = $report == 'importable' ? 'Prodotti aggiornabili' : 'Prodotti non importabili';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/danea.js");
        $view = array('report' => $report);
        $this->toolbar();
        return $this->render($view);*/
    }

    public function getDeletable()
    {
        /*$this->page = 'danea.deletable';
        $this->pageheader = 'Prodotti rimossi in Danea';
        $this->addBreadcrumb('Prodotti rimossi in Danea');
        $this->toFooter("js/echo/danea.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);*/
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Report Zanox');
        $this->toFooter("js/echo/zanox.js");

        $enabled = explode(",", ZanoxConfig::get('enabled_countries','IT,GB,US'));

        $countries = \Country::rows(\Core::getLang())->where('gm', 1)
            ->leftJoin('currencies', 'countries.currency_id', '=', 'currencies.id')
            ->select([
                'countries.id',
                'countries_lang.name',
                'countries.iso_code',
                'countries.accepted_lang',
                'countries.default_lang',
                DB::raw('currencies.name as currency'),
                DB::raw('currencies.iso_code as currency_iso'),
            ])
            ->orderBy('countries_lang.name')
            ->get();

        foreach($countries as $country){
            $country->enabled = in_array($country->iso_code,$enabled);
        }

        $view = array(
            'countries' => $countries
        );
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }


    public function getEdit($id)
    {

    }


    public function getTable()
    {

    }


    public function getTabletrash()
    {

    }





    protected function actions_default($params = [])
    {

        $config = ($this->action("getConfig", TRUE));
        $index = ($this->action("getIndex", TRUE));
        $actions = array(
            new AdminAction('Impostazioni', $config, 'font-edit', '', 'Imposta i parametri per la generazione dei Feed', 'index'),
            /*new AdminAction('Aggiornabili', $importable, 'font-ok', 'btn-success', 'Gestione dei prodotti aggiornabili', 'report'),
            new AdminAction('Inesistenti', $fixable, 'font-off', 'btn-danger', 'Gestione dei prodotti NON aggiornabili', 'report'),
            new AdminAction('Rimossi', $deletable, 'font-remove', 'btn-warning', 'Gestione dei prodotti rimossi da Danea', 'report'),*/

            //new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente', 'reload'),
        );
        return $actions;
    }




    function postSaveconfig(){
        //Flash current values to session
        \Input::flashExcept("_lang_fields[]","_lang_fields");

        $data = \Input::all();
        unset($data['_token']);
        unset($data['task']);


        \Utils::log($data,__METHOD__);

        ZanoxConfig::setMultiple($data);

        \Input::flush();

        Notification::success("Configurazione aggiornata con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getConfig"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }




}