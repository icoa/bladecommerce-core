<?php

use services\GoogleRemarketingConfig;

class GoogleRemarketingController extends BackendController
{

    public $component_id = 81;
    public $title = 'Gestione Google Remarketing';
    public $page = 'google_remarketing.index';
    public $pageheader = 'Gestione Google Remarketing';
    public $iconClass = 'font-columns';
    const EMPLOYER_ID = 10;


    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _after_create($model)
    {

    }

    function _prepare()
    {

    }

    public function getConfig()
    {
        $this->page = 'google_remarketing.config';
        $title = 'Configurazione Google Remarketing';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/google_remarketing.js");
        $view = array();
        $this->toolbar('create');
        return $this->render($view);
    }

    public function getReport($report)
    {
        /*$this->page = 'danea.table';
        $title = $report == 'importable' ? 'Prodotti aggiornabili' : 'Prodotti non importabili';
        $this->pageheader = $title;
        $this->addBreadcrumb($title);
        $this->toFooter("js/echo/danea.js");
        $view = array('report' => $report);
        $this->toolbar();
        return $this->render($view);*/
    }

    public function getDeletable()
    {
        /*$this->page = 'danea.deletable';
        $this->pageheader = 'Prodotti rimossi in Danea';
        $this->addBreadcrumb('Prodotti rimossi in Danea');
        $this->toFooter("js/echo/danea.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);*/
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Report Google Remarketing');
        $this->toFooter("js/echo/google_remarketing.js");
        $view = [];
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {

    }

    public function getTrash()
    {

    }


    public function getEdit($id)
    {

    }


    public function getTable()
    {

    }


    public function getTabletrash()
    {

    }





    protected function actions_default($params = [])
    {

        $config = ($this->action("getConfig", TRUE));
        $index = ($this->action("getIndex", TRUE));
        $actions = array(
            new AdminAction('Impostazioni', $config, 'font-edit', '', 'Imposta i parametri per la gestione del Remarketing', 'index'),
            /*new AdminAction('Aggiornabili', $importable, 'font-ok', 'btn-success', 'Gestione dei prodotti aggiornabili', 'report'),
            new AdminAction('Inesistenti', $fixable, 'font-off', 'btn-danger', 'Gestione dei prodotti NON aggiornabili', 'report'),
            new AdminAction('Rimossi', $deletable, 'font-remove', 'btn-warning', 'Gestione dei prodotti rimossi da Danea', 'report'),*/

            //new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente', 'reload'),
        );
        return $actions;
    }




    function postSaveconfig(){
        //Flash current values to session
        \Input::flashExcept("_lang_fields[]","_lang_fields");

        $data = \Input::all();
        unset($data['_token']);
        unset($data['task']);


        \Utils::log($data,__METHOD__);

        $langFields = \Input::get('_lang_fields',[]);
        $defaultLang = \Core::getDefaultLang();
        $languages = \Core::getLanguages();
        foreach($langFields as $field){
            foreach($languages as $lang){
                $defaultValue = \Input::get($field."_".$defaultLang);
                $value = \Input::get($field."_".$lang);
                if(trim($value) == ''){
                    $data[$field."_".$lang] = $defaultValue;
                }
            }
        }

        GoogleRemarketingConfig::setMultiple($data);

        \Input::flush();

        Notification::success("Configurazione aggiornata con successo");

        $task = Input::get('task');

        if ($task == 'reopen') {
            $url = URL::action($this->action("getConfig"));
            return Redirect::to($url);
        } else {
            return Redirect::action($this->action("getIndex"));
        }
    }




}