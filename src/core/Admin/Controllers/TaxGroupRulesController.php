<?php

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class TaxGroupRulesController extends BackendController {

    public $model = 'TaxGroupRule';
    protected $rules = array();

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getEdit($id) {
        $model = $this->model;
        $obj = $model::find($id);        
        $success = true;
        $html = TaxHelper::getRuleForm($obj->tax_rules_group_id, $id);
        return Json::encode(compact("success","html"));
    }
    
    
    public function getForm($tax_rules_group_id){
        $success = true;
        $html = TaxHelper::getRuleForm($tax_rules_group_id, 0);
        return Json::encode(compact("success","html"));
    }

    public function getTable() {

        $model = $this->model;
        //\Utils::watch();
        $lang_id = \Core::getLang();
        $group_id = \Input::get("group_id");

        $pages = $model::leftJoin("countries_lang", "taxes_rules.country_id", "=", "countries_lang.country_id")
                ->leftJoin("states", "taxes_rules.state_id", "=", "states.id")
                ->leftJoin("taxes_lang", "taxes_rules.tax_id", "=", "taxes_lang.tax_id")
                ->where("countries_lang.lang_id", $lang_id)
                ->whereNested(function($query) use($lang_id) {
                    $query->where("taxes_lang.lang_id", $lang_id)
                    ->orWhere("taxes_rules.tax_id", 0);
                })
                ->where("taxes_rules.tax_rules_group_id", $group_id)
                ->select("taxes_rules.id", "countries_lang.name as country", "states.name as state", "zipcode", "behavior", "description", "taxes_lang.name as tax", "taxes_rules.active");

        return \Datatables::of($pages)
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })
                        ->edit_column('behavior', function($data) {
                            $str = '';
                            switch ($data["behavior"]) {
                                case 0:
                                    $str = 'Metodo diretto';
                                    break;
                                case 1:
                                    $str = 'Combinazione';
                                    break;
                                case 2:
                                    $str = 'Consecutivo';
                                    break;
                            }
                            return $str;
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;

        \Input::replace($_POST);

    }

    function postSave() {
        $obj = new TaxGroupRule(\Input::all());
        return $this->savedata($obj);
    }

    function savedata($obj) {
        //\Utils::watch();
        $errors = array();
        $messages = array();
        if ($obj->zipcode != "") {
            if (\Str::contains($obj->zipcode, "-")) {
                $tokens = explode("-", $obj->zipcode);
                $obj->zipcode_from = $tokens[0];
                $obj->zipcode_to = $tokens[1];
            } else {
                $obj->zipcode_from = $obj->zipcode;
                $obj->zipcode_to = $obj->zipcode;
            }
        }
        //\Utils::log($obj->toArray(),"OBJ");
        if ($obj->country_id == 0 AND $obj->state_id == 0) {
            //for all countries
            $countries = Country::whereNotIn('id', function($query) use ($obj) {
                        $query->select(['country_id'])
                                ->from('taxes_rules')
                                ->where('behavior', 0)
                                ->where('tax_rules_group_id', $obj->tax_rules_group_id);
                    })->get();

            if (count($countries) > 0) {
                $counter = 0;
                foreach ($countries as $country) {
                    $counter++;
                    $newObj = $obj->replicate();
                    $newObj->country_id = $country->id;
                    $newObj->setDataSource(false);
                    $newObj->save();
                }
                $messages[] = "Sono state create regole per $counter nazioni";
                $totalCountries = Country::count("id");
                $difference = $totalCountries - $counter;
                if ($difference > 0) {
                    $messages[] = "Attenzione: Sono state saltate $difference nazioni perchè contenevano già delle regole con Metodo diretto";
                }
            } else {
                $errors[] = "Non è stato possibile aggiungere regole globali perchè esistono già regole con Metodo diretto per le soluzioni scelte";
            }
        } else {

            if ($obj->country_id > 0 AND $obj->state_id == 0) { //only country - all states
                $existingObj = TaxGroupRule::where("country_id", $obj->country_id)->where('behavior', 0)->where('tax_rules_group_id', $obj->tax_rules_group_id)->where("id","<>",$obj->id)->first();
                if ($existingObj AND $existingObj->id > 0) {
                    $errors[] = "Esiste già una regola per questa nazione con il metodo diretto";
                } else {
                    $obj->save();
                    $messages[] = "Regola aggiornata con successo";
                }
            }

            if ($obj->state_id > 0) { //only states
                $existingObj = TaxGroupRule::where("state_id", $obj->state_id)->where('behavior', 0)->where('tax_rules_group_id', $obj->tax_rules_group_id)->where("id","<>",$obj->id)->first();
                if ($existingObj AND $existingObj->id > 0) {
                    $errors[] = "Esiste già una regola per questa provincia con il metodo diretto";
                } else {
                    $obj->save();
                    $messages[] = "Regola aggiornata con successo";
                }
            }
        }

        $success = (count($errors) == 0) ? true : false;
        $errors = implode("<br>", $errors);
        $messages = implode("<br>", $messages);

        return Json::encode(compact("success", "errors", "messages"));
    }
    
    
    protected function column_actions($data) {
        $edit = \URL::action($this->action("getEdit"), $data['id']);
        $destroy = \URL::action($this->action("postDelete"), $data['id']);
        

        $flag_label = 'Attiva';
        $flag_icon = 'icon-ok';
        $flag_status = 1;

        if (isset($data['active'])) {
            if ($data['active'] == '1') {
                $flag_label = 'Disattiva';
                $flag_icon = 'icon-off';
                $flag_status = 0;
            }
        }
        
        $status = \URL::action($this->action("postFlag"), array($data['id'], $flag_status));

        $actions = <<<HTML
<ul class="table-controls">
    <li><a href="javascript:;" onclick="EchoTable.action('$status');" class="btn" title="$flag_label"><i class="$flag_icon"></i></a> </li>
    <li><a href="javascript:;" onclick="TaxGroups.loadRecord('$edit');" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
        return $actions;
    }

    function postUpdate($id) {
        $obj = TaxGroupRule::find($id);
        $data = Input::all();
        unset($data['_token']);
        $obj->setAttributes($data);
        $obj->setDataSource(false);
        return $this->savedata($obj);
    }

}
