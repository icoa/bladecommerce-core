<?php

class AttributesController extends MultiLangController {

    public $component_id = 19;
    public $title = 'Gestione Attributi';
    public $page = 'attributes.index';
    public $pageheader = 'Gestione Attributi';
    public $iconClass = 'font-columns';
    public $model = 'Attribute';
    protected $rules = array(
        'code' => 'required|unique:attributes,code',
    );
    protected $lang_rules = array(
        'name' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/attributes.js");
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Attributi');
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->page = 'attributes.trash';
        $this->pageheader = 'Cestino Attributi';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->page = 'attributes.create';
        $this->pageheader = 'Nuovo Attributo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->page = 'attributes.create';
        $this->pageheader = 'Modifica Attributo';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('attributes_lang', 'id', '=', 'attributes_lang.attribute_id')
                ->where('lang_id', $lang_id)
                ->select('id', 'code', 'name', 'frontend_input', 'published', 'created_at', 'position', 'ldesc', 'lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('frontend_input', function($data) {
                            if ($data['frontend_input'] == 'select' OR $data['frontend_input'] == 'multiselect') {
                                $link = \URL::action($this->action("getSummary"), $data['id']);
                                //return "<strong><a title=\"{$data['name']}\" class='popover-trigger' data-class='large nopadding' data-remotecontent='$link' href='javascript:;'>{$data['frontend_input']}</a></strong>";
                                return "<strong><a title=\"{$data['name']}\" href=\"javascript:Echo.remoteModal('$link');\">{$data['frontend_input']}</a></strong>";
                            }
                            return "<strong>{$data['frontend_input']}</strong>";
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong><br><em class='disabled'>{$data['ldesc']}</em>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('published', function($data) {
                            return ($data['published'] == 1) ? '<span class="label label-success">Attivo</span>' : '<span class="label label-important">Disattivo</span>';
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('attributes_lang', 'id', '=', 'attributes_lang.attribute_id')
                ->where('lang_id', $lang_id)
                ->select('id', 'code', 'name', 'frontend_input', 'position', 'deleted_at', 'created_at', 'ldesc', 'lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::datetime($data['deleted_at']);
                        })
                        ->edit_column('name', function($data) {
                            return "<strong>{$data['name']}</strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->rules['code'] = 'required|unique:attributes,code,' . $model->id;
        $this->_prepare();
    }

    /* function _before_clone(&$model) {
      $selfModel = $this->model;
      $cnt = $selfModel::withTrashed()->where('code','LIKE',$model->code.'%')->count('id');
      $cnt++;
      $model->code = $model->code."_".$cnt;
      } */

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $default_value_text = $_POST["default_value_text_" . $langDef];
        $default_value_date = $_POST["default_value_date_" . $langDef];
        $default_value_textarea = $_POST["default_value_textarea_" . $langDef];
        $default_value_yesno = $_POST["default_value_yesno_" . $langDef];
        $default_prefix = $_POST["txt_prefix_" . $langDef];
        $default_suffix = $_POST["txt_suffix_" . $langDef];

        //replace spaces from code and make it lower
        $_POST['code'] = str_replace(' ', '_', Str::lower($_POST['code']));
        
        if($_POST['frontend_input'] == 'select' OR $_POST['frontend_input'] == 'multiselect' OR $_POST['frontend_input'] == 'boolean'){
            $_POST['is_unique'] = 1;
        }
        

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if ($lang != $langDef) {
                if ($_POST["default_value_text_" . $lang] == "")
                    $_POST["default_value_text_" . $lang] = $default_value_text;
                if ($_POST["default_value_date_" . $lang] == "")
                    $_POST["default_value_date_" . $lang] = $default_value_date;
                if ($_POST["default_value_textarea_" . $lang] == "")
                    $_POST["default_value_textarea_" . $lang] = $default_value_textarea;
                if ($_POST["default_value_yesno_" . $lang] == "")
                    $_POST["default_value_yesno_" . $lang] = $default_value_yesno;

                /* if ($_POST["txt_prefix_" . $lang] == "")
                  $_POST["txt_prefix_" . $lang] = $default_prefix;
                  if ($_POST["txt_suffix_" . $lang] == "")
                  $_POST["txt_suffix_" . $lang] = $default_suffix; */
            }
            if ($_POST["default_value_date_" . $lang] == "") {
                $_POST["default_value_date_" . $lang] = null;
            }
            $_POST['slug_'.$lang] = Str::slug($_POST['name_'.$lang]);
        }

        if(strlen($_POST['nav_code']) > 0){
            $_POST['nav_code'] = \Str::upper($_POST['nav_code'][0]);
        }


        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }

    function _before_delete(&$model) {
        
    }

    function _after_create($model) {
        $this->_handleAttributeOptions($model);
        $model->flushCache();
    }

    function _after_update($model) {
        $this->_handleAttributeOptions($model);
        $model->flushCache();
    }

    function _handleAttributeOptions($model) {
        $languages = \Mainframe::languages();
        
        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $type = \Input::get('frontend_input');
        if (!in_array($type, array('select', 'multiselect'))) {
            return;
        }
        $old = \Input::get('option');
        $default = \Input::get('default', []);
        $value = $old['value'];
        $order = $old['order'];
        $delete = $old['delete'];
        $ids = $old['ids'];

        $auto_position = \Input::get('auto_position');
        $auto_position_by = \Input::get('auto_position_by');
        $delete_attributes = \Input::get('delete_attributes');

        if ($delete_attributes == 1) {
            $rows = AttributeOption::where('attribute_id', $model->id)->get();
            foreach ($rows as $row) {
                $row->delete();
            }
        }

        if (!empty($value)) {


        foreach ($value as $option => $obj) {
            $id = isset($ids[$option]) ? $ids[$option] : null;
            if ($delete[$option] != '1') {
                $ds = array();
                foreach ($languages as $lang) {
                    $ds['name_' . $lang->id] = $obj[$lang->id];
                    $ds['slug_' . $lang->id] = '';
                    if($lang->id != $langDef AND $ds['name_' . $lang->id] == ''){
                        $ds['name_' . $lang->id] = $ds['name_' . $langDef];
                    }
                }

                if ($id > 0 AND $delete_attributes == 0) {
                    $ao = AttributeOption::find($id);
                } else {
                    $ao = new AttributeOption();
                }
                $ao->setDataSource($ds);
                $ao->uname = $obj['u'];
                if($ao->uname == ""){
                    $ao->uname = Str::upper($ds['name_'.$langDef][0]);
                }
                $ao->uslug = Str::slug($ao->uname);
                $ao->attribute_id = $model->id;
                $ao->position = (int) $order[$option];
                $ao->is_default = (in_array($option, $default)) ? 1 : 0;

                \Utils::log($ao->getAttributes(), "ATTRIBUTES");

                $ao->save();
            } else {
                if ($id > 0) {
                    $ao = AttributeOption::find($id);
                    $ao->delete();
                    AttributeOption_Lang::where('attribute_option_id', $id)->delete();
                }
            }
        }

        }

        /* Rerorder items */
        if ($auto_position == 'Y') {
            if ($auto_position_by == 'u') { //from universal column
                $rows = DB::table("attributes_options")->where('attribute_id', $model->id)->orderBy('uname')->get(['id']);
            } else { //from lang column
                $rows = DB::table("attributes_options")
                        ->leftJoin('attributes_options_lang', 'attribute_option_id', '=', 'id')
                        ->where('lang_id', $auto_position_by)
                        ->where('attribute_id', $model->id)
                        ->orderBy('name')
                        ->get(['id']);
            }
            $counter = 0;
            foreach ($rows as $ao) {
                $counter++;
                DB::table("attributes_options")->where('id', $ao->id)->update(['position' => $counter]);
            }
        }

        AttributeOption::reinitCache($model->id);
    }

    function postImportDataset($id) {
        $html = Helper::getAttributesTable($id, false);
        $data = array('success' => true, 'msg' => 'Set attributi caricato correttamente', 'html' => $html);
        return Json::encode($data);
    }

    function getSummary($id) {
        $html = Helper::getAttributesTableSummary($id);
        echo $html;
    }

    function postAddOption($attribute_id) {
        $languages = \Mainframe::languages();
        $attribute_option_uname = \Input::get("attribute_option_uname");
        $result = false;
        if($attribute_option_uname == ''){
            $error = "Non puoi inserire una opzione vuota";
            return Json::encode(compact("error","success"));
        }

        $languages = \Mainframe::languages();

        $ds = array();
        foreach ($languages as $lang) {
            $ds['name_' . $lang->id] = \Input::get("attribute_option_name_" . $lang->id);
            $ds['slug_' . $lang->id] = '';
        }


        $ao = new AttributeOption();
        $ao->setDataSource($ds);
        $ao->uname = $attribute_option_uname;
        $ao->uslug = Str::slug($ao->uname);
        $ao->attribute_id = $attribute_id;
        $ao->position = 0;
        $ao->is_default = 0;

        \Utils::log($ao->getAttributes(), "ATTRIBUTES");

        $ao->save();
        
        $attribute = Attribute::find($attribute_id);
        $success = true;
        $type = $attribute->frontend_input;
        $value = $ds['name_'. \Core::getLang()];
        if($ds['name_'. \Core::getLang()] == '' ){
            $value = $ao->uname;
        }
        $option = "<option value=\"$ao->id\" selected>$value</option>";

        $rows = DB::table("attributes_options")
                ->leftJoin('attributes_options_lang', 'attribute_option_id', '=', 'id')
                ->where('lang_id', \Core::getLang())
                ->where('attribute_id', $attribute_id)
                ->orderBy('name')
                ->get(['id']);

        $counter = 0;
        foreach ($rows as $ao) {
            $counter++;
            DB::table("attributes_options")->where('id', $ao->id)->update(['position' => $counter]);
        }
        
        
        return Json::encode(compact("option","success","type"));
    }
    
    
    function postListAvailable(){
        //\Utils::watch();
        $ids = \Input::get("ids");
        $rows = Attribute::dataset('default',$ids);
        $success = true;
        $data = $rows->toArray();
        return Json::encode(compact("data","success"));
    }
    
    
    function postOptions($attribute_id){
        //\Utils::watch();
        $attribute = Attribute::find($attribute_id);
        if($attribute){
            $rows = $attribute->options();
            $success = true;
            $data = $rows->toArray();
        }else{
            $success = false;
            $data = [];
        }

        return Json::encode(compact("data","success"));
    }

}
