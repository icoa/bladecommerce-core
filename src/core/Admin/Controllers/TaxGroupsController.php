<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 29-gen-2014 15.14.45
 */

class TaxGroupsController extends BackendController {

    public $component_id = 50;
    public $title = 'Gestione Regole Tasse';
    public $page = 'taxgroups.index';
    public $pageheader = 'Gestione Regole Tasse';
    public $iconClass = 'font-columns';
    public $model = 'TaxGroup';
    protected $rules = array(        
        'name' => 'required',
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Regole Tasse');
        $this->toFooter("js/echo/taxgroups.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/taxgroups.js");
        $this->page = 'taxgroups.trash';
        $this->pageheader = 'Cestino Regole Tasse';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->toFooter("js/echo/taxgroups.js");
        $this->page = 'taxgroups.create';
        $this->pageheader = 'Nuova Regola Tassa';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->toFooter("js/echo/taxgroups.js");
        $this->page = 'taxgroups.create';
        $this->pageheader = 'Modifica Regola Tassa';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);        
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::select('id', 'name', 'active', 'created_at');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::date($data['created_at']);
                        })
                        ->edit_column('name', function($data) {
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->select('id', 'name', 'active', 'deleted_at');

        return \Datatables::of($pages)                        
                        ->edit_column('deleted_at', function($data) {
                            return \Format::date($data['deleted_at']);
                        })
                        ->edit_column('name', function($data) {
                            return "<strong>{$data['name']}</strong>";
                        })
                        ->edit_column('active', function($data) {
                            return ($data['active'] == 1) ? '<span class="label label-success">Abilitata</span>' : '<span class="label label-important">Disabilitata</span>';
                        })   
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })                        
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {                 
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $model = $this->model;

        \Input::replace($_POST);

    }
    


    function postList() {
        $rows = \Mainframe::brands();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

}
