<?php

class BrandsController extends MultiLangController {

    public $component_id = 18;
    public $title = 'Gestione Brand';
    public $page = 'brands.index';
    public $pageheader = 'Gestione Brand';
    public $iconClass = 'font-columns';
    public $model = 'Brand';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:brands_lang,slug,brand_id'
    );

    function __construct() {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex() {
        $this->addBreadcrumb('Elenco Brand');
        $this->toFooter("js/echo/brands.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash() {
        $this->toFooter("js/echo/brands.js");
        $this->page = 'brands.trash';
        $this->pageheader = 'Cestino Brand';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate() {
        $this->page = 'brands.create';
        $this->pageheader = 'Nuovo Brand';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id) {
        $this->page = 'brands.create';
        $this->pageheader = 'Modifica Brand';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable() {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('brands_lang', 'id', '=', 'brands_lang.brand_id')
                ->where('lang_id', $lang_id)
                ->leftJoin('products', 'products.brand_id', '=', 'brands.id')
                ->whereNull('products.deleted_at')
                ->groupBy("brands.id")
                ->select('brands.id', 'brands_lang.name as name', 'brands_lang.slug', 'brands_lang.published', 'brands.created_at', 'brands.position', DB::raw("count(products.id) as products"), 'brands_lang.ldesc', 'brands_lang.lang_id', 'brands_lang.image_thumb');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('products', function($data) {
                            return "<b><a href='/admin/products/?brand_id=" . $data['id'] . "'>" . $data['products'] . "</a></b>";
                        })
                        ->edit_column('name', function($data) {
                            $img = public_path() . $data['image_thumb'];
                            if ($data['image_thumb'] != '' AND \File::exists($img)) {
                                $img = "<img height='30' class='pull-right mr15' src='" . $data['image_thumb'] . "' />";
                            } else {
                                $img = '';
                            }
                            $link = \URL::action($this->action("getEdit"), $data['id']);
                            return "<strong><a href='$link'>{$data['name']}</a></strong>$img";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_actions($data);
                        })
                        ->edit_column('published', function($data) {
                            return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
                        })
                        ->edit_column('position', function($data, $index, $obj) {
                            return $this->column_position($data, $index, $obj);
                        })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->remove_column('image_thumb')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    public function getTabletrash() {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('brands_lang', 'id', '=', 'brands_lang.brand_id')
                ->where('lang_id', $lang_id)
                ->leftJoin('products', 'products.brand_id', '=', 'brands.id')
                ->groupBy("brands.id")
                ->select('brands.id', 'brands_lang.name', 'brands_lang.slug', 'brands.position', 'brands.deleted_at', 'brands.created_at', DB::raw("count(products.id) as products"), 'brands_lang.ldesc', 'brands_lang.lang_id');

        return \Datatables::of($pages)
                        ->edit_column('created_at', function($data) {
                            return \Format::datetime($data['created_at']);
                        })
                        ->edit_column('products', function($data) {
                            return "<b><a href='/admin/products/?brand_id=" . $data['id'] . "'>" . $data['products'] . "</a></b>";
                        })
                        ->edit_column('deleted_at', function($data) {
                            return \Format::datetime($data['deleted_at']);
                        })
                        ->edit_column('name', function($data) {
                            return "<strong>{$data['name']}</strong>";
                        })
                        ->add_column('actions', function($data) {
                            return $this->column_trash_actions($data);
                        })
                        ->remove_column('ldesc')
                        ->remove_column('lang_id')
                        ->edit_column('id', function($data) {
                            return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id']."</label>";
                        })
                        ->make();
    }

    function _before_create() {
        $this->_prepare();
    }

    function _before_update($model) {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _prepare() {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];
        $defaultImage = $_POST["image_default_" . $langDef];
        $defaultImageThumb = $_POST["image_thumb_" . $langDef];
        $defaultImageZoom = $_POST["image_zoom_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
                if ($_POST["image_default_" . $lang] == "")
                    $_POST["image_default_" . $lang] = $defaultImage;
                if ($_POST["image_thumb_" . $lang] == "")
                    $_POST["image_thumb_" . $lang] = $defaultImageThumb;
                if ($_POST["image_zoom_" . $lang] == "")
                    $_POST["image_zoom_" . $lang] = $defaultImageZoom;
            }
            /*$source = $_POST["name_" . $lang];
            $target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }
            $target = $_POST["h1_" . $lang];
            if ($target == "") {
                $_POST["h1_" . $lang] = $source;
            }*/
        }

        $model = $this->model;
        if ((int) $_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }

    function postList() {
        $rows = \Mainframe::brands()->toArray();        
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

    protected function checkRecords(&$messages,$obj){
        parent::checkRecords($messages,$obj);
        if($obj){
            $translations = $obj->translations;
            foreach($translations as $tr){
                if(isset($tr->image_default) AND $tr->image_default == ''){
                    $messages[] = "l'immagine di default per la lingua <strong class='label'>$tr->lang_id</strong> non è impostata - si consiglia di scegliere un'immagine almeno per una lingua";
                }
                if(isset($tr->image_thumb) AND $tr->image_thumb == ''){
                    $messages[] = "l'immagine di miniatura per la lingua <strong class='label'>$tr->lang_id</strong> non è impostata - si consiglia di scegliere un'immagine almeno per una lingua";
                }
            }
        }
    }

}
