<?php

class TrendsController extends MultiLangController
{

    public $component_id = 58;
    public $title = 'Gestione Gruppi personalizzati';
    public $page = 'trends.index';
    public $pageheader = 'Gestione Gruppi personalizzati';
    public $iconClass = 'font-columns';
    public $model = 'Trend';
    protected $rules = array();
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'unique_lang:trends_lang,slug,trend_id'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Gruppi personalizzati');
        $this->toFooter("js/echo/trends.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/trends.js");
        $this->page = 'trends.trash';
        $this->pageheader = 'Cestino Gruppi personalizzati';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'trends.create';
        $this->pageheader = 'Nuovo Gruppo personalizzati';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->page = 'trends.create';
        $this->pageheader = 'Modifica Gruppo personalizzati';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $model = $this->model;

        $pages = $model::leftJoin('trends_lang', 'id', '=', 'trends_lang.trend_id')
            ->where('lang_id', $lang_id)
            ->leftJoin('trends_products', 'trends_products.trend_id', '=', 'trends.id')
            ->groupBy("trends.id")
            ->select('trends.id', 'trends_lang.name as name', 'trends_lang.slug', 'trends_lang.published', 'trends.created_at', 'trends.position', 'trends_lang.ldesc', 'trends_lang.lang_id', DB::raw("count(trends_products.product_id) as products"));

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $public_url = Link::to('trend', $data['id']);
                $action = $this->action('postEmpty', true, $data['id']);
                $cnt = "<span style='float: right'><span class='label'>{$data['products']}</span> | <a href='javascript:;' onclick=\"EchoTable.confirmAction('$action')\">Svuota</a> | <a href='$public_url' target='_blank'>Apri nel frontend</a></span>";
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>$cnt";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->edit_column('position', function ($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->remove_column('products')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('trends_lang', 'id', '=', 'trends_lang.trend_id')
            ->where('lang_id', $lang_id)
            ->leftJoin('trends_products', 'trends_products.trend_id', '=', 'trends.id')
            ->groupBy("trends.id")
            ->select('trends.id', 'trends_lang.name', 'trends_lang.slug', 'trends.position', 'trends.deleted_at', 'trends.created_at', 'trends_lang.ldesc', 'trends_lang.lang_id', DB::raw("count(trends_products.product_id) as products"));

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('name', function ($data) {
                $cnt = "<span class='label' style='float: right'>{$data['products']}</span>";
                return "<strong>{$data['name']}</strong>$cnt";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];
        $defaultImage = $_POST["image_default_" . $langDef];
        $defaultImageThumb = $_POST["image_thumb_" . $langDef];
        $defaultImageZoom = $_POST["image_zoom_" . $langDef];

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "")
                    $_POST["name_" . $lang] = $defaultName;
                if ($_POST["image_default_" . $lang] == "")
                    $_POST["image_default_" . $lang] = $defaultImage;
                if ($_POST["image_thumb_" . $lang] == "")
                    $_POST["image_thumb_" . $lang] = $defaultImageThumb;
                if ($_POST["image_zoom_" . $lang] == "")
                    $_POST["image_zoom_" . $lang] = $defaultImageZoom;
            }
            $source = $_POST["name_" . $lang];
            $target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }
            $target = $_POST["h1_" . $lang];
            if ($target == "") {
                $_POST["h1_" . $lang] = $source;
            }
        }

        $model = $this->model;
        if ((int)$_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }

    function postList()
    {
        $rows = \Mainframe::brands()->toArray();
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

    function postEmpty($id)
    {
        $success = true;
        $msg = 'Gruppo svuotato con successo';
        $error = null;
        try {
            if (is_numeric($id)) {
                DB::table('trends_products')->where('trend_id', $id)->delete();
            }
        } catch (Exception $e) {
            $success = false;
            $error = $e->getMessage();
        }
        return Response::json(compact('success', 'msg', 'error'));

    }

}
