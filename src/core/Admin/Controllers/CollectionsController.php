<?php

class CollectionsController extends MultiLangController
{

    public $component_id = 20;
    public $title = 'Gestione Collezioni';
    public $page = 'collections.index';
    public $pageheader = 'Gestione Collezioni';
    public $iconClass = 'font-columns';
    public $model = 'Collection';
    protected $rules = array(
        'brand_id' => 'required|not_in:0',
        'category_id' => 'required_if:category_mode,custom'
    );
    protected $lang_rules = array(
        'name' => 'required',
        'slug' => 'required|unique_lang:collections_lang,slug,collection_id'
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->toFooter("js/echo/collections.js");
    }


    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Collezioni');
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->page = 'collections.trash';
        $this->pageheader = 'Cestino Collezioni';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->page = 'collections.create';
        $this->pageheader = 'Nuova Collezione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->page = 'collections.create';
        $this->pageheader = 'Modifica Collezione';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->fillLanguages();
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {

        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        \Utils::watch();

        $model = $this->model;


        $pages = $model::leftJoin('collections_lang', 'id', '=', 'collections_lang.collection_id')
            ->leftJoin('brands_lang', function ($join) use ($lang_id) {
                $join->on('brands_lang.brand_id', '=', 'collections.brand_id')->on('brands_lang.lang_id', "=", 'collections_lang.lang_id');
            })
            ->leftJoin('categories_lang', function ($join) use ($lang_id) {
                $join->on('categories_lang.category_id', '=', 'collections.category_id')->on('categories_lang.lang_id', "=", 'collections_lang.lang_id');
            })
            ->where('collections_lang.lang_id', $lang_id)
            ->leftJoin('products', 'products.collection_id', '=', 'collections.id')
            ->whereNull('products.deleted_at')
            ->groupBy("collections.id")
            ->select('collections.id', 'collections_lang.name', 'brands_lang.name as brand_name', 'collections_lang.slug', 'collections_lang.published', 'collections.created_at', 'collections.position', DB::raw("count(products.id) as products"), 'collections_lang.ldesc AS ldesc', 'collections_lang.lang_id AS lang_id', 'collections.brand_id', 'categories_lang.name as category');

        //$sql = $pages->toSql();
        //Utils::log($sql,"getTable SQL");

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('products', function ($data) {
                return "<b><a href='/admin/products/?collection_id=" . $data['id'] . "'>" . $data['products'] . "</a></b>";
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $a = ($data['category'] != '') ? " (Categoria: " . $data['category'] . ")" : "";
                return "<strong><a href='$link'>{$data['name']}$a</a></strong>";
            })
            ->edit_column('brand_name', function ($data) {
                $link = \URL::action("BrandsController@getEdit", $data['brand_id']);
                return "<strong><a href='$link'>{$data['brand_name']}</a></strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('published', function ($data) {
                return ($data['published'] == 1) ? '<span class="label label-success">Pubblicata</span>' : '<span class="label label-important">Sospesa</span>';
            })
            ->edit_column('position', function ($data, $index, $obj) {
                return $this->column_position($data, $index, $obj);
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->remove_column('brand_id')
            ->remove_column('category')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getTabletrash()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('collections_lang', 'id', '=', 'collections_lang.collection_id')
            ->leftJoin('brands_lang', 'brands_lang.brand_id', '=', 'collections.brand_id')
            ->where('collections_lang.lang_id', $lang_id)
            ->where('brands_lang.lang_id', $lang_id)
            ->leftJoin('products', 'products.collection_id', '=', 'collections.id')
            ->groupBy("collections.id")
            ->select('collections.id', 'collections_lang.name AS name', 'brands_lang.name AS brand_name', 'collections_lang.slug', 'collections.deleted_at', 'collections.created_at', 'collections.position', DB::raw("count(products.id) as products"), 'collections_lang.ldesc AS ldesc', 'collections_lang.lang_id AS lang_id', 'collections.brand_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('products', function ($data) {
                return "<b><a href='/admin/products/?collection_id=" . $data['id'] . "'>" . $data['products'] . "</a></b>";
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::datetime($data['deleted_at']);
            })
            ->edit_column('brand_name', function ($data) {
                $link = \URL::action("BrandsController@getEdit", $data['brand_id']);
                return "<strong><a href='$link'>{$data['brand_name']}</a></strong>";
            })
            ->edit_column('name', function ($data) {
                return "<strong>{$data['name']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->remove_column('brand_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->lang_rules['slug'] = $this->lang_rules['slug'] . ',' . $model->id;
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $langDef = Cfg::get('DEFAULT_LANGUAGE');

        $defaultName = $_POST["name_" . $langDef];
        $defaultImage = $_POST["image_default_" . $langDef];
        $defaultImageThumb = $_POST["image_thumb_" . $langDef];
        $defaultImageZoom = $_POST["image_zoom_" . $langDef];

        $category = false;
        $category_mode = $_POST['category_mode'];
        if ($category_mode == 'auto') {
            $_POST['category_id'] = 0;
        }

        $category_id = $_POST['category_id'];

        if ($category_id > 0) {
            $category = Category::find($category_id);
            $category->fillLanguages();
        }

        $languages = \Mainframe::languagesCodes();
        foreach ($languages as $lang) { //set the metatitle = name if the former is null            
            if ($lang != $langDef) {
                if ($_POST["name_" . $lang] == "") $_POST["name_" . $lang] = $defaultName;
                if ($_POST["image_default_" . $lang] == "") $_POST["image_default_" . $lang] = $defaultImage;
                if ($_POST["image_thumb_" . $lang] == "") $_POST["image_thumb_" . $lang] = $defaultImageThumb;
                if ($_POST["image_zoom_" . $lang] == "") $_POST["image_zoom_" . $lang] = $defaultImageZoom;
            }
            $source = $_POST["name_" . $lang];
            /*$target = $_POST["metatitle_" . $lang];
            if ($target == "") {
                $_POST["metatitle_" . $lang] = $source;
            }*/
            $slug = $_POST["slug_" . $lang];
            if ($slug == "") {
                if ($category) {
                    $source = $source . " " . $category->{"slug_" . $lang};
                }
                $_POST["slug_" . $lang] = Str::slug(trim($source));
            }
        }

        $model = $this->model;
        if ((int)$_POST['position'] == 0)
            $_POST['position'] = $model::position();

        \Input::replace($_POST);

    }


    function postList($brand_id)
    {
        $rows = \Mainframe::collections($brand_id);
        $data = array('success' => true, 'data' => $rows);
        return Json::encode($data);
    }

}