<?php

/* 
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 20-gen-2014 17.06.59
 */

class ProductsImagesController extends BackendController{
    
    function postChangeFlag($image_id,$flag,$status){            
        
        $image = ProductImage::find($image_id);
        if($flag == 'cover'){
            DB::table("images")->where('product_id', '=', $image->product_id)->update(array('cover' => 0));
        }
        $image->{$flag} = $status;
        $image->update();
        $this->setProductDefaultImg($image->product_id);
        $response = array('success' => true);
        return Json::encode($response);
    }
    
    function postRemove($image_id){
        //\Utils::watch();
        $image = ProductImage::find($image_id);
        try{
            $filename = public_path()."/assets/products/".$image->filename;
            File::delete($filename);
            $image->delete();
            $response = array('success' => true);
        } catch (Exception $ex) {
            $response = array('success' => false);
        }
        
        return Json::encode($response);
    }


    function postTouch($image_id){
        //\Utils::watch();
        $image = ProductImage::find($image_id);
        try{
            $filename = public_path()."/assets/products/".$image->filename;
            touch($filename);
            $response = array('success' => true);
            Utils::log("File: $filename touched succesfully");
        } catch (Exception $ex) {
            $response = array('success' => false);
        }

        return Json::encode($response);
    }
    
    
    function postReload($product_id){
        $response = array('success' => true, 'html' => ProductHelper::getImagesTableForProduct($product_id));
        return Json::encode($response);
    }
    
    function postSaveTable($product_id){
        $languages = \Mainframe::languagesCodes();
        $image_ids = Input::get("image_ids");
        $ids = explode(",", $image_ids);
        $counter = 0;
        
        foreach ($ids as $id) {
            $counter++;
            $ds = array();
            
            foreach($languages as $lang){
                $ds["legend_".$lang] = Input::get($id."_legend_".$lang);
            }
            $ds['position'] = $counter;
            
            $image = ProductImage::find($id);            
            if($image){
                $image->setDataSource($ds);            
                $image->save();
            }
            
        }
        //\Utils::watch();
        $totalImages = DB::table("images")->where("product_id",$product_id)->count();
        DB::table("products")->where("id",$product_id)->update(["cnt_images" => $totalImages]);

        $this->setProductDefaultImg($product_id);
        
        $response = array('success' => true);
        return Json::encode($response);
    }


    private function setProductDefaultImg($product_id){
        $image_id = DB::table('images')->where('product_id',$product_id)->where('cover',1)->pluck('id');
        if($image_id > 0){
            DB::table('products')->where('id',$product_id)->update(['default_img' => $image_id]);
        }
    }
    
    
    
}