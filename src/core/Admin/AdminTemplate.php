<?php

namespace Core\Admin;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */
use Theme, Request;

class AdminTemplate
{

    protected $scope;
    protected $data = [];

    function __construct()
    {
        \Theme::theme('admin');
    }

    function img($url)
    {
        return Theme::asset()->url($url);
    }

    function boolean($value)
    {
        $v = ($value == 1) ? 1 : 0;

        $src = '<img style="vertical-align:middle" src="' . "/media/admin/markbool$v.gif" . '"/>';
        return $src;
    }

    function flag($lang)
    {
        $src = '<img style="vertical-align:middle" src="' . \AdminTpl::img("images/flags/{$lang}.jpg") . '"/>';
        return $src;
    }

    function iconNav($url, $tag = false)
    {
        $path = Theme::asset()->url("images/icons/mainnav/" . $url);
        if ($tag) {
            $path = '<img src="' . $path . '" alt="" />';
        }
        return $path;
    }

    function css($url)
    {
        return Theme::asset()->url("css/$url");
    }

    function js($url)
    {
        return Theme::asset()->url("js/$url");
    }

    function root()
    {
        return Request::root() . "/" . Theme::path();
    }

    function path()
    {
        return public_path() . "/" . Theme::path();
    }

    function thumb($src, $w, $h)
    {
        $root = Request::root();
        $src = str_replace($root, $root . "/images/2/$w/$h/5", $src);
        return $src;
    }

    function design()
    {
        $rows = \Mainframe::modulePositionsRows();
        $html = '';

        foreach ($rows as $row) {
            $html .= $this->row($row);
        }
        return $html;
    }

    function row($row_index)
    {
        $positions = \Mainframe::modulePositionsDesign($row_index);
        $html = '';

        foreach ($positions as $p) {
            $html .= $this->position($p);
        }
        $tpl = <<<TPL
<div class="row-fluid" data-id="$row_index">
    $html
</div>
TPL;

        return $tpl;
    }

    function position($obj)
    {
        $name = $obj->name;
        $html = '';
        $modules = \Mainframe::modules($name);
        foreach ($modules as $m) {
            $html .= "<li class='ui-state-highlight' data-id='$m->id'><i class='icon font-sort'></i>$m->name ($m->mod_type) <a href='#' class='pull-right' data-action='remove-module' title='Rimuovi modulo'><i class='icon font-remove'></i></a><a href='#' class='pull-right' data-action='edit-module' title='Modifica modulo'><i class='icon font-cog'></i></a></li>";
        }
        $tpl = <<<TPL
<div class="span{$obj->width} position" id="position-{$name}" data-name="{$name}">
    <div class="well block">
        <div class="navbar">
            <div class="navbar-inner"><h5>{$name}</h5></div>
            <div class="body">
                <div class="droppable">
                <ul class="sortable connectedSortable">$html</ul>
                </div>
            </div>
        </div>
    </div>
</div>
TPL;

        return $tpl;
    }


    function afterHead()
    {
        $str = '';
        $responses = \Event::fire('admin.template.afterHead');
        if (is_array($responses)) {
            $str .= implode(PHP_EOL, $responses);
        }
        return trim($str);
    }


    function afterFooter()
    {
        $str = '';
        $responses = \Event::fire('admin.template.afterFooter');
        if (is_array($responses)) {
            $str .= implode(PHP_EOL, $responses);
        }
        return trim($str);
    }

    function setScope($scope)
    {
        $this->scope = $scope;
    }

    function getScope()
    {
        return $this->scope;
    }

    function setData($key, $value)
    {
        $this->data[$key] = $value;
    }

    function appendData($key, $value, $separator = ' ')
    {
        if (isset($this->data[$key])) {
            $this->data[$key] = $this->data[$key] . $separator . $value;
        } else {
            $this->setData($key, $value);
        }
    }

    function prependData($key, $value, $separator = ' ')
    {
        if (isset($this->data[$key])) {
            $this->data[$key] = $separator . $this->data[$key] . $value;
        } else {
            $this->setData($key, $value);
        }
    }

    function getData($key, $default_value = null)
    {
        if (isset($this->data[$key])) {
            return $this->data[$key];
        }
        return $default_value;
    }

    function setMultipleData($array)
    {
        $this->data = $this->data + $array;
    }


    function getFrontStats()
    {
        $data = [];
        for ($i = 30; $i >= -1; $i--) {
            $data[] = date("Y-m-d", strtotime(" -{$i} days"));
        }

        $start = $data[0];
        $end = last($data);

        $carts = \Cart::whereBetween('created_at', [$start, $end])->selectRaw('date(created_at) as date, count(id) as aggregate')->groupBy('date')->orderBy('date')->get();
        $orders = \Order::whereNull('parent_id')->whereBetween('created_at', [$start, $end])->selectRaw('date(created_at) as date, count(id) as aggregate')->groupBy('date')->orderBy('date')->get();

        $series1 = [];
        $series2 = [];
        $days = [];

        foreach ($data as $date) {
            $total_carts = 0;
            $total_orders = 0;

            foreach($carts as $cart){
                if($cart->date == $date)
                    $total_carts = $cart->aggregate;
            }

            foreach($orders as $order){
                if($order->date == $date)
                    $total_orders = $order->aggregate;
            }

            $time = strtotime($date) * 1000;
            $series1[] = "$time,$total_carts";
            $series2[] = "$time,$total_orders";
            $days[] = $time;
        }


        $min = min($days);
        $max = last($days);

        return compact('series1', 'series2', 'min', 'max');

    }


    function getFrontStats2()
    {
        $data = [];
        $max = time();
        for ($i = 30; $i >= 0; $i--) {
            $data[] = date("Y-m-d", strtotime(" -{$i} days"));
        }

        //\Utils::watch();

        $series1 = [];
        $series2 = [];
        $days = [];
        $sign = -1;

        foreach ($data as $date) {
            $carts = \DB::table('cart')->where('created_at', 'like', "%$date%")->count('id');
            $orders = \DB::table('orders')->where('created_at', 'like', "%$date%")->count('id');
            //\Utils::log("DATE: $date | CARTS: $carts | ORDERS: $orders");
            $day = (int)date("d", strtotime($date));
            if ($day == 1) {
                $sign = 1;
            }
            $day = $sign * $day;
            if ($day < 0) {
                $day += 29;
            }
            $series1[] = "$day,$carts";
            $series2[] = "$day,$orders";
            $days[] = $day;
        }


        $min = min($days);
        $max = last($days);

        return compact('series1', 'series2', 'min', 'max');

    }


}