<?php

namespace Core\Admin;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */
use Sentry,
    AdminTpl,
    File;

use Robbo\Presenter\Presenter;

class AdminUser
{

    private $user;

    function __construct()
    {
        $this->user = new AdminUserModel(Sentry::getUser());
    }

    function get()
    {
        return $this->user;
    }

    function thumb($w, $h)
    {
        return AdminTpl::thumb($this->user->profileImg, $w, $h);
    }

    function isNegoziando()
    {
        $user = $this->get();
        return $user->negoziando == 1;
    }

    function isOperator()
    {
        return ($this->isNegoziando() == false and $this->isAffiliate() == false);
    }

    function isAffiliate()
    {
        $user = $this->get();
        return $user->affiliate_id > 0;
    }

    /**
     * @return \Cartalyst\Sentry\Users\UserInterface
     */
    function user(){
        return Sentry::getUser();
    }
}


