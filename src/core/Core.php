<?php

namespace Core;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 12-giu-2013 11.33.03
 */

use Config,
    Log,
    DB;
use Illuminate\Support\Facades\Session;
use services\Morellato\Database\Sql\Connection as NegoziandoConnection;
use Theme;

class Core
{

    public $lang;
    public $currency = null;
    private $currencies;
    private $negoziandoConnection;

    const ATTR_GENDER = 214;
    const ATTR_STYLE = 215;


    function __construct()
    {
        $this->lang = \App::getLocale();
        $this->currencies = \Cache::rememberForever('echo_currencies', function () {
            return \Currency::where('active', 1)->get();
        });


        //$this->currency = $this->getDefaultCurrency();
    }

    function getLang($lang = 'default')
    {
        $defaultLang = $this->lang;
        $savedLang = \Session::get('blade_lang');
        if ($savedLang != null AND $savedLang != '') $defaultLang = $savedLang;
        //return ($lang == 'default' OR $lang === null) ? \App::getLocale() : $lang;
        return ($lang == 'default' OR $lang === null) ? $defaultLang : $lang;
    }

    function getDefaultLang()
    {
        return \Cfg::get('DEFAULT_LANGUAGE', \App::getLocale());
    }

    function getEmulatedFiles()
    {
        return [
            'robots.txt',
            'sitemap.xml',
            'products.xml',
            'boilerplates.xml',
            'categories.xml',
            'custom.xml',
            'feed.xml',
            'rss.xml',
            'atom.xml',
            'zanox.csv',
        ];
    }

    function isEmulatedFile($file)
    {
        if (\Str::contains($file, 'googlemerchant')) return true;
        if (\Str::contains($file, 'zanox')) return true;
        if (\Str::contains($file, 'selligent')) return true;
        $valid = $this->getEmulatedFiles();
        return in_array($file, $valid);
    }

    function getLanguages()
    {
        return \Mainframe::languagesCodes(true);
    }

    /**
     * @return array
     */
    function getSeoEnabledLanguages()
    {
        return (array)config('seo.enabled_languages', ['it', 'en', 'es', 'fr']);
    }

    function getLangJS()
    {
        $languages = \Mainframe::languagesCodes();
        $str = "[";
        foreach ($languages as $lang) {
            $str .= "'$lang',";
        }
        $str = rtrim($str, ",") . "]";
        return $str;
    }

    function getCurrencyByIso($iso_code)
    {
        $iso_code = \Str::upper($iso_code);
        $currencies = $this->currencies;
        foreach ($currencies as $c) {
            if ($c->iso_code == $iso_code) {
                return $c;
            }
        }
        return $this->getDefaultCurrency();
    }

    function convert($value, $to, $from = 'default')
    {
        if ($value == 0) return 0;
        $source = null;
        $target = null;
        $default = $this->getDefaultCurrency();
        if ($from == 'default') {
            $source = $default;
        } elseif (is_object($from)) {
            $source = $from;
        } elseif (is_numeric($from)) {
            $source = \Currency::getPublicObj($from);
        } else {
            $source = $this->getCurrencyByIso($from);
        }

        if ($to == 'default') {
            $target = $default;
        } elseif (is_numeric($to)) {
            $target = \Currency::getPublicObj($to);
        } elseif (is_object($to)) {
            $target = $to;
        } else {
            $target = $this->getCurrencyByIso($to);
        }

        $ratio = $target->conversion_rate / $source->conversion_rate;
        $result = $value * $ratio;
        return $result;
    }

    function getCurrencySign()
    {
        $currency = ($this->currency) ? \Currency::getObj($this->currency) : $this->getDefaultCurrency();
        return $currency->sign;
    }

    function getCurrencyCode()
    {
        $currency = ($this->currency) ? \Currency::getObj($this->currency) : $this->getDefaultCurrency();
        return $currency->iso_code;
    }

    function setCurrency($currency)
    {
        //\Utils::log($currency,__METHOD__);
        $currencyObj = \Currency::getPublicObj($currency);
        //\Utils::log($currencyObj,__METHOD__);
        $this->currency = (isset($currencyObj) AND $currencyObj->active == 1) ? $currencyObj->id : \Cfg::get("CURRENCY_DEFAULT");
        \Session::set('blade_currency', $this->currency);
        //\Utils::log("SETTING CURRENCY: $this->currency",__METHOD__);
    }

    function getCurrency()
    {
        $savedCurrency = \Session::get('blade_currency');
        if ($savedCurrency > 0) return $savedCurrency;
        return isset($this->currency) ? $this->currency : \Cfg::get('CURRENCY_DEFAULT');
    }

    function setLang($lang)
    {
        $this->lang = $lang;
        \App::setLocale($lang);
        if ($lang == 'it') {
            setlocale(LC_TIME, $this->isWin() == false ? 'it_IT.utf8' : 'ita,italy');
        }
        if ($lang == 'es') {
            setlocale(LC_TIME, $this->isWin() == false ? 'es_ES.utf8' : 'esp,espana');
        }
        if ($lang == 'de') {
            setlocale(LC_TIME, $this->isWin() == false ? 'de_DE.utf8' : 'deu,germany');
        }
        if ($lang == 'fr') {
            setlocale(LC_TIME, $this->isWin() == false ? 'fr_FR.utf8' : 'fra,france');
        }
        \Session::set('blade_lang', $lang);
    }

    function getDefaultCurrency()
    {
        $id_default_currency = \Cfg::get('CURRENCY_DEFAULT');
        $key = "currency-$id_default_currency";
        if (\Cache::has($key)) {
            $currency = \Cache::get($key);
        } else {
            $currency = \Currency::getObj($id_default_currency);
            \Cache::put($key, $currency, 60 * 24);
        }

        return $currency;
    }

    function getCurrencyObj()
    {
        $currency = $this->getCurrency();

        $currencyObj = \Currency::getPublicObj($currency);
        return isset($currencyObj) ? $currencyObj : $this->getDefaultCurrency();
    }

    function getDefaultTaxGroup()
    {
        $tax_group_id = \Cfg::get('TAX_GROUP_DEFAULT');
        return \TaxGroup::getPublicObj($tax_group_id);
    }

    function getDefaultTaxRate()
    {
        $tax_group = $this->getDefaultTaxGroup();
        return ($tax_group) ? $tax_group->getDefaultTaxRatio() : 1;
    }

    function getCategoriesForProduct($product_id)
    {

    }

    function randomString($size = 10, $uppercase = true)
    {
        //$str = substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $size);
        $str = \Str::random($size);
        if ($uppercase) $str = \Str::upper($str);
        return $str;

    }

    function getDiscount($source, $discount_type, $discount_value, $currency = 'default', $include_tax = 1, $tax = 'default')
    {
        $discount = $discount_value;
        $discounted = 0;
        $reduction_percent = 0;
        $reduction_amount = 0;
        $ratio = 1;

        if ($source > 0):
            if ($include_tax == 0) {
                $taxGroup = ($tax == 'default') ? $this->getDefaultTaxGroup() : \TaxGroup::getPublicObj($tax);
                $ratio = $taxGroup->getDefaultTaxRatio();
            }

            //audit(compact('source', 'discount_type', 'discount_value', 'currency', 'include_tax', 'tax', 'ratio'));

            $source = $source / $ratio;

            switch ($discount_type) {
                case 'percent':
                case 'percentage':
                    $discount = $source / 100 * $discount_value;
                    if ($discount_value == 100) {
                        $discount = $source;
                    }
                    $reduction_percent = $discount_value;
                    break;
                case 'fixed':
                case 'amount':
                    $discount_value = $discount_value / $ratio;
                    $reduction_amount = $discount_value;
                    break;
                case 'fixed_amount':
                    $discount_value = $discount_value / $ratio;
                    $discounted = $discount_value;
                    $discount = $source - $discounted;
                    $reduction_amount = $discount;
                    break;
                case 'fixed_percent':
                    $discounted = $source / 100 * $discount_value;
                    $discount = $source - $discounted;
                    $reduction_percent = 100 - $discount_value;
                    break;
            }
            if ($source == $discount) {
                $discounted = 0;
            } else {
                $discount = $this->convert($discount, \FrontTpl::getCurrency(), $currency);
                $discounted = $source - $discount;
            }

            if ($discounted < 0)
                $discounted = 0;
        endif;

        $return = compact('discount', 'discounted', 'reduction_amount', 'reduction_percent');
        //\Utils::log($return,__METHOD__);
        return $return;
    }


    function tax($value, $ratio)
    {
        $ratio = ($ratio == 0) ? $this->getDefaultTaxRate() : $ratio;
        return $value * $ratio;
    }

    function untax($value, $ratio, $decimals = 2)
    {
        $ratio = ($ratio == 0) ? $this->getDefaultTaxRate() : $ratio;
        $v = $value / $ratio;
        return round($v, $decimals);
    }


    function getZone($country_id = false)
    {
        $zone = null;
        $country = ($country_id) ? \Country::getPublicObj($country_id) : $this->getCountry();
        if ($country == null and $country_id == false) {
            $country = $this->getDefaultCountry();
        }
        if ($country) {
            $zone_id = $country->zone_id;
            $zone = \Zone::getPublicObj($zone_id);
        }
        return $zone;
    }

    function getCountry()
    {
        $geo = Geo::getInstance();
        $country = $geo->getCountry();
        return $country;
    }

    /**
     * @return \Country|null
     */
    function getDefaultCountry()
    {
        $country_id = \Cfg::get('COUNTRY_DEFAULT');
        $country = \Country::getPublicObj($country_id);
        return $country;
    }

    function getShopInfo()
    {
        /*
         * SHOP_NAME
            SHOP_CITY
            SHOP_CODE
            SHOP_ADDRESS
            SHOP_COUNTRY_ID
            SHOP_STATE_ID
            SHOP_PHONE
            SHOP_DETAILS
            SHOP_FAX
            SHOP_EMAIL
         * */
        $cfg = \Cfg::getGroup('general');

        $state = \State::getObj($cfg['SHOP_STATE_ID']);
        $country = \Country::getObj($cfg['SHOP_COUNTRY_ID']);

        $data = [
            'name' => $cfg['SHOP_NAME'],
            'email' => $cfg['SHOP_EMAIL'],
            'address' => $cfg['SHOP_ADDRESS'],
            'phone' => $cfg['SHOP_PHONE'],
            'fax' => $cfg['SHOP_FAX'],
            'postcode' => $cfg['SHOP_CODE'],
            'city' => $cfg['SHOP_CITY'],
            'details' => $cfg['SHOP_DETAILS'],
            'state' => ($state) ? $state->name : false,
            'state_code' => ($state) ? $state->iso_code : false,
            'country' => ($country) ? $country->name : false,
            'country_code' => ($country) ? $country->iso_code : false,
        ];

        $full_address = $data['address'] . "<br>";
        if ($data['postcode'] != '') $full_address .= " " . $data['postcode'];
        if ($data['city'] != '') $full_address .= " " . $data['city'];
        if ($data['state_code'] != '') $full_address .= " (" . $data['state_code'] . ")";
        if ($data['country'] != '') $full_address .= " - " . $data['country'];
        $data['full_address'] = $full_address;

        $full_phone = '';
        if ($data['phone'] != '') $full_phone .= "Tel: " . $data['phone'];
        if ($data['fax'] != '') $full_phone .= " - Fax: " . $data['fax'];
        $data['full_phone'] = $full_phone;

        return $data;
    }


    function getRmaContacts()
    {
        /*
         * SHOP_NAME
            SHOP_CITY
            SHOP_CODE
            SHOP_ADDRESS
            SHOP_COUNTRY_ID
            SHOP_STATE_ID
            SHOP_PHONE
            SHOP_DETAILS
            SHOP_FAX
            SHOP_EMAIL
         * */
        $cfg = \Cfg::getGroup('order');

        $state = \State::getObj($cfg['RMA_SHOP_STATE_ID']);
        $country = \Country::getObj($cfg['RMA_SHOP_COUNTRY_ID']);

        $data = [
            'name' => $cfg['RMA_SHOP_NAME'],
            'email' => $cfg['RMA_SHOP_EMAIL'],
            'address' => $cfg['RMA_SHOP_ADDRESS'],
            'phone' => $cfg['RMA_SHOP_PHONE'],
            'fax' => $cfg['RMA_SHOP_FAX'],
            'postcode' => $cfg['RMA_SHOP_CODE'],
            'city' => $cfg['RMA_SHOP_CITY'],
            'details' => $cfg['RMA_SHOP_DETAILS'],
            'state' => ($state) ? $state->name : false,
            'state_code' => ($state) ? $state->iso_code : false,
            'country' => ($country) ? $country->name : false,
            'country_code' => ($country) ? $country->iso_code : false,
        ];

        $full_address = $data['address'] . "<br>";
        if ($data['postcode'] != '') $full_address .= " " . $data['postcode'];
        if ($data['city'] != '') $full_address .= " " . $data['city'];
        if ($data['state_code'] != '') $full_address .= " (" . $data['state_code'] . ")";
        if ($data['country'] != '') $full_address .= " - " . $data['country'];
        $data['full_address'] = $full_address;

        $full_phone = '';
        if ($data['phone'] != '') $full_phone .= "Tel: " . $data['phone'];
        if ($data['fax'] != '') $full_phone .= " - Fax: " . $data['fax'];
        $data['full_phone'] = $full_phone;

        return $data;
    }


    function getInvoiceNumber()
    {
        return (int)\Cfg::real('ORDER_INVOICE_NUMBER') + 1;
    }

    function setInvoiceNumber($number, $year = null)
    {
        $year = is_null($year) ? date('Y') : $year;
        //get latest 2 order to check if the progressive is ok
        $orders = \Order::whereYear('invoice_date', '=', $year)
            ->where('invoice_number', '>', 0)
            ->select(['id', 'invoice_number'])
            ->orderBy('invoice_number', 'desc')
            ->take(2)
            ->get();

        $apply = false;

        //if there are no previous orders, then apply the code directly
        if (count($orders) == 0) {
            $apply = true;
        } else {
            //if there is one order
            if (count($orders) == 1) {
                $order = $orders[0];
                if ($order->invoice_number == $number) {
                    $apply = true;
                } else {
                    $apply = true;
                    $number = $order->invoice_number;
                }
            } else {
                $first = $orders[1];
                $second = $orders[0];
                $numbers = [$first->invoice_number, $second->invoice_number];
                if (abs($first->invoice_number - $second->invoice_number) == 1) {
                    //if the difference between the 2 number is 1, it means they are consecutive, so take the second one
                    $number = $second->invoice_number;
                    $apply = true;
                } else {
                    //then we have some problems, because the 2 numbers are not consecutive
                    //we start from the min number and update the second order
                    $number = min($numbers);
                    //audit_track("Overriding invoice_number to [$number] for order [$first->id]", __METHOD__);
                    audit("Overriding invoice_number to [$number] for order [$first->id]", __METHOD__);
                    DB::table('orders')->where('id', $first->id)->update(['invoice_number' => $number]);
                    $number++;
                    //audit_track("Overriding invoice_number to [$number] for order [$second->id]", __METHOD__);
                    audit("Overriding invoice_number to [$number] for order [$second->id]", __METHOD__);
                    DB::table('orders')->where('id', $second->id)->update(['invoice_number' => $number]);
                    $apply = true;
                }
            }
        }

        if ($apply) {
            //audit_track("Saving global ORDER_INVOICE_NUMBER to [$number]", __METHOD__);
            audit("Saving global ORDER_INVOICE_NUMBER to [$number]", __METHOD__);
            \Cfg::save('ORDER_INVOICE_NUMBER', $number);
        }

    }

    function incrementInvoiceNumber()
    {
        \Cfg::save('ORDER_INVOICE_NUMBER', $this->getInvoiceNumber());
    }

    function getDeliveryNumber()
    {
        return (int)\Cfg::real('ORDER_DTD_NUMBER') + 1;
    }

    function getCampaignId()
    {
        $session = \Session::get('blade_campaign_id', null);
        if ($session != null AND $session > 0) {
            return $session;
        }
        $cookie = \Cookie::get('blade_campaign_id', null);
        if ($cookie != null AND $cookie > 0) {
            return $cookie;
        }
        return 0;
    }


    function initCatalogCache()
    {
        $attributes = \Attribute::get();
        foreach ($attributes as $attribute) {
            print_r($attribute->toArray());
            $master = 'redis-attribute-' . $attribute->id;
            $master = \Utils::redisPrefix($master);
            \Redis::hset($master, 'code', $attribute->code);

            $translations = $attribute->translations()->get();

            foreach ($translations as $tr) {
                print_r($tr->toArray());
                \Redis::hset($master, 'name_' . $tr->lang_id, $tr->name);
            }

        }
    }


    public function redirectRegExp($url, $redirect, $callback = null)
    {
        $matches = [];
        if (preg_match_all("#{$redirect['source']}#iU", $url, $matches)) {

            $target_url = $redirect['target'];

            $params = [];

            for ($i = 1; $i < count($matches); $i++) {
                $reg = $matches[$i][0];
                $target_url = str_replace('$' . $i, $reg, $target_url);
                $params['$' . $i] = $reg;
            }
            if (is_callable($callback)) {
                $target_url = call_user_func($callback, $params);
            }

            \Utils::log("$url ==>> $target_url", __METHOD__);
            return $target_url;
        }
        return false;
    }


    function isWin()
    {
        if (strtolower(substr(PHP_OS, 0, 3)) === 'win' || PHP_SHLIB_SUFFIX == 'dll' || PATH_SEPARATOR == ';') {
            return true;
        } else {
            return false;
        }
    }

    function ip()
    {
        return Geo::getInstance()->getIp();
    }

    /**
     * @return \services\Morellato\Database\Sql\Connection
     */
    function getNegoziandoConnection()
    {
        if ($this->negoziandoConnection == null) {
            $conn = \App::make('services\Morellato\Database\Sql\Connection');
            $conn->connect();
            $this->negoziandoConnection = $conn;
        }
        return $this->negoziandoConnection;
    }

    /**
     * @return bool
     */
    function b2b()
    {
        return \Config::get('negoziando.b2b_enabled', false);
    }

    /**
     * @return bool
     */
    public function membership()
    {
        $membership_enabled = (bool)config('membership.enabled', false);
        $membership_service = (bool)config('services.membership', false);
        return $membership_enabled && $membership_service;
    }

    /**
     * @return bool
     */
    function isB2bLogged()
    {
        return \View::shared('clerk_user', false) and \View::shared('clerk_shop', false);
    }

    /**
     * @param $lang
     */
    function swapLocale($lang)
    {
        $this->swap_lang = $this->getLang();
        \App::setLocale($lang);
    }

    /**
     *
     */
    function restoreLocale()
    {
        \App::setLocale($this->swap_lang);
    }

    /**
     * @return bool
     */
    public function isThemeBackend()
    {
        return Theme::getThemeName() === 'admin';
    }

    /**
     * @return bool
     */
    public function isThemeDesktop()
    {
        return Theme::getThemeName() === 'frontend';
    }

    /**
     * @return bool
     */
    public function isThemeMobile()
    {
        return Theme::getThemeName() === 'mobile';
    }

    /**
     * @return bool
     */
    public function isThemeFrontend()
    {
        return $this->isThemeDesktop() || $this->isThemeMobile();
    }

    /**
     * @return string|string[]
     */
    public function getHost()
    {
        return str_replace(['https://', 'http://'], '', config('app.url'));
    }

    /**
     * @return bool
     */
    public function cartExists()
    {
        $obj = \Session::get('blade_cart', null);
        if ($obj) {
            $obj = (object)$obj;
        }
        return $obj and isset($obj->id) and $obj->id > 0;
    }

    /**
     *
     */
    public function resetVolatileSessionForCustomer()
    {
        \Session::forget('blade_customer_group_id');
    }

    /**
     * @param $url
     * @return bool
     */
    public function isUrlStrictToSameHost($url)
    {
        try {
            $host = parse_url($url, PHP_URL_HOST);
            return $host === $this->getHost();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return null|int
     */
    public function cartIsNotEmpty(){
        $session = Session::all();

        if (array_key_exists('blade_cart', $session)) {
            if (is_array($session['blade_cart']) && isset($session['blade_cart']['id']) && $session['blade_cart']['id'] > 0) {
                $cart_id = (int)$session['blade_cart']['id'];
                $is_empty = (int)\CartProduct::where(compact('cart_id'))->count() === 0;
                return $is_empty ? null : $cart_id;
            }
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function customerIsLogged(){
        $session = Session::all();

        if (array_key_exists('blade_auth', $session)) {
            if ((int)$session['blade_auth'] > 0) {
                return (int)$session['blade_auth'];
            }
        }

        return null;
    }

}