<?php

namespace Core;

/*
 * This is a N3m3s7s creation
 * @author Fabio Politi <f.politi@icoa.it at icoa.it>
 * @created 16-mag-2013 17.37.21
 */

use App, DB, Cache;

class Cfg
{

    private $lang = null;
    private $data = null;
    private $data_lang = null;
    private $groups = null;
    private $groups_lang = null;
    private $table = 'config';
    private $table_lang = 'config_lang';

    /** @var Cache */
    protected $store;

    const SERVICE_ORDER_STATUS_WORKING = 'SERVICE_ORDER_STATUS_WORKING';
    const SERVICE_ORDER_STATUS_CLOSED = 'SERVICE_ORDER_STATUS_CLOSED';
    const SERVICE_SEND_ORDER_WS1 = 'SERVICE_SEND_ORDER_WS1';
    const SERVICE_PULL_ORDER_WS2 = 'SERVICE_PULL_ORDER_WS2';
    const SERVICE_SHOP_AVAILABILITY = 'SERVICE_SHOP_AVAILABILITY';
    const SERVICE_BESTSHOP_MIGRATION = 'SERVICE_BESTSHOP_MIGRATION';
    const SERVICE_FTP_CSV_DOWNLOAD = 'SERVICE_FTP_CSV_DOWNLOAD';
    const SERVICE_IMAGE_REPOSITORY = 'SERVICE_IMAGE_REPOSITORY';
    const SERVICE_FIDELITY_CARD_REGISTER = 'SERVICE_FIDELITY_CARD_REGISTER';
    const SERVICE_FIDELITY_CARD_POINTS = 'SERVICE_FIDELITY_CARD_POINTS';
    const SERVICE_FIDELITY_CARD_VOUCHER = 'SERVICE_FIDELITY_CARD_VOUCHER';
    const SERVICE_GLS_ORDER_MIGRATION = 'SERVICE_GLS_ORDER_MIGRATION';
    const SERVICE_GLS_ORDER_ADDRESS = 'SERVICE_GLS_ORDER_ADDRESS';
    const SERVICE_CUSTOMER_WARNING_PACKAGE_READY = 'SERVICE_CUSTOMER_WARNING_PACKAGE_READY';
    const SERVICE_CUSTOMER_WARNING_MISSED_PICKUP = 'SERVICE_CUSTOMER_WARNING_MISSED_PICKUP';
    const SERVICE_CUSTOMER_WARNING_SHOP_CARRIER = 'SERVICE_CUSTOMER_WARNING_SHOP_CARRIER';
    const SERVICE_CUSTOMER_WARNING_SHOP_CUSTOMER_CALL = 'SERVICE_CUSTOMER_WARNING_SHOP_CUSTOMER_CALL';

    /**
     * Cfg constructor.
     */
    public function __construct()
    {
        $this->store = Cache::driver(config('cache.config.store', 'redis'))->tags(['config']);
    }

    /**
     * @return Cache
     */
    protected function cache()
    {
        return $this->store;
    }

    /**
     *
     */
    private function init()
    {
        $locale = \App::getLocale();
        $this->loadLanguage($locale);
    }

    /**
     * @param $locale
     */
    private function loadLanguage($locale)
    {
        if (PHP_SAPI !== 'cli' && $this->cache()->has('config_cache_' . $locale)) {
            try {
                $c = $this->cache()->get('config_cache_' . $locale);
                $this->data = $c['data'];
                $this->groups = $c['groups'];
                $this->data_lang = $c['data_lang'];
                $this->groups_lang = $c['groups_lang'];
                return;
            } catch (\Exception $e) {

            }
        }

        $config = array();
        $groups = array();
        $groups_temp = array();
        $rows = DB::table($this->table)->get();

        foreach ($rows as $row) {
            $config[$row->cfg_key] = $row->prod_value;
            $groups_temp[$row->cfg_key] = $row->cfg_group;
        }

        $codes = \Mainframe::languagesCodes();
        $lang_rows = DB::table($this->table_lang)->leftJoin($this->table, 'config_lang.cfg_key', '=', 'config.cfg_key')->select('config_lang.*', 'config.cfg_group')->whereIn('lang_id', $codes)->get();

        $lang_config = array();
        $lang_groups = array();

        foreach ($lang_rows as $row) {
            $key = $row->cfg_key . "_" . $row->lang_id;
            $lang_config[$key] = $row->prod_value;
            $lang_groups[$key] = $row->cfg_group;
            if ((string)$locale === (string)$row->lang_id) {
                $lang_config[$row->cfg_key] = $row->prod_value;
            }
        }

        $this->data_lang = $lang_config;
        $this->groups_lang = $lang_groups;

        $config = \EchoArray::set($config)->extend($this->data_lang)->get();
        $groups_temp = \EchoArray::set($groups_temp)->extend($this->groups_lang)->get();
        foreach ($groups_temp as $value_key => $group_key) {
            if (!isset($groups[$group_key])) {
                $groups[$group_key] = array();
            }
            $groups[$group_key][$value_key] = $config[$value_key];
        }
        $this->data = $config;
        $this->groups = $groups;

        $c = array();
        $c['data'] = $this->data;
        $c['groups'] = $this->groups;
        $c['data_lang'] = $this->data_lang;
        $c['groups_lang'] = $this->groups_lang;
        $this->cache()->forever('config_cache_' . $locale, $c);
    }

    /**
     * @param $lang
     */
    public function setLocale($lang)
    {
        $this->lang = $lang;
        \App::setLocale($lang);
        $this->loadLanguage($lang);
    }

    /**
     *
     */
    public function purge()
    {
        $codes = \Mainframe::languagesCodes();
        foreach ($codes as $locale) {
            $this->cache()->forget('config_cache_' . $locale);
        }
        //always reset standard
        if ($this->cache()->has('config_cache_'))
            $this->cache()->forget('config_cache_');

        if ($this->cache()->has('config_cache_it'))
            $this->cache()->forget('config_cache_it');
    }

    /**
     * @param $key
     * @param bool $default
     * @return bool|mixed
     */
    public function get($key, $default = false)
    {
        if ($this->data === null) {
            $this->init();
        }
        return array_key_exists($key, $this->data) ? $this->data[$key] : $default;
    }

    /**
     * @param $key
     * @param bool $default
     * @return bool|mixed|null
     */
    public function real($key, $default = false)
    {
        $key = strtoupper(trim($key));
        $row = DB::table($this->table)->where('cfg_key', $key)->pluck('prod_value');
        return (isset($row) AND ($row !== '' OR $row !== null)) ? $row : $default;
    }

    /**
     * @param $key
     * @param $value
     * @param null $group
     */
    public function save($key, $value, $group = null)
    {
        $key = strtoupper(trim($key));
        $data = $this->prepareValue($value);
        if (is_string($group)) {
            $data['cfg_group'] = $group;
        }

        $row = DB::table($this->table)->where('cfg_key', $key)->count();
        if ($row > 0) {
            DB::table($this->table)->where('cfg_key', $key)->update($data);
        } else {
            $data['cfg_key'] = $key;
            DB::table($this->table)->insert($data);
        }
        $this->purge();
    }

    /**
     * @param $value
     * @return array
     */
    private function prepareValue($value, $forLang = false)
    {
        $isBool = false;
        if (is_bool($value)) {
            $value = (int)$value;
        }
        if ($value === '1' or $value === '0' or $value === 1 or $value === 0) {
            $isBool = true;
        }
        return ($forLang) ? ['prod_value' => $value] : ['prod_value' => $value, 'isBool' => (int)$isBool];
    }

    /**
     * @return array|false
     */
    public function getBooleanKeys()
    {
        return DB::table($this->table)->where('isBool', 1)->lists('cfg_key');
    }

    /**
     * @param $str_array
     * @return array|null
     */
    public function getMultiple($str_array)
    {
        if ($this->data === null) {
            $this->init();
        }
        if ($str_array === '*') {
            return $this->data;
        }
        //$env = App::environment();
        if (is_array($str_array)) {
            $keys = $str_array;
        } else {
            $keys = explode(',', $str_array);
        }
        $values = array();
        foreach ($keys as $key) {
            $values[$key] = $this->get($key);
        }
        return $values;
    }

    /**
     * @param $group
     * @return array|mixed
     */
    public function getGroup($group)
    {
        if ($this->data === null) {
            $this->init();
        }
        return isset ($this->groups[$group]) ? $this->groups[$group] : [];
    }

    /**
     * @param $group
     * @return array
     */
    public function getGroupKeys($group)
    {
        if ($this->data === null) {
            $this->init();
        }
        $group = $this->getGroup($group);
        return array_keys($group);
    }

    /**
     * @param $group
     * @param $data
     */
    public function storeGroup($group, $data)
    {
        $group_keys = $this->getGroupKeys($group);

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $value = implode(',', $value);
            }
            $data = $this->prepareValue($value);

            if (in_array($key, $group_keys, true)) {
                //update
                DB::table($this->table)
                    ->where('cfg_key', $key)
                    ->where('cfg_group', $group)
                    ->update($data);
            } else {
                try {
                    $data['cfg_key'] = $key;
                    $data['cfg_group'] = $group;
                    DB::table($this->table)->insert($data);
                } catch (\Exception $e) {
                    //update
                    DB::table($this->table)
                        ->where('cfg_key', $key)
                        ->where('cfg_group', $group)
                        ->update($data);
                }
            }
        }

        if (isset($_POST['_lang_fields'])) {
            $codes = \Mainframe::languagesCodes();
            $lang_fields = $_POST['_lang_fields'];
            foreach ($lang_fields as $field) {
                foreach ($codes as $code) {
                    $cfg_key = $field;
                    $cfg_value = $_POST[$field . '_' . $code];
                    $data = $this->prepareValue($cfg_value, true);
                    $stored_value = DB::table($this->table_lang)->where('cfg_key', $cfg_key)->where('lang_id', $code)->pluck('cfg_key');
                    if ((string)$stored_value !== '') { //KEY EXISTS -> UPDATE
                        DB::table($this->table_lang)
                            ->where('cfg_key', $cfg_key)
                            ->where('lang_id', $code)
                            ->update($data);
                    } else { //KEY DOES NOT EXIST -> INSERT
                        $data['cfg_key'] = $cfg_key;
                        $data['lang_id'] = $code;
                        DB::table($this->table_lang)->insert($data);
                    }
                }
            }
        }

        $this->purge();
    }

}