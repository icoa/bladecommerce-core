<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 03/12/2014
 * Time: 17:52
 */

namespace Core;

use services\GeoIpLoc;
use services\IpResolver;
use services\Traits\TraitLocalDebug;
use Session;

class Geo
{
    use TraitLocalDebug;

    private static $instance = null;
    private $country;
    private $default_country;
    private $ip;
    protected $localDebug = false;

    function __construct()
    {
        $ip = null;
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } elseif (isset($_SERVER['REMOTE_ADDR'])) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        if (($ip == null or $ip == '') and \App::runningInConsole()) {
            $ip = '127.0.0.1';
        }
        if ($ip == '127.0.0.1') {
            $ip = '2.236.193.57';
        }

        //simulated IP
        $sip = \Input::get('sip');
        if ($sip and $sip != '') {
            Session::forget('blade_country');
            if (filter_var($sip, FILTER_VALIDATE_IP)) {
                $ip = $sip;
            }
        }
        $this->audit($ip, __METHOD__, 'IP');
        $this->ip = $ip;
        $this->default_country = \Cache::rememberForever('default_country', function () {
            $id = \Cfg::get('COUNTRY_DEFAULT');
            return \Country::where('id', $id)->first();
        });
    }

    /**
     * @return Geo
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    function getIp()
    {
        return $this->ip;
    }

    function setIp($ip)
    {
        $this->ip = $ip;
        $this->country = null;
    }

    function getCountryCode()
    {
        $country = $this->getCountry();
        return $country->iso_code;
    }

    function getCountryId()
    {
        $country = $this->getCountry();
        return $country->id;
    }

    function findCountry($isoCode){
        $this->country = \Cache::rememberForever('country_' . $isoCode, function () use ($isoCode) {
            return \Country::where('iso_code', $isoCode)->first();
        });
        return $this->country === null ? $this->default_country : $this->country;
    }

    function getCountry()
    {
        if (isset($this->country)) {
            return $this->country;
        }

        $ip = $this->ip;

        if (!filter_var($ip, FILTER_VALIDATE_IP) or $ip == '127.0.0.1') {
            //\Utils::log("Passed IP is not valid - return default country", "GEO");
            return $this->default_country;
        }

        $blade_country = Session::get('blade_country');
        if(is_string($blade_country) && strlen($blade_country) === 2){
            $blade_country = $this->findCountry($blade_country);
        }
        if ($blade_country && strlen($blade_country->iso_code) === 2) {
            Session::put('blade_country', $blade_country->iso_code);
            return $blade_country;
        }

        $service = IpResolver::getInstance();
        try {
            $payload = $service->resolve($ip);
        } catch (\Exception $e) {
            $payload = $service->getEmptyPayload();
        }

        $this->audit($payload, __METHOD__, 'PAYLOAD');
        //$this->audit(\Request::fullUrl(), __METHOD__, 'URL');

        if ($payload->hasAttribute('countryCode') and strlen($payload->getAttribute('countryCode')) == 2) {
            $isoCode = $payload->getAttribute('countryCode');
            $this->country = \Cache::rememberForever('country_' . $isoCode, function () use ($isoCode) {
                return \Country::where('iso_code', $isoCode)->first();
            });
        }

        if ($this->country === null) {
            $this->country = $this->default_country;
        }

        Session::put('blade_country', $this->country->iso_code);

        return $this->country;
    }
} 