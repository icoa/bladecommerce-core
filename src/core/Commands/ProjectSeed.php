<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProjectSeed extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:seed';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Truncate the default tables for a new installation';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$env = \App::environment();
		$database = \Config::get('database.connections.mysql.database');
		$bool = $this->confirm("Are you sure to truncate all primary tables for [$env] environment and [$database] database?");
		$this->info( $bool ? 'YES' : 'NO');
		if($bool){
			$this->comment( \App::environment() );

			$tables = [
				'address',
				'cart',
				'cart_cart_rules',
				'cart_product',
				'customers',
				'danea_import_deleted_products',
				'danea_import_updated_products',
				'danea_import_updated_products',
				'google_analytics',
				'messages',
				'newsletters',
				'order_carriers',
				'order_cart_rules',
				'order_details',
				'order_details',
				'order_history',
				'orders',
				'profiles',
				'redirect_links',
				'rmas',
				'rma_details',
				'sessions',
				'stock_mvts',
				'throttle',
				'transactions',
				'transactions_paypal',
				'transactions_safepay',
				'wishlists',
				'wishlists_products',
			];

			foreach($tables as $table){
				$this->info("Truncating table [$table]");
				\DB::table($table)->truncate();
				$this->comment("=> DONE");
			}
		}

	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('target', InputArgument::REQUIRED, 'The target path'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}