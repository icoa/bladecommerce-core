<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class LexiconExport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'lexicon:export';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all lexicon entries to a sql format.';

    private $queries = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $date = $this->argument('date');
        if(strtotime($date) <= 0){
            $this->error("Could not determine starting date from [$date]");
            return;
        }
        $this->collect();

        $rows = \Lexicon::where('created_at','>=',$date)->orWhere('updated_at','>=',$date)->get();
        $lexicon_schema = [];
        $this->info('Prepare to exporting '.count($rows).' latest Lexicons');
        foreach($rows as $row){

            foreach($row->translations as $tr){

            }
            
        }


        $queries = $this->queries;
        foreach ($queries as $k => $query) {
            if (substr($query, 0, 6) == 'select') unset($queries[$k]);
        }

        if(count($queries)){
            $content = implode(';' . PHP_EOL, $queries);
            $content = str_replace(" = ''", " = null", $content);
            $content = str_replace(", ''", ", null", $content);
            $sqlFile = storage_path('export/lexicons_'.time().'sql');
            \File::put($sqlFile, $content);
            $this->info("Saved SQL file to $sqlFile");
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('date', InputArgument::REQUIRED, 'The starting date'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

    private function collect(){
        $queries = &$this->queries;
        \Event::listen('illuminate.query', function ($sql, $bindings) use (&$queries) {
            $i = 0;
            $searches = [];
            $replaces = [];
            foreach ($bindings as $binding) {
                $binding = \DB::connection()->getPdo()->quote($binding);
                $sql = preg_replace('/\?/', '{' . $i . '}', $sql, 1);
                $searches[] = '{' . $i . '}';
                $replaces[] = $binding;
                $i++;
            }
            $sql = str_replace($searches, $replaces, $sql);
            $queries[] = $sql;
        });
    }

    private function getQueries(){
        return $this->queries;
    }

}