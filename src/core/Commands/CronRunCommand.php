<?php

# Cron job command for Laravel 4.2
# Inspired by Laravel 5's new upcoming scheduler (https://laravel-news.com/2014/11/laravel-5-scheduler)
#
# Author: Soren Schwert (GitHub: sisou)
#
# Requirements:
# =============
# PHP 5.4
# Laravel 4.2 ? (not tested with 4.1 or below)
# A desire to put all application logic into version control
#
# Installation:
# =============
# 1. Put this file into your app/commands/ directory and name it 'CronRunCommand.php'.
# 2. In your artisan.php file (found in app/start/), put this line: 'Artisan::add(new CronRunCommand);'.
# 3. On the server's command line, run 'php artisan cron:run'. If you see a message telling you the
#    execution time, it works!
# 4. On your server, configure a cron job to call 'php-cli artisan cron:run >/dev/null 2>&1' and to
#    run every five minutes (*/5 * * * *)
# 5. Observe your laravel.log file (found in app/storage/logs/) for messages starting with 'Cron'.
#
# Usage:
# ======
# 1. Have a look at the example provided in the fire() function.
# 2. Have a look at the available schedules below (starting at line 132).
# 4. Code your schedule inside the fire() function.
# 3. Done. Now go push your cron logic into version control!

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\CronTasks as Cron;

class CronRunCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cron:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the scheduler';

    /**
     * Current timestamp when command is called.
     *
     * @var integer
     */
    protected $timestamp;

    /**
     * Hold messages that get logged
     *
     * @var array
     */
    protected $messages = array();

    /**
     * Specify the time of day that daily tasks get run
     *
     * @var string [HH:MM]
     */
    protected $runAt = '03:00';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->timestamp = time();
        $this->runAt = \Config::get('cron.startAt');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $pretend = \Config::get('cron.pretend');

        if ($pretend) {
            Cron::pretend();
        }

        $this->daily(function () {
            $this->trackInit();
            Cron::downloadCurrencies();
            Cron::removeDeletedProducts();
        });

        /*$this->twiceDaily(function () {
            global states and cache has been moved to once a day
        });*/

        $this->dailyOffset("+5 minutes", function () {
            $this->trackInit();
            Cron::statesCheck();
            //TODO: try to remove these heavy ops from cron
            //Cron::handleCache();
            //Cron::handleCacheAdvanced();
            Cron::wanted();
            Cron::googleAnalytics();
        });

        if(feats()->enabled('elasticsearch')){
            $this->dailyOffset("+10 minutes", function () {
                $this->trackInit();
                $this->call('elastic:run', ['task' => 'ids']);
            });
            $this->weeklyOn('Sun', '04:00', function () {
                $this->trackInit();
                $this->call('elastic:run', ['task' => 'index_all']);
            });
        }

        $this->dailyOffset("+15 minutes", function () {
            $this->trackInit();
            Cron::sitemap('Boilerplates');
            Cron::sitemap('Categories');
        });

        $this->dailyOffset("+20 minutes", function () {
            $this->trackInit();
            Cron::sitemap('Products');
        });

        if (\Config::get('plugins.GoogleMerchant', true) === true) {
            $offset = 25;
            $increment = 10;
            $languages = Core::getLanguages();
            foreach ($languages as $language) {
                switch ($language) {
                    case 'it':
                        $this->dailyOffset("+$offset minutes", function () {
                            $this->trackInit();
                            Cron::googleMerchant('it', 'IT');
                        });
                        break;
                    case 'en':
                        $this->dailyOffset("+$offset minutes", function () {
                            $this->trackInit();
                            Cron::googleMerchant('en', 'US');
                        });
                        $this->dailyOffset("+$offset minutes", function () {
                            $this->trackInit();
                            Cron::googleMerchant('en', 'GB');
                        });
                        break;
                    case 'es':
                        $this->dailyOffset("+$offset minutes", function () {
                            $this->trackInit();
                            Cron::googleMerchant('es', 'ES');
                        });
                        break;
                    case 'fr':
                        $this->dailyOffset("+$offset minutes", function () {
                            $this->trackInit();
                            Cron::googleMerchant('fr', 'FR');
                        });
                        break;
                    case 'de':
                        $this->dailyOffset("+$offset minutes", function () {
                            $this->trackInit();
                            Cron::googleMerchant('de', 'DE');
                        });
                        break;
                }
                $offset += $increment;
            }
        }

        if (\Config::get('plugins.ZanoxFrontend') === true) {
            $this->dailyOffset("+40 minutes", function () {
                $this->trackInit();
                Cron::zanox('it', 'IT');
            });
        }

        $this->dailyOffset("+45 minutes", function () {
            Cron::report();
        });

        $this->dailyOffset("+50 minutes", function () {
            Cron::sessionLottery();
        });

        if(config('cache.static.enabled', false)){
            $this->dailyAt('15:59', function(){
                $this->call('cache:static:clear');
            });

            $this->dailyAt('07:59', function(){
                $this->call('cache:static:clear');
            });
        }

        $this->dailyAt('16:30', function(){
           $this->call('emergency:catalog');
        });

        $this->dailyAt('07:30', function(){
            $this->call('emergency:catalog');
        });

        $this->weekly(function(){
            $this->trackInit();
            $this->call('sirv:360');
        });

        //handle the invoice reset on 01-01-XXXX at 00:30
        $this->runAt = '00:05';
        $this->yearly(function(){
            \Cfg::save('ORDER_INVOICE_NUMBER', 0);
        });

        $this->messages = Cron::getMessages();

        $this->finish();
    }

    protected function finish()
    {
        // Write execution time and messages to the log
        if (count($this->messages)) {
            $dump = implode(PHP_EOL, $this->messages);
            \Utils::cron($dump);
            $this->trackEnd();
        }
    }

    protected function message($str)
    {
        $this->messages[] = $str;
    }

    protected function trackInit()
    {
        $start = date("H:i:s", $this->timestamp);
        \Utils::cron("<=== STARTING CRON DAEMON AT [$start] (Run at $this->runAt) ===>");
    }

    protected function trackEnd()
    {
        $end = date("H:i:s");
        \Utils::cron("<=== FINISHED CRON DAEMON AT [$end] ===>");
    }

    /**
     * AVAILABLE SCHEDULES
     */

    protected function everyFiveMinutes(callable $callback)
    {
        if ((int)date('i', $this->timestamp) % 5 === 0) call_user_func($callback);
    }

    protected function everyTenMinutes(callable $callback)
    {
        if ((int)date('i', $this->timestamp) % 10 === 0) call_user_func($callback);
    }

    protected function everyFifteenMinutes(callable $callback)
    {
        if ((int)date('i', $this->timestamp) % 15 === 0) call_user_func($callback);
    }

    protected function everyThirtyMinutes(callable $callback)
    {
        if ((int)date('i', $this->timestamp) % 30 === 0) call_user_func($callback);
    }

    /**
     * Called every full hour
     */
    protected function hourly(callable $callback)
    {
        if (date('i', $this->timestamp) === '00') call_user_func($callback);
    }

    /**
     * Called every hour at the minute specified
     *
     * @param  integer $minute
     */
    protected function hourlyAt($minute, callable $callback)
    {
        if ((int)date('i', $this->timestamp) === $minute) call_user_func($callback);
    }

    /**
     * Called every day
     */
    protected function daily(callable $callback)
    {
        if (date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    /**
     * Called every day after a certain period
     */
    protected function dailyOffset($offset, callable $callback)
    {
        $runAt = date("H:i", strtotime($this->runAt . " " . $offset));
        //\Utils::log($runAt, __METHOD__);
        if (date('H:i', $this->timestamp) === $runAt) call_user_func($callback);
    }

    /**
     * Called every day at the 24h-format time specified
     *
     * @param  string $time [HH:MM]
     */
    protected function dailyAt($time, callable $callback)
    {
        if (date('H:i', $this->timestamp) === $time) call_user_func($callback);
    }

    /**
     * Called every day at 12:00am and 12:00pm
     */
    protected function twiceDaily(callable $callback)
    {
        if (date('h:i', $this->timestamp) === '12:00') call_user_func($callback);
    }

    /**
     * Called every weekday
     */
    protected function weekdays(callable $callback)
    {
        $days = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri'];
        if (in_array(date('D', $this->timestamp), $days) && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function mondays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Mon' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function tuesdays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Tue' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function wednesdays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Wed' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function thursdays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Thu' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function fridays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Fri' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function saturdays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Sat' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    protected function sundays(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Sun' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    /**
     * Called once every week (basically the same as using sundays() above...)
     */
    protected function weekly(callable $callback)
    {
        if (date('D', $this->timestamp) === 'Sun' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    /**
     * Called once every week at the specified day and time
     *
     * @param  string $day [Three letter format (Mon, Tue, ...)]
     * @param  string $time [HH:MM]
     */
    protected function weeklyOn($day, $time, callable $callback)
    {
        if (date('D', $this->timestamp) === $day && date('H:i', $this->timestamp) === $time) call_user_func($callback);
    }

    /**
     * Called each month on the 1st
     */
    protected function monthly(callable $callback)
    {
        if (date('d', $this->timestamp) === '01' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

    /**
     * Called each year on the 1st of January
     */
    protected function yearly(callable $callback)
    {
        if (date('m', $this->timestamp) === '01' && date('d', $this->timestamp) === '01' && date('H:i', $this->timestamp) === $this->runAt) call_user_func($callback);
    }

}