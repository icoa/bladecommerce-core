<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProjectClone extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'project:clone';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Clone the current schema and files for a new instance.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$target = "c:/wamp/www/l4.ninalondon/";

		//only recreate structure
		$dirs = [
			'app/storage',
			'app/storage/export',
			'app/storage/files',
			'app/storage/files/barcodes',
			'app/storage/files/delivery',
			'app/storage/files/invoices',
			'app/storage/files/rmas',
			'app/storage/logs',
			'app/storage/sessions',
			'app/storage/tools',
			'app/storage/uploads',
			'app/storage/views',
			'app/storage/xml',
			'app/storage/xml/danea',
			'app/storage/xml/googlemerchant',
			'app/storage/xml/sitemap',
			'app/storage/xml/zanox',
			'app/storage/yajit',
			'app/storage/yajit/default',
			'app/storage/yajit/huge',
			'app/storage/yajit/small',
			'app/storage/yajit/thumb',
			'app/storage/yajit/zoom',
			'vhost',
			'public/assets/groups',
			'public/assets/products',
			'public/cache',
			'public/media',
			'public/media/files',
			'public/media/images',
		];

		foreach($dirs as $dir){
			$targetDir = $target.$dir;
			if(is_dir($targetDir)){
				$this->comment("Directory [$dir] skipped");
			}else{
				mkdir($targetDir);
				$this->info("Created directory [$dir]");
			}
		}

		//copy directory and their content
		$dirs = [
			"app/config/google",
			"app/config/home",
			"app/config/live",
			"app/config/local",
			"bootstrap",
			"public/themes/frontend/layouts",
			"public/themes/frontend/config.php",
			"public/themes/frontend/functions.php",
			"public/themes/frontend/redirects.php",
			'public/themes/frontend/assets/cache',
			'public/themes/frontend/assets/fonts',
			'public/themes/frontend/assets/images',

			"public/themes/mobile/assets/css/variables.less",
			"public/themes/mobile/layouts",
			"public/themes/mobile/config.php",
			"public/themes/mobile/functions.php",
			"public/themes/mobile/assets/cache",
			"public/themes/mobile/assets/fonts",

			'app/storage/cert',
			'app/storage/fonts',
			'app/storage/meta',
			'vhost/config.txt',
			'public/media',
			'public/assets/groups',
		];

		foreach($dirs as $dir){
			$targetDir = $target.$dir;
			\Helper::copy(base_path($dir), $targetDir);
			$this->info("Copied file/directory [$dir]");
		}




	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('target', InputArgument::REQUIRED, 'The target path'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}