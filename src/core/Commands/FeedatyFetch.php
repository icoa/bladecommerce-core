<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\ZoorateSnippet;

class FeedatyFetch extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'feedaty:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch and store all aggregate ratings from Feedaty';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $codes = ['it' => '10213634', 'en' => '10213635'];

        $this->comment("Fetching general microdata");
        foreach($codes as $lang => $code){
            $zs = new ZoorateSnippet($code,$lang);
            $zs->fetch_default();
            $this->comment("General microdata saved for lang [$lang]");
        }


        $products = Product::where('is_out_of_production',0)->select('sku')->where('sku','<>','')->orderBy('id','desc')->get();
        foreach($products as $product){
            foreach($codes as $lang => $code){
                $zs = new ZoorateSnippet($code,$lang);
                $this->comment("Fetching product microdata sku:[{$product->sku}]");
                $zs->fetch_product($product->sku);
            }
        }
        $this->info("All finished");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}