<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BlameCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'blame:add';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add the blameable columns to an existing DB table.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {

        global $softDelete, $table_name;
        $table_name = $this->argument('table');
        $softDelete = ($this->option('softDelete') == 'Y');

        if (!Schema::hasTable($table_name)) {
            $this->error("Warning: table [$table_name] not found in the current schema.");
            return;
        }

        $this->comment("Seeding table [$table_name]...");

        Schema::table($table_name, function ($table) {
            global $softDelete, $table_name;

            if (Schema::hasColumn($table_name, 'created_by')) {
                $this->line("Table [$table_name] already has [created_by] column - skipping this action...");
            } else {
                $table->integer('created_by')->nullable()->unsigned();
            }

            if (Schema::hasColumn($table_name, 'updated_by')) {
                $this->line("Table [$table_name] already has [updated_by] column - skipping this action...");
            } else {
                $table->integer('updated_by')->nullable()->unsigned();
            }

            if (Schema::hasColumn($table_name, 'created_at') OR Schema::hasColumn($table_name, 'updated_at')) {
                $this->line("Table [$table_name] already has blameable columns - skipping this action...");
            } else {
                $table->timestamps();
            }

            if ($softDelete) {

                if (Schema::hasColumn($table_name, 'deleted_by')) {
                    $this->line("Table [$table_name] already has [deleted_by] column - skipping this action...");
                } else {
                    $table->integer('deleted_by')->nullable()->unsigned();
                }

                if (Schema::hasColumn($table_name, 'deleted_at')) {
                    $this->line("Table [$table_name] already has [deleted_at] column - skipping this action...");
                } else {
                    $table->softDeletes();
                }
            }
        });

        $this->comment("EOF seeding job");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('table', InputArgument::REQUIRED, 'The table to modify.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('softDelete', null, InputOption::VALUE_OPTIONAL, 'If the table support the soft-delete functionality.', null),
        );
    }

}