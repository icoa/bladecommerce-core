<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProjectAsset extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:asset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Prepare asset for production';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //disable HMR
        Config::set('assets.hmr', false);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $bool = $this->confirm("Are you sure to publish assets?");
        $this->info($bool ? 'YES' : 'NO');
        if (!$bool) {
            return;
        }

        $folder = $this->argument('folder');
        $pretend = $this->option('pretend');
        $this->comment($pretend);
        $pretend = $pretend == 'yes';
        $no_closure = $this->option('no-closure') == '1';
        $no_critical = $this->option('no-critical') == '1';
        $no_evaluate = $this->option('no-evaluate') == '1';

        try {
            $this->info("Prepare assets for [$folder]");

            if ($no_evaluate === false) {
                $this->info("Invoking page Url for LESS compilation");
                $url = \Config::get('app.url') . '/menu-info.html';
                $qs = 'splash=0&assetUseMin=false';
                if ($folder === 'mobile') {
                    $command = "node D:/wamp/www/l4.puppeteer/open.js theme mobile url \"$url?_theme=mobile&$qs\" width 480 height 800";
                } else {
                    $command = "node D:/wamp/www/l4.puppeteer/open.js theme frontend url \"$url?$qs\" width 1600 height 1000";
                }
                $this->comment("Executing $command");
                $output = shell_exec($command);
                $this->line($output);
            }

            $this->info("Handling blade assets for [$folder]");

            $blame_paths = [
                "public/themes/$folder/assets/css",
                "public/themes/$folder/assets/js",
            ];

            foreach ($blame_paths as $dir) {
                $files = glob("$dir/blade_*.*");
                $iefiles = glob("$dir/ie*.*");
                $critical_files = $no_critical == false ? glob("$dir/critical*.*") : [];
                $files = array_merge($files, $iefiles, $critical_files);
                foreach ($files as $file) {
                    $filename = basename($file);
                    \File::delete(base_path($dir . "/" . $filename));
                    $this->info("Deleted blade asset [$filename]");
                }
            }

            $commands = Helper::publishAssets($folder);
            foreach ($commands as $cmd) {
                $this->comment($cmd);
                if (!$pretend)
                    shell_exec($cmd);
                $this->info("-> OK");
            }
            $url = \Config::get('app.url');
            $asset_versioning = Theme::getConfig('assetVersioning');
            $asset_url = Theme::getConfig('assetUrl');

            $cdn = config('assets.cdn');
            if ($cdn) {
                $asset_url = '//' . $cdn . '/';
                if (config('assets.force_ssl')) {
                    $asset_url = 'https:' . $asset_url;
                }
            }
            $themePath = Theme::path();
            $cssSource = "public\\$themePath\\assets\\css\\blade_{$asset_versioning}.min.css";
            $defaultCssSource = "public\\$themePath\\assets\\css\\blade_{$asset_versioning}.css";
            $jsSource = "public\\$themePath\\assets\\js\\blade_{$asset_versioning}.min.js";
            $defaultJsSource = "public\\$themePath\\assets\\js\\blade_{$asset_versioning}.js";
            $cssCritical = "public\\$themePath\\assets\\css\\critical_home.css";
            $cssCriticalCatalog = "public\\$themePath\\assets\\css\\critical_catalog.css";
            $cssCriticalProduct = "public\\$themePath\\assets\\css\\critical_product.css";
            $cssCriticalPage = "public\\$themePath\\assets\\css\\critical_page.css";
            $basePath = base_path() . '/';

            //recompress asset
            $this->comment("Executing CleanCSS");
            $cmd = "cleancss -o $cssSource $defaultCssSource";
            $this->comment($cmd);
            shell_exec($cmd);

            if (file_exists($cssSource) and true === config('assets.sri.enabled')) {
                $this->comment("Generating SRI Hash for $cssSource");
                $this->generateSRI(base_path($cssSource), 'css');
            }

            $pageProduct = config('assets.pages.product');
            $pageCatalog = config('assets.pages.catalog');
            $pagePage = config('assets.pages.page');

            $criticalBin = 'D:/wamp/www/l4.penthouse/critical.js';
            $criticalBin = 'D:/wamp/www/l4.critical/run.js';
            $qs = 'splash=0';
            $qs = '';
            $phantom = [];
            if ($folder == 'mobile') {
                $phantom[] = "node $criticalBin theme mobile url $url?_theme=mobile width 480 height 800 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCritical}";
                $phantom[] = "node $criticalBin theme mobile url $url/$pageCatalog?_theme=mobile width 480 height 800 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCriticalCatalog}";
                $phantom[] = "node $criticalBin theme mobile url $url/$pageProduct?_theme=mobile width 480 height 800 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCriticalProduct}";
                $phantom[] = "node $criticalBin theme mobile url $url/$pagePage?_theme=mobile width 480 height 800 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCriticalPage}";
            } else {
                $phantom[] = "node $criticalBin url $url height 1000 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCritical}";
                $phantom[] = "node $criticalBin url $url/$pageCatalog height 1000 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCriticalCatalog}";
                $phantom[] = "node $criticalBin url $url/$pageProduct height 1000 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCriticalProduct}";
                $phantom[] = "node $criticalBin url $url/$pagePage height 1000 rebase {$asset_url} input {$basePath}{$cssSource} output {$basePath}{$cssCriticalPage}";
            }

            if ($no_critical == false) {
                $this->comment("Executing CriticalCSS");
                foreach ($phantom as $cmd) {
                    $this->info($cmd);
                    if (!$pretend)
                        shell_exec($cmd);
                }

                $criticalUrl = $asset_url . $themePath . '/assets/';
                $relativeUrl = "/$themePath/assets/";
                $base_path = public_path();
                $base_path = str_replace('\\', '/', $base_path);

                if (File::exists($cssCritical)) {
                    $this->comment("Replacing [$relativeUrl] to [$criticalUrl] into file $cssCritical");
                    $content = File::get($cssCritical);
                    $content = str_replace([$relativeUrl, '../'], $criticalUrl, $content);
                    $content = str_replace($base_path, '', $content);
                    File::put($cssCritical, $content);
                }

                if (File::exists($cssCriticalCatalog)) {
                    $this->comment("Replacing [$relativeUrl] to [$criticalUrl] into file $cssCriticalCatalog");
                    $content = File::get($cssCriticalCatalog);
                    $content = str_replace([$relativeUrl, '../'], $criticalUrl, $content);
                    $content = str_replace($base_path, '', $content);
                    File::put($cssCriticalCatalog, $content);
                }

                if (File::exists($cssCriticalProduct)) {
                    $this->comment("Replacing [$relativeUrl] to [$criticalUrl] into file $cssCriticalProduct");
                    $content = File::get($cssCriticalProduct);
                    $content = str_replace([$relativeUrl, '../'], $criticalUrl, $content);
                    $content = str_replace($base_path, '', $content);
                    File::put($cssCriticalProduct, $content);
                }

                if (File::exists($cssCriticalPage)) {
                    $this->comment("Replacing [$relativeUrl] to [$criticalUrl] into file $cssCriticalPage");
                    $content = File::get($cssCriticalPage);
                    $content = str_replace([$relativeUrl, '../'], $criticalUrl, $content);
                    $content = str_replace($base_path, '', $content);
                    File::put($cssCriticalPage, $content);
                }
            }

            if ($no_closure == false) {
                $this->comment("Executing Google Closure Compiler");
                $cmd = "java -jar d:\\wamp\\www\\closure-compiler.jar --js $defaultJsSource --js_output_file $jsSource";
                $this->comment($cmd);
                shell_exec($cmd);
            } else {
                copy($defaultJsSource, $jsSource);
            }

            if (file_exists($jsSource) and true === config('assets.sri.enabled', false)) {
                $this->generateSRI($jsSource, 'js');
            }

            $this->info("-> OK");

        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

    }

    /**
     * @param $path
     * @param string $type
     */
    protected function generateSRI($path, $type = 'js')
    {
        $theme = $this->argument('folder');
        $algorithm = config('assets.sri.algorithm', 'sha384');
        $hash = \Utils::getSriHash($path, $algorithm);

        $metadata_path = storage_path('files/themes');
        if (!is_dir($metadata_path)) {
            if (!mkdir($metadata_path, 0777) && !is_dir($metadata_path)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $metadata_path));
            }
        }
        $metadata_file = $metadata_path . DIRECTORY_SEPARATOR . 'meta.json';
        $json = [];
        if (file_exists($metadata_file)) {
            $json = json_decode(file_get_contents($metadata_file), JSON_FORCE_OBJECT | JSON_OBJECT_AS_ARRAY);
        }
        array_set($json, "{$theme}.{$type}", $algorithm . '-' . $hash);
        file_put_contents($metadata_file, json_encode($json));
        $this->info("Metadata file created at $metadata_file");
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('folder', InputArgument::REQUIRED, 'The folder theme name'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('pretend', null, InputOption::VALUE_OPTIONAL, 'Pretend option.', null),
            array('no-critical', null, InputOption::VALUE_OPTIONAL, 'Skip creation of critical rendering path.', null),
            array('no-closure', null, InputOption::VALUE_OPTIONAL, 'Skip creation of JS bundle via Google Closure Compiler.', null),
            array('no-evaluate', null, InputOption::VALUE_OPTIONAL, 'Skip creation of URI evaluation with disabled minification.', null),
        );
    }

}