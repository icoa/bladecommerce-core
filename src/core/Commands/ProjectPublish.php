<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ProjectPublish extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'project:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all resources for deployment that are under .gitignore';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $deployPath = "d:/wamp/www/l4.deploy/";
        $database = \Config::get('database.connections.mysql.database');
        $deploy_folder = date('Y-m-d');

        $target = $deployPath . $database . "/" . $deploy_folder . "/";

        if (!is_dir($deployPath . $database)) {
            mkdir($deployPath . $database);
        }
        if (!is_dir($target)) {
            mkdir($target);
        }

        $light = $this->confirm("Are you sure to publish resources to [$target] with 'light' setup ?");
        $this->info($light ? 'YES' : 'NO');


        //copy directory and their content
        $dirs = [
            "app/config/google",
            "app/config/live",

            "public/themes/frontend/layouts",
            "public/themes/frontend/config.php",
            "public/themes/frontend/functions.php",
            "public/themes/frontend/redirects.php",
            'public/themes/frontend/assets/cache',
            'public/themes/frontend/assets/images',
            'public/themes/frontend/assets/font',

            "public/themes/mobile/assets/css/variables.less",
            "public/themes/mobile/layouts",
            "public/themes/mobile/config.php",
            "public/themes/mobile/functions.php",
            "public/themes/mobile/assets/cache",
            "public/themes/mobile/assets/font",

            "public/static",
            "public/manifest.json",
            "public/browserconfig.xml",
        ];

        if ($light) {
            $dirs = [
                "app/config/live",
                "public/themes/frontend/layouts",
                "public/themes/frontend/config.php",
                "public/themes/frontend/functions.php",
                "public/themes/frontend/redirects.php",
                'public/themes/frontend/assets/cache',

                "public/themes/mobile/layouts",
                "public/themes/mobile/config.php",
                "public/themes/mobile/functions.php",
                "public/themes/mobile/assets/cache",
            ];
        }

        foreach ($dirs as $dir) {
            $targetDir = $target . $dir;
            \Helper::copy(base_path($dir), $targetDir);
            $this->info("Copied file/directory [$dir]");
        }

        //now copy the "blame_*" assets
        $blame_paths = [
            'public/themes/frontend/assets/css',
            'public/themes/frontend/assets/js',
            'public/themes/mobile/assets/css',
            'public/themes/mobile/assets/js'
        ];

        foreach ($blame_paths as $dir) {
            $targetDir = $target . $dir;
            if (!is_dir($targetDir)) {
                mkdir($targetDir, 0777, true);
            }
            $files = glob("$dir/blade_*.*");
            $iefiles = glob("$dir/ie*.*");
            $critical_files = glob("$dir/critical*.*");
            $files = array_merge($files, $iefiles, $critical_files);
            foreach ($files as $file) {
                $filename = basename($file);
                $this->info($filename);
                \Helper::copy(base_path($dir . "/" . $filename), $targetDir . "/" . $filename);
                $this->info("Copied blade asset [$filename]");
            }
        }

        //create zip file
        $zip = new \ZipArchive();
        $zipFileName = $target . $deploy_folder . '.zip';
        $this->line("Creating ZIP $zipFileName");
        $zip->open($zipFileName, ZipArchive::CREATE);
        $rootPath = $target;
        // Create recursive directory iterator
        /** @var SplFileInfo[] $files */
        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($rootPath), RecursiveIteratorIterator::LEAVES_ONLY);
        foreach ($files as $name => $file) {
            // Skip directories (they would be added automatically)
            if (!$file->isDir()) {
                // Get real and relative path for current file
                $filePath = $file->getRealPath();
                $filePath = str_replace("\\", '/', $filePath);
                $relativePath = substr($filePath, strlen($rootPath));
                $this->line("Adding $filePath as $relativePath");

                // Add current file to archive
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        $this->info("ZIP file successfully created");

        //build transfer.bat
        if (config('server.transfer_deploy.enabled') === true) {

            $template = 'pscp -i {ppk} {file}.zip {user}@{server}:/workarea/www/{folder}/l4/';
            $production_params = [
                '{ppk}' => config('server.transfer_deploy.local_ppk'),
                '{file}' => $deploy_folder,
                '{user}' => config('server.transfer_deploy.production.remote_user'),
                '{server}' => config('server.transfer_deploy.production.remote_server'),
                '{folder}' => config('server.transfer_deploy.production.remote_folder'),
            ];

            $staging_params = [
                '{ppk}' => config('server.transfer_deploy.local_ppk'),
                '{file}' => $deploy_folder,
                '{user}' => config('server.transfer_deploy.stage.remote_user'),
                '{server}' => config('server.transfer_deploy.stage.remote_server'),
                '{folder}' => config('server.transfer_deploy.stage.remote_folder'),
            ];

            $lines = [
                str_replace(array_keys($production_params), array_values($production_params), $template),
                str_replace(array_keys($staging_params), array_values($staging_params), $template),
            ];

            $targetBatFile = $target . 'transfer.bat';
            file_put_contents($targetBatFile, implode(PHP_EOL, $lines));
            $this->info("BAT transfer file successfully created");
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(//array('target', InputArgument::REQUIRED, 'The target path'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(//array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
        );
    }

}