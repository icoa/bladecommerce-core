<?php

use Illuminate\Console\Command;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Filesystem\Filesystem;

class clear_cacheview extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cache:clearblade';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Flush the application Blade engine cache.';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        // get compiled path
        $compiled_path = rtrim(Blade::getCompiledPath(null), md5(null));

        // find and delete compiled files (dot files will be ignored)
        $filesystem = new Filesystem();
        $filesystem->remove(Finder::create()->in($compiled_path)->files());

        // display info message
        $this->info('Application Blade cache engine cleared!');
    }
}