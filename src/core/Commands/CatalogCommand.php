<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CatalogCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'catalog:do';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'General catalog handler.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire() {
        $this->line('Executing CatalogCommand...');
        $task = $this->option('task');
        $this->line("Try to fire task [{$task}]");
        $cmd = "_" . strtolower($task);
        if (method_exists($this, $cmd)) {
            $this->{$cmd}();
        } else {
            $this->error("No [{$task}] task founded!");
        }
    }

    public function _brand_image() {
        if (!$this->confirm("Are you really want to reinit all brand images? [yes|no]", true)) {
            $this->comment('Exiting...');
            return;
        }
        $this->line("Working...");
        $rows = Brand_Lang::all();
        foreach ($rows as $row) {
            $img = "/media/images/brands/{$row->slug}.png";
            $query = DB::table("brands_lang")
                    ->where('lang_id', $row->lang_id)
                    ->where('brand_id', $row->brand_id)
                    ->where('image_default', '<>', '')
                    ->update(array('image_default' => $img));
            $verbose = "[$row->brand_id][$row->lang_id] => [$img]";
            $this->line($verbose);
        }
    }

    public function _brand_check_image() {
        $this->line("Checking...");
        $path = public_path();
        $this->line("Considering root path: [$path/media/images/brands]");
        $rows = Brand_Lang::where('image_default', '<>', '')->get();
        foreach ($rows as $row) {
            $file = $path . $row->image_default;
            if (!\File::exists($file)) {
                $this->error("File [$file] does not exist!");
            }
        }
    }

    public function _attribute_set_clone() {
        $this->line("Attribute set cloning...");
        $source_id = 2;

        $obj = new AttributeSet(
                        ['uname' => 'Orologi Hip Hop']
        );
        $obj->save();

        $target_id = $obj->id;

        $rows = DB::table('attributes_sets_content')->where('attribute_set_id', $source_id)->get();
        foreach ($rows as $row) {
            DB::table('attributes_sets_content')->insert(
                    [
                        'attribute_set_id' => $target_id,
                        'attribute_id' => $row->attribute_id,
                        'position' => $row->position,
                    ]
            );
            $this->line("Inserted new content for [$row->attribute_id]");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments() {
        return array(
            array('example', InputArgument::OPTIONAL, 'An example argument.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions() {
        return array(
            array('task', null, InputOption::VALUE_REQUIRED, 'The task to perform.', null),
        );
    }

}