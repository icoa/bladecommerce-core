<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 09/12/2014
 * Time: 14:07
 */

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\CronTasks as Cron;


class CronTaskCommand extends Command {
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cron:task';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute a single cron task (use "help" to list arguments)';
    /**
     * Application instance
     *
     * @var Illuminate\Foundation\Application
     */
    protected $app;
    /**
     * Currencies table name
     *
     * @var string
     */
    protected $table_name;


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $task = $this->argument('task');
        $lang = $this->option('lang');
        $locale = $this->option('locale');
        $type = $this->option('type');
        $this->comment("task: $task | lang: $lang | locale: $locale | type: $type");
        //php artisan cron:task sitemap --lang=it --locale=IT --type=categories
        if($task === 'help'){
            $this->showHelp();
            return;
        }

        Registry::set('console', $this);

        if(method_exists('services\CronTasks',$task)){
            $this->info("Executing Cron::$task()");
            $langTasks = ['googleMerchant','zanox'];
            $localeTasks = ['googleMerchant','zanox'];
            $typeTasks = ['sitemap'];

            if($lang == null AND in_array($task,$langTasks)){
                $this->error("ERROR: LANG parameter required for task $task");
            }

            if($locale == null AND in_array($task,$localeTasks)){
                $this->error("ERROR: LOCALE parameter required for task $task");
            }

            if($type == null AND in_array($task,$typeTasks)){
                $this->error("ERROR: TYPE parameter required for task $task");
            }

            if($lang != null AND $locale != null){
                Cron::$task($lang,$locale);
            }

            if($type != null){
                Cron::$task($type);
            }

            if($type == null AND $lang == null AND $locale == null){
                Cron::$task();
            }

            $this->info("Finished execution of Cron::$task()");

        }else{
            $this->info("WARNING: Method Cron::$task does not exist");
        }
    }

    private function showHelp(){
        $commands = [
            [
                'php artisan cron:task sitemap --lang=it --locale=IT --type=categories',
                'Generate the Sitemap XML for given lang/locale and type'
            ],
            [
                'php artisan cron:task googleMerchant --lang=it --locale=IT',
                'Generate the Google Merchant XML for given lang/locale and type'
            ],
            [
                'php artisan cron:task handleCache',
                'Re-init Redis cache for common Models'
            ],
            [
                'php artisan cron:task handleCacheAdvanced',
                'Purge Redis cache for common Models'
            ],
            [
                'php artisan cron:task statesCheck',
                'Perform various checks on Products status'
            ],
            [
                'php artisan cron:task removeDeletedProducts',
                'Removed deleted products from cache'
            ],
            [
                'php artisan cron:task wanted',
                'Re-establish most wanted attribute for Products'
            ],
            [
                'php artisan cron:task sessionLottery',
                'Performs session lottery clean-up for session files'
            ],
            [
                'php artisan cron:task googleAnalytics',
                'Save JS files for Google Analytics locally'
            ],
            [
                'php artisan cron:task downloadCurrencies',
                'Download updated currencies from the Internet'
            ],
        ];
        $this->table(['Command', 'Synopsys'], $commands);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('task', InputArgument::REQUIRED, 'The task to execute.'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('lang', null, InputOption::VALUE_OPTIONAL, 'The language specified for some tasks', null),
            array('locale', null, InputOption::VALUE_OPTIONAL, 'The locale specified for some tasks', null),
            array('type', null, InputOption::VALUE_OPTIONAL, 'The type specified for some tasks', null),
        );
    }
}