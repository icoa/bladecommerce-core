<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 28/10/14
 * Time: 17.23
 */

namespace  Core;

class Param{

    protected $params;

    function getParam($param, $default = false){
        if(!$this->params){
            return $default;
        }
        if(isset($this->params->$param) AND $this->params->$param != ''){
            return $this->params->$param;
        }
        return $default;
    }
}